using System;

namespace Bio.Portal.Server.Common.BioMembership
{
	/// <summary>Interface that exposes the identifier of an instance.</summary>
	/// <typeparam name="T">the type of the identifier.</typeparam>
	public interface IIdentifier<T>
	{
		#region Properties

		/// <summary>Gets or sets the identifier for this instance.</summary>
		T Id { get; set; }

		#endregion
	}
}