using System;
using System.ComponentModel;
using System.Globalization;

namespace Bio.Portal.Server.Common.BioMembership
{
	/// <summary>Class that implements equality and hash code the way it is supposed to be.</summary> 
	/// <typeparam name="T">the type we want to have equality and hash code implemented.</typeparam> 
	/// <typeparam name="ID">the type of the identifier.</typeparam> 
	public abstract class BaseEntity<T, ID> : IEquatable<T>, IIdentifier<ID> where T : BaseEntity<T, ID>
	{
		#region Fields

		private int? _hashCode;

		#endregion

		#region Properties

		/// <summary>Gets or sets the identifier for this instance.</summary>
		[DataObjectField(true)]
		public abstract ID Id { get; set; }

		#endregion

		#region Methods

		#region Equality Methods

		/// <summary>Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.</summary>
		/// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
		/// <returns>true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return Equals(obj as T);
		}

		///<summary>Indicates whether the current object is equal to another object of the same type.</summary>
		///<returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
		///<param name="other">An object to compare with this object.</param>
		public virtual bool Equals(T other)
		{
			if (other == null) {
				return false;
			}

			// if both entities are transient, they are the equal if they're the same reference
			if (IsTransient(this) && IsTransient(other)) {
				return ReferenceEquals(other, this);
			}

			// otherwise, check Id equality
			return Id.Equals(other.Id);
		}

		/// <summary>Implements the operator ==.</summary>
		/// <param name="x">The first object.</param>
		/// <param name="y">The second object.</param>
		/// <returns>The result of the operator.</returns>
		public static bool operator ==(BaseEntity<T, ID> x, BaseEntity<T, ID> y)
		{
			return Equals(x, y);
		}

		/// <summary>Implements the operator !=.</summary>
		/// <param name="x">The first object.</param>
		/// <param name="y">The second object.</param>
		/// <returns>The result of the operator.</returns>
		public static bool operator !=(BaseEntity<T, ID> x, BaseEntity<T, ID> y)
		{
			return !(x == y);
		}

		#endregion

		#region Method Overrides

		/// <summary>Serves as a hash function for a particular type.</summary>
		/// <returns>A hash code for the current <see cref="T:System.Object"/>.</returns>
		public override int GetHashCode()
		{
			// once a hash code has been returned, we have to return the same hash code
			if (_hashCode.HasValue) {
				return _hashCode.Value;
			}

			// if this object is transient, we use the base hash code for the life of the instance
			if (IsTransient(this)) {
				_hashCode = base.GetHashCode();
				return _hashCode.Value;
			}

			// otherwise return the hash code of the identity
			return Id.GetHashCode();
		}

		/// <summary>Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.</summary>
		/// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.</returns>
		public override string ToString()
		{
			return String.Format(CultureInfo.CurrentCulture, "[{0}]", Id);
		}

		#endregion

		#region Helper Methods

		/// <summary>Determines whether the specified entity is transient.</summary>
		/// <param name="obj">The entity to check.</param>
		/// <returns>true if the specified entity is transient; otherwise, false.</returns>
		public static bool IsTransient(BaseEntity<T, ID> obj)
		{
			return Equals(obj.Id, default(ID));
		}

		#endregion

		#endregion
	}
}