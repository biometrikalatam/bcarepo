﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestDMAWS.DMANECTransactionManagerWS;

namespace TestDMAWS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] byWSQ = null;
                byte[] byRAW = null;
                short w, h;
                

                System.IO.FileStream fs = new FileStream(@"C:\tmp\0A\01.wsq", FileMode.Open);
                byWSQ = new byte[fs.Length];
                fs.Read(byWSQ, 0, (int)fs.Length);
                
                byte[] byHeader = CreateHeader();

                DMANECTransactionManagerWS.TransactionOut txout;

                DMANECTransactionManagerWS.TransactionIn txin = new DMANECTransactionManagerWS.TransactionIn();
                txin.header = byHeader;
                //txin.biometricData = byWSQ; // byWSQ;
                txin.transactionType = "FEW1";
                //txin.transactionType = "FER1";
                txin.workstations = 0;


                System.IO.StreamReader sr = new System.IO.StreamReader(@"C:\tmp\DMA\7_0_wsq.txt");
                string sWsq = sr.ReadToEnd();
                sr.Close();
                byWSQ = Convert.FromBase64String(sWsq);
                Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                bool b = dec.DecodeMemory(byWSQ, out w, out h, out byRAW);
                txin.biometricData = byWSQ;
                pictureBox2.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(byRAW, w, h);


                using (DMANECTransactionManagerWS.TransactionManagerWSService target = new DMANECTransactionManagerWS.TransactionManagerWSService())
                {
                    txout = target.generateTransaction(txin);
                }
                int i = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //9999 = "Connections could not be acquired from the underlying database!"
                
        }

        /*
        Nombre Tipo Descripción
            RUT         Numérico (8)    RUT de la Persona a verificar
            DV-RUT      String(1)       0 (si no interesa el dato)
            Office      Numérico (4)    0000 (si no interesa el dato) Código de oficina donde se realiza la operación
            Terminal    String(20)      Dirección IP de la terminal donde se realiza la operación 
            Channel     String(4)       0000 (si no interesa el dato) Código de canal (para organizaciones con varios canales de atención)
            Date        Numeric(8)      MMDDYYYY
            Hour        Numeric(8)      HHMMSSCC
            CUT         String(30)      Código único de transacción. 
        */
        private byte[] CreateHeader()
        {
            //ID 2128441520E2C074D9D2D4BC980668
            string trackid = Guid.NewGuid().ToString("N").Substring(0,30);

            string sHeader = "2128441520000192.168.5.86        00000403201614404500" + trackid;
            return ASCIIEncoding.UTF8.GetBytes(sHeader);
        }

        private void button2_Click(object sender, EventArgs e)
        {

            SendMail("gsuhit@gmail.com", "Testito", "test from Contacto", null, "pdf", "nom");

            //try {
            //    ///* Esta clase representa las transacciones de entrada. */
            //    TransactionIn transactionIn = new TransactionIn();
            //    /* Bloque de bytes correspondientes a la estructura Header. */
            //    byte[] headerIn = CreateHeader();
            //    transactionIn.setHeader(headerIn);
            //    /* Tipo de transacción. */
            //    String transactionType = ……;
            //    transactionIn.setTransactionType(transactionType);
            //    /*
            //    * Bloque Biométrico (puede ser minucias, imágenes RAW, imágenes
            //    * WSQ, según corresponda).
            //    */
            //    byte[] biometricData = ……;
            //    transactionIn.setBiometricData(biometricData);
            //    /* Bloque Biométrico (puede ser minucias, imágenes RAW, imágenes
            //    WSQ, según corresponda). En caso de las transacciones de comparación de bloques biométricos, este campo es obligatorio. En transacciones de extracción este campo es opcional y su valor no es tenido en cuenta. */
            //    byte[] compareTo = ……;
            //    transactionIn.setCompareTo(compareTo);
            //    /* Identificador de Estación de Trabajo origen de la transacción. */
            //    int workstations = ……;
            //    transactionIn.setWorkstations(workstations);
            //    /*
            //    * Se inicializan los objetos que permiten consumir el servicio
            //    * web publicado por el sistema AFIS DMA 1:1.
            //    */
            //    TransactionManagerWS tm = null;
            //    TransactionManagerWSServiceLocator locator = new TransactionManagerWSServiceLocator();
            //    String urlStr = "http://127.0.0.1:8081/AFISIOBankWS/services/TransactionManagerWS";
            //    URL url = new URL(urlStr);
            //    tm = locator.getTransactionManagerWS(url);
            //    /* Esta clase representa las transacciones de salida. */
            //    TransactionOut out = null;
            //    /* Se genera la transacción. */
            //    out = tm.generateTransaction(transactionIn);
            //    Interfaz AFIS DMA 1:1 31
            //    /* Se procesan los resultados de la transacción. */
            //    if (out != null) {
            //    /*
            //    * Si el resultado de la transacción es 'Error', significa que
            //    * hubo un problema al procesar el trabajo.
            //    */
            //    if ("Error".equalsIgnoreCase(out.getResult())) {
            //    Error[] errors = out.getErrorArray();
            //    for (int i = 0; i < errors.length; i++) {
            //    ……
            //    }
            //    } else {
            //    /*
            //    * En otro caso, la transacción terminó exitosamente. A
            //    * continuación, se procesan los resultados.
            //    */
            //    byte[] headerOut = out.getHeader();
            //    ……
            //    String result = out.getResult();
            //    ……
            //    int score = out.getScore();
            //    ……
            //    String tcn = out.getTransactionControlNumber();
            //    ……
            //    /*
            //    * En caso de una transacción de extracción, la minucia resultante se obtiene invocando al método 'getMinutia'.
            //    */
            //    byte[] minucia = out.getMinutia();
            //    ……
            //    }
            //    }
            //    } catch (Exception e) {
            //    ……
            //    }
        }

        public static bool SendMail(string mailto, string subject, string body,
                                    byte[] attach, string mimetypeattach,
                                    string nombreattach)
        {
            bool ret = false;

            try
            {
                //save the data to a memory stream
                //MemoryStream ms = new MemoryStream(attach);
                //Envio de Correo
                MailAddress SendFrom = new MailAddress("soporte@biometrika.cl");
                MailAddress SendTo = SendTo = new MailAddress(mailto);
                MailMessage MyMessage = null;
                try
                {
                    SmtpClient emailClient = new SmtpClient("smtp.gmail.com");
                    emailClient.Port = 587;
                    emailClient.EnableSsl = true;
                    NetworkCredential _Credential =
                                new NetworkCredential("soporte@biometrika.cl",
                                                      "B1ometrika2014",
                                                      null);
                    MyMessage = new MailMessage(SendFrom, SendTo);
                    //log.Debug("Correo Enviar a:" + SendTo + " a para : " + SendFrom);
                    //ms.Position = 0;
                    //string mt = MediaTypeNames.Application.Pdf;
                    //if ((mimetypeattach == "docx") || (mimetypeattach == "doc"))
                    //    mt = MediaTypeNames.Application.Rtf;
                    //else if ((mimetypeattach == "xls") || (mimetypeattach == "xlsx"))
                    //    mt = MediaTypeNames.Application.Octet;
                    //else if ((mimetypeattach == "pdf"))
                    //    mt = MediaTypeNames.Application.Pdf;
                    //else
                    //    mt = MediaTypeNames.Application.Octet;
                    //MyMessage.Attachments.Add(new Attachment(ms, nombreattach, mt));

                    MyMessage.Subject = subject;
                    MyMessage.Body = body;
                    emailClient.Credentials = _Credential;
                    try
                    {
                        emailClient.Send(MyMessage);
                        ret = true;
                    }
                    catch (Exception ex)
                    {
                        //log.Error("emailClient.Send(MyMessage)- Error", ex);
                        ret = false;
                    }

                }
                catch (Exception ex)
                {
                    //log.Error("emailClient.Send [" + ex.Message + "]");
                    ret = false;
                }

                //if (ms != null)
                //{
                //    ms.Close();
                //    ms.Dispose();
                //}
            }
            catch (Exception ex)
            {
                //log.Error("Error SendMail", ex);
                ret = false;
            }

            return ret;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] byMinutiae = null;
                System.IO.StreamReader sr = new System.IO.StreamReader(@"C:\tmp\DMA\pc1.txt");
                string sMin = sr.ReadToEnd();
                sr.Close();
                byMinutiae = Convert.FromBase64String(sMin);

                byMinutiae = Convert.FromBase64String("UEMxcYxzQDcE5DyKdtmQcuWSf2V9a7mjbXqCW6FzfNGEj2GkYlmZV3GsbJZqcbV6Up+clf1kgcV5mWtqjl+3cLy3fidgaKlff7Guk4NqUaG/dri1U9eMP4GFqXGfpntpSZa9jxdUaalajxG1SuFgTBR2rHXHYNdVlA1nqPOorwaurISOL3eEuXm8pwnMU+CyNmlRnvihLelfrnJzyHuOG3OGywCPzHyF0IqA0gSk34I3RQ==");
                

                byte[] byWSQ = null;
                byte[] byRAW = null;
                short w, h;
                sr = new System.IO.StreamReader(@"C:\tmp\DMA\WSQ_GGS_Dedo1.txt");
                string sWsq = sr.ReadToEnd();
                sr.Close();
                byWSQ = Convert.FromBase64String(sWsq);

                byWSQ = Convert.FromBase64String("/6D/pAA6CQcACTLTJc0ACuDzGZoBCkHv8ZoBC44nZM0AC+F5ozMACS7/VgABCvkz0zMBC/KHIZoACiZ32jP/pQGFAgAsA1VMA2ZbA1VMA2ZbA1VMA2ZbA1VMA2ZbA3FiA4gPA22pA4OXA2rYA4A3A3L4A4n2A205A4MSA2xQA4H6A2JrA3YaA2cxA3vUA2TiA3kPA1qmA2zHA1zoA298A2dkA3wSA2gbA3ztA2m6A37gA2vSA4FjA3F0A4glA4OiA532A3QhA4tbA4X6A6DFA4TiA592A4m/A6VMA4UpA5/LA43KA6omA3qYA5MdA4gkA6NeA3/uA5mEA4QKA55yA4sQA6bfA5h6A7b4A4b/A6H/A467A6tHA2GgA3UmA23+A4P9A3/mA5l6A3+wA5k5A3c4A48QA35QA5eUA4TrA5+BA4BdA5oJA31AA5ZNA3vEA5SFA4GiA5uPA4OUA53lA4IyA5w9A4QoA56WA5FAA65MA5JnA6+uA5nIA7iKA3+QA5kTA6v9A85jA4KIA5yjA7tmA+DhA3rCA5NPA4SwA585A7PhA9fbA88wA/igAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/6IAEQD/AgACAAJeqQRJpu0nv/+mAHYAAAADBAQIBggLChIbAAAAAAGztbGytrcCsLi+EBGur7m6u70DDxKsrbwIExRqqaqrvwQHCRUWpqeowMLDChcYmZ2io6XBxAYMGhx0hYqNkpaboKGkxcbIygUOHR5aXWlvdXl+f4CBgoOHiY6Qk5WYnJ6fzP+jAAMAvtttttve973ve9982yDja973ve99/b08fD7PeN973ve9z9f7Pb9X0/Zlvve973v9f0/9f9/p+r7PDbe973vf9H5f8v8AD/T/AJ/4+22973ve/wCX8n5P7/8AH+78/wCrO+973vf8/wDn/X/Z+b+39H6mvve973+n83+v5f8Ab/f6vCb3ve97+79P0/8An1f++WVr3ve973vffzve973ve97777777773ve973ve973ve973ve973ve973ve973ve973ve973ve973ve973ve973ve97qhP4UUF80KGl5XZUZu6hdjCHYuex233012r2Lyy1bpbnPp95qVtGUelBS7xmvS6q9gXXqQK/pX4SiILHscsooex3VoIPZmojN0fVvEaHRLsrp9i8oXYR8oz60XpOijpnUr0269bb6zMekuSSf6DwPzPUzQ+pw5evzQbPR12HoQ7OuzIaVr52j1M3OZn1ZVqszqD6cupLvEdjqU0Jf1Rh/mNEDQ+tEOS5BELuXBFIIrtRQq5hAkJLlIKBkRPBYoJKhcoFLAoswIUGrQlhAKo7RAlQu2000kHPZysSxizOCaITjJsHmsRZMzIrjmDJ04TzME1XEuYkjSesNKNcM3coPEIIiTgkqzoXgIpycHs4tWEgU8A8iEWpCo55OuBoQj3gNoSaEhH5y6Jbg/cmdkS8Akg8jo6omhGjLAskQcy8KKrGCUi6SZ3QOCTgyIIQkMsE5DWh4gQaLByxyCsgnocc2d9DKKrGjA4dLAnQK1tNJbSp45AkLadKvmxiuOYOgVolc0HDYMhZ4zVLWtCw0MhoZnRsEJwkNDRzyEnhCwMzEouQZTzy6PmWrQuGevcfSe8BuSEuC9aEBBApdqT0dvQQeSerOkodERGJgoGkkkg8qhoWdCHQXI2TBAw8Tsjg7uU+mpB0kvi6zeWQZSnMLCEIge1wtDmxWExOwzJ8nWimJWDzrs9RXRUkOeLVq+2sC2oQcMuJeevXV3Ntb2BxXWNpObmSmc4taHylplVEZnEmZRKNrGU6xeUCCC4MBd4D4IEHgvxpJOfYXIiKIOT3OFAJRUFYuIoQWcqO2yehDRAMBYoNAkGKJdqKehKLk0WD0hGsFn2DpckZeln2yeznGASVKjPQNylV1h3aW0kEHBLotHUxILI42yl4DB4EhJYyGMSVUFA8msFnCq4SR5FniCCiD3IrgqFd4DcGi/EaFJBJdxBXAk0PcWhwnEQI7SuDBEu9FgUknTkwlHaihAMiFkuS4HRhEvEQlghFTKioLuxfCsQzA2I0zgrF3rEtos9mqyZzxJHOW2F9AyhQsLeFdYIg9RmXzPHr/APa+/wAtGB0aYaDhf9zQikCjKSw0LuIZxLPR1g8VKUEOiijgUSUkJM/iPBA94DcEH8aoqEg8koQRIc0PazwDBcHgcXQNLSjBntcEuCqFi65KjmCiHD9qCSd6HVA42EIGmdsyxY4RmoBXVp8oKjGq02gZvudniS+BNg7+La11zEqMJqL26Jtsp1Kc4OtOlRJgEw5OEOmqdJghgZOHVmD0RBl1yQTrSHlgWPIyYooTsgu8B2CCD86oYK4LtQhQ6eAavPJBOapFPFEcDCFUIVIPaTCawMhUc4ooh9SWkpI4I00ZzNpJh1h1zdQHMO5QdYW2ESDF/t01cNj7VvXQWf7/AGjXIE4fsz2y6SNMv3fV95d2wa/n93u+LPPTVWssXj3/AC0YPHTRMK2w61qC6cupRc4b0cw4UJWhLEgmLAynBLYF6JGhkv3gN8vYggqFH1kMeBRSR9ZChULh+1IlTIijWPJilGYgJkFg6IImRArIKwRREl40CcScMoZ6qoQTRR8JazWVg7iEicMqQfBIO7govxzIT+aFtBUwgcCms23w8JexSxsiM9Onv15wyVDxdOXrr15y7Z10WDh5TB9KQ6bFtCZztaWhrZ2ywnIFoqWEF5rbC0CVoIsmikA8T6l3gM2gfxKhSD+xUQRJQQXrIgGhPckWCEFPIdYEIIpDMN0e3JNqz5l3Itd3wYhIxoHtpGWawnKtVp56OU7HScOtupWf7egjrMKxwXvH3Hn8Id4o9Wwim+8a9Yc1bWdI46wILsvlLQmJwTqw1OWd3rmnSwzOskbV8BaAca2PQkKNfIF1i4QizuefwUy/bMGqinvTrkqvVFKkklPigu8BnlHpXrjVKionR7Z2Id4KCQPK2xrZ0FEBHlO+1ZygWaXEHHfTd9ZiCUUFg22an5dNQUHMPhOvX3NfKjEwljNd/K1+ezuFoicIM/bu8Nn1dURwUm/6/r98tnHjCD4Io6v9zHXpDMJPF1WPH5fw/fZWhGHfjMF4PXy1qYBOWp/lqRZ9fHp5Smfn7vM8U4VPj/HNyiw+C4vKOvh/H4v9vnzhVZcfg0HfT5c/138dqINx8rN59OZ93t5+ZexEcUghFECqLvAZVcCkfYnsapPQ9pSznZjaK1ZLEqXGpynavPM9sKXlvB9enSahYFOudofn4+XvylzgyKptbXrv10IWGUlav8GO3ntmDj8I8jGXyv8AHradtVj5e74jX7K6On5+FarC2rZb5LpYGtdes4JSav8AbpkMkMquuNWzjwm2WqFlv92i4o2K/f0vzsmb3+Or8WhzHn1aPtzJhwuK2nM8zHt+HX5ff0WMZcz/AA0C06U8E8YW8/jv7d5fNae67h1xy+7r8GtU06aTUScCD+Beg94DLn+eKL6EDwKSJ7SEqShC7jQ2dSnZkjyj0O8bCJftQRonL1ZJYOhKBIMzqjjrIXA+UZGtWxXOxeD8o03rGuiw56t1J8FRWjJjgx39wdGr3rbZ3PEuK+IOuXjrNausCNLbaitWtFrPXA1++BIsq9Hq4WLeO+0l5tqIEY1856GKGoch8Swh6ZiIj1vAKh6Qg/eA6q/kIP0Gho4PsVEEaLuQJCpAgg4oODFGoSTi/CCU9CIg4kIITRKsdtRKKiXYOeU685Bbbm3SzmDh9d56QDv5R98+CSPFv121rEWOW3Pc2iOOfj7oaiao6uEsM1C86Vnr4WjRJ+JYu5SEuEXOG3jdqEKBLErCGJBEKH9CwqJsSk7lCOSCQIIQRB7wHUISS+c0IhzSF3Hg6kPEJd0S5ENkxYLuSINeg2DntJKMA72sYBwzLkKeu9per8pFQ07WdeTM/J6h0+fNGFMRjVnKjSXc5b1yfCVWrhpTi3O3XQ4rrULojD75EniUwmq0l3t0TWxMjn0VXNq6JNjPvW5hEyHD4nn93xrRPOusv29bOw1ThP7GQd4Cf0HE+hfgPeA2y4L6EgSUVwXrMIpA/Ok8F6IFckjSWTGiSOLmKQ6eZKCwiCYYwTJTnFqQ0lyogw6wtZ3hM75bNBEYRz+L2NZmXdrEHjWP27OXq1YmGDzxiPNtIK0ma85JXGGMskSTr1zgrAu3vtkNfKzSE54qBDWD1QdCDhWU21FJTmCcNa1UJFI0rydPSHSLPCC7wGw//6YAgwEAAgEBBAMHCAsRFhALCQAAs7UBAgMEsrYFBrEHCBAREmm3CQoMExRqsLgLDQ4PFRYYRElKrxcaG0FCQ0VGR0hLTU5QUVK5GR0eH0xTVFVWV1laXl9gY6ytrrq7vRwgISUnKk9YW1xdYmSnvL4iIyRAYaSmqaqrwikrLC8xO7/Aw/+jAAMB/rfvoxjD0jESn3xVsdzhoiQ7SDWDQ44oExsYZhOCMGGDpOmORmGEAhjI8lKxkyuDi5wiGzOmHtPdFM5/dPBy4PP/AC/l93/97oZP/H/XDnph8+0/j/v0J92Zt4OM44fu6eA4g4CvA4fxSYNmGDFPDwzGNOMwi8M7KaYmD8L3gOm6H4GP3swjoR5AhpmZe5I2aXiUxrCkccXMztihg9uNCBjx9GI9I4m0c55OYJmFEY8GKM2mbJ3fw6Ym0RWPDHSMUOm2cx7sBFEXkq5YYz6QcYmKRU4sAIUj2hDvAd1piNjuYTZgQ9BNoMWMDiQwbZjRDtwRmOhcjxwo9JnGSZeKuXPSEZjEOD4lFPQYx4YzDNH8tiZi8Nvd0rGfd4f7bZwx4fu/7/8AmdMHj4OfdhOI5+7w2hnp4fu6U8Npjb91ZmczJDh45EzF6HSMOObuYDRy6RpuwjwMxhuz3gOsfhNT0pdh7xZs0cim7B9SDCPpwZITEXtMucAdKXuKbOMxHiDYiZO4xkziumOkIvDEznGDK4tnkQLZ8MZE446TDEzbHc2zRAaeIkbYRO0gxCIduAwQpp7wH7T4Apo/A+gsCQoOWcrkWNByKc6A9uMRIQPAjyc5Fcxe4smekwzadHg5zMzDNrYOObmVgzPa9GZJgSMeGyEwpszLyRrECwcsaZgPocmCNYYqccqRIQo9K2e8B2z/AO0iP3iLYgx9BTcp95oayccWwwGL2jisDsaHBFMwYlJwCAZgZmcZOBFBxmMyQ4dMGJhhHLh5fcZCyxE4bZjWVKI8mhi5ozDhmmKEGPJuAj3gOy/hWz7xYphH0KFMVhxCKTESjtCYhigXkZYa5jyAzls5Tt2hG6vdks402Hj0ysyYysKOHSujg2OlYhxwdMkXKUPEOmbL0pF4YRCBss6THFSAKwgcQjWL4O5rLTmA94D6L/S0lHvjs2eJEpumT0YoIY9GaIDG7wbFhTu2YkSYpz3EQs0BwLCUGZjljpQwxBE5EwWIEQ4YxtGiMR5eIlZg6nnbZxEgMfQEYQp5ENSA94DsHvrY0e0IJCijk2KaYckzFKKF7sWHBCng5mCbZCbYh24YwhiOHlgzQ+P750Ho8D3fxWGZh8To8fH3fu8dpjpTidBPPnxxPGZMB0yR4Zh0L9J4THTiQz4xxDJnGeOcZPHGRBiHBGYiIRR4ACUXffe8B+l0D7zD1F3Q9TTA9I0UQhH04RI0cVYDMQoOIl84Qp44Ujs4KwrwL4MwgicCeHTDBxlx6DOcmFDPc9ApExMx4Z8GJQoPamINIPaxsWYnJityJ3gPsw/pxH3yYSintC7AMOOKQIUjScUWnFAnHNEc+Gdlw8sQsOPcBnl0KIg9H3dI8NqwYbbRFPO4ym1dHPSKHnPEQmaUxl4jMTGXbaYI8DJ44hEmaXhlxtSzFDy6RDGgnJYI2ID3gPy/eIWF+8waPeIjiK/ea6EPTgirGzxztBwkyEDiEyQjhw92LleOGY7RcTBlDMccGIBGbZwhxTOQHO2FxxxQFZHGU5J42JiAPEmSmzE5ZsRsXeA2yEYQ7wIaWz3FmAPoWEIoPv5LnLGMCRLPcLROkSniXaY57kgZZmNEOGKCse4zTR58Q2hGYJmHI2pQcCx4mKddqOGMliww5bTKOijywNgmPeNQ7wH7fvn4DUPeaLDDtew9BdiduJmmwUnEhCwMTkQ0IMB4LBKY2Y+dWmmJHtICRBI8WMyDH0mItmwnaIlgscMZoVWj33vAd4+AYwj7zWYRu+rBq9whTB7jV6MVOBoYpY9qmJnpFLPAGZsGYdzSTDGZhxcVmhWHcXKXHTKcGYxMtYcbeEOA5gBGZrHFUbBGzw9wWxhsvdlIQigHAjDDc7wH6Pwt33xg+8bslHaUjEgD2tJGEORGKJBjDgMKQxAhyaaUozHhgiN30oEFIdywdEYdqUxYejCRxfAHpHBMxI8MGGFDMWDg6bQh3gNu/hybk7umSMIj3Zz/ALBhMOBeWZnx2TGfujk4s/8AP7/9jOCFnjifu/7/APjMMzLkjw/f0/d+/HjjO02scU9z/wDv+/h45SODj02+6f8Ar+fuwbYjg45js/x90cOTOYcHoZ28cu22WZ6cNszp7v8A1/v/ALrnwwZxt5yOX+H8/wCeMk/gO2eHTpW23/Zy5/jnEfOu2Onh/wBfd/r922YZ4pnHTP8Ar/r/ANv9n3MHh4/c4/n4f7Z/n/L+A+HLx26eH8fH/wDP4n8smTDxbHeA3ZWcFk9BDxNukPDaPblR9x4n78OeQbZ2z4dOjj+T2szHPT+X8c+GzDieDWdvd7v3/u2xHj/CmdD+H8P4bG0eOPu92cfdPdh8c+Lg4Y8P4eONv5ZHGNsJxxPdTjZjmjhtb7vDxAx4QhwQ8XaE8ZjZf38MU5/hs4cmNs5POTGP4/zrpt4eHgYeBtk2/fgmPFaOGc7fvPD+McH+xiPDpHwf4vuz4dNtnt6e526ZZ028MocnvAeN98KRyPpXpgjEX0KNYzT27TYxMVsEOTWfGNs+gg+DMrkIZ4dGslOpwYp0AxgIBwJjplzFrLnPn2hDBMTpnNPBaxlb5eQxz4FnMwHAb9Omdul3hlmOmHZYRDgQ8QIQh2i1imzHvAdgp94iKRTtaIavaFk2FgPIUF6UByMTMMYCC9zYBA8O1CZjlrC8Ui4cQZmHIekxlWGZk4tZoozDEPOsyYh0cRXjjaZDD4CYMcTaumV8cCROGMpM4rp0I9uxhmSYzMTDwRxiDMwm3dmO4V7wHUYfAxWKQ9JYXHuD0IhMeFPJhMZdvd4bYO1sTx+7x8fHo9yB0MeP3eOY8sJMP3fyx/JAzx6Y2JsGJifwTtyzaZxW2Onb0MBswc4xHgGTCzbMPFzni0dKMRNp044xPu2DxXGzniM93jkmc+HuSPHb3ePjnI522O0/l/PM2mc1keRnxcFNBnvAdx9LG5CnuY7n1AwSDxYoWYU9uUMxRCPEIwHFJyw5jAOkzHHLpOjiYyr0hx+46OSsYxjxxDiG0MbaEOBiYZgyTwzRwYRMuMVttjD5zaDlGeGbPDEJnAUpg5ZnSOCEzTxMxhtM7QGHHJCLQtHeA+a6o8lYwhM+nFMH1AZpzige1KIVle0oBmUzlOOVXpHDMJ6MOMZzs3eBFI7WGsdpZmY7JxJhK2Acna5iTYI5R4I+OYG2ZiZxxM+4JkJnD2kYxmXA9ucqRp0ODCMO8B5j3z+o9RCFZoXgQgsyzL2mdFrCnIg4B2hY5OG7M4XiOI04MzpMnAcnQ6ZWYxCHn6Q+7wtjGRzsnIYImaXgOFIZrxhni4yZwRPugvJaCZWZHjjMwVmFJDg5jGCiLyGMe8B2mlbMXuaWzZ5MG5lV5ExManoDBtC21HDFA0JTDh0mcvjl0HjmMy5JkWnhiGJhcZm2IY4YA6BiMMeExwYrOm2TYBj5zNZjlCAw864ow4ZjpjEOGRJtiMF7dohtvxwCMazjoYCHBjjOzkjTxcbOJ4Y2Ns47wHWUoD3yEKfQkbMfewTFYoh3PuhhYgvJIrM1leJDKOMApjiUdMptMU4OOJhgzLbPaYnSgaxjiQz7srTWKORiywp4rAnT3Zjh6Z4mDODxx7o42eWYZxjFdBwncVnbPimKxw2MDHOFMgcDB4YmIYzlwHeA6T+BsxueosRPQTBZPecjEWnkTG2FIQI9uILEjZ4BmYGgIYOGYMxAoAe7o5Chp7SZFacmeGKZnIaJwKYvRi9Ix4hCbUZhlOJCnamdMdOGYkJk6Ux4oMAxFs9pmeGfGJ6AsQh3gN+ep/4r6lhTTF7hWHqI1ijQ4mRmIMGHLKNZiCJxLZjMOZkDgECZpmYQ4mZk2aKQ5BiKWF4sVWKIPEiTEArKHd0XGc2ynEttRtB7cWEgzL2uTPQrIYI9wQoe8Bt2l/pYepuMXubJilTtSLPDp0vjk2YZQB7lacjDtcOSZzWY93jjNuiTFng5NkHoQ2XixwFiZGPETFJYyY7S+cBmGOWZjJszDF7XTLDK8Xo7GbGNujyVdmEdmPIpDNhh3gO2HvFimnubAB6ggBDHc2XCRLPJIqwIPHMIZxGGKOLRMYjTEOGYZIETouTgoszi2WdOQmZ0ZgIQ4GKxYIZY8AjMbQSEYecgdMNBkY8R26DR0u8AWYOlExTwACFEKe5iU0d4D6Fz32FOj6Qohin0g0zHpIjdR5EVYRfQsc2Id2SlYkYciMxRZg8li0ws8mm7WYHaKJGEXi0AtZ9DHMLEPe2azZp4FdIQKDtSEaIOYHpe8B23QfURogw+ByffLvIiiFLA4vjFhlMbEOJRiiizxzhFsJQ8XN11OBCEXArTyQrFlaPPh3DBjxEpYEKOJhAITBHlmmgpmTimNo0QI9pixCw94KjHOOKI7w/GeYsdZ1v6S5uTznN4bPF/Q9ZzanqadDqLsf1p8D+p3PqYR/UeQsUxsQbLG5+ZdTRKbGjENX7TyvFLMNx9bqQuELMbq4hBh5D6XUgNGpqxo3G8Y+0KYmjTqNF2CMSEYXX2BoaNO7EbNG5iaY+xhGjR0IBAop85D2lnysabMIRs0WPxsI0kKLILZphowS7ZI/ifKpGEYxjubsLNB7WkNMZgw0KNBYRtm6aD+J1IdYWLF2zGiyPsGF2KQp0KKLFFPlfoVbFG9LllIatCxg2IewYrEp0KbJmsWLZpjBYwY/LhM5xq4osxuXaNGMaGNmy+sixobjTDVIpYepdU+ZYmjcomS7iGGF26aEY0D8ajdLlLRBY2NMRwwoLoJZ+PNFJjRHzNLcjBYFLC78aUtimGrSNLGLBixhGzRnU+EcatFyOhdWYFrEbEILHQ+MulFilgsNSiBWNEoi2WHrL4hGEaN6XYsYXXMYUqxCPxolEIpTCzYV0I0AxihopT8SQYwSNmksNC7mlabmgifEAtCqwh1DDGoZGEzCiIu5+JY02Y5wkLNEUbNELDlos/O0RYDZAWzQ0WIwiUphidR8wRir1saYRaXFFNhhGz8hCETtMwQjRmIkFoaNWHwsCIFKMaJmwRhHUQEGEXDRZ+NIxNChuXG2KaYLM5g6BQr/zIm5uhChY4pGwiwisIxbvrJjVgFhs0XVFoSBixRZhYX4WKw3DFYajYdxZLIw9jQxDqDcRGFkrBDNJZYw9Y4iNOhZogwhGxEY0QsxsXPjfKjWGDEEYLCYB0YtH0limxkzBShutkiWSICnsIXVKzDCRpKdxSQ0dH2Goohjpi46O5gQzQUpYsfORQRhjIxxDV0W4lwYkYfic4ialneWIMIUt2i58oTYEIUZuxsw3sJmgS6hZ+MIRmDNi2TKWIXLNyNs6gR+UoGACIQikYXW7GEabv1GGCGRRRaaNQpRhGMIQ1fWkYQraNOhYzDe6Fl/uG4RsMd6arSgx35x7SgoZkYpdbDZaGDvaLkfmWAWSmmxTCDSFxo600fWaFY1NxGxYH0PxgQhRZuUEDMIGYKwho9WI/MmNMZg0dxHcELNk+FjTCxGEIkCjUWFFi5ZPaUaGjDDuWnrbkYfaampQ+UfKd41k//6MAAwHvAplebsHN7O8BBSxd5vjRuObOw8r6j/I7CgObO07j1P5yH4DqfzljQu8TcUfpLlG4hzaGjrxo2D/NOosHW3Ow5txH9LoYj5DVdCBzZlpsHW2LH5iiLYuGN5cs9jofSnEo8ubn2NF3QgcCz5n6XVepepLO4sanz54Yuecoo0x9B1iw3NBd0bPkPaROsinY9bRZ/OPU2ezNPUNz1lzFOhZub13nkPYC6NL1tNmy0UaNP0BphjoGYaFJZV1Wz9juTqTQhhhbO4tlp/EoRaBxHVhEpjcsu5+1gnU7mMSFiML4u2fldCGjudCiG4IaFPtzdboJTDrWwaDnep8zxLFAXIsCGjSRbvyPW2NQsRdFsw1xTZfWxjqtgNSiimKQswoouUfEWItMbgR3FiZWmCwNWzD4zRCEfIwblxpKLsVhd9ebgUpZdUixbjCJFXQo+bEI0RswohG5Q2LNECBYuRPiLGiCwVbhS5zdgrGDTGz84QhGNgitLRAgrDRIaFNFj1sclL5G5RdbDSsI+QPiaUpim4pgUYKMRsUFFNDZ+NAsgaMKEoWFsgsDdlsxpPiaYxGg62Kt0xhApaYTOh6wgBCBizTotZbEAwwI2I0MaPWxgCBiiFzUmaVYRw0N2lLj8IqiLS0URbO8pI1mAGpZ+NRwxSnK5Zg63RphimzGnR+EdFQoohTSxhbGGYopiFmy/KhTFjBseVbDqFMd77FoGBAu6ptRMzAwdMaPzgEDcBTGmnqY2Cg6mB6yFysWGLY62DuYWzD2g0lygb5jZdFubwukfkDDneQxA3NG4KxuYQj8zHEb4I0deYFmZpLsNGsRT4lu7gwU9RQjTcs2LHsImgaFOYwsXYRYYQaUdx8hGELilZdzCiN2myc2t6zUszJ9pGwNHUeU1NxT8zvLNmnOp3A+3EAoL5h1qXaC5q06HxHkYF2LDeGG7oBow+QMvYR1GzSbmzqlPtd73Nz84vmKY02aTKYuLA+1wQWxh0DzMWJR1H1PadTT6TvHkc8jY7wF3fvPNtOf8+k5vbq83g5uxzZnnRn6x/oNz+ZseV6mxT+h4J52x1v2J/8AJ+gp5uLqfffzv9Rq/jP/AKNT2lHU/wBg/I9b6HV8q/MWPfaX/R/oNDzv2tmnR6yje0fjNHe7nm0F2mz5j/AblEHzFnQ4t36Hi6lOpvfpdWO95NFP5H8Lxfxv4BsMND6Te+YsOp5DRfkJinsdxqaOh1F38R1upRqUlNiFB7GncPWe8mh7Hqex7DyvkP1EfO00e05JodRHuObcRph5n4TyHYl3ROx0fW7yn0DTq9TYh8T9884Q3B7ES7vLvW8Esnrdwxpj3MSEIXPodGxCnkb2NOjR8zRZpw0XN7vLH6TceZ0SmkjZsfWakB8xZ9JT8JBouQdD8DGsfO7nc9pTxIPxI+dseRuUQ1fqYU9RqmhcORH5WzvDk9jcp/uaI7nc7jk6vxJ5s8BLnlPnPI6PFjRd0Te/I09iQ6nzv95ueJR5X+4osbnrbHvvztPF3H7CEKbvvP7Xec3U1E8r6T7Wz3Oh+tu3bqXd75j7XeeY7HU5tZ3jrWc485tBqc4Y5vrzfA3nWP6z1HE+06mn0O85wLzajU/a711zDef6PlCiPUPN6bD2P6SO9hRwPzPvHNudHm0nkd4WdWxuU+wC7wdXMdTrfylnRh6Gzofa9b5C7q3WFn5zgaHW3WjV9gXN7DVuG5j5H9DqeV7T2G53mho6Gj+jMe43B1u9+cosx8pR1FG4bLYPmWxRZYu4ojS9RYLFn+4jT2FmHUaD+IstCwDrNDVhvf72g8xTCyRpdx9bnQos6lyFmzTo/iYFjR1KIwsWYU2I6MNX1lNG4Opbhq6rCmmPyhuLtOgFFNEzoQbNl+csEXyELAEbrC6xmI6PrSO9oswuWKzAQgdYQsfOQ62ncU0tMWNLTDc/K3HLBu6kYUwCGi6kLsPhIwjTEKaLFNMYEWmOjYjR8ypGDCkfMtFnrYF31tLSRu8TqYWOx+JI0kYvYdjGAWKfoDUFpNWlTRdxuLHzowgkxFaN7QdbYG4UtB8jE3nY2BbkLujoPym5gUUR6wpaaIligu0esWjVYcACzDqLlH/NohoaL2OjGFguxfYQtjcXC7AIWCMbJTQMNCPxpTYjCLG5Fo3sQj2B8yU0xo4rToaFyx7FjRci0Q7HsdT5iBYo0bHAN5o/ULGhp0Q3vWXYWPpYHWeUo8hoU0esCgjTTCnc3KbLiyDojD5liwunYaEOo1I/UbyncRgQ0VjuLvzEW5TTRGB5WxTR1lD8rYjGnqLNijsObsWdV+lYanUeV0NVbPtxEKLhZo0Eudp9hTchxyxsw3lK+shRuQYaOpTuXyv2GhweT9bR5EfK3f2LdF3itnzH2Lq718jF3nlO8iWn0HPaOf6dhzgHtP2HY8T/ACf+T1HNrKO8BfzQ5th8B9R1H9h7X+k5vj5zm+JuNT1nI5uz/Q9h9B1HU+d3O49qfgPS/jfUnkebe0bzqebSftecaavUeR8h8j/yeL7C72Hpep/OnB++fSc3NpphZoj5z9Z6hu3eo5s5zaH0mj6T6HU6z+1+J/4G51T6iD1lni2NTR/E3f7Xm7Gj9RTY9D2P5WzuafeR/wAWMdHU+Au/M0Pae+fS09ifhPyH/A3H1Oh771PW/UH9jdu+0dX+0/Q8i5yfjHeXLnpOcK3PrOb8ec981KfqeL8D+Z9JxfY+p3voPaf2P7W75nn/AJ3jvIm49J/gHOHfI82x1bsbsets737F0PUWND8hqUncFiwUfkL4xGNFi5chRcdx9Sqb2nXN16lY/kVyb2s65u72LF/MvU7yjc6EE/MsLm4MNEwQ0wBRi7D6XVsQHVhYsO5sap7XVps6tnRjZjF6n8i2zQ0w3ro/nS5ApMDBuExYIDAiaN19pCFGAYwgaG1YLANYhcoh9rdjCyRrOjRqtP2Or1LoqLHNOqx+1svkYXaKG4w0fyMwxjAhMYiTG1MSbVgtiExR9YMIK3YFsRHcRoiQhCBYD5SkurSlZojTZuxIMbP1sbMVY5tmjOjFbsYxjHLT9SiMacwjQxgt3VY3WMPxAmLmJsQopDATaExZjlhAuUfKQSEMDqYiRmSAEEuQhjULsPmdGJHLF0ZlI6OpGL9TGMKY2crqqxWnUaaY2flI0sIosxGxG5ox0KbsfoI3HFBTFhAIm5aNG7SFn4mEITFOKIUFEMGjRHTMYUUQ2+R0aKYrFSmCRhYhCOjqfQxpjZhFswojdpu3d2fnZmzpmAqdbZppabLo6HwsadEykbAMaSg0IXUhc+duUiQWBAWNDQYgwDRZiYKSL8hqagARjqQ0KKRSmlafhaYOmWjNsw6m7GNPUmj8bYsrTM5Vg3dWLosaadF+JIG5sTotFje+RuujH1kIathuQobNEwTFy5DQsfCF2wsSEGmMLCTFFFExCEEmITHrNCihwhFc6ujq0dSDD2GpGlXMHQp3MbpZYfG08CKtNJS03ItmCxafifK6AFKtBo3I6Go/jKYjgukYdS0QhtQ0w+UGG4oEDAavWFFYuwM/K2HqGmGIRieQjdthp+MjZ4Kxu6sbLFitkgfKeRu2ViaoxhSRtmLBPkLu4bvUaJYjTYpikfpIFGHOKALlMcXNtT8RYhoFZGEDGIGxREKZgmKMaGL7fGajo2xFsUQcwhBrNi6B9D5GmKVmCTC1mDEhDRs0r8iXGmNMJg3EYxjEgxLMY0vxNzsFgAExRGZGOq0xjZ9bZibyzcgWYwLrcayWGEfhIQooKKAopjYmYQthEoutj5GBCmnEIMIhFgtsEQooGiFygx8aQjRYhDTMRWimmNGLH1sLsbLCysYUARpM6v1qWabOdWzSl2NMdGjc/GMLNiyEdBPIasIFOj8pqMIgMWwAMApQY7sD7GbWJiFFbQSLMBWLm2G5q7w+RAtnBje0hZp1ZgouXB9qGNGbF1mHGC4YKdwaGp3jimd4C/mpzjDvAXBPOc2djB995s71mjTzeB1Obc9T+xg+bFNBS6j/ANHJqXaaNGjrPrbEPSRjF8jz8S7uPrdU6khcmdWnyHztIx62yxYQ0LGj+c4rCii40Q5uhcdT7Xi8HRsfjLsWw6FmF2Kbghc+h7nRiMKCK7nQ/OeRLsDcReo/KeYg9ZY/Kli75GxHykaPodx5CxYdHeRjofSw3GY6sOtaI2KFufiC75kLtzylj6T32ECFmxYo9Zd6ijqLOpZCnRufIwNBPKkV0IRhYswSNj4hhEoh1NmC6jDQpWF1PWxhRCiiHWwFsuqRopbHsNEdSzTGl3kNxYs/OF2L1ZpjoXLFBqDT7CFy5cs0+YsaFj1sWgKKNGzTcixsnUlin6Cyx7CNF3RgEdx7GFyzdpLnvn0O8Es6FF3zmhT8rF1NG7Cg0IWKYx8gfIdS2eoo0eoirqe0jTqUjYhobmLFuWLP2GrvA0Bp3hCNn4zqWnU7GgWNzUp/Keps6v7G7Cgoudz9TZPKG9s0U0QbHyh5hTUo61sTMLHyMKexp1dCy6INsb35ngeV3MKbGp7XyEexjZo0ad78xHqCNPY9bq2fxoFnemg2Yw3Fn5zUCNmnetMbESYd6QV+ZjuQjdNQ8oaDZ/uNCxCO8pudgUPtKIRXUiaDZuUPUUPsbCxXsYnViz9bd3kaTqblmw+d+M3LQQps2dDeuh+R1I6Nzcxu0kexp+QuNGjuOwadC5D6khud7wbn2MYkKY0v3z6gKRIXTQjRc3n+I0f1L5n4mNmjV0GENCjifQlNMXeR4h+lp0e0aN7qd474veAsoeQ7Xm9vN9f+j/W2ftdxTHzti5+YAubjQxc6wo/OdR5HQ6nnuO595sb36QsWYtG43HU7z9B6j/JpWHW73VHm3h95/wATRpp87o/aaK9Qec/KdTqdTSaG4/QXNy7mm7vPynnKPI/4NDGmN3c9jcbP+ZveosWPtNxCg1dA6zefa07yOrdp8j+R7CjsLtOh9jZo1Kd5o2dXsf8AQsfsNFsEbHI8p9buYf8Au82p/wAnk/ff8CDzemMTtDcaH2kO14tjm2NjVsc9h1P7D6yyfAf3v/saH7SHeAsz2H/wR+t0KPgbBF+I7m7q+pSH7HnHO4/Ubn+g5vxDm3ljcc28dDV/avI8rzcTn0m47DeH6mNFG4uWOsoD5X76bixc+lo5NMXzGg+1/oNHqex/zeL+Z9DwLur8x5SnQuxeb4G8/WcCG4u2NV/G9S7mKkXyG4+gs85I5uxotPNtNx3kMqc3J4HeAvZ3gIed4CyEfIc/17wF7Ot5tz3N3n8POGObad4E9Hlf1HWf8TvAdA+A5szY7wEhe8BiDvAkF7wE1f0H9Z3gSo86I5xb3gOQ8385yzzZzvHHz/+h");

                //System.IO.FileStream fs = new FileStream(@"C:\tmp\0A\01.wsq", FileMode.Open);
                //byWSQ = new byte[fs.Length];
                //fs.Read(byWSQ, 0, (int)fs.Length);


                Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                bool b = dec.DecodeMemory(byWSQ, out w, out h, out byRAW);
                pictureBox2.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(byRAW, w, h);
                
                byte[] byHeader = CreateHeader();

                DMANECTransactionManagerWS.TransactionOut txout;

                DMANECTransactionManagerWS.TransactionIn txin = new DMANECTransactionManagerWS.TransactionIn();
                txin.header = byHeader;
                txin.biometricData = byWSQ; // byWSQ;
                txin.transactionType = "FEW1";
                //txin.transactionType = "FER1";
                txin.workstations = 0;

                using (DMANECTransactionManagerWS.TransactionManagerWSService target = new DMANECTransactionManagerWS.TransactionManagerWSService())
                {
                    txout = target.generateTransaction(txin);
                }


                byte[] byMinEx = txout.minutia;

                txin.header = CreateHeader();
                txin.transactionType = "VER11";
                txin.biometricData = byMinutiae;
                //txin.compareTo = byMinEx;
                txin.compareTo = byMinEx;
                using (DMANECTransactionManagerWS.TransactionManagerWSService target = new DMANECTransactionManagerWS.TransactionManagerWSService())
                {
                    txout = target.generateTransaction(txin);
                }



                int i = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            byte[] src = new byte[10];
            src[0] = 1;
            src[1] = 1;
            src[2] = 1;

            byte[] dest = new byte[3];
            Buffer.BlockCopy(src, 0, dest, 0, 3);


            string rut = "21284415-2";
            string rutmasdv = rut.Substring(0, rut.IndexOf("-")) + rut.Substring(rut.IndexOf("-") + 1);

            rut = rutmasdv;
            rutmasdv = rut.Substring(0, rut.Length - 1) + "-" + rut.Substring(rut.Length - 1, 1);

            //  Date        Numeric(8)      MMDDYYYY
            //  Hour        Numeric(8)      HHMMSSCC

            string sdate = DateTime.Now.ToString("MMddyyyy");
            string shour = DateTime.Now.ToString("HHmmss00");

            //this.pictureBox1.Image = Image.FromFile(@"C:\tmp\A\bitmapfinger_1_GGS.bmp");

            //Bitmap resized = new Bitmap(this.pictureBox1.Image, new Size(this.pictureBox1.Image.Width / 4, this.pictureBox1.Image.Height / 4));
            //resized.Save(@"C:\tmp\A\bitmapfinger_1_GGS_cuarto.bmp");

            //this.pictureBox2.Image = resized;


            string s;
            System.IO.StreamReader sr = new System.IO.StreamReader(@"c:\biometrika\raw300x400_2.txt");
            s = sr.ReadToEnd();
            sr.Close();

            byte[] by = Convert.FromBase64String(s);


            byte[] by512 = Bio.Core.Imaging.ImageProcessor.FillRaw(by, 300, 400, 512, 512);
            this.pictureBox1.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(by512, 512, 512);


            //int i = 0;

        }



    }
}
