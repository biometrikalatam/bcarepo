﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Helpers;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.UserSkins;
using DevExpress.XtraCharts;
using System.Threading;


namespace Bio.Portal.Diagnostic
{
    public partial class frmMain : RibbonForm
    {
        public frmMain()
        {
            InitializeComponent();
            InitSkinGallery();
            InitChartControl();

        }
        void InitSkinGallery()
        {
            SkinHelper.InitSkinGallery(rgbiSkins, true);
        }
        void InitChartControl()
        {
            //chartControl.AppearanceName = "Light";
            //chartControl.PaletteName = "Office";
            //ChangeSeries(ViewType.Bar);
            //chartControl.Legend.Visible = false;
            //chartControl.Series[0].LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
        }
        void ChangeSeries(ViewType viewType)
        {
            //double[] values = { 3.1, 2.3, 3.2, 3.9, 5.1 };
            //Series series = new Series("Series", viewType);
            //for (int i = 0; i < values.Length; i++)
            //    series.Points.Add(new SeriesPoint((i + 1) * 10, new double[] { values[i], values[i] + 3, values[i] + 2, values[i] + 1 }));
            //chartControl.Series.Clear();
            //chartControl.Series.Add(series);
        }

        private void iAbout_ItemClick(object sender, ItemClickEventArgs e)
        {
            //About ab = new About();
            //ab.ShowMode = DevExpress.XtraSplashScreen.ShowMode.Image;
            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(About), true, true);
            Thread.Sleep(3000);
            DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            
        }

    }
}