﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bio.Core.Matcher.Connector.Namku
{
    public partial class TokenNamku2
    {
        [JsonProperty("api_version")]
        public string ApiVersion { get; set; }

        [JsonProperty("error")]
        public TokenNamkuError Error { get; set; }
    }
}
