﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bio.Core.Matcher.ISO_19794_2_2005
{
    internal class DPUtils
    {
        internal static int MT_DPENROLL = 1;
        internal static int MT_DPVERIFY = 2;
        internal static int MT_ANSI = 3;
        internal static int MT_ISO = 4;
        
        internal static string GeneraXML(string b64Template, int mt)
        {
            string xml = null;
            switch (mt)
            {
                case 1: //DP Enroll
                    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                          "<Fid>" +
                          "<Bytes>" + b64Template + "</Bytes>" +
                          "<Format>1</Format>" +
                          "<Version>1.0.0</Version>" +
                          "</Fid>";
                    break;
                case 2: //DP Verify
                    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                          "<Fid>" +
                          "<Bytes>" + b64Template + "</Bytes>" +
                          "<Format>2</Format>" +
                          "<Version>1.0.0</Version>" +
                          "</Fid>";
                    break;
                case 3: //ANSI
                    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                          "<Fid>" +
                          "<Bytes>" + b64Template + "</Bytes>" +
                          "<Format>1769473</Format>" +
                          "<Version>1.0.0</Version>" +
                          "</Fid>";
                    break;
                case 4: //ISO
                    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                          "<Fid>" +
                          "<Bytes>" + b64Template + "</Bytes>" +
                          "<Format>16842753</Format>" +
                          "<Version>1.0.0</Version>" +
                          "</Fid>";
                    break;

                default:
                    break;
            }
            return xml;
        }
    }
}
