﻿
using System;
using Bio.Core.Matcher.Interface;
using log4net;

namespace Bio.Core.Matcher.ISO_19794_2_2005
{
    [Serializable]
    public class Template : ITemplate
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Template));

        //Added 01-11-2013 - Porque vienen ambos estándares de minucias 
        //                   ANSI INSITS 378-2004.   	Fingerprint Minutiae Data (FMD)
        //                   ISO/IEC 19794-2:2005.   	Fingerprint Minutiae Data (FMD)
        private byte[] _datatANSI378;
        public byte[] DataANSI378
        {
            get { return _datatANSI378; }
            set { _datatANSI378 = value; }
        }
        private byte[] _datatISO19794_2;
        public byte[] DataISO19794_2
        {
            get { return _datatISO19794_2; }
            set { _datatISO19794_2 = value; }
        }
        //Added 07-07-2010 - POrque vienen ambos templates de Verify y Enroll en ese orden
        private byte[] _datatoverify;
        public byte[] DataToVerify
        {
            get { return _datatoverify; }
            set { _datatoverify = value; }
        }
        private byte[] _datatoenroll;
        public byte[] DataToEnroll
        {
            get { return _datatoenroll; }
            set { _datatoenroll = value; }
        }

#region Implementation of ITemplate

        private int _authenticationFactor;

        private int _minutiaeType;

        private int _bodyPart;

        private int _type = Bio.Core.Matcher.Constant.BirType.PROCESSED_DATA;

        private byte[] _data;

        private string _additionalData;


        /// <summary>
        /// Tecnologia a la que corresponde el template
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia que contiene Data, incluidos WSQ, RAW, etc.
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Tipo de informacion (data) que contendra el template (desde BIR_TYPE)
        /// </summary>
        public int Type
        {
            get { return _type; }
            set { _type = value; }
        }

        /// <summary>
        /// Parte del cuerpo si es tecnologia biometrica
        /// </summary>
        public int BodyPart
        {
            get { return _bodyPart; }
            set { _bodyPart = value; }
        }

        /// <summary>
        /// Data propia del template o sample
        /// </summary>
        public byte[] Data
        {
            get { return _data; }
            set { _data = value; }
        }

        /// <summary>
        /// Datos adicionales segun sea la data. Por ejemplo si es WSQ, 
        /// se puede colocar las dimensiones de la forma w=512|h=512, o si
        /// es template NEC, el coeficiente coef=xxx. Si hay mas de uno separado 
        /// por pipe "|"
        /// </summary>
        public string AdditionalData
        {
            get { return _additionalData; }
            set { _additionalData = value; }
        }

        /// <summary>
        /// Obtiene la Data en base 64
        /// </summary>
        public string GetData
        {
            get { return (_data != null && _data.Length > 0) ? Convert.ToBase64String(_data) : null; }
        }

        /// <summary>
        /// Setea la data desde base 64
        /// </summary>
        public string SetData
        {
            set
            {
               
                //Modify 01-11-2013 - Added ISO y ANSI estandares
                //Added 07-07-2010 - Como vienen verify-septml-enroll debo sacar cada uno
                try
                {
                    string[] sep = new string[1] { "-septmpl-" };
                    string[] sa = value.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    _datatoverify = Convert.FromBase64String(sa[0]);
                    _data = _datatoverify;
                    if (sa.Length > 1) _datatoenroll = Convert.FromBase64String(sa[1]);
                    if (sa.Length > 2) _datatANSI378 = Convert.FromBase64String(sa[2]); 
                    if (sa.Length > 3) _datatISO19794_2 = Convert.FromBase64String(sa[3]);

                }
                catch (Exception ex)
                {
                    LOG.Error("ISO_19794_2_2005.Template SetError Split.-septmpl-", ex);
                    _data = null;
                }
                
            }
        }

#endregion Implementation of ITemplate

        public void Dispose()
        {
            
        }
    }
}
