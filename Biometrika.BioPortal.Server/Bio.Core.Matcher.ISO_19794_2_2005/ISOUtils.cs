﻿
   using System;
   using System.Collections.Generic;
using System.IO;
//#if !N_PRODUCT_HAS_NO_LICENSES
   using log4net;
//#endif

namespace Bio.Core.Matcher.ISO_19794_2_2005
{
	public static class ISOUtils
	{
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ISOUtils));

        public static bool IsInitialized;

        public static bool _GeneraconISO = false;
        public static bool _GeneraconWSQ = false;
        public static bool _GeneraconRAW = false;
        //public static bool _InnovEnabled = false;

        /// <summary>
        /// Inicializa la licencia la primera vez que se usa una clase de este Namespace.
        /// </summary>
        public static bool Initialize(string parameters)
        {
            
            try
            {

                if (parameters == null)
                {
                    LOG.Error("Bio.Core.Matcher.ISO_19794_2_2005.Initialize Error - Parameters Matcher = NULL");
                    return false;
                }

                LOG.Debug("Bio.Core.Matcher.ISO_19794_2_2005.Initialize - ExtractPath(parameters) de parameters=" + parameters);
                IsInitialized = ExtractParams(parameters, out _GeneraconISO, out _GeneraconWSQ, out _GeneraconRAW); //, out _InnovEnabled);

                if (!IsInitialized)
                {
                    LOG.Error("Bio.Core.Matcher.ISO_19794_2_2005.Initialize Error - Component License Cfg = NULL");
                    return false;
                } 

                LOG.Debug("Bio.Core.Matcher.ISO_19794_2_2005.Initialize - IsInitialized=" + IsInitialized);

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.ISO_19794_2_2005.Initializee Error", ex);
            }
            return IsInitialized;
            
        }

        //Extrae el pathj de NLicense.cfg de los parametros
        // Formato: PathLicenseCfg=C:\\NLicense.cfg
	    private static string ExtractPath(string parameters)
	    {
	        string path = null;
	        try
	        {
	            string[] arr = parameters.Split(new string[] {"|"}, StringSplitOptions.RemoveEmptyEntries);
                if (arr.Length == 0) return null;

                for (int i = 0; i < arr.Length; i++)
	            {
                    string[] item = parameters.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    if (item[0].Equals("ComponentLicenseCfg") || item[0].Equals("ConfigLicense"))
                    {
                        path = item[1];
                        break;
                    }
	            }
	            
	        } catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Verifinger6_4.VfeUtils.ExtractPath Error", ex);
            }
	        return path;
	    }


        //Extrae el pathj de NLicense.cfg de los parametros
        // Formato: Server=/local|Port=5000|ComponentLicenseCfg=Biometrics.FingerExtraction,Biometrics.FingerMatching
        private static bool ExtractParams(string parameters, out bool generaconISO, out bool generaconWSQ, 
                                          out bool generaconRAW) //, out bool innovEnabled)
        {
            generaconISO = false;
            generaconWSQ = false;
            generaconRAW = false;
            //innovEnabled = false;
            try
            {
               
                string[] arr = parameters.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                LOG.Debug("ExtractParams arr.length=" + arr.Length.ToString());
                if (arr.Length == 0) return false;
                 
                for (int i = 0; i < arr.Length; i++)
                {
                    LOG.Debug("ExtractParams procesando arr[" + i.ToString() + "]=" + arr[i]);
                    string[] item = arr[i].Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    LOG.Debug("ExtractParams item.length=" + item.Length.ToString());
                    if (item[0].Equals("ISO"))
                    {
                        generaconISO = Convert.ToBoolean(item[1]);
                        LOG.Debug("ExtractParams generaconISO=" + generaconISO);
                    }
                    if (item[0].Equals("WSQ"))
                    {
                        generaconWSQ = Convert.ToBoolean(item[1]);
                        LOG.Debug("ExtractParams generaconWSQ=" + generaconWSQ);
                    }
                    if (item[0].Equals("RAW"))
                    {
                        generaconRAW = Convert.ToBoolean(item[1]);
                        LOG.Debug("ExtractParams generaconRAW=" + generaconRAW);
                    }
                    //if (item[i].Equals("InnovEnabled"))
                    //{
                    //    innovEnabled = Convert.ToBoolean(item[1]);
                    //    LOG.Debug("ExtractParams InnovEnabled=" + innovEnabled);
                    //}
                }

                //if (generaconWSQ || generaconRAW)
                //{
                //    try
                //    {
                //        Innovatrics.AnsiIso.IEngine.Init();
                //    }
                //    catch (Exception exep)
                //    {
                //        LOG.Debug("Innovatrics.AnsiIso.IEngine.Init Error [" + exep.Message + "]");
                //    }
                //}

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.ISO_19794_2_2005.ExtractParams Error", ex);
                return false;
            }
            return true;
        }


	   // Load configuration file 
		public static Dictionary<string, string> LoadConfigurations(string filename)
		{
			Dictionary<string, string> config = new Dictionary<string, string>();

			string[] lines = File.ReadAllLines(filename);

			foreach (string line in lines)
			{
				if (line.TrimStart().StartsWith("#") || line.Trim() == string.Empty)
				{
					//ignore comment or empty line
					continue;
				}

				string[] values = line.Split('=');
				string value = (values.Length > 1) ? values[1] : "";
				if (values.Length > 0)
				{
					// values[0] is the key
					config.Add(values[0].Trim(), value.Trim());
				}
			}

			return config;
		}

		// Load licenses configuration file with names of licenses to obtain
		private static void LoadLicenseConfiguration(string pathlicensecfg)
		{
			//string path = Path.Combine(GetAssemblyPath(), LicensesConfiguration);

            //_licenseCfg = LoadConfigurations(pathlicensecfg);
		}

	

        public static string GetUserLocalDataDir(string productName)
        {
            string localDataDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            localDataDir = Path.Combine(localDataDir, "Neurotechnology");
            if (!Directory.Exists(localDataDir))
            {
                Directory.CreateDirectory(localDataDir);
            }
            localDataDir = Path.Combine(localDataDir, productName);
            if (!Directory.Exists(localDataDir))
            {
                Directory.CreateDirectory(localDataDir);
            }

            return localDataDir;
        }
	}
}
