﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bio.Core.Api;
using Bio.Core.Constant;
using log4net;

namespace Bio.Core.TokenCL050101
{

    public class TokenCL050101 : Bio.Core.Matcher.Token.IToken
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(TokenCL050101));


        #region Private Properties Implementation of IToken

        private int _operationType;

        private int _authenticationFactor;

        private int _minutiaeType;

        private int _datatypetoken;

        private int _bodyPart;

        private List<Sample> _samples;

        private string _additionalData;

        private string _getData;

        private string _setData;

        private string _typeid;

        private string _valueid;

        private string _clientid;

        private string _ipenduser;

        private string _enduser;

        #endregion Private Properties Implementation of IToken

        #region Public Properties Implementation of IToken

        /// <summary>
        /// 1-Verify | 2-Enroll
        /// </summary>
        public int OperationType
        {
            get { return _operationType; }
            set { _operationType = value; }
        }

        /// <summary>
        /// Tecnologia a la que corresponde el template
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia que contiene Data, incluidos WSQ, RAW, etc.
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        ///// <summary>
        ///// Tipo de informacion (data) que contendra el template (desde BIR_TYPE)
        ///// </summary>
        //public int Type
        //{
        //    get { return _type; }
        //    set { _type = value; }
        //}

        /// <summary>
        /// Parte del cuerpo si es tecnologia biometrica
        /// </summary>
        public int BodyPart
        {
            get { return _bodyPart; }
            set { _bodyPart = value; }
        }

        /// <summary>
        /// [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
        /// </summary>
        public int DataTypeToken
        {
            get { return _datatypetoken; }
            set { _datatypetoken = value; }
        }

        /// <summary>
        /// Data propia del template o sample
        /// </summary>
        public List<Sample> Samples
        {
            get { return _samples; }
            set { _samples = value; }
        }

        /// <summary>
        /// Datos adicionales segun sea la data. Por ejemplo si es WSQ, 
        /// se puede colocar las dimensiones de la forma w=512|h=512, o si
        /// es template NEC, el coeficiente coef=xxx. Si hay mas de uno separado 
        /// por pipe "|"
        /// </summary>
        public string AdditionalData
        {
            get { return _additionalData; }
            set { _additionalData = value; }
        }

        /// <summary>
        /// Obtiene la Data en base 64
        /// </summary>
        public string GetData
        {
            get { return _getData; }
        }

        /// <summary>
        /// Setea la data desde base 64
        /// </summary>
        public string SetData
        {
            set { _setData = value; }
        }

        /// Tipo ID de la persona (RUT, DNI, etc) 
        /// </summary>
        public string Typeid
        {
            get { return _typeid; }
            set { _typeid = value; }
        }

        /// Valor ID de la persona (RUT, DNI, etc) 
        /// </summary>
        public string Valueid
        {
            get { return _valueid; }
            set { _valueid = value; }
        }

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist 
        /// </summary>
        public string Clientid
        {
            get { return _clientid; }
            set { _clientid = value; }
        }

        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        public string Ipenduser
        {
            get { return _ipenduser; }
            set { _ipenduser = value; }
        }

        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        public string Enduser
        {
            get { return _enduser; }
            set { _enduser = value; }
        }

        #endregion Public Properties Implementation of IToken

        #region Public Method

        /// <summary>
        /// Constructor vacio
        /// </summary>
        public TokenCL050101() { }

        /// <summary>
        /// Constructor que recibe token y extrae
        /// </summary>
        /// <param name="token"></param>
        public TokenCL050101(string token)
        {
            Extract(token);
        }


        /// <summary>
        /// Extrae el contenido del token y llena las propiedades del objeto
        /// </summary>
        /// <param name="token">Token a extraer</param>
        /// <returns></returns>
        public int Extract(string token)
        {
            int ret = Errors.IERR_OK;
            try
            {
                LOG.Debug("TokenCL050101.Extract - Token = " + token);
                //      CL050101 - Cliente liviano especial para Movil por PERU 08-07
                if (token == null ||
                    ((!token.Trim().Substring(0, 8).Equals("CL050101"))))
                    return Errors.IERR_BAD_PARAMETER;

                //Saco encabezado
                string auxtoken = token.Trim().Substring(8);
                LOG.Debug("TokenCL050101.Extract - auxtoken = " + auxtoken);
                //Desencripto la data que viene en Hexa
                //BKclassCryptoClass oCrypt = new BKclassCryptoClass();
                string dataDecrypted = null;
                try 
                {
                    //dataDecrypted = oCrypt.Decrypt(auxtoken, "BioPortalClientAx14042010", true,
                    //                                      frezCryptoEncryptionType.frezBlockEncryption);
                    dataDecrypted = auxtoken; // Crypto.DecryptWithSymmetricAid(Crypto.PRIVATE_KEY, auxtoken);
                    LOG.Debug("TokenCL050101.Extract - Token decrypted = " + dataDecrypted);
                    if (dataDecrypted == null) return Errors.IERR_INVALID_TOKEN;
                }
                catch (Exception exdec)
                {
                    LOG.Error("TokenCL050101.Extract - Desencriptando - ", exdec);
                    return Errors.IERR_INVALID_TOKEN;
                }

                //Separo segun patron de separacion
                dataDecrypted = dataDecrypted.Replace("&#x0;", "");
                string[] aData = dataDecrypted.Split('|');
                LOG.Debug("TokenCL050101 - Len(aData)=" + aData.Length);
                //Lleno propiedades desde token
                /*
                  De estos originales:
                   Pos > 0 - Tipo_Doc
                   Pos > 1 - Nro_Doc
                   Pos > 2 - Si es DP Serial Number
                   Pos > 3 - [1-10] Dedo  = BodyPart
                   Pos > 4 - WSQ en B64 si sContenido = 0,1
                   Pos > 5 - Template en B64 si sContenido = 0,2 (NEC o DP)
                   Pos > 6 - Timestamp Cliente [ddmmyyyyhhmmss]
                   Pos > 7 - IP cliente
                   Pos > 8 - [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
                   Pos > 9 - [1-NEC | 2-DP] Formato del Template
                   Pos > 10 - [UNKNOW-0|DP-1|SEC-2|BIOI-3|CROSSMATCH-4] Dispositivo
                   Pos > 11 - AutenticationFactor
                   Pos > 12 - OperationType
                 En version Cliente Liviano especial viene:
                   Pos > 0 - Nro_Doc
                   Pos > 1 - Serial Number Lector
                   Pos > 2 - Template en B64 ANSI Solo                 
                 */
                _operationType = 1; //Convert.ToInt32(aData[12]);
                _authenticationFactor = 2; //Convert.ToInt32(aData[11]);
                _minutiaeType = 13; //Convert.ToInt32(aData[9]);
                _bodyPart = 2; //Convert.ToInt32(aData[3]);
                _samples = new List<Sample>();
                _datatypetoken = 2; //Convert.ToInt32(aData[8]);

                //Solo viene Template ANSI
                Sample sTmpl = null;
                sTmpl = new Sample
                {
                    Data = aData[2],
                    Additionaldata = "", //aData[10],
                    Device = 2, //Convert.ToInt32(aData[11]),
                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                };
                _samples.Add(sTmpl);
                _typeid = "DNI";
                _valueid = aData[0];
                _clientid = aData[1].Trim();
                _ipenduser = "127.0.0.1"; //aData[7].Trim();
                _enduser = "ClienteLivianoForP"; //aData[7].Trim();
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("TokenCL050101.Extarct", ex);
            }
            LOG.Debug("TokenCL050101.Extract - Out!");
            return ret;
        }

 

        /// <summary>
        /// Extrae el contenido del token y llena las propiedades del objeto y ademas de llenar las prop del bjeto, 
        /// setea las variable que corresponden a un objeto XmlParamIn
        /// </summary>
        /// <param name="token">token en base 64</param>
        /// <param name="newparamin">Objeto XmlParamIn con datos extraidos desde token</param>
        /// <returns></returns>
        public int ExtractToParameters(string token, out string newparamin)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {

        }

        #endregion Public Method

    }
}

