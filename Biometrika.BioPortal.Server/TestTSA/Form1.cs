﻿using Org.BouncyCastle.Math;
using Org.BouncyCastle.Tsp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace TestTSA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {


                //1.- Tomo el string de entrada, lo paso a byte[]
                //2.- Saco el hash con SHA1
                //3.- Me conecto al URLGeneration y genero TS
                //4.- Genero el XML de salida

                //1.- Tomo el string de entrada, lo paso a byte[]
                string byteDocB64 = "... <COLOCAR un doc en Base64> ...";
                byteDocB64 = Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes("test de TSA safestamper"));
                byte[] byDocToTS = Convert.FromBase64String(byteDocB64);

                //2.- Saco el hash con SHA1
                SHA1 sha1 = SHA1CryptoServiceProvider.Create();
                byte[] hash = sha1.ComputeHash(byDocToTS);                    // generar un hash del documento
                string sHash = BitConverter.ToString(hash).Replace("-", "");
                
                //3.- Me conecto al URLGeneration y genero TS
                TimeStampRequestGenerator reqGen = new TimeStampRequestGenerator();
                reqGen.SetCertReq(true);   // ??
                TimeStampRequest request = reqGen.Generate(TspAlgorithms.Sha1, hash, BigInteger.ValueOf(100));
                byte[] reqData = request.GetEncoded();
                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(txtUrl.Text.Trim());
                httpReq.Method = "POST";
                httpReq.ContentType = "application/timestamp-query";
                string user = txtUser.Text.Trim(); // "gsuhit@biometrika.cl";
                string pass = txtPass.Text.Trim(); // "B1ometrika";
                httpReq.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(user + ":" + pass)));
                httpReq.ContentLength = reqData.Length;

                // Write the request content
                Stream reqStream = httpReq.GetRequestStream();
                reqStream.Write(reqData, 0, reqData.Length);
                reqStream.Close();

                HttpWebResponse httpResp = (HttpWebResponse)httpReq.GetResponse();

                // Read the response
                Stream respStream = new BufferedStream(httpResp.GetResponseStream());
                TimeStampResponse response = new TimeStampResponse(respStream);
                respStream.Close();

                TimeStampToken t = response.TimeStampToken;

                if (t == null)
                {
                    richTextBox1.Text = "TimeStampToken == null";
                }
                else
                {
                    //Armo XML de Salida
                    richTextBox1.Text =  "HASH = " + sHash + Environment.NewLine +
                        "<TimeStamping>" +
                                    "<Policy>" + t.TimeStampInfo.Policy.ToString() + "</Policy>" +
                                    "<DateTimeSign>" + t.TimeStampInfo.GenTime + "</DateTimeSign>" +
                                    "<SerialNumber>" + t.TimeStampInfo.TstInfo.SerialNumber.ToString() + "</SerialNumber>" +
                                    "<TSA>" + (t.TimeStampInfo.Tsa!=null?t.TimeStampInfo.Tsa.Name.ToString():"Tsa=null") + "</TSA>" +
                                    "<URLGenerator>" + txtUrl.Text.Trim() + "</URLGenerator>" +
                                    "<URLVerify>" + "https://www.safestamper.com/tsa/stamps?hash=" + "</URLVerify>" +
                            "</TimeStamping>";
                }
                
            }
            catch (Exception ex)
            {
                richTextBox1.Text = "Bio.Core.TSA.TSAGenerator.GeneraTS Error = " + ex.Message;
            }
           
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                txtUrl.Text = "https://safestamper.com/tsa";
                txtUser.Text = "gsuhit@biometrika.cl";
                txtPass.Text = "B1ometrika";

            } else
            {
                txtUrl.Text = "https://timestamp.esign-la.com:8443/signserver/tsa?workerName=TimeStampSignerBiometrika";
                txtUser.Text = "Biometrika";
                txtPass.Text = "UJ?iy45TyJ099?45";
            }
        }
    }
}
