﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018")]
[assembly: AssemblyDescription("Conector a SRCeI 2018 - Validacion Mutua")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Biometrika")]
[assembly: AssemblyProduct("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("Biometrika")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ad2b7229-6cd4-4007-9eeb-baa65f5d8cb1")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.1.0.0")]
[assembly: AssemblyFileVersion("2.0.0.0")]
