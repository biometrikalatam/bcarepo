﻿using System;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.ServiceModel.Security.Tokens;
using System.Text;
using Bio.Core.Api;
using Bio.Core.Api.Matcher.Interface;
using Bio.Core.Constant;
using Bio.Core.Wsq.Encoder;
using BioPortal.Server.Api;
using log4net;
using System.IO;
using FreeImageAPI;
using System.Runtime.InteropServices;
using System.Globalization;
using Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.cl.registrocivil.ds64;
using Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.cl.registrocivil.pv;

namespace Bio.Core.Matcher.Connector.SRCeI.BioVerify2018
{
    public class Connector : IConnector
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Connector));

#region Private properties propietario
        RUN _currenRUN;
        bool _getInfo;
        bool _getFoto;
        bool _getFirma;

        private string _urlWSCOW = "https://terceros.sidiv.registrocivil.cl:8443/InteroperabilityPlateform/TercerosDS64ProxyService";
        private string _urlWSPV = "https://terceros.sidiv.registrocivil.cl:8443/InteroperabilityPlateform/TercerosDS64ProxyService";
        private string _urlWSDS64 = "https://terceros.sidiv.registrocivil.cl:8443/InteroperabilityPlateform/TercerosDS64ProxyService";
        private int _qReintentosVerify = 3;
        private int _timeout = 30000;
        static internal int _idEmpresaSRCeI = 61003000;
        private string _userSRCeI = "usr_sence"; //"usr_medleg";
        private string _claveSRCeI = "dE34frSE#SK2"; //"H%E1K!9&rC2U";
        static internal string _ip;
        private int _threshold = 3500;

        //0 - Todo 
        //1 - Solo Info
        //2 - Solo Foto
        //3 - Solo Firma
        //4 - Info + Foto
        //5 - Info + Firma
        //6 - Foto + Firma
        private int _typeget = 0;
        private int _wFoto = 140;
        private int _hFoto = 140;
        private int _wFirma = 200;
        private int _hFirma = 85;


        private bool _isConfigured; //Solo para control en initialization
        private string _pathCertifySRCeI;
        private string _pathCertifySignaturePFX;
        private string _pswCertifySignaturePFX;
        private int _clientType;  //1 - DS64 | 2 - PV

        //private byte[] _certifySRCeI;
        //private string _pathpvkey;
        //private string _pvkey;
        //private string _pathcertifcatekey;
        //private string _certifcatekey;

        static internal string COW_ENDPOINT_BASE_URI = "https://terceros.sidiv.registrocivil.cl:8443/InteroperabilityPlateform/TercerosDS64ProxyService";
        static internal string DS64_ENDPOINT_BASE_URI = "https://terceros.sidiv.registrocivil.cl:8443/InteroperabilityPlateform/TercerosDS64ProxyService";
        static internal string PV_ENDPOINT_BASE_URI = "https://terceros.sidiv.registrocivil.cl:8443/InteroperabilityPlateform/TercerosPVProxyService";
        static internal string ENDPOINT_CERTIFICATE = @"D:\Biometrika\Desarrollo\SRCeI_2015\2015\solucionNueva\certificados\RegistroCivil.cer";
        static internal string USERTOKEN_USERNAME = "usr_sence";
        static internal string USERTOKEN_PASSWORD = "dE34frSE#SK2";
        static internal string SIGNATURE_P12 = @"D:\Biometrika\Desarrollo\SRCeI_2015\2015\solucionNueva\certificados\SENCE_SIGN_283B2A1509EB08B7.p12";
        static internal string SIGNATURE_PASSWORD = "sence";
        static internal int CLIENT_TYPE = 1; //1 - DS64 | 2 - PV

        //Para armar el cliente en Initialize y de ahi no tener que levantar nada mas por cada Tx
        DS64Client DS64c;
        PVClient PVc;

        #endregion Private properties propietario

        #region Public properties propietario
        public int WFoto
        {
            get { return _wFoto; }
            set { _wFoto = value; }
        }
        public int HFoto
        {
            get { return _hFoto; }
            set { _hFoto = value; }
        }
        public int WFirma
        {
            get { return _wFirma; }
            set { _wFirma = value; }
        }
        public int HFirma
        {
            get { return _hFirma; }
            set { _hFirma = value; }
        }
        
        public string PathcertifySRCeI
        {
            get { return _pathCertifySRCeI; }
            set { _pathCertifySRCeI = value;
                  byte[] _certifySRCeI = File.ReadAllBytes(this._pathCertifySRCeI);
                  ENDPOINT_CERTIFICATE = _pathCertifySRCeI;
            }
        }

        public string PathCertifySignaturePFX
        {
            get { return _pathCertifySignaturePFX; }
            set
            {
                _pathCertifySignaturePFX = value;
                SIGNATURE_P12 = value;
            }
        }

        public string PswCertifySignaturePFX
        {
            get { return _pswCertifySignaturePFX; }
            set
            {
                _pswCertifySignaturePFX = value;
                SIGNATURE_PASSWORD = value;
            }
        }

        public string UrlWsCOW
        {
            get { return _urlWSCOW; }
            set { _urlWSCOW = value;
                COW_ENDPOINT_BASE_URI = value;
            }
        }

        public string UrlWsPV
        {
            get { return _urlWSPV; }
            set { _urlWSPV = value;
                PV_ENDPOINT_BASE_URI = value;
            }
        }

        public string UrlWsDS64
        {
            get { return _urlWSDS64; }
            set { _urlWSDS64 = value;
                DS64_ENDPOINT_BASE_URI = value;
            }
        }

        public int ClientType
        {
            get { return _clientType; }
            set
            {
                _clientType = value;
                CLIENT_TYPE = value;
            }
        }
        

        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        public int IdEmpresaSRCeI
        {
            get { return _idEmpresaSRCeI; }
            set { _idEmpresaSRCeI = value; }
        }

        public string UserSRCeI
        {
            get { return _userSRCeI; }
            set { _userSRCeI = value;
                USERTOKEN_USERNAME = value;
            }
        }

        public string ClaveSRCeI
        {
            get { return _claveSRCeI; }
            set { _claveSRCeI = value;
                USERTOKEN_PASSWORD = value;
            }
        }

        public string Ip
        {
            get { return _ip; }
            set { _ip = value; }
        }

        public int Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        #endregion Public properties propietario

#region Private Method

        /// <summary>
        /// Inicializa las variables del objeto desde config, para no perder tiempo en buquedas luego
        /// </summary>
        private void Initialization()
        {
            try
            {
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization IN...");
                if (_config == null || _config.DynamicDataItems == null)
                {
                    _isConfigured = false;
                    return;
                }

                foreach (DynamicDataItem dd in _config.DynamicDataItems)
                {
                    if (dd.key.Trim().Equals("UrlWSCOW"))
                    {
                        UrlWsCOW = dd.value.Trim();
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization URL COW = " + COW_ENDPOINT_BASE_URI);
                    }
                    if (dd.key.Trim().Equals("UrlWSPV"))
                    {
                        UrlWsPV = dd.value.Trim();
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization URL PV = " + PV_ENDPOINT_BASE_URI);
                    }
                    if (dd.key.Trim().Equals("UrlWSDS64"))
                    {
                        UrlWsDS64 = dd.value.Trim();
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization URL DS64 = " + DS64_ENDPOINT_BASE_URI);
                    }
                    if (dd.key.Trim().Equals("ClientType"))
                    {
                        ClientType = Convert.ToInt32(dd.value.Trim());
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization Client Type = " + CLIENT_TYPE);
                        //creo cleitne para luego usarlo sin tener que generarlo en cada Tx
                        var b = GetCustomHttpBinding();
                        EndpointIdentity identity = EndpointIdentity.CreateDnsIdentity("terceros.sign");
                        switch (CLIENT_TYPE)
                        {
                            case 1:
                                EndpointAddress ds64Endpoint = new EndpointAddress(new Uri(DS64_ENDPOINT_BASE_URI), identity);
                                DS64c = new DS64Client(b, ds64Endpoint);
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization DS64client != Null => " + (DS64c != null).ToString());
                                break;
                            case 2:
                                EndpointAddress pvEndpoint = new EndpointAddress(new Uri(PV_ENDPOINT_BASE_URI), identity);
                                PVc = new PVClient(b, pvEndpoint);
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization PVclient != Null => " + (PVc != null).ToString());
                                break;
                        }
                    }


                    if (dd.key.Trim().Equals("QReintentosVerify"))
                    {
                        _qReintentosVerify = Convert.ToInt32(dd.value.Trim());
                    }

                    if (dd.key.Trim().Equals("Timeout"))
                    {
                        try
                        {
                            _timeout = Convert.ToInt32(dd.value.Trim());
                        }
                        catch (Exception ex)
                        {
                            _timeout = 30000;
                        }
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization Timeout = " + _timeout.ToString());
                    }

                    if (dd.key.Trim().Equals("IdEmpresaSRCeI"))
                    {
                        _idEmpresaSRCeI = Convert.ToInt32(dd.value.Trim());
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization IdEmpresaSRCeI = " + _idEmpresaSRCeI.ToString());
                    }
                    if (dd.key.Trim().Equals("UserSRCeI"))
                    {
                        UserSRCeI = dd.value.Trim();
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization UserSRCeI = " + USERTOKEN_USERNAME);
                    }
                    if (dd.key.Trim().Equals("ClaveSRCeI"))
                    {
                        ClaveSRCeI = dd.value.Trim();
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization ClaveSRCeI = " + USERTOKEN_PASSWORD);
                    }

                    if (dd.key.Trim().Equals("Threshold"))
                    {
                        _threshold = Convert.ToInt32(dd.value.Trim());
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization TH = " + _threshold.ToString());
                    }
                    if (dd.key.Trim().Equals("WFoto"))
                    {
                        _wFoto = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("HFoto"))
                    {
                        _hFoto = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("WFirma"))
                    {
                        _wFirma = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("HFirma"))
                    {
                        _hFirma = Convert.ToInt32(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("TypeGet"))
                    {
                        _typeget = Convert.ToInt32(dd.value.Trim());
                        if (_typeget < 0 || _typeget > 6) _typeget = 0;
                        if (_typeget == 0 || _typeget == 1 || _typeget == 4 || _typeget == 5)
                        {
                            _getInfo = true;
                        }
                        if (_typeget == 0 || _typeget == 2 || _typeget == 4 || _typeget == 6)
                        {
                            _getFoto = true;
                        }
                        if (_typeget == 0 || _typeget == 3 || _typeget == 5 || _typeget == 6)
                        {
                            _getFirma = true;
                        }
                    }

                    if (dd.key.Trim().Equals("PathCertifySRCeI"))
                    {
                        PathcertifySRCeI = dd.value.Trim();
                        //_certifySRCeI = File.ReadAllBytes(this._pathcertifySRCeI);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization PathCertifySRCeI = " + PathcertifySRCeI);
                        //LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization _certifySRCeI.len = " + _certifySRCeI.Length);  
                    }
                    if (dd.key.Trim().Equals("PathCertifySignaturePFX"))
                    {
                        PathCertifySignaturePFX = dd.value.Trim();
                        //_certifySRCeI = File.ReadAllBytes(this._pathcertifySRCeI);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization PathCertifySignaturePFX = " + SIGNATURE_P12);
                        //LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization _certifySRCeI.len = " + _certifySRCeI.Length);  
                    }
                    if (dd.key.Trim().Equals("PswCertifySignaturePFX"))
                    {
                        PswCertifySignaturePFX = dd.value.Trim();
                        //_certifySRCeI = File.ReadAllBytes(this._pathcertifySRCeI);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization PswCertifySignature OK => " + (!String.IsNullOrEmpty(SIGNATURE_PASSWORD)).ToString());
                        //LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization _certifySRCeI.len = " + _certifySRCeI.Length);  
                    }

                    
                }
                _ip = BioVerify2018.Utiles.GetIP();
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization IP = " + _ip);
                _isConfigured = true;
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.Initialization OUT!");
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018 Initialization Error", ex);
                _isConfigured = false;
            }
        }

        /// <summary>
        /// Custom bindind. WS-Security: timestamp + usernameToken + encrypted + signature
        /// </summary>
        /// <returns>Obj custom binding</returns>
        private static CustomBinding GetCustomHttpBinding()
        {
            LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.GetCustomHttpBinding IN...");
            CustomBinding binding = new CustomBinding();
            try
            {
                //CustomBinding binding = new CustomBinding();

                // Open and Close = 20s 
                binding.OpenTimeout = new TimeSpan(0, 0, 20);
                binding.CloseTimeout = new TimeSpan(0, 0, 20);
                // Send and Receive = 300s
                binding.SendTimeout = new TimeSpan(0, 5, 0);
                binding.ReceiveTimeout = new TimeSpan(0, 5, 0);

                // ++ Setting message encoding binding ++
                var encodingElement = new TextMessageEncodingBindingElement();
                encodingElement.MessageVersion = MessageVersion.Soap11;
                encodingElement.WriteEncoding = Encoding.UTF8;
                encodingElement.ReaderQuotas.MaxArrayLength = 50000000;
                encodingElement.ReaderQuotas.MaxStringContentLength = 50000000;

                binding.Elements.Add(encodingElement);

                // ++ Setting https transport binding ++
                var httpsElement = new HttpsTransportBindingElement();
                // Messagge buffer size
                httpsElement.MaxBufferSize = 50000000;
                httpsElement.MaxReceivedMessageSize = 50000000;
                httpsElement.MaxBufferPoolSize = 50000000;
                httpsElement.RequireClientCertificate = true;

                // Others
                httpsElement.UseDefaultWebProxy = true;
                binding.Elements.Add(httpsElement);

                return binding;
                //// Open and Close = 20s 
                //binding.OpenTimeout = new TimeSpan(0, 0, 20);
                //binding.CloseTimeout = new TimeSpan(0, 0, 20);
                //// Send and Receive = 300s
                //binding.SendTimeout = new TimeSpan(0, 5, 0);
                //binding.ReceiveTimeout = new TimeSpan(0, 5, 0);
                //// ++ Setting security binding ++

                //SecurityBindingElement securityElement;
                //securityElement = (AsymmetricSecurityBindingElement)
                //SecurityBindingElement.CreateMutualCertificateBindingElement(MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10);
                ////SecurityBindingElement.CreateMutualCertificateDuplexBindingElement(MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10);
                //securityElement.SetKeyDerivation(false);
                //securityElement.IncludeTimestamp = false;
                //securityElement.SecurityHeaderLayout = SecurityHeaderLayout.Strict;
                //securityElement.DefaultAlgorithmSuite = SecurityAlgorithmSuite.Basic256;
                //securityElement.LocalClientSettings.DetectReplays = false;
                //(securityElement as AsymmetricSecurityBindingElement).MessageProtectionOrder = MessageProtectionOrder.SignBeforeEncrypt;
                //(securityElement as AsymmetricSecurityBindingElement).RequireSignatureConfirmation = false;

                //var userNameToken = new UserNameSecurityTokenParameters();
                //userNameToken.InclusionMode = SecurityTokenInclusionMode.AlwaysToRecipient;
                //securityElement.EndpointSupportingTokenParameters.Signed.Add(userNameToken);

                //binding.Elements.Add(securityElement);

                //// ++ Setting message encoding binding ++
                //var encodingElement = new TextMessageEncodingBindingElement();
                //encodingElement.MessageVersion = MessageVersion.Soap11;
                //encodingElement.WriteEncoding = Encoding.UTF8;
                //encodingElement.ReaderQuotas.MaxArrayLength = 50000000;
                //encodingElement.ReaderQuotas.MaxStringContentLength = 50000000;

                //binding.Elements.Add(encodingElement);

                //// ++ Setting https transport binding ++
                //var httpsElement = new HttpsTransportBindingElement();
                //httpsElement.MaxBufferSize = 50000000;
                //httpsElement.MaxReceivedMessageSize = 50000000;
                //httpsElement.MaxBufferPoolSize = 50000000;
                //httpsElement.RequireClientCertificate = true;

                //binding.Elements.Add(httpsElement);
            }
            catch (Exception ex)
            {
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.GetCustomHttpBinding Error", ex);
            }

            LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.GetCustomHttpBinding OUT!");
            return binding;
        }

        // callback used to validate the certificate in an SSL conversation
        private static bool ValidateRemoteCertificate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors policyErrors)
        {
            string lower = cert.Subject.ToLower();
            return (lower.Contains("terceros.sidiv.registrocivil.cl") || lower.Contains("servers ca") || lower.Contains("technical root ca"));
        }
 
         private byte[] ExtraeWSQFromParamIn(XmlParamIn oXmlIn)
        {
            byte[] wsqr = null;
            try
            {
                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.ExtraeWSQFromParamIn IN...");
                foreach (var sample in oXmlIn.SampleCollection)
                {
                    if (sample.Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ)
                    {
                        wsqr = Convert.FromBase64String(sample.Data);
                        break;
                    } else if (sample.Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW)
                    {
                        byte[] raw = Convert.FromBase64String(sample.Data);
                        WsqEncoder encoder = new WsqEncoder();
                        encoder.EncodeMemory(raw, 512, 512, out wsqr);
                        break;
                    }  
                }
                 
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.BioVerify2018 ExtraeWSQFromParamIn Error", ex);
            }
            LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.BioVerify2018.ExtraeWSQFromParamIn OUT!");
            return wsqr;
        }

#endregion Private Method

#region Implementation of IConnector

        private string _connectorId;

        private DynamicData _config;

        /// <summary>
        /// Id identificador del conector
        /// </summary>
        public string ConnectorId
        {
            get { return _connectorId; }
            set { _connectorId = value; }
        }

        /// <summary>
        /// Pares de key/value de configuracion para el conector
        /// </summary>
        public DynamicData Config
        {
            get { return _config; }
            set
            {
                 _config = value;
                 if (!_isConfigured) Initialization();
            }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */
            int _ret = Errors.IERR_OK;
            string msg;
            string xmloutbp;
            int iretremoto;
            int _currentThreshold;
            int idtx;


            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "");
            oXmlout.AddValue("result", "");
            oXmlout.AddValue("score", "");
            oXmlout.AddValue("threshold", "");
            oXmlout.AddValue("timestamp", "");
            //Added 02-208 para agregar todas las BpTxConx cuando son varias
            /*
                 Consultationtype > string > nombre metodo
                 Connectorid > string
                 Trackid > string > Trackid externa
                 Status > int > Retorno de servicio 
                 Result > int > 1-Verify Positivo | 2-Verify Negativo (Si es verify si es get 0)
                 Score > double > Score obtenido (Si es verify si es get 0)
                 Threshold > double > Umbral utilizado (Si es verify si es get 0)
                 Timestamp > string > fecha y hora tx externa
                Devuelve un Dynamicdata de la forma:
                    <DynamicData>
                        <DynamicDataItem>
                            <key>tx1</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                      .........  
                        <DynamicDataItem>
                            <key>txN</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                   </DynamicData>
             */
            DynamicData oExternalTxs = new DynamicData();
            oXmlout.AddValue("externaltxs", "");  //Tipo DynamicData

            //oXmlout.SetValue("message", "");
            //oXmlout.SetValue("trackid", "283734647");
            //oXmlout.SetValue("status", "0");
            //oXmlout.SetValue("result", "1");
            //oXmlout.SetValue("score", "1000");
            //oXmlout.SetValue("threshold", _threshold.ToString());
            //oXmlout.SetValue("timestamp", FormatFechaHoraSRCeI("23112013100000"));
            //xmloutput = DynamicData.SerializeToXml(oXmlout);
            //return Errors.IERR_OK;

            try
            {
                LOG.Debug("Bio.Core.Matcher.SRCeI.BioVerify2018 - Ingresando...");
                if (!this._isConfigured)
                {
                    msg = "Bio.Core.Matcher.SRCeI.BioVerify2018 Error = Connector No Configurado!";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
                }
                
                //1.-Deserializo parametros de entrada
                xmlinput = xmlinput.Replace("&#x0;", "");
                LOG.Debug("Bio.Core.Matcher.SRCeI.BioVerify2018 - xmlinput = " + xmlinput);
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlinput);
                if (oXmlIn == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.BioVerify2018 Error deserealizando xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //Tomo el WSQ si es que viene
                LOG.Debug("Bio.Core.Matcher.SRCeI.BioVerify2018 - Extrayendo WSQ from xmlinput...");
                byte[] wsq = ExtraeWSQFromParamIn(oXmlIn);

                if (wsq != null)
                {
                    LOG.Debug("Bio.Core.Matcher.SRCeI.BioVerify2018 - (wsq != null!");

                    _currentThreshold = (oXmlIn.Threshold > 0) ? (int)oXmlIn.Threshold : _threshold;


                    // validate cert by calling a function
                    System.Net.ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateRemoteCertificate);
                    // use TLS 1.2
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                    //var b = GetCustomHttpBinding();

                    //EndpointIdentity identity = EndpointIdentity.CreateDnsIdentity("terceros.sign");
                    //EndpointAddress cowEndpoint = new EndpointAddress(new Uri(COW_ENDPOINT_BASE_URI), identity);
                    //EndpointAddress ds64Endpoint = new EndpointAddress(new Uri(DS64_ENDPOINT_BASE_URI), identity);
                    //EndpointAddress pvEndpoint = new EndpointAddress(new Uri(PV_ENDPOINT_BASE_URI), identity);

                    AFISImpresionDactilarDS64Response responseds64;
                    cl.registrocivil.pv.AFISImpresionDactilarPVResponse responsepv;

                    RUN run = RUN.Convertir(oXmlIn.PersonalData.Valueid);
                    if (CLIENT_TYPE == 1)
                    {
                        responseds64 = DS64c.afisImpresionDactilar(oXmlIn.Bodypart, run.Mantisa, wsq, out idtx);
                        //ds64c.handleAFISImpresionDactilarResponse(responseds64);
                        if (responseds64 != null) // && response..EncabezadoRespuesta != null)
                        {
                            /*
                                201 (Error técnico)
                                206 (Solicitud de formato invalido)    
                                207 (Error Seguridad)
                                301 (Datos inexistentes)
                                304 (Mala Calidad)
                            */

                            if (responseds64.EncabezadoRespuesta != null) {
                                EstadoType? estado = responseds64.EncabezadoRespuesta.Estado;
                                if (EstadoType.Item000 == estado)
                                {
                                    oXmlout.SetValue("message", "");
                                    oXmlout.SetValue("trackid", idtx.ToString());
                                    oXmlout.SetValue("status", ConvertToNumber(estado.ToString()));
                                    oXmlout.SetValue("result", responseds64.Score >= _currentThreshold ? "1" : "2");
                                    oXmlout.SetValue("score", responseds64.Score.ToString());
                                    oXmlout.SetValue("threshold", _currentThreshold.ToString());
                                    oXmlout.SetValue("timestamp", responseds64.EncabezadoRespuesta.FechaHoraOperacion);
                                }
                                else //!= Item000 => Error
                                {
                                    oXmlout.SetValue("message", "Error indicado por SRCeI = " + estado.ToString());
                                    oXmlout.SetValue("trackid", idtx.ToString());
                                    oXmlout.SetValue("status", ConvertToNumber(estado.ToString()));
                                    oXmlout.SetValue("result", "2");
                                    oXmlout.SetValue("score", "0");
                                    oXmlout.SetValue("threshold", _currentThreshold.ToString());
                                    oXmlout.SetValue("timestamp", responseds64.EncabezadoRespuesta.FechaHoraOperacion);
                                    //_ret = Errors.IERR_CONNECTOR_MALFORMED_ANSWER;
                                }
                            }
                            int res = (responseds64.Score >= _currentThreshold) ? 1 : 2;
                            oExternalTxs.AddValue("txVerify", GetDDFormExternalTx("AFISImpresionDactilarDS64", responseds64, res, _currentThreshold, responseds64.Score));
                            oXmlout.SetValue("externaltxs", DynamicData.SerializeToXml(oExternalTxs));
                        }
                        else
                        {
                            msg = "Bio.Core.Matcher.SRCeI.BioVerify2018.Connector - Respuesta Erronea del SRCeI - " +
                                "Response=" + (responseds64 == null ? "NULL" : "Not NULL");
                            LOG.Fatal(msg);
                            oXmlout.SetValue("message", msg);
                            xmloutput = DynamicData.SerializeToXml(oXmlout);
                            return Errors.IERR_CONNECTOR_MALFORMED_ANSWER;
                        }
                    } else
                    {
                        //PVClient pvc = new PVClient(b, pvEndpoint);
                        responsepv = PVc.afisImpresionDactilar(oXmlIn.Bodypart, run.Mantisa, wsq, out idtx);
                        //pvc.handleAFISImpresionDactilarResponse(responsepv);

                        if (responsepv != null) // && response..EncabezadoRespuesta != null)
                        {
                        }
                     }
                }
                else
                {
                    msg = "Bio.Core.Matcher.SRCeI.BioVerify2018.Connector Falta huella para enviar";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
           
                xmloutput = DynamicData.SerializeToXml(oXmlout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.SRCeI.BioVerify2018.Verify Error [" + ex.Message + "]";
                LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.Verify Error", ex);
                oXmlout.SetValue("message", msg);
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                return Errors.IERR_UNKNOWN;
            }

            return _ret;
        }

        private string ConvertToNumber(string status)
        {
            string ret = "0";
            try
            {
                ret = status.Substring(4);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.ConvertToNumber Error", ex);
            }
            return ret;
        }

        private string ReadFromHDD(string path)
        {
            string sret = null;
            try
            {
                System.IO.StreamReader sr = new StreamReader(path);
                sret = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.ReadFromHDD Error", ex);
                sret = null;
            }
            return sret;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */

            string msg;
            xmloutput = null;
            int iretremoto;

            //DynamicData oXmlout = new DynamicData();
            //oXmlout.AddValue("message", "");
            //oXmlout.AddValue("trackid", "");
            //oXmlout.AddValue("status", "");
            //oXmlout.AddValue("result", "");
            //oXmlout.AddValue("score", "");
            //oXmlout.AddValue("timestamp", "");
            //oXmlout.AddValue("personaldata", "");

            //try
            //{
            //    if (!this._isConfigured)
            //    {
            //        msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error = Connector No Configurado!";
            //        LOG.Fatal(msg);
            //        oXmlout.SetValue("message", msg);
            //        xmloutput = DynamicData.SerializeToXml(oXmlout);
            //        return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
            //    }

            //    using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
            //    {
            //        ws.Timeout = this._timeout;
            //        ws.Url = this._urlWS;

            //        iretremoto = ws.Identify(xmlinput, out xmloutbp);
            //        if (iretremoto == Errors.IERR_OK)
            //        {
            //            XmlParamOut oXmlParamOut = Utils.XmlUtils.DeserializeObject<XmlParamOut>(xmloutbp);
            //            oXmlout.SetValue("message", "");
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            oXmlout.SetValue("trackid", oXmlParamOut.Trackid);
            //            oXmlout.SetValue("result", oXmlParamOut.Result.ToString());
            //            oXmlout.SetValue("score", oXmlParamOut.Score.ToString());
            //            oXmlout.SetValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
            //            oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
            //        }
            //        else
            //        {
            //            msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error en WS Remoto [" + iretremoto.ToString() + "]";
            //            LOG.Fatal(msg);
            //            oXmlout.SetValue("message", msg);
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            xmloutput = DynamicData.SerializeToXml(oXmlout);
            //            return Errors.IERR_CONNECTOR_REMOTE_ERROR;
            //        }
            //    }

            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //}
            //catch (Exception ex)
            //{
            //    msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error [" + ex.Message + "]";
            //    LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.Identity Error", ex);
            //    oXmlout.SetValue("message", msg);
            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //    return Errors.IERR_UNKNOWN;
            //}

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de Recuperación. Ingresa información recuperar, 
        /// y se realiza la operacion.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Get(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */

            string msg;
            string xmloutbp;
            int iretremoto;
            int idtx = 0;

            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "");
            oXmlout.AddValue("result", "0");
            oXmlout.AddValue("score", "0");
            oXmlout.AddValue("threshold", "0");
            oXmlout.AddValue("timestamp", "");
            oXmlout.AddValue("personaldata", "");
                //Added 02-208 para agregar todas las BpTxConx cuando son varias
            /*
                 Consultationtype > string > nombre metodo
                 Connectorid > string
                 Trackid > string > Trackid externa
                 Status > int > Retorno de servicio 
                 Result > int > 1-Verify Positivo | 2-Verify Negativo (Si es verify si es get 0)
                 Score > double > Score obtenido (Si es verify si es get 0)
                 Threshold > double > Umbral utilizado (Si es verify si es get 0)
                 Timestamp > string > fecha y hora tx externa
                Devuelve un Dynamicdata de la forma:
                    <DynamicData>
                        <DynamicDataItem>
                            <key>tx1</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                      .........  
                        <DynamicDataItem>
                            <key>txN</key>
                            <value>
                              <DynamicData>
                                <DynamicDataItem>
                                     ... Cada valor d elos de arriba...                                          
                                </DynamicDataItem>
                              </DynamicData>
                            </value>
                        </DynamicDataItem>
                   </DynamicData>
             */
            DynamicData oExternalTxs = new DynamicData();
            oXmlout.AddValue("externaltxs", "");  //Tipo DynamicData


            //oXmlout.SetValue("message", "");
            //oXmlout.SetValue("trackid", "283734647");
            //oXmlout.SetValue("status", "0");
            //oXmlout.SetValue("result", "1");
            //oXmlout.SetValue("score", "1000");
            //oXmlout.SetValue("threshold", _threshold.ToString());
            //oXmlout.SetValue("timestamp", FormatFechaHoraSRCeI("23112013100000"));
            //xmloutput = DynamicData.SerializeToXml(oXmlout);
            //return Errors.IERR_OK;
            LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo208 IN...");
            try
            {
                if (!this._isConfigured)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo208 Error = Connector No Configurado!";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
                }
                else
                {
                    LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo208 Connector Configurado!");
                }


                //1.-Deserializo parametros de entrada
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlinput);
                if (oXmlIn == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo208 Error deserealizando xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo208 XmlParamin <> null");
                }

                if (oXmlIn.PersonalData == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo208 Error PersonalData Nulo en xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    try
                    {
                        LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo208 tomando RUT...");
                        _currenRUN = RUN.ConvertirEx(oXmlIn.PersonalData.Valueid.Trim());
                        if (_currenRUN == null)
                        {
                            msg = "Bio.Core.Matcher.SRCeI.GetInfo208 Error Parseando RUT PersonalData.ValueId (_currenRUN == null) en xmlinput";
                            LOG.Fatal(msg);
                            oXmlout.SetValue("message", msg);
                            xmloutput = DynamicData.SerializeToXml(oXmlout);
                            return Errors.IERR_BAD_PARAMETER;
                        }
                        else
                        {
                            LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo208 RUT para consultar = " + _currenRUN.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        msg = "Bio.Core.Matcher.SRCeI.GetInfo208 Error Parseando RUT PersonalData.ValueId en xmlinput";
                        LOG.Fatal(msg);
                        oXmlout.SetValue("message", msg);
                        xmloutput = DynamicData.SerializeToXml(oXmlout);
                        return Errors.IERR_UNKNOWN;
                    }
                }

                // validate cert by calling a function
                System.Net.ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateRemoteCertificate);
                // use TLS 1.2
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                //var b = GetCustomHttpBinding();

                //EndpointIdentity identity = EndpointIdentity.CreateDnsIdentity("terceros.sign");
                //EndpointAddress cowEndpoint = new EndpointAddress(new Uri(COW_ENDPOINT_BASE_URI), identity);
                //EndpointAddress ds64Endpoint = new EndpointAddress(new Uri(DS64_ENDPOINT_BASE_URI), identity);
                //EndpointAddress pvEndpoint = new EndpointAddress(new Uri(PV_ENDPOINT_BASE_URI), identity);

                ConsultaDocumentoDS64Response responseDatos = null;
                ConsultaFotografiaDS64Response responseFoto = null;
                ConsultaFotografiaDS64Response responseFirma = null;
                ConsultaDocumentoPVResponse responseDatosPV = null;
                ConsultaFotografiaPVResponse responseFotoPV = null;
                ConsultaFotografiaPVResponse responseFirmaPV = null;

                RUN run = RUN.Convertir(oXmlIn.PersonalData.Valueid);
                if (CLIENT_TYPE == 1) // => Usar DS64
                {
                    //DS64Client ds64c = new DS64Client(b, ds64Endpoint);


                    if (_getInfo)
                    { //Tomo info
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2018.Connector Ingresando a consulta Datos en URL = "); // + client.Endpoint.ListenUri);
                        //responseDatos = client.ConsultaDocumentoDS64(objEncabezado, objDocParam, out objDocRet);
                        responseDatos = DS64c.ConsultaDocumentoDS64(run.Mantisa, out idtx);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2018.Connector Salio consulta Datos!");
                    }

                    if (_getFoto)
                    { //Tomo Foto
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connector Ingresando a consulta Foto en URL = "); // + client.Endpoint.ListenUri);
                        //responseFoto = client.ConsultaFotografiaDS64(objEncabezado, runparam, out dataFoto);
                        responseFoto = DS64c.ConsultaFotoDS64(run.Mantisa, out idtx);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connector Salio consulta Foto!");
                    }

                    if (_getFirma)
                    { //Tomo Firma
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connector Ingresando a consulta Firma en URL = "); // + client.Endpoint.ListenUri);
                        //responseFirma = client.ConsultaFirmaDS64(objEncabezado, runparam, out dataFirma);
                        responseFirma = DS64c.ConsultaFirmaDS64(run.Mantisa, out idtx);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connector Salio consulta Firma!");
                    }

                }
                else //if (CLIENT_TYPE == 1) => Usar PV
                {
                    if (_getInfo)
                    { //Tomo info
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2018.Connector Ingresando a consulta Datos en URL = "); // + client.Endpoint.ListenUri);
                        //responseDatos = client.ConsultaDocumentoDS64(objEncabezado, objDocParam, out objDocRet);
                        responseDatosPV =  PVc.ConsultaDocumentoPV(run.Mantisa, out idtx);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo2018.Connector Salio consulta Datos!");
                    }

                    if (_getFoto)
                    { //Tomo Foto
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connector Ingresando a consulta Foto en URL = "); // + client.Endpoint.ListenUri);
                        //responseFoto = client.ConsultaFotografiaDS64(objEncabezado, runparam, out dataFoto);
                        responseFotoPV = PVc.ConsultaFotoPV(run.Mantisa, out idtx);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connector Salio consulta Foto!");
                    }

                    if (_getFirma)
                    { //Tomo Firma
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connector Ingresando a consulta Firma en URL = "); // + client.Endpoint.ListenUri);
                        //responseFirma = client.ConsultaFirmaDS64(objEncabezado, runparam, out dataFirma);
                        responseFirmaPV = PVc.ConsultaFirmaPV(run.Mantisa, out idtx);
                        LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connector Salio consulta Firma!");
                    }
                }

                    // trust sender

                    if (responseDatos == null && responseFoto == null && responseFirma == null)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2018.Connector - Respuesta Erronea del SRCeI - " +
                      "ResponseDatos=" + (responseDatos == null ? "NULL" : "Not NULL") + " | " +
                      "ResponseFoto=" + (responseFoto == null ? "NULL" : "Not NULL") + " | " +
                      "ResponseFirma=" + (responseFirma == null ? "NULL" : "Not NULL");
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_MALFORMED_ANSWER;
                }

                if (_getInfo && responseDatos != null && responseDatos.EncabezadoRespuesta.Estado != EstadoType.Item000)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo2018.Connector - Respuesta Erronea del SRCeI - " +
                      "ResponseDatos.Estado=" + responseDatos.EncabezadoRespuesta.Estado.ToString();
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    oXmlout.SetValue("status", ConvertToNumber(responseDatos.EncabezadoRespuesta.Estado.ToString()));
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                }

                if (_getFoto && responseFoto != null && responseFoto.EncabezadoRespuesta.Estado != EstadoType.Item000)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo208.Connector - Respuesta Erronea del SRCeI - " +
                      "ResponseFoto.Estado=" + responseFoto.EncabezadoRespuesta.Estado.ToString();
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    oXmlout.SetValue("status", ConvertToNumber(responseFoto.EncabezadoRespuesta.Estado.ToString()));
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                }

                if (_getFirma && responseFirma != null && responseFirma.EncabezadoRespuesta.Estado != EstadoType.Item000)
                {
                    msg = "Bio.Core.Matcher.SRCeI.GetInfo208.Connector - Respuesta Erronea del SRCeI - " +
                      "ResponseFirma.Estado=" + responseFirma.EncabezadoRespuesta.Estado.ToString();
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    oXmlout.SetValue("status", ConvertToNumber(responseFirma.EncabezadoRespuesta.Estado.ToString()));
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_REMOTE_ERROR;
                }


                //else
                //{
                /*
                    201 (Error técnico)
                    206 (Solicitud de formato invalido)    
                    207 (Error Seguridad)
                    301 (Datos inexistentes)
                */
                string _satusConcat = "0"; //Concatena estados de todas las consultas
                string _msgerrConcat = ""; //Concatena msg errores de todas las consultas
                PersonalData pdataOut = new PersonalData();
                string _fechaOp = "";
                pdataOut.Typeid = "RUT";
                pdataOut.Valueid = _currenRUN.Mantisa + "-" + _currenRUN.Dv;
                pdataOut.Verificationsource = "SRCeI";
                if (_getInfo)
                { //Proceso info
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getInfo IN...");
                    if (responseDatos != null)
                    {
                        if (responseDatos.EncabezadoRespuesta.Estado == EstadoType.Item000)
                        {
                            LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getInfo armando retorno pdataOut...");
                            //_satusConcat = "0"; //"EstadoDatos:" + responseDatos.Estado;
                            oExternalTxs.AddValue("txDatos", GetDDFormExternalTx("ConsultaDocumentoDS64", responseDatos.EncabezadoRespuesta, 0, 0, 0));
                            _fechaOp = responseDatos.EncabezadoRespuesta.FechaHoraOperacion;
                            LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getInfo responseDatos.FechaHoraOperacion = " + _fechaOp);
                            if (responseDatos != null && responseDatos.DatosDocumento != null)
                            {
                                pdataOut.Name = responseDatos.DatosDocumento.Nombres;
                                pdataOut.Patherlastname = responseDatos.DatosDocumento.PrimerApellido;
                                pdataOut.Motherlastname = responseDatos.DatosDocumento.SegundoApellido;
                                pdataOut.Sex = responseDatos.DatosDocumento.Sexo.ToString();
                                pdataOut.Documentseriesnumber = responseDatos.DatosDocumento.Serie;
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getInfo objDocRet.FechaVencimiento = " + responseDatos.DatosDocumento.FechaVencimiento);
                                pdataOut.Documentexpirationdate = ParseFecha(responseDatos.DatosDocumento.FechaVencimiento);
                                pdataOut.Visatype = responseDatos.DatosDocumento.TipoVisa;
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getInfo objDocRet.FechaNacimiento = " + responseDatos.DatosDocumento.FechaNacimiento);
                                pdataOut.Birthdate = ParseFecha(responseDatos.DatosDocumento.FechaNacimiento);
                                pdataOut.Birthplace = responseDatos.DatosDocumento.LugarNacimiento;
                                pdataOut.Nationality = responseDatos.DatosDocumento.PaisNacionalidad;
                                pdataOut.Profession = responseDatos.DatosDocumento.Profesion;
                            }
                            else
                            {
                                _msgerrConcat = "Bio.Core.Matcher.SRCeI.GetInfo208.Get Error [objDocRet=NULL]";
                                LOG.Error(_msgerrConcat);
                            }
                        }
                        else
                        {
                            _satusConcat = ConvertToNumber(responseDatos.EncabezadoRespuesta.Estado.ToString());
                            oExternalTxs.AddValue("txDatos", GetDDFormExternalTx("ConsultaDocumentoDS64", responseDatos.EncabezadoRespuesta, 0, 0, 0));
                        }
                    }
                    else
                    {
                        _satusConcat = "-1";
                    }
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getInfo OUT!");
                }

                if (_getFoto)
                { //Proceso Foto
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getFoto IN...");
                    if (responseFoto != null)
                    {
                        if (responseFoto.EncabezadoRespuesta.Estado == EstadoType.Item000)
                        {
                            oExternalTxs.AddValue("txFoto", GetDDFormExternalTx("ConsultaFotografiaDS64", responseFoto.EncabezadoRespuesta, 0, 0, 0));
                            _fechaOp = responseDatos.EncabezadoRespuesta.FechaHoraOperacion;
                            if (responseFoto != null && responseFoto.Imagen != null)
                            {
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getFoto GeneraImagenB64FromBytes IN...");
                                pdataOut.Photography = GeneraImagenB64FromBytes(1, responseFoto.Imagen.Imagen, responseFoto.Imagen.Formato);
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getFoto GeneraImagenB64FromBytes OUT!");
                            }
                            else
                            {
                                _msgerrConcat = (String.IsNullOrEmpty(_msgerrConcat)) ?
                                    "Bio.Core.Matcher.SRCeI.GetInfo208.Get Error [dataFoto=NULL]" :
                                    _msgerrConcat + "|Bio.Core.Matcher.SRCeI.GetInfo208.Get Error [dataFoto=NULL]";
                                LOG.Error(_msgerrConcat);
                            }
                        }
                        else
                        {
                            _satusConcat = ConvertToNumber(responseFoto.EncabezadoRespuesta.Estado.ToString());
                            oExternalTxs.AddValue("txFoto", GetDDFormExternalTx("ConsultaFotografiaDS64", responseFoto.EncabezadoRespuesta, 0, 0, 0));
                        }
                    }
                    else
                    {
                        _satusConcat = "-1";
                    }
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getFoto OUT!");
                }

                if (_getFirma)
                { //Proceso Firma
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getFirma IN...");
                    if (responseFirma != null)
                    {
                        if (responseFirma.EncabezadoRespuesta.Estado == EstadoType.Item000)
                        {
                            oExternalTxs.AddValue("txFirma", GetDDFormExternalTx("ConsultaFirmaDS64", responseFirma.EncabezadoRespuesta, 0, 0, 0));
                            _fechaOp = responseDatos.EncabezadoRespuesta.FechaHoraOperacion;
                            if (responseFirma != null && responseFirma.Imagen != null)
                            {
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getFirma GeneraImagenB64FromBytes IN...");
                                pdataOut.Signatureimage = GeneraImagenB64FromBytes(2, responseFirma.Imagen.Imagen, responseFirma.Imagen.Formato);
                                LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getFirma GeneraImagenB64FromBytes OUT!");
                            }
                            else
                            {
                                _msgerrConcat = (String.IsNullOrEmpty(_msgerrConcat)) ?
                                    "Bio.Core.Matcher.SRCeI.GetInfo208.Get Error [dataFoto=NULL]" :
                                    _msgerrConcat + "|Bio.Core.Matcher.SRCeI.GetInfo208.Get Error [dataFirma=NULL]";
                                LOG.Error(_msgerrConcat);
                            }
                        }
                        else
                        {
                            _satusConcat = ConvertToNumber(responseFirma.EncabezadoRespuesta.Estado.ToString());
                            oExternalTxs.AddValue("txFoto", GetDDFormExternalTx("ConsultaFirmaDS64", responseFirma.EncabezadoRespuesta, 0, 0, 0));
                        }
                    }
                    else
                    {
                        _satusConcat = "-1";
                    }
                    LOG.Debug("Bio.Core.Matcher.Connector.SRCeI.GetInfo208.Connecto _getFirma OUT!");
                }

                oXmlout.SetValue("message", _msgerrConcat);
                oXmlout.SetValue("status", _satusConcat);
                oXmlout.SetValue("trackid", idtx.ToString());
                oXmlout.SetValue("timestamp", (!String.IsNullOrEmpty(_fechaOp) ? _fechaOp : DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")));
                oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(pdataOut));
                oXmlout.SetValue("externaltxs", DynamicData.SerializeToXml(oExternalTxs));
            
                xmloutput = DynamicData.SerializeToXml(oXmlout);

            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.SRCeI.GetInfo208.Get Error [" + ex.Message + "]";
                LOG.Error("Bio.Core.Matcher.SRCeI.GetInfo208.Get Error", ex);
                oXmlout.SetValue("message", msg);
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                return Errors.IERR_UNKNOWN;
            }
            LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo208.Get OUT! => xmloutput = " + xmloutput);
            return Errors.IERR_OK;
        }

        private DateTime ParseFecha(string _date)
        {
            DateTime ret = new DateTime();
            LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo208.ParseFecha IN...");
            try
            {
                LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo208.ParseFecha => Parseando = " + _date);
                ret = DateTime.ParseExact(_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo208.ParseFecha => Fecha Parseada = " + ret.ToString("dd/MM/yyyy"));
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.GetInfo208.ParseFecha Error", ex);
            }
            LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo208.ParseFecha OUT!");
            return ret;
        }

        private string GetDDFormExternalTx(string CT, AFISImpresionDactilarDS64Response response,
                                           int result, int th, double score)
        {
            string _ret = "";
            try
            {
                if (response == null) return "";

                DynamicData ddTx = new DynamicData();
                ddTx.AddValue("Consultationtype", CT);
                ddTx.AddValue("Connectorid", "SRCeI.BioVerify2018");
                ddTx.AddValue("Trackid", response.EncabezadoRespuesta.IdTransaccion.ToString());
                ddTx.AddValue("Status", ConvertToNumber(response.EncabezadoRespuesta.Estado.ToString()));
                ddTx.AddValue("Result", result.ToString());
                ddTx.AddValue("Score", score.ToString());
                ddTx.AddValue("Threshold", th.ToString());
                ddTx.AddValue("Timestamp", response.EncabezadoRespuesta.FechaHoraOperacion.ToString());
                _ret = DynamicData.SerializeToXml(ddTx);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.BioVerify2018 Error", ex);
                _ret = "";
            }
            return _ret;
        }

        private string GetDDFormExternalTx(string CT, EncabezadoRespuesta response,
                                    int result, int th, double score)
        {
            string _ret = "";
            try
            {
                if (response == null) return "";

                DynamicData ddTx = new DynamicData();
                ddTx.AddValue("Consultationtype", CT);
                ddTx.AddValue("Connectorid", "SRCeI.BioVerify2018");
                ddTx.AddValue("Trackid", response.IdTransaccion.ToString());
                ddTx.AddValue("Status", ConvertToNumber(response.Estado.ToString()));
                ddTx.AddValue("Result", result.ToString());
                ddTx.AddValue("Score", score.ToString());
                ddTx.AddValue("Threshold", th.ToString());
                ddTx.AddValue("Timestamp", response.FechaHoraOperacion.ToString());
                _ret = DynamicData.SerializeToXml(ddTx);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.BioVerify2018 Error", ex);
                _ret = "";
            }
            return _ret;
        }
        /// <summary>
        /// Dado un arreglo de byte[] copn una imagen que viene del SRCeI, 
        /// Lo transforma a 
        /// </summary>
        /// <param name="p"></param>
        /// <param name="nullable"></param>
        /// <returns></returns>
        private string GeneraImagenB64FromBytes(int imageType, byte[] byImage, FormatoType? formato)
        {
            string _strret = null;
            try
            {
                LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo208.Get GeneraImagenB64FromBytes IN...");
                FIBITMAP dib = new FIBITMAP();
                FreeImage.UnloadEx(ref dib);
                IntPtr imgPtr = Marshal.AllocHGlobal(byImage.Length);
                Marshal.Copy(byImage, 0, imgPtr, byImage.Length);
                FIMEMORY fiMStream = FreeImage.OpenMemory(imgPtr, (uint)byImage.Length);
                // get the file type
                FREE_IMAGE_FORMAT fiFormat = FreeImage.GetFileTypeFromMemory(fiMStream, 0);
                // load an image from the memory stream
                // mFileHandle is a static uint property of the class
                dib = FreeImage.LoadFromMemory(fiFormat, fiMStream, 0);
                //Bitmap bFoto = FreeImage.GetBitmap(dib);
                FIBITMAP dib2;
                if (imageType == 1) //es Foto
                {
                    if (_wFoto > 0) dib2 = FreeImage.Rescale(dib, _wFoto, _hFoto, FREE_IMAGE_FILTER.FILTER_BICUBIC);
                    else dib2 = dib;
                }
                else
                { //Es Firma
                    if (_wFirma > 0) dib2 = FreeImage.Rescale(dib, _wFirma, _hFirma, FREE_IMAGE_FILTER.FILTER_BICUBIC);
                    else dib2 = dib;
                }
                byte[] buff = new byte[FreeImage.GetWidth(dib2) * FreeImage.GetHeight(dib2)];
                MemoryStream ms = new MemoryStream(buff);
                FreeImage.SaveToStream(dib2, ms, FREE_IMAGE_FORMAT.FIF_JPEG);
                _strret = Convert.ToBase64String(buff);
                ms.Close();
                Marshal.FreeHGlobal(imgPtr);

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.SRCeI.GetInfo208.Get Error", ex);
                _strret = null;
            }
            LOG.Debug("Bio.Core.Matcher.SRCeI.GetInfo208.Get GeneraImagenB64FromBytes OUT!");
            return _strret;
        }

#endregion Implementation of IConnector

        private static string FormatFechaHoraSRCeI(string fechaoperacion)
        {
            string strRet = "";
            try
            {
                strRet = fechaoperacion.Substring(6, 2) + "/" +
                         fechaoperacion.Substring(4, 2) + "/" +
                         fechaoperacion.Substring(0, 4) + " " +
                         fechaoperacion.Substring(8, 2) + ":" +
                         fechaoperacion.Substring(10, 2) + ":" +
                         fechaoperacion.Substring(12, 2);
            }
            catch
            {
                strRet = "";
            }
            return strRet;
        }

        public void Dispose()
        {
            
        }
    }
}
