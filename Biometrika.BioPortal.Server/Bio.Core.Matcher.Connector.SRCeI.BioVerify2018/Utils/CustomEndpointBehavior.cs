﻿using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Xml;

namespace Bio.Core.Matcher.Connector.SRCeI.BioVerify2018
{
    internal class CustomEndpointBehavior : IEndpointBehavior
    {
        public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
            // No implementation necessary  
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new CustomMessageInspector());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            // No implementation necessary  
        }

        public void Validate(ServiceEndpoint endpoint)
        {
            // No implementation necessary  
        }
    }

    internal class CustomMessageInspector : IClientMessageInspector
    {
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            // No implementation necessary  
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            // sign the soap message
            string signedSoapMessage = SignatureHelper.SignRequest(request.ToString());
            // Modify the request with signed SOAP message
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(signedSoapMessage));
            var reader = XmlDictionaryReader.CreateTextReader(ms, new XmlDictionaryReaderQuotas());
            request = Message.CreateMessage(reader, Int32.MaxValue, request.Version);

            return null;
        }
    }

}