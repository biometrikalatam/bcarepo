﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;

namespace Bio.Core.Matcher.Connector.SRCeI.BioVerify2018
{
    public class MessageConstants
    {
        public const string UsernameTokenType = "uri:usernameTokenSample";

        internal const string UsernameTokenPrefix = "o";
        internal const string UsernameTokenNamespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
        internal const string UsernameTokenName = "UsernameToken";

        internal const string IdAttributeName = "Id";
        internal const string WsUtilityPrefix = "u";
        internal const string WsUtilityNamespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
        internal const string UsernameElementName = "Username";
        internal const string PasswordElementName = "Password";
        internal const string NonceElementName = "Nonce";
        internal const string EncodingTypeAttributeName = "EncodingType";
        internal const string NonceEncodingTypeText = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary";
        internal const string CreatedElementName = "Created";
        internal const string TypeAttributeName = "Type";
        internal const string PasswordTextType = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";
    }

    class SignatureHelper
    {
        public static string GetSHA1String(string phrase)
        {
            SHA1CryptoServiceProvider sha1Hasher = new SHA1CryptoServiceProvider();
            byte[] hashedDataBytes = sha1Hasher.ComputeHash(Encoding.UTF8.GetBytes(phrase));
            return Convert.ToBase64String(hashedDataBytes);
        }

        public static string SignRequest(string request)
        {
            if (string.IsNullOrEmpty(request))
                throw new ArgumentNullException("request");

            // Load the certificate from file.
            X509Certificate2 cert = new X509Certificate2(Connector.SIGNATURE_P12, Connector.SIGNATURE_PASSWORD, X509KeyStorageFlags.Exportable | X509KeyStorageFlags.PersistKeySet);

            // Create a new XML document.
            XmlDocument doc = new XmlDocument();

            // Format the document to ignore white spaces.
            doc.PreserveWhitespace = false;

            // Load the passed XML 
            doc.LoadXml(request);

            // Add the declaration as per Entrust sample provided -don't think it's necessary though
            if (!(doc.FirstChild is XmlDeclaration))
            {
                XmlDeclaration declaration = doc.CreateXmlDeclaration("1.0", "UTF-8", string.Empty);
                doc.InsertBefore(declaration, doc.FirstChild);
            }

            // Remove the Action (MustUnderstand). 
            // TODO: Need to find a more elegant way to do so
            XmlNode headerNode = null;
            XmlNodeList nodeList = doc.GetElementsByTagName("Action");
            if (nodeList.Count > 0)
            {
                headerNode = nodeList[0].ParentNode;
                headerNode.RemoveChild(nodeList[0]);
            }

            // Set the body id
            XmlNamespaceManager ns = new XmlNamespaceManager(doc.NameTable);
            ns.AddNamespace("o", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            ns.AddNamespace("u", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            ns.AddNamespace("s", "http://schemas.xmlsoap.org/soap/envelope/");

            XmlElement body = doc.DocumentElement.SelectSingleNode(@"//s:Body", ns) as XmlElement;
            if (body == null)
                throw new ApplicationException("No body tag found");
            body.RemoveAllAttributes();  // no need to have namespace
            String bodyUUID = String.Format("B-{0}", Guid.NewGuid().ToString());
            body.SetAttribute("Id", bodyUUID);

            XmlNode securityNode = doc.CreateNode(XmlNodeType.Element, "o", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");

            DateTime created = DateTime.Now.ToUniversalTime();
            string createdStr = created.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            string phrase = Guid.NewGuid().ToString();
            var nonce = GetSHA1String(phrase);

            XmlElement usernameTokenNode = doc.CreateElement(MessageConstants.UsernameTokenPrefix, MessageConstants.UsernameTokenName, MessageConstants.UsernameTokenNamespace);
            String usernameTokenUUID = Guid.NewGuid().ToString();
            XmlAttribute usernameTokenId = doc.CreateAttribute(MessageConstants.WsUtilityPrefix, MessageConstants.IdAttributeName, MessageConstants.WsUtilityNamespace);
            usernameTokenId.Value = String.Format("UID-{0}", usernameTokenUUID);
            usernameTokenNode.Attributes.Append(usernameTokenId);

            XmlElement usernameElement = doc.CreateElement(MessageConstants.UsernameTokenPrefix, MessageConstants.UsernameElementName, MessageConstants.UsernameTokenNamespace);
            XmlText usernameElementValue = doc.CreateTextNode(Connector.USERTOKEN_USERNAME);
            usernameElement.AppendChild(usernameElementValue);
            usernameTokenNode.AppendChild(usernameElement);

            XmlElement passwordElement = doc.CreateElement(MessageConstants.UsernameTokenPrefix, MessageConstants.PasswordElementName, MessageConstants.UsernameTokenNamespace);
            XmlAttribute passwordType = doc.CreateAttribute(MessageConstants.TypeAttributeName);
            passwordType.Value = MessageConstants.PasswordTextType;
            passwordElement.Attributes.Append(passwordType);
            XmlText passwordElementValue = doc.CreateTextNode(Constants.USERTOKEN_PASSWORD);
            passwordElement.AppendChild(passwordElementValue);
            usernameTokenNode.AppendChild(passwordElement);

            XmlElement nonceElement = doc.CreateElement(MessageConstants.UsernameTokenPrefix, MessageConstants.NonceElementName, MessageConstants.UsernameTokenNamespace);
            XmlAttribute nonceType = doc.CreateAttribute(MessageConstants.EncodingTypeAttributeName);
            nonceType.Value = MessageConstants.NonceEncodingTypeText;
            nonceElement.Attributes.Append(nonceType);
            XmlText nonceElementValue = doc.CreateTextNode(nonce);
            nonceElement.AppendChild(nonceElementValue);
            usernameTokenNode.AppendChild(nonceElement);

            XmlElement createdElement = doc.CreateElement(MessageConstants.WsUtilityPrefix, MessageConstants.CreatedElementName, MessageConstants.WsUtilityNamespace);
            XmlText createdElementValue = doc.CreateTextNode(createdStr);
            createdElement.AppendChild(createdElementValue);
            usernameTokenNode.AppendChild(createdElement);

            // Create a custom SignedXml object so that we could sign the keyinfo
            CustomSignedXml signedXml = new CustomSignedXml(doc);
            signedXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl;

            // Add the key to the SignedXml document. 
            signedXml.SigningKey = cert.PrivateKey;

            // Create a new KeyInfo object.
            KeyInfo keyInfo = new KeyInfo();
            String keyInfoUUID = Guid.NewGuid().ToString();
            keyInfo.Id = String.Format("KI-{0}", keyInfoUUID);

            // Load the certificate into a KeyInfoX509Data object
            // and add it to the KeyInfo object.
            KeyInfoX509Data keyInfoData = new KeyInfoX509Data();
            keyInfoData.AddIssuerSerial(cert.Issuer, cert.GetSerialNumberString());
            keyInfoData.AddCertificate(cert);
            keyInfo.AddClause(keyInfoData);

            // Add the KeyInfo object to the SignedXml object.
            signedXml.KeyInfo = keyInfo;

            // Create a reference to be signed.
            Reference reference = new Reference();
            reference.Uri = String.Format("#{0}", bodyUUID);

            // Add an enveloped transformation to the reference.
            XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
            env.Algorithm = "http://www.w3.org/2001/10/xml-exc-c14n#";
            //env.InclusiveNamespacesPrefixList = "urn";
            reference.DigestMethod = "http://www.w3.org/2000/09/xmldsig#sha1";
            reference.AddTransform(env);

            // Add the reference to the SignedXml object.
            signedXml.AddReference(reference);

            // Add the Signature Id
            String signatureUUID = Guid.NewGuid().ToString();
            signedXml.Signature.Id = String.Format("S-{0}", signatureUUID);

            // Compute the signature.
            signedXml.ComputeSignature();

            // Get the XML representation of the signature and save
            // it to an XmlElement object.
            XmlElement xmlDigitalSignature = signedXml.GetXml();

            // Append the Signature element to the XML document.
            if (headerNode != null)
            {
                securityNode.AppendChild(doc.ImportNode(xmlDigitalSignature, true));
                securityNode.AppendChild(usernameTokenNode);
                headerNode.AppendChild(securityNode);
            }

            return doc.InnerXml;
        }
    }

    public class CustomSignedXml : SignedXml
    {
        public CustomSignedXml(XmlDocument doc) : base(doc)
        {
            return;
        }
        public override XmlElement GetIdElement(XmlDocument doc, string id)
        {
            // see if this is the key info being referenced, otherwise fall back to default behavior
            if (String.Compare(id, this.KeyInfo.Id, StringComparison.OrdinalIgnoreCase) == 0)
                return this.KeyInfo.GetXml();
            else
                return base.GetIdElement(doc, id);
        }
    }

}
