﻿using System;

namespace Bio.Core.Matcher.Connector.SRCeI.BioVerify2018
{
    class Utils
    {
        public static double GetUnixEpoch(DateTime dateTime)
        {
            var unixTime = dateTime.ToUniversalTime() -
                new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return unixTime.TotalSeconds;
        }
    }
}
