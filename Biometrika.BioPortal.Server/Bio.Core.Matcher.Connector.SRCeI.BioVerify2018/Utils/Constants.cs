﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bio.Core.Matcher.Connector.SRCeI.BioVerify2018
{
    class Constants
    {
        public static readonly string COW_ENDPOINT_BASE_URI = "https://terceros.sidiv.registrocivil.cl:8443/InteroperabilityPlateform/TercerosDS64ProxyService";
        public static readonly string DS64_ENDPOINT_BASE_URI = "https://terceros.sidiv.registrocivil.cl:8443/InteroperabilityPlateform/TercerosDS64ProxyService";
        public static readonly string PV_ENDPOINT_BASE_URI = "https://terceros.sidiv.registrocivil.cl:8443/InteroperabilityPlateform/TercerosPVProxyService";
        public static readonly string ENDPOINT_CERTIFICATE = @"D:\Biometrika\Desarrollo\SRCeI_2015\2018\solucionNueva\certificados\RegistroCivil.cer";
        public static readonly string USERTOKEN_USERNAME = "usr_sence";
        public static readonly string USERTOKEN_PASSWORD = "dE34frSE#SK2";
        public static readonly string SIGNATURE_P12 = @"D:\Biometrika\Desarrollo\SRCeI_2015\2018\solucionNueva\certificados\SENCE_SIGN_283B2A1509EB08B7.p12";
        public static readonly string SIGNATURE_PASSWORD = "sence";
    }
}
