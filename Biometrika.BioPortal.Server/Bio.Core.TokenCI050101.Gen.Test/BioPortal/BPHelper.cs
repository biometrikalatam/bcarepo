﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bio.Core.Api;
using BioPortal.Server.Api;

namespace Bio.Core.TokenCI050101.Gen.Test.BioPortal
{
    internal static class BPHelper
    {
        internal static Bio.Core.TokenCI050101.Gen.TokenCI050101Generator GENERATOR = new TokenCI050101Generator();

        internal static int VerifyNECDMAbyBioPortal(string url, string _rut, string _clienteId, string _endUser, string _ipUser, string _serialIdSensor,
                                                    short _fingerId, string _sample, string _pdf417, short _typeDevice,
                                                    out bool _matchok, out double _score)
        {
            int _ret_actual = 0;
            string xmlparamout = string.Empty;
            _matchok = false;
            _score = 0;

            try
            {
                using (BioPortalServerWSWEB.BioPortalServerWSWeb target = new BioPortalServerWSWEB.BioPortalServerWSWeb())
                {
                    target.Timeout = Properties.Settings.Default.WSTimeout;
                    if (String.IsNullOrEmpty(url))
                    {
                        target.Url = Properties.Settings.Default.Bio_Core_TokenCI050101_Gen_Test_BioPortalServerWSWEB_BioPortalServerWSWeb;
                    }
                    else
                    {
                        target.Url = url; // Properties.Settings.Default.Bio_Core_TokenCI050101_Gen_Test_BioPortalServerWSWEB_BioPortalServerWSWeb;
                    }
                    XmlParamIn pin = new XmlParamIn();
                    pin.Actionid = 1;  //Verify
                    pin.Additionaldata = null;
                    pin.Authenticationfactor = 2; //Fingerprint
                    pin.Minutiaetype = 24; //Token
                    pin.Bodypart = 1;  //pulgar derecho
                    pin.Clientid = _clienteId;  //Puedes sacarlo del Id de estacion de trabajo, algun identificador del cliente o mismo Companyid
                    pin.Companyid = Properties.Settings.Default.IdCompany_BP;  //Biometrika o un ID valido segun sea la empresa dada de alta en BioPortal
                    pin.Enduser = _endUser; //identificador de operador, quiza el ID Sensor
                    pin.Ipenduser = _ipUser; //ip estación de trabajo
                    pin.Matchingtype = 1;
                    pin.Origin = Properties.Settings.Default.OriginBP; //Este valor debemos definirlo. Es el Origen de la verificación, por lo que debemos definir 
                                                                       //Ese valor fijo en el BioPortal. Por ahora configuralo en 1 = Desconocido (Pero configurable)   

                    pin.SampleCollection = new List<Sample>();
                    Sample sample = new Sample();
                    string txtToken;
                    int err = GENERATOR.GenerateTokenCI("RUT", _rut, 
                                                        _serialIdSensor, 
                                                        _fingerId, 
                                                        _sample,
                                                        _pdf417,
                                                        Properties.Settings.Default.TypeDevice,  //2 es secugen, pero que quede configurable
                                                        out txtToken);
                    sample.Data = txtToken;
                    sample.Minutiaetype = 24; //Token
                    sample.Additionaldata = null;
                    pin.SampleCollection.Add(sample);

                    pin.SaveVerified = 1;
                    pin.Threshold = 1000; //Definido por NEC como mínimo
                    pin.Userid = 0;
                    pin.Verifybyconnectorid = "DMANec"; //Fijo
                    pin.InsertOption = 2;  //No
                    pin.PersonalData = new PersonalData();
                    pin.PersonalData.Typeid = "RUT";
                    pin.PersonalData.Valueid = _rut;
                    pin.OperationOrder = 3;  //Solo Remoto, para que chequee contra el DMA

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);

                    //System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\tmp\xmlNew.xml");
                    //sw.Write(xmlparamin);
                    //sw.Close();


                    _ret_actual = target.Verify(xmlparamin, out xmlparamout);
                }

                //Si el retorno = 0 => Está ok la ejecución, y hay que ver que resultado de verificación dio
                if (_ret_actual == 0) { //Ejecuto ok, deserializopara usar la info
                    XmlParamOut paramout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                    _matchok = (paramout.Result == 1) ;//  "Positiva" = 1 : "Negativa" = 2);
                    _score = paramout.Score;

                }
            }
            catch (Exception ex)
            {
                _ret_actual = -1;
                //LOG - Agregar para tner log de errores
            }
            return _ret_actual;
        }

        internal static int GetISOCompactFromBioPortal(string _sampleISO, out string _sampleISOCompact)
        {
            int _ret_actual;
            string xmlparamout = string.Empty;
            _sampleISOCompact = null;

            try
            {
                using (BioPortalServerUtils.Bio_Portal_Server_Utils target = new BioPortalServerUtils.Bio_Portal_Server_Utils())
                {
                    target.Timeout = 60000;
                    target.Url = Properties.Settings.Default.Bio_Core_TokenCI050101_Gen_Test_BioPortalServerUtils_Bio_Portal_Server_Utils;

                    _ret_actual = target.ISOToISOC(_sampleISO, out _sampleISOCompact);
                }
            }
            catch (Exception ex)
            {
                _ret_actual = -1;
                //LOG - Agregar para tner log de errores
            }
            return _ret_actual;
        }

    }
}
