﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BioPortal.Server.Api;

namespace Bio.Core.TokenCI050101.Gen.Test
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            //try
            //{
            //    System.IO.StreamReader sr = new System.IO.StreamReader(@"c:\tmp\xmlEnviado.xml");
            //    string sxml = sr.ReadToEnd();
            //    sr.Close();

            //    XmlParamIn oXml = XmlUtils.DeserializeObject<XmlParamIn>(sxml); 
            //}
            //catch (Exception ex)
            //{
            //    string s = ex.Message;
            //}

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
