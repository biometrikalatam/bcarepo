﻿namespace Bio.Core.TokenCI050101.Gen.Test
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.button12 = new System.Windows.Forms.Button();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rtxSample = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rtxPDF417 = new System.Windows.Forms.RichTextBox();
            this.chkTypeSample = new System.Windows.Forms.CheckBox();
            this.rtxResult = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRUT = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Green;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Location = new System.Drawing.Point(521, 398);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(258, 36);
            this.button12.TabIndex = 101;
            this.button12.Text = "Consulta a BioPortal NEC";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(95, 31);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(382, 20);
            this.txtURL.TabIndex = 102;
            this.txtURL.Text = "http://localhost:4030/BioPortal.Server.WS.WEB.asmx";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 103;
            this.label1.Text = "URL WS:";
            // 
            // rtxSample
            // 
            this.rtxSample.Location = new System.Drawing.Point(12, 83);
            this.rtxSample.Name = "rtxSample";
            this.rtxSample.Size = new System.Drawing.Size(767, 180);
            this.rtxSample.TabIndex = 104;
            this.rtxSample.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 105;
            this.label2.Text = "Sample";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(29, 266);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 107;
            this.label3.Text = "PDF417";
            // 
            // rtxPDF417
            // 
            this.rtxPDF417.Location = new System.Drawing.Point(12, 282);
            this.rtxPDF417.Name = "rtxPDF417";
            this.rtxPDF417.Size = new System.Drawing.Size(767, 110);
            this.rtxPDF417.TabIndex = 106;
            this.rtxPDF417.Text = resources.GetString("rtxPDF417.Text");
            // 
            // chkTypeSample
            // 
            this.chkTypeSample.AutoSize = true;
            this.chkTypeSample.Location = new System.Drawing.Point(83, 66);
            this.chkTypeSample.Name = "chkTypeSample";
            this.chkTypeSample.Size = new System.Drawing.Size(67, 17);
            this.chkTypeSample.TabIndex = 108;
            this.chkTypeSample.Text = "Es RAW";
            this.chkTypeSample.UseVisualStyleBackColor = true;
            // 
            // rtxResult
            // 
            this.rtxResult.Location = new System.Drawing.Point(13, 450);
            this.rtxResult.Name = "rtxResult";
            this.rtxResult.Size = new System.Drawing.Size(767, 110);
            this.rtxResult.TabIndex = 109;
            this.rtxResult.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(537, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 111;
            this.label4.Text = "RUT:";
            // 
            // txtRUT
            // 
            this.txtRUT.Location = new System.Drawing.Point(580, 31);
            this.txtRUT.Name = "txtRUT";
            this.txtRUT.Size = new System.Drawing.Size(80, 20);
            this.txtRUT.TabIndex = 110;
            this.txtRUT.Text = "21284415-2";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkBlue;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(13, 398);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(318, 36);
            this.button1.TabIndex = 112;
            this.button1.Text = "Consulta a BioPortal Innovatrics";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(689, 54);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(79, 25);
            this.button2.TabIndex = 113;
            this.button2.Text = "Fill ISO";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 617);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtRUT);
            this.Controls.Add(this.rtxResult);
            this.Controls.Add(this.chkTypeSample);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.rtxPDF417);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rtxSample);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtURL);
            this.Controls.Add(this.button12);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Verify DMA via BioPortal...";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox rtxSample;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox rtxPDF417;
        private System.Windows.Forms.CheckBox chkTypeSample;
        private System.Windows.Forms.RichTextBox rtxResult;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRUT;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;

    }
}

