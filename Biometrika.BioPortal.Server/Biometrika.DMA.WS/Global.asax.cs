﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using log4net;
using log4net.Config;

namespace Biometrika.DMA.WS
{
    public class Global : System.Web.HttpApplication
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Global));

        internal static Biometrika.NEC.BiometrikaNEC oNEC = new NEC.BiometrikaNEC();

        protected void Application_Start(object sender, EventArgs e)
        {
            oNEC._PATHDEBUG = "\\log\\Biometrika.NEC.log";
            oNEC._DEBUG = true;
            //0.- Inicializo LOG
            XmlConfigurator.Configure(
                new FileInfo(Properties.Settings.Default.PathConfigLog));
            LOG.Info("Biometrika.DMA.WS Starting...");
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}