﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using log4net;

namespace Biometrika.DMA.WS
{
    /// <summary>
    /// Summary description for Biometrika_DMA_WS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Biometrika_DMA_WS : System.Web.Services.WebService
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(Biometrika_DMA_WS));

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public int Matching(string _pdf417Cedula, string _sample, int _typesample, out string msgerr)
        {
            int _ret = 0;
            msgerr = null;
            string _sampleProcesed;
            try
            {
                string strDatos;

                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL oNEC.ParsePDF417 Entrando...");
                Biometrika.NEC.Result _result1 = Global.oNEC.ParsePDF417(_pdf417Cedula, out strDatos);
                if (_result1.Code != 0)
                {
                    msgerr = "Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL Error oNEC.ParsePDF417 = " + _result1.Code + " - Msg = " + _result1.Message;
                    LOG.Error(msgerr);
                    return -2;  
                }
                else
                {
                    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL oNEC.ParsePDF417 OK!");
                }
                //PArseopara sacar minucias de PDF417
                string[] strArr = _result1.Message.Split(new string[1] { "|" }, StringSplitOptions.None);

                //Chequear que minucias está ok sino error -3
                if (strArr.Equals(null) || strArr[9].Equals(null) || strArr[9].Length == 0)
                {
                    msgerr = "Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL Error oNEC.ParsePDF417 Minucias extraidas de la Cedula = Null";
                    LOG.Error(msgerr);
                    return -3; 
                }
                else
                {
                    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL oNEC.ParsePDF417 Minucias = " + strArr[9]);
                }

                if (_typesample == 1) //Es RAW
                {
                    _sampleProcesed = _sample;
                }
                else //Es WSQ
                {
                    _sampleProcesed = DecompressWSQ(_sample);
                }
                //Chequear que minucias está ok sino error -3
                if (_sampleProcesed.Equals(null) || _sampleProcesed.Length == 0)
                {
                    msgerr = "Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL Error Sample Processed = Null";
                    LOG.Error(msgerr);
                    return -4;
                }
                else
                {
                    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL Sample Processed OK = " + _sampleProcesed);
                }

                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL oNEC.Extract Entrando...");
                Biometrika.NEC.Result _result = Global.oNEC.Extract(_sampleProcesed, 1, 10);
                if (_result.Code != 0)
                {
                    msgerr = "Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL Error oNEC.Extract = " + _result.Code + " - Msg = " + _result.Message;
                    LOG.Error(msgerr);
                    return -5;  
                }
                else
                    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL oNEC.Extract OK!");

                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL oNEC.Matching Entrando...");
                _result = null;
                _result = Global.oNEC.Matching(_result.Minutiae1, strArr[9], 10);
                if (_result.Code != 0)
                {
                    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL Error oNEC.Matching = " + _result.Code + " - Msg = " + _result.Message);
                    _ret = _result.Score;
                }
                else
                {
                    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL oNEC.Matching OK! Result Score = " + _result.ToString());
                    _ret = _result.Code;
                }
            }
            catch (Exception ex)
            {
                msgerr = "Bio.Core.Matcher.Connector.NecDMA.VerifyInternoDLL Error EX - Msg=" + ex.Message + " - Stack=" + ex.StackTrace;
                msgerr = msgerr + " - [len pdf=" + (_pdf417Cedula.Equals(null)?"null":_pdf417Cedula.Length.ToString()) +
                                  " - len raw= " + (_sample.Equals(null) ? "null" : _sample.Length.ToString()) + "]";
                LOG.Debug(msgerr);
                _ret = -1;
            }
            return _ret;
        }

        private string DecompressWSQ(string _sample)
        {
            return _sample;
        }


        //[WebMethod]
        //public int Extract(string _sample, int _typesample, out string msgerr)
        //{
        //    Array _aRawGenerated;
        //    Array _aPdf417Cedula;
        //    Array _aPc1Generated = new byte[400];
        //    int _pcLenGenerated = 0;
        //    Array _aPc1Cedula;
        //    int _pcLenAux = 0;
        //    int _ret = 0;

        //    msgerr = "";

        //    try
        //    {
        //        _aRawGenerated = Convert.FromBase64String(_sample);

        //        BiometrikaAxNEC.BiometrikaAxWNEC BKAx = new BiometrikaAxNEC.BiometrikaAxWNEC();
        //        msgerr = BKAx.Extract(ref _sample);
        //        _ret = 0;

        //    }
        //    catch (Exception ex)
        //    {
        //        msgerr = ex.Message;
        //        _ret = -1;
        //    }

        //    return _ret;
        //}
    }
}
