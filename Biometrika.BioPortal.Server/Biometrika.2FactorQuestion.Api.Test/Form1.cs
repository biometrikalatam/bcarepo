﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Biometrika._2FactorQuestion.Api
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (Test.TwoFactorService.IDV_Execute service = new Test.TwoFactorService.IDV_Execute())
            {
                DCRequest _DCRequest = new DCRequest();
                _DCRequest.Authentication = new Authentication();
                _DCRequest.Authentication.UserId = "IDV.BIOMETRIKADEMO";
                _DCRequest.Authentication.Password = "B1ometrik@2021";
                _DCRequest.RequestInfo = new RequestInfo();
                _DCRequest.RequestInfo.ExecutionMode = "NewWithContext";
                _DCRequest.Fields = new Fields();
                _DCRequest.Fields.Field = new List<Field>();
                Field _Field = new Field();
                _Field.Key = "Rut";
                _Field.Text = textBox1.Text;
                _DCRequest.Fields.Field.Add(_Field);
                _Field = new Field();
                _Field.Key = "NoSerie";
                _Field.Text = textBox2.Text;
                _DCRequest.Fields.Field.Add(_Field);

                _Field = new Field();
                _Field.Key = "InquirySource";
                _Field.Text = "IDV-WEBSERVICE";
                _DCRequest.Fields.Field.Add(_Field);
                _Field = new Field();
                _Field.Key = "TipoDocumento";
                _Field.Text = "C";
                _DCRequest.Fields.Field.Add(_Field);
                _Field = new Field();
                _Field.Key = "SoloValidarIdentidad";
                _Field.Text = "true";
                _DCRequest.Fields.Field.Add(_Field);
                _Field = new Field();
                _Field.Key = "TipoConsulta";
                _Field.Text = "Cliente Nuevo";
                _DCRequest.Fields.Field.Add(_Field);

                string xmlin = Bio.Core.Utils.XmlUtils.SerializeAnObject(_DCRequest);
                xmlin = xmlin.Substring(xmlin.IndexOf("<DCRequest>"));
                XmlNode xmlout = service.RetrieveExam(xmlin);

                DCResponse DCResponse = Bio.Core.Utils.XmlUtils.DeserializeObject<DCResponse>(xmlout.OuterXml);

                richTextBox1.Text = DCResponse.Status;

            }
        }
    }
}
