﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Biometrika._2FactorQuestion.Api
{
    public class Models
    {
    }

    // using System.Xml.Serialization;
    // XmlSerializer serializer = new XmlSerializer(typeof(DCRequest));
    // using (StringReader reader = new StringReader(xml))
    // {
    //    var test = (DCRequest)serializer.Deserialize(reader);
    // }

    [XmlRoot(ElementName = "Authentication")]
    public class Authentication
    {

        [XmlElement(ElementName = "UserId")]
        public string UserId { get; set; }

        [XmlElement(ElementName = "Password")]
        public string Password { get; set; }

        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "RequestInfo")]
    public class RequestInfo
    {

        [XmlElement(ElementName = "ExecutionMode")]
        public string ExecutionMode { get; set; }
    }

    [XmlRoot(ElementName = "Field")]
    public class Field
    {

        [XmlAttribute(AttributeName = "key")]
        public string Key { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Fields")]
    public class Fields
    {

        [XmlElement(ElementName = "Field")]
        public List<Field> Field { get; set; }
    }

    [XmlRoot(ElementName = "DCRequest")]
    public class DCRequest
    {

        [XmlElement(ElementName = "Authentication")]
        public Authentication Authentication { get; set; }

        [XmlElement(ElementName = "RequestInfo")]
        public RequestInfo RequestInfo { get; set; }

        [XmlElement(ElementName = "Fields")]
        public Fields Fields { get; set; }
    }

    // OUT -------------------------------------------------------------
    // using System.Xml.Serialization;
    // XmlSerializer serializer = new XmlSerializer(typeof(Root));
    // using (StringReader reader = new StringReader(xml))
    // {
    //    var test = (Root)serializer.Deserialize(reader);
    // }

    [XmlRoot(ElementName = "Authentication")]
    public class AuthenticationOut
    {

        [XmlElement(ElementName = "Status")]
        public string Status { get; set; }

        [XmlElement(ElementName = "Token")]
        public string Token { get; set; }
    }

    [XmlRoot(ElementName = "ResponseInfo")]
    public class ResponseInfo
    {

        [XmlElement(ElementName = "ApplicationId")]
        public int ApplicationId { get; set; }

        [XmlElement(ElementName = "SolutionSetInstanceId")]
        public string SolutionSetInstanceId { get; set; }

        [XmlElement(ElementName = "CurrentQueue")]
        public string CurrentQueue { get; set; }
    }

    [XmlRoot(ElementName = "Field")]
    public class FieldOut
    {

        [XmlAttribute(AttributeName = "key")]
        public string Key { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "ContextData")]
    public class ContextData
    {

        [XmlElement(ElementName = "Field")]
        public List<FieldOut> Field { get; set; }
    }

    [XmlRoot(ElementName = "DCResponse")]
    public class DCResponse
    {

        [XmlElement(ElementName = "Status")]
        public string Status { get; set; }

        [XmlElement(ElementName = "Authentication")]
        public AuthenticationOut Authentication { get; set; }

        [XmlElement(ElementName = "ResponseInfo")]
        public ResponseInfo ResponseInfo { get; set; }

        [XmlElement(ElementName = "ContextData")]
        public ContextData ContextData { get; set; }
    }
}
