﻿namespace Bio.Portal.Server.Services.GUITest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtValueID = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button11 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTypeId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtValueId2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAF = new System.Windows.Forms.TextBox();
            this.btnSetAx = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDevice = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTokenContent = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtOT = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMT = new System.Windows.Forms.TextBox();
            this.chkTokenComponent = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtOO = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtConnectorId = new System.Windows.Forms.TextBox();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button22 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtScore = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.btnCapture = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.txtPSW = new System.Windows.Forms.TextBox();
            this.button21 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.txtTH = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button26 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.chkAddSample = new System.Windows.Forms.CheckBox();
            //this.ubpClient1 = new Bio.Portal.Client.v5.UBPClient();
            this.button27 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(368, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Test Verify WS";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(36, 97);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(404, 153);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(487, 39);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(126, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Test Enroll WS";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(622, 37);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(132, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Test Identify WS";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(74, 39);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(80, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.Text = "9";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(160, 36);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(112, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "TestGET WS";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(65, 285);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(140, 154);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(24, 445);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(230, 108);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(74, 10);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(80, 20);
            this.txtID.TabIndex = 8;
            this.txtID.Text = "RUT";
            // 
            // txtValueID
            // 
            this.txtValueID.Location = new System.Drawing.Point(160, 10);
            this.txtValueID.Name = "txtValueID";
            this.txtValueID.Size = new System.Drawing.Size(112, 20);
            this.txtValueID.TabIndex = 9;
            this.txtValueID.Text = "21284415-4";
            this.txtValueID.TextChanged += new System.EventHandler(this.txtValueID_TextChanged);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(368, 8);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(112, 23);
            this.button5.TabIndex = 11;
            this.button5.Text = "Test Verify WS Web";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(622, 8);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(132, 23);
            this.button6.TabIndex = 12;
            this.button6.Text = "Test Identify WS WEB";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(489, 8);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(126, 23);
            this.button7.TabIndex = 13;
            this.button7.Text = "Test Enroll WS Web";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(377, 68);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(125, 23);
            this.button8.TabIndex = 14;
            this.button8.Text = "Test Verify WS w/VF";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(476, 97);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(404, 135);
            this.richTextBox2.TabIndex = 15;
            this.richTextBox2.Text = "";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(160, 68);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(86, 23);
            this.button9.TabIndex = 16;
            this.button9.Text = "Test Get Op";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(508, 68);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(126, 23);
            this.button10.TabIndex = 17;
            this.button10.Text = "Test Modify WS";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Oper";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Company";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(74, 68);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(80, 20);
            this.textBox2.TabIndex = 19;
            this.textBox2.Text = "7";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(748, 71);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(132, 23);
            this.button11.TabIndex = 21;
            this.button11.Text = "Certificado";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(335, 383);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "TypeID";
            // 
            // txtTypeId
            // 
            this.txtTypeId.Location = new System.Drawing.Point(383, 380);
            this.txtTypeId.Name = "txtTypeId";
            this.txtTypeId.Size = new System.Drawing.Size(80, 20);
            this.txtTypeId.TabIndex = 23;
            this.txtTypeId.Text = "RUT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(335, 409);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "ValueID";
            // 
            // txtValueId2
            // 
            this.txtValueId2.Location = new System.Drawing.Point(383, 406);
            this.txtValueId2.Name = "txtValueId2";
            this.txtValueId2.Size = new System.Drawing.Size(80, 20);
            this.txtValueId2.TabIndex = 25;
            this.txtValueId2.Text = "21284415-2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(357, 435);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "AF";
            // 
            // txtAF
            // 
            this.txtAF.Location = new System.Drawing.Point(383, 432);
            this.txtAF.Name = "txtAF";
            this.txtAF.Size = new System.Drawing.Size(80, 20);
            this.txtAF.TabIndex = 27;
            this.txtAF.Text = "2";
            // 
            // btnSetAx
            // 
            this.btnSetAx.Location = new System.Drawing.Point(360, 458);
            this.btnSetAx.Name = "btnSetAx";
            this.btnSetAx.Size = new System.Drawing.Size(112, 23);
            this.btnSetAx.TabIndex = 29;
            this.btnSetAx.Text = "Set >>>";
            this.btnSetAx.UseVisualStyleBackColor = true;
            this.btnSetAx.Click += new System.EventHandler(this.btnSetAx_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(328, 507);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(144, 23);
            this.button12.TabIndex = 30;
            this.button12.Text = "Valida Token en WS Web";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(335, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Device";
            // 
            // txtDevice
            // 
            this.txtDevice.Location = new System.Drawing.Point(383, 269);
            this.txtDevice.Name = "txtDevice";
            this.txtDevice.Size = new System.Drawing.Size(80, 20);
            this.txtDevice.TabIndex = 31;
            this.txtDevice.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(335, 298);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "Model";
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(383, 295);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(80, 20);
            this.txtModel.TabIndex = 33;
            this.txtModel.Text = "FDU05";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(318, 324);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 36;
            this.label8.Text = "Tipo Token";
            // 
            // txtTokenContent
            // 
            this.txtTokenContent.Location = new System.Drawing.Point(383, 321);
            this.txtTokenContent.Name = "txtTokenContent";
            this.txtTokenContent.Size = new System.Drawing.Size(80, 20);
            this.txtTokenContent.TabIndex = 35;
            this.txtTokenContent.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(297, 350);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 38;
            this.label9.Text = "Operation Type";
            // 
            // txtOT
            // 
            this.txtOT.Location = new System.Drawing.Point(383, 347);
            this.txtOT.Name = "txtOT";
            this.txtOT.Size = new System.Drawing.Size(80, 20);
            this.txtOT.TabIndex = 37;
            this.txtOT.Text = "1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(223, 272);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 40;
            this.label10.Text = "MT";
            // 
            // txtMT
            // 
            this.txtMT.Location = new System.Drawing.Point(249, 269);
            this.txtMT.Name = "txtMT";
            this.txtMT.Size = new System.Drawing.Size(80, 20);
            this.txtMT.TabIndex = 39;
            this.txtMT.Text = "13";
            // 
            // chkTokenComponent
            // 
            this.chkTokenComponent.AutoSize = true;
            this.chkTokenComponent.Location = new System.Drawing.Point(315, 487);
            this.chkTokenComponent.Name = "chkTokenComponent";
            this.chkTokenComponent.Size = new System.Drawing.Size(157, 17);
            this.chkTokenComponent.TabIndex = 42;
            this.chkTokenComponent.Text = "Usa Token de Componente";
            this.chkTokenComponent.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(206, 310);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 13);
            this.label11.TabIndex = 44;
            this.label11.Text = "Operation Order";
            // 
            // txtOO
            // 
            this.txtOO.Location = new System.Drawing.Point(209, 326);
            this.txtOO.Name = "txtOO";
            this.txtOO.Size = new System.Drawing.Size(80, 20);
            this.txtOO.TabIndex = 43;
            this.txtOO.Text = "1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(208, 367);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 46;
            this.label12.Text = "ConnectorId";
            // 
            // txtConnectorId
            // 
            this.txtConnectorId.Location = new System.Drawing.Point(211, 383);
            this.txtConnectorId.Name = "txtConnectorId";
            this.txtConnectorId.Size = new System.Drawing.Size(100, 20);
            this.txtConnectorId.TabIndex = 45;
            this.txtConnectorId.Text = "NONE";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(476, 238);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(112, 23);
            this.button13.TabIndex = 47;
            this.button13.Text = "Test Get Company";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(249, 68);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(93, 23);
            this.button14.TabIndex = 48;
            this.button14.Text = "TestGET Sync";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(24, 256);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(93, 23);
            this.button15.TabIndex = 49;
            this.button15.Text = "Borrar ^^";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(637, 238);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(126, 23);
            this.button16.TabIndex = 50;
            this.button16.Text = "Test Enroll WS Web";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button22);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtScore);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtResult);
            this.groupBox1.Controls.Add(this.btnCapture);
            this.groupBox1.Controls.Add(this.button19);
            this.groupBox1.Controls.Add(this.pictureBox3);
            this.groupBox1.Controls.Add(this.button18);
            this.groupBox1.Controls.Add(this.button17);
            this.groupBox1.Location = new System.Drawing.Point(899, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(261, 476);
            this.groupBox1.TabIndex = 51;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Facial";
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(28, 399);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(205, 23);
            this.button22.TabIndex = 60;
            this.button22.Text = "Test Verify WS F7 x Token";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 369);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 59;
            this.label15.Text = "Score";
            // 
            // txtScore
            // 
            this.txtScore.Location = new System.Drawing.Point(86, 366);
            this.txtScore.Name = "txtScore";
            this.txtScore.Size = new System.Drawing.Size(132, 20);
            this.txtScore.TabIndex = 58;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(25, 331);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 57;
            this.label14.Text = "Resultado";
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(86, 328);
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(132, 20);
            this.txtResult.TabIndex = 56;
            // 
            // btnCapture
            // 
            this.btnCapture.Location = new System.Drawing.Point(14, 290);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(115, 23);
            this.btnCapture.TabIndex = 55;
            this.btnCapture.Text = "Captura Foto";
            this.btnCapture.UseVisualStyleBackColor = true;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(14, 228);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(115, 23);
            this.button19.TabIndex = 54;
            this.button19.Text = "Load Image...";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(11, 24);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(232, 201);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 53;
            this.pictureBox3.TabStop = false;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(135, 257);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(108, 23);
            this.button18.TabIndex = 52;
            this.button18.Text = "Test Verify WS F7";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(14, 257);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(115, 23);
            this.button17.TabIndex = 51;
            this.button17.Text = "Test Enroll WS F7";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(779, 7);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(86, 23);
            this.button20.TabIndex = 52;
            this.button20.Text = "Test Password";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // txtPSW
            // 
            this.txtPSW.Location = new System.Drawing.Point(779, 34);
            this.txtPSW.Name = "txtPSW";
            this.txtPSW.Size = new System.Drawing.Size(80, 20);
            this.txtPSW.TabIndex = 53;
            this.txtPSW.Text = "gsuhit";
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(871, 7);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(129, 23);
            this.button21.TabIndex = 54;
            this.button21.Text = "Test Password c/Token";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(24, 576);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(456, 20);
            this.txtURL.TabIndex = 56;
            this.txtURL.Text = "http://localhost:4030/BioPortal.Server.WS.WEB.asmx";
            // 
            // txtTH
            // 
            this.txtTH.Location = new System.Drawing.Point(383, 536);
            this.txtTH.Name = "txtTH";
            this.txtTH.Size = new System.Drawing.Size(80, 20);
            this.txtTH.TabIndex = 57;
            this.txtTH.Text = "100000";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(331, 539);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 58;
            this.label13.Text = "Treshold";
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(833, 559);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(126, 23);
            this.button23.TabIndex = 60;
            this.button23.Text = "Test AditionalData";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(336, 674);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(144, 23);
            this.button24.TabIndex = 63;
            this.button24.Text = "Valida Token en WS Web";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(368, 625);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(112, 23);
            this.button25.TabIndex = 62;
            this.button25.Text = "Set >>>";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button26);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.richTextBox4);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.richTextBox3);
            this.groupBox2.Location = new System.Drawing.Point(1001, 536);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(355, 259);
            this.groupBox2.TabIndex = 65;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(122, 105);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(126, 23);
            this.button26.TabIndex = 66;
            this.button26.Text = "Decrypt TripleDES";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(25, 117);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 68;
            this.label17.Text = "Decrypted";
            // 
            // richTextBox4
            // 
            this.richTextBox4.Location = new System.Drawing.Point(15, 134);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.Size = new System.Drawing.Size(312, 67);
            this.richTextBox4.TabIndex = 67;
            this.richTextBox4.Text = "AV040101EZCfpr0/QAeL8T7cnW9RvxP78ijqt92Au4OVqm0f1JNvkhgg9s4z6WwM12YOfTqKS9BMbOA9Q" +
    "wk1RcwlDW/aK6ObagTIMphx7PpIOsNUAYk=";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(25, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 66;
            this.label16.Text = "Crypted";
            // 
            // richTextBox3
            // 
            this.richTextBox3.Location = new System.Drawing.Point(15, 40);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(312, 67);
            this.richTextBox3.TabIndex = 65;
            this.richTextBox3.Text = "FcvE9u5raKUSAXdUuyaWrhmn8pKRimA092dYul1sNY0=";
            // 
            // chkAddSample
            // 
            this.chkAddSample.AutoSize = true;
            this.chkAddSample.Checked = true;
            this.chkAddSample.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAddSample.Location = new System.Drawing.Point(209, 422);
            this.chkAddSample.Name = "chkAddSample";
            this.chkAddSample.Size = new System.Drawing.Size(83, 17);
            this.chkAddSample.TabIndex = 66;
            this.chkAddSample.Text = "Add Sample";
            this.chkAddSample.UseVisualStyleBackColor = true;
            // 
            // ubpClient1
            // 
            //this.ubpClient1.AuthenticationFactor = 2;
            //this.ubpClient1.BackColor = System.Drawing.Color.White;
            //this.ubpClient1.DEBUG = false;
            //this.ubpClient1.Device = 1;
            //this.ubpClient1.Finger = 1;
            //this.ubpClient1.Location = new System.Drawing.Point(489, 267);
            //this.ubpClient1.Minutiaetype = 4;
            //this.ubpClient1.Model = "4500";
            //this.ubpClient1.Name = "ubpClient1";
            //this.ubpClient1.Operationtype = 1;
            //this.ubpClient1.PATHDEBUG = "c:\\";
            //this.ubpClient1.Size = new System.Drawing.Size(290, 301);
            //this.ubpClient1.TabIndex = 59;
            //this.ubpClient1.Timeout = 3000;
            //this.ubpClient1.Tokencontent = 0;
            //this.ubpClient1.Typeid = "RUT";
            //this.ubpClient1.Umbralcalidad = 400;
            //this.ubpClient1.Valueid = null;
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(820, 518);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(126, 23);
            this.button27.TabIndex = 67;
            this.button27.Text = "Get Token";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1172, 876);
            this.Controls.Add(this.button27);
            this.Controls.Add(this.chkAddSample);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.button23);
            //this.Controls.Add(this.ubpClient1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtTH);
            this.Controls.Add(this.txtURL);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.txtPSW);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtConnectorId);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtOO);
            this.Controls.Add(this.chkTokenComponent);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtMT);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtOT);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtTokenContent);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtModel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtDevice);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.btnSetAx);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAF);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtValueId2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTypeId);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.txtValueID);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.GUITest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.TextBox txtValueID;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button11;
        //private AxBPClientP.AxBPClientAx axBPClientAx1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTypeId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtValueId2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAF;
        private System.Windows.Forms.Button btnSetAx;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDevice;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTokenContent;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtOT;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMT;
        private System.Windows.Forms.CheckBox chkTokenComponent;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtOO;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtConnectorId;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.TextBox txtPSW;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnCapture;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.TextBox txtTH;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtScore;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Button button22;
        //private Client.v5.UBPClient ubpClient1;
        private System.Windows.Forms.Button button23;
        //private AxBPClient.AxBPClientAx axBPClientAx1;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.CheckBox chkAddSample;
        private System.Windows.Forms.Button button27;
    }
}

