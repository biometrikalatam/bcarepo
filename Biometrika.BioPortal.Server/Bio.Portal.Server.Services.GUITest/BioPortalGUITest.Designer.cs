﻿namespace Bio.Portal.Server.Services.GUITest
{
    partial class BioPortalGUITest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BioPortalGUITest));
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rtbResult = new System.Windows.Forms.RichTextBox();
            this.btnDeleteResult = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.ubpClient1 = new Bio.Portal.Client.v5.UBPClient();
            this.btnSet = new System.Windows.Forms.Button();
            this.txtValueID = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.txtCompanyId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // picLogo
            // 
            this.picLogo.Image = global::Bio.Portal.Server.Services.GUITest.Properties.Resources.biometrika;
            this.picLogo.Location = new System.Drawing.Point(338, 12);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(400, 64);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picLogo.TabIndex = 0;
            this.picLogo.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtValueID);
            this.groupBox1.Controls.Add(this.txtID);
            this.groupBox1.Controls.Add(this.btnSet);
            this.groupBox1.Controls.Add(this.ubpClient1);
            this.groupBox1.Location = new System.Drawing.Point(22, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(579, 502);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ingreso de Datos...";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtCompanyId);
            this.groupBox2.Location = new System.Drawing.Point(620, 95);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(349, 254);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Operaciones Posibles...";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rtbResult);
            this.groupBox3.Controls.Add(this.btnDeleteResult);
            this.groupBox3.Location = new System.Drawing.Point(620, 355);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(349, 275);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Resultados...";
            // 
            // rtbResult
            // 
            this.rtbResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.rtbResult.Location = new System.Drawing.Point(7, 20);
            this.rtbResult.Name = "rtbResult";
            this.rtbResult.Size = new System.Drawing.Size(331, 222);
            this.rtbResult.TabIndex = 0;
            this.rtbResult.Text = "";
            // 
            // btnDeleteResult
            // 
            this.btnDeleteResult.Location = new System.Drawing.Point(263, 248);
            this.btnDeleteResult.Name = "btnDeleteResult";
            this.btnDeleteResult.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteResult.TabIndex = 1;
            this.btnDeleteResult.Text = "&Borrar";
            this.btnDeleteResult.UseVisualStyleBackColor = true;
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(287, 607);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 4;
            this.btnSalir.Text = "&Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            // 
            // ubpClient1
            // 
            this.ubpClient1.AuthenticationFactor = 2;
            this.ubpClient1.BackColor = System.Drawing.Color.White;
            this.ubpClient1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ubpClient1.DEBUG = false;
            this.ubpClient1.Device = 1;
            this.ubpClient1.Finger = 1;
            this.ubpClient1.Location = new System.Drawing.Point(280, 19);
            this.ubpClient1.Minutiaetype = 4;
            this.ubpClient1.Model = "4500";
            this.ubpClient1.Name = "ubpClient1";
            this.ubpClient1.Operationtype = 1;
            this.ubpClient1.PATHDEBUG = "c:\\";
            this.ubpClient1.Size = new System.Drawing.Size(290, 299);
            this.ubpClient1.TabIndex = 0;
            this.ubpClient1.Timeout = 3000;
            this.ubpClient1.Tokencontent = 0;
            this.ubpClient1.Typeid = "RUT";
            this.ubpClient1.Umbralcalidad = 400;
            this.ubpClient1.Valueid = null;
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(220, 19);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(54, 23);
            this.btnSet.TabIndex = 5;
            this.btnSet.Text = "Set >>";
            this.btnSet.UseVisualStyleBackColor = true;
            // 
            // txtValueID
            // 
            this.txtValueID.Location = new System.Drawing.Point(146, 48);
            this.txtValueID.Name = "txtValueID";
            this.txtValueID.Size = new System.Drawing.Size(112, 20);
            this.txtValueID.TabIndex = 11;
            this.txtValueID.Text = "21284415-2";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(95, 48);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(45, 20);
            this.txtID.TabIndex = 10;
            this.txtID.Text = "RUT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "TypeId/ValueId";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(95, 103);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(80, 20);
            this.textBox2.TabIndex = 21;
            this.textBox2.Text = "9";
            // 
            // txtCompanyId
            // 
            this.txtCompanyId.Location = new System.Drawing.Point(84, 21);
            this.txtCompanyId.Name = "txtCompanyId";
            this.txtCompanyId.Size = new System.Drawing.Size(45, 20);
            this.txtCompanyId.TabIndex = 20;
            this.txtCompanyId.Text = "9";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Company Id";
            // 
            // BioPortalGUITest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 732);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.picLogo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BioPortalGUITest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BioPortal Testing Example...";
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox rtbResult;
        private System.Windows.Forms.Button btnDeleteResult;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnSet;
        //private Client.v5.UBPClient ubpClient1;
        private System.Windows.Forms.TextBox txtValueID;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCompanyId;
    }
}