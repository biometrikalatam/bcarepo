﻿namespace Bio.Portal.Server.Services.GUITest
{
    partial class frmCaptureFoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.bkFotoNetCtrl1 = new BKFotoNet.BKFotoNetCtrl();
            this.SuspendLayout();
            // 
            // bkFotoNetCtrl1
            // 
            //this.bkFotoNetCtrl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            //this.bkFotoNetCtrl1.Location = new System.Drawing.Point(-4, -1);
            //this.bkFotoNetCtrl1.Name = "bkFotoNetCtrl1";
            //this.bkFotoNetCtrl1.Size = new System.Drawing.Size(728, 552);
            //this.bkFotoNetCtrl1.TabIndex = 0;
            // 
            // frmCaptureFoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 573);
            //this.Controls.Add(this.bkFotoNetCtrl1);
            this.Name = "frmCaptureFoto";
            this.Text = "frmCaptureFoto";
            this.Load += new System.EventHandler(this.frmCaptureFoto_Load);
            this.ResumeLayout(false);

        }

        #endregion

        //private BKFotoNet.BKFotoNetCtrl bkFotoNetCtrl1;
    }
}