﻿using BioPortal.Server.Api.Json;
using BioPortal.Server.Api.Json.API;
using NUnit.Framework;
using System.Collections.Generic;

namespace Test_BioPortalServerServicesWebApi
{
    public class BaseTest
    {
        protected API API;
        [SetUp]
        public void Setup()
        {
            API = new API("http://localhost/BPSWebAPI/");
        }
    }
}
