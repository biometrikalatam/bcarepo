﻿using BioPortal.Server.Api.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_BioPortalServerServicesWebApi
{
    public class Test_APIWSWEB : BaseTest
    {
        [Test]
        public void Enroll()
        {
            XmlParamIn xmlParamIn = new XmlParamIn { };

            XmlParamOut response = API.WSWEBEnroll(xmlParamIn);
            Assert.IsNotNull(response);
        }

        [Test]
        public void Identify()
        {
            XmlParamIn xmlParamIn = new XmlParamIn { };

            XmlParamOut response = API.WSWEBIdentify(xmlParamIn);
            Assert.IsNotNull(response);
        }

        [Test]
        public void Verify()
        {
            XmlParamIn xmlParamIn = new XmlParamIn { };

            XmlParamOut response = API.WSWEBVerify(xmlParamIn);
            Assert.IsNotNull(response);
        }
    }
}
