﻿using Bio.Core.Api.Constant;
using BioPortal.Server.Api.Json;
using NUnit.Framework;
using System.Collections.Generic;

namespace Test_BioPortalServerServicesWebApi
{
    public class Test_APIWS : BaseTest
    {
        [Test]
        public void Enroll()
        {
            XmlParamIn xmlParamIn = new XmlParamIn
            {
                Actionid = Action.ACTION_ADDBIR,
                Additionaldata = null,
                Authenticationfactor = AuthenticationFactor.AUTHENTICATIONFACTOR_ANY,
                Minutiaetype = MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004,
                Bodypart = BodyPart.BODYPART_ALL,
                Clientid = "IdClienteTest",
                Companyid = 7,
                Enduser = "EnUserTest",
                Ipenduser = "127.0.0.1",
                Matchingtype = 1,
                Origin = 1,
                SampleCollection = new List<Sample>(),
                SaveVerified = 1,
                Threshold = 0,
                Userid = 0,
                Verifybyconnectorid = "final",
                InsertOption = 0,
                PersonalData = new PersonalData
                {
                    Typeid = "RUT",
                    Valueid = "16122431-6",
                    Name = "MATIAS ALEJANDRO",
                    Patherlastname = "AREVALO",
                    Motherlastname = "LIRA"
                },
                OperationOrder = OperationOrder.OPERATIONORDER_DEFAULT
            };

            Sample sample = new Sample
            {
                Data = "Rk1SACAyMAABOgAKADUAAAEsAZAAxQDFAQAAEFAvgHgAFFMAQPMAJp4AgIQANlYAgEAAOQQAgLMAOU8AgOUAPUoAgFsASwEAgKgAZFAAgHMAZVwAgOEAck4AQJYAdawAgJcAiqcAgFgAkmkAgJYApkwAgFQAqxIAgIYAq1cAgLQAtJsAgFEAu3UAgIwAwFYAgEMAxnwAQDUAzXkAQLoA3I4AgNgA35UAgIQA6JwAgDkA6oMAgLQA7nwAQM8A9ZoAQJEA90UAQC0A+oEAgIgA+0oAgMAA+0kAQMwA/jQAgMgBA1IAgOIBA0oAgH4BBKsAgNQBClYAgKoBElsAgPMBF1IAgCIBGw4AgOQBIFwAgEgBJkoAQGsBKlIAgM4BOWAAQHEBQlYAQIIBWFwAgK8BcmAAQIcBdgUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=",
                Minutiaetype = MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004,
                Additionaldata = null
            };

            xmlParamIn.SampleCollection.Add(sample);

            XmlParamOut response = API.WSEnroll(xmlParamIn);
            Assert.AreEqual(0, response.Score);
        }

        [Test]
        public void Identify()
        {
            XmlParamIn xmlParamIn = new XmlParamIn
            {
                Actionid = Action.ACTION_IDENTIFY,
                Additionaldata = null,
                Authenticationfactor = AuthenticationFactor.AUTHENTICATIONFACTOR_ANY,
                Minutiaetype = MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004,
                Bodypart = BodyPart.BODYPART_ALL,
                Clientid = "IdClienteTest",
                Companyid = 7,
                Enduser = "EnUserTest",
                Ipenduser = "127.0.0.1",
                Matchingtype = 1,
                Origin = 1,
                SampleCollection = new List<Sample>(),
                SaveVerified = 1,
                Threshold = 100000,
                Userid = 0,
                Verifybyconnectorid = "NONE",
                InsertOption = 0,
                PersonalData = new PersonalData
                {
                    Typeid = "RUT",
                    Valueid = "21284415-2"
                },
                OperationOrder = OperationOrder.OPERATIONORDER_LOCALONLY
            };

            Sample sample = new Sample
            {
                Data = "Rk1SACAyMAABagAz/v8AAAFlAYgAxADEAQAAAFY3gIwA+4FkgNMBMkFhQJQA14tbQIEAy35aQNQBS0hZQLsAJ1ZXQRcApZJXgJYBDn9XQN0BF0RXgIkBIidXgREAvJRWgMMA5pRVQFIBWmdVgOsAgzxVQQoA3ThVQMwAmz1UQFEAPBNUgHIAmRtTQEgBThRTQNoANqBTgKkAt4tSQM8BD55SgFIBKBtSgPYBE0FRgE8BFyJRgMAAFKpQQMkAXJ5QQIkA3YFQgQoAZ5hPQHcAoXtOgHEBShlOQKkBO15NgGAA8yJNQJoA+ZFNgH8BRBlMQOcAoJJMQJwAuYNJgRsBCD5JQGYBXQpJQKkApZZGgOQAv5REQJwAq3xCgJwAiHBCQRcAtTpAQJkADwI/gOYAuzc+QFUA44E6QNoBbEk5gHgBa2Q3QJwBLpQ2AKUAr4k1ABQAsXszAI8BPgIwAJ0AsX8vALIBcVYuAAA=",
                Minutiaetype = MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004,
                Additionaldata = null
            };

            xmlParamIn.SampleCollection.Add(sample);

            XmlParamOut response = API.WSIdentify(xmlParamIn);
            Assert.IsNotNull(response);
        }

        [Test]
        public void Verify()
        {
            XmlParamIn xmlParamIn = new XmlParamIn
            {
                Actionid = Action.ACTION_VERIFYANDGET,
                Additionaldata = null,
                Authenticationfactor = AuthenticationFactor.AUTHENTICATIONFACTOR_ANY,
                Minutiaetype = MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004,
                Bodypart = BodyPart.BODYPART_ALL,
                Clientid = "IdClienteTest",
                Companyid = 7,
                Enduser = "EnUserTest",
                Ipenduser = "127.0.0.1",
                Matchingtype = 1,
                Origin = 1,
                SampleCollection = new List<Sample>(),
                SaveVerified = 1,
                Threshold = 10000,
                Userid = 0,
                Verifybyconnectorid = "NONE",
                InsertOption = 1,
                PersonalData = new PersonalData
                {
                    Typeid = "RUT",
                    Valueid = "21284415-2"
                },
                OperationOrder = OperationOrder.OPERATIONORDER_LOCALONLY
            };

            Sample sample = new Sample
            {
                Data = "Rk1SACAyMAABagAz/v8AAAFlAYgAxADEAQAAAFY3gIwA+4FkgNMBMkFhQJQA14tbQIEAy35aQNQBS0hZQLsAJ1ZXQRcApZJXgJYBDn9XQN0BF0RXgIkBIidXgREAvJRWgMMA5pRVQFIBWmdVgOsAgzxVQQoA3ThVQMwAmz1UQFEAPBNUgHIAmRtTQEgBThRTQNoANqBTgKkAt4tSQM8BD55SgFIBKBtSgPYBE0FRgE8BFyJRgMAAFKpQQMkAXJ5QQIkA3YFQgQoAZ5hPQHcAoXtOgHEBShlOQKkBO15NgGAA8yJNQJoA+ZFNgH8BRBlMQOcAoJJMQJwAuYNJgRsBCD5JQGYBXQpJQKkApZZGgOQAv5REQJwAq3xCgJwAiHBCQRcAtTpAQJkADwI/gOYAuzc+QFUA44E6QNoBbEk5gHgBa2Q3QJwBLpQ2AKUAr4k1ABQAsXszAI8BPgIwAJ0AsX8vALIBcVYuAAA=",
                Minutiaetype = MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004,
                Additionaldata = null
            };

            xmlParamIn.SampleCollection.Add(sample);

            XmlParamOut response = API.WSVerify(xmlParamIn);

            if (xmlParamIn.Actionid == 1)
                Assert.Greater(response.Score, 0);
            else if (xmlParamIn.Actionid == 9)
                Assert.IsNotNull(response.PersonalData.Photography);
        }

        [Test]
        public void Get()
        {
            XmlParamIn xmlParamIn = new XmlParamIn { };

            XmlParamOut response = API.WSGet(xmlParamIn);
            Assert.IsNotNull(response);
        }
    }
}
