﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using Bio.Core.Api;
using Bio.Core.Constant;
using log4net;

namespace Bio.Core.TokenAN050101
{

    public class TokenAN050101 : Bio.Core.Matcher.Token.IToken
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(TokenAN050101));


        #region Private Properties Implementation of IToken

        private int _operationType;

        private int _authenticationFactor;

        private int _minutiaeType;

        private int _datatypetoken;

        private int _bodyPart;

        private List<Sample> _samples;

        private string _additionalData;

        private string _getData;

        private string _setData;

        private string _typeid;

        private string _valueid;

        private string _clientid;

        private string _ipenduser;

        private string _enduser;

        #endregion Private Properties Implementation of IToken

        #region Public Properties Implementation of IToken

        /// <summary>
        /// 1-Verify | 2-Enroll
        /// </summary>
        public int OperationType
        {
            get { return _operationType; }
            set { _operationType = value; }
        }

        /// <summary>
        /// Tecnologia a la que corresponde el template
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia que contiene Data, incluidos WSQ, RAW, etc.
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        ///// <summary>
        ///// Tipo de informacion (data) que contendra el template (desde BIR_TYPE)
        ///// </summary>
        //public int Type
        //{
        //    get { return _type; }
        //    set { _type = value; }
        //}

        /// <summary>
        /// Parte del cuerpo si es tecnologia biometrica
        /// </summary>
        public int BodyPart
        {
            get { return _bodyPart; }
            set { _bodyPart = value; }
        }

        /// <summary>
        /// [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
        /// </summary>
        public int DataTypeToken
        {
            get { return _datatypetoken; }
            set { _datatypetoken = value; }
        }

        /// <summary>
        /// Data propia del template o sample
        /// </summary>
        public List<Sample> Samples
        {
            get { return _samples; }
            set { _samples = value; }
        }

        /// <summary>
        /// Datos adicionales segun sea la data. Por ejemplo si es WSQ, 
        /// se puede colocar las dimensiones de la forma w=512|h=512, o si
        /// es template NEC, el coeficiente coef=xxx. Si hay mas de uno separado 
        /// por pipe "|"
        /// </summary>
        public string AdditionalData
        {
            get { return _additionalData; }
            set { _additionalData = value; }
        }

        /// <summary>
        /// Obtiene la Data en base 64
        /// </summary>
        public string GetData
        {
            get { return _getData; }
        }

        /// <summary>
        /// Setea la data desde base 64
        /// </summary>
        public string SetData
        {
            set { _setData = value; }
        }

        /// Tipo ID de la persona (RUT, DNI, etc) 
        /// </summary>
        public string Typeid
        {
            get { return _typeid; }
            set { _typeid = value; }
        }

        /// Valor ID de la persona (RUT, DNI, etc) 
        /// </summary>
        public string Valueid
        {
            get { return _valueid; }
            set { _valueid = value; }
        }

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist 
        /// </summary>
        public string Clientid
        {
            get { return _clientid; }
            set { _clientid = value; }
        }

        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        public string Ipenduser
        {
            get { return _ipenduser; }
            set { _ipenduser = value; }
        }

        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        public string Enduser
        {
            get { return _enduser; }
            set { _enduser = value; }
        }

        #endregion Public Properties Implementation of IToken

        #region Public Method

        /// <summary>
        /// Constructor vacio
        /// </summary>
        public TokenAN050101() { }

        /// <summary>
        /// Constructor que recibe token y extrae
        /// </summary>
        /// <param name="token"></param>
        public TokenAN050101(string token)
        {
            Extract(token);
        }


        /// <summary>
        /// Extrae el contenido del token y llena las propiedades del objeto
        /// </summary>
        /// <param name="token">Token a extraer</param>
        /// <returns></returns>
        public int Extract(string token)
        {
            int ret = Errors.IERR_OK;
            try
            {
                LOG.Debug("TokenAN050101.Extract - Token = " + token);
                //Si ES NULO O NO EMPIEZA CON av0305 retorno error
                //Diferentes oprefijos dependiendo de donde viene
                //      AN050101 - Android desde Grabba
                //      AM050101 - Android desde Morpho Tablet
                //      AJ050101 - Desde Java Applet
                if (token == null ||
                    ((!token.Trim().Substring(0, 8).Equals("AN050101")) && (!token.Trim().Substring(0, 8).Equals("AJ050101"))
                      && (!token.Trim().Substring(0, 8).Equals("AM050101")) && (!token.Trim().Substring(0, 8).Equals("AH050101"))))
                    return Errors.IERR_BAD_PARAMETER;

                //Si viene de Java => Extaigo con uno especial porque viene RAW y no WSQ => Secugen
                if (token.Trim().Substring(0, 8).Equals("AJ050101"))
                {
                    return ExtractJ(token);
                }

                //Si viene de Java Handheld China => Solo RAW de 256x360
                if (token.Trim().Substring(0, 8).Equals("AH050101"))
                {
                    return ExtractH(token);
                }

                //Si viene de Java => Extaigo con uno especial porque viene RAW y no WSQ
                if (token.Trim().Substring(0, 8).Equals("AM050101"))
                {
                    return ExtractM(token);  //Morpho - Viente con RAW en tamaño 256x400 en lugar de WSQ
                }

                //Saco encabezado
                string auxtoken = token.Trim().Substring(8);
                LOG.Debug("TokenAN050101.Extract - auxtoken = " + auxtoken);
                //Desencripto la data que viene en Hexa
                //BKclassCryptoClass oCrypt = new BKclassCryptoClass();
                string dataDecrypted = null;
                try 
                {
                    //dataDecrypted = oCrypt.Decrypt(auxtoken, "BioPortalClientAx14042010", true,
                    //                                      frezCryptoEncryptionType.frezBlockEncryption);
                    dataDecrypted = auxtoken; // Crypto.DecryptWithSymmetricAid(Crypto.PRIVATE_KEY, auxtoken);
                    LOG.Debug("TokenAN050101.Extract - Token decrypted = " + dataDecrypted);
                    if (dataDecrypted == null) return Errors.IERR_INVALID_TOKEN;
                }
                catch (Exception exdec)
                {
                    LOG.Error("TokenAN050101.Extract - Desencriptando - ", exdec);
                    return Errors.IERR_INVALID_TOKEN;
                }

                //Separo segun patron de separacion
                dataDecrypted = dataDecrypted.Replace("&#x0;", "");
                string[] aData = dataDecrypted.Split('|');
                LOG.Debug("TokenAN050101 - Len(aData)=" + aData.Length);
                //Lleno propiedades desde token
                /*
                   Pos > 0 - Tipo_Doc
                   Pos > 1 - Nro_Doc
                   Pos > 2 - Si es DP Serial Number
                   Pos > 3 - [1-10] Dedo  = BodyPart
                   Pos > 4 - WSQ en B64 si sContenido = 0,1
                   Pos > 5 - Template en B64 si sContenido = 0,2 (NEC o DP)
                   Pos > 6 - Timestamp Cliente [ddmmyyyyhhmmss]
                   Pos > 7 - IP cliente
                   Pos > 8 - [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
                   Pos > 9 - [1-NEC | 2-DP] Formato del Template
                   Pos > 10 - [UNKNOW-0|DP-1|SEC-2|BIOI-3|CROSSMATCH-4] Dispositivo
                   Pos > 11 - AutenticationFactor
                   Pos > 12 - OperationType
                 * 
                 * Saco porque no vienen nusnca Pos > 10 - Coeficiente = AdditionalData
                   
                 */
                _operationType = Convert.ToInt32(aData[12]);
                _authenticationFactor = Convert.ToInt32(aData[11]);
                _minutiaeType = Convert.ToInt32(aData[9]);
                _bodyPart = Convert.ToInt32(aData[3]);
                _samples = new List<Sample>();
                _datatypetoken = Convert.ToInt32(aData[8]);

                //Si viene Tewmplate, lo pongo primero para qyue verifique por el 
                string auxdata;
                if (_datatypetoken == 0 || _datatypetoken == 2)
                {
                    //Si es Password, aplico hash
                    if (_authenticationFactor == Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
                    {
                        auxdata = aData[5]; // Encoding.ASCII.GetString(aData[5]);
                        auxdata = FormsAuthentication.HashPasswordForStoringInConfigFile(auxdata, "SHA1");
                    }
                    else
                    {    //Sino paso tal cual viene
                        auxdata = aData[5];
                    }

                    //Added 01-11-2013 para agregar soprote a M;inucias ANSI y ISO que vienene en nuevo Token de BPClient.NET
                    //Con SDK de DP v2.2.0
                    string[] sep = new string[1] { "-septmpl-" };
                    string[] sa = auxdata.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    LOG.Debug("TokenAN050101 - Len(sa)=" + sa.Length);

                    Sample sTmpl = null;
                    Sample sTmpl1 = null;
                    Sample sTmpl2 = null;
                    string _datatANSI378;
                    string _datatISO19794_2;
                    string _templDP;
                    //Added apra diferenciar el token de Grabba
                    if (sa != null && sa.Length <= 2)
                    {
                        sTmpl = new Sample
                        {
                            Data = "",
                            Additionaldata = "", //aData[10],
                            Device = Convert.ToInt32(aData[11]),
                            Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
                        };
                        if (sa.Length == 1)
                        {
                            if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
                            {
                                LOG.Debug("TokenAN050101 - Seteando ANSI...");
                                _datatANSI378 = sa[0];
                                //Add ANSI 378
                                sTmpl1 = new Sample
                                {
                                    Data = _datatANSI378,
                                    Additionaldata = "", //aData[10],
                                    Device = Convert.ToInt32(aData[11]),
                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                                };
                                sTmpl2 = new Sample
                                {
                                    Data = "",
                                    Additionaldata = "", //aData[10],
                                    Device = Convert.ToInt32(aData[11]),
                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                                };
                            }
                            else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
                            {
                                LOG.Debug("TokenAN050101 - Seteando ISO...");
                                _datatISO19794_2 = sa[0];
                                //Add ISO 19794-2
                                sTmpl2 = new Sample
                                {
                                    Data = _datatISO19794_2,
                                    Additionaldata = "", //aData[10],
                                    Device = Convert.ToInt32(aData[11]),
                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                                };
                                sTmpl1 = new Sample
                                {
                                    Data = "",
                                    Additionaldata = "", //aData[10],
                                    Device = Convert.ToInt32(aData[11]),
                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                                };

                            }
                        }
                        else //es 2 => ANSI e ISO en ese orden
                        {
                            LOG.Debug("TokenAN050101 - Seteando ANSI...");
                            _datatANSI378 = sa[0];
                            //Add ANSI 378
                            sTmpl1 = new Sample
                            {
                                Data = _datatANSI378,
                                Additionaldata = "", //aData[10],
                                Device = Convert.ToInt32(aData[11]),
                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                            };
                            LOG.Debug("TokenAN050101 - Seteando ISO...");
                            _datatISO19794_2 = sa[1];
                            //Add ISO 19794-2
                            sTmpl2 = new Sample
                            {
                                Data = _datatISO19794_2,
                                Additionaldata = "", //aData[10],
                                Device = Convert.ToInt32(aData[11]),
                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                            };
                        }
                    }
                    else
                    {
                        _templDP = sa[0] + "-septmpl-" + sa[1];
                        //Add DP enroll y verify template concatenados. Si no es DP, debe ser de minucias propietarias que se generen. 
                        sTmpl = new Sample
                        {
                            Data = _templDP,
                            Additionaldata = "", //aData[10],
                            Device = Convert.ToInt32(aData[11]),
                            Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
                        };
                        //_samples.Add(sTmpl);

                        if (sa.Length > 2)
                        {
                            LOG.Debug("TokenAN050101 - Seteando ANSI...");
                            _datatANSI378 = sa[2];
                            //Add ANSI 378
                            sTmpl1 = new Sample
                            {
                                Data = _datatANSI378,
                                Additionaldata = "", //aData[10],
                                Device = Convert.ToInt32(aData[11]),
                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                            };
                            //_samples.Add(sTmpl1);
                        }
                        if (sa.Length > 3)
                        {
                            LOG.Debug("TokenAN050101 - Seteando ISO...");
                            _datatISO19794_2 = sa[3];
                            //Add ISO 19794-2
                            sTmpl2 = new Sample
                            {
                                Data = _datatISO19794_2,
                                Additionaldata = "", //aData[10],
                                Device = Convert.ToInt32(aData[11]),
                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                            };
                            //_samples.Add(sTmpl2);
                        }
                    }

                    //Added 05-2014 para armar el orden d elos templates, porque si es ANSI o ISO deben ir primero
                    //              por como se verifica en Service.Manager.Verify, que si viene un template generado
                    //              => Verifica con el Minutiaetype indicado pero con el sample[0] (el primero d ela lista)
                    if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
                    {
                        LOG.Error("TokenAN050101.Extract - Add ANSI/OTW/ISO");
                        _samples.Add(sTmpl1);  //Primero ANSI
                        _samples.Add(sTmpl);   //Segundo OTW
                        _samples.Add(sTmpl2);  //Tercero ISO
                    }
                    else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
                    {
                        LOG.Error("TokenAN050101.Extract - ISO/OTW/ANSI");
                        _samples.Add(sTmpl2);  //Primero ISO
                        _samples.Add(sTmpl);   //Segundo OTW
                        _samples.Add(sTmpl1);  //Tercero ANSI
                    }
                    else //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW)
                    {
                        LOG.Error("TokenAN050101.Extract - Add OTW/ANSI/ISO");
                        _samples.Add(sTmpl);  //Primero OTW o Secugen o el que sea
                        if (sTmpl1 != null) _samples.Add(sTmpl1); //Segundo ANSI    Agrego chequeo de NULL, para hacerlo compatible con anteriores versiones de 
                        if (sTmpl2 != null) _samples.Add(sTmpl2); //Tercero ISO     TokenAN050101
                    }

                }
                //Si viene WSQ, lo agrego para verificacion si no viene Template
                if (_datatypetoken == 0 || _datatypetoken == 1)  //Viene WSQ
                {
                    LOG.Debug("TokenAN050101.Extract - Add WSQ");
                    string wsq512x512 = aData[4];
                    LOG.Debug("TokenAN050101.Extract - Entrando Check WSQ Size...");
                    int ierr = CheckSizeWSQ(aData[4], out wsq512x512);
                    LOG.Debug("TokenAN050101.Extract - Saliendo Check WSQ Size ierr = " + ierr);
                    Sample sWsq = new Sample
                    {
                        Data = wsq512x512,
                        Additionaldata = aData[10],
                        Device = Convert.ToInt32(aData[11]),
                        Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ
                    };
                    _samples.Add(sWsq);
                }
                _typeid = aData[0];
                _valueid = aData[1];
                _clientid = aData[2].Trim();
                _ipenduser = aData[7].Trim();
                _enduser = aData[7].Trim();
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("TokenAN050101.Extarct", ex);
            }
            LOG.Debug("TokenAN050101.Extract - Out!");
            return ret;
        }

        private int ExtractM(string token)
        {
            int ret = Errors.IERR_OK;
            try
            {
                LOG.Debug("TokenAN050101.ExtractM - Token = " + token);
                //Si ES NULO O NO EMPIEZA CON av0305 retorno error
                if (token == null ||
                    ((!token.Trim().Substring(0, 8).Equals("AM050101"))))
                    return Errors.IERR_BAD_PARAMETER;

                //Saco encabezado
                string auxtoken = token.Trim().Substring(8);
                LOG.Debug("TokenAN050101.ExtractM - auxtoken = " + auxtoken);

                ////Separamos IV+Key del token 
                //string[] ivKeyToken = auxtoken.Split(new string[] { "-SEP-" }, StringSplitOptions.None);

                ////Desencriptamos con RSA el iv+Key
                //string symmetricKey = Crypto.Decrypt(Crypto.PRIVATE_KEY, ivKeyToken[0]);

                ////Separamos IV de Key
                //string[] ivKey = symmetricKey.Split(new string[] { "-SEP-" }, StringSplitOptions.None);

                ////Pasamos el IV y la key de base 64 a array de bytes
                //byte[] keyToken = Convert.FromBase64String(ivKey[0]);
                //byte[] ivToken = Convert.FromBase64String(ivKey[1]);

                //Desencriptamos el token utilizando la IV y la key

                string dataDecrypted = null;
                try
                {
                    //dataDecrypted = Crypto.decryptTripleDES(ivKeyToken[1], ivToken, keyToken);
                    dataDecrypted = auxtoken;
                    LOG.Debug("TokenAN050101.ExtractM - Token decrypted = " + dataDecrypted);
                    if (dataDecrypted == null) return Errors.IERR_INVALID_TOKEN;
                }
                catch (Exception exdec)
                {
                    LOG.Error("TokenAN050101.ExtractJ - Desencriptando - ", exdec);
                    return Errors.IERR_INVALID_TOKEN;
                }

                //Separo segun patron de separacion
                dataDecrypted = dataDecrypted.Replace("&#x0;", "");
                string[] aData = dataDecrypted.Split('|');
                LOG.Debug("TokenAN050101.ExtractM - Len(aData)=" + aData.Length);
                //Lleno propiedades desde token
                /*
                   Pos > 0 - Tipo_Doc
                   Pos > 1 - Nro_Doc
                   Pos > 2 - Si es DP Serial Number
                   Pos > 3 - [1-10] Dedo  = BodyPart
                   Pos > 4 - WSQ en B64 si sContenido = 0,1
                   Pos > 5 - Template en B64 si sContenido = 0,2 (NEC o DP)
                   Pos > 6 - Timestamp Cliente [ddmmyyyyhhmmss]
                   Pos > 7 - IP cliente
                   Pos > 8 - [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
                   Pos > 9 - [1-NEC | 2-DP] Formato del Template
                   Pos > 10 - [UNKNOW-0|DP-1|SEC-2|BIOI-3|CROSSMATCH-4] Dispositivo
                   Pos > 11 - AutenticationFactor
                   Pos > 12 - OperationType
                 * 
                 * Saco porque no vienen nusnca Pos > 10 - Coeficiente = AdditionalData
                   
                 */
                _operationType = Convert.ToInt32(aData[12]);
                _authenticationFactor = Convert.ToInt32(aData[11]);
                _minutiaeType = Convert.ToInt32(aData[9]);
                _bodyPart = Convert.ToInt32(aData[3]);
                _samples = new List<Sample>();
                _datatypetoken = Convert.ToInt32(aData[8]);

                //Si viene Tewmplate, lo pongo primero para qyue verifique por el 
                string auxdata;
                if (_datatypetoken == 0 || _datatypetoken == 2)
                {
                    //Si es Password, aplico hash
                    if (_authenticationFactor == Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
                    {
                        auxdata = aData[5]; // Encoding.ASCII.GetString(aData[5]);
                        auxdata = FormsAuthentication.HashPasswordForStoringInConfigFile(auxdata, "SHA1");
                    }
                    else
                    {    //Sino paso tal cual viene
                        auxdata = aData[5];
                    }

                    //Added 01-11-2013 para agregar soprote a M;inucias ANSI y ISO que vienene en nuevo Token de BPClient.NET
                    //Con SDK de DP v2.2.0
                    string[] sep = new string[1] { "-septmpl-" };
                    string[] sa = auxdata.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    LOG.Debug("TokenAN050101.ExtractM - Len(sa)=" + sa.Length);

                    Sample sTmpl = null;
                    Sample sTmpl1 = null;
                    Sample sTmpl2 = null;
                    string _datatANSI378;
                    string _datatISO19794_2;
                    string _templDP;
                    //Added apra diferenciar el token de Grabba
                    if (sa != null && sa.Length <= 2)
                    {
                        sTmpl = new Sample
                        {
                            Data = "",
                            Additionaldata = "", //aData[10],
                            Device = Convert.ToInt32(aData[11]),
                            Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
                        };
                        if (sa.Length == 1)
                        {
                            if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
                            {
                                LOG.Debug("TokenAN050101.ExtractM - Seteando ANSI...");
                                _datatANSI378 = sa[0];
                                //Add ANSI 378
                                sTmpl1 = new Sample
                                {
                                    Data = _datatANSI378,
                                    Additionaldata = "", //aData[10],
                                    Device = Convert.ToInt32(aData[11]),
                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                                };
                                sTmpl2 = new Sample
                                {
                                    Data = "",
                                    Additionaldata = "", //aData[10],
                                    Device = Convert.ToInt32(aData[11]),
                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                                };
                            }
                            else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
                            {
                                LOG.Debug("TokenAN050101.ExtractM - Seteando ISO...");
                                _datatISO19794_2 = sa[0];
                                //Add ISO 19794-2
                                sTmpl2 = new Sample
                                {
                                    Data = _datatISO19794_2,
                                    Additionaldata = "", //aData[10],
                                    Device = Convert.ToInt32(aData[11]),
                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                                };
                                sTmpl1 = new Sample
                                {
                                    Data = "",
                                    Additionaldata = "", //aData[10],
                                    Device = Convert.ToInt32(aData[11]),
                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                                };

                            }
                        }
                        else //es 2 => ANSI e ISO en ese orden
                        {
                            LOG.Debug("TokenAN050101.ExtractM - Seteando ANSI...");
                            _datatANSI378 = sa[0];
                            //Add ANSI 378
                            sTmpl1 = new Sample
                            {
                                Data = _datatANSI378,
                                Additionaldata = "", //aData[10],
                                Device = Convert.ToInt32(aData[11]),
                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                            };
                            LOG.Debug("TokenAN050101.ExtractM - Seteando ISO...");
                            _datatISO19794_2 = sa[1];
                            //Add ISO 19794-2
                            sTmpl2 = new Sample
                            {
                                Data = _datatISO19794_2,
                                Additionaldata = "", //aData[10],
                                Device = Convert.ToInt32(aData[11]),
                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                            };
                        }
                    }
                    else
                    {
                        _templDP = sa[0] + "-septmpl-" + sa[1];
                        //Add DP enroll y verify template concatenados. Si no es DP, debe ser de minucias propietarias que se generen. 
                        sTmpl = new Sample
                        {
                            Data = _templDP,
                            Additionaldata = "", //aData[10],
                            Device = Convert.ToInt32(aData[11]),
                            Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
                        };
                        //_samples.Add(sTmpl);

                        if (sa.Length > 2)
                        {
                            LOG.Debug("TokenAN050101.ExtractM - Seteando ANSI...");
                            _datatANSI378 = sa[2];
                            //Add ANSI 378
                            sTmpl1 = new Sample
                            {
                                Data = _datatANSI378,
                                Additionaldata = "", //aData[10],
                                Device = Convert.ToInt32(aData[11]),
                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                            };
                            //_samples.Add(sTmpl1);
                        }
                        if (sa.Length > 3)
                        {
                            LOG.Debug("TokenAN050101.ExtractM - Seteando ISO...");
                            _datatISO19794_2 = sa[3];
                            //Add ISO 19794-2
                            sTmpl2 = new Sample
                            {
                                Data = _datatISO19794_2,
                                Additionaldata = "", //aData[10],
                                Device = Convert.ToInt32(aData[11]),
                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                            };
                            //_samples.Add(sTmpl2);
                        }
                    }

                    //Added 05-2014 para armar el orden d elos templates, porque si es ANSI o ISO deben ir primero
                    //              por como se verifica en Service.Manager.Verify, que si viene un template generado
                    //              => Verifica con el Minutiaetype indicado pero con el sample[0] (el primero d ela lista)
                    if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
                    {
                        LOG.Error("TokenAN050101.ExtractM - Add ANSI/OTW/ISO");
                        _samples.Add(sTmpl1);  //Primero ANSI
                        _samples.Add(sTmpl);   //Segundo OTW
                        _samples.Add(sTmpl2);  //Tercero ISO
                    }
                    else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
                    {
                        LOG.Error("TokenAN050101.ExtractM - ISO/OTW/ANSI");
                        _samples.Add(sTmpl2);  //Primero ISO
                        _samples.Add(sTmpl);   //Segundo OTW
                        _samples.Add(sTmpl1);  //Tercero ANSI
                    }
                    else //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW)
                    {
                        LOG.Error("TokenAN050101.ExtractM - Add OTW/ANSI/ISO");
                        _samples.Add(sTmpl);  //Primero OTW o Secugen o el que sea
                        if (sTmpl1 != null) _samples.Add(sTmpl1); //Segundo ANSI    Agrego chequeo de NULL, para hacerlo compatible con anteriores versiones de 
                        if (sTmpl2 != null) _samples.Add(sTmpl2); //Tercero ISO     TokenAN050101
                    }

                }
                //Si viene WSQ, lo agrego para verificacion si no viene Template
                if (_datatypetoken == 0 || _datatypetoken == 1)  //Viene WSQ
                {
                    LOG.Debug("TokenAN050101.ExtractM - Add WSQ");
                    LOG.Debug("TokenAN050101.ExtractM - Entrando Generate WSQ desde = " + aData[4]);
                    //(orig 300x400)
                    byte[] raworig = Convert.FromBase64String(aData[4]);
                    byte[] raw512x512 = Bio.Core.Imaging.ImageProcessor.Adjust(raworig, 256, 400, 512, 512);
                    LOG.Debug("TokenAN050101.ExtractM - RAW Ajustada de 256x400 => 512x512...");
                    Bio.Core.Wsq.Encoder.WsqEncoder encoder = new Wsq.Encoder.WsqEncoder();
                    short w512 = 512;
                    short h512 = 512;
                    byte[] wsq512x512;
                    string strwsq512x512;
                    if (encoder.EncodeMemory(raw512x512, w512, h512, out wsq512x512))
                    {
                        strwsq512x512 = Convert.ToBase64String(wsq512x512);
                        LOG.Debug("TokenAN050101.ExtractM - WSQ creado desde RAW Ajustada de 300x400 => 512x512 = " + strwsq512x512);
                    }
                    else
                    {
                        strwsq512x512 = aData[4];
                        LOG.Debug("TokenAN050101.ExtractM - WSQ NO creado desde RAW Ajustada de 300x400 => 512x512! => Se setea RAW original = " + strwsq512x512);
                    }
                    Sample sWsq = new Sample
                    {
                        Data = strwsq512x512,
                        Additionaldata = aData[10],
                        Device = Convert.ToInt32(aData[11]),
                        Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ
                    };
                    _samples.Add(sWsq);
                }
                _typeid = aData[0];
                _valueid = aData[1];
                _clientid = aData[2].Trim();
                _ipenduser = aData[7].Trim();
                _enduser = aData[7].Trim();
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("TokenAN050101.ExtractM", ex);
            }
            LOG.Debug("TokenAN050101.ExtractM - Out!");
            return ret;
        }

        private int ExtractJ(string token)
        {
            int ret = Errors.IERR_OK;
            try
            {
                LOG.Debug("TokenAN050101.ExtractJ - Token = " + token);
                //Si ES NULO O NO EMPIEZA CON av0305 retorno error
                if (token == null ||
                    ((!token.Trim().Substring(0, 8).Equals("AJ050101"))))
                    return Errors.IERR_BAD_PARAMETER;

                //Saco encabezado
                string auxtoken = token.Trim().Substring(8);
                LOG.Debug("TokenAN050101.ExtractJ - auxtoken = " + auxtoken);

                //Separamos IV+Key del token 
                string[] ivKeyToken = auxtoken.Split(new string[] { "-SEP-" }, StringSplitOptions.None);

                ////Desencriptamos con RSA el iv+Key
                string symmetricKey = Crypto.Decrypt(Crypto.PRIVATE_KEY, ivKeyToken[0]);

                ////Separamos IV de Key
                string[] ivKey = symmetricKey.Split(new string[] { "-SEP-" }, StringSplitOptions.None);

                ////Pasamos el IV y la key de base 64 a array de bytes
                byte[] keyToken = Convert.FromBase64String(ivKey[0]);
                byte[] ivToken = Convert.FromBase64String(ivKey[1]);

                //Desencriptamos el token utilizando la IV y la key

                string dataDecrypted = null;
                try
                {
                    dataDecrypted = Crypto.decryptTripleDES(ivKeyToken[1], ivToken, keyToken);
                    LOG.Debug("TokenAN050101.ExtractJ - Token decrypted = " + dataDecrypted);
                    if (dataDecrypted == null) return Errors.IERR_INVALID_TOKEN;
                }
                catch (Exception exdec)
                {
                    LOG.Error("TokenAN050101.ExtractJ - Desencriptando - ", exdec);
                    return Errors.IERR_INVALID_TOKEN;
                }

                //Separo segun patron de separacion
                dataDecrypted = dataDecrypted.Replace("&#x0;", "");
                string[] aData = dataDecrypted.Split('|');
                LOG.Debug("TokenAN050101.ExtractJ - Len(aData)=" + aData.Length);
                //Lleno propiedades desde token
                /*
                   Pos > 0 - Tipo_Doc
                   Pos > 1 - Nro_Doc
                   Pos > 2 - Si es DP Serial Number
                   Pos > 3 - [1-10] Dedo  = BodyPart
                   Pos > 4 - WSQ en B64 si sContenido = 0,1
                   Pos > 5 - Template en B64 si sContenido = 0,2 (NEC o DP)
                   Pos > 6 - Timestamp Cliente [ddmmyyyyhhmmss]
                   Pos > 7 - IP cliente
                   Pos > 8 - [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
                   Pos > 9 - [1-NEC | 2-DP] Formato del Template
                   Pos > 10 - [UNKNOW-0|DP-1|SEC-2|BIOI-3|CROSSMATCH-4] Dispositivo
                   Pos > 11 - AutenticationFactor
                   Pos > 12 - OperationType
                 * 
                 * Saco porque no vienen nusnca Pos > 10 - Coeficiente = AdditionalData
                   
                 */
                _operationType = Convert.ToInt32(aData[12]);
                _authenticationFactor = Convert.ToInt32(aData[11]);
                _minutiaeType = Convert.ToInt32(aData[9]);
                _bodyPart = Convert.ToInt32(aData[3]);
                _samples = new List<Sample>();
                _datatypetoken = Convert.ToInt32(aData[8]);

                //Si viene Tewmplate, lo pongo primero para qyue verifique por el 
                string auxdata;
                if (_datatypetoken == 0 || _datatypetoken == 2)
                {
                    //Si es Password, aplico hash
                    if (_authenticationFactor == Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
                    {
                        auxdata = aData[5]; // Encoding.ASCII.GetString(aData[5]);
                        auxdata = FormsAuthentication.HashPasswordForStoringInConfigFile(auxdata, "SHA1");
                    }
                    else
                    {    //Sino paso tal cual viene
                        auxdata = aData[5];
                    }

                    //Added 01-11-2013 para agregar soprote a M;inucias ANSI y ISO que vienene en nuevo Token de BPClient.NET
                    //Con SDK de DP v2.2.0
                    string[] sep = new string[1] { "-septmpl-" };
                    string[] sa = auxdata.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    LOG.Debug("TokenAN050101.ExtractJ - Len(sa)=" + sa.Length);

                    Sample sTmpl = null;
                    Sample sTmpl1 = null;
                    Sample sTmpl2 = null;
                    string _datatANSI378;
                    string _datatISO19794_2;
                    string _templDP;
                    //Added apra diferenciar el token de Grabba
                    if (sa != null && sa.Length <= 2)
                    {
                        sTmpl = new Sample
                        {
                            Data = "",
                            Additionaldata = "", //aData[10],
                            Device = Convert.ToInt32(aData[11]),
                            Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
                        };
                        if (sa.Length == 1)
                        {
                            if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
                            {
                                LOG.Debug("TokenAN050101.ExtractJ - Seteando ANSI...");
                                _datatANSI378 = sa[0];
                                //Add ANSI 378
                                sTmpl1 = new Sample
                                {
                                    Data = _datatANSI378,
                                    Additionaldata = "", //aData[10],
                                    Device = Convert.ToInt32(aData[11]),
                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                                };
                                sTmpl2 = new Sample
                                {
                                    Data = "",
                                    Additionaldata = "", //aData[10],
                                    Device = Convert.ToInt32(aData[11]),
                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                                };
                            }
                            else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
                            {
                                LOG.Debug("TokenAN050101.ExtractJ - Seteando ISO...");
                                _datatISO19794_2 = sa[0];
                                //Add ISO 19794-2
                                sTmpl2 = new Sample
                                {
                                    Data = _datatISO19794_2,
                                    Additionaldata = "", //aData[10],
                                    Device = Convert.ToInt32(aData[11]),
                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                                };
                                sTmpl1 = new Sample
                                {
                                    Data = "",
                                    Additionaldata = "", //aData[10],
                                    Device = Convert.ToInt32(aData[11]),
                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                                };

                            }
                        }
                        else //es 2 => ANSI e ISO en ese orden
                        {
                            LOG.Debug("TokenAN050101.ExtractJ - Seteando ANSI...");
                            _datatANSI378 = sa[0];
                            //Add ANSI 378
                            sTmpl1 = new Sample
                            {
                                Data = _datatANSI378,
                                Additionaldata = "", //aData[10],
                                Device = Convert.ToInt32(aData[11]),
                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                            };
                            LOG.Debug("TokenAN050101.ExtractJ - Seteando ISO...");
                            _datatISO19794_2 = sa[1];
                            //Add ISO 19794-2
                            sTmpl2 = new Sample
                            {
                                Data = _datatISO19794_2,
                                Additionaldata = "", //aData[10],
                                Device = Convert.ToInt32(aData[11]),
                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                            };
                        }
                    }
                    else
                    {
                        _templDP = sa[0] + "-septmpl-" + sa[1];
                        //Add DP enroll y verify template concatenados. Si no es DP, debe ser de minucias propietarias que se generen. 
                        sTmpl = new Sample
                        {
                            Data = _templDP,
                            Additionaldata = "", //aData[10],
                            Device = Convert.ToInt32(aData[11]),
                            Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
                        };
                        //_samples.Add(sTmpl);

                        if (sa.Length > 2)
                        {
                            LOG.Debug("TokenAN050101.ExtractJ - Seteando ANSI...");
                            _datatANSI378 = sa[2];
                            //Add ANSI 378
                            sTmpl1 = new Sample
                            {
                                Data = _datatANSI378,
                                Additionaldata = "", //aData[10],
                                Device = Convert.ToInt32(aData[11]),
                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                            };
                            //_samples.Add(sTmpl1);
                        }
                        if (sa.Length > 3)
                        {
                            LOG.Debug("TokenAN050101.ExtractJ - Seteando ISO...");
                            _datatISO19794_2 = sa[3];
                            //Add ISO 19794-2
                            sTmpl2 = new Sample
                            {
                                Data = _datatISO19794_2,
                                Additionaldata = "", //aData[10],
                                Device = Convert.ToInt32(aData[11]),
                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                            };
                            //_samples.Add(sTmpl2);
                        }
                    }

                    //Added 05-2014 para armar el orden d elos templates, porque si es ANSI o ISO deben ir primero
                    //              por como se verifica en Service.Manager.Verify, que si viene un template generado
                    //              => Verifica con el Minutiaetype indicado pero con el sample[0] (el primero d ela lista)
                    //Added 12-06-2016 => Dado que la minutiaetype que se setea en el create token hace que se coloque aqui el template 
                    //                    en formato erroneo, y las minucias secugen no se usan nunca, ahora comento en todos lados 
                    //                    para que no se pongan las minucias con formato secugen en la lista de samples porque 
                    //                    puede traer problemas en RENIEC si se quiere usar la minucia, o verificaicon en BioPOrtal con ANSI o ISO
                    if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
                    {
                        LOG.Error("TokenAN050101.ExtractJ - Add ANSI/OTW/ISO");
                        _samples.Add(sTmpl1);  //Primero ANSI
                        //_samples.Add(sTmpl);   //Segundo OTW
                        _samples.Add(sTmpl2);  //Tercero ISO
                    }
                    else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
                    {
                        LOG.Error("TokenAN050101.ExtractJ - ISO/OTW/ANSI");
                        _samples.Add(sTmpl2);  //Primero ISO
                        //_samples.Add(sTmpl);   //Segundo OTW
                        _samples.Add(sTmpl1);  //Tercero ANSI
                    }
                    else //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW)
                    {
                        LOG.Error("TokenAN050101.ExtractJ - Add OTW/ANSI/ISO");
                        //_samples.Add(sTmpl);  //Primero OTW o Secugen o el que sea
                        if (sTmpl1 != null) _samples.Add(sTmpl1); //Segundo ANSI    Agrego chequeo de NULL, para hacerlo compatible con anteriores versiones de 
                        if (sTmpl2 != null) _samples.Add(sTmpl2); //Tercero ISO     TokenAN050101
                    }

                }
                //Si viene WSQ, lo agrego para verificacion si no viene Template
                if (_datatypetoken == 0 || _datatypetoken == 1)  //Viene WSQ
                {
                    LOG.Debug("TokenAN050101.ExtractJ - Add WSQ");
                    LOG.Debug("TokenAN050101.ExtractJ - Entrando Generate WSQ desde = " + aData[4]);
                    //(orig 300x400)
                    byte[] raworig = Convert.FromBase64String(aData[4]);
                    byte[] raw512x512 = null;
                    if (raworig.Length == 120000) //Es de Secugen Hamster Pro 20 SGDEV_FDU05 => size 120000
                    {
                        raw512x512 = Bio.Core.Imaging.ImageProcessor.Adjust(raworig, 300, 400, 512, 512);
                    }
                    else if (raworig.Length == 86688) //Es de Secugen Hamster IV SGDEV_FDU04 => size 86688
                    {
                        raw512x512 = Bio.Core.Imaging.ImageProcessor.Adjust(raworig, 258, 336, 512, 512);
                    }
                    else //es de Secugen Hamster Plus SGDEV_FDU03 => size 78000
                    {
                        raw512x512 = Bio.Core.Imaging.ImageProcessor.Adjust(raworig, 260, 300, 512, 512);
                    }
                    LOG.Debug("TokenAN050101.ExtractJ - RAW Ajustada de 300x400 => 512x512...");
                    Bio.Core.Wsq.Encoder.WsqEncoder encoder = new Wsq.Encoder.WsqEncoder();
                    short w512 = 512;
                    short h512 = 512;
                    byte[] wsq512x512;
                    string strwsq512x512;
                    if (encoder.EncodeMemory(raw512x512, w512, h512, out wsq512x512))
                    {
                        strwsq512x512 = Convert.ToBase64String(wsq512x512);
                        LOG.Debug("TokenAN050101.ExtractJ - WSQ creado desde RAW Ajustada de 300x400 => 512x512 = " + strwsq512x512);
                    }
                    else
                    {
                        strwsq512x512 = aData[4];
                        LOG.Debug("TokenAN050101.ExtractJ - WSQ NO creado desde RAW Ajustada de 300x400 => 512x512! => Se setea RAW original = " + strwsq512x512);
                    }
                    Sample sWsq = new Sample
                    {
                        Data = strwsq512x512,
                        Additionaldata = aData[10],
                        Device = Convert.ToInt32(aData[11]),
                        Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ
                    };
                    _samples.Add(sWsq);
                }
                _typeid = aData[0];
                _valueid = aData[1];
                _clientid = aData[2].Trim();
                _ipenduser = aData[7].Trim();
                _enduser = aData[7].Trim();
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("TokenAN050101.ExtarctJ", ex);
            }
            LOG.Debug("TokenAN050101.ExtractJ - Out!");
            return ret;
        }

        /// <summary>
        /// Extrae token desde Handheld China 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private int ExtractH(string token)
        {
            int ret = Errors.IERR_OK;
            try
            {
                LOG.Debug("TokenAN050101.ExtractH - Token = " + token);
                //Si ES NULO O NO EMPIEZA CON AH050101 retorno error
                if (token == null ||
                    ((!token.Trim().Substring(0, 8).Equals("AH050101"))))
                    return Errors.IERR_BAD_PARAMETER;

                //Saco encabezado
                string auxtoken = token.Trim().Substring(8);
                LOG.Debug("TokenAN050101.ExtractH - auxtoken = " + auxtoken);

                ////Separamos IV+Key del token 
                //string[] ivKeyToken = auxtoken.Split(new string[] { "-SEP-" }, StringSplitOptions.None);

                //////Desencriptamos con RSA el iv+Key
                //string symmetricKey = Crypto.Decrypt(Crypto.PRIVATE_KEY, ivKeyToken[0]);

                //////Separamos IV de Key
                //string[] ivKey = symmetricKey.Split(new string[] { "-SEP-" }, StringSplitOptions.None);

                //////Pasamos el IV y la key de base 64 a array de bytes
                //byte[] keyToken = Convert.FromBase64String(ivKey[0]);
                //byte[] ivToken = Convert.FromBase64String(ivKey[1]);

                //Desencriptamos el token utilizando la IV y la key

                string dataDecrypted = null;
                try
                {
                    dataDecrypted = auxtoken; //Crypto.decryptTripleDES(ivKeyToken[1], ivToken, keyToken);
                    LOG.Debug("TokenAN050101.ExtractH - Token decrypted = " + dataDecrypted);
                    if (dataDecrypted == null) return Errors.IERR_INVALID_TOKEN;
                }
                catch (Exception exdec)
                {
                    LOG.Error("TokenAN050101.ExtractH - Desencriptando - ", exdec);
                    return Errors.IERR_INVALID_TOKEN;
                }

                //Separo segun patron de separacion
                dataDecrypted = dataDecrypted.Replace("&#x0;", "");
                string[] aData = dataDecrypted.Split('|');
                LOG.Debug("TokenAN050101.ExtractH - Len(aData)=" + aData.Length);
                //Lleno propiedades desde token
                /*
                   Pos > 0 - Tipo_Doc
                   Pos > 1 - Nro_Doc
                   Pos > 2 - Si es DP Serial Number
                   Pos > 3 - [1-10] Dedo  = BodyPart
                   Pos > 4 - WSQ en B64 si sContenido = 0,1
                   Pos > 5 - Template en B64 si sContenido = 0,2 (NEC o DP)
                   Pos > 6 - Timestamp Cliente [ddmmyyyyhhmmss]
                   Pos > 7 - IP cliente
                   Pos > 8 - [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
                   Pos > 9 - [1-NEC | 2-DP] Formato del Template
                   Pos > 10 - [UNKNOW-0|DP-1|SEC-2|BIOI-3|CROSSMATCH-4] Dispositivo
                   Pos > 11 - AutenticationFactor
                   Pos > 12 - OperationType
                 * 
                 * Saco porque no vienen nusnca Pos > 10 - Coeficiente = AdditionalData
                   
                 */
                _operationType = Convert.ToInt32(aData[12]);
                _authenticationFactor = Convert.ToInt32(aData[11]);
                _minutiaeType = Convert.ToInt32(aData[9]);
                _bodyPart = Convert.ToInt32(aData[3]);
                _samples = new List<Sample>();
                _datatypetoken = Convert.ToInt32(aData[8]);

                //Si viene Tewmplate, lo pongo primero para qyue verifique por el 
                //string auxdata;
                //if (_datatypetoken == 0 || _datatypetoken == 2)
                //{
                //    //Si es Password, aplico hash
                //    if (_authenticationFactor == Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
                //    {
                //        auxdata = aData[5]; // Encoding.ASCII.GetString(aData[5]);
                //        auxdata = FormsAuthentication.HashPasswordForStoringInConfigFile(auxdata, "SHA1");
                //    }
                //    else
                //    {    //Sino paso tal cual viene
                //        auxdata = aData[5];
                //    }

                //    //Added 01-11-2013 para agregar soprote a Minucias ANSI y ISO que vienene en nuevo Token de BPClient.NET
                //    //Con SDK de DP v2.2.0
                //    string[] sep = new string[1] { "-septmpl-" };
                //    string[] sa = auxdata.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                //    LOG.Debug("TokenAN050101.ExtractH - Len(sa)=" + sa.Length);

                //    Sample sTmpl = null;
                //    Sample sTmpl1 = null;
                //    Sample sTmpl2 = null;
                //    string _datatANSI378;
                //    string _datatISO19794_2;
                //    string _templDP;
                //    //Added apra diferenciar el token de Grabba
                //    if (sa != null && sa.Length <= 2)
                //    {
                //        sTmpl = new Sample
                //        {
                //            Data = "",
                //            Additionaldata = "", //aData[10],
                //            Device = Convert.ToInt32(aData[11]),
                //            Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
                //        };
                //        if (sa.Length == 1)
                //        {
                //            if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
                //            {
                //                LOG.Debug("TokenAN050101.ExtractH - Seteando ANSI...");
                //                _datatANSI378 = sa[0];
                //                //Add ANSI 378
                //                sTmpl1 = new Sample
                //                {
                //                    Data = _datatANSI378,
                //                    Additionaldata = "", //aData[10],
                //                    Device = Convert.ToInt32(aData[11]),
                //                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                //                };
                //                sTmpl2 = new Sample
                //                {
                //                    Data = "",
                //                    Additionaldata = "", //aData[10],
                //                    Device = Convert.ToInt32(aData[11]),
                //                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                //                };
                //            }
                //            else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
                //            {
                //                LOG.Debug("TokenAN050101.ExtractH - Seteando ISO...");
                //                _datatISO19794_2 = sa[0];
                //                //Add ISO 19794-2
                //                sTmpl2 = new Sample
                //                {
                //                    Data = _datatISO19794_2,
                //                    Additionaldata = "", //aData[10],
                //                    Device = Convert.ToInt32(aData[11]),
                //                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                //                };
                //                sTmpl1 = new Sample
                //                {
                //                    Data = "",
                //                    Additionaldata = "", //aData[10],
                //                    Device = Convert.ToInt32(aData[11]),
                //                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                //                };

                //            }
                //        }
                //        else //es 2 => ANSI e ISO en ese orden
                //        {
                //            LOG.Debug("TokenAN050101.ExtractH - Seteando ANSI...");
                //            _datatANSI378 = sa[0];
                //            //Add ANSI 378
                //            sTmpl1 = new Sample
                //            {
                //                Data = _datatANSI378,
                //                Additionaldata = "", //aData[10],
                //                Device = Convert.ToInt32(aData[11]),
                //                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                //            };
                //            LOG.Debug("TokenAN050101.ExtractJ - Seteando ISO...");
                //            _datatISO19794_2 = sa[1];
                //            //Add ISO 19794-2
                //            sTmpl2 = new Sample
                //            {
                //                Data = _datatISO19794_2,
                //                Additionaldata = "", //aData[10],
                //                Device = Convert.ToInt32(aData[11]),
                //                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                //            };
                //        }
                //    }
                //    else
                //    {
                //        _templDP = sa[0] + "-septmpl-" + sa[1];
                //        //Add DP enroll y verify template concatenados. Si no es DP, debe ser de minucias propietarias que se generen. 
                //        sTmpl = new Sample
                //        {
                //            Data = _templDP,
                //            Additionaldata = "", //aData[10],
                //            Device = Convert.ToInt32(aData[11]),
                //            Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
                //        };
                //        //_samples.Add(sTmpl);

                //        if (sa.Length > 2)
                //        {
                //            LOG.Debug("TokenAN050101.ExtractH - Seteando ANSI...");
                //            _datatANSI378 = sa[2];
                //            //Add ANSI 378
                //            sTmpl1 = new Sample
                //            {
                //                Data = _datatANSI378,
                //                Additionaldata = "", //aData[10],
                //                Device = Convert.ToInt32(aData[11]),
                //                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                //            };
                //            //_samples.Add(sTmpl1);
                //        }
                //        if (sa.Length > 3)
                //        {
                //            LOG.Debug("TokenAN050101.ExtractH - Seteando ISO...");
                //            _datatISO19794_2 = sa[3];
                //            //Add ISO 19794-2
                //            sTmpl2 = new Sample
                //            {
                //                Data = _datatISO19794_2,
                //                Additionaldata = "", //aData[10],
                //                Device = Convert.ToInt32(aData[11]),
                //                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
                //            };
                //            //_samples.Add(sTmpl2);
                //        }
                //    }

                //    //Added 05-2014 para armar el orden d elos templates, porque si es ANSI o ISO deben ir primero
                //    //              por como se verifica en Service.Manager.Verify, que si viene un template generado
                //    //              => Verifica con el Minutiaetype indicado pero con el sample[0] (el primero d ela lista)
                //    //Added 12-06-2016 => Dado que la minutiaetype que se setea en el create token hace que se coloque aqui el template 
                //    //                    en formato erroneo, y las minucias secugen no se usan nunca, ahora comento en todos lados 
                //    //                    para que no se pongan las minucias con formato secugen en la lista de samples porque 
                //    //                    puede traer problemas en RENIEC si se quiere usar la minucia, o verificaicon en BioPOrtal con ANSI o ISO
                //    if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
                //    {
                //        LOG.Error("TokenAN050101.ExtractH - Add ANSI/OTW/ISO");
                //        _samples.Add(sTmpl1);  //Primero ANSI
                //        //_samples.Add(sTmpl);   //Segundo OTW
                //        _samples.Add(sTmpl2);  //Tercero ISO
                //    }
                //    else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
                //    {
                //        LOG.Error("TokenAN050101.ExtractH - ISO/OTW/ANSI");
                //        _samples.Add(sTmpl2);  //Primero ISO
                //        //_samples.Add(sTmpl);   //Segundo OTW
                //        _samples.Add(sTmpl1);  //Tercero ANSI
                //    }
                //    else //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW)
                //    {
                //        LOG.Error("TokenAN050101.ExtractH - Add OTW/ANSI/ISO");
                //        //_samples.Add(sTmpl);  //Primero OTW o Secugen o el que sea
                //        if (sTmpl1 != null) _samples.Add(sTmpl1); //Segundo ANSI    Agrego chequeo de NULL, para hacerlo compatible con anteriores versiones de 
                //        if (sTmpl2 != null) _samples.Add(sTmpl2); //Tercero ISO     TokenAN050101
                //    }

                //}
                //Si viene WSQ, lo agrego para verificacion si no viene Template
                if (_datatypetoken == 0 || _datatypetoken == 1)  //Viene WSQ
                {
                    LOG.Debug("TokenAN050101.ExtractH - Add WSQ");
                    LOG.Debug("TokenAN050101.ExtractH - Entrando Generate WSQ desde = " + aData[4]);
                    //(orig 300x400)
                    byte[] raworig = Convert.FromBase64String(aData[4]);
                    byte[] raw512x512 = Bio.Core.Imaging.ImageProcessor.Adjust(raworig, 256, 360, 512, 512);
                    LOG.Debug("TokenAN050101.ExtractH - RAW Ajustada de 256x360 => 512x512...");
                    Bio.Core.Wsq.Encoder.WsqEncoder encoder = new Wsq.Encoder.WsqEncoder();
                    short w512 = 512;
                    short h512 = 512;
                    byte[] wsq512x512;
                    string strwsq512x512;
                    if (encoder.EncodeMemory(raw512x512, w512, h512, out wsq512x512))
                    {
                        strwsq512x512 = Convert.ToBase64String(wsq512x512);
                        LOG.Debug("TokenAN050101.ExtractH - WSQ creado desde RAW Ajustada de 256x360 => 512x512 = " + strwsq512x512);
                    }
                    else
                    {
                        strwsq512x512 = aData[4];
                        LOG.Debug("TokenAN050101.ExtractH - WSQ NO creado desde RAW Ajustada de 256x360 => 512x512! => Se setea RAW original = " + strwsq512x512);
                    }
                    Sample sWsq = new Sample
                    {
                        Data = strwsq512x512,
                        Additionaldata = aData[10],
                        Device = Convert.ToInt32(aData[11]),
                        Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ
                    };
                    _samples.Add(sWsq); 
                }
                _typeid = aData[0];
                _valueid = aData[1];
                _clientid = aData[2].Trim();
                _ipenduser = aData[7].Trim();
                _enduser = aData[7].Trim();
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("TokenAN050101.ExtractH", ex);
            }
            LOG.Debug("TokenAN050101.ExtractH - Out!");
            return ret;
        }


        private int CheckSizeWSQ(string wsqorig, out string strwsq512x512)
        {
            int ierr = 0;
            strwsq512x512 = wsqorig;
            try
            {
                LOG.Debug("TokenAN050101.CheckSizeWSQ - Entrando...");
                LOG.Debug("TokenAN050101.CheckSizeWSQ - Convieritnedo desde B64 wsqorig =" + wsqorig);
                byte[] bywsqorig = Convert.FromBase64String(wsqorig);
                LOG.Debug("TokenAN050101.CheckSizeWSQ - Size byte WSQ Orig bywsqorig.Length = " + bywsqorig.Length);
                byte[] raw;
                short worig = 0;
                short horig = 0;
                Bio.Core.Wsq.Decoder.WsqDecoder decoder = new Wsq.Decoder.WsqDecoder();
                if (decoder.DecodeMemory(bywsqorig, out worig, out horig, out raw))
                {
                    LOG.Debug("TokenAN050101.CheckSizeWSQ - WSQ Decoded => worig = " + worig + " x horig = " + horig);
                    if (worig != 512 || horig != 512)
                    {
                        byte[] raw512x512 = Bio.Core.Imaging.ImageProcessor.Adjust(raw, worig, horig, 512, 512);
                        Bio.Core.Wsq.Encoder.WsqEncoder encoder = new Wsq.Encoder.WsqEncoder();
                        short w512 = 512;
                        short h512 = 512;
                        byte[] wsq512x512;
                        if (encoder.EncodeMemory(raw512x512, w512, h512, out wsq512x512))
                        {
                            strwsq512x512 = Convert.ToBase64String(wsq512x512);
                            LOG.Debug("TokenAN050101.CheckSizeWSQ - RAW relleno con blanco en strwsq512x512");
                        }
                        else
                        {
                            strwsq512x512 = wsqorig;
                            LOG.Debug("TokenAN050101.CheckSizeWSQ - RAW relleno con wsqorig => No pudo comprimir!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("TokenAN050101.CheckSizeWSQ", ex);
                ierr = -1;
            }
            return ierr;
        }

        /// <summary>
        /// Extrae el contenido del token y llena las propiedades del objeto y ademas de llenar las prop del bjeto, 
        /// setea las variable que corresponden a un objeto XmlParamIn
        /// </summary>
        /// <param name="token">token en base 64</param>
        /// <param name="newparamin">Objeto XmlParamIn con datos extraidos desde token</param>
        /// <returns></returns>
        public int ExtractToParameters(string token, out string newparamin)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {

        }

        #endregion Public Method

    }
}



//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Web.Security;
//using Bio.Core.Api;
//using Bio.Core.Constant;
//using log4net;

//namespace Bio.Core.TokenAN050101
//{
    
//    public class TokenAN050101 : Bio.Core.Matcher.Token.IToken
//    {

//        private static readonly ILog LOG = LogManager.GetLogger(typeof(TokenAN050101));


//        #region Private Properties Implementation of IToken

//        private int _operationType;

//        private int _authenticationFactor;

//        private int _minutiaeType;

//        private int _datatypetoken;

//        private int _bodyPart;

//        private List<Sample> _samples;

//        private string _additionalData;

//        private string _getData;

//        private string _setData;

//        private string _typeid;

//        private string _valueid;

//        private string _clientid;

//        private string _ipenduser;

//        private string _enduser;

//        #endregion Private Properties Implementation of IToken

//        #region Public Properties Implementation of IToken

//        /// <summary>
//        /// 1-Verify | 2-Enroll
//        /// </summary>
//        public int OperationType
//        {
//            get { return _operationType; }
//            set { _operationType = value; }
//        }

//        /// <summary>
//        /// Tecnologia a la que corresponde el template
//        /// </summary>
//        public int AuthenticationFactor
//        {
//            get { return _authenticationFactor; }
//            set { _authenticationFactor = value; }
//        }

//        /// <summary>
//        /// Tipo de minucia que contiene Data, incluidos WSQ, RAW, etc.
//        /// </summary>
//        public int MinutiaeType
//        {
//            get { return _minutiaeType; }
//            set { _minutiaeType = value; }
//        }

//        ///// <summary>
//        ///// Tipo de informacion (data) que contendra el template (desde BIR_TYPE)
//        ///// </summary>
//        //public int Type
//        //{
//        //    get { return _type; }
//        //    set { _type = value; }
//        //}

//        /// <summary>
//        /// Parte del cuerpo si es tecnologia biometrica
//        /// </summary>
//        public int BodyPart
//        {
//            get { return _bodyPart; }
//            set { _bodyPart = value; }
//        }

//        /// <summary>
//        /// [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
//        /// </summary>
//        public int DataTypeToken
//        {
//            get { return _datatypetoken; }
//            set { _datatypetoken = value; }
//        }

//        /// <summary>
//        /// Data propia del template o sample
//        /// </summary>
//        public List<Sample> Samples
//        {
//            get { return _samples; }
//            set { _samples = value; }
//        }

//        /// <summary>
//        /// Datos adicionales segun sea la data. Por ejemplo si es WSQ, 
//        /// se puede colocar las dimensiones de la forma w=512|h=512, o si
//        /// es template NEC, el coeficiente coef=xxx. Si hay mas de uno separado 
//        /// por pipe "|"
//        /// </summary>
//        public string AdditionalData
//        {
//            get { return _additionalData; }
//            set { _additionalData = value; }
//        }

//        /// <summary>
//        /// Obtiene la Data en base 64
//        /// </summary>
//        public string GetData
//        {
//            get { return _getData; }
//        }

//        /// <summary>
//        /// Setea la data desde base 64
//        /// </summary>
//        public string SetData
//        {
//            set { _setData = value; }
//        }

//        /// Tipo ID de la persona (RUT, DNI, etc) 
//        /// </summary>
//        public string Typeid
//        {
//            get { return _typeid; }
//            set { _typeid = value; }
//        }

//        /// Valor ID de la persona (RUT, DNI, etc) 
//        /// </summary>
//        public string Valueid
//        {
//            get { return _valueid; }
//            set { _valueid = value; }
//        }

//        /// <summary>
//        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
//        /// un default NE = No Exist 
//        /// </summary>
//        public string Clientid
//        {
//            get { return _clientid; }
//            set { _clientid = value; }
//        }

//        /// <summary>
//        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
//        /// </summary>
//        public string Ipenduser
//        {
//            get { return _ipenduser; }
//            set { _ipenduser = value; }
//        }

//        /// <summary>
//        /// Usuario final de quien es el sample
//        /// </summary>
//        public string Enduser
//        {
//            get { return _enduser; }
//            set { _enduser = value; }
//        }

//        #endregion Public Properties Implementation of IToken

//        #region Public Method

//        /// <summary>
//        /// Constructor vacio
//        /// </summary>
//        public TokenAN050101() { }

//        /// <summary>
//        /// Constructor que recibe token y extrae
//        /// </summary>
//        /// <param name="token"></param>
//        public TokenAN050101(string token)
//        {
//            Extract(token);
//        }


//        /// <summary>
//        /// Extrae el contenido del token y llena las propiedades del objeto
//        /// </summary>
//        /// <param name="token">Token a extraer</param>
//        /// <returns></returns>
//        public int Extract(string token)
//        {
//            int ret = Errors.IERR_OK; 
//            try
//            {
//                LOG.Debug("TokenAN050101.Extract - Token = " + token);
//                //Si ES NULO O NO EMPIEZA CON av0305 retorno error
//                if (token == null ||
//                    ((!token.Trim().Substring(0, 8).Equals("AN050101")) && (!token.Trim().Substring(0, 8).Equals("AJ050101"))))
//                    return Errors.IERR_BAD_PARAMETER;

//                //Si viene de Java => Extaigo con uno especial porque viene RAW y no WSQ
//                if (token.Trim().Substring(0, 8).Equals("AJ050101"))
//                {
//                    return ExtractJ(token);
//                }

//                //Saco encabezado
//                string auxtoken = token.Trim().Substring(8);
//                LOG.Debug("TokenAN050101.Extract - auxtoken = " + auxtoken);
//                //Desencripto la data que viene en Hexa
//                //BKclassCryptoClass oCrypt = new BKclassCryptoClass();
//                string dataDecrypted = null;
//                try
//                {
//                    //dataDecrypted = oCrypt.Decrypt(auxtoken, "BioPortalClientAx14042010", true,
//                    //                                      frezCryptoEncryptionType.frezBlockEncryption);
//                    dataDecrypted = auxtoken; // Crypto.DecryptWithSymmetricAid(Crypto.PRIVATE_KEY, auxtoken);
//                    LOG.Debug("TokenAN050101.Extract - Token decrypted = " + dataDecrypted);
//                    if (dataDecrypted == null) return Errors.IERR_INVALID_TOKEN;
//                }
//                catch (Exception exdec)
//                {
//                    LOG.Error("TokenAN050101.Extract - Desencriptando - ", exdec);
//                    return Errors.IERR_INVALID_TOKEN;
//                }

//                //Separo segun patron de separacion
//                dataDecrypted = dataDecrypted.Replace("&#x0;", "");
//                string[] aData = dataDecrypted.Split('|');
//                LOG.Debug("TokenAN050101 - Len(aData)=" + aData.Length);
//                //Lleno propiedades desde token
//                /*
//                   Pos > 0 - Tipo_Doc
//                   Pos > 1 - Nro_Doc
//                   Pos > 2 - Si es DP Serial Number
//                   Pos > 3 - [1-10] Dedo  = BodyPart
//                   Pos > 4 - WSQ en B64 si sContenido = 0,1
//                   Pos > 5 - Template en B64 si sContenido = 0,2 (NEC o DP)
//                   Pos > 6 - Timestamp Cliente [ddmmyyyyhhmmss]
//                   Pos > 7 - IP cliente
//                   Pos > 8 - [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
//                   Pos > 9 - [1-NEC | 2-DP] Formato del Template
//                   Pos > 10 - [UNKNOW-0|DP-1|SEC-2|BIOI-3|CROSSMATCH-4] Dispositivo
//                   Pos > 11 - AutenticationFactor
//                   Pos > 12 - OperationType
//                 * 
//                 * Saco porque no vienen nusnca Pos > 10 - Coeficiente = AdditionalData
                   
//                 */
//                _operationType = Convert.ToInt32(aData[12]);
//                _authenticationFactor = Convert.ToInt32(aData[11]);
//                _minutiaeType = Convert.ToInt32(aData[9]);
//                _bodyPart = Convert.ToInt32(aData[3]);
//                _samples = new List<Sample>();
//                _datatypetoken = Convert.ToInt32(aData[8]);

//                //Si viene Tewmplate, lo pongo primero para qyue verifique por el 
//                string auxdata;
//                if (_datatypetoken == 0 || _datatypetoken == 2)
//                {
//                    //Si es Password, aplico hash
//                    if (_authenticationFactor == Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
//                    {
//                        auxdata = aData[5]; // Encoding.ASCII.GetString(aData[5]);
//                        auxdata = FormsAuthentication.HashPasswordForStoringInConfigFile(auxdata, "SHA1");
//                    }
//                    else
//                    {    //Sino paso tal cual viene
//                        auxdata = aData[5];
//                    }

//                    //Added 01-11-2013 para agregar soprote a M;inucias ANSI y ISO que vienene en nuevo Token de BPClient.NET
//                    //Con SDK de DP v2.2.0
//                    string[] sep = new string[1] { "-septmpl-" };
//                    string[] sa = auxdata.Split(sep, StringSplitOptions.RemoveEmptyEntries);
//                    LOG.Debug("TokenAN050101 - Len(sa)=" + sa.Length);

//                    Sample sTmpl = null;
//                    Sample sTmpl1 = null;
//                    Sample sTmpl2 = null;
//                    string _datatANSI378;
//                    string _datatISO19794_2;
//                    string _templDP;
//                    //Added apra diferenciar el token de Grabba
//                    if (sa != null && sa.Length <= 2)
//                    {
//                        sTmpl = new Sample
//                        {
//                            Data = "",
//                            Additionaldata = "", //aData[10],
//                            Device = Convert.ToInt32(aData[11]),
//                            Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
//                        };
//                        if (sa.Length == 1)
//                        {
//                            if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
//                            {
//                                LOG.Debug("TokenAN050101 - Seteando ANSI...");
//                                _datatANSI378 = sa[0];
//                                //Add ANSI 378
//                                sTmpl1 = new Sample
//                                {
//                                    Data = _datatANSI378,
//                                    Additionaldata = "", //aData[10],
//                                    Device = Convert.ToInt32(aData[11]),
//                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
//                                };
//                                sTmpl2 = new Sample
//                                {
//                                    Data = "",
//                                    Additionaldata = "", //aData[10],
//                                    Device = Convert.ToInt32(aData[11]),
//                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
//                                };
//                            }
//                            else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
//                            {
//                                LOG.Debug("TokenAN050101 - Seteando ISO...");
//                                _datatISO19794_2 = sa[0];
//                                //Add ISO 19794-2
//                                sTmpl2 = new Sample
//                                {
//                                    Data = _datatISO19794_2,
//                                    Additionaldata = "", //aData[10],
//                                    Device = Convert.ToInt32(aData[11]),
//                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
//                                };
//                                sTmpl1 = new Sample
//                                {
//                                    Data = "",
//                                    Additionaldata = "", //aData[10],
//                                    Device = Convert.ToInt32(aData[11]),
//                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
//                                };

//                            }
//                        }
//                        else //es 2 => ANSI e ISO en ese orden
//                        {
//                            LOG.Debug("TokenAN050101 - Seteando ANSI...");
//                            _datatANSI378 = sa[0];
//                            //Add ANSI 378
//                            sTmpl1 = new Sample
//                            {
//                                Data = _datatANSI378,
//                                Additionaldata = "", //aData[10],
//                                Device = Convert.ToInt32(aData[11]),
//                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
//                            };
//                            LOG.Debug("TokenAN050101 - Seteando ISO...");
//                            _datatISO19794_2 = sa[1];
//                            //Add ISO 19794-2
//                            sTmpl2 = new Sample
//                            {
//                                Data = _datatISO19794_2,
//                                Additionaldata = "", //aData[10],
//                                Device = Convert.ToInt32(aData[11]),
//                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
//                            };
//                        }
//                    }
//                    else
//                    {
//                        _templDP = sa[0] + "-septmpl-" + sa[1];
//                        //Add DP enroll y verify template concatenados. Si no es DP, debe ser de minucias propietarias que se generen. 
//                        sTmpl = new Sample
//                        {
//                            Data = _templDP,
//                            Additionaldata = "", //aData[10],
//                            Device = Convert.ToInt32(aData[11]),
//                            Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
//                        };
//                        //_samples.Add(sTmpl);
                       
//                        if (sa.Length > 2)
//                        {
//                            LOG.Debug("TokenAN050101 - Seteando ANSI...");
//                            _datatANSI378 = sa[2];
//                            //Add ANSI 378
//                            sTmpl1 = new Sample
//                            {
//                                Data = _datatANSI378,
//                                Additionaldata = "", //aData[10],
//                                Device = Convert.ToInt32(aData[11]),
//                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
//                            };
//                            //_samples.Add(sTmpl1);
//                        }
//                        if (sa.Length > 3)
//                        {
//                            LOG.Debug("TokenAN050101 - Seteando ISO...");
//                            _datatISO19794_2 = sa[3];
//                            //Add ISO 19794-2
//                            sTmpl2 = new Sample
//                            {
//                                Data = _datatISO19794_2,
//                                Additionaldata = "", //aData[10],
//                                Device = Convert.ToInt32(aData[11]),
//                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
//                            };
//                            //_samples.Add(sTmpl2);
//                        }
//                    }

//                    //Added 05-2014 para armar el orden d elos templates, porque si es ANSI o ISO deben ir primero
//                    //              por como se verifica en Service.Manager.Verify, que si viene un template generado
//                    //              => Verifica con el Minutiaetype indicado pero con el sample[0] (el primero d ela lista)
//                    if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
//                    {
//                        LOG.Error("TokenAN050101.Extract - Add ANSI/OTW/ISO");
//                        _samples.Add(sTmpl1);  //Primero ANSI
//                        _samples.Add(sTmpl);   //Segundo OTW
//                        _samples.Add(sTmpl2);  //Tercero ISO
//                    }
//                    else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
//                    {
//                        LOG.Error("TokenAN050101.Extract - ISO/OTW/ANSI");
//                        _samples.Add(sTmpl2);  //Primero ISO
//                        _samples.Add(sTmpl);   //Segundo OTW
//                        _samples.Add(sTmpl1);  //Tercero ANSI
//                    }
//                    else //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW)
//                    {
//                        LOG.Error("TokenAN050101.Extract - Add OTW/ANSI/ISO");
//                        _samples.Add(sTmpl);  //Primero OTW o Secugen o el que sea
//                        if (sTmpl1 != null) _samples.Add(sTmpl1); //Segundo ANSI    Agrego chequeo de NULL, para hacerlo compatible con anteriores versiones de 
//                        if (sTmpl2 != null) _samples.Add(sTmpl2); //Tercero ISO     TokenAN050101
//                    }

//                }
//                //Si viene WSQ, lo agrego para verificacion si no viene Template
//                if (_datatypetoken == 0 || _datatypetoken == 1)  //Viene WSQ
//                {
//                    LOG.Debug("TokenAN050101.Extract - Add WSQ");
//                    string wsq512x512 = aData[4];
//                    LOG.Debug("TokenAN050101.Extract - Entrando Check WSQ Size...");
//                    int ierr = CheckSizeWSQ(aData[4], out wsq512x512);
//                    LOG.Debug("TokenAN050101.Extract - Saliendo Check WSQ Size ierr = " + ierr);
//                    Sample sWsq = new Sample
//                    {
//                        Data = wsq512x512,
//                        Additionaldata = aData[10],
//                        Device = Convert.ToInt32(aData[11]),
//                        Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ
//                    };
//                    _samples.Add(sWsq);
//                }
//                _typeid = aData[0];
//                _valueid = aData[1];
//                _clientid = aData[2].Trim();
//                _ipenduser = aData[7].Trim();
//                _enduser = aData[7].Trim();
//            }
//            catch (Exception ex)
//            {
//                ret = Errors.IERR_UNKNOWN;
//                LOG.Error("TokenAN050101.Extarct", ex);
//            }
//            LOG.Debug("TokenAN050101.Extract - Out!");
//            return ret;
//        }

//        private int ExtractJ(string token)
//        {
//            int ret = Errors.IERR_OK;
//            try
//            {
//                LOG.Debug("TokenAN050101.ExtractJ - Token = " + token);
//                //Si ES NULO O NO EMPIEZA CON av0305 retorno error
//                if (token == null ||
//                    ((!token.Trim().Substring(0, 8).Equals("AJ050101"))))
//                    return Errors.IERR_BAD_PARAMETER;

//                //Saco encabezado
//                string auxtoken = token.Trim().Substring(8);
//                LOG.Debug("TokenAN050101.ExtractJ - auxtoken = " + auxtoken);
//                //Desencripto la data que viene en Hexa
//                //BKclassCryptoClass oCrypt = new BKclassCryptoClass();
//                string dataDecrypted = null;
//                try
//                {
//                    //dataDecrypted = oCrypt.Decrypt(auxtoken, "BioPortalClientAx14042010", true,
//                    //                                      frezCryptoEncryptionType.frezBlockEncryption);
//                    dataDecrypted = auxtoken; // Crypto.DecryptWithSymmetricAid(Crypto.PRIVATE_KEY, auxtoken);
//                    LOG.Debug("TokenAN050101.ExtractJ - Token decrypted = " + dataDecrypted);
//                    if (dataDecrypted == null) return Errors.IERR_INVALID_TOKEN;
//                }
//                catch (Exception exdec)
//                {
//                    LOG.Error("TokenAN050101.ExtractJ - Desencriptando - ", exdec);
//                    return Errors.IERR_INVALID_TOKEN;
//                }

//                //Separo segun patron de separacion
//                dataDecrypted = dataDecrypted.Replace("&#x0;", "");
//                string[] aData = dataDecrypted.Split('|');
//                LOG.Debug("TokenAN050101.ExtractJ - Len(aData)=" + aData.Length);
//                //Lleno propiedades desde token
//                /*
//                   Pos > 0 - Tipo_Doc
//                   Pos > 1 - Nro_Doc
//                   Pos > 2 - Si es DP Serial Number
//                   Pos > 3 - [1-10] Dedo  = BodyPart
//                   Pos > 4 - WSQ en B64 si sContenido = 0,1
//                   Pos > 5 - Template en B64 si sContenido = 0,2 (NEC o DP)
//                   Pos > 6 - Timestamp Cliente [ddmmyyyyhhmmss]
//                   Pos > 7 - IP cliente
//                   Pos > 8 - [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
//                   Pos > 9 - [1-NEC | 2-DP] Formato del Template
//                   Pos > 10 - [UNKNOW-0|DP-1|SEC-2|BIOI-3|CROSSMATCH-4] Dispositivo
//                   Pos > 11 - AutenticationFactor
//                   Pos > 12 - OperationType
//                 * 
//                 * Saco porque no vienen nusnca Pos > 10 - Coeficiente = AdditionalData
                   
//                 */
//                _operationType = Convert.ToInt32(aData[12]);
//                _authenticationFactor = Convert.ToInt32(aData[11]);
//                _minutiaeType = Convert.ToInt32(aData[9]);
//                _bodyPart = Convert.ToInt32(aData[3]);
//                _samples = new List<Sample>();
//                _datatypetoken = Convert.ToInt32(aData[8]);

//                //Si viene Tewmplate, lo pongo primero para qyue verifique por el 
//                string auxdata;
//                if (_datatypetoken == 0 || _datatypetoken == 2)
//                {
//                    //Si es Password, aplico hash
//                    if (_authenticationFactor == Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
//                    {
//                        auxdata = aData[5]; // Encoding.ASCII.GetString(aData[5]);
//                        auxdata = FormsAuthentication.HashPasswordForStoringInConfigFile(auxdata, "SHA1");
//                    }
//                    else
//                    {    //Sino paso tal cual viene
//                        auxdata = aData[5];
//                    }

//                    //Added 01-11-2013 para agregar soprote a M;inucias ANSI y ISO que vienene en nuevo Token de BPClient.NET
//                    //Con SDK de DP v2.2.0
//                    string[] sep = new string[1] { "-septmpl-" };
//                    string[] sa = auxdata.Split(sep, StringSplitOptions.RemoveEmptyEntries);
//                    LOG.Debug("TokenAN050101.ExtractJ - Len(sa)=" + sa.Length);

//                    Sample sTmpl = null;
//                    Sample sTmpl1 = null;
//                    Sample sTmpl2 = null;
//                    string _datatANSI378;
//                    string _datatISO19794_2;
//                    string _templDP;
//                    //Added apra diferenciar el token de Grabba
//                    if (sa != null && sa.Length <= 2)
//                    {
//                        sTmpl = new Sample
//                        {
//                            Data = "",
//                            Additionaldata = "", //aData[10],
//                            Device = Convert.ToInt32(aData[11]),
//                            Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
//                        };
//                        if (sa.Length == 1)
//                        {
//                            if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
//                            {
//                                LOG.Debug("TokenAN050101.ExtractJ - Seteando ANSI...");
//                                _datatANSI378 = sa[0];
//                                //Add ANSI 378
//                                sTmpl1 = new Sample
//                                {
//                                    Data = _datatANSI378,
//                                    Additionaldata = "", //aData[10],
//                                    Device = Convert.ToInt32(aData[11]),
//                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
//                                };
//                                sTmpl2 = new Sample
//                                {
//                                    Data = "",
//                                    Additionaldata = "", //aData[10],
//                                    Device = Convert.ToInt32(aData[11]),
//                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
//                                };
//                            }
//                            else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
//                            {
//                                LOG.Debug("TokenAN050101.ExtractJ - Seteando ISO...");
//                                _datatISO19794_2 = sa[0];
//                                //Add ISO 19794-2
//                                sTmpl2 = new Sample
//                                {
//                                    Data = _datatISO19794_2,
//                                    Additionaldata = "", //aData[10],
//                                    Device = Convert.ToInt32(aData[11]),
//                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
//                                };
//                                sTmpl1 = new Sample
//                                {
//                                    Data = "",
//                                    Additionaldata = "", //aData[10],
//                                    Device = Convert.ToInt32(aData[11]),
//                                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
//                                };

//                            }
//                        }
//                        else //es 2 => ANSI e ISO en ese orden
//                        {
//                            LOG.Debug("TokenAN050101.ExtractJ - Seteando ANSI...");
//                            _datatANSI378 = sa[0];
//                            //Add ANSI 378
//                            sTmpl1 = new Sample
//                            {
//                                Data = _datatANSI378,
//                                Additionaldata = "", //aData[10],
//                                Device = Convert.ToInt32(aData[11]),
//                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
//                            };
//                            LOG.Debug("TokenAN050101.ExtractJ - Seteando ISO...");
//                            _datatISO19794_2 = sa[1];
//                            //Add ISO 19794-2
//                            sTmpl2 = new Sample
//                            {
//                                Data = _datatISO19794_2,
//                                Additionaldata = "", //aData[10],
//                                Device = Convert.ToInt32(aData[11]),
//                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
//                            };
//                        }
//                    }
//                    else
//                    {
//                        _templDP = sa[0] + "-septmpl-" + sa[1];
//                        //Add DP enroll y verify template concatenados. Si no es DP, debe ser de minucias propietarias que se generen. 
//                        sTmpl = new Sample
//                        {
//                            Data = _templDP,
//                            Additionaldata = "", //aData[10],
//                            Device = Convert.ToInt32(aData[11]),
//                            Minutiaetype = _minutiaeType //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW //Convert.ToInt32(aData[9])
//                        };
//                        //_samples.Add(sTmpl);

//                        if (sa.Length > 2)
//                        {
//                            LOG.Debug("TokenAN050101.ExtractJ - Seteando ANSI...");
//                            _datatANSI378 = sa[2];
//                            //Add ANSI 378
//                            sTmpl1 = new Sample
//                            {
//                                Data = _datatANSI378,
//                                Additionaldata = "", //aData[10],
//                                Device = Convert.ToInt32(aData[11]),
//                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
//                            };
//                            //_samples.Add(sTmpl1);
//                        }
//                        if (sa.Length > 3)
//                        {
//                            LOG.Debug("TokenAN050101.ExtractJ - Seteando ISO...");
//                            _datatISO19794_2 = sa[3];
//                            //Add ISO 19794-2
//                            sTmpl2 = new Sample
//                            {
//                                Data = _datatISO19794_2,
//                                Additionaldata = "", //aData[10],
//                                Device = Convert.ToInt32(aData[11]),
//                                Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005
//                            };
//                            //_samples.Add(sTmpl2);
//                        }
//                    }

//                    //Added 05-2014 para armar el orden d elos templates, porque si es ANSI o ISO deben ir primero
//                    //              por como se verifica en Service.Manager.Verify, que si viene un template generado
//                    //              => Verifica con el Minutiaetype indicado pero con el sample[0] (el primero d ela lista)
//                    if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004)
//                    {
//                        LOG.Error("TokenAN050101.ExtractJ - Add ANSI/OTW/ISO");
//                        _samples.Add(sTmpl1);  //Primero ANSI
//                        _samples.Add(sTmpl);   //Segundo OTW
//                        _samples.Add(sTmpl2);  //Tercero ISO
//                    }
//                    else if (Convert.ToInt32(aData[9]) == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005)
//                    {
//                        LOG.Error("TokenAN050101.ExtractJ - ISO/OTW/ANSI");
//                        _samples.Add(sTmpl2);  //Primero ISO
//                        _samples.Add(sTmpl);   //Segundo OTW
//                        _samples.Add(sTmpl1);  //Tercero ANSI
//                    }
//                    else //Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_DIGITALPERSONAOTW)
//                    {
//                        LOG.Error("TokenAN050101.ExtractJ - Add OTW/ANSI/ISO");
//                        _samples.Add(sTmpl);  //Primero OTW o Secugen o el que sea
//                        if (sTmpl1 != null) _samples.Add(sTmpl1); //Segundo ANSI    Agrego chequeo de NULL, para hacerlo compatible con anteriores versiones de 
//                        if (sTmpl2 != null) _samples.Add(sTmpl2); //Tercero ISO     TokenAN050101
//                    }

//                }
//                //Si viene WSQ, lo agrego para verificacion si no viene Template
//                if (_datatypetoken == 0 || _datatypetoken == 1)  //Viene WSQ
//                {
//                    LOG.Debug("TokenAN050101.ExtractJ - Add WSQ");
//                    LOG.Debug("TokenAN050101.ExtractJ - Entrando Generate WSQ desde = " + aData[4]);
//                    //(orig 300x400)
//                    byte[] raworig = Convert.FromBase64String(aData[4]);
//                    byte[] raw512x512 = Bio.Core.Imaging.ImageProcessor.Adjust(raworig, 300, 400, 512, 512);
//                    LOG.Debug("TokenAN050101.ExtractJ - RAW Ajustada de 300x400 => 512x512...");
//                    Bio.Core.Wsq.Encoder.WsqEncoder encoder = new Wsq.Encoder.WsqEncoder();
//                    short w512 = 512;
//                    short h512 = 512;
//                    byte[] wsq512x512;
//                    string strwsq512x512;
//                    if (encoder.EncodeMemory(raw512x512, w512, h512, out wsq512x512))
//                    {
//                        strwsq512x512 = Convert.ToBase64String(wsq512x512);
//                        LOG.Debug("TokenAN050101.ExtractJ - WSQ creado desde RAW Ajustada de 300x400 => 512x512 = " + strwsq512x512);
//                    }
//                    else
//                    {
//                        strwsq512x512 = aData[4];
//                        LOG.Debug("TokenAN050101.ExtractJ - WSQ NO creado desde RAW Ajustada de 300x400 => 512x512! => Se setea RAW original = " + strwsq512x512);
//                    }
//                    Sample sWsq = new Sample
//                    {
//                        Data = strwsq512x512,
//                        Additionaldata = aData[10],
//                        Device = Convert.ToInt32(aData[11]),
//                        Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ
//                    };
//                    _samples.Add(sWsq);
//                }
//                _typeid = aData[0];
//                _valueid = aData[1];
//                _clientid = aData[2].Trim();
//                _ipenduser = aData[7].Trim();
//                _enduser = aData[7].Trim();
//            }
//            catch (Exception ex)
//            {
//                ret = Errors.IERR_UNKNOWN;
//                LOG.Error("TokenAN050101.ExtarctJ", ex);
//            }
//            LOG.Debug("TokenAN050101.ExtractJ - Out!");
//            return ret;
//        }

//        private int CheckSizeWSQ(string wsqorig, out string strwsq512x512)
//        {
//            int ierr = 0;
//            strwsq512x512 = wsqorig;
//            try
//            {
//                LOG.Debug("TokenAN050101.CheckSizeWSQ - Entrando...");
//                LOG.Debug("TokenAN050101.CheckSizeWSQ - Convieritnedo desde B64 wsqorig =" + wsqorig);
//                byte[] bywsqorig = Convert.FromBase64String(wsqorig);
//                LOG.Debug("TokenAN050101.CheckSizeWSQ - Size byte WSQ Orig bywsqorig.Length = " + bywsqorig.Length);
//                byte[] raw;
//                short worig = 0;
//                short horig = 0;
//                Bio.Core.Wsq.Decoder.WsqDecoder decoder = new Wsq.Decoder.WsqDecoder();
//                if (decoder.DecodeMemory(bywsqorig, out worig, out horig, out raw)) {
//                    LOG.Debug("TokenAN050101.CheckSizeWSQ - WSQ Decoded => worig = " + worig + " x horig = " + horig);
//                    if (worig != 512 || horig != 512)
//                    {
//                        byte[] raw512x512 = Bio.Core.Imaging.ImageProcessor.Adjust(raw, worig, horig, 512, 512);
//                        Bio.Core.Wsq.Encoder.WsqEncoder encoder = new Wsq.Encoder.WsqEncoder();
//                        short w512 = 512;
//                        short h512 = 512;
//                        byte[] wsq512x512;
//                        if (encoder.EncodeMemory(raw512x512, w512, h512, out wsq512x512))
//                        {
//                            strwsq512x512 = Convert.ToBase64String(wsq512x512);
//                            LOG.Debug("TokenAN050101.CheckSizeWSQ - RAW relleno con blanco en strwsq512x512");
//                        }
//                        else
//                        {
//                            strwsq512x512 = wsqorig;
//                            LOG.Debug("TokenAN050101.CheckSizeWSQ - RAW relleno con wsqorig => No pudo comprimir!");
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                 LOG.Error("TokenAN050101.CheckSizeWSQ", ex);
//                 ierr = -1;
//            }
//            return ierr;
//        }

//        /// <summary>
//        /// Extrae el contenido del token y llena las propiedades del objeto y ademas de llenar las prop del bjeto, 
//        /// setea las variable que corresponden a un objeto XmlParamIn
//        /// </summary>
//        /// <param name="token">token en base 64</param>
//        /// <param name="newparamin">Objeto XmlParamIn con datos extraidos desde token</param>
//        /// <returns></returns>
//        public int ExtractToParameters(string token, out string newparamin)
//        {
//            throw new NotImplementedException();
//        }

//        public void Dispose()
//        {

//        }

//#endregion Public Method

//    }
//}
