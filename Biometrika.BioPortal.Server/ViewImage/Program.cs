﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewImage
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            double d = GetPorcentajeScoreNegativo(1.134567456, 60, 1.1);

            d = GetPorcentajeScoreNegativo(2.134567456, 60, 1.1);

            GetLocationProperty();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }

        static void GetLocationProperty()
        {
            GeoCoordinateWatcher watcher = new GeoCoordinateWatcher();

            // Do not suppress prompt, and wait 1000 milliseconds to start.
            watcher.TryStart(false, TimeSpan.FromMilliseconds(10000));

            for (int i = 1; i < 10000; i++)
            {

            }

            GeoCoordinate coord = watcher.Position.Location;

            if (coord.IsUnknown != true)
            {
                Console.WriteLine("Lat: {0}, Long: {1}",
                    coord.Latitude,
                    coord.Longitude);
            }
            else
            {
                Console.WriteLine("Unknown latitude and longitude.");
            }
        }

        private static double GetPorcentajeScoreNegativo(double score, double threshold, double tV2D_Threshold)
        {
            double ret = 0;
            double _BASE = 50;
            try
            {
                try
                {
                    //LOG.Debug("BPUnitOfWork.GetPorcentajeScoreNegativo IN => score = " + score.ToString() + "/threshold = " + tV2D_Threshold);
                    _BASE = threshold;
                    if (_BASE == 0)
                        _BASE = 50;
                    //LOG.Debug("BPUnitOfWork.GetPorcentajeScoreNegativo - _BASE = " + _BASE.ToString());
                }
                catch (Exception ex1)
                {
                    //LOG.Error("BPHelper.GetPorcentajeScoreNegativo - " +
                    //    "Falta configurar o mal configurado en web.config CIScorePorcentajeBase [Error=" +
                    //    ex1.Message + "] => Default=75");
                }

                //double rts = (25 * (treshold - score)) / treshold;
                double rts = ((100 - _BASE) * (tV2D_Threshold - score)) / tV2D_Threshold;
                //LOG.Debug("BPUnitOfWork.GetPorcentajeScoreNegativo => rts = " + rts.ToString());

                //ret = 75 + rts;
                ret = _BASE + rts;
                //LOG.Debug("BPHelper.GetPorcentajeScoreNegativo Porcentaje Seguridad => " + ret.ToString("##.##") + "%");
            }
            catch (Exception ex)
            {
                ret = 49;// Properties.Settings.Default.BPWebScorePorcentajeBase; // 75;
                //LOG.Error("BPHelper.GetPorcentajeScoreNegativo Error => " + ex.Message);
            }
            //LOG.Debug("BPHelper.GetPorcentajeScoreNegativo OUT!");
            return ret;
        }
    }
}
