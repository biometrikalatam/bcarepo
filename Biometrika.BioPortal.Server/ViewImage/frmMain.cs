﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Device.Location;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bio.Core.Imaging;
using Bio.Core.Wsq.Decoder;
using Bio.Core.Wsq.Encoder;

namespace ViewImage
{
    public partial class frmMain : Form
    {
        private GeoCoordinateWatcher Watcher = null;

        public frmMain()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                if (!String.IsNullOrEmpty(openFileDialog1.FileName))
                {
                    try
                    {
                        textBox3.Text = openFileDialog1.FileName;
                        System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                        string s = sr.ReadToEnd();
                        sr.Close();
                        richTextBox1.Text = s;
                    }
                    catch (Exception ex)
                    {
                        richTextBox1.Text = "Error = " + ex.Message;
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (radioButton1.Checked) //WSQ
                {
                    Bio.Core.Wsq.Decoder.WsqDecoder decoder = new WsqDecoder();
                    short w, h;
                    byte[] byRAW;
                    if (decoder.DecodeMemory(Convert.FromBase64String(richTextBox1.Text.Trim()), out w, out h, out byRAW))
                    {
                        Image img = Bio.Core.Imaging.ImageProcessor.RawToBitmap(byRAW, w, h);
                        picHuella.Image = img;
                    }
                }
                else if (radioButton2.Checked) //RAW
                {
                    byte[] by = Convert.FromBase64String(richTextBox1.Text.Trim());
                    Image img = Bio.Core.Imaging.ImageProcessor.RawToBitmap(Convert.FromBase64String(richTextBox1.Text.Trim()),
                        Convert.ToInt16(textBox1.Text.Trim()), Convert.ToInt16(textBox2.Text.Trim()));
                    picHuella.Image = img;
                }
            }
            catch (Exception ex)
            {
                richTextBox1.Text = "Error = " + ex.Message;
            }
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
                if (!String.IsNullOrEmpty(textBox3.Text.Trim()))
                {
                    try
                    {
                        textBox3.Text = openFileDialog1.FileName;
                        System.IO.StreamReader sr = new System.IO.StreamReader(textBox3.Text.Trim());
                        string s = sr.ReadToEnd();
                        sr.Close();
                        richTextBox1.Text = s;
                    }
                    catch (Exception ex)
                    {
                        richTextBox1.Text = "Error = " + ex.Message;
                    }
                }
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ConvertImageDPRawTo357x392Raw(Convert.FromBase64String(richTextBox1.Text.Trim()), false,
                Convert.ToInt16(textBox1.Text), Convert.ToInt16(textBox2.Text),
                Convert.ToInt16(textBox5.Text), Convert.ToInt16(textBox4.Text));
        }


        internal byte[] ConvertImageDPRawTo357x392Raw(byte[] imageDPRaw, bool wsqCompressed, int wRaw, int hRaw, int wFinal, int hFinal)
        {
            //Los parametros wRaw y hRaw vienen seteados si compressed es false, 
            //sino se calculan dentro de WsqTools.Decompress
            int w = 0;
            int h = 0;
            int dpix = 500;
            int dpiy = 500;
            int ww = 0; //Convert.ToInt32(pSample.Height);
            int hh = 0; //Convert.ToInt32(pSample.Width);
            ww = wFinal; //(ww * dpix) / sensor.Xdpi;
            hh = hFinal; //(hh * dpiy) / sensor.Ydpi;
            byte[] byDataRawD;

            try
            {
                //Si es Raw comprimido, descomprimo
                if (wsqCompressed)
                {
                    WsqDecoder dec = new WsqDecoder();
                    short sw = 0;
                    short sh = 0;
                    if (!dec.DecodeMemory(imageDPRaw, out sw, out sh, out byDataRawD))
                    {
                        return null;
                    }
                    else
                    {
                        w = sw;
                        h = sh;
                    }
                }
                else
                {
                    byDataRawD = imageDPRaw;
                    w = wRaw;
                    h = hRaw;
                }

                byte[] aux = new byte[275000];
                if (imageDPRaw.Length == 275050)
                {
                    for (int i = 50; i < imageDPRaw.Length; i++)
                    {
                        aux[i - 50] = imageDPRaw[i];
                    }
                }
                byDataRawD = aux;

                //Creo Bitmap para redimensionar a tamaño comun para comparaciones
                Bitmap img = ImageProcessor.RawToBitmap(byDataRawD, w, h);
                Bitmap bmp = new Bitmap(ww, hh, PixelFormat.Format24bppRgb);
                bmp.SetResolution(dpix, dpiy);
                Graphics gr = Graphics.FromImage(bmp);
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.DrawImage(img, new Rectangle(0, 0, ww, hh), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel);
                gr.Dispose();
                byte[] imgData = ImageProcessor.BitmapToRaw(bmp);
                byte[] imgData512 = ImageProcessor.Adjust(imgData, ww, hh, 512, 512);

                //picHuella.Image = Bio.Core.Imaging.ImageProcessor.RawToBitmap(imgData512, 512, 512);
                WsqEncoder enc = new WsqEncoder();
                byte[] wsq;
                if (enc.EncodeMemory(imgData512, 512, 512, out wsq))
                {
                    richTextBox1.Text = Convert.ToBase64String(wsq);
                }

                //img1.Save(@"C:\TFS\Proyectos\SENCE\Desarrollo\DataInput\bmp.bmp");

                return imgData512;
            }
            catch (Exception ex)
            {
                //LOG.Error("BiometrikaConverter.ConvertImageDPRawTo357x392Raw OUT!");
                richTextBox1.Text = ex.Message;
                return null;
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            // Create the watcher.
            Watcher = new GeoCoordinateWatcher();

            // Catch the StatusChanged event.
            Watcher.StatusChanged += Watcher_StatusChanged;

            // Start the watcher.
            Watcher.Start();
        }

        // The watcher's status has change. See if it is ready.
        private void Watcher_StatusChanged(object sender,
            GeoPositionStatusChangedEventArgs e)
        {
            if (e.Status == GeoPositionStatus.Ready)
            {
                // Display the latitude and longitude.
                if (Watcher.Position.Location.IsUnknown)
                {
                    txtLatitud.Text = "Cannot find location data";
                }
                else
                {
                    GeoCoordinate location =
                        Watcher.Position.Location;
                    txtLatitud.Text = location.Latitude.ToString();
                    txtLongitud.Text = location.Longitude.ToString();
                }
            }
        }
    }
}
