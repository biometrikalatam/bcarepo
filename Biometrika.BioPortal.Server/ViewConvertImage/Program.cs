﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ViewConvertImage
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {

               

                //double d = GetPorcentajeScoreNegativo(1.134567456, 60, 1.1);

        //d = GetPorcentajeScoreNegativo(2.134567456, 60, 1.1);

        //DateTime dt = DateTime.ParseExact("8/2/2022", "M/d/yyyy", null);
        //int edad = CalcularEdad(dt);
        //dt = DateTime.ParseExact("28/9/1969", "dd/M/yyyy", null);
        //edad = CalcularEdad(dt);
        //dt = DateTime.ParseExact("06/2/2008", "dd/M/yyyy", null);
        //edad = CalcularEdad(dt);
        //dt = DateTime.ParseExact("02/12/2001", "dd/MM/yyyy", null);
        //edad = CalcularEdad(dt);
        //dt = DateTime.ParseExact("9/28/1969", "M/dd/yyyy", null);
        //edad = CalcularEdad(dt);
        //dt = DateTime.ParseExact("1969-09-28", "yyyy/MM/dd", null);
        //edad = CalcularEdad(dt);
        //int i = 0;
    }
            catch (Exception ex)
            {

                string err = " Error: " + ex.Message;
            }


            //Random r = new Random(75);

            //double d = (double) r.Next(7500, 9800);
            //double score = (d / 100);
            //d = r.Next(7500, 9800);
            //score = d / 100;
            //d = r.Next(7500, 9800);
            //score = d / 100;
            //d = r.Next(7500, 9800);
            //score = d / 100;
            //d = r.Next(7500, 9800);
            //score = d / 100;


            //string s = " a ver ";
            //s = s.Replace(" ", string.Empty);

            string serial;
            string rut = SelectValueId("212844152", "A123456789", out serial);

            rut = SelectValueId("631452258", "21284415 2", out serial);

            string s = serial;
            testJson();


            IList<string> licensesMain = new List<string>(new string[] { "FingersExtractor", "FingersMatcher" });

            try
            {

                string ret = "<?xml version=\"1.0\" encoding=\"UTF - 8\"?><VerificationSource xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
                ret = ret.Substring(ret.IndexOf("<VerificationSource"));
                int i = 0;

                //Utils.ObtainLicenses(licensesMain);
                //Hashtable ht;
                //bool b = IsIdentityValidated("2,1,A,SRCeI|4,0,A", out ht);
                //ht = null;
                //b = false;
                //b = IsIdentityValidated("2,1,A,CI|4,1,M,Usuario1", out ht);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error. Details: " + ex.Message, "Fingers Sample", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public static int CalcularEdad(DateTime fechaNacimiento)
        {
            // Obtiene la fecha actual:
            DateTime fechaActual = DateTime.Today;

            // Comprueba que la se haya introducido una fecha válida; si 
            // la fecha de nacimiento es mayor a la fecha actual se muestra mensaje 
            // de advertencia:
            if (fechaNacimiento > fechaActual)
            {
                Console.WriteLine("La fecha de nacimiento es mayor que la actual.");
                return -1;
            }
            else
            {
                int edad = fechaActual.Year - fechaNacimiento.Year;

                // Comprueba que el mes de la fecha de nacimiento es mayor 
                // que el mes de la fecha actual:
                if (fechaNacimiento.Month >= fechaActual.Month)
                {
                    if (fechaNacimiento.Day > fechaActual.Day)
                    {
                        --edad;
                    }
                }

                return edad;
            }
        }

        public static bool IsIdentityValidated(string identity, out Hashtable htVerified)
        {
            //   Verificationsource = 2,1,A,SRCeI|4,0,A        => Finger verificado automatico en Registro Civil y facial no verificado
            //                        2,1,A,CI|4,1,M,Usuario1   => Finger verificado automatico contra cedula de identidad y facial manual por Usuario1
            bool ret = false;
            htVerified = new Hashtable();
            VerificationSource objVS;
            try
            {
                if (identity != null && identity != null)
                {
                    string[] allVS = identity.Split('|');

                    if (allVS == null || allVS.Length == 0)
                    {
                        htVerified = null;
                    }
                    else
                    {
                        foreach (var item in allVS)
                        {
                            string[] itemVS = item.Split(',');
                            if (itemVS != null && itemVS.Length >= 3)
                            {
                                objVS = new VerificationSource();
                                objVS.AuthenticationFactor = Convert.ToInt32(itemVS[0]);
                                objVS.StatusVerified = Convert.ToInt32(itemVS[1]);
                                objVS.TypeVerified = itemVS[2];
                                if (itemVS.Length > 3)
                                {
                                    objVS.SourceVerified = itemVS[3];
                                }
                                htVerified.Add(objVS.AuthenticationFactor, objVS);
                                if (objVS.StatusVerified == 1)
                                {
                                    ret = true;   //Si al menos uno de todos los AF es 1 => devuelvo true que esta verificado, pero devuelvo 
                                                  //igual la lista de cada AF con su estado.
                                }
                            }
                        }
                    }
                }
                else
                {
                    htVerified = null;
                }
            }
            catch
            {
                ret = false;
                htVerified = null;
            }
            return ret;
        }

        public class VerificationSource
        {
            public int AuthenticationFactor;
            public int StatusVerified;  //0- No verificado | 1-Verificado
            public string TypeVerified; //A-Automatico | M-Manual
            public string SourceVerified; //Verificador => CI - Cedula de identidad | SRCeI2018 - Registro Civul, etc. 
        }

        private static void testJson()
        {
            
        }

        private static string SelectValueId(string number, string optionalData2, out string serial)
        {
            string ret = null;
            serial = null;
            try
            {
                if (string.IsNullOrEmpty(number) || string.IsNullOrEmpty(optionalData2)) return null;
                //TRato de parsear rut con number, si falla el 
                //try
                //{
                    number = number.Replace(" ", string.Empty);
                    Bio.Core.Utils.RUN run = Bio.Core.Utils.RUN.Convertir(number);
                    if (run != null)
                    {
                        ret = number.Substring(0, number.Length - 1) + "-" + number.Substring(number.Length-1);
                        serial = optionalData2;
                    }
                    else
                    {
                        optionalData2 = optionalData2.Replace(" ", string.Empty);
                        ret = optionalData2.Substring(0, optionalData2.Length - 1) + "-" + optionalData2.Substring(optionalData2.Length - 1);
                        serial = number;
                    }
                //}
                //catch (Exception ex)
                //{
                //    LOG.Error(" Error: " + ex.Message);
                //}

            }
            catch (Exception ex)
            {

                    string e = ex.Message;
            }
            return ret;
        }

        private static double GetPorcentajeScoreNegativo(double score, double threshold, double tV2D_Threshold)
        {
            double ret = 0;
            double _BASE = 50;
            try
            {
                try
                {
                    //LOG.Debug("BPUnitOfWork.GetPorcentajeScoreNegativo IN => score = " + score.ToString() + "/threshold = " + tV2D_Threshold);
                    _BASE = threshold;
                    if (_BASE == 0)
                        _BASE = 50;
                    //LOG.Debug("BPUnitOfWork.GetPorcentajeScoreNegativo - _BASE = " + _BASE.ToString());
                }
                catch (Exception ex1)
                {
                    //LOG.Error("BPHelper.GetPorcentajeScoreNegativo - " +
                    //    "Falta configurar o mal configurado en web.config CIScorePorcentajeBase [Error=" +
                    //    ex1.Message + "] => Default=75");
                }

                //double rts = (25 * (treshold - score)) / treshold;
                //double rts = ((100 - _BASE) * (tV2D_Threshold - score)) / tV2D_Threshold;
                double rts = ((100 - _BASE) * (tV2D_Threshold - score)) / tV2D_Threshold;
                //LOG.Debug("BPUnitOfWork.GetPorcentajeScoreNegativo => rts = " + rts.ToString());

                //ret = 75 + rts;
                ret = _BASE + rts;
                if (ret < 0)
                    ret = 0;
                //LOG.Debug("BPHelper.GetPorcentajeScoreNegativo Porcentaje Seguridad => " + ret.ToString("##.##") + "%");
            }
            catch (Exception ex)
            {
                ret = 49;// Properties.Settings.Default.BPWebScorePorcentajeBase; // 75;
                         //LOG.Error("BPHelper.GetPorcentajeScoreNegativo Error => " + ex.Message);
            }
            //LOG.Debug("BPHelper.GetPorcentajeScoreNegativo OUT!");
            return ret;
        }

    public class Root
    {
        public string logoServiceCompany { get; set; }
        public object logoCustomer { get; set; }
        public string background { get; set; }
        public string primary { get; set; }
        public string secondary { get; set; }
        public string accent { get; set; }
        public string error { get; set; }
        public string info { get; set; }
        public string success { get; set; }
        public string warning { get; set; }

    }

    
    }
}
