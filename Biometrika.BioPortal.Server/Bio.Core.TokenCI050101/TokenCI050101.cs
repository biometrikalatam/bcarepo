﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core;
using Bio.Core.Api;
using Bio.Core.Constant;
using log4net;

namespace Bio.Core.TokenCI050101
{
    public class TokenCI050101: Bio.Core.Matcher.Token.IToken
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(TokenCI050101));


        #region Private Properties Implementation of IToken

        private int _operationType;

        private int _authenticationFactor;

        private int _minutiaeType;

        private int _datatypetoken;

        private int _bodyPart;

        private List<Sample> _samples;

        private string _additionalData;

        private string _getData;

        private string _setData;

        private string _typeid;

        private string _valueid;

        private string _clientid;

        private string _ipenduser;

        private string _enduser;

        #endregion Private Properties Implementation of IToken

        #region Public Properties Implementation of IToken

        /// <summary>
        /// 1-Verify | 2-Enroll
        /// </summary>
        public int OperationType
        {
            get { return _operationType; }
            set { _operationType = value; }
        }

        /// <summary>
        /// Tecnologia a la que corresponde el template
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia que contiene Data, incluidos WSQ, RAW, etc.
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        ///// <summary>
        ///// Tipo de informacion (data) que contendra el template (desde BIR_TYPE)
        ///// </summary>
        //public int Type
        //{
        //    get { return _type; }
        //    set { _type = value; }
        //}

        /// <summary>
        /// Parte del cuerpo si es tecnologia biometrica
        /// </summary>
        public int BodyPart
        {
            get { return _bodyPart; }
            set { _bodyPart = value; }
        }

        /// <summary>
        /// [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
        /// </summary>
        public int DataTypeToken
        {
            get { return _datatypetoken; }
            set { _datatypetoken = value; }
        }

        /// <summary>
        /// Data propia del template o sample
        /// </summary>
        public List<Sample> Samples
        {
            get { return _samples; }
            set { _samples = value; }
        }

        /// <summary>
        /// Datos adicionales segun sea la data. Por ejemplo si es WSQ, 
        /// se puede colocar las dimensiones de la forma w=512|h=512, o si
        /// es template NEC, el coeficiente coef=xxx. Si hay mas de uno separado 
        /// por pipe "|"
        /// </summary>
        public string AdditionalData
        {
            get { return _additionalData; }
            set { _additionalData = value; }
        }

        /// <summary>
        /// Obtiene la Data en base 64
        /// </summary>
        public string GetData
        {
            get { return _getData; }
        }

        /// <summary>
        /// Setea la data desde base 64
        /// </summary>
        public string SetData
        {
            set { _setData = value; }
        }

        /// Tipo ID de la persona (RUT, DNI, etc) 
        /// </summary>
        public string Typeid
        {
            get { return _typeid; }
            set { _typeid = value; }
        }

        /// Valor ID de la persona (RUT, DNI, etc) 
        /// </summary>
        public string Valueid
        {
            get { return _valueid; }
            set { _valueid = value; }
        }

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist 
        /// </summary>
        public string Clientid
        {
            get { return _clientid; }
            set { _clientid = value; }
        }

        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        public string Ipenduser
        {
            get { return _ipenduser; }
            set { _ipenduser = value; }
        }

        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        public string Enduser
        {
            get { return _enduser; }
            set { _enduser = value; }
        }

        #endregion Public Properties Implementation of IToken

        #region Public Method

        /// <summary>
        /// Constructor vacio
        /// </summary>
        public TokenCI050101() { }

        /// <summary>
        /// Constructor que recibe token y extrae
        /// </summary>
        /// <param name="token"></param>
        public TokenCI050101(string token)
        {
            Extract(token);
        }


        /// <summary>
        /// Extrae el contenido del token y llena las propiedades del objeto
        /// </summary>
        /// <param name="token">Token a extraer</param>
        /// <returns></returns>
        public int Extract(string token)
        {
            string[] aDataPDF;
            int ret = Errors.IERR_OK;
            try
            {
                LOG.Error("TokenCI050101.Extract - Token = " + token);
                //Si ES NULO O NO EMPIEZA CON av0305 retorno error
                if (token == null ||
                    ((!token.Trim().Substring(0, 8).Equals("CI050101"))))
                    return Errors.IERR_BAD_PARAMETER;

                //Saco encabezado
                string auxtoken = token.Trim().Substring(8);
                LOG.Error("TokenCI050101.Extract - auxtoken = " + auxtoken);
                //Desencripto la data que viene en Hexa
                //BKclassCryptoClass oCrypt = new BKclassCryptoClass();
                string dataDecrypted = null;
                try
                {
                    //dataDecrypted = oCrypt.Decrypt(auxtoken, "BioPortalClientAx14042010", true,
                    //                                      frezCryptoEncryptionType.frezBlockEncryption);
                    dataDecrypted = Crypto.DecryptWithSymmetricAid(Crypto.PRIVATE_KEY, auxtoken);
                    LOG.Error("TokenCI050101.Extract - Token decrypted = " + dataDecrypted);
                    if (dataDecrypted == null) return Errors.IERR_INVALID_TOKEN;
                }
                catch (Exception exdec)
                {
                    LOG.Error("TokenCI050101.Extract - Desencriptando - ", exdec);
                    return Errors.IERR_INVALID_TOKEN;
                }

                //Separo segun patron de separacion
                dataDecrypted = dataDecrypted.Replace("&#x0;", "");

                //Saca la muestra primero, que puede ser RAW o WSQ y despues PDF417. Lo parsea y llena lo necesario
                string[] aData = dataDecrypted.Split('|');

                if (aData == null || aData.Length != 14)
                {
                    LOG.Error("TokenCI050101.Extract - aData.Length = " + (aData!=null?aData.Length.ToString():"null"));
                    return Errors.IERR_INVALID_TOKEN;
                }
                else
                {
                    LOG.Debug("TokenCI050101.Extract - Sample = " + aData[4]);
                    LOG.Debug("TokenCI050101.Extract - PDF417 = " + aData[5]);
                }

                //Parseo PDF417
                string datosPDF;
                string msgret;

                ret = ParsePDF417(aData[5], out datosPDF, out msgret);
                if (ret < 0)
                {
                    LOG.Error("TokenCI050101.ParsePDF417 Error - " + msgret);
                    return Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    LOG.Debug("TokenCI050101.ParsePDF41 OK!");
                    /*
                          0  rut + "|" +
                          1  apellido + "|" +
                          2  pais + "|" +
                          3  numeroDeSerie + "|" +
                          4  expiracion + "|" +
                          5  finger.ToString() + "|" +
                          6  Convert.ToBase64String(pc1) + "|" +
                          7  pcLen.ToString();
                      
                     */
                    aDataPDF = datosPDF.Split('|');
                    if (aDataPDF.Length != 8)
                    {
                        LOG.Error("TokenCI050101.ParsePDF417 Error - aDataPDF.Length = " + aDataPDF.Length);
                        return Errors.IERR_BAD_PARAMETER;
                    }
                }

                //Lleno propiedades desde token
                /*
                   Pos > 0 - Tipo_Doc
                   Pos > 1 - Nro_Doc
                   Pos > 2 - Si es DP Serial Number
                   Pos > 3 - [1-10] Dedo  = BodyPart
                   Pos > 4 - WSQ en B64 si sContenido = 0,1
                   Pos > 5 - Template en B64 si sContenido = 0,2 (NEC o DP)
                   Pos > 6 - Timestamp Cliente [ddmmyyyyhhmmss]
                   Pos > 7 - IP cliente
                   Pos > 8 - [0-WSQ+Template | 1-WSQ Solo | 2-Template Solo]
                   Pos > 9 - [1-NEC | 2-DP] Formato del Template
                   Pos > 10 - Coeficiente = AdditionalData
                   Pos > 11 - [UNKNOW-0|DP-1|SEC-2|BIOI-3|CROSSMATCH-4] Dispositivo
                   Pos > 12 - AutenticationFactor
                   Pos > 13 - OperationType
                 */
                _operationType = 1;
                _authenticationFactor = 2;
                _minutiaeType = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ;
                _bodyPart = Convert.ToInt32(aDataPDF[5]);
                _samples = new List<Sample>();
                _datatypetoken = 0; // Convert.ToInt32(aData[8]);

                //Si viene Tewmplate, lo pongo primero para qyue verifique por el 
                string auxdata;
                if (_datatypetoken == 0 || _datatypetoken == 2)
                {
                   
                    LOG.Error("TokenCI050101.Extract - Add WSQ");
                    Sample sWsq = new Sample
                    {
                        Data = aData[4],
                        Additionaldata = "",
                        Device = 1,
                        Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ
                    };
                    _samples.Add(sWsq);

                    //Add NEC
                    Sample sTmpl = new Sample
                    {
                        Data = aDataPDF[6],
                        Additionaldata = aDataPDF[7],
                        Device = 1,
                        Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_NEC
                    };
                    _samples.Add(sTmpl);
                }
                _typeid = "RUT";
                if (aDataPDF[0].IndexOf("-") > 0)
                {
                    _valueid = aDataPDF[0];
                }
                else
                {
                    _valueid = aDataPDF[0].Substring(0, aDataPDF[0].Length - 1) + "-" + aDataPDF[0].Substring(aDataPDF[0].Length - 1, 1);
                }
                _clientid = aData[2];
                _ipenduser = aData[7];
                _enduser = aData[7];
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("TokenCI050101.Extarct", ex);
            }
            LOG.Error("TokenCI050101.Extract - Out!");
            return ret;
        }

        /// <summary>
        /// Extrae el contenido del token y llena las propiedades del objeto y ademas de llenar las prop del bjeto, 
        /// setea las variable que corresponden a un objeto XmlParamIn
        /// </summary>
        /// <param name="token">token en base 64</param>
        /// <param name="newparamin">Objeto XmlParamIn con datos extraidos desde token</param>
        /// <returns></returns>
        public int ExtractToParameters(string token, out string newparamin)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {

        }

        public int ParsePDF417(string pdf417, out string datos, out string msgerr)
        {
            int iErr = 0;
            msgerr = null;
            byte[] pdf = null;
            datos = null;
            try
            {

                if (!string.IsNullOrEmpty(pdf417))
                {
                    pdf = Convert.FromBase64String(pdf417);
                    string rut = Encoding.UTF7.GetString(pdf, 0, 9).Replace('\0', ' ').TrimEnd();
                    string apellido = Encoding.UTF7.GetString(pdf, 19, 30).Replace('\0', ' ').TrimEnd();
                    string pais = Encoding.UTF7.GetString(pdf, 49, 3).Replace('\0', ' ').TrimEnd();
                    string numeroDeSerie = Encoding.UTF7.GetString(pdf, 58, 10).Replace('\0', ' ').TrimEnd();

                    string aa = Encoding.UTF7.GetString(pdf, 52, 2);
                    string mm = Encoding.UTF7.GetString(pdf, 54, 2);
                    string dd = Encoding.UTF7.GetString(pdf, 56, 2);
                    string expiracion = dd + "/" + mm + "/" + aa;

                    int finger = (pdf[73] << 24) + (pdf[72] << 16) + (pdf[71] << 8) + (pdf[70]);
                    int pcLen = (pdf[77] << 24) + (pdf[76] << 16) + (pdf[75] << 8) + (pdf[74]);
                    byte[] pc1 = new Byte[pcLen];
                    Buffer.BlockCopy(pdf, 78, pc1, 0, pcLen);

                    //appNumber + "|" +
                    //       disability + "|" +
                    //       docType + "|" +
                    datos = rut + "|" +
                            apellido + "|" +
                            pais + "|" +
                            numeroDeSerie + "|" +
                            expiracion + "|" +
                            finger.ToString() + "|" +
                            Convert.ToBase64String(pc1) + "|" +
                            pcLen.ToString();

                    LOG.Debug("TokenCI050101.ParsePDF417 OK - " + datos);
                    iErr = 0;
                }
                else
                {
                    iErr = Core.Constant.Errors.IERR_BAD_PARAMETER;
                    msgerr = "PDF417 es nulo";
                    LOG.Debug("TokenCI050101.ParsePDF417 Err - " + msgerr);
                }

            }
            catch (Exception ex)
            {
                iErr = Core.Constant.Errors.IERR_UNKNOWN;
                msgerr = ex.Message;
                LOG.Debug("TokenCI050101.ParsePDF417 Err Ex - " + msgerr);
            }

            return iErr;
        }


#endregion Public Method

    }
}
