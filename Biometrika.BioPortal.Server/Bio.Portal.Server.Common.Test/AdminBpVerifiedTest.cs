﻿using BioPortal.Server.Common.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;

namespace Bio.Portal.Server.Common.Test
{
    
    
    /// <summary>
    ///This is a test class for AdminBpVerifiedTest and is intended
    ///to contain all AdminBpVerifiedTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AdminBpVerifiedTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AdminBpVerified Constructor
        ///</summary>
        [TestMethod()]
        public void AdminBpVerifiedConstructorTest()
        {
            AdminBpVerified target = new AdminBpVerified();
            Assert.AreNotEqual(null,target);
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public void CreateTest()
        {
            string typeid = string.Empty; // TODO: Initialize to an appropriate value
            string valueid = string.Empty; // TODO: Initialize to an appropriate value
            int datatype = 0; // TODO: Initialize to an appropriate value
            string data = string.Empty; // TODO: Initialize to an appropriate value
            DateTime timestamp = new DateTime(); // TODO: Initialize to an appropriate value
            BpTx bptx = null; // TODO: Initialize to an appropriate value
            Nullable<int> authenticatorfactor = new Nullable<int>(); // TODO: Initialize to an appropriate value
            Nullable<int> minutiaetype = new Nullable<int>(); // TODO: Initialize to an appropriate value
            Nullable<int> bodypart = new Nullable<int>(); // TODO: Initialize to an appropriate value
            Nullable<int> wsqw = new Nullable<int>(); // TODO: Initialize to an appropriate value
            Nullable<int> wsqh = new Nullable<int>(); // TODO: Initialize to an appropriate value
            Nullable<int> coefficient = new Nullable<int>(); // TODO: Initialize to an appropriate value
            Nullable<int> insertoption = new Nullable<int>(); // TODO: Initialize to an appropriate value
            Nullable<int> validationtype = new Nullable<int>(); // TODO: Initialize to an appropriate value
            Nullable<int> originid = new Nullable<int>(); // TODO: Initialize to an appropriate value
            string enduser = string.Empty; // TODO: Initialize to an appropriate value
            string ipenduser = string.Empty; // TODO: Initialize to an appropriate value
            string clientid = string.Empty; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            BpVerified expected = null; // TODO: Initialize to an appropriate value
            BpVerified actual = null;
            //actual = AdminBpVerified.Create(typeid, valueid, datatype, data, timestamp, bptx, authenticatorfactor, minutiaetype, bodypart, wsqw, wsqh, coefficient, insertoption, validationtype, originid, enduser, ipenduser, clientid, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Retrieve
        ///</summary>
        [TestMethod()]
        public void RetrieveTest()
        {
            int id = 0; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            BpVerified expected = null; // TODO: Initialize to an appropriate value
            BpVerified actual;
            actual = AdminBpVerified.Retrieve(id, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Write
        ///</summary>
        [TestMethod()]
        public void WriteTest()
        {
            BpVerified data = null; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = AdminBpVerified.Write(data, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ListAll
        ///</summary>
        [TestMethod()]
        public void ListAllTest()
        {
            IList actual;
            actual = AdminBpVerified.ListAll;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
