﻿using Bio.Portal.Server.Common.Database;
using BioPortal.Server.Common.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Data;
using System.Collections.Generic;
using System.Drawing;

namespace Bio.Portal.Server.Common.Test
{
    
    
    /// <summary>
    ///This is a test class for AdminBpIdentityTest and is intended
    ///to contain all AdminBpIdentityTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AdminBpIdentityTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AdminBpIdentity Constructor
        ///</summary>
        [TestMethod()]
        public void AdminBpIdentityConstructorTest()
        {
            AdminBpIdentity target = new AdminBpIdentity();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for AddBIR
        ///</summary>
        [TestMethod()]
        public void AddBIRTest()
        {
            BpIdentity bpi = null; // TODO: Initialize to an appropriate value
            BpBir bir = null; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            AdminBpIdentity.AddBIR(bpi, bir, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public void CreateTest()
        {
            string typeid = "RUT"; // TODO: Initialize to an appropriate value
            string valueid = "22643186-8"; // TODO: Initialize to an appropriate value
            bool checkExists = false; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = "S/C"; // TODO: Initialize to an appropriate value
            BpIdentity expected = null; // TODO: Initialize to an appropriate value
            BpIdentity actual;
            actual = AdminBpIdentity.Create(0, typeid, valueid, checkExists, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreNotEqual(expected, actual);
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public void CreateTest1()
        {
            BpIdentity bpi = null; // TODO: Initialize to an appropriate value
            bool checkExists = false; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            BpIdentity expected = null; // TODO: Initialize to an appropriate value
            BpIdentity actual;
            actual = AdminBpIdentity.Create("pepe", checkExists, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public void CreateTest2()
        {
            string nick = string.Empty; // TODO: Initialize to an appropriate value
            bool checkExists = false; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            BpIdentity expected = null; // TODO: Initialize to an appropriate value
            BpIdentity actual;
            actual = AdminBpIdentity.Create(nick, checkExists, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Delete
        ///</summary>
        [TestMethod()]
        public void DeleteTest()
        {
            string typeid = string.Empty; // TODO: Initialize to an appropriate value
            string valueid = string.Empty; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual = false;
            //actual = AdminBpIdentity.Delete(0, typeid, valueid, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Delete
        ///</summary>
        [TestMethod()]
        public void DeleteTest1()
        {
            BpIdentity bpi = null; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = AdminBpIdentity.Delete(bpi, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetAllIdentitiesDataView
        ///</summary>
        [TestMethod()]
        public void GetAllIdentitiesDataViewTest()
        {
            DictionaryEntry[] columns = null; // TODO: Initialize to an appropriate value
            DataView expected = null; // TODO: Initialize to an appropriate value
            DataView actual;
            actual = AdminBpIdentity.GetAllIdentitiesDataView(columns);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetBIRs
        ///</summary>
        [TestMethod()]
        public void GetBIRsTest()
        {
            BpIdentity bpi = null; // TODO: Initialize to an appropriate value
            IList<BpBir> expected = null; // TODO: Initialize to an appropriate value
            IList<BpBir> actual;
            actual = AdminBpIdentity.GetBIRs(bpi);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetDynamicData
        ///</summary>
        [TestMethod()]
        public void GetDynamicDataTest()
        {
            BpIdentity bpi = null; // TODO: Initialize to an appropriate value
            string key = string.Empty; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = AdminBpIdentity.GetDynamicData(bpi, key, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetIdentitiesByFiltro
        ///</summary>
        [TestMethod()]
        public void GetIdentitiesByFiltroTest()
        {
            string typeid = string.Empty; // TODO: Initialize to an appropriate value
            string valueid = string.Empty; // TODO: Initialize to an appropriate value
            string name = string.Empty; // TODO: Initialize to an appropriate value
            string lastname = string.Empty; // TODO: Initialize to an appropriate value
            IList expected = null; // TODO: Initialize to an appropriate value
            IList actual;
            actual = AdminBpIdentity.GetIdentitiesByFiltro(0, typeid, valueid, name, lastname);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetImageFoto
        ///</summary>
        [TestMethod()]
        public void GetImageFotoTest()
        {
            BpIdentity bpi = null; // TODO: Initialize to an appropriate value
            Image expected = null; // TODO: Initialize to an appropriate value
            Image actual;
            actual = AdminBpIdentity.GetImageFoto(bpi);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Retrieve
        ///</summary>
        [TestMethod()]
        public void RetrieveTest()
        {
            string typeid = "RUT"; // TODO: Initialize to an appropriate value
            string valueid = "21284415-2"; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            BpIdentity expected = null; // TODO: Initialize to an appropriate value
            BpIdentity actual;
            actual = AdminBpIdentity.Retrieve(0, typeid, valueid, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Retrieve
        ///</summary>
        [TestMethod()]
        public void RetrieveTest1()
        {
            int id = 0; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            BpIdentity expected = null; // TODO: Initialize to an appropriate value
            BpIdentity actual;
            actual = AdminBpIdentity.Retrieve(id, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Retrieve
        ///</summary>
        [TestMethod()]
        public void RetrieveTest2()
        {
            string nick = string.Empty; // TODO: Initialize to an appropriate value
            int flag = 0; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            object expected = null; // TODO: Initialize to an appropriate value
            object actual;
            actual = AdminBpIdentity.Retrieve(nick, flag, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SetDynamicData
        ///</summary>
        [TestMethod()]
        public void SetDynamicDataTest()
        {
            BpIdentity bpi = null; // TODO: Initialize to an appropriate value
            string key = string.Empty; // TODO: Initialize to an appropriate value
            string value = string.Empty; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            AdminBpIdentity.SetDynamicData(bpi, key, value, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Update
        ///</summary>
        [TestMethod()]
        public void UpdateTest()
        {
            try
            {
                string msgErr = string.Empty; // TODO: Initialize to an appropriate value
                BpIdentity bpi = null;
                int i = AdminBpIdentity.Retrieve(7, "RUT", "21284415-2", out bpi, out msgErr);
                    // TODO: Initialize to an appropriate value

                string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
                BpIdentity expected = null; // TODO: Initialize to an appropriate value
                bool actual;
                //actual = AdminBpIdentity.Update(bpi, out msgErr);

                IList<BpBir> bpBirToDelete = new List<BpBir>();
                foreach (BpBir bir in bpi.BpBir)
                {
                    if (bir.Authenticationfactor == 1)
                    {
                        bpBirToDelete.Add(bir);
                    }
                }
                foreach (BpBir bir in bpBirToDelete)
                {
                    bpi.BpBir.Remove(bir);
                }
                actual = AdminBpIdentity.Write(bpi, out msgErr);
                Assert.AreEqual(msgErrExpected, msgErr);
                Assert.AreEqual(expected, actual);
                Assert.Inconclusive("Verify the correctness of this test method.");
            } catch(Exception ex)
            {
                System.Console.WriteLine(ex.StackTrace);
            }
        }

        /// <summary>
        ///A test for ListAll
        ///</summary>
        [TestMethod()]
        public void ListAllTest()
        {
            IList actual;
            actual = AdminBpIdentity.ListAll;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
