﻿using BioPortal.Server.Common.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Data;

namespace Bio.Portal.Server.Common.Test
{
    
    
    /// <summary>
    ///This is a test class for AdminBpTxTest and is intended
    ///to contain all AdminBpTxTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AdminBpTxTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AdminBpTx Constructor
        ///</summary>
        [TestMethod()]
        public void AdminBpTxConstructorTest()
        {
            AdminBpTx target = new AdminBpTx();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for AddTxConx
        ///</summary>
        [TestMethod()]
        public void AddTxConxTest()
        {
            BpTx tx = null; // TODO: Initialize to an appropriate value
            BpTxConx conx = null; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            BpTx expected = null; // TODO: Initialize to an appropriate value
            BpTx actual;
            actual = AdminBpTx.AddTxConx(tx, conx, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public void CreateTest()
        {
            string trackid = string.Empty; // TODO: Initialize to an appropriate value
            string typeid = string.Empty; // TODO: Initialize to an appropriate value
            string valueid = string.Empty; // TODO: Initialize to an appropriate value
            int result = 0; // TODO: Initialize to an appropriate value
            int score = 0; // TODO: Initialize to an appropriate value
            int threshold = 0; // TODO: Initialize to an appropriate value
            DateTime timestampstart = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime timestampend = new DateTime(); // TODO: Initialize to an appropriate value
            int authenticatorfactor = 0; // TODO: Initialize to an appropriate value
            int minutiaetype = 0; // TODO: Initialize to an appropriate value
            int bodypart = 0; // TODO: Initialize to an appropriate value
            int actiontype = 0; // TODO: Initialize to an appropriate value
            BpOrigin bporigin = null; // TODO: Initialize to an appropriate value
            Nullable<DateTime> timestampclient = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            string clientid = string.Empty; // TODO: Initialize to an appropriate value
            string ipenduser = string.Empty; // TODO: Initialize to an appropriate value
            string enduser = string.Empty; // TODO: Initialize to an appropriate value
            string dynamicdata = string.Empty; // TODO: Initialize to an appropriate value
            int companyidtx = 0; // TODO: Initialize to an appropriate value
            int useridtx = 0; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            BpTx expected = null; // TODO: Initialize to an appropriate value
            BpTx actual;
            actual = AdminBpTx.Create(trackid, typeid, valueid, result, score, threshold, timestampstart, timestampend, authenticatorfactor, minutiaetype, bodypart, actiontype, bporigin, timestampclient, clientid, ipenduser, enduser, dynamicdata, companyidtx, useridtx, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public void CreateTest1()
        {
            string trackid = string.Empty; // TODO: Initialize to an appropriate value
            string typeid = string.Empty; // TODO: Initialize to an appropriate value
            string valueid = string.Empty; // TODO: Initialize to an appropriate value
            int result = 0; // TODO: Initialize to an appropriate value
            int score = 0; // TODO: Initialize to an appropriate value
            int threshold = 0; // TODO: Initialize to an appropriate value
            DateTime timestampstart = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime timestampend = new DateTime(); // TODO: Initialize to an appropriate value
            int authenticatorfactor = 0; // TODO: Initialize to an appropriate value
            int minutiaetype = 0; // TODO: Initialize to an appropriate value
            int bodypart = 0; // TODO: Initialize to an appropriate value
            int actiontype = 0; // TODO: Initialize to an appropriate value
            BpOrigin bporigin = null; // TODO: Initialize to an appropriate value
            Nullable<DateTime> timestampclient = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            string clientid = string.Empty; // TODO: Initialize to an appropriate value
            string ipenduser = string.Empty; // TODO: Initialize to an appropriate value
            string enduser = string.Empty; // TODO: Initialize to an appropriate value
            string dynamicdata = string.Empty; // TODO: Initialize to an appropriate value
            int companyidtx = 0; // TODO: Initialize to an appropriate value
            int useridtx = 0; // TODO: Initialize to an appropriate value
            IList conxList = null; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            BpTx expected = null; // TODO: Initialize to an appropriate value
            BpTx actual;
            actual = AdminBpTx.Create(trackid, typeid, valueid, result, score, threshold, timestampstart, timestampend, authenticatorfactor, minutiaetype, bodypart, actiontype, bporigin, timestampclient, clientid, ipenduser, enduser, dynamicdata, companyidtx, useridtx, conxList, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public void CreateTest2()
        {
            string trackid = string.Empty; // TODO: Initialize to an appropriate value
            string typeid = string.Empty; // TODO: Initialize to an appropriate value
            string valueid = string.Empty; // TODO: Initialize to an appropriate value
            int result = 0; // TODO: Initialize to an appropriate value
            int score = 0; // TODO: Initialize to an appropriate value
            int threshold = 0; // TODO: Initialize to an appropriate value
            DateTime timestampstart = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime timestampend = new DateTime(); // TODO: Initialize to an appropriate value
            int authenticatorfactor = 0; // TODO: Initialize to an appropriate value
            int minutiaetype = 0; // TODO: Initialize to an appropriate value
            int bodypart = 0; // TODO: Initialize to an appropriate value
            int actiontype = 0; // TODO: Initialize to an appropriate value
            BpOrigin bporigin = null; // TODO: Initialize to an appropriate value
            Nullable<DateTime> timestampclient = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            string clientid = string.Empty; // TODO: Initialize to an appropriate value
            string ipenduser = string.Empty; // TODO: Initialize to an appropriate value
            string enduser = string.Empty; // TODO: Initialize to an appropriate value
            string dynamicdata = string.Empty; // TODO: Initialize to an appropriate value
            int companyidtx = 0; // TODO: Initialize to an appropriate value
            int useridtx = 0; // TODO: Initialize to an appropriate value
            BpTxConx conx = null; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            BpTx expected = null; // TODO: Initialize to an appropriate value
            BpTx actual = null;
            //actual = AdminBpTx.Create(trackid, typeid, valueid, result, score, threshold, timestampstart, timestampend, authenticatorfactor, minutiaetype, bodypart, actiontype, bporigin, timestampclient, clientid, ipenduser, enduser, dynamicdata, companyidtx, useridtx, conx, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CumpleFiltro
        ///</summary>
        [TestMethod()]
        [DeploymentItem("BioPortal.Server.Common.dll")]
        public void CumpleFiltroTest()
        {
            BpTx tx = null; // TODO: Initialize to an appropriate value
            string idconx = string.Empty; // TODO: Initialize to an appropriate value
            int statusconx = 0; // TODO: Initialize to an appropriate value
            int resultconx = 0; // TODO: Initialize to an appropriate value
            string timestamprcdesde = string.Empty; // TODO: Initialize to an appropriate value
            string timestamprchasta = string.Empty; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = AdminBpTx_Accessor.CumpleFiltro(tx, idconx, statusconx, resultconx, timestamprcdesde, timestamprchasta);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CumpleFiltroFecha
        ///</summary>
        [TestMethod()]
        [DeploymentItem("BioPortal.Server.Common.dll")]
        public void CumpleFiltroFechaTest()
        {
            string timestamp = string.Empty; // TODO: Initialize to an appropriate value
            string desde = string.Empty; // TODO: Initialize to an appropriate value
            string hasta = string.Empty; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = AdminBpTx_Accessor.CumpleFiltroFecha(timestamp, desde, hasta);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetAllTxDataView
        ///</summary>
        [TestMethod()]
        public void GetAllTxDataViewTest()
        {
            DictionaryEntry[] columns = null; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            DataView expected = null; // TODO: Initialize to an appropriate value
            DataView actual;
            actual = AdminBpTx.GetAllTxDataView(columns, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetAllTxWithFilterToDataView
        ///</summary>
        [TestMethod()]
        public void GetAllTxWithFilterToDataViewTest()
        {
            int id = 0; // TODO: Initialize to an appropriate value
            string trackid = string.Empty; // TODO: Initialize to an appropriate value
            int origin = 0; // TODO: Initialize to an appropriate value
            string typeid = string.Empty; // TODO: Initialize to an appropriate value
            string valueid = string.Empty; // TODO: Initialize to an appropriate value
            string actiontype = string.Empty; // TODO: Initialize to an appropriate value
            string idconx = string.Empty; // TODO: Initialize to an appropriate value
            int statusconx = 0; // TODO: Initialize to an appropriate value
            int resultconx = 0; // TODO: Initialize to an appropriate value
            string timestamprcdesde = string.Empty; // TODO: Initialize to an appropriate value
            string timestamprchasta = string.Empty; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            DataView expected = null; // TODO: Initialize to an appropriate value
            DataView actual;
            actual = AdminBpTx.GetAllTxWithFilterToDataView(id, trackid, origin, typeid, valueid, actiontype, idconx, statusconx, resultconx, timestamprcdesde, timestamprchasta, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Retrieve
        ///</summary>
        [TestMethod()]
        public void RetrieveTest()
        {
            int id = 0; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            BpTx expected = null; // TODO: Initialize to an appropriate value
            BpTx actual;
            actual = AdminBpTx.Retrieve(id, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Retrieve
        ///</summary>
        [TestMethod()]
        public void RetrieveTest1()
        {
            string typeid = string.Empty; // TODO: Initialize to an appropriate value
            string valueid = string.Empty; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            IList expected = null; // TODO: Initialize to an appropriate value
            IList actual;
            actual = AdminBpTx.Retrieve(typeid, valueid, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for RetrieveTxByTrackid
        ///</summary>
        [TestMethod()]
        public void RetrieveTxByTrackidTest()
        {
            string trackid = string.Empty; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            IList expected = null; // TODO: Initialize to an appropriate value
            IList actual;
            actual = AdminBpTx.RetrieveTxByTrackid(trackid, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Write
        ///</summary>
        [TestMethod()]
        public void WriteTest()
        {
            BpTx bptx = null; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = AdminBpTx.Write(bptx, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ListAll
        ///</summary>
        [TestMethod()]
        public void ListAllTest()
        {
            IList actual;
            actual = AdminBpTx.ListAll;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
