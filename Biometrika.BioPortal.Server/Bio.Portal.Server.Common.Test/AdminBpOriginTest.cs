﻿using BioPortal.Server.Common.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Data;

namespace Bio.Portal.Server.Common.Test
{
    
    
    /// <summary>
    ///This is a test class for AdminBpOriginTest and is intended
    ///to contain all AdminBpOriginTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AdminBpOriginTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AdminBpOrigin Constructor
        ///</summary>
        [TestMethod()]
        public void AdminBpOriginConstructorTest()
        {
            AdminBpOrigin target = new AdminBpOrigin();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public void CreateTest()
        {
            int id = 200; // TODO: Initialize to an appropriate value
            string descripcion = "200Desc"; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = "S/C"; // TODO: Initialize to an appropriate value
            BpOrigin expected = null; // TODO: Initialize to an appropriate value
            BpOrigin actual;
            actual = AdminBpOrigin.Create(id, descripcion, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Delete
        ///</summary>
        [TestMethod()]
        public void DeleteTest()
        {
            int id = 0; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = AdminBpOrigin.Delete(id, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetAllOrigenDataView
        ///</summary>
        [TestMethod()]
        public void GetAllOrigenDataViewTest()
        {
            DictionaryEntry[] columns = null; // TODO: Initialize to an appropriate value
            DataView expected = null; // TODO: Initialize to an appropriate value
            DataView actual;
            actual = AdminBpOrigin.GetAllOrigenDataView(columns);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Retrieve
        ///</summary>
        [TestMethod()]
        public void RetrieveTest()
        {
            int id = 1; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = "S/C"; // TODO: Initialize to an appropriate value
            BpOrigin expected = null; // TODO: Initialize to an appropriate value
            BpOrigin actual;
            actual = AdminBpOrigin.Retrieve(id, out msgErr);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for RetrieveAll
        ///</summary>
        [TestMethod()]
        public void RetrieveAllTest()
        {
            BpOrigin[] result = null; // TODO: Initialize to an appropriate value
            BpOrigin[] resultExpected = null; // TODO: Initialize to an appropriate value
            string msgErr = string.Empty; // TODO: Initialize to an appropriate value
            string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = AdminBpOrigin.RetrieveAll(out result, out msgErr);
            Assert.AreEqual(resultExpected, result);
            Assert.AreEqual(msgErrExpected, msgErr);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ListAll
        ///</summary>
        [TestMethod()]
        public void ListAllTest()
        {
            IList actual;
            actual = AdminBpOrigin.ListAll;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
