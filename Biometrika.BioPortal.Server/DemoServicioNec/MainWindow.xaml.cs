﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BioPortal.Server.Api;
using Bio.Core.Api;

namespace DemoServicioNec
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CallServiceRestSharp(datosPrueba.RawTania, datosPrueba.NecTania);
        }

        private void Button_Click_BP(object sender, RoutedEventArgs e)
        {
            DoSinToken();
        }

        private void CallServiceRestSharp(string minutiae1, string minutiae2)
        {
            var uri = new Uri(urlTextbox.Text);
            var host = uri.GetLeftPart(System.UriPartial.Authority);
            var verb = uri.ToString().Replace(host.ToString(), String.Empty);

            //Objeto nec con el que se realiza la comparación
            var necDataToCompare = new NecDataToCompare()
            {
                rawData = minutiae1,
                necTemplate = minutiae2
            };

            var client = new RestClient(host);
            var request = new RestRequest(verb, Method.POST);
            request.RequestFormat = RestSharp.DataFormat.Json;
            request.AddBody(necDataToCompare);
            try
            {
                var respuesta = client.Execute(request);
                resultado.Text = "Score: " + respuesta.Content.ToString();

            }
            catch(Exception ex)
            {
                resultado.Text = ex.Message;
            }
        }

        private void urlTextbox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            resultado.Text = "Resultado ";

        }

        private void DoSinToken()
        {
            int expected = 0;
            int actual;
            string xmlparamout = string.Empty;
            double score = 0;
            bool result = false;
            try
            {

                int res = BPHelper.VerifyByDMA(urlTextbox_Copy.Text + "BioPortal.Server.WS.asmx", datosPrueba.RawTania, 22, 
                                               datosPrueba.NecTania, "RUT", "21284415-2", out result, out score);

                resultado.Text = "Code = " + res + " - Result=" + result.ToString() + " - Score=" + score.ToString();
            }
            catch (Exception ex)
            {
                resultado.Text = ex.Message;
            }
        }

        private void ShowResultV(int actual, string xmlparamout)
        {
            XmlParamOut paramout = null;
            try
            {
                this.resultado.Text += Environment.NewLine + "Codigo Retorno=" + actual.ToString() + " [" + Bio.Core.Api.Constant.Errors.GetDescription(actual) + "]";
                if (xmlparamout != null)
                {
                    paramout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                }
                if (actual == 0)
                {
                    this.resultado.Text += Environment.NewLine + "Track Id = " + paramout.Trackid;
                }
                else
                {
                    this.resultado.Text += Environment.NewLine + "Retorno = " + actual.ToString();
                }
                if (paramout != null)
                {
                    if (paramout.PersonalData != null)
                    {

                        this.resultado.Text += Environment.NewLine + "TypeId/ValueId = " + paramout.PersonalData.Typeid + "/" +
                            paramout.PersonalData.Valueid;
                        
                    }
                    this.resultado.Text += Environment.NewLine + "Result = " + paramout.Result.ToString();
                    this.resultado.Text += Environment.NewLine + "Score = " + paramout.Score.ToString();
                }
            }
            catch (Exception ex)
            {
                this.resultado.Text = "Exception = " + ex.StackTrace; 
            }
        }

        private void Button_Click_ConvertISO(object sender, RoutedEventArgs e)
        {
            int expected = 0;
            int actual;
            string _MinuciaISOCompact = "Rk1SACAyMAABXgAKADUAAAEsAZAAxQDFAQAAAAA1QJkABwAAgL4ADagAQLgAHlIAQNkAMKAAgEwAMBIAQMEAUJ8AgQcAXpQAgJkAeFcAQOQAezsAgGQAhhoAQMQAiTsAgG8AinUAgNoAi5IAQRUAnZIAQKAAoY4AQJAAon8AgHEArn4AgRoArzkAQQkAsJIAgIIAt4YAQHoAwH8AgLUAypAAgQAAzjgAQHgA2YEAQIoA4IkAgE0A4CUAgMEA7pUAgIMA74QAQQ8A+z8AgIYA/ocAQNEA/0IAQEABACIAgO4BAj8AgHIBDB8AgD8BEh4AQMYBFEEAgJ0BHUoAQRMBJEgAQHQBKQoAgFwBLRcAgMYBN0QAgHoBPAAAgJIBPlUAgEsBPw4AgEEBRWEAQMkBT0oAgMEBVlIAgJgBW1IAgEgBXQcAgOkBXlEAgJMBZ1QAQGkBcbAAgIQBclUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
            double score = 0;
            bool result = false;
            
            try
            {

                int res = BPHelper.ConvertISOtoISOCompact(urlTextbox_Copy.Text, _MinuciaISOCompact, out _MinuciaISOCompact);

                resultado.Text = "Code = " + res + " - Result=" + result.ToString() + " - Score=" + score.ToString();
            }
            catch (Exception ex)
            {
                resultado.Text = ex.Message;
            }
        }

 
    }
}
