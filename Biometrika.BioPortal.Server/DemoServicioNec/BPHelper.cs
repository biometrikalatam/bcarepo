﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bio.Core.Api;
using BioPortal.Server.Api;

namespace DemoServicioNec
{
    public class BPHelper
    {



        internal static int VerifyByDMA(string _URLBP, string _Sample, int _MTSample, string _NECMinutiae, string _TypeID, string _ValueID, 
             out bool _VerifyOk, out double _Score)
        {
            int res = 0;
            string xmlparamout = null;
            _VerifyOk = false;
            _Score = 0;

            try
            {
                using (BPWS.BioPortalServerWS target = new BPWS.BioPortalServerWS())
                {
                    target.Timeout = 30000;  //Timeout del WS, puede ser configurable 
                    target.Url = _URLBP; //urlTextbox_Copy.Text + "BioPortal.Server.WS.asmx"; //"http://localhost:4030/BioPortal.Server.WS.asmx";
                    //this.rtxResult.Text += Environment.NewLine + "Consultando a " + target.Url + "...";
                    //this.rtxResult.Refresh();
                    XmlParamIn pin = new XmlParamIn();
                    pin.Actionid = 1; //Verify
                    pin.Additionaldata = null;
                    pin.Authenticationfactor = 2; //Fingerprint
                    pin.Minutiaetype = 0; //No aplica porque va a Connector
                    pin.Clientid = "IdClienteTest";  //IP del cliente final que usa esto, o bien el serialid del sensor, sino algo fijo
                    pin.Companyid = 7; //No aplica para esto igual pero podria ser un id int configurable
                    pin.Enduser = "EnUserTest"; //Algun setring si se puede del cliente que manda a hacer esta funcion, IP, Serial del lector, o algo. Sino algo fijo
                    pin.Ipenduser = "127.0.0.1"; //IP del cliente final o sino ip ficticia 0.0.0.0 o 127.0.0.1
                    pin.Matchingtype = 1;  //Mejor matching - Igual no aplica en esto
                    pin.Origin = 1;        //Int configurable. default es 1 = Origen desconocido    

                    //Agregamos los datos a usar, el WSQ o RW primero, y luego las minucias NEC PC1 en base 64 leida desde la cedula.
                    pin.SampleCollection = new List<Sample>();
                    Sample sample0 = new Sample();
                    sample0.Data = _Sample; // WSQ o RAW que se capturo.  
                    sample0.Minutiaetype = _MTSample; //MinutiaeType si es 21 = WSQ o sino 22 = RAW;
                    sample0.Additionaldata = null;
                    pin.SampleCollection.Add(sample0);

                    Sample sample1 = new Sample();
                    sample1.Data = _NECMinutiae; //Minucia NEC leida desde PDF417 de lña cedula
                    sample1.Minutiaetype = 1;  //1 es Minucia NEC
                    sample1.Additionaldata = "0";  //Vaor fijo solo para que no falle, pero no se usa
                    pin.SampleCollection.Add(sample1);

                    pin.SaveVerified = 0;  //Usa o no la auditoria. 0 es que usa la default del BP.
                    pin.Threshold = 0; //No aplica
                    pin.Userid = 0;     //No aplica
                    pin.Verifybyconnectorid = "DMANec";  //El conector a usar es este
                    pin.InsertOption = 1;                //Esto indica que ante verify ok, enrola
                    pin.PersonalData = new PersonalData();
                    pin.PersonalData.Typeid = _TypeID;
                    pin.PersonalData.Valueid = _ValueID;
                    pin.OperationOrder = 3; //Solo Remote

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    res = target.Verify(xmlparamin, out xmlparamout);
                }

                if (res == 0) //Si anduvo OK
                {
                    XmlParamOut pout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                    if (pout != null)
                    {
                        _VerifyOk = pout.Result == 1 ? true : false;
                        _Score = pout.Score;
                    }
                }
            }
            catch (Exception ex)
            {
                res = -1;
                //Enviar a LOG Error
            }
            return res;
        }




        internal static int ConvertISOtoISOCompact(string _URLBP, string _MinuciaISO, out string _MinuciaISOCompact)
        {
            int res = 0;
            _MinuciaISOCompact = null;
           
            try
            {
                using (BPUtils.Bio_Portal_Server_Utils target = new BPUtils.Bio_Portal_Server_Utils())
                {
                    target.Timeout = 30000;  //Timeout del WS, puede ser configurable 
                    target.Url = _URLBP; 
                    res = target.ISOToISOC(_MinuciaISO, out _MinuciaISOCompact);
                }

            }
            catch (Exception ex)
            {
                res = -1;
                //Enviar a LOG Error
            }
            return res;
        }
    }
}
