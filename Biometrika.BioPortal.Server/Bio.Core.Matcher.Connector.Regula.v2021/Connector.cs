﻿using System;
using System.IO;
using System.Text;
using Bio.Core.Api;
using Bio.Core.Api.Matcher.Interface;
using Bio.Core.Constant;
using BioPortal.Server.Api;
using log4net;
using System.Net.Http;
using System.Net.Http.Headers;
//using MultipartFormDataSample.Client;
using System.Collections.Generic;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Serialization;
using Biometrika.Regula.WebAPI.Api;
using Biometrika.Regula.WebAPI.Api._2021;

namespace Bio.Core.Matcher.Connector.Regula.v2021
{
    public class Connector : IConnector
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Connector));

#region Private properties propietario

        private bool _getDocImageFront;
        private bool _getFoto;
        private bool _getFirma;

        internal string _urlWS = "https://api.regulaforensics.com/"; //"http://localhost:8089/";
        internal string _licenseFile = "regula.license";
        //internal string _password = "Regul@SdkTest";

        private int _processType = 0; //Agregado para manejar el bug de Regula v6.2
        private int _timeout = 30000;
        private string _ip;

        private bool _isConfigured; //Solo para control en initialization

       RegulaHandler _REGULA_HANDLER = new RegulaHandler();


#endregion Private properties propietario

#region Public properties propietario

        public string UrlWs
        {
            get { return _urlWS; }
            set { _urlWS = value; }
        }

        public string LicenseFile
        {
            get { return _licenseFile; }
            set { _licenseFile = value; }
        }

        public int ProcessType
        {
            get { return _processType; }
            set { _processType = value; }
        }

        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

       

#endregion Public properties propietario

#region Private Method

        /// <summary>
        /// Inicializa las variables del objeto desde config, para no perder tiempo en buquedas luego
        /// </summary>
        private void Initialization()
        {
            try
            {
                LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021.v2021.Initialize IN...");
                if (_config == null || _config.DynamicDataItems == null)
                {
                    _isConfigured = false;
                    return;
                }

                foreach (DynamicDataItem dd in _config.DynamicDataItems)
                {
                    if (dd.key.Trim().Equals("UrlWS"))
                    {
                        _urlWS = dd.value.Trim();
                    }
                    if (dd.key.Trim().Equals("Timeout"))
                    {
                        try
                        {
                            _timeout = Convert.ToInt32(dd.value.Trim());
                        }
                        catch (Exception ex)
                        {
                            _timeout = 30000;
                        }
                    }
                    if (dd.key.Trim().Equals("LicenseFile"))
                    {
                        _licenseFile = dd.value.Trim();
                    }
                    //if (dd.key.Trim().Equals("Password"))
                    //{
                    //    _password = dd.value.Trim();
                    //}
                    if (dd.key.Trim().Equals("GetFoto"))
                    {
                        _getFoto = Convert.ToBoolean(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("GetFirma"))
                    {
                        _getFirma = Convert.ToBoolean(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("GetDocFront"))
                    {
                        _getDocImageFront = Convert.ToBoolean(dd.value.Trim());
                    }
                    if (dd.key.Trim().Equals("ProcessType"))
                    {
                        try
                        {
                            _processType = Convert.ToInt32(dd.value.Trim());
                        }
                        catch (Exception ex)
                        {
                            _processType = 0;
                        }
                    }
                }

                LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021.v2021.Initialize...");
                _REGULA_HANDLER = new RegulaHandler();
                int ret = _REGULA_HANDLER.Init(_urlWS, _licenseFile, _processType);
                if (ret == 0)
                {
                    _isConfigured = _REGULA_HANDLER.IsInitialized(); // NamkuUtils.GetOrRefreshToken();
                    LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021.v2021.Initialize Configured OK!");
                } else
                {
                    _isConfigured = false;
                    LOG.Warn("Bio.Core.Matcher.Connector.Regula.v2021.v2021.Initialize Error - NOT Configured!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Connector.Regula.v2021.v2021.Initialize Error", ex);
                _isConfigured = false;
            }
            LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021.v2021.Initialize OUT!");
        }
 
#endregion Private Method

#region Implementation of IConnector

        private string _connectorId;

        private DynamicData _config;

        /// <summary>
        /// Id identificador del conector
        /// </summary>
        public string ConnectorId
        {
            get { return _connectorId; }
            set { _connectorId = value; }
        }

        /// <summary>
        /// Pares de key/value de configuracion para el conector
        /// </summary>
        public DynamicData Config
        {
            get { return _config; }
            set
            {
                 _config = value;
                 if (!_isConfigured) Initialization();
            }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            xmloutput = null;
            int _ret = Errors.IERR_OK;
            string msg;
            string xmloutbp;
            int iretremoto;
            int _currentThreshold;
            return _ret;

            //LOG.Debug("Verify In...");
            //DynamicData oXmlout = new DynamicData();
            //oXmlout.AddValue("message", "");
            //oXmlout.AddValue("trackid", "");
            //oXmlout.AddValue("status", "");
            //oXmlout.AddValue("result", "");
            //oXmlout.AddValue("score", "");
            //oXmlout.AddValue("threshold", "");
            //oXmlout.AddValue("timestamp", "");
            ////Added 02-2015 para agregar todas las BpTxConx cuando son varias
            ///*
            //     Consultationtype > string > nombre metodo
            //     Connectorid > string
            //     Trackid > string > Trackid externa
            //     Status > int > Retorno de servicio 
            //     Result > int > 1-Verify Positivo | 2-Verify Negativo (Si es verify si es get 0)
            //     Score > double > Score obtenido (Si es verify si es get 0)
            //     Threshold > double > Umbral utilizado (Si es verify si es get 0)
            //     Timestamp > string > fecha y hora tx externa
            //    Devuelve un Dynamicdata de la forma:
            //        <DynamicData>
            //            <DynamicDataItem>
            //                <key>tx1</key>
            //                <value>
            //                  <DynamicData>
            //                    <DynamicDataItem>
            //                         ... Cada valor d elos de arriba...                                          
            //                    </DynamicDataItem>
            //                  </DynamicData>
            //                </value>
            //            </DynamicDataItem>
            //          .........  
            //            <DynamicDataItem>
            //                <key>txN</key>
            //                <value>
            //                  <DynamicData>
            //                    <DynamicDataItem>
            //                         ... Cada valor d elos de arriba...                                          
            //                    </DynamicDataItem>
            //                  </DynamicData>
            //                </value>
            //            </DynamicDataItem>
            //       </DynamicData>
            // */
            //DynamicData oExternalTxs = new DynamicData();
            //oXmlout.AddValue("externaltxs", "");  //Tipo DynamicData

            ////oXmlout.SetValue("message", "");
            ////oXmlout.SetValue("trackid", "283734647");
            ////oXmlout.SetValue("status", "0");
            ////oXmlout.SetValue("result", "1");
            ////oXmlout.SetValue("score", "1000");
            ////oXmlout.SetValue("threshold", _threshold.ToString());
            ////oXmlout.SetValue("timestamp", FormatFechaHoraSRCeI("23112013100000"));
            ////xmloutput = DynamicData.SerializeToXml(oXmlout);
            ////return Errors.IERR_OK;

            //try
            //{
            //    LOG.Debug("Bio.Core.MatcherNecDMA - Ingresando...");
            //    if (!this._isConfigured)
            //    {
            //        msg = "Bio.Core.MatcherNecDMA Error = Connector No Configurado!";
            //        LOG.Fatal(msg);
            //        oXmlout.SetValue("message", msg);
            //        xmloutput = DynamicData.SerializeToXml(oXmlout);
            //        return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
            //    }
            //    else
            //    {
            //        LOG.Debug("Verify Configured...");
            //    }


            //    //1.-Deserializo parametros de entrada
            //    xmlinput = xmlinput.Replace("&#x0;", "");
            //    LOG.Debug("Bio.Core.MatcherNecDMA - xmlinput = " + xmlinput);
            //    XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlinput);
            //    if (oXmlIn == null)
            //    {
            //        msg = "Bio.Core.MatcherNecDMA Error deserealizando xmlinput";
            //        LOG.Fatal(msg);
            //        oXmlout.SetValue("message", msg);
            //        xmloutput = DynamicData.SerializeToXml(oXmlout);
            //        return Errors.IERR_BAD_PARAMETER;
            //    }
            //    else
            //    {
            //        LOG.Debug("Verify oXmlIn OK...");
            //    }

            //    //Tomo el WSQ si es que viene
            //    LOG.Debug("Bio.Core.MatcherNecDMA - Extrayendo WSQ y MinNEC from xmlinput...");
                
            //    int iErr = ExtraeSamplesFromParamIn(oXmlIn, out _wsqGenerated, out _rawGenerated, out _PC1Cedula, out _pcLen, out msg);

            //    if (_rawGenerated == null || _rawGenerated.Length == 0 ||
            //        _PC1Cedula == null || _PC1Cedula.Length == 0)
            //    {
            //        msg = "Bio.Core.MatcherNecDMA.Connector - Error extracción de parametros [len sample = " +
            //            _rawGenerated == null ? "null" : _rawGenerated.Length + " - len _PC1Cedula =" +
            //            _PC1Cedula == null ? "null" : _PC1Cedula.Length + "]";
            //        LOG.Fatal(msg);
            //        oXmlout.SetValue("message", msg);
            //        xmloutput = DynamicData.SerializeToXml(oXmlout); 
            //        return Errors.IERR_VERIFY;
            //    }
            //    else
            //    {
            //        LOG.Debug("Verify extract param OK...");
            //    }

            //    if (iErr == 0) //Sin error
            //    {
            //        LOG.Debug("Bio.Core.MatcherNecDMA - (raw != null!) = " + Convert.ToBase64String(_rawGenerated));
            //        LOG.Debug("Bio.Core.MatcherNecDMA - (_PC1Cedula != null!) = " + Convert.ToBase64String(_PC1Cedula));

            //        _currentThreshold = (oXmlIn.Threshold > 0) ? (int)oXmlIn.Threshold : _threshold;
            //        LOG.Debug("Bio.Core.MatcherNecDMA - _currentThreshold = " + _currentThreshold);

            //        int _Score = 0;
            //        LOG.Debug("Verify _typeConnector = " + _typeConnector.ToString());
            //        switch (_typeConnector)
	           //     {
            //            case 0:         //      0 - Interno al BioPortal con COM
            //                //iErr = VerifyInternoCOM(_rawGenerated, _pdf417Cedula, out msg, out _Score);
            //                //break;
            //            case 1:         //      1 - Interno al BioPortal con SDK NEW
            //                //TODO
            //                //oBKNec = new Biometrika.NEC.BiometrikaNEC();
            //                //iErr = VerifyInternoDLL(_rawGenerated, _pdf417Cedula, out msg, out _Score);
            //                try
            //                {
            //                    //using (Biometrika.DMA.WS.Biometrika_DMA_WS ws = new Biometrika.DMA.WS.Biometrika_DMA_WS())
            //                    //{
            //                    //    ws.Url = _urlWS;
            //                    //    ws.Timeout = _timeout;
            //                    //    iErr = ws.MatchingEx(oXmlIn.PersonalData.Valueid, Convert.ToBase64String(_PC1Cedula), 
            //                    //                         Convert.ToBase64String(_rawGenerated), 1, out msg);
            //                    //    _Score = iErr >= 0 ? iErr : 0;
            //                    _Score = CallServiceRestSharp(Convert.ToBase64String(_rawGenerated), Convert.ToBase64String(_PC1Cedula));
            //                } 
            //                catch (Exception ex)
            //                {
            //                    LOG.Error("Calling CallServiceRestSharp Ex - " + ex.Message);
            //                    iErr = -10000;  //Error consumiendo WS DMA
            //                }

            //                break;
            //            case 2:         //      2 - DMA NEC WS
            //                try
            //                {
            //                    LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService In...");
            //                    using (DMANECTransactionManagerWS.TransactionManagerWSService ws = new DMANECTransactionManagerWS.TransactionManagerWSService())
            //                    {
            //                        ws.Url = _urlWS;
            //                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService URL = " + ws.Url);
            //                        ws.Timeout = _timeout;
            //                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService Timeout = " + ws.Timeout);
            //                        DMANECTransactionManagerWS.TransactionIn TxIN = new DMANECTransactionManagerWS.TransactionIn();
            //                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService oXmlIn.PersonalData.Valueid = " + oXmlIn.PersonalData.Valueid);
            //                        byte[] byHeader = CreateHeader(oXmlIn.PersonalData.Valueid, out _trackIdExternal);
            //                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService _trackIdExternal = " + _trackIdExternal);
            //                        TxIN.header = byHeader;
            //                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxIN.biometricData = " + Convert.ToBase64String(_wsqGenerated));
            //                        TxIN.biometricData = _wsqGenerated;
            //                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxIN.transactionType = FEW1");
            //                        TxIN.transactionType = "FEW1";
            //                        TxIN.workstations = 0;

            //                        byte[] minutiaePC1 = null;
            //                        DMANECTransactionManagerWS.TransactionOut TxOUT = ws.generateTransaction(TxIN);
            //                        LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService ws.generateTransaction(TxIN) Out!");
            //                        if (TxOUT != null)
            //                        {
            //                            LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxOut != null");
            //                            if ((TxOUT.errorArray == null || TxOUT.errorArray.Length == 0) && (TxOUT.minutia != null))
            //                            {
            //                                //_Score = iErr >= 0 ? iErr : 0;
            //                                minutiaePC1 = TxOUT.minutia;
            //                                LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService minutiaePC1 = " + Convert.ToBase64String(minutiaePC1));
            //                            }
            //                            else
            //                            {
            //                                msg = "Bio.Core.MatcherNecDMA Error DMANECTransactionManagerWS.TransactionOut (FEW1) " +
            //                                    TxOUT.errorArray[0] + " - " + TxOUT.errorArray[1];
            //                                LOG.Fatal(msg);
            //                                oXmlout.SetValue("message", msg);
            //                                xmloutput = DynamicData.SerializeToXml(oXmlout);
            //                                return Errors.IERR_CONNECTOR_REMOTE_ERROR;
            //                            } 
            //                        }
            //                        else
            //                        {
            //                            msg = "Bio.Core.MatcherNecDMA Error DMANECTransactionManagerWS.TransactionOut (FEW1) null ";
            //                            LOG.Fatal(msg);
            //                            oXmlout.SetValue("message", msg);
            //                            xmloutput = DynamicData.SerializeToXml(oXmlout);
            //                            return Errors.IERR_CONNECTOR_REMOTE_ERROR;
            //                        }

            //                            //Sigo solo si extrae bien
            //                        if (minutiaePC1 != null) 
            //                        {
            //                            LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService In Compare...");
            //                            byHeader = CreateHeader(oXmlIn.PersonalData.Valueid, out _trackIdExternal);
            //                            LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService _trackIdExternal = " + _trackIdExternal); 
            //                            TxIN.header = byHeader;
            //                            TxIN.biometricData = minutiaePC1;
            //                            LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxIN.biometricData = " + Convert.ToBase64String(TxIN.biometricData));
            //                            TxIN.compareTo = _PC1Cedula; //ExtraePC1FromPDF417(_PC1Cedula);
            //                            LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxIN.compareTo = " + Convert.ToBase64String(TxIN.compareTo));
            //                            TxIN.transactionType = "VER11";
            //                            LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxIN.transactionType = VER11");

            //                            TxOUT = null;
            //                            TxOUT = ws.generateTransaction(TxIN);
            //                            LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService ws.generateTransaction(TxIN) Out!");
            //                        }

            //                        if (TxOUT != null)
            //                        {
            //                            LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService TxOut != null");
            //                            if (TxOUT.errorArray == null || TxOUT.errorArray.Length == 0)
            //                            {
            //                               _Score = TxOUT.score;
            //                               LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService Scrore = " + _Score.ToString());
            //                               if (TxOUT.result.Equals("Coincidencia"))
            //                               {
            //                                   iErr = 1;
            //                                   LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService (TxOUT.result == Coincidencia");
            //                               }
            //                               else
            //                               {
            //                                   if (TxOUT.result.Equals("No Coincidencia"))
            //                                   {
            //                                       iErr = 2;
            //                                       LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService (TxOUT.result == No Coincidencia");
            //                                   }
            //                                   else
            //                                   {
            //                                       iErr = -1;
            //                                       LOG.Debug("Verify DMANECTransactionManagerWS.TransactionManagerWSService (TxOUT.result == " + TxOUT.result + ")");
            //                                   }
            //                               }
            //                            }
            //                            else
            //                            {
            //                                msg = "Bio.Core.MatcherNecDMA Error DMANECTransactionManagerWS.generateTransaction (VER11) " +
            //                                    TxOUT.errorArray[0] + " - " + TxOUT.errorArray[1];
            //                                LOG.Fatal(msg);
            //                                oXmlout.SetValue("message", msg);
            //                                xmloutput = DynamicData.SerializeToXml(oXmlout);
            //                                return Errors.IERR_CONNECTOR_REMOTE_ERROR;
            //                            }

            //                        }
            //                        else
            //                        {
            //                            msg = "Bio.Core.MatcherNecDMA Error DMANECTransactionManagerWS.generateTransaction null (VER11) ";
            //                            LOG.Fatal(msg);
            //                            oXmlout.SetValue("message", msg);
            //                            xmloutput = DynamicData.SerializeToXml(oXmlout);
            //                            return Errors.IERR_CONNECTOR_REMOTE_ERROR;
            //                        }
            //                    }
            //                } 
            //                catch (Exception ex )
            //                {
            //                    iErr = -10000;  //Error consumiendo WS DMA
            //                }

            //                break;
		          //      default:
            //                break;
	           //     }
 
            //        if (iErr >= 0) // Error en verificacion
            //        {
            //            oXmlout.SetValue("message", "");
            //            oXmlout.SetValue("trackid", _trackIdExternal);
            //            oXmlout.SetValue("status", "0");
            //            oXmlout.SetValue("result", _Score >= _currentThreshold ? "1" : "2");
            //            oXmlout.SetValue("score", _Score.ToString());
            //            oXmlout.SetValue("threshold", _currentThreshold.ToString());
            //            string ts = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            //            oXmlout.SetValue("timestamp", ts);
            //            int res = (_Score >= _currentThreshold) ? 1 : 2;
            //            //oExternalTxs.AddValue("txVerify", GetDDFormExternalTx("AFISImpresionDactilarDS64", response, res, _currentThreshold, Score));
                        
            //            //oExternalTxs.AddValue("Consultationtype", "generateTransaction");
            //            //oExternalTxs.AddValue("Connectorid", "DMANec");
            //            //oExternalTxs.AddValue("Trackid", _trackIdExternal);
            //            //oExternalTxs.AddValue("Status", "0");
            //            //oExternalTxs.AddValue("Result",  _Score >= _currentThreshold ? "1" : "2");
            //            //oExternalTxs.AddValue("Score", _Score.ToString());
            //            //oExternalTxs.AddValue("Threshold",  _currentThreshold.ToString());
            //            //oExternalTxs.AddValue("Timestamp", ts);

            //            oExternalTxs.AddValue("VerifyDMA", GetDDFormExternalTx("generateTransaction", _trackIdExternal,
            //                                                                   res, _currentThreshold, _Score, ts));
            //            string extxml = DynamicData.SerializeToXml(oExternalTxs);
            //            oXmlout.SetValue("externaltxs", extxml);
            //            LOG.Debug("Verify Out externaltxs = " + extxml);
            //        }
            //        else
            //        {
            //            if (iErr == -10000) {
            //                msg = "Bio.Core.MatcherNecDMA.Connector - Respuesta Erronea del WS DMA - " +
            //                        "Response Error [" + iErr + " - Excp WS]";
            //            }
            //            else
            //            {
            //                msg = "Bio.Core.MatcherNecDMA.Connector - Respuesta Erronea del DMA - " +
            //                    "Response Error [" + iErr + "]";
            //            }
            //            LOG.Fatal(msg);

            //            oXmlout.SetValue("message", msg);
            //            xmloutput = DynamicData.SerializeToXml(oXmlout);
            //            return Errors.IERR_VERIFY;
            //        }
            //    }
            //    else
            //    {
            //        msg = "Bio.Core.MatcherNecDMA.Connector Error extrayendo muestras [" + iErr + "]";
            //        LOG.Fatal(msg);
            //        oXmlout.SetValue("message", msg);
            //        xmloutput = DynamicData.SerializeToXml(oXmlout);
            //        return Errors.IERR_BAD_PARAMETER;
            //    }
           
            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //}
            //catch (Exception ex)
            //{
            //    msg = "Bio.Core.MatcherNecDMA.Verify Error [" + ex.Message + "]";
            //    LOG.Error("Bio.Core.Matcher.MatcherNecDMA.Verify Error", ex);
            //    oXmlout.SetValue("message", msg);
            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //    return Errors.IERR_UNKNOWN;
            //}
            //LOG.Debug("Verify Out!");
            //return _ret;
        }

        private string GetDDFormExternalTx(string method, string trackid,
                                          int result, int th, double score, string ts)
        {
            string _ret = "";
            try
            {
                LOG.Debug("GetDDFormExternalTx In...");
                DynamicData ddTx = new DynamicData();
                ddTx.AddValue("Consultationtype", method);
                ddTx.AddValue("Connectorid", "NamkuMRZ");
                ddTx.AddValue("Trackid", trackid);
                ddTx.AddValue("Status", "0");
                ddTx.AddValue("Result", result.ToString());
                ddTx.AddValue("Score", score.ToString());
                ddTx.AddValue("Threshold", th.ToString());
                ddTx.AddValue("Timestamp", ts);
                _ret = DynamicData.SerializeToXml(ddTx);
                LOG.Debug("GetDDFormExternalTx Out - _ret = " + _ret);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.DMANec.GetDDFormExternalTx Error", ex);
                _ret = "";
            }
            return _ret;
        }

        private string ReadFromHDD(string path)
        {
            string sret = null;
            try
            {
                System.IO.StreamReader sr = new StreamReader(path);
                sret = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.MatcherNecDMA.ReadFromHDD Error", ex);
                sret = null;
            }
            return sret;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            /*
            Trackid" column="trackid" type="string" />
            Status" column="status" type="int" />
            Result" column="result" type="int" />
            Score" column="score" type="double" />
            Timestamp" column="timestamp" type="string" />
              
            Message = String 
             */

            string msg;
            xmloutput = null;
            int iretremoto;

            //DynamicData oXmlout = new DynamicData();
            //oXmlout.AddValue("message", "");
            //oXmlout.AddValue("trackid", "");
            //oXmlout.AddValue("status", "");
            //oXmlout.AddValue("result", "");
            //oXmlout.AddValue("score", "");
            //oXmlout.AddValue("timestamp", "");
            //oXmlout.AddValue("personaldata", "");

            //try
            //{
            //    if (!this._isConfigured)
            //    {
            //        msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error = Connector No Configurado!";
            //        LOG.Fatal(msg);
            //        oXmlout.SetValue("message", msg);
            //        xmloutput = DynamicData.SerializeToXml(oXmlout);
            //        return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
            //    }

            //    using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
            //    {
            //        ws.Timeout = this._timeout;
            //        ws.Url = this._urlWS;

            //        iretremoto = ws.Identify(xmlinput, out xmloutbp);
            //        if (iretremoto == Errors.IERR_OK)
            //        {
            //            XmlParamOut oXmlParamOut = Utils.XmlUtils.DeserializeObject<XmlParamOut>(xmloutbp);
            //            oXmlout.SetValue("message", "");
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            oXmlout.SetValue("trackid", oXmlParamOut.Trackid);
            //            oXmlout.SetValue("result", oXmlParamOut.Result.ToString());
            //            oXmlout.SetValue("score", oXmlParamOut.Score.ToString());
            //            oXmlout.SetValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
            //            oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
            //        }
            //        else
            //        {
            //            msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error en WS Remoto [" + iretremoto.ToString() + "]";
            //            LOG.Fatal(msg);
            //            oXmlout.SetValue("message", msg);
            //            oXmlout.SetValue("status", iretremoto.ToString());
            //            xmloutput = DynamicData.SerializeToXml(oXmlout);
            //            return Errors.IERR_CONNECTOR_REMOTE_ERROR;
            //        }
            //    }

            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //}
            //catch (Exception ex)
            //{
            //    msg = "Bio.Core.Matcher.ConnectorBioPortal4.Identify Error [" + ex.Message + "]";
            //    LOG.Error("Bio.Core.Matcher.ConnectorBioPortal4.Identity Error", ex);
            //    oXmlout.SetValue("message", msg);
            //    xmloutput = DynamicData.SerializeToXml(oXmlout);
            //    return Errors.IERR_UNKNOWN;
            //}

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de Recuperación. Ingresa información recuperar, 
        /// y se realiza la operacion.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Get(string xmlinput, out string xmloutput)
        {
            string msg;
            string xmloutbp;
            int iretremoto;
            string _msgerrConcat;
            RUN _currenRUN;
            DynamicData oXmlout = new DynamicData();
            oXmlout.AddValue("message", "");
            oXmlout.AddValue("trackid", "");
            oXmlout.AddValue("status", "");
            oXmlout.AddValue("result", "0");
            oXmlout.AddValue("score", "0");
            oXmlout.AddValue("threshold", "0");
            oXmlout.AddValue("timestamp", "");
            oXmlout.AddValue("personaldata", "");

            try
            {
                LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 IN...");
                if (!this._isConfigured)
                {
                    msg = "Bio.Core.Matcher.Connector.Regula.v2021 Error = Connector No Configurado!";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONNECTOR_NOT_CONFIGURED;
                }

                LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 Deserializa Param Entrada...");
                //1.-Deserializo parametros de entrada
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlinput);
                if (oXmlIn == null)
                {
                    msg = "Bio.Core.Matcher.Connector.Regula.v2021 Error deserealizando xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                } else
                {
                    if (oXmlIn.PersonalData != null) {
                        if (string.IsNullOrEmpty(oXmlIn.PersonalData.Valueid) || oXmlIn.PersonalData.Valueid.Equals("NA"))
                        {
                            LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 - oXmlIn.PersonalData.Valueid IN = " +
                                        (string.IsNullOrEmpty(oXmlIn.PersonalData.Valueid) ? "NULL" : oXmlIn.PersonalData.Valueid));
                        } else
                        {
                            LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 - oXmlIn.PersonalData.Valueid IN = " +
                                oXmlIn.PersonalData.Valueid);
                        }
                        
                    } else
                    {
                        LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 - oXmlIn.PersonalData IN = NULL");
                    }
                }

                LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 Check imagen cedula en oXmlIn.PersonalData.DocImageFront...");
                if (oXmlIn.PersonalData == null || (oXmlIn.PersonalData.DocImageFront == null && oXmlIn.PersonalData.DocImageBack == null))
                {
                    msg = "Bio.Core.Matcher.Connector.Regula.v2021 Error PersonalData Nulo o DocImageFront Nulo en xmlinput";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                PersonalData pdataOut = new PersonalData(); 
                DateTime start = DateTime.Now;
                LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 Pro IN...");
                //Biometrika.NamkuFaceAPI.Face.CedulaResponse extResult = NamkuUtils.NAMKU_API.ParseDocument(new MemoryStream(Convert.FromBase64String(oXmlIn.PersonalData.DocImageFront)));
                Document _Document;
                string msgerr; 
                int ret = _REGULA_HANDLER.RecognizeDocument(oXmlIn.PersonalData.DocImageFront, oXmlIn.PersonalData.DocImageBack, 
                                out _Document, out msgerr);
                LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 _REGULA_HANDLER.RecognizeDocument OUT - ret = " + ret);

                if (ret == 0) //Funciono OK
                {
                    LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 Rta no nula! => Completo salida");
                    if (_Document != null && _Document._DocInfo != null) {
                        LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 Rta Info no nula! => Completo personaldata...");
                        if ((!string.IsNullOrEmpty(_Document._DocInfo.ft_Document_Class_Code) &&
                             (_Document._DocInfo.ft_Document_Class_Code.Equals("IE") ||
                              _Document._DocInfo.ft_Document_Class_Code.Equals("ID"))) &&
                            ((!string.IsNullOrEmpty(_Document._DocInfo.ft_Issuing_State_Code) &&
                              _Document._DocInfo.ft_Issuing_State_Code.Equals("CHL"))
                            ))
                        {
                            pdataOut.Typeid = "RUT";
                        } else
                        {
                            pdataOut.Typeid = _Document._DocInfo.ft_Document_Class_Code;
                        }
                        pdataOut.Valueid = _Document._DocInfo.ft_Personal_Number;
                        pdataOut.Verificationsource = "DR"; //DR = Document Reader
                        pdataOut.Name = _Document._DocInfo.ft_Given_Names;
                        pdataOut.Patherlastname = _Document._DocInfo.ft_Surname;
                        pdataOut.Motherlastname = "";
                        pdataOut.Sex = _Document._DocInfo.ft_Sex;
                        pdataOut.Documentseriesnumber = _Document._DocInfo.ft_Document_Number;
                        pdataOut.Documentexpirationdate = !string.IsNullOrEmpty(_Document._DocInfo.ft_Date_of_Expiry) ?
                                                            Bio.Core.Utils.DateTimeHelper.Parse(_Document._DocInfo.ft_Date_of_Expiry) :
                                                            DateTime.MinValue; // FormatDateFromNamku(extResult.data.dni_data.expiration_date);
                        //pdataOut.Visatype = objDocRet.TipoVisa;
                        pdataOut.Birthdate = !string.IsNullOrEmpty(_Document._DocInfo.ft_Date_of_Birth) ?
                                                    Bio.Core.Utils.DateTimeHelper.Parse(_Document._DocInfo.ft_Date_of_Birth) :
                                                        DateTime.MinValue;
                        pdataOut.Creation = !string.IsNullOrEmpty(_Document._DocInfo.ft_Date_of_Issue)?
                                                        Bio.Core.Utils.DateTimeHelper.Parse(_Document._DocInfo.ft_Date_of_Issue):
                                                        DateTime.MinValue;
                        pdataOut.Birthplace = _Document._DocInfo.ft_Place_of_Birth;
                        pdataOut.Nationality = _Document._DocInfo.ft_Nationality;
                        pdataOut.Profession = _Document._DocInfo.ft_Profesion;
                        pdataOut.Visatype = _Document._DocInfo.ft_Visa;
                    } else
                    {
                        LOG.Warn("Bio.Core.Matcher.Connector.Regula.v2021 - Retorno de WebApi Regula con info nula"); 
                    }

                    if (_getDocImageFront && _Document != null && _Document._DocImages.Count > 0)
                    {
                        LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 Rta Images no nula! => Completo Front y Back...");
                        pdataOut.DocImageFront = GetFoto(_Document._DocImages, 199); //Front Cropped Image Document
                        //Added con pruebas de POS que la imagen del front era 207
                        if (string.IsNullOrEmpty(pdataOut.DocImageFront) && 
                            !string.IsNullOrEmpty(oXmlIn.PersonalData.DocImageFront))
                        {
                            pdataOut.DocImageFront = GetFoto(_Document._DocImages, 207);
                        }
                        pdataOut.DocImageBack = GetFoto(_Document._DocImages, 200); //Back Cropped Image Document
                        if (string.IsNullOrEmpty(pdataOut.DocImageBack) &&
                            !string.IsNullOrEmpty(oXmlIn.PersonalData.DocImageBack))
                        {
                            pdataOut.DocImageFront = GetFoto(_Document._DocImages, 207);
                        }
                    } else
                    {
                        if (_getDocImageFront)
                            LOG.Warn("Bio.Core.Matcher.Connector.Regula.v2021 Pide image front pero no retorno imagenes en _Document._DocImages...");
                    }
                    if (_getFoto && _Document != null && _Document._DocImages.Count > 0)
                    {
                        LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 Rta Images no nula! => Completo Photo...");
                        pdataOut.Photography = GetFoto(_Document._DocImages, 201); //Back Cropped Image Document
                    }
                    else
                    {
                        if (_getFoto)
                            LOG.Warn("Bio.Core.Matcher.Connector.Regula.v2021 Pide image foto pero no retorno imagenes en _Document._DocImages...");
                    }
                    if (_getFirma && _Document != null && _Document._DocImages.Count > 0)
                    {
                        LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 Rta Images no nula! => Completo Signature...");
                        pdataOut.Signatureimage = GetFoto(_Document._DocImages, 204); //Back Cropped Image Document
                    }
                    else
                    {
                        if (_getFirma)
                            LOG.Warn("Bio.Core.Matcher.Connector.Regula.v2021 Pide image signature pero no retorno imagenes en _Document._DocImages...");
                    }
                }
                else
                {
                    msg = "Bio.Core.Matcher.Connector.Regula.v2021 - Respuesta Nulo o error [" + 
                                        (string.IsNullOrEmpty(msgerr)?"Error Nulo":msgerr) + "]";
                    LOG.Fatal(msg);
                    oXmlout.SetValue("message", msg);
                    xmloutput = DynamicData.SerializeToXml(oXmlout);
                    return Errors.IERR_CONX_WS;
                }
                
                oXmlout.SetValue("message", "");
                oXmlout.SetValue("status", "0");
                oXmlout.SetValue("trackid", Guid.NewGuid().ToString("N"));
                oXmlout.SetValue("timestamp", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(pdataOut));
                //}
                xmloutput = DynamicData.SerializeToXml(oXmlout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Connector.Regula.v2021.Get Error [" + ex.Message + "]";
                LOG.Error("Bio.Core.Matcher.Connector.Regula.v2021.Get Error", ex);
                oXmlout.SetValue("message", msg);
                xmloutput = DynamicData.SerializeToXml(oXmlout);
                return Errors.IERR_UNKNOWN;
            }
            LOG.Debug("Bio.Core.Matcher.Connector.Regula.v2021 OUT!");
            return Errors.IERR_OK;
        }


        /// <summary>
        /// Dado que vienen en formatos no estandares si no parsea bien lo armo a mano
        /// </summary>
        /// <param name="sdate"></param>
        /// <returns></returns>
        private DateTime FormatDateFromNamku(string sdate)
        {
            DateTime ret = new DateTime(1900,1,1);
            int mes = 0;
            bool hayerr = false;
            try
            {
                try
                {
                    DateTime dt = DateTime.Parse(sdate); 
                    ret = dt; // dt.ToString("dd/MM/yyyy");
                }
                catch (Exception)
                {
                    hayerr = true;
                }

                if (hayerr)
                {
                    string[] arrDate = sdate.Split(' ');
                    if (arrDate[1].Trim().Equals("ENE") || arrDate[1].Trim().Equals("JAN")) mes = 1;
                    if (arrDate[1].Trim().Equals("FEB")) mes = 2;
                    if (arrDate[1].Trim().Equals("MAR")) mes = 3;
                    if (arrDate[1].Trim().Equals("ABR") || arrDate[1].Trim().Equals("APR")) mes = 4;
                    if (arrDate[1].Trim().Equals("MAY")) mes = 5;
                    if (arrDate[1].Trim().Equals("JUN")) mes = 6;
                    if (arrDate[1].Trim().Equals("JUL")) mes = 7;
                    if (arrDate[1].Trim().Equals("AGO") || arrDate[1].Trim().Equals("AUG")) mes = 8;
                    if (arrDate[1].Trim().Equals("SEP") || arrDate[1].Trim().Equals("SEPT")) mes = 9;
                    if (arrDate[1].Trim().Equals("OCT")) mes = 10;
                    if (arrDate[1].Trim().Equals("NOV")) mes = 11;
                    if (arrDate[1].Trim().Equals("DIC") || arrDate[1].Trim().Equals("DEC")) mes = 12;

                    DateTime dt = new DateTime(Convert.ToInt32(arrDate[2].Trim()), mes, Convert.ToInt32(arrDate[0].Trim()));
                    ret = dt; // dt.ToString("dd/MM/yyyy");
                }

            }
            catch (Exception ex)
            {
                ret = new DateTime(1900, 1, 1);
            }
            return ret;
        }

        //private string GetDDFormExternalTx(string CT, EncabezadoRespuesta response,
        //                                   int result, int th, double score)
        //{
        //    string _ret = "";
        //    try
        //    {
        //        if (response == null) return "";

        //        DynamicData ddTx = new DynamicData();
        //        ddTx.AddValue("Consultationtype", CT);
        //        ddTx.AddValue("Connectorid", "SRCeI.BioVerify2015");
        //        ddTx.AddValue("Trackid", response.IdTransaccion.ToString());
        //        ddTx.AddValue("Status", ConvertToNumber(response.Estado.ToString()));
        //        ddTx.AddValue("Result", result.ToString());
        //        ddTx.AddValue("Score", score.ToString());
        //        ddTx.AddValue("Threshold", th.ToString());
        //        ddTx.AddValue("Timestamp", response.FechaHoraOperacion.ToString());
        //        _ret = DynamicData.SerializeToXml(ddTx);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Bio.Core.MatcherNecDMA Error", ex);
        //        _ret = "";
        //    }
        //    return _ret;
        //}


        #endregion Implementation of IConnector

        private static string FormatFechaHoraSRCeI(string fechaoperacion)
        {
            string strRet = "";
            try
            {
                strRet = fechaoperacion.Substring(6, 2) + "/" +
                         fechaoperacion.Substring(4, 2) + "/" +
                         fechaoperacion.Substring(0, 4) + " " +
                         fechaoperacion.Substring(8, 2) + ":" +
                         fechaoperacion.Substring(10, 2) + ":" +
                         fechaoperacion.Substring(12, 2);
            }
            catch
            {
                strRet = "";
            }
            return strRet;
        }

        public void Dispose()
        {
            
        }

        /// <summary>
        /// Parsea PDF417 de cedula Chilena
        /// </summary>
        /// <param name="pdf417"></param>
        /// <param name="datos"></param>
        /// <returns></returns>
        //[ComVisible(true)]
        private int ParsePDF417(string pdf417, out byte[] _PC1Cedula, out int _pcLen)
        {
            int res = 0;
            byte[] pdf = null;
            _pcLen = 0;
            _PC1Cedula = null;

            try
            {
                LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.ParsePDF417 In...");
                if (!string.IsNullOrEmpty(pdf417))
                {
                    pdf = Convert.FromBase64String(pdf417);
                    
                    //string rut = Encoding.UTF7.GetString(pdf, 0, 9).Replace('\0', ' ').TrimEnd();
                    //string apellido = Encoding.UTF7.GetString(pdf, 19, 30).Replace('\0', ' ').TrimEnd();
                    //string pais = Encoding.UTF7.GetString(pdf, 49, 3).Replace('\0', ' ').TrimEnd();
                    //string numeroDeSerie = Encoding.UTF7.GetString(pdf, 58, 10).Replace('\0', ' ').TrimEnd();

                    //string aa = Encoding.UTF7.GetString(pdf, 52, 2);
                    //string mm = Encoding.UTF7.GetString(pdf, 54, 2);
                    //string dd = Encoding.UTF7.GetString(pdf, 56, 2);
                    //string expiracion = dd + "/" + mm + "/" + aa;

                    int finger = (pdf[73] << 24) + (pdf[72] << 16) + (pdf[71] << 8) + (pdf[70]);
                    _pcLen = (pdf[77] << 24) + (pdf[76] << 16) + (pdf[75] << 8) + (pdf[74]);
                    _PC1Cedula = new Byte[_pcLen];
                    Buffer.BlockCopy(pdf, 78, _PC1Cedula, 0, _pcLen);

                    LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.ParsePDF417 PCLen = " + _pcLen + " - MinucitaePC1 = " + Convert.ToBase64String(_PC1Cedula));

                    //datos = rut + "|" +
                    //        apellido + "|" +
                    //        pais + "|" +
                    //        numeroDeSerie + "|" +
                    //        expiracion + "|" +
                    //        appNumber + "|" +
                    //        disability + "|" +
                    //        docType + "|" +
                    //        finger.ToString() + "|" +
                    //        Convert.ToBase64String(pc1) + "|" +
                    //        pcLen.ToString();

                    //WriteLog("ParsePDF417 OK - " + datos);

                    res = 0;
                }
                else
                {
                    res = Bio.Core.Constant.Errors.IERR_NULL_TEMPLATE;
                    LOG.Error("Bio.Core.Matcher.Connector.NecDMA.ParsePDF417 Error PDF417 es nulo...");
                }
            }
            catch (Exception ex)
            {
                res = -1;
                LOG.Error("Bio.Core.Matcher.Connector.NecDMA.ParsePDF417 Error - Msg=" + ex.Message + " - Stack=" + ex.StackTrace);
            }

            LOG.Debug("Bio.Core.Matcher.Connector.NecDMA.ParsePDF417 Out!");
            return res;
        }

        ///   199 - Front Cedula Cropped (Inventado por mi este codigo)
        ///   200 - Back Cedula Cropped (Inventado por mi este codigo)
        ///   201 - Photo
        ///   202 - Fingerprint
        ///   204 - Signature
        ///   205 - Barcode
        ///   210 - Ghost Photo
        private string GetFoto(List<Biometrika.Regula.WebAPI.Api._2021.DOCInfoImage> docImages, int v)
        {
            foreach (DOCInfoImage item in docImages)
            {
                if (item.code.Equals(v.ToString())) return item.b64image;
            }
            return null;
        }
    }
}
