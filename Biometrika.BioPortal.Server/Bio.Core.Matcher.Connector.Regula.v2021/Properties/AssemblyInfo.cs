﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Bio.Core.Matcher.Connector.Regula.v2021")]
[assembly: AssemblyDescription("Conector a WebAPI Regula v2021")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Biometrika S.A.")]
[assembly: AssemblyProduct("Bio.Core.Matcher.Connector.Regula.v2021")]
[assembly: AssemblyCopyright("Copyright ©Biometrika  2021")]
[assembly: AssemblyTrademark("Bioemtrika")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("131127b2-45d9-4825-ba33-d320dc0928f0")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("5.9.0.0")]
[assembly: AssemblyFileVersion("5.9.0.0")]
