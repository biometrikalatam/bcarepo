﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BVI_Proxy
{
    public class WebApiApplication : System.Web.HttpApplication
    {

        /// <summary>
        /// LOG del modulo
        /// </summary>
        private static readonly ILog LOG = LogManager.GetLogger(typeof(WebApiApplication));

        internal string ROOT;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ROOT = HttpContext.Current.Server.MapPath(".");
            //0.- Inicializo LOG
            XmlConfigurator.Configure(new FileInfo(ROOT + ConfigurationManager.AppSettings["Log4netConfigPath"]));
            LOG.Info("BVI Proxy Starting...");
            LOG.Info("   => URL BioPortal WS => " + ConfigurationManager.AppSettings["URLBP.WS"]);
            LOG.Info("   => URL BioPortal PROCESS => " + ConfigurationManager.AppSettings["URLBP.PROCESS"]);
            LOG.Info("   => Timeout BioPortal => " + ConfigurationManager.AppSettings["BP.Timeout"]);

        }
    }
}
