﻿using Bio.Core.Api;
using BioPortal.Server.Api;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BVI_Proxy.HELPERS
{
    public class BPHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BPHelper));

        public static bool IsCorrectParamInForAction(int company, string typeid, string valueid, string clientid, string minutiaeISO, out string msgerr)
        {
            bool ret = false;
            msgerr = null;
            try
            {
                LOG.Debug("BPHelper.IsCorrectParamInForAction IN...");
                LOG.Debug("BPHelper.IsCorrectParamInForAction - Company = " + company);
                LOG.Debug("BPHelper.IsCorrectParamInForAction - clientid = " + clientid);
                LOG.Debug("BPHelper.IsCorrectParamInForAction - minutiaeISO = " + minutiaeISO);
                LOG.Debug("BPHelper.IsCorrectParamInForAction - typeid = " + typeid);
                LOG.Debug("BPHelper.IsCorrectParamInForAction - typeid = " + valueid);
                if (company <= 0 || string.IsNullOrEmpty(clientid) || string.IsNullOrEmpty(minutiaeISO) ||
                    string.IsNullOrEmpty(typeid) || string.IsNullOrEmpty(valueid))
                {
                    ret = false;
                    LOG.Debug("BPHelper.IsCorrectParamInForAction NOOK!");
                }
                else
                {
                    LOG.Debug("BPHelper.IsCorrectParamInForAction OK!");
                    ret = true;
                }
            }
            catch (Exception ex)
            {
                ret = false;
                msgerr = "BPHelper.IsCorrectParamInForAction Error [" + ex.Message + "]";
                LOG.Error("BPHelper.IsCorrectParamInForAction Error ", ex);
            }
            LOG.Debug("BPHelper.IsCorrectParamInForAction OUT!");
            return ret;
        }


        /// <summary>
        /// PErmite convertir Minucias de ISO a ISOCompact usandoi el Plugin de BioPortal v5.8
        /// </summary>
        /// <param name="company"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="clientid"></param>
        /// <param name="minutiaeISO"></param>
        /// <param name="minutiaeISOCP"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int Process(int company, string typeid, string valueid, string clientid, string minutiaeISO, out string minutiaeISOCP, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            minutiaeISOCP = null;

            try
            {
                LOG.Info("BPHelper.Process IN [typeid = " + typeid + "-valueid=" + valueid + "]");
                using (BioPortalServerPluginProcess.BioPortal_Server_Plugin_Process ws = new BioPortalServerPluginProcess.BioPortal_Server_Plugin_Process())
                {
                    ws.Url = ConfigurationManager.AppSettings["URLBP.PROCESS"];
                    ws.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["BP.Timeout"]);
                    
                    BioPortal.Server.Api.XmlParamIn paramin = new BioPortal.Server.Api.XmlParamIn();
                    paramin.Actionid = 1000;
                    paramin.Companyid = company;
                    paramin.Clientid = clientid;
                    paramin.Origin = 12; //BVIProxy
                    Sample sample = new Sample();
                    sample.Minutiaetype = 13;
                    sample.Data = minutiaeISO;
                    paramin.SampleCollection = new List<Sample>();
                    paramin.SampleCollection.Add(sample);
                    paramin.PersonalData = new PersonalData();
                    paramin.PersonalData.Typeid = typeid;
                    paramin.PersonalData.Valueid = valueid;

                    string xmlin = BioPortal.Server.Api.XmlUtils.SerializeAnObject(paramin);
                    string xmlout;
                    LOG.Debug("BPHelper.Process Consultando en => " + ws.Url + " [Timeout=" + ws.Timeout + "]");
                    ret = ws.Process(xmlin, out xmlout);

                    BioPortal.Server.Api.XmlParamOut paramout = null;
                    if (!string.IsNullOrEmpty(xmlout))
                         paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
                    if (ret == 0)
                    {
                        minutiaeISOCP = paramout.Additionaldata.DynamicDataItems[0].value;
                        LOG.Debug("BPHelper.Process minutiaeISOCP = " + minutiaeISOCP);
                    } else
                    {
                        msgerr = paramout.Message;
                        LOG.Debug("BPHelper.Process Error = " + msgerr);
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BPHelper.Process Error ", ex);
            }
            LOG.Info("BPHelper.Process OUT! [Ret=" + ret + "]");
            return ret;
        }

        /// <summary>
        /// Verifica WSQ o RAW contra PDF417 con NEC
        /// </summary>
        /// <param name="company"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="clientid"></param>
        /// <param name="pdf417"></param>
        /// <param name="sample"></param>
        /// <param name="minutiaetype"></param>
        /// <param name="threshold"></param>
        /// <param name="result"></param>
        /// <param name="score"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int Verify(int company, string typeid, string valueid, string clientid, string pdf417, string sample, int minutiaetype, int threshold, 
            out int result, out double score, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            result = 2;
            score = 0;
            try
            {
                LOG.Info("BPHelper.Verify IN [typeid = " + typeid + "-valueid=" + valueid + "]");
                using (BioPortalServerWS.BioPortalServerWS ws = new BioPortalServerWS.BioPortalServerWS())
                {
                    ws.Url = ConfigurationManager.AppSettings["URLBP.WS"];
                    ws.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["BP.Timeout"]);
                    BioPortal.Server.Api.XmlParamIn pin = new BioPortal.Server.Api.XmlParamIn();
                    pin.Actionid = Bio.Core.Api.Constant.Action.ACTION_VERIFY;  //Valor 1
                    pin.Additionaldata = null;
                    pin.Authenticationfactor = Bio.Core.Api.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;  //Valor 2
                    pin.Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_NEC; //Valor 1
                    pin.Bodypart = Bio.Core.Api.Constant.BodyPart.BODYPART_DEDOPULGARDERECHO; //Valor 1
                    pin.Clientid = clientid;
                    pin.Companyid = company;
                    pin.Enduser = "EnUserTest";
                    pin.Ipenduser = "127.0.0.1";
                    pin.Matchingtype = 1;
                    pin.Origin = 1;  //Luego en produccion les inidcamos un valor 

                    //Primero agrego la muestra
                    pin.SampleCollection = new List<Bio.Core.Api.Sample>();
                    Bio.Core.Api.Sample sample1 = new Bio.Core.Api.Sample();
                    sample1.Data = sample;
                    sample1.Minutiaetype = minutiaetype; // Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_WSQ;  //Valor 21 o RAW Valor 22
                    sample1.Additionaldata = null;
                    pin.SampleCollection.Add(sample1);

                    //Segundo agrego PDF417
                    Bio.Core.Api.Sample sample2 = new Bio.Core.Api.Sample();
                    sample2.Data = pdf417;
                    sample2.Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_PDF417CEDULA;  //Valor 25
                    sample2.Additionaldata = null;
                    pin.SampleCollection.Add(sample2);

                    pin.SaveVerified = 1;
                    pin.Threshold = threshold; // Convert.ToDouble(textBox4.Text);
                    pin.Userid = 0;
                    pin.Verifybyconnectorid = "DMANec";
                    pin.InsertOption = 1;
                    pin.PersonalData = new Bio.Core.Api.PersonalData();
                    pin.PersonalData.Typeid = typeid;
                    pin.PersonalData.Valueid = valueid;
                    pin.OperationOrder = Bio.Core.Api.Constant.OperationOrder.OPERATIONORDER_REMOTEONLY; //Valor 3

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    LOG.Debug("BPHelper.Verify Consultando en => " + ws.Url + " [Timeout=" + ws.Timeout + "]");
                    string xmlparamout;
                    ret = ws.Verify(xmlparamin, out xmlparamout);

                    BioPortal.Server.Api.XmlParamOut paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlparamout);
                    if (ret == 0)
                    {
                        result = Convert.ToInt32(paramout.Result);
                        score = paramout.Score; 
                        LOG.Debug("BPHelper.Verify Result = " + result + " - score = " + score);
                    }
                    else
                    {
                        msgerr = paramout.Message;
                        LOG.Debug("BPHelper.Verify Error = " + msgerr);
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BPHelper.Verify Error ", ex);
            }
            LOG.Info("BPHelper.Process OUT! [Ret=" + ret + "]");
            return ret;
        }

        /// <summary>
        /// Chequea parametros de entrada para hacer verificacion contra cedula vieja
        /// </summary>
        /// <param name="company"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="clientid"></param>
        /// <param name="pdf417"></param>
        /// <param name="sample"></param>
        /// <param name="minutiaetype"></param>
        /// <param name="threshold"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static bool IsCorrectParamInForActionV(int company, string typeid, string valueid, string clientid, string pdf417, string sample, int minutiaetype, int threshold, 
            out string msgerr)
        {
            bool ret = false;
            msgerr = null;
            try
            {
                LOG.Debug("BPHelper.IsCorrectParamInForActionV IN...");
                LOG.Debug("BPHelper.IsCorrectParamInForActionV - Company = " + company);
                LOG.Debug("BPHelper.IsCorrectParamInForActionV - clientid = " + clientid);
                LOG.Debug("BPHelper.IsCorrectParamInForActionV - pdf417 = " + pdf417);
                LOG.Debug("BPHelper.IsCorrectParamInForActionV - sample = " + sample);
                LOG.Debug("BPHelper.IsCorrectParamInForActionV - minutiaetype = " + minutiaetype);
                LOG.Debug("BPHelper.IsCorrectParamInForActionV - threshold = " + threshold);
                LOG.Debug("BPHelper.IsCorrectParamInForActionV - typeid = " + typeid);
                LOG.Debug("BPHelper.IsCorrectParamInForActionV - typeid = " + valueid);
                if (company <= 0 || string.IsNullOrEmpty(clientid) || string.IsNullOrEmpty(pdf417) || string.IsNullOrEmpty(sample) ||
                    string.IsNullOrEmpty(typeid) || string.IsNullOrEmpty(valueid) || threshold < 75 || (minutiaetype != 21 && minutiaetype != 22))
                {
                    ret = false;
                    LOG.Debug("BPHelper.IsCorrectParamInForActionV NOOK!");
                }
                else
                {
                    LOG.Debug("BPHelper.IsCorrectParamInForActionV OK!");
                    ret = true;
                }
            }
            catch (Exception ex)
            {
                ret = false;
                msgerr = "BPHelper.IsCorrectParamInForActionV Error [" + ex.Message + "]";
                LOG.Error("BPHelper.IsCorrectParamInForActionV Error ", ex);
            }
            LOG.Debug("BPHelper.IsCorrectParamInForActionV OUT!");
            return ret;
        }
    }
}