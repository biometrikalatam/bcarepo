﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BVI_Proxy.HELPERS;
using Bio.Core.Api.Constant;

namespace BVI_Proxy
{
    /// <summary>
    /// Summary description for BVIProxy
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BVIProxy : System.Web.Services.WebService
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BVIProxy));

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        /// <summary>
        /// Dada una minucia ISO la transforma en ISO Compact via BioPortal Plugin
        /// </summary>
        /// <param name="company"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="clientid"></param>
        /// <param name="minutiaeISO"></param>
        /// <param name="minutiaeISOCP"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        [WebMethod]
        public int ConvertISOtoISOCP(int company, string typeid, string valueid, string clientid, string minutiaeISO, out string minutiaeISOCP, out string msgerr)
        {
            int ret = -1;
            minutiaeISOCP = null;
            msgerr = null;
            try
            {
                //1.- Chequeo que esté toda la información necesaria sino retorno con ERR
                if (!BPHelper.IsCorrectParamInForAction(company, typeid, valueid, clientid, minutiaeISO, out msgerr))
                {
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Llamo a ServicesPluginManager.Process con parametros deserializados.
                ret = BPHelper.Process(company, typeid, valueid, clientid, minutiaeISO, out minutiaeISOCP, out msgerr);
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BVIProxy.ConvertISOtoISOCP Error", ex);     
            }
            return ret;
        }

        /// <summary>
        /// Dado un PDF417 y sample (WSQ o RAW) hace matching considerando el threshold indicado.
        /// </summary>
        /// <param name="company"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="clientid"></param>
        /// <param name="pdf417"></param>
        /// <param name="sample"></param>
        /// <param name="minutiaetype"></param>
        /// <param name="threshold"></param>
        /// <param name="result"></param>
        /// <param name="score"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        [WebMethod]
        public int Verify(int company, string typeid, string valueid, string clientid, string pdf417, string sample, int minutiaetype, int threshold,
                          out int result, out double score, out string msgerr)
        {
            int ret = -1;
            result = 2;
            score = 0;
            msgerr = null;
            try
            {
                //1.- Chequeo que esté toda la información necesaria sino retorno con ERR
                if (!BPHelper.IsCorrectParamInForActionV(company, typeid, valueid, clientid, pdf417, sample, minutiaetype, threshold, out msgerr))
                {
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Llamo a ServicesPluginManager.Process con parametros deserializados.
                ret = BPHelper.Verify(company, typeid, valueid, clientid, pdf417, sample, minutiaetype, threshold, out result, out score, out msgerr);
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BVIProxy.Verify Error", ex);
            }
            return ret;
        }
    }
}
