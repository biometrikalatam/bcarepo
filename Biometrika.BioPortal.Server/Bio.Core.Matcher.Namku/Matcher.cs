﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Core.Serialize;
using log4net;
using System.Net.Http;
using System.Net.Http.Headers;
using MultipartFormDataSample.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Bio.Core.Matcher.Namku
{
    public class Matcher : IMatcher
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Matcher));

#region Implementation of IMatcher

        private int _authenticationFactor;
        private int _minutiaeType;
        private double _threshold;
        private int _matchingType;

        private string _parameters;

        //FPhi.Matcher.Matcher matcher;
        //FPhi.Matcher.MatcherConfigurationManager matcherConfig;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }


        /// <summary>
        /// Tecnologia a utilizar para matching
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de verificación considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Dos posibles valores:
        ///     0 - FIRST   -> El primer matching positivo
        ///     1 - BEST    -> El mejor matching positivo
        /// </summary>
        public int MatchingType
        {
            get { return _matchingType; }
            set { _matchingType = (value == 1 || value == 2) ? value : 1; }
        }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// y nuevo, parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;
            NamkuResponseConpareFace oResponse = null;

            try
            {
                if (!NamkuUtils.IsInitialized)
                {
                    if (!NamkuUtils.Initialize(_parameters))
                    {
                        msg = "Bio.Core.Matcher.Namku.Matcher Error Chequeando Parameters";
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_BAD_PARAMETER;
                    }
                }

                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                try
                {
                    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.Namku.Matcher Error importando CurrentTemplate " +
                         "[ex=" + ex.Message + "]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Preapro prametros para enviar
                ImageSet imageSetNew = new ImageSet();
                MultipartFormDataSample.Client.Image parameter1 = new MultipartFormDataSample.Client.Image();
                parameter1.FileName = @"image1.jpg";
                //parameter1.ImageData = Convert.FromBase64String(image1);
                parameter1.MimeType = "image/jpeg";

                MultipartFormDataSample.Client.Image parameter2 = new MultipartFormDataSample.Client.Image();
                parameter2.FileName = @"image2";
                //parameter2.ImageData = Convert.FromBase64String(image2);
                parameter2.MimeType = "image/jpeg";

                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                for (int i = 0; i < listTemplatesBase.Count; ++i)
                {

                    parameter1.ImageData = byCurrentTemplate;
                    parameter2.ImageData = listTemplatesBase[i].Data;
                    //authenticationResult = matcher.Authenticate(user1, listTemplatesBase[i].Data);

                    imageSetNew.Images = new List<MultipartFormDataSample.Client.Image>();
                    imageSetNew.Images.Add(parameter1);
                    imageSetNew.Images.Add(parameter2);

                    var multipartContent = new MultipartFormDataContent();

                    var imageSetJson = JsonConvert.SerializeObject(imageSetNew,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        });

                    multipartContent.Add(new StringContent(imageSetJson, Encoding.UTF8, "application/json"), "imageset");

                    int counter = 0;
                    foreach (var image in imageSetNew.Images)
                    {
                        var imageContent = new ByteArrayContent(image.ImageData);
                        imageContent.Headers.ContentType = new MediaTypeHeaderValue(image.MimeType);
                        multipartContent.Add(imageContent, "image" + counter++, image.FileName);
                    }

                    var response = new HttpClient()
                         .PostAsync(NamkuUtils._URLServcie, multipartContent)
                         .Result;

                    var responseContent = response.Content.ReadAsStringAsync().Result;

                    oResponse = JsonConvert.DeserializeObject<NamkuResponseConpareFace>(responseContent);
                    LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse != null => " + (oResponse != null).ToString());
                    if (oResponse.output != null) LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse.output.distance => " + (oResponse.output.distance).ToString());
                    if (oResponse.error != null && oResponse.error.message != null) LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse.error.message => " + oResponse.error.message);

                    score = (oResponse != null && oResponse.output != null) ? oResponse.output.distance : 5;
                    if (score <= _threshold) //La distancia debe ser menor al threshold indicado, cunado mas cerca de 0 mejor
                    {
                        verifyOk = true;
                        //Si es Matching type first => Retorno
                        if (this._matchingType == 1)
                        {
                            bestscore = score;
                            templateVerified = listTemplatesBase[i];
                            break;
                        }
                        else
                        {
                            if (score < bestscore)
                            {
                                templateVerified = listTemplatesBase[i];
                                bestscore = score;
                            }
                        }
                    }
                    else
                    {
                        bestscore = score;
                    }
                }

                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                }
                else
                {
                    paramout.SetValue("message", "No hubo matching positivo [" + (oResponse != null &&
                                                                                  oResponse.error != null &&
                                                                                  oResponse.error.message != null ? oResponse.error.message : "") + "]");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                LOG.Debug("Bio.Core.Matcher.Namku.Matcher xmloutput = " + xmloutput);
            }
            catch (Exception ex) 
            {
                msg = "Bio.Core.Matcher.Namku.Matcher.Verify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_VERIFY;
            }

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// y lista contra que verificar, parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;

            try
            {
                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                try
                {
                    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.Namku.Matcher Error importando CurrentTemplate " +
                         "[ex=" + ex.Message + "]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Preapro prametros para enviar
                ImageSet imageSetNew = new ImageSet();
                MultipartFormDataSample.Client.Image parameter1 = new MultipartFormDataSample.Client.Image();
                parameter1.FileName = @"image1.jpg";
                //parameter1.ImageData = Convert.FromBase64String(image1);
                parameter1.MimeType = "image/jpeg";

                MultipartFormDataSample.Client.Image parameter2 = new MultipartFormDataSample.Client.Image();
                parameter2.FileName = @"image2";
                //parameter2.ImageData = Convert.FromBase64String(image2);
                parameter2.MimeType = "image/jpeg";

                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                for (int i = 0; i < listTemplatesBase.Count; ++i)
                {
                    parameter1.ImageData = byCurrentTemplate;
                    parameter2.ImageData = listTemplatesBase[i].Data;
                    
                    imageSetNew.Images = new List<MultipartFormDataSample.Client.Image>();
                    imageSetNew.Images.Add(parameter1);
                    imageSetNew.Images.Add(parameter2);

                    var multipartContent = new MultipartFormDataContent();

                    var imageSetJson = JsonConvert.SerializeObject(imageSetNew,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        });

                    multipartContent.Add(new StringContent(imageSetJson, Encoding.UTF8, "application/json"), "imageset");

                    int counter = 0;
                    foreach (var image in imageSetNew.Images)
                    {
                        var imageContent = new ByteArrayContent(image.ImageData);
                        imageContent.Headers.ContentType = new MediaTypeHeaderValue(image.MimeType);
                        multipartContent.Add(imageContent, "image" + counter++, image.FileName);
                    }

                    var response = new HttpClient()
                        .PostAsync(NamkuUtils._URLServcie, multipartContent)
                        .Result;

                    var responseContent = response.Content.ReadAsStringAsync().Result;

                    NamkuResponseConpareFace oResponse = JsonConvert.DeserializeObject<NamkuResponseConpareFace>(responseContent);
                    LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse != null => " + (oResponse != null).ToString());
                    if (oResponse.output != null) LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse.output.distance => " + (oResponse.output.distance).ToString());
                    if (oResponse.error != null && oResponse.error.message != null) LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse.error.message => " + oResponse.error.message);

                    score = (oResponse != null && oResponse.output != null) ? oResponse.output.distance : 5;
                    if (score <= _threshold) //La distancia debe ser menor al threshold indicado, cunado mas cerca de 0 mejor
                    {
                        verifyOk = true;
                        //Si es Matching type first => Retorno
                        if (this._matchingType == 1)
                        {
                            bestscore = score;
                            templateVerified = listTemplatesBase[i];
                            break;
                        }
                        else
                        {
                            if (score < bestscore)
                            {
                                templateVerified = listTemplatesBase[i];
                                bestscore = score;
                            }
                        }
                    }
                }

                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                }
                else
                {
                    paramout.SetValue("message", "No hubo matching positivo");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Namku.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        public int Identify(ITemplate templateCurrent, List<Bio.Core.Matcher.Template> listTemplatePeople, 
                            out float score, out int idBir)
        {
            idBir = 0;
            score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;

            try
            {
                byCurrentTemplate = templateCurrent.Data;

                ////2.- Genero objeto para matching
                //matcherConfig = new MatcherConfigurationManager();
                //matcherConfig.MatcherSecurityLevel = NamkuUtils._SecurityLevel;

                //matcher = new FPhi.Matcher.Matcher(matcherConfig);

                ////Deserializo la lista de templates pasados contra que verificar
                //List<Bio.Core.Matcher.Template> listTemplatesBase = listTemplatePeople;
                ////Contendra el template contra el que se verifico positivametne si asi fue,
                ////para datos de bodypart, companyid, etc.
                //Bio.Core.Matcher.Template templateVerified = null;

                ////Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                //// Crear el primer usuario.
                //user1 = matcher.CreateUser(byCurrentTemplate);

                //// Verificar los resultados de las extracciones.
                //AuthenticationResult authenticationResult;

                //for (int i = 0; i < listTemplatesBase.Count; ++i)
                //{
                //    authenticationResult = matcher.Authenticate(user1, listTemplatesBase[i].Data);
                //    score = (float)authenticationResult.Similarity;
                //    if (authenticationResult.IsPositiveMatch && score >= _threshold)
                //    {
                //        verifyOk = true;
                //        //Si es Matching type first => Retorno
                //        if (this._matchingType == 1)
                //        {
                //            bestscore = score;
                //            templateVerified = listTemplatesBase[i];
                //            break;
                //        }
                //        else
                //        {
                //            if (score > bestscore)
                //            {
                //                templateVerified = listTemplatesBase[i];
                //                bestscore = score;
                //            }
                //        }
                //    }
                //}

                //2.- Preapro prametros para enviar
                ImageSet imageSetNew = new ImageSet();
                MultipartFormDataSample.Client.Image parameter1 = new MultipartFormDataSample.Client.Image();
                parameter1.FileName = @"image1.jpg";
                //parameter1.ImageData = Convert.FromBase64String(image1);
                parameter1.MimeType = "image/jpeg";

                MultipartFormDataSample.Client.Image parameter2 = new MultipartFormDataSample.Client.Image();
                parameter2.FileName = @"image2";
                //parameter2.ImageData = Convert.FromBase64String(image2);
                parameter2.MimeType = "image/jpeg";

                //Deserializo la lista de templates pasados contra que verificar
                List < Bio.Core.Matcher.Template > listTemplatesBase = listTemplatePeople;
                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                for (int i = 0; i < listTemplatesBase.Count; ++i)
                {
                    parameter1.ImageData = byCurrentTemplate;
                    parameter2.ImageData = listTemplatesBase[i].Data;

                    imageSetNew.Images = new List<MultipartFormDataSample.Client.Image>();
                    imageSetNew.Images.Add(parameter1);
                    imageSetNew.Images.Add(parameter2);

                    var multipartContent = new MultipartFormDataContent();

                    var imageSetJson = JsonConvert.SerializeObject(imageSetNew,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        });

                    multipartContent.Add(new StringContent(imageSetJson, Encoding.UTF8, "application/json"), "imageset");

                    int counter = 0;
                    foreach (var image in imageSetNew.Images)
                    {
                        var imageContent = new ByteArrayContent(image.ImageData);
                        imageContent.Headers.ContentType = new MediaTypeHeaderValue(image.MimeType);
                        multipartContent.Add(imageContent, "image" + counter++, image.FileName);
                    }

                    var response = new HttpClient()
                        .PostAsync(NamkuUtils._URLServcie, multipartContent)
                        .Result;

                    var responseContent = response.Content.ReadAsStringAsync().Result;

                    NamkuResponseConpareFace oResponse = JsonConvert.DeserializeObject<NamkuResponseConpareFace>(responseContent);
                    LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse != null => " + (oResponse != null).ToString());
                    if (oResponse.output != null) LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse.output.distance => " + (oResponse.output.distance).ToString());
                    if (oResponse.error != null && oResponse.error.message != null) LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse.error.message => " + oResponse.error.message);

                    score = (oResponse != null && oResponse.output != null) ? oResponse.output.distance : 5;
                    if (score <= _threshold) //La distancia debe ser menor al threshold indicado, cunado mas cerca de 0 mejor
                    {
                        verifyOk = true;
                        //Si es Matching type first => Retorno
                        if (this._matchingType == 1)
                        {
                            bestscore = score;
                            templateVerified = listTemplatesBase[i];
                            break;
                        }
                        else
                        {
                            if (score < bestscore)
                            {
                                templateVerified = listTemplatesBase[i];
                                bestscore = score;
                            }
                        }
                    }
                }

                //3.- Si verifico bien armo salida
                score = (float)bestscore;
                idBir = templateVerified == null ? 0 : templateVerified.Id;

                //paramout.AddValue("score", bestscore);
                //paramout.AddValue("result", verifyOk ? 1 : 2);
                //paramout.AddValue("threshold", threshold);
                //if (verifyOk)
                //{
                //    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                //    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                //    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                //    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                //    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                //}
                //else
                //{
                //    paramout.SetValue("message", "No hubo matching positivo");
                //}
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Namku.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        public int Verify(ITemplate template1, ITemplate template2, out float score)
        {
            throw new NotImplementedException();
        }


        #endregion

        public void Dispose()
        {
        }
    }

    //{"output": {"distance": "0.0"}}
    public class output
    {

        public float distance { get; set; }

    }

    public class error
    {
        public string message { get; set; }
        public string reason { get; set; }

    }

    /*
       {
         "data":null,
         "error":{
              "message":"No se detectaron caras en alguna de las imagenes",
               "reason":"noFaces"},
         "status_code":400,
         "success":false
        }
    */
    public class NamkuResponseConpareFace
    {
        public output output { get; set; }
        public string data { get; set; }
        public string status_code { get; set; }
        public string success { get; set; }
        public error error { get; set; }

    }
}
