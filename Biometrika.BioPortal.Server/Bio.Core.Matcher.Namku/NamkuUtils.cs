﻿
using System;
using log4net;


namespace Bio.Core.Matcher.Namku
{
    public static class NamkuUtils
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NamkuUtils));

        public static bool IsInitialized;

        public static double _SecurityLevel = 0.7;
        public static string _URLServcie = "http://test.namku.cl:5000/api/v1.2/tesnor/compare_faces";


        /// <summary>
        /// Inicializa la licencia la primera vez que se usa una clase de este Namespace.
        /// </summary>
        public static bool Initialize(string parameters)
        {

            try
            {

                if (parameters == null)
                {
                    LOG.Error("Bio.Core.Matcher.Namku.Initialize Error - Parameters Matcher = NULL");
                    return false;
                }

                LOG.Debug("Bio.Core.Matcher.Namku.Initialize - ExtractPath(parameters) de parameters=" + parameters);
                IsInitialized = ExtractParams(parameters, out _SecurityLevel, out _URLServcie);

                if (!IsInitialized)
                {
                    LOG.Error("Bio.Core.Matcher.Namku.Initialize Error - Component Parse parameters error");
                    return false;
                }

                LOG.Debug("Bio.Core.Matcher.Namku.Initialize - IsInitialized=" + IsInitialized);

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Namku.Initializee Error", ex);
            }
            return IsInitialized;

        }

 
        //Extrae nivel d eseguridad de matching
        private static bool ExtractParams(string parameters, out double securitylevel, out string url)
        {
            securitylevel = 0.7;
            url = "http://test.namku.cl:5000/api/v1.2/tesnor/compare_faces";

            try
            {
                string[] arr = parameters.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                LOG.Debug("Bio.Core.Matcher.Namku.ExtractParams arr.length=" + arr.Length.ToString());
                if (arr.Length == 0) return false;

                for (int i = 0; i < arr.Length; i++)
                {
                    LOG.Debug("Bio.Core.Matcher.Namku.ExtractParams procesando arr[" + i.ToString() + "]=" + arr[i]);
                    string[] item = arr[i].Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    LOG.Debug("Bio.Core.Matcher.Namku.ExtractParams item.length=" + item.Length.ToString());
                    if (item[0].Equals("SecurityLevel"))
                    {
                        if (item.Length == 2 && !string.IsNullOrEmpty(item[1])) { 
                            securitylevel = Convert.ToDouble(item[1]); // SetSecurityLevel(item[1]);
                        }
                        LOG.Debug("Bio.Core.Matcher.Namku.ExtractParams securitylevel=" + securitylevel);
                    }

                    if (item[0].Equals("URLService"))
                    {
                        if (item.Length == 2 && !string.IsNullOrEmpty(item[1]))
                        {
                            url = item[1]; // SetSecurityLevel(item[1]);
                        }
                        LOG.Debug("Bio.Core.Matcher.Namku.ExtractParams URLService=" + url);
                    }
                }

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Namku.ExtractParams Error", ex);
                return false;
            }
            return true;
        }

        //private static MatchingSecurityLevel SetSecurityLevel(string sSecurityLevel)
        //{
        //    if (sSecurityLevel.Equals("Medium")) return MatchingSecurityLevel.MediumSecurityLevel;
        //    if (sSecurityLevel.Equals("High")) return MatchingSecurityLevel.HighSecurityLevel;
        //    if (sSecurityLevel.Equals("MediumHigh")) return MatchingSecurityLevel.MediumHighSecurityLevel;
        //    if (sSecurityLevel.Equals("VeryHigh")) return MatchingSecurityLevel.VeryHighSecurityLevel;
        //    if (sSecurityLevel.Equals("Highest")) return MatchingSecurityLevel.HighestSecurityLevel;
        //    return MatchingSecurityLevel.MediumHighSecurityLevel;
        //}
    }

}
