﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Facetec.Api
{
    public class SessionResponse
    {
        public AdditionalSessionData additionalSessionData { get; set; }
        public CallData callData { get; set; }
        public bool error { get; set; }
        public ServerInfo serverInfo { get; set; }
        public string sessionToken { get; set; }
        public bool success { get; set; }
    }
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class AdditionalSessionData
    {
        public bool isAdditionalDataPartiallyIncomplete { get; set; }
    }

    public class CallData
    {
        public string tid { get; set; }
        public string path { get; set; }
        public string date { get; set; }
        public int epochSecond { get; set; }
        public string requestMethod { get; set; }
    }

    

    public class ServerInfo
    {
        public string version { get; set; }
        public string mode { get; set; }
        public string notice { get; set; }
    }
}
