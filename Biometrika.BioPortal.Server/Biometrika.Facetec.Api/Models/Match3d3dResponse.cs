﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Facetec.Api
{
    public class Match3d3dResponse
    {
        public AdditionalSessionData additionalSessionData { get; set; }
        public int ageEstimateGroupEnumInt { get; set; }
        public CallData callData { get; set; }
        public object documentData { get; set; }
        public bool error { get; set; }
        public string externalDatabaseRefID { get; set; }
        public FaceScanSecurityChecks faceScanSecurityChecks { get; set; }
        public int faceTecRetryScreen { get; set; }
        public int matchLevel { get; set; }
        public object ocrResults { get; set; }
        public int retryScreenEnumInt { get; set; }
        public string scanResultBlob { get; set; }
        public ServerInfo serverInfo { get; set; }
        public bool success { get; set; }
        public bool wasProcessed { get; set; }
    }
   

    public class FaceScanSecurityChecks
    {
        public bool auditTrailVerificationCheckSucceeded { get; set; }
        public bool faceScanLivenessCheckSucceeded { get; set; }
        public bool replayCheckSucceeded { get; set; }
        public bool sessionTokenCheckSucceeded { get; set; }
    }


}
