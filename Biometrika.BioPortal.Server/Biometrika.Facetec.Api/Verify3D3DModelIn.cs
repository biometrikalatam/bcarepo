﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Facetec.Api
{

    public class Verify3D3DIn
    {
        public Verify3D3DIn() { }

        public Verify3D3DIn(string _enrollmentIdentifier, string _facemapsource, string _facemaptarget, 
                                 string _auditTrailImage, string _lowQualityAuditTrailImage, string _session)
        {
            sessionId = _session;
            source = new Source(_enrollmentIdentifier, _facemapsource);
            target = new Target(_facemaptarget);
            auditTrailImage = _auditTrailImage;
            lowQualityAuditTrailImage = _lowQualityAuditTrailImage;
        }

        public Source source { get; set; }
        public Target target { get; set; }
        public string auditTrailImage { get; set; }
        public string lowQualityAuditTrailImage { get; set; }
        public string sessionId { get; set; }
    }

    public class Source
    {
        public Source() { }

        public Source(string _enrollmentIdentifier, string _facemapsource)
        {
            enrollmentIdentifier = _enrollmentIdentifier;
            faceMap = _facemapsource;
        }

        public string enrollmentIdentifier { get; set; }
        public string faceMap { get; set; }

    }

    public class Target
    {
        public Target() { }

        public Target(string _facemaptarget)
        {
            faceMap = _facemaptarget;
        }
        public string faceMap { get; set; }

    }

    
}
