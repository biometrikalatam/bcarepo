﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Facetec.Api
{

    public class Verify3D2DModelIn
    {
        public Verify3D2DModelIn() { }

        public Verify3D2DModelIn(string _enrollmentIdentifier, string _facemapsource, string _imagetarget, 
                                 string _auditTrailImage, string _lowQualityAuditTrailImage)
        {
            source = new Source(_enrollmentIdentifier, _facemapsource);
            target = new Target2D(_imagetarget);
            auditTrailImage = _auditTrailImage;
            lowQualityAuditTrailImage = _lowQualityAuditTrailImage;
        }

        public Source source { get; set; }
        public Target2D target { get; set; }
        public string auditTrailImage { get; set; }
        public string lowQualityAuditTrailImage { get; set; }

    }

    //internal class Source
    //{
    //    internal Source() { }

    //    internal Source(string _enrollmentIdentifier, string _facemapsource)
    //    {
    //        enrollmentIdentifier = _enrollmentIdentifier;
    //        faceMap = _facemapsource;
    //    }

    //    internal string enrollmentIdentifier { get; set; }
    //    internal string faceMap { get; set; }

    //}

    public class Target2D
    {
        public Target2D() { }

        public Target2D(string _imagetarget)
        {
            image = _imagetarget;
        }
        public string image { get; set; }

    }

    
}
