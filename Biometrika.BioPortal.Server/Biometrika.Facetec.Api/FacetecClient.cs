﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using log4net;
using RestSharp;
using System.Net;

namespace Biometrika.Facetec.Api
{
    public class FacetecClient
    {
        private string _baseURL;
        private string _licenseKey;
        private ZoomResponse<ZoomSession> _ZOOMRESPONSE;

        private HttpClient client;
        private RestClient _CLIENT;
        //private readonly ILogger<ZoomService> _logger;
        private static readonly ILog LOG = LogManager.GetLogger(typeof(FacetecClient));

        public FacetecClient(string baseURL, string licenseKey, int timeout)
        {
            _baseURL = baseURL;
            _licenseKey = licenseKey;

            client = new HttpClient
            {
                BaseAddress = new Uri(_baseURL),
                // BaseAddress = new Uri("http://localhost:10000/"),
                Timeout = TimeSpan.FromSeconds(timeout)
            };

            _CLIENT = new RestClient(baseURL);
            _CLIENT.Timeout = timeout;

            // client.DefaultRequestHeaders.Add("X-Device-License-Key", _deviceLicenseKeyIdentifier);
            // client.DefaultRequestHeaders.Add("X-User-Agent", "ZoomSDK.createZoomAPIUserAgentString()");
        }

        #region Sync

        public ZoomResponse<ZoomSession> sessionTokenSync(string deviceLicenseKey)
        {
            LOG.Debug("FacetecClient.sessionTokenSync IN...");
            var request = new RestRequest(Method.GET);
            _CLIENT.BaseUrl = new Uri(_baseURL + "session-token");
            LOG.Debug("FacetecClient.sessionTokenSync - _CLIENT.BaseUrl = " + _CLIENT.BaseUrl);
            request.AddHeader("X-Device-License-Key", deviceLicenseKey);
            request.AddHeader("Content-Type", "application/json");

            //Si es sitio con SSL => HAbilito
            if (_baseURL.StartsWith("https")) {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            }
            
            request.AddHeader("Accept", "application/json");

            IRestResponse response = _CLIENT.Execute(request);
            LOG.Debug("FacetecClient.sessionTokenSync response = " + ((response == null) ? "NULL" : 
                                                                      (response.StatusCode!=null?response.StatusCode.ToString(): "response.StatusCode Null")));
            LOG.Debug("FacetecClient.sessionTokenSync response.contenct = " + ((response == null || response.Content == null) ? 
                                                                                "NULL" : response.Content.ToString()));
            _ZOOMRESPONSE = JsonConvert.DeserializeObject<ZoomResponse<ZoomSession>>(response.Content);
            LOG.Debug("FacetecClient.sessionTokenSync - _ZOOMRESPONSE != null => " + (_ZOOMRESPONSE!=null).ToString());
            LOG.Debug("FacetecClient.sessionTokenSync OUT!");
            return _ZOOMRESPONSE;

        }

        public ZoomResponse<ZoomLiveness> livenessSync(string deviceLicenseKey, string userAgent, string sessionId, string faceMap, 
                                                       string auditTrailImage, string lowQualityAuditTrailImage)
        {
            LOG.Debug("FacetecClient.livenessSync IN...");
            var request = new RestRequest(Method.POST);
            _CLIENT.BaseUrl = new Uri(_baseURL + "liveness");
            LOG.Debug("FacetecClient.livenessSync - _CLIENT.BaseUrl = " + _CLIENT.BaseUrl);
            request.AddHeader("X-Device-License-Key", deviceLicenseKey);
            request.AddHeader("X-User-Agent", userAgent);
            string jsonParams = JsonConvert.SerializeObject(new
            {
                sessionId, //INSERT_SESSION_ID_FROM_zoomSessionResult,
                faceMap, //INSERT_FACEMAP_FROM_zoomSessionResult,
                auditTrailImage, //INSERT_AUDIT_TRAIL_IMAGE_FROM_zoomSessionResult
                lowQualityAuditTrailImage //INSERT_LOW_QUALITY_AUDIT_TRAIL_IMAGE_FROM_zoomSessionResult
            });
            request.AddParameter("application/json", jsonParams, ParameterType.RequestBody);
  
            IRestResponse response = _CLIENT.Execute(request);
            LOG.Debug("FacetecClient.livenessSync response = " + ((response == null) ? "NULL" :
                                                                      (response.StatusCode != null ? response.StatusCode.ToString() : "response.StatusCode Null")));
            LOG.Debug("FacetecClient.livenessSync response.contenct = " + ((response == null || response.Content == null) ? "NULL" : response.Content.ToString()));
            LOG.Debug("FacetecClient.livenessSync OUT!");
            return JsonConvert.DeserializeObject<ZoomResponse<ZoomLiveness>>(response.Content);
        }

        public ZoomResponse<ZoomIDCheck> idCheckSync(string deviceLicenseKey, string userAgent, string sessionId, string faceMap, 
            string enrollmentIdentifier, string idScan, string idScanFrontImage, string idScanBackImage)
        {
            LOG.Debug("FacetecClient.idCheckSync IN...");
            var request = new RestRequest(Method.POST);
            _CLIENT.BaseUrl = new Uri(_baseURL + "id-check");
            LOG.Debug("FacetecClient.idCheckSync - _CLIENT.BaseUrl = " + _CLIENT.BaseUrl);
            request.AddHeader("X-Device-License-Key", deviceLicenseKey);
            request.AddHeader("X-User-Agent", userAgent);
            string jsonParams;
            if (string.IsNullOrEmpty(enrollmentIdentifier))
            {
                LOG.Debug("FacetecClient.idCheckSync - enrollmentIdentifier nulo...");
                jsonParams = JsonConvert.SerializeObject(new
                {
                    sessionId, //INSERT_SESSION_ID_FROM_zoomSessionResult,
                    faceMap, //INSERT_FACEMAP_FROM_zoomSessionResult
                    idScan, //INSERT_ID_SCAN_HERE,
                    idScanFrontImage, //INSERT_ID_SCAN_FRONT_IMAGE_FROM_zoomIDScanResult,
                    idScanBackImage, //INSERT_ID_SCAN_BACK_IMAGE_FROM_zoomIDScanResult
                });
            } else
            {
                LOG.Debug("FacetecClient.idCheckSync - enrollmentIdentifier NO nulo = " + enrollmentIdentifier);
                jsonParams = JsonConvert.SerializeObject(new
                {
                    sessionId, //INSERT_SESSION_ID_FROM_zoomSessionResult,
                    faceMap, //INSERT_FACEMAP_FROM_zoomSessionResult
                    enrollmentIdentifier, // optional
                    idScan, //INSERT_ID_SCAN_HERE,
                    idScanFrontImage, //INSERT_ID_SCAN_FRONT_IMAGE_FROM_zoomIDScanResult,
                    idScanBackImage, //INSERT_ID_SCAN_BACK_IMAGE_FROM_zoomIDScanResult
                });
            }
            LOG.Debug("FacetecClient.idCheckSync - sessionId == null => " + (string.IsNullOrEmpty(sessionId)).ToString());
            LOG.Debug("FacetecClient.idCheckSync - faceMap == null => " + (string.IsNullOrEmpty(faceMap)).ToString());
            LOG.Debug("FacetecClient.idCheckSync - enrollmentIdentifier == null => " + (string.IsNullOrEmpty(enrollmentIdentifier)).ToString());
            LOG.Debug("FacetecClient.idCheckSync - idScan == null => " + (string.IsNullOrEmpty(idScan)).ToString());
            LOG.Debug("FacetecClient.idCheckSync - idScanFrontImage == null => " + (string.IsNullOrEmpty(idScanFrontImage)).ToString());
            LOG.Debug("FacetecClient.idCheckSync - idScanBackImage == null => " + (string.IsNullOrEmpty(idScanBackImage)).ToString());
            request.AddParameter("application/json", jsonParams, ParameterType.RequestBody);

            IRestResponse response = _CLIENT.Execute(request);
            LOG.Debug("FacetecClient.idCheckSync response = " + ((response == null) ? "NULL" :
                                                                      (response.StatusCode != null ? response.StatusCode.ToString() : "response.StatusCode Null")));
            LOG.Debug("FacetecClient.idCheckSync response.contenct = " + ((response == null || response.Content == null) ? "NULL" : response.Content.ToString()));
            LOG.Debug("FacetecClient.idCheckSync OUT!");
            return JsonConvert.DeserializeObject<ZoomResponse<ZoomIDCheck>>(response.Content);
        }

        public ZoomResponse<ZoomVerify3D3D_2D> verify3D3DSync(string deviceLicenseKey, string userAgent, string sessionId,
                    string faceMapSource, string faceMapTarget, string enrollmentIdentifier, string auditTrailImage, string lowQualityAuditTrailImage)
        {
            LOG.Debug("FacetecClient.verify3D3DSync IN...");
            var request = new RestRequest(Method.POST);
            _CLIENT.BaseUrl = new Uri(_baseURL + "match-3d-3d");
            LOG.Debug("FacetecClient.verify3D3DSync - _CLIENT.BaseUrl = " + _CLIENT.BaseUrl);
            request.AddHeader("X-Device-License-Key", deviceLicenseKey);
            request.AddHeader("X-User-Agent", userAgent);
            Verify3D3DIn paramin = 
                new Verify3D3DIn(enrollmentIdentifier, faceMapSource, faceMapTarget, auditTrailImage, 
                                      lowQualityAuditTrailImage, sessionId);
            LOG.Debug("FacetecClient.verify3D3DSync - sessionId == null => " + (string.IsNullOrEmpty(sessionId)).ToString());
            LOG.Debug("FacetecClient.verify3D3DSync - faceMapSource == null => " + (string.IsNullOrEmpty(faceMapSource)).ToString());
            LOG.Debug("FacetecClient.verify3D3DSync - faceMapTarget == null => " + (string.IsNullOrEmpty(faceMapTarget)).ToString());
            LOG.Debug("FacetecClient.verify3D3DSync - enrollmentIdentifier == null => " + (string.IsNullOrEmpty(enrollmentIdentifier)).ToString());
            LOG.Debug("FacetecClient.verify3D3DSync - auditTrailImage == null => " + (string.IsNullOrEmpty(auditTrailImage)).ToString());
            LOG.Debug("FacetecClient.verify3D3DSync - lowQualityAuditTrailImage == null => " + (string.IsNullOrEmpty(lowQualityAuditTrailImage)).ToString());
            string jsonParams = JsonConvert.SerializeObject(paramin);
            request.AddParameter("application/json", jsonParams, ParameterType.RequestBody);

            IRestResponse response = _CLIENT.Execute(request);
            LOG.Debug("FacetecClient.verify3D3DSync response = " + ((response == null) ? "NULL" :
                                                                        (response.StatusCode != null ? response.StatusCode.ToString() : 
                                                                                                       "response.StatusCode Null")));
            LOG.Debug("FacetecClient.verify3D3DSync response.Content = " + ((response == null || response.Content == null) ? "NULL" : response.Content.ToString()));
            LOG.Debug("FacetecClient.verify3D3DSync OUT!");
            return JsonConvert.DeserializeObject<ZoomResponse<ZoomVerify3D3D_2D>>(response.Content);
        }

        public ZoomResponse<ZoomVerify3D3D_2D> verify3D2DSync(string deviceLicenseKey, string userAgent, string sessionId,
            string faceMapSource, string imageTarget, string enrollmentIdentifier, string auditTrailImage, string lowQualityAuditTrailImage)
        {
            LOG.Debug("FacetecClient.verify3D2DSync IN...");
            var request = new RestRequest(Method.POST);
            _CLIENT.BaseUrl = new Uri(_baseURL + "match-3d-2d");
            LOG.Debug("FacetecClient.verify3D2DSync - _CLIENT.BaseUrl = " + _CLIENT.BaseUrl);
            request.AddHeader("X-Device-License-Key", deviceLicenseKey);
            request.AddHeader("X-User-Agent", userAgent);
            Verify3D2DModelIn paramin =
                new Verify3D2DModelIn(enrollmentIdentifier, faceMapSource, imageTarget, auditTrailImage, lowQualityAuditTrailImage);
            LOG.Debug("FacetecClient.verify3D2DSync - sessionId == null => " + (string.IsNullOrEmpty(sessionId)).ToString());
            LOG.Debug("FacetecClient.verify3D2DSync - faceMapSource == null => " + (string.IsNullOrEmpty(faceMapSource)).ToString());
            LOG.Debug("FacetecClient.verify3D2DSync - enrollmentIdentifier == null => " + (string.IsNullOrEmpty(enrollmentIdentifier)).ToString());
            LOG.Debug("FacetecClient.verify3D2DSync - auditTrailImage == null => " + (string.IsNullOrEmpty(auditTrailImage)).ToString());
            LOG.Debug("FacetecClient.verify3D2DSync - lowQualityAuditTrailImage == null => " + (string.IsNullOrEmpty(lowQualityAuditTrailImage)).ToString());
            string jsonParams = JsonConvert.SerializeObject(paramin);
            request.AddParameter("application/json", jsonParams, ParameterType.RequestBody);

            IRestResponse response = _CLIENT.Execute(request);
            LOG.Debug("FacetecClient.verify3D2DSync response = " + ((response == null) ? "NULL" :
                                                                        (response.StatusCode != null ? response.StatusCode.ToString() :
                                                                                                       "response.StatusCode Null")));
            LOG.Debug("FacetecClient.verify3D2DSync response.contenct = " + ((response == null || response.Content == null) ? "NULL" : response.Content.ToString()));
            LOG.Debug("FacetecClient.verify3D2DSync OUT!");
            return JsonConvert.DeserializeObject<ZoomResponse<ZoomVerify3D3D_2D>>(response.Content);
        }

        public ZoomResponse<ZoomEnrollment> enrollmentSync(string deviceLicenseKey, string userAgent, string sessionId,
                        string faceMap, string enrollmentIdentifier, string auditTrailImage, string lowQualityAuditTrailImage)
        {
            LOG.Debug("FacetecClient.enrollmentSync IN...");
            var request = new RestRequest(Method.POST);
            _CLIENT.BaseUrl = new Uri(_baseURL + "enrollment");
            LOG.Debug("FacetecClient.enrollmentSync - _CLIENT.BaseUrl = " + _CLIENT.BaseUrl);
            request.AddHeader("X-Device-License-Key", deviceLicenseKey);
            request.AddHeader("X-User-Agent", userAgent);
            string jsonParams = JsonConvert.SerializeObject(new
            {
                sessionId, //INSERT_SESSION_ID_FROM_zoomSessionResult,
                faceMap, //INSERT_FACEMAP_FROM_zoomSessionResult,
                enrollmentIdentifier, //INSERT_ENROLLMENT_IDENTIFIER_HERE,
                auditTrailImage, //INSERT_AUDIT_TRAIL_IMAGE_FROM_zoomSessionResult
                lowQualityAuditTrailImage //INSERT_LOW_QUALITY_AUDIT_TRAIL_IMAGE_FROM_zoomSessionResult
            });
            LOG.Debug("FacetecClient.enrollmentSync - sessionId == null => " + (string.IsNullOrEmpty(sessionId)).ToString());
            LOG.Debug("FacetecClient.enrollmentSync - faceMap == null => " + (string.IsNullOrEmpty(faceMap)).ToString());
            LOG.Debug("FacetecClient.enrollmentSync - enrollmentIdentifier == null => " + (string.IsNullOrEmpty(enrollmentIdentifier)).ToString());
            LOG.Debug("FacetecClient.enrollmentSync - auditTrailImage == null => " + (string.IsNullOrEmpty(auditTrailImage)).ToString());
            LOG.Debug("FacetecClient.enrollmentSync - lowQualityAuditTrailImage == null => " + (string.IsNullOrEmpty(lowQualityAuditTrailImage)).ToString());
            request.AddParameter("application/json", jsonParams, ParameterType.RequestBody);

            IRestResponse response = _CLIENT.Execute(request);
            LOG.Debug("FacetecClient.enrollmentSync response = " + ((response == null) ? "NULL" :
                                                                        (response.StatusCode != null ? response.StatusCode.ToString() :
                                                                                                       "response.StatusCode Null")));
            LOG.Debug("FacetecClient.enrollmentSync response.contenct = " + ((response == null || response.Content == null) ? "NULL" : response.Content.ToString()));
            LOG.Debug("FacetecClient.enrollmentSync OUT!");
            return JsonConvert.DeserializeObject<ZoomResponse<ZoomEnrollment>>(response.Content);
        }

        public ZoomResponse<ZoomEnrollment> enrollmentDeleteSync(string deviceLicenseKey, string userAgent, string sessionId,
                string faceMap, string enrollmentIdentifier, string auditTrailImage, string lowQualityAuditTrailImage)
        {
            LOG.Debug("FacetecClient.enrollmentDeleteSync IN...");
            var request = new RestRequest(Method.DELETE);
            _CLIENT.BaseUrl = new Uri(_baseURL + "enrollment/" + enrollmentIdentifier);
            LOG.Debug("FacetecClient.enrollmentDeleteSync - _CLIENT.BaseUrl = " + _CLIENT.BaseUrl);
            request.AddHeader("X-Device-License-Key", deviceLicenseKey);
            request.AddHeader("X-User-Agent", userAgent);

            IRestResponse response = _CLIENT.Execute(request);
            LOG.Debug("FacetecClient.enrollmentDeleteSync response = " + ((response == null) ? "NULL" :
                                                                        (response.StatusCode != null ? response.StatusCode.ToString() :
                                                                                                       "response.StatusCode Null")));
            LOG.Debug("FacetecClient.enrollmentDeleteSync response.contenct = " + ((response == null || response.Content == null) ? "NULL" : response.Content.ToString()));
            LOG.Debug("FacetecClient.enrollmentDeleteSync OUT!");
            return JsonConvert.DeserializeObject<ZoomResponse<ZoomEnrollment>>(response.Content);
        }

        public ZoomResponse<ZoomEnrollmentData> enrollmentReadSync(string deviceLicenseKey, string userAgent, string sessionId,
                                                               string enrollmentIdentifier)
        {
            LOG.Debug("FacetecClient.enrollmentReadSync IN...");
            var request = new RestRequest(Method.GET);
            _CLIENT.BaseUrl = new Uri(_baseURL + "enrollment/" + enrollmentIdentifier);
            LOG.Debug("FacetecClient.enrollmentReadSync - _CLIENT.BaseUrl = " + _CLIENT.BaseUrl);
            request.AddHeader("X-Device-License-Key", deviceLicenseKey);
            request.AddHeader("X-User-Agent", userAgent);

            IRestResponse response = _CLIENT.Execute(request);
            LOG.Debug("FacetecClient.enrollmentReadSync response = " + ((response == null) ? "NULL" :
                                                                        (response.StatusCode != null ? response.StatusCode.ToString() :
                                                                                                       "response.StatusCode Null")));
            LOG.Debug("FacetecClient.enrollmentReadSync response.contenct = " + ((response == null || response.Content == null) ? "NULL" : response.Content.ToString()));
            LOG.Debug("FacetecClient.enrollmentReadSync IN...");
            return JsonConvert.DeserializeObject<ZoomResponse<ZoomEnrollmentData>>(response.Content);
        }

        #endregion Sync

        #region Asyc

        public async Task<ZoomResponse<ZoomSession>> sessionToken(string deviceLicenseKey)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, "session-token");

            request.Headers.Add("X-Device-License-Key", deviceLicenseKey);

            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var jsonString = await response.Content.ReadAsStringAsync();
            LOG.Debug("sessionToken Response {} = " + jsonString);
            Console.WriteLine(jsonString);
            return JsonConvert.DeserializeObject<ZoomResponse<ZoomSession>>(jsonString);
        }

        public async Task<ZoomResponse<ZoomLiveness>> liveness(string deviceLicenseKey, string userAgent, string sessionId, string faceMap, string auditTrailImage, string lowQualityAuditTrailImage)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "liveness");

            request.Headers.Add("X-Device-License-Key", deviceLicenseKey);
            request.Headers.Add("X-User-Agent", userAgent);

            request.Content = new StringContent(JsonConvert.SerializeObject(new
            {
                sessionId, //INSERT_SESSION_ID_FROM_zoomSessionResult,
                faceMap, //INSERT_FACEMAP_FROM_zoomSessionResult,
                auditTrailImage, //INSERT_AUDIT_TRAIL_IMAGE_FROM_zoomSessionResult
                lowQualityAuditTrailImage //INSERT_LOW_QUALITY_AUDIT_TRAIL_IMAGE_FROM_zoomSessionResult
            }),
            Encoding.UTF8, "application/json");

            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var jsonString = await response.Content.ReadAsStringAsync();
            LOG.Debug("liveness Response {} = " +  jsonString);
            Console.WriteLine(jsonString);
            return JsonConvert.DeserializeObject<ZoomResponse<ZoomLiveness>>(jsonString);
        }

        public async Task<ZoomResponse<ZoomEnrollment>> enrollment(string deviceLicenseKey, string userAgent, string sessionId, string faceMap, string enrollmentIdentifier, string auditTrailImage, string lowQualityAuditTrailImage)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "enrollment");

            request.Headers.Add("X-Device-License-Key", deviceLicenseKey);
            request.Headers.Add("X-User-Agent", userAgent);

            request.Content = new StringContent(JsonConvert.SerializeObject(new
            {
                sessionId, //INSERT_SESSION_ID_FROM_zoomSessionResult,
                faceMap, //INSERT_FACEMAP_FROM_zoomSessionResult,
                enrollmentIdentifier, //INSERT_ENROLLMENT_IDENTIFIER_HERE,
                auditTrailImage, //INSERT_AUDIT_TRAIL_IMAGE_FROM_zoomSessionResult
                lowQualityAuditTrailImage //INSERT_LOW_QUALITY_AUDIT_TRAIL_IMAGE_FROM_zoomSessionResult
            }),
            Encoding.UTF8, "application/json");

            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var jsonString = await response.Content.ReadAsStringAsync();
            LOG.Debug("enrollment Response {} = " + jsonString);
            Console.WriteLine(jsonString);
            return JsonConvert.DeserializeObject<ZoomResponse<ZoomEnrollment>>(jsonString);
        }

        public async Task<ZoomResponse<ZoomIDCheck>> idCheck(string deviceLicenseKey, string userAgent, string sessionId, string faceMap, string enrollmentIdentifier, string idScan, string idScanFrontImage, string idScanBackImage)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "id-check");

            request.Headers.Add("X-Device-License-Key", deviceLicenseKey);
            request.Headers.Add("X-User-Agent", userAgent);

            request.Content = new StringContent(JsonConvert.SerializeObject(new
            {
                sessionId, //INSERT_SESSION_ID_FROM_zoomSessionResult,
                faceMap, //INSERT_FACEMAP_FROM_zoomSessionResult
                enrollmentIdentifier, // optional
                idScan, //INSERT_ID_SCAN_HERE,
                idScanFrontImage, //INSERT_ID_SCAN_FRONT_IMAGE_FROM_zoomIDScanResult,
                idScanBackImage, //INSERT_ID_SCAN_BACK_IMAGE_FROM_zoomIDScanResult
            }),
            Encoding.UTF8, "application/json");

            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var jsonString = await response.Content.ReadAsStringAsync();
            LOG.Debug("idCheck Response {} = " + jsonString);
            Console.WriteLine(jsonString);
            return JsonConvert.DeserializeObject<ZoomResponse<ZoomIDCheck>>(jsonString);
        }

        #endregion Asyc
    }

    public class ZoomResponse<T>
    {
        public IDictionary<string, object> Meta { get; set; }
        public T Data { get; set; }


        public override string ToString()
        {
            return base.ToString() +
                ", Meta=" + (Meta != null ? "{" + string.Join(",", Meta) + "}" : null) +
                ", Data=" + Data;
        }
    }

    public class ZoomSession
    {
        public string SessionToken { get; set; }
    }

    public class ZoomLiveness
    {
        public string ErrorMessageFromZoomServer { get; set; }
        public int ErrorStatusFromZoomServer { get; set; }
        public bool Glasses { get; set; }
        public bool IsLowQuality { get; set; }
        public bool IsReplayFaceMap { get; set; }
        public int LivenessStatus { get; set; }
    }

    public class ZoomEnrollment
    {
        public string AuditTrailVerificationMessage { get; set; }
        public int AuditTrailVerificationStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public string EnrollmentIdentifier { get; set; }
        public string ErrorMessageFromZoomServer { get; set; }
        public int ErrorStatusFromZoomServer { get; set; }
        public int FaceMapType { get; set; }
        public bool Glasses { get; set; }
        public bool IsEnrolled { get; set; }
        public bool IsLowQuality { get; set; }
        public bool IsReplayFaceMap { get; set; }
        public int LivenessStatus { get; set; }
    }

    public class ZoomIDCheck
    {
        public int DigitalSpoofStatus { get; set; }
        public string ErrorMessageFromZoomServer { get; set; }
        public int ErrorStatusFromZoomServer { get; set; }
        public int FaceMapAgeEstimateGroup { get; set; }
        public int FullIDStatus { get; set; }
        public bool Glasses { get; set; }
        public int IDScanAgeEstimateGroup { get; set; }
        public bool IsLowQuality { get; set; }
        public bool IsReplayFaceMap { get; set; }
        public int LivenessStatus { get; set; }
        public int MatchLevel { get; set; }
    }

   
    /*  Respuesta en Verify 3D-3D*/
    public class ZoomVerify3D3D_2D
    {
        public SourceFaceMap sourceFaceMap { get; set; }
        public int matchLevel { get; set; }
        public string enrollmentIdentifier { get; set; }
        public TargetFaceMap targetFaceMap { get; set; }
        public string message { get; set; }
        public string tid { get; set; }
        public string continuousLearningFaceMap { get; set; }
    }
    public class SourceFaceMap
    {
        public int livenessStatus { get; set; }

    }

    public class TargetFaceMap
    {
        public bool glasses { get; set; }
        public int faceMapType { get; set; }
        public int ageResult { get; set; }
        public int livenessStatus { get; set; }
        public bool auditTrailVerificationCheck { get; set; }
        public bool auditTrailMatchCheck { get; set; }
        public bool isReplayFaceMap { get; set; }

    }
    /*  Respuesta en Verify 3D-3D*/

    public class ZoomEnrollmentData
    {
        public string enrollmentIdentifier { get; set; }
        public DateTime createDate { get; set; }
        public string faceMap { get; set; }
        public string faceMapType { get; set; }
        public int auditTrailImage { get; set; }
    }

}
