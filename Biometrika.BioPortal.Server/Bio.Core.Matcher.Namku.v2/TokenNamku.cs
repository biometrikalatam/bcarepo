﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bio.Core.Matcher.Namku
{
    public class TokenNamku
    {
        
            [JsonProperty("api_version")]
            public string ApiVersion { get; set; }

            [JsonProperty("data")]
            public Data Data { get; set; }
        

    }
}
