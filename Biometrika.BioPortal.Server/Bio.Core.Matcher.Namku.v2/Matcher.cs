﻿using System;
using System.Collections.Generic;
using System.Text;
using Bio.Core.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Core.Serialize;
using log4net;
using System.Net.Http;
using System.Net.Http.Headers;
using MultipartFormDataSample.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Globalization;
using System.IO;
using Biometrika.NamkuFaceAPI.Face;

namespace Bio.Core.Matcher.Namku
{
    public class Matcher : IMatcher
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Matcher));

#region Implementation of IMatcher

        private int _authenticationFactor;
        private int _minutiaeType;
        private double _threshold;
        private int _matchingType;

        private string _parameters;

        //FPhi.Matcher.Matcher matcher;
        //FPhi.Matcher.MatcherConfigurationManager matcherConfig;

        /// <summary>
        /// Parametros adicionales.
        /// </summary>
        public string Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }


        /// <summary>
        /// Tecnologia a utilizar para matching
        /// </summary>
        public int AuthenticationFactor
        {
            get { return _authenticationFactor; }
            set { _authenticationFactor = value; }
        }

        /// <summary>
        /// Tipo de minucia dentro de la tecnologia utilziada
        /// </summary>
        public int MinutiaeType
        {
            get { return _minutiaeType; }
            set { _minutiaeType = value; }
        }

        /// <summary>
        /// Umbral de verificación considerada aceptable
        /// </summary>
        public double Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Dos posibles valores:
        ///     0 - FIRST   -> El primer matching positivo
        ///     1 - BEST    -> El mejor matching positivo
        /// </summary>
        public int MatchingType
        {
            get { return _matchingType; }
            set { _matchingType = (value == 1 || value == 2) ? value : 1; }
        }

        //int IMatcher.AuthenticationFactor { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //int IMatcher.MinutiaeType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //double IMatcher.Threshold { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //int IMatcher.MatchingType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //string IMatcher.Parameters { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        /// <summary>
        /// Acción de verificación. Ingresa información para verificar, template base
        /// y nuevo, parámetros de chequeo, y se realiza la verificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Verify(string xmlinput, out string xmloutput)
        {
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;
            NamkuResponseConpareFace oResponse = null;
            TokenNamku tokennamku = null;
            TokenNamku2 token = null;

            try
            {
                if (!NamkuUtils.IsInitialized)
                {
                    if (!NamkuUtils.Initialize(_parameters))
                    {
                        msg = "Bio.Core.Matcher.Namku.Matcher Error Chequeando Parameters";
                        LOG.Fatal(msg);
                        paramout.SetValue("message", msg);
                        xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                        return Errors.IERR_BAD_PARAMETER;
                    }
                }
                //else
                //{
                //    //if (!NamkuUtils.CheckValidityToken())
                //    //{
                //    //msg = "Bio.Core.Matcher.Namku.Matcher Error Chequeando Token";
                //    //LOG.Fatal(msg);
                //    //paramout.SetValue("message", msg);
                //    //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                //    //return Errors.IERR_BAD_PARAMETER;
                //    //}
                //}

                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                // Debo tratar diferente si es un JPG o un Embedding ya generado, y considerarlo cuando se hace Compare
                int CurrentTemplateMT = 44;
                try
                {
                    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                    try
                    {
                           CurrentTemplateMT = (int)paramin.GetValue("currentTemplateMT");
                    }
                    catch (Exception ex)
                    {
                        msg = "Bio.Core.Matcher.Namku.Matcher Error obteniendo MT del CurrentTemplate... [" + ex.Message + "]";
                        LOG.Warn(msg);
                        CurrentTemplateMT = 44;
                    }
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.Namku.Matcher Error importando CurrentTemplate " +
                         "[ex=" + ex.Message + "]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                

                //2.- Preapro prametros para enviar
                //ImageSet imageSetNew = new ImageSet();
                //MultipartFormDataSample.Client.Image parameter1 = new MultipartFormDataSample.Client.Image();
                //parameter1.FileName = @"image1.jpg";
                ////parameter1.ImageData = Convert.FromBase64String(image1);
                //parameter1.MimeType = "image/jpeg";

                //MultipartFormDataSample.Client.Image parameter2 = new MultipartFormDataSample.Client.Image();
                //parameter2.FileName = @"image2";
                ////parameter2.ImageData = Convert.FromBase64String(image2);
                //parameter2.MimeType = "image/jpeg";

                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                //for (int i = 0; i < listTemplatesBase.Count; i++)
                //{

                    CompareResponse _CompareResponse;

                    /* Debo armar el Compare segun los parametros enviados:
                          1.- Si son dos embeding => Debo armar un unico JSON con Source y Target y enviar null en ambos JPG    
                          2.- Si son dos JPG => Envio ambos JPG como MemoryStream y JSON nulo
                          3.- Si es un JPG + Embedding => Envio el JPG, segundo JPG en nulo, y JSON solo ocn el target 
                    */ 
                    if (CurrentTemplateMT == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_FACIAL_NAMKU &&
                        listTemplatesBase[0].MinutiaeType == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_FACIAL_NAMKU)  
                    {
                        //Si embeddings, hay que armar el CompareRequest con Source y Target. Si sin varios Target es una lista de embeddings
                        CompareRequest req = new CompareRequest();

                        //Cargo Source
                        req.Source = new CompareRequestFaceObject();
                        req.Source.Id = 1;
                        CompareRequest reqAux1 = JsonConvert.DeserializeObject<CompareRequest>(Encoding.ASCII.GetString(byCurrentTemplate));
                        req.Source.Embeddings = reqAux1.Target[0].Embeddings;

                        //Cargo Target
                        req.Target = new List<CompareRequestFaceObject>();
                        for (int j = 0; j < listTemplatesBase.Count; j++)
                        {
                            CompareRequest reqAux = JsonConvert.DeserializeObject<CompareRequest>(Encoding.ASCII.GetString(listTemplatesBase[0].Data));
                            CompareRequestFaceObject crObj = new CompareRequestFaceObject();
                            crObj.Id = j + 2;
                            crObj.Embeddings = reqAux.Target[0].Embeddings;
                            req.Target.Add(crObj);
                        }
                        //Llamo a Compare solo con JSON con Source y Target tipo Embedding
                        _CompareResponse = NamkuUtils.NAMKU_API.Compare(null, null, req);
                    } else if (CurrentTemplateMT == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_JPG && 
                        listTemplatesBase[0].MinutiaeType == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_JPG)
                    {
                        _CompareResponse = NamkuUtils.NAMKU_API.Compare(new MemoryStream(byCurrentTemplate), new MemoryStream(listTemplatesBase[0].Data), null);
                    }
                    else // Si es un JPG + Embedding =>  CurrentTemplateMT == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_JPG && listTemplatesBase[i].MinutiaeType == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_FACIAL_NAMKU
                    {
                        //Si vienen varios agregarlos en lista de embeddings
                        CompareRequest req = new CompareRequest();
                        req.Target = new List<CompareRequestFaceObject>();
                        
                        for (int j = 0; j < listTemplatesBase.Count; j++)
                        {
                            CompareRequest reqAux = JsonConvert.DeserializeObject<CompareRequest>(Encoding.ASCII.GetString(listTemplatesBase[0].Data));
                            CompareRequestFaceObject crObj = new CompareRequestFaceObject();
                            crObj.Id = j+1;
                            crObj.Embeddings = reqAux.Target[0].Embeddings;
                            req.Target.Add(crObj);
                        }
                        _CompareResponse = NamkuUtils.NAMKU_API.Compare(new MemoryStream(byCurrentTemplate), null, req);
                    }

                    if (_CompareResponse == null || _CompareResponse.Error != null)
                    {
                        LOG.Debug("Bio.Core.Matcher.Namku.Matcher _CompareResponse == NULL => " + (_CompareResponse==null));
                        if (_CompareResponse.Error != null)  // => Hubo error => Lo muestro
                        {
                            LOG.Error("Bio.Core.Matcher.Namku.Matcher Error => " + _CompareResponse.Error.Code + " - " + _CompareResponse.Error.Message);
                        }
                        score = 5;
                        verifyOk = false;
                        bestscore = 5;
                    }
                    else //No hubo error
                    {
                        LOG.Debug("Bio.Core.Matcher.Namku.Matcher tokennamku.Data.Distance => " + (_CompareResponse.Data.Closest.Distance).ToString());

                        score = Convert.ToDouble(_CompareResponse.Data.Closest.Distance);    //(tokennamku.Data != null && tokennamku.Data.Distance != null) ? Convert.ToDouble(tokennamku.Data.Distance, CultureInfo.InvariantCulture) : 5;
                        if (score <= _threshold) //La distancia debe ser menor al threshold indicado, cunado mas cerca de 0 mejor
                        {
                            verifyOk = true;
                            //Si es Matching type first => Retorno
                            if (this._matchingType == 1)
                            {
                                bestscore = score;
                                templateVerified = listTemplatesBase[0];
                                //break;
                            }
                            else
                            {
                                if (score < bestscore)
                                {
                                    templateVerified = listTemplatesBase[0];
                                    bestscore = score;
                                }
                            }
                        }
                        else
                        {
                            bestscore = score;
                        }
                        LOG.Debug("Bio.Core.Matcher.Namku.Matcher Score = " + score);
                        LOG.Debug("Bio.Core.Matcher.Namku.Matcher bestscore = " + bestscore);
                        LOG.Debug("Bio.Core.Matcher.Namku.Matcher verifyOk = " + verifyOk.ToString());
                    }
                //    break; //Salgo del for porque 
                //}

                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                }
                else
                {
                    paramout.SetValue("message", "No hubo matching positivo [" + (token != null &&
                                                                                  oResponse.error != null &&
                                                                                  oResponse.error.message != null ? oResponse.error.message : "") + "]");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                LOG.Debug("Bio.Core.Matcher.Namku.Matcher xmloutput = " + xmloutput);
            }
            catch (Exception ex) 
            {
                msg = "Bio.Core.Matcher.Namku.Matcher.Verify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_VERIFY;
            }

            return Errors.IERR_OK;
        }

        /// <summary>
        /// Acción de identificación. Ingresa información para identificar, template base
        /// y lista contra que verificar, parámetros de chequeo, y se realiza la identificación.
        /// </summary>
        /// <param name="xmlinput">Parametros de ingreso serialziados</param>
        /// <param name="xmloutput">Parámetros de salida serializados</param>
        /// <returns>Codigo que indica como funcionó la operación.</returns>
        public int Identify(string xmlinput, out string xmloutput)
        {
            double score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;

            try
            {
                //0.-Deserializo xmlimput
                paramin = Serialize.Parameters.DeserializeFromXml(xmlinput);

                //1. Importo template de registro, creo cada uno y los importo
                try
                {
                    byCurrentTemplate = Convert.FromBase64String((string)paramin.GetValue("currentTemplate"));
                }
                catch (Exception ex)
                {
                    msg = "Bio.Core.Matcher.Namku.Matcher Error importando CurrentTemplate " +
                         "[ex=" + ex.Message + "]";
                    LOG.Fatal(msg);
                    paramout.SetValue("message", msg);
                    xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Preapro prametros para enviar
                ImageSet imageSetNew = new ImageSet();
                MultipartFormDataSample.Client.Image parameter1 = new MultipartFormDataSample.Client.Image();
                parameter1.FileName = @"image1.jpg";
                //parameter1.ImageData = Convert.FromBase64String(image1);
                parameter1.MimeType = "image/jpeg";

                MultipartFormDataSample.Client.Image parameter2 = new MultipartFormDataSample.Client.Image();
                parameter2.FileName = @"image2";
                //parameter2.ImageData = Convert.FromBase64String(image2);
                parameter2.MimeType = "image/jpeg";

                //Deserializo la lista de templates pasados contra que verificar
                List<Bio.Core.Matcher.Template> listTemplatesBase =
                    Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Bio.Core.Matcher.Template>>(
                                (string)paramin.GetValue("listBaseTemplates"));
                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                for (int i = 0; i < listTemplatesBase.Count; ++i)
                {
                    parameter1.ImageData = byCurrentTemplate;
                    parameter2.ImageData = listTemplatesBase[i].Data;
                    
                    imageSetNew.Images = new List<MultipartFormDataSample.Client.Image>();
                    imageSetNew.Images.Add(parameter1);
                    imageSetNew.Images.Add(parameter2);

                    var multipartContent = new MultipartFormDataContent();

                    var imageSetJson = JsonConvert.SerializeObject(imageSetNew,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        });

                    multipartContent.Add(new StringContent(imageSetJson, Encoding.UTF8, "application/json"), "imageset");

                    int counter = 0;
                    foreach (var image in imageSetNew.Images)
                    {
                        var imageContent = new ByteArrayContent(image.ImageData);
                        imageContent.Headers.ContentType = new MediaTypeHeaderValue(image.MimeType);
                        multipartContent.Add(imageContent, "image" + counter++, image.FileName);
                    }

                    var response = new HttpClient()
                        .PostAsync(NamkuUtils._URLServcieBase + "/v2/Compare", multipartContent)
                        .Result;

                    var responseContent = response.Content.ReadAsStringAsync().Result;

                    NamkuResponseConpareFace oResponse = JsonConvert.DeserializeObject<NamkuResponseConpareFace>(responseContent);
                    LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse != null => " + (oResponse != null).ToString());
                    if (oResponse.output != null) LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse.output.distance => " + (oResponse.output.distance).ToString());
                    if (oResponse.error != null && oResponse.error.message != null) LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse.error.message => " + oResponse.error.message);

                    score = (oResponse != null && oResponse.output != null) ? oResponse.output.distance : 5;
                    if (score <= _threshold) //La distancia debe ser menor al threshold indicado, cunado mas cerca de 0 mejor
                    {
                        verifyOk = true;
                        //Si es Matching type first => Retorno
                        if (this._matchingType == 1)
                        {
                            bestscore = score;
                            templateVerified = listTemplatesBase[i];
                            break;
                        }
                        else
                        {
                            if (score < bestscore)
                            {
                                templateVerified = listTemplatesBase[i];
                                bestscore = score;
                            }
                        }
                    }
                }

                //3.- Si verifico bien armo salida
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                }
                else
                {
                    paramout.SetValue("message", "No hubo matching positivo");
                }
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Namku.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        public int Identify(ITemplate templateCurrent, List<Bio.Core.Matcher.Template> listTemplatePeople, 
                            out float score, out int idBir)
        {
            idBir = 0;
            score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;

            try
            {
                byCurrentTemplate = templateCurrent.Data;

                ////2.- Genero objeto para matching
                //matcherConfig = new MatcherConfigurationManager();
                //matcherConfig.MatcherSecurityLevel = NamkuUtils._SecurityLevel;

                //matcher = new FPhi.Matcher.Matcher(matcherConfig);

                ////Deserializo la lista de templates pasados contra que verificar
                //List<Bio.Core.Matcher.Template> listTemplatesBase = listTemplatePeople;
                ////Contendra el template contra el que se verifico positivametne si asi fue,
                ////para datos de bodypart, companyid, etc.
                //Bio.Core.Matcher.Template templateVerified = null;

                ////Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                //// Crear el primer usuario.
                //user1 = matcher.CreateUser(byCurrentTemplate);

                //// Verificar los resultados de las extracciones.
                //AuthenticationResult authenticationResult;

                //for (int i = 0; i < listTemplatesBase.Count; ++i)
                //{
                //    authenticationResult = matcher.Authenticate(user1, listTemplatesBase[i].Data);
                //    score = (float)authenticationResult.Similarity;
                //    if (authenticationResult.IsPositiveMatch && score >= _threshold)
                //    {
                //        verifyOk = true;
                //        //Si es Matching type first => Retorno
                //        if (this._matchingType == 1)
                //        {
                //            bestscore = score;
                //            templateVerified = listTemplatesBase[i];
                //            break;
                //        }
                //        else
                //        {
                //            if (score > bestscore)
                //            {
                //                templateVerified = listTemplatesBase[i];
                //                bestscore = score;
                //            }
                //        }
                //    }
                //}

                //2.- Preapro prametros para enviar
                ImageSet imageSetNew = new ImageSet();
                MultipartFormDataSample.Client.Image parameter1 = new MultipartFormDataSample.Client.Image();
                parameter1.FileName = @"image1.jpg";
                //parameter1.ImageData = Convert.FromBase64String(image1);
                parameter1.MimeType = "image/jpeg";

                MultipartFormDataSample.Client.Image parameter2 = new MultipartFormDataSample.Client.Image();
                parameter2.FileName = @"image2";
                //parameter2.ImageData = Convert.FromBase64String(image2);
                parameter2.MimeType = "image/jpeg";

                //Deserializo la lista de templates pasados contra que verificar
                List < Bio.Core.Matcher.Template > listTemplatesBase = listTemplatePeople;
                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                Bio.Core.Matcher.Template templateVerified = null;

                for (int i = 0; i < listTemplatesBase.Count; ++i)
                {
                    parameter1.ImageData = byCurrentTemplate;
                    parameter2.ImageData = listTemplatesBase[i].Data;

                    imageSetNew.Images = new List<MultipartFormDataSample.Client.Image>();
                    imageSetNew.Images.Add(parameter1);
                    imageSetNew.Images.Add(parameter2);

                    var multipartContent = new MultipartFormDataContent();

                    var imageSetJson = JsonConvert.SerializeObject(imageSetNew,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        });

                    multipartContent.Add(new StringContent(imageSetJson, Encoding.UTF8, "application/json"), "imageset");

                    int counter = 0;
                    foreach (var image in imageSetNew.Images)
                    {
                        var imageContent = new ByteArrayContent(image.ImageData);
                        imageContent.Headers.ContentType = new MediaTypeHeaderValue(image.MimeType);
                        multipartContent.Add(imageContent, "image" + counter++, image.FileName);
                    }

                    var response = new HttpClient()
                        .PostAsync(NamkuUtils._URLServcieBase + "/v2/Compare", multipartContent)
                        .Result;

                    var responseContent = response.Content.ReadAsStringAsync().Result;

                    NamkuResponseConpareFace oResponse = JsonConvert.DeserializeObject<NamkuResponseConpareFace>(responseContent);
                    LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse != null => " + (oResponse != null).ToString());
                    if (oResponse.output != null) LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse.output.distance => " + (oResponse.output.distance).ToString());
                    if (oResponse.error != null && oResponse.error.message != null) LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse.error.message => " + oResponse.error.message);

                    score = (oResponse != null && oResponse.output != null) ? oResponse.output.distance : 5;
                    if (score <= _threshold) //La distancia debe ser menor al threshold indicado, cunado mas cerca de 0 mejor
                    {
                        verifyOk = true;
                        //Si es Matching type first => Retorno
                        if (this._matchingType == 1)
                        {
                            bestscore = score;
                            templateVerified = listTemplatesBase[i];
                            break;
                        }
                        else
                        {
                            if (score < bestscore)
                            {
                                templateVerified = listTemplatesBase[i];
                                bestscore = score;
                            }
                        }
                    }
                }

                //3.- Si verifico bien armo salida
                score = (float)bestscore;
                idBir = templateVerified == null ? 0 : templateVerified.Id;

                //paramout.AddValue("score", bestscore);
                //paramout.AddValue("result", verifyOk ? 1 : 2);
                //paramout.AddValue("threshold", threshold);
                //if (verifyOk)
                //{
                //    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                //    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                //    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                //    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                //    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                //}
                //else
                //{
                //    paramout.SetValue("message", "No hubo matching positivo");
                //}
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Namku.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        public int Verify(ITemplate template1, ITemplate template2, out float score)
        {
            throw new NotImplementedException();
        }


        #endregion

        public void Dispose()
        {
        }


        int IMatcher.Identify(ITemplate templateCurrent, List<ITemplate> listTemplatePeople, out float score, out string idBir)
        {
            idBir = null;
            score = 0;
            double bestscore = 0;
            double threshold = _threshold;

            Parameters paramin;
            Parameters paramout = new Parameters();
            paramout.AddValue("message", "");
            string msg;
            byte[] byCurrentTemplate;
            bool verifyOk = false;
            byte[] user1 = null;

            try
            {
                byCurrentTemplate = templateCurrent.Data;

                ////2.- Genero objeto para matching
                //matcherConfig = new MatcherConfigurationManager();
                //matcherConfig.MatcherSecurityLevel = NamkuUtils._SecurityLevel;

                //matcher = new FPhi.Matcher.Matcher(matcherConfig);

                ////Deserializo la lista de templates pasados contra que verificar
                //List<Bio.Core.Matcher.Template> listTemplatesBase = listTemplatePeople;
                ////Contendra el template contra el que se verifico positivametne si asi fue,
                ////para datos de bodypart, companyid, etc.
                //Bio.Core.Matcher.Template templateVerified = null;

                ////Comienza identificacion entre los templates de una misma persona. Quiza sea solo verificacion 1:1
                //// Crear el primer usuario.
                //user1 = matcher.CreateUser(byCurrentTemplate);

                //// Verificar los resultados de las extracciones.
                //AuthenticationResult authenticationResult;

                //for (int i = 0; i < listTemplatesBase.Count; ++i)
                //{
                //    authenticationResult = matcher.Authenticate(user1, listTemplatesBase[i].Data);
                //    score = (float)authenticationResult.Similarity;
                //    if (authenticationResult.IsPositiveMatch && score >= _threshold)
                //    {
                //        verifyOk = true;
                //        //Si es Matching type first => Retorno
                //        if (this._matchingType == 1)
                //        {
                //            bestscore = score;
                //            templateVerified = listTemplatesBase[i];
                //            break;
                //        }
                //        else
                //        {
                //            if (score > bestscore)
                //            {
                //                templateVerified = listTemplatesBase[i];
                //                bestscore = score;
                //            }
                //        }
                //    }
                //}

                //2.- Preapro prametros para enviar
                ImageSet imageSetNew = new ImageSet();
                MultipartFormDataSample.Client.Image parameter1 = new MultipartFormDataSample.Client.Image();
                parameter1.FileName = @"image1.jpg";
                //parameter1.ImageData = Convert.FromBase64String(image1);
                parameter1.MimeType = "image/jpeg";

                MultipartFormDataSample.Client.Image parameter2 = new MultipartFormDataSample.Client.Image();
                parameter2.FileName = @"image2";
                //parameter2.ImageData = Convert.FromBase64String(image2);
                parameter2.MimeType = "image/jpeg";

                //Deserializo la lista de templates pasados contra que verificar
                List<ITemplate> listTemplatesBase = listTemplatePeople;
                //Contendra el template contra el que se verifico positivametne si asi fue,
                //para datos de bodypart, companyid, etc.
                ITemplate templateVerified = null;

                for (int i = 0; i < listTemplatesBase.Count; ++i)
                {
                    parameter1.ImageData = byCurrentTemplate;
                    parameter2.ImageData = listTemplatesBase[i].Data;

                    imageSetNew.Images = new List<MultipartFormDataSample.Client.Image>();
                    imageSetNew.Images.Add(parameter1);
                    imageSetNew.Images.Add(parameter2);

                    var multipartContent = new MultipartFormDataContent();

                    var imageSetJson = JsonConvert.SerializeObject(imageSetNew,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        });

                    multipartContent.Add(new StringContent(imageSetJson, Encoding.UTF8, "application/json"), "imageset");

                    int counter = 0;
                    foreach (var image in imageSetNew.Images)
                    {
                        var imageContent = new ByteArrayContent(image.ImageData);
                        imageContent.Headers.ContentType = new MediaTypeHeaderValue(image.MimeType);
                        multipartContent.Add(imageContent, "image" + counter++, image.FileName);
                    }

                    var response = new HttpClient()
                        .PostAsync(NamkuUtils._URLServcieBase + "/v2/Compare", multipartContent)
                        .Result;

                    var responseContent = response.Content.ReadAsStringAsync().Result;

                    NamkuResponseConpareFace oResponse = JsonConvert.DeserializeObject<NamkuResponseConpareFace>(responseContent);
                    LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse != null => " + (oResponse != null).ToString());
                    if (oResponse.output != null) LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse.output.distance => " + (oResponse.output.distance).ToString());
                    if (oResponse.error != null && oResponse.error.message != null) LOG.Debug("Bio.Core.Matcher.Namku.Matcher oResponse.error.message => " + oResponse.error.message);

                    score = (oResponse != null && oResponse.output != null) ? oResponse.output.distance : 5;
                    if (score <= _threshold) //La distancia debe ser menor al threshold indicado, cunado mas cerca de 0 mejor
                    {
                        verifyOk = true;
                        //Si es Matching type first => Retorno
                        if (this._matchingType == 1)
                        {
                            bestscore = score;
                            templateVerified = listTemplatesBase[i];
                            break;
                        }
                        else
                        {
                            if (score < bestscore)
                            {
                                templateVerified = listTemplatesBase[i];
                                bestscore = score;
                            }
                        }
                    }
                }

                //3.- Si verifico bien armo salida
                score = (float)bestscore;
                idBir = templateVerified == null ? null : templateVerified.BodyPart.ToString();

                //paramout.AddValue("score", bestscore);
                //paramout.AddValue("result", verifyOk ? 1 : 2);
                //paramout.AddValue("threshold", threshold);
                //if (verifyOk)
                //{
                //    paramout.AddValue("id", templateVerified == null ? 0 : templateVerified.Id);
                //    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                //    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                //    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                //    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource);
                //}
                //else
                //{
                //    paramout.SetValue("message", "No hubo matching positivo");
                //}
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
            }
            catch (Exception ex)
            {
                msg = "Bio.Core.Matcher.Namku.Matcher.Identify Exception [" + ex.Message + "]";
                LOG.Fatal(msg);
                paramout.SetValue("message", msg);
                //xmloutput = Serialize.Parameters.SerializeToXml(paramout);
                return Errors.IERR_IDENTIFY;
            }

            return Errors.IERR_OK;
        }

        int IMatcher.Identify(ITemplate templateCurrent, out float score, out int idBir)
        {
            throw new NotImplementedException();
        }

        int IMatcher.Inicialize(string xmlparam)
        {
            throw new NotImplementedException();
        }

        int IMatcher.Enroll(List<ITemplate> listTemplatePeople)
        {
            throw new NotImplementedException();
        }

        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }
    }

    //{"output": {"distance": "0.0"}}
    public class output
    {

        public float distance { get; set; }

    }

    public class error
    {
        public string message { get; set; }
        public string reason { get; set; }

    }

    /*
       {
         "data":null,
         "error":{
              "message":"No se detectaron caras en alguna de las imagenes",
               "reason":"noFaces"},
         "status_code":400,
         "success":false
        }
    */
    public class NamkuResponseConpareFace
    {
        public output output { get; set; }
        public string data { get; set; }
        public string status_code { get; set; }
        public string success { get; set; }
        public error error { get; set; }

    }
}
