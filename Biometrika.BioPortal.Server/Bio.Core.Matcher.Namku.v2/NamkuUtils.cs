﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Biometrika.NamkuFaceAPI;
using log4net;


namespace Bio.Core.Matcher.Namku
{
    public static class NamkuUtils
    {

        internal static NamkuFaceApi NAMKU_API; 

        internal static string SERVICE_USER = "facial@biometrika.cl";
        internal static string SERVICE_PSW = "$F4n=hh!WxgHakf-";
        internal static string ACCESS_TOKEN;
        internal static string REFRESH_TOKEN;
        internal static DateTime DATE_ACCESS_TOKEN;
        internal static DateTime DATE_REFRESH_TOKEN;
        internal static TokenNamku TOKENNAMKU;
        internal static string MASK_DATE = "yyyyMMdd";

        private static readonly ILog LOG = LogManager.GetLogger(typeof(NamkuUtils));

        public static bool IsInitialized;

        public static double _SecurityLevel = 0.7;
        public static string _URLServcieBase = "https://facial.biometrika.cl/";


        /// <summary>
        /// Inicializa la licencia la primera vez que se usa una clase de este Namespace.
        /// </summary>
        public static bool Initialize(string parameters)
        {

            try
            {

                if (parameters == null)
                {
                    LOG.Error("Bio.Core.Matcher.Namku.Initialize Error - Parameters Matcher = NULL");
                    return false;
                }

                LOG.Debug("Bio.Core.Matcher.Namku.Initialize - ExtractPath(parameters) de parameters=" + parameters);
                IsInitialized = ExtractParams(parameters, out _SecurityLevel, out _URLServcieBase);

                if (!IsInitialized)
                {
                    LOG.Error("Bio.Core.Matcher.Namku.Initialize Error - Component Parse parameters error");
                    return false;
                } else
                { 
                    ////IsInitialized = GetOrRefreshToken();
                    //if (!IsInitialized)
                    //{
                        //NAMKU_API = new NamkuFaceApi(_URLServcieBase, "v2/recognition/extract", "v2/recognition/compare", "v2/dni", "v2/auth/token", SERVICE_USER, SERVICE_PSW, 30000);

                        LOG.Debug("Bio.Core.Matcher.Namku.Initialize Crea NamkuFaceApi...");
                        //NamkuUtils._URLServcieBase = _URLServcieBase;
                        NamkuUtils.NAMKU_API = new NamkuFaceApi(_URLServcieBase, "v2/recognition/extract", "v2/recognition/compare", "v2/dni", "v2/auth/token", NamkuUtils.SERVICE_USER, NamkuUtils.SERVICE_PSW, 30000, false);
                        LOG.Debug("Bio.Core.Matcher.Namku.Initialize NAMKU_API != NULL => " + (NamkuUtils.NAMKU_API != null).ToString());
                    //}

                    


                    IsInitialized = (NamkuUtils.NAMKU_API != null); // NamkuUtils.GetOrRefreshToken();
                }

                LOG.Debug("Bio.Core.Matcher.Namku.Initialize - IsInitialized=" + IsInitialized);

            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Namku.Initializee Error", ex);
            }
            return IsInitialized;

        }

        public static bool CheckValidityToken()
        {

            try
            {
                LOG.Debug("Bio.Core.Matcher.Namku.CheckValidityToken IN...");
                IsInitialized = GetOrRefreshToken();
                LOG.Debug("Bio.Core.Matcher.Namku.CheckValidityToken - IsInitialized=" + IsInitialized);
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Namku.CheckValidityToken Error", ex);
            }
            return IsInitialized;

        }

        /// <summary>
        /* 

            Las API en productivo las manejamos por autenticacion de usuario y contraseña, y el consumo de recursos usando JWT como medio de autenticacion y autorizacion.
            El flujo debe ser el siguiente:


            Realizar un request tipo POST a /v2/auth/token usando Basic Auth como metodo de autenticacion con su usuario y contraseña.Esto devuelve:
                {
                   "api_version": "2.0.0",
                    "data": {
                            "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NTA3NzI0NDksIm5iZiI6MTU1MDc3MjQ0OSwianRpIjoiMzIwMGM0YzUtNDE1MC00NzBmLTlhMzQtNjZjOT
                            A2ZTcyMjViIiwiZXhwIjoxNTUwNzczMzQ5LCJzdWIiOjEsImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyIsImNsYWltcyI6eyJ1c2VybmFtZSI6InVzZXJAbmFta3UuY2wiLCJuYW1lIjoiUGVk
                            cm8gUGVyZXoiLCJlbWFpbCI6InVzZXJAbmFta3UuY2wiLCJjb21wYW55IjoxLCJsaWNlbnNlcyI6eyJyZWNvZ25pdGlvbiI6eyJtYXhfbWF0Y2hfcmVxdWVzdCI6IjEwIiwibWF4X3Blb3BsZXMiOiI
                            yMCJ9LCJhY2Nlc3NfY29udHJvbCI6e319fX0.HzSaWLEO7S5HbVoanEiYTcAJX9YT7GpwazfjCF6ZQMw",
                            "refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NTA3NzI0NDksIm5iZiI6MTU1MDc3MjQ0OSwianRpIjoiMjhjNDFlOWMtYWYyNi00ZmYyLTk4OT
                            QtN2UwNTUwZmI3YzhiIiwiZXhwIjoxNTUzMzY0NDQ5LCJzdWIiOjEsInR5cGUiOiJyZWZyZXNoIn0.lLpo7jL9NJoaTpqe-5eC2K_6f3Z91U_ZB68gPCTcFAA"
                            }
                }

            Toda peticion subsiguiente debe realizarse usando Bearer Token como método de autenticacion, incluyendo como token el access_token recibido anteriormente.
            Este token tiene una vida util de 15 minutos.
            El access_token puede refrescarse haciendo una peticion tipo POST a /v2/auth/refresh usando el refresh_token como BearerToken. 

            Esto devuelve un nuevo access_token valido por otros 15 minutos.El refresh_token tiene una vida util de 30 dias.Despues de vencer estos tokens o de extraviarse
    
            el refresh_token debe volver a iniciar sesion.
            El endpoint de comparacion de caras es: /v2/recognition/compare
            El endpoint de dni es: /v2/dni
            El usuario y contraseña asociado es:

            username: facial @biometrika.cl
            password: $F4n= hh!WxgHakf-
        */

        /// </summary>
        /// <returns></returns>
        private static bool GetOrRefreshToken()
        {
            bool ret = false;
            try
            {
                LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken IN...");
                if ((string.IsNullOrEmpty(ACCESS_TOKEN) && string.IsNullOrEmpty(REFRESH_TOKEN)) ||
                    ((DateTime.Now - DATE_REFRESH_TOKEN).Days > 30))
                {
                    using (var responsetoken = new HttpClient())
                    {
                        var byteArray = Encoding.ASCII.GetBytes(SERVICE_USER + ":" + SERVICE_PSW);
                        responsetoken.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                        LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken LLamando a " + _URLServcieBase + "/v2/auth/token ...");
                        var resulta = responsetoken.PostAsync(_URLServcieBase + "/v2/auth/token", null).Result;
                        LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken resulta != null => " + (resulta != null).ToString());
                        var responseContenttoken = resulta.Content.ReadAsStringAsync().Result;
                        LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken responseContenttoken != null => " + (responseContenttoken != null).ToString());
                        TOKENNAMKU = (TokenNamku)Newtonsoft.Json.JsonConvert.DeserializeObject(responseContenttoken, typeof(TokenNamku));
                        //Se actualiza información de token en el archivo de configuración.
                        if (TOKENNAMKU != null)
                        {
                            ACCESS_TOKEN = TOKENNAMKU.Data.AccessToken;
                            LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken ACCESS_TOKEN = " + ACCESS_TOKEN);
                            REFRESH_TOKEN = TOKENNAMKU.Data.RefreshToken;
                            LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken REFRESH_TOKEN = " + REFRESH_TOKEN);
                            DATE_ACCESS_TOKEN = DateTime.Now; //.ToString(MASK_DATE));
                            LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken DATE_ACCESS_TOKEN = " + DATE_ACCESS_TOKEN.ToString());
                            DATE_REFRESH_TOKEN = DATE_ACCESS_TOKEN;
                            LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken DATE_REFRESH_TOKEN = " + DATE_REFRESH_TOKEN.ToString());
                            ret = true;
                        } else
                        {
                            LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken TOKENNAMKU == null");
                            ret = false;
                        }
                    }
                } else if ((DateTime.Now - DATE_ACCESS_TOKEN).Minutes > 15)  //Si no son nulos, y paso mas de 15 minutos hay que refescar el token
                {
                    using (var responsetoken = new HttpClient())
                    {
                        responsetoken.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", REFRESH_TOKEN);
                        LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken LLamando a " + _URLServcieBase + "/v2/auth/refresh ...");
                        var resulta = responsetoken.PostAsync(_URLServcieBase + "/v2/auth/refresh", null).Result;
                        LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken resulta != null => " + (resulta != null).ToString());
                        var responseContenttoken = resulta.Content.ReadAsStringAsync().Result;
                        LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken responseContenttoken != null => " + (responseContenttoken != null).ToString());
                        TOKENNAMKU = (TokenNamku)Newtonsoft.Json.JsonConvert.DeserializeObject(responseContenttoken, typeof(TokenNamku));
                        //Se actualiza información de token en el archivo de configuración.
                        if (TOKENNAMKU != null)
                        {
                            ACCESS_TOKEN = TOKENNAMKU.Data.AccessToken;
                            LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken ACCESS_TOKEN = " + ACCESS_TOKEN);
                            DATE_ACCESS_TOKEN = DateTime.Now; //.ToString(MASK_DATE));
                            LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken DATE_ACCESS_TOKEN = " + DATE_ACCESS_TOKEN.ToString());
                            ret = true;
                        }
                        else
                        {
                            LOG.Debug("Bio.Core.Matcher.Namku.GetOrRefreshToken TOKENNAMKU == null");
                            ret = false;
                        }
                        //tokennamku = (TokenNamku)Newtonsoft.Json.JsonConvert.DeserializeObject(responseContenttoken, typeof(TokenNamku));
                        ////Se actualiza información de token en el archivo de configuración.
                        //if (tokennamku != null)
                        //{
                        //    GuardarValorAppSettings("Token", tokennamku.Data.AccessToken);
                        //    GuardarValorAppSettings("FechaToken", DateTime.Now.ToString(TestNamkurestApi.Properties.Settings.Default.MaskDate));
                        //    GuardarArchivo();
                        //}
                    }
                } else
                {
                    ret = true;
                }
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("Bio.Core.Matcher.Namku.GetOrRefreshToken Error", ex);
            }
            return ret;
        }


        //Extrae nivel d eseguridad de matching
        private static bool ExtractParams(string parameters, out double securitylevel, out string url)
        {
            securitylevel = 0.7;
            url = "http://facial.biometrika.cl/";

            try
            {
                string[] arr = parameters.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                LOG.Debug("Bio.Core.Matcher.Namku.ExtractParams arr.length=" + arr.Length.ToString());
                if (arr.Length == 0) return false;

                for (int i = 0; i < arr.Length; i++)
                {
                    LOG.Debug("Bio.Core.Matcher.Namku.ExtractParams procesando arr[" + i.ToString() + "]=" + arr[i]);
                    string[] item = arr[i].Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                    LOG.Debug("Bio.Core.Matcher.Namku.ExtractParams item.length=" + item.Length.ToString());
                    if (item[0].Equals("SecurityLevel"))
                    {
                        if (item.Length == 2 && !string.IsNullOrEmpty(item[1])) { 
                            securitylevel = Convert.ToDouble(item[1]); 
                        }
                        LOG.Debug("Bio.Core.Matcher.Namku.ExtractParams securitylevel=" + securitylevel);
                    }

                    if (item[0].Equals("URLService"))
                    {
                        if (item.Length == 2 && !string.IsNullOrEmpty(item[1]))
                        {
                            url = item[1];
                        }
                        LOG.Debug("Bio.Core.Matcher.Namku.ExtractParams URLService=" + url);
                    }

                    if (item[0].Equals("ServiceUser"))
                    {
                        if (item.Length == 2 && !string.IsNullOrEmpty(item[1]))
                        {
                            SERVICE_USER = item[1].Trim(); 
                        }
                        LOG.Debug("Bio.Core.Matcher.Namku.ExtractParams ServiceUser=" + SERVICE_USER);
                    }

                    if (item[0].Equals("ServicePsw"))
                    {
                        if (item.Length == 2 && !string.IsNullOrEmpty(item[1]))
                        {
                            SERVICE_PSW = item[1].Trim();
                        }
                        LOG.Debug("Bio.Core.Matcher.Namku.ExtractParams ServicePsw=" + SERVICE_PSW);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Bio.Core.Matcher.Namku.ExtractParams Error", ex);
                return false;
            }
            return true;
        }

        //private static MatchingSecurityLevel SetSecurityLevel(string sSecurityLevel)
        //{
        //    if (sSecurityLevel.Equals("Medium")) return MatchingSecurityLevel.MediumSecurityLevel;
        //    if (sSecurityLevel.Equals("High")) return MatchingSecurityLevel.HighSecurityLevel;
        //    if (sSecurityLevel.Equals("MediumHigh")) return MatchingSecurityLevel.MediumHighSecurityLevel;
        //    if (sSecurityLevel.Equals("VeryHigh")) return MatchingSecurityLevel.VeryHighSecurityLevel;
        //    if (sSecurityLevel.Equals("Highest")) return MatchingSecurityLevel.HighestSecurityLevel;
        //    return MatchingSecurityLevel.MediumHighSecurityLevel;
        //}
    }

}
