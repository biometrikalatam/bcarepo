﻿namespace TestNamkurestApi
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.btnEnviar = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtTH = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtValueid = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTypeid = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.txtFoto = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btnEndCapture = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnLiveness2D = new System.Windows.Forms.Button();
            this.chkSaveImages = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.chkF2 = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.chkF1 = new System.Windows.Forms.CheckBox();
            this.btnCaptura2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.imageCam = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCaptura1 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.grpConfig = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtApiPOS = new System.Windows.Forms.TextBox();
            this.btnPOSController = new System.Windows.Forms.Button();
            this.txtConnector = new System.Windows.Forms.TextBox();
            this.txtFNro = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.txtURLWebApi = new System.Windows.Forms.TextBox();
            this.chkWebApiOn = new System.Windows.Forms.CheckBox();
            this.txtCompanyId = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.picFront = new System.Windows.Forms.PictureBox();
            this.picFirmaRes = new System.Windows.Forms.PictureBox();
            this.picFotoRes = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEraseResult = new System.Windows.Forms.Button();
            this.labScore = new System.Windows.Forms.Label();
            this.rtbResultado = new System.Windows.Forms.RichTextBox();
            this.labResVerify = new System.Windows.Forms.Label();
            this.picQuestion = new System.Windows.Forms.PictureBox();
            this.picNOOK = new System.Windows.Forms.PictureBox();
            this.picOK = new System.Windows.Forms.PictureBox();
            this.btnMindee = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.grpConfig.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFront)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFirmaRes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoRes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQuestion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNOOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOK)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEnviar
            // 
            this.btnEnviar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviar.Location = new System.Drawing.Point(157, 20);
            this.btnEnviar.Margin = new System.Windows.Forms.Padding(2);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(157, 28);
            this.btnEnviar.TabIndex = 0;
            this.btnEnviar.Text = "Enviar Caras Directo...";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(945, 0);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(246, 27);
            this.button3.TabIndex = 7;
            this.button3.Text = "Enviar Caras x BP Plugin";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(482, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(449, 20);
            this.textBox1.TabIndex = 8;
            this.textBox1.Text = "http://nvprod.cloudapp.net/BioPortal.Services.v5.8/BioPortal.Server.Plugin.Proces" +
    "s.asmx";
            // 
            // txtTH
            // 
            this.txtTH.Location = new System.Drawing.Point(230, 53);
            this.txtTH.Name = "txtTH";
            this.txtTH.Size = new System.Drawing.Size(73, 20);
            this.txtTH.TabIndex = 9;
            this.txtTH.Text = "55";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Value id";
            // 
            // txtValueid
            // 
            this.txtValueid.Location = new System.Drawing.Point(85, 76);
            this.txtValueid.Name = "txtValueid";
            this.txtValueid.Size = new System.Drawing.Size(94, 20);
            this.txtValueid.TabIndex = 13;
            this.txtValueid.Text = "21284415-2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Type Id";
            // 
            // txtTypeid
            // 
            this.txtTypeid.Location = new System.Drawing.Point(85, 54);
            this.txtTypeid.Name = "txtTypeid";
            this.txtTypeid.Size = new System.Drawing.Size(45, 20);
            this.txtTypeid.TabIndex = 11;
            this.txtTypeid.Text = "RUT";
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(945, 91);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(247, 28);
            this.button4.TabIndex = 15;
            this.button4.Text = "Enviar Cara a Verify contra BD";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtFoto
            // 
            this.txtFoto.Location = new System.Drawing.Point(999, 77);
            this.txtFoto.Name = "txtFoto";
            this.txtFoto.Size = new System.Drawing.Size(20, 20);
            this.txtFoto.TabIndex = 16;
            this.txtFoto.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(603, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 25);
            this.label2.TabIndex = 17;
            this.label2.Text = "1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(854, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 25);
            this.label5.TabIndex = 18;
            this.label5.Text = "2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(181, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Umbral";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(483, 76);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(449, 20);
            this.textBox2.TabIndex = 20;
            this.textBox2.Text = "http://nvprod.cloudapp.net/BioPortal.Services.v5.8//BioPortal.Server.WS.asmx";
            // 
            // btnEndCapture
            // 
            this.btnEndCapture.Location = new System.Drawing.Point(1084, 771);
            this.btnEndCapture.Margin = new System.Windows.Forms.Padding(2);
            this.btnEndCapture.Name = "btnEndCapture";
            this.btnEndCapture.Size = new System.Drawing.Size(141, 28);
            this.btnEndCapture.TabIndex = 25;
            this.btnEndCapture.Text = "Finalizar";
            this.btnEndCapture.UseVisualStyleBackColor = true;
            this.btnEndCapture.Click += new System.EventHandler(this.btnEndCapture_Click);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(516, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(485, 54);
            this.label7.TabIndex = 151;
            this.label7.Text = "Demostración de verificación facial  para POS...";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button11);
            this.groupBox1.Controls.Add(this.button10);
            this.groupBox1.Controls.Add(this.btnLiveness2D);
            this.groupBox1.Controls.Add(this.chkSaveImages);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.chkF2);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.chkF1);
            this.groupBox1.Controls.Add(this.btnCaptura2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.imageCam);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.btnCaptura1);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Location = new System.Drawing.Point(33, 110);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1219, 264);
            this.groupBox1.TabIndex = 152;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Zona de captura...";
            // 
            // btnLiveness2D
            // 
            this.btnLiveness2D.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLiveness2D.Location = new System.Drawing.Point(636, 243);
            this.btnLiveness2D.Margin = new System.Windows.Forms.Padding(2);
            this.btnLiveness2D.Name = "btnLiveness2D";
            this.btnLiveness2D.Size = new System.Drawing.Size(157, 23);
            this.btnLiveness2D.TabIndex = 157;
            this.btnLiveness2D.Text = "Liveness 2D";
            this.btnLiveness2D.UseVisualStyleBackColor = true;
            this.btnLiveness2D.Click += new System.EventHandler(this.btnLiveness2D_Click);
            // 
            // chkSaveImages
            // 
            this.chkSaveImages.AutoSize = true;
            this.chkSaveImages.Location = new System.Drawing.Point(356, 100);
            this.chkSaveImages.Name = "chkSaveImages";
            this.chkSaveImages.Size = new System.Drawing.Size(115, 17);
            this.chkSaveImages.TabIndex = 165;
            this.chkSaveImages.Text = "Save Images HDD";
            this.chkSaveImages.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(353, 154);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(109, 13);
            this.label16.TabIndex = 164;
            this.label16.Text = ">>>>>>>>>>>>>>>>>";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(353, 141);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 13);
            this.label15.TabIndex = 163;
            this.label15.Text = ">>>>>>>>>>>>>>>>>";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(353, 128);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(109, 13);
            this.label14.TabIndex = 162;
            this.label14.Text = ">>>>>>>>>>>>>>>>>";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(832, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(25, 25);
            this.label12.TabIndex = 161;
            this.label12.Text = "2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(490, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(25, 25);
            this.label13.TabIndex = 160;
            this.label13.Text = "1";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(134, 23);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(113, 21);
            this.button6.TabIndex = 159;
            this.button6.Text = "Stop Preview...";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // chkF2
            // 
            this.chkF2.AutoSize = true;
            this.chkF2.Location = new System.Drawing.Point(1104, 25);
            this.chkF2.Name = "chkF2";
            this.chkF2.Size = new System.Drawing.Size(45, 17);
            this.chkF2.TabIndex = 158;
            this.chkF2.Text = "B64";
            this.chkF2.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(965, 22);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(132, 22);
            this.button2.TabIndex = 157;
            this.button2.Text = "Seleccionar Foto 2...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(826, 50);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(335, 201);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 156;
            this.pictureBox2.TabStop = false;
            // 
            // chkF1
            // 
            this.chkF1.AutoSize = true;
            this.chkF1.Location = new System.Drawing.Point(748, 26);
            this.chkF1.Name = "chkF1";
            this.chkF1.Size = new System.Drawing.Size(45, 17);
            this.chkF1.TabIndex = 155;
            this.chkF1.Text = "B64";
            this.chkF1.UseVisualStyleBackColor = true;
            // 
            // btnCaptura2
            // 
            this.btnCaptura2.Enabled = false;
            this.btnCaptura2.Location = new System.Drawing.Point(826, 22);
            this.btnCaptura2.Margin = new System.Windows.Forms.Padding(2);
            this.btnCaptura2.Name = "btnCaptura2";
            this.btnCaptura2.Size = new System.Drawing.Size(134, 22);
            this.btnCaptura2.TabIndex = 31;
            this.btnCaptura2.Text = "Capturar de Preview...";
            this.btnCaptura2.UseVisualStyleBackColor = true;
            this.btnCaptura2.Click += new System.EventHandler(this.btnCaptura2_Click_1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(614, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 22);
            this.button1.TabIndex = 154;
            this.button1.Text = "Seleccionar Foto 1...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // imageCam
            // 
            this.imageCam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageCam.Location = new System.Drawing.Point(17, 50);
            this.imageCam.Margin = new System.Windows.Forms.Padding(2);
            this.imageCam.Name = "imageCam";
            this.imageCam.Size = new System.Drawing.Size(317, 201);
            this.imageCam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageCam.TabIndex = 30;
            this.imageCam.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(483, 49);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(325, 202);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 153;
            this.pictureBox1.TabStop = false;
            // 
            // btnCaptura1
            // 
            this.btnCaptura1.Enabled = false;
            this.btnCaptura1.Location = new System.Drawing.Point(483, 22);
            this.btnCaptura1.Margin = new System.Windows.Forms.Padding(2);
            this.btnCaptura1.Name = "btnCaptura1";
            this.btnCaptura1.Size = new System.Drawing.Size(126, 22);
            this.btnCaptura1.TabIndex = 29;
            this.btnCaptura1.Text = "Capturar de Preview...";
            this.btnCaptura1.UseVisualStyleBackColor = true;
            this.btnCaptura1.Click += new System.EventHandler(this.btnCaptura1_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(17, 23);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(113, 21);
            this.button5.TabIndex = 28;
            this.button5.Text = "Iniciar Preview...";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = global::TestNamkurestApi.Properties.Resources.Biometrika_Bajada_400x140;
            this.pictureBox3.Location = new System.Drawing.Point(33, 12);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(314, 92);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 150;
            this.pictureBox3.TabStop = false;
            // 
            // grpConfig
            // 
            this.grpConfig.Controls.Add(this.label20);
            this.grpConfig.Controls.Add(this.txtApiPOS);
            this.grpConfig.Controls.Add(this.btnPOSController);
            this.grpConfig.Controls.Add(this.txtConnector);
            this.grpConfig.Controls.Add(this.txtFNro);
            this.grpConfig.Controls.Add(this.label19);
            this.grpConfig.Controls.Add(this.button7);
            this.grpConfig.Controls.Add(this.label18);
            this.grpConfig.Controls.Add(this.txtURLWebApi);
            this.grpConfig.Controls.Add(this.chkWebApiOn);
            this.grpConfig.Controls.Add(this.txtFoto);
            this.grpConfig.Controls.Add(this.txtCompanyId);
            this.grpConfig.Controls.Add(this.label11);
            this.grpConfig.Controls.Add(this.label10);
            this.grpConfig.Controls.Add(this.label9);
            this.grpConfig.Controls.Add(this.label8);
            this.grpConfig.Controls.Add(this.btnEnviar);
            this.grpConfig.Controls.Add(this.txtTH);
            this.grpConfig.Controls.Add(this.button3);
            this.grpConfig.Controls.Add(this.label6);
            this.grpConfig.Controls.Add(this.txtValueid);
            this.grpConfig.Controls.Add(this.txtTypeid);
            this.grpConfig.Controls.Add(this.label3);
            this.grpConfig.Controls.Add(this.label4);
            this.grpConfig.Controls.Add(this.textBox1);
            this.grpConfig.Controls.Add(this.button4);
            this.grpConfig.Controls.Add(this.textBox2);
            this.grpConfig.Location = new System.Drawing.Point(33, 381);
            this.grpConfig.Name = "grpConfig";
            this.grpConfig.Size = new System.Drawing.Size(1219, 131);
            this.grpConfig.TabIndex = 153;
            this.grpConfig.TabStop = false;
            this.grpConfig.Text = "Zona de Configuración...";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(369, 57);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 13);
            this.label20.TabIndex = 35;
            this.label20.Text = "URL API:";
            // 
            // txtApiPOS
            // 
            this.txtApiPOS.Location = new System.Drawing.Point(427, 54);
            this.txtApiPOS.Name = "txtApiPOS";
            this.txtApiPOS.Size = new System.Drawing.Size(449, 20);
            this.txtApiPOS.TabIndex = 34;
            this.txtApiPOS.Text = "https://qabpservice5.9.biometrikalatam.com/api/v1/pos/facial/verify";
            // 
            // btnPOSController
            // 
            this.btnPOSController.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPOSController.Location = new System.Drawing.Point(915, 49);
            this.btnPOSController.Margin = new System.Windows.Forms.Padding(2);
            this.btnPOSController.Name = "btnPOSController";
            this.btnPOSController.Size = new System.Drawing.Size(246, 27);
            this.btnPOSController.TabIndex = 33;
            this.btnPOSController.Text = "Verify x  POSController";
            this.btnPOSController.UseVisualStyleBackColor = true;
            this.btnPOSController.Click += new System.EventHandler(this.btnPOSController_Click);
            // 
            // txtConnector
            // 
            this.txtConnector.Location = new System.Drawing.Point(6, 105);
            this.txtConnector.Name = "txtConnector";
            this.txtConnector.Size = new System.Drawing.Size(73, 20);
            this.txtConnector.TabIndex = 32;
            this.txtConnector.Text = "Regula";
            // 
            // txtFNro
            // 
            this.txtFNro.Location = new System.Drawing.Point(134, 105);
            this.txtFNro.Name = "txtFNro";
            this.txtFNro.Size = new System.Drawing.Size(20, 20);
            this.txtFNro.TabIndex = 30;
            this.txtFNro.Text = "1";
            this.txtFNro.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(85, 108);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 13);
            this.label19.TabIndex = 31;
            this.label19.Text = "Foto Nro:";
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(157, 101);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(206, 27);
            this.button7.TabIndex = 29;
            this.button7.Text = "MRZ Image via BP";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(404, 99);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 28;
            this.label18.Text = "URL WebApi:";
            // 
            // txtURLWebApi
            // 
            this.txtURLWebApi.Location = new System.Drawing.Point(483, 96);
            this.txtURLWebApi.Name = "txtURLWebApi";
            this.txtURLWebApi.Size = new System.Drawing.Size(449, 20);
            this.txtURLWebApi.TabIndex = 27;
            this.txtURLWebApi.Text = "http://nvprod.cloudapp.net/BPWebApiService_v5.8/";
            // 
            // chkWebApiOn
            // 
            this.chkWebApiOn.AutoSize = true;
            this.chkWebApiOn.Location = new System.Drawing.Point(1067, 75);
            this.chkWebApiOn.Name = "chkWebApiOn";
            this.chkWebApiOn.Size = new System.Drawing.Size(125, 17);
            this.chkWebApiOn.TabIndex = 26;
            this.chkWebApiOn.Text = "Consulta via WebApi";
            this.chkWebApiOn.UseVisualStyleBackColor = true;
            // 
            // txtCompanyId
            // 
            this.txtCompanyId.Location = new System.Drawing.Point(85, 28);
            this.txtCompanyId.Name = "txtCompanyId";
            this.txtCompanyId.Size = new System.Drawing.Size(45, 20);
            this.txtCompanyId.TabIndex = 24;
            this.txtCompanyId.Text = "7";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Company Id";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(950, 80);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Foto Nro:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(424, 78);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "URL WS:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(423, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "URL WS:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.picFront);
            this.groupBox3.Controls.Add(this.picFirmaRes);
            this.groupBox3.Controls.Add(this.picFotoRes);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.btnEraseResult);
            this.groupBox3.Controls.Add(this.labScore);
            this.groupBox3.Controls.Add(this.rtbResultado);
            this.groupBox3.Controls.Add(this.labResVerify);
            this.groupBox3.Controls.Add(this.picQuestion);
            this.groupBox3.Controls.Add(this.picNOOK);
            this.groupBox3.Controls.Add(this.picOK);
            this.groupBox3.Location = new System.Drawing.Point(33, 526);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1219, 240);
            this.groupBox3.TabIndex = 154;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Zona de Resultados...";
            // 
            // picFront
            // 
            this.picFront.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFront.Location = new System.Drawing.Point(381, 18);
            this.picFront.Margin = new System.Windows.Forms.Padding(2);
            this.picFront.Name = "picFront";
            this.picFront.Size = new System.Drawing.Size(81, 56);
            this.picFront.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFront.TabIndex = 158;
            this.picFront.TabStop = false;
            // 
            // picFirmaRes
            // 
            this.picFirmaRes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFirmaRes.Location = new System.Drawing.Point(295, 18);
            this.picFirmaRes.Margin = new System.Windows.Forms.Padding(2);
            this.picFirmaRes.Name = "picFirmaRes";
            this.picFirmaRes.Size = new System.Drawing.Size(81, 56);
            this.picFirmaRes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFirmaRes.TabIndex = 157;
            this.picFirmaRes.TabStop = false;
            // 
            // picFotoRes
            // 
            this.picFotoRes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFotoRes.Location = new System.Drawing.Point(238, 18);
            this.picFotoRes.Margin = new System.Windows.Forms.Padding(2);
            this.picFotoRes.Name = "picFotoRes";
            this.picFotoRes.Size = new System.Drawing.Size(53, 56);
            this.picFotoRes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFotoRes.TabIndex = 156;
            this.picFotoRes.TabStop = false;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.DimGray;
            this.label17.Location = new System.Drawing.Point(23, 159);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(454, 54);
            this.label17.TabIndex = 155;
            this.label17.Text = "Valor de Score mas cercano a 0 (cero) indica positivo mas seguro";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(505, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 16);
            this.label1.TabIndex = 148;
            this.label1.Text = "Detalles";
            // 
            // btnEraseResult
            // 
            this.btnEraseResult.Location = new System.Drawing.Point(122, 0);
            this.btnEraseResult.Margin = new System.Windows.Forms.Padding(2);
            this.btnEraseResult.Name = "btnEraseResult";
            this.btnEraseResult.Size = new System.Drawing.Size(112, 26);
            this.btnEraseResult.TabIndex = 147;
            this.btnEraseResult.Text = "&Borrar Resultados";
            this.btnEraseResult.UseVisualStyleBackColor = true;
            this.btnEraseResult.Click += new System.EventHandler(this.btnEraseResult_Click);
            // 
            // labScore
            // 
            this.labScore.AutoSize = true;
            this.labScore.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labScore.ForeColor = System.Drawing.Color.DimGray;
            this.labScore.Location = new System.Drawing.Point(141, 110);
            this.labScore.Name = "labScore";
            this.labScore.Size = new System.Drawing.Size(124, 18);
            this.labScore.TabIndex = 146;
            this.labScore.Text = "Score = 0 / 0";
            this.labScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labScore.Visible = false;
            // 
            // rtbResultado
            // 
            this.rtbResultado.BackColor = System.Drawing.Color.PapayaWhip;
            this.rtbResultado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbResultado.Location = new System.Drawing.Point(495, 35);
            this.rtbResultado.Margin = new System.Windows.Forms.Padding(2);
            this.rtbResultado.Name = "rtbResultado";
            this.rtbResultado.Size = new System.Drawing.Size(697, 176);
            this.rtbResultado.TabIndex = 145;
            this.rtbResultado.Text = "";
            // 
            // labResVerify
            // 
            this.labResVerify.AutoSize = true;
            this.labResVerify.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labResVerify.ForeColor = System.Drawing.Color.Blue;
            this.labResVerify.Location = new System.Drawing.Point(139, 85);
            this.labResVerify.Name = "labResVerify";
            this.labResVerify.Size = new System.Drawing.Size(285, 25);
            this.labResVerify.TabIndex = 144;
            this.labResVerify.Text = "Verifique IDENTIDAD...";
            this.labResVerify.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picQuestion
            // 
            this.picQuestion.Image = global::TestNamkurestApi.Properties.Resources.Question_Shield;
            this.picQuestion.Location = new System.Drawing.Point(42, 57);
            this.picQuestion.Name = "picQuestion";
            this.picQuestion.Size = new System.Drawing.Size(93, 88);
            this.picQuestion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picQuestion.TabIndex = 141;
            this.picQuestion.TabStop = false;
            // 
            // picNOOK
            // 
            this.picNOOK.Image = global::TestNamkurestApi.Properties.Resources.Error_Shield;
            this.picNOOK.Location = new System.Drawing.Point(42, 57);
            this.picNOOK.Name = "picNOOK";
            this.picNOOK.Size = new System.Drawing.Size(93, 88);
            this.picNOOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picNOOK.TabIndex = 143;
            this.picNOOK.TabStop = false;
            this.picNOOK.Visible = false;
            // 
            // picOK
            // 
            this.picOK.Image = global::TestNamkurestApi.Properties.Resources.Good_Shield;
            this.picOK.Location = new System.Drawing.Point(42, 57);
            this.picOK.Name = "picOK";
            this.picOK.Size = new System.Drawing.Size(93, 88);
            this.picOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picOK.TabIndex = 142;
            this.picOK.TabStop = false;
            this.picOK.Visible = false;
            // 
            // btnMindee
            // 
            this.btnMindee.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMindee.Location = new System.Drawing.Point(824, 504);
            this.btnMindee.Margin = new System.Windows.Forms.Padding(2);
            this.btnMindee.Name = "btnMindee";
            this.btnMindee.Size = new System.Drawing.Size(157, 28);
            this.btnMindee.TabIndex = 155;
            this.btnMindee.Text = "OCR via Mindee";
            this.btnMindee.UseVisualStyleBackColor = true;
            this.btnMindee.Click += new System.EventHandler(this.btnMindee_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(663, 504);
            this.button8.Margin = new System.Windows.Forms.Padding(2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(157, 28);
            this.button8.TabIndex = 156;
            this.button8.Text = "QR via ZXing";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(989, 504);
            this.button9.Margin = new System.Windows.Forms.Padding(2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(157, 28);
            this.button9.TabIndex = 159;
            this.button9.Text = "OCR via FoxId";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(1166, 50);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(47, 22);
            this.button10.TabIndex = 166;
            this.button10.Text = "Delete 2";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(430, 50);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(47, 22);
            this.button11.TabIndex = 167;
            this.button11.Text = "Delete 2";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 810);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.btnMindee);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.grpConfig);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.btnEndCapture);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bimetrika Facial Demo v2...";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.grpConfig.ResumeLayout(false);
            this.grpConfig.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFront)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFirmaRes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFotoRes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQuestion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNOOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOK)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtTH;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtValueid;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTypeid;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtFoto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btnEndCapture;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.CheckBox chkF2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.CheckBox chkF1;
        private System.Windows.Forms.Button btnCaptura2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox imageCam;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnCaptura1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox grpConfig;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCompanyId;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEraseResult;
        private System.Windows.Forms.Label labScore;
        private System.Windows.Forms.RichTextBox rtbResultado;
        private System.Windows.Forms.Label labResVerify;
        private System.Windows.Forms.PictureBox picQuestion;
        private System.Windows.Forms.PictureBox picNOOK;
        private System.Windows.Forms.PictureBox picOK;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chkWebApiOn;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtURLWebApi;
        private System.Windows.Forms.TextBox txtFNro;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.PictureBox picFirmaRes;
        private System.Windows.Forms.PictureBox picFotoRes;
        private System.Windows.Forms.TextBox txtConnector;
        private System.Windows.Forms.Button btnPOSController;
        private System.Windows.Forms.TextBox txtApiPOS;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox picFront;
        private System.Windows.Forms.CheckBox chkSaveImages;
        private System.Windows.Forms.Button btnMindee;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button btnLiveness2D;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
    }
}