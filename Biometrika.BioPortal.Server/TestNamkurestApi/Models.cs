﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestNamkurestApi
{
    internal class Models
    {
    }

    //Facial - 09-08-2022
    public class BPResponse
    {
        public int Code { get; set; }       //Codigo de error: 0 - Sin Error | <0 - Codigo de error
        public string Message { get; set; } //Descripcion de error si hubo
        public object Data { get; set; }    //Respuesta Json segun operacion

    }
    public class POSVerifyFacialIn
    {
        public string valueid { get; set; }
        public string clientid { get; set; }
        public string frontcardid { get; set; }
        public string backcardid { get; set; }
        public string selfie { get; set; }
        public double threshold { get; set; }
    }

    public class POSVerifyFacialOutData
    {
        public string trackid { get; set; }
        public int result { get; set; }
        public double score { get; set; }
        public double threshold { get; set; }
        public Bio.Core.Api.PersonalData personaldata { get; set; }
    }
}
