﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestNamkurestApi
{
    internal class MindeeModels
    {
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class ApiRequest
    {
        public Error error { get; set; }
        public List<string> resources { get; set; }
        public string status { get; set; }
        public int status_code { get; set; }
        public string url { get; set; }
    }

    public class Birthdate
    {
        public double confidence { get; set; }
        public List<Value> values { get; set; }
        public int page_id { get; set; }
    }

    public class Document
    {
        public string id { get; set; }
        public Inference inference { get; set; }
        public int n_pages { get; set; }
        public string name { get; set; }
    }

    public class Error
    {
    }

    public class Expirationdate
    {
        public double confidence { get; set; }
        public List<Value> values { get; set; }
        public int page_id { get; set; }
    }

    public class Extras
    {
    }

    public class Inference
    {
        public Extras extras { get; set; }
        public DateTime finished_at { get; set; }
        public bool is_rotation_applied { get; set; }
        public List<Page> pages { get; set; }
        public PredictionMindee prediction { get; set; }
        public double processing_time { get; set; }
        public Product product { get; set; }
        public DateTime started_at { get; set; }
    }

    public class Issuedate
    {
        public double confidence { get; set; }
        public List<Value> values { get; set; }
        public int page_id { get; set; }
    }

    public class Lastname
    {
        public double confidence { get; set; }
        public List<Value> values { get; set; }
        public int page_id { get; set; }
    }

    public class Mrz
    {
        public double confidence { get; set; }
        public List<object> values { get; set; }
        public object page_id { get; set; }
    }

    public class Nacionality
    {
        public double confidence { get; set; }
        public List<Value> values { get; set; }
        public int page_id { get; set; }
    }

    public class Name
    {
        public double confidence { get; set; }
        public List<Value> values { get; set; }
        public int page_id { get; set; }
    }

    public class Orientation
    {
        public int value { get; set; }
    }

    public class Page
    {
        public Extras extras { get; set; }
        public int id { get; set; }
        public Orientation orientation { get; set; }
        public PredictionMindee prediction { get; set; }
    }

    public class PredictionMindee
    {
        public Birthdate birthdate { get; set; }
        public Expirationdate expirationdate { get; set; }
        public Issuedate issuedate { get; set; }
        public Lastname lastname { get; set; }
        public Mrz mrz { get; set; }
        public Nacionality nacionality { get; set; }
        public Name name { get; set; }
        public Profession profession { get; set; }
        public Run run { get; set; }
        public Serial serial { get; set; }
        public Sex sex { get; set; }
        public Visa visa { get; set; }
    }

    public class Product
    {
        public List<string> features { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string version { get; set; }
    }

    public class Profession
    {
        public double confidence { get; set; }
        public List<object> values { get; set; }
        public object page_id { get; set; }
    }

    public class RootMindee
    {
        public ApiRequest api_request { get; set; }
        public Document document { get; set; }
    }

    public class Run
    {
        public double confidence { get; set; }
        public List<Value> values { get; set; }
        public int page_id { get; set; }
    }

    public class Serial
    {
        public double confidence { get; set; }
        public List<Value> values { get; set; }
        public int page_id { get; set; }
    }

    public class Sex
    {
        public double confidence { get; set; }
        public List<Value> values { get; set; }
        public int page_id { get; set; }
    }

    public class Value
    {
        public double confidence { get; set; }
        public string content { get; set; }
        public List<List<double>> polygon { get; set; }
    }

    public class Visa
    {
        public double confidence { get; set; }
        public List<object> values { get; set; }
        public object page_id { get; set; }
    }

    public class ParameterMindee
    {
        public string document { get; set; }
    }
}
