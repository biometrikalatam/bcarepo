﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace TestNamkurestApi
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            double d = 0.1;
            string s = d.ToString("##.##") + "%";
            d = 1.5;
            s = d.ToString("##.##") + "%";
            d = 8.151234;
            s = d.ToString("##.##") + "%";
            d = 10.5;
            s = d.ToString("##.##") + "%";

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form2());
        }
    }
}
