﻿using log4net;
using System;
using System.Web.Services;
using Innovatrics.AnsiIso;
using Bio.Core.Constant;
using BioPortal.Server.Api;
using Bio.Portal.Server.Services.Core.Database;
using Bio.Core.Api;
using Innovatrics.AnsiIso.Enums;
using Bio.Portal.Server.Services.Core.Services;

namespace Bio.Portal.Server.Services
{
    /// <summary>
    /// Summary description for BioPortal_Server_Plugin_Process
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BioPortal_Server_Plugin_Process : System.Web.Services.WebService
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BioPortal_Server_Plugin_Process));

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlparamin"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        [WebMethod]
        public int Process(string xmlparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;

            try
            {

                //0.- Chequeo licencia valida
                if (!Global.LicenseValid(Global.SERVICE_PLUGIN_PROCESS))
                {
                    oXmlOut.Message = Bio.Core.Api.Constant.Errors.SERR_LICENSE;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Bio.Core.Api.Constant.Errors.IERR_LICENSE;
                }

                //1.- Deserializo objeto xmlparamin
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                if (!ServicesPluginManager.IsCorrectParamInForAction(oXmlIn, oXmlIn.Actionid, out msgerr))
                {
                    oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                        msgerr + "]";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //3.- Llamo a ServicesPluginManager.Process con parametros deserializados.
                res = ServicesPluginManager.Process(oXmlIn, out xmlparamout);

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                oXmlOut.Message = Errors.SERR_UNKNOWN;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                LOG.Error("BioPortal_Server_Plugin_Process.Process", ex);
            }

            return res;

            //return ConvertISOToISOCompact(xmlparamin, out xmlparamout);

        }

        //(int companyid, string typeid, string valueid, string serial, string _iso, out string _isoConverted) //Recibe una huella iso en base64
        /// <summary>
        /// Conveirte minucias ISO en ISO Compact
        /// </summary>
        /// <param name="xmlparamin">Parametros de entrada de BioPortal
        ///            En Sample, en CMLParamIN debe venir la minucia ISO
        ///            En AdditionalData se devuelve la minucia ISO Compact si se realizo con exito
        /// </param>
        /// <param name="xmlparamout">Parámetros de Salida de BioPortal</param>
        /// <returns></returns>
        internal int ConvertISOToISOCompact(string xmlparamin, out string xmlparamout) 
        {
            int iret = -1;
            //_isoConverted = null;
            //string msg;
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msg = null;
            DateTime start, end;
            XmlParamIn oXmlIn = null;
            string _isoConverted = null;

            try
            {
                LOG.Info("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact IN...");
                start = DateTime.Now;

                //0.- Chequeo licencia valida
                if (Global.PLUGIN_PROCESS < 1)
                {
                    oXmlOut.Message = "BioPortal_Server_Plugin_Process.ConvertISOToISOCompact Saliendo por licencia inválida [PLUGIN_PROCESS = " +
                        Global.PLUGIN_PROCESS.ToString();
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Info("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact Saliendo por licencia inválida [PLUGIN_PROCESS = " +
                        Global.PLUGIN_PROCESS.ToString());
                    return Errors.IERR_LICENSE;
                }

                //1.- Deserializo objeto xmlparamin
                oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    LOG.Info("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact Error deserializando entrada...");
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }
                               
                //Added 29-04-2016
                //Si debe chequear serial de device, se fija en tabla bp_devices para esa compañia si esá y está habilitado.
                if (Global.CONFIG.CheckClientConnected == 1)
                {
                    //Added 29-04-2016
                    //Para control de cliente via IP y no x Serial ID del lector
                    if (Global.CONFIG.AutoRegisterClientConnected == 2)
                    {
                        oXmlIn.Clientid = oXmlIn.Ipenduser;
                    }
                    LOG.Debug("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact - checking ClientId Autorized [" + oXmlIn.Clientid + "]");
                    if (!ClientAuthorized(oXmlIn, out msg))
                    {
                        msg = "Device no habilitado en la plataforma! [Serial ID = " + oXmlIn.Clientid + " - " +
                                                                       "(Msg=" + msg + ")]";
                        LOG.Warn(msg);
                        oXmlOut.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_CLIENT_NOT_AUTHORIZED;
                    }
                }

                //Chequeo que venga template ISO
                if (oXmlIn.SampleCollection == null || oXmlIn.SampleCollection.Count == 0 || 
                    oXmlIn.SampleCollection[0].Data == null || oXmlIn.SampleCollection[0].Data.Length == 0)
                {
                    oXmlOut.Message = "Sample collection vacio o template informado (Data) nulo";
                    LOG.Info("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact Error Sample collection vacio o template informado (Data) nulo...");
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_NULL_TEMPLATE;
                }

                //Chequeo que venga template ISO
                if (oXmlIn.Actionid != Bio.Core.Matcher.Constant.Action.ACTION_PLUGIN_ISOTOISOCOMPACT)
                {
                    oXmlOut.Message = "Acción indicada no corresponse (Debe ser = " + Bio.Core.Matcher.Constant.Action.ACTION_PLUGIN_ISOTOISOCOMPACT + 
                        " Indicado = " + oXmlIn.Actionid + ")";
                    LOG.Info("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact Error " + oXmlOut.Message);
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_NULL_TEMPLATE;
                }

                LOG.Debug("BioPortal_Server_Plugin_Process.ISOToISOC Param ISO IN = " + oXmlIn.SampleCollection[0].Data);
                Byte[] _isoTemplate = Convert.FromBase64String(oXmlIn.SampleCollection[0].Data);

                LOG.Debug("BioPortal_Server_Plugin_Process.ISOToISOC IEngine initializing...");
                //IEngine.Init(); //Inicialización Innovatrics
                IEngine iEngine = IEngine.Instance;
                Iso iso = Iso.Instance;
                iEngine.Init();
                LOG.Debug("BioPortal_Server_Plugin_Process.ISOToISOC IEngine initializated!");

                byte[] _temp = new Byte[4000];
                Int32 _isoLength = 0, _isocLength = 0;

                LOG.Debug("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact ConvertToISOCardCC IN...");
                // iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
                //LOG.Debug("BioPortal_Server_Plugin_Process.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

                //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

                //LOG.Debug("BioPortal_Server_Plugin_Process.ISOToISOC ConvertToISOCardCC IN 2...");
                byte[] _isocTemplateWrapped = iso.ConvertToISOCardCC(_isoTemplate, 63, IEngineSortOrder.SortNone, IEngineSortOrder.SortXAsc);
                LOG.Debug("BioPortal_Server_Plugin_Process.ISOToISOC ConvertToISOCardCC OUT!");

                iEngine.Terminate(); //Finalización Innovatrics
                LOG.Debug("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact IEngine Terminated!");

                LOG.Debug("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact Starting Reorder Minutiae...");
                //Re-ordenamiento de minucias
                for (Int32 n = 0; n < _isocTemplateWrapped.Length; n++)
                {
                    _temp[n] = _isocTemplateWrapped[n + 28];

                    if (n > 0 && _temp[n - 1] == 0x00)
                    {
                        _isocLength = n;

                        break;
                    }
                }

                _isocLength = _isocLength - 1;

                Byte[] _isocTemplate = new Byte[_isocLength];

                for (Int32 n = 0; n < _isocLength; n++)
                {
                    _isocTemplate[n] = _temp[n];
                }
                LOG.Debug("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact Reorder Minutiae OUT!");

                _isoConverted = BitConverter.ToString(_isocTemplate);
                LOG.Debug("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact _isoConverted = " + _isoConverted);
                end = DateTime.Now;

                ////LLeno Salida
                //oXmlOut.Actionid = oXmlIn.Actionid;
                //oXmlOut.Trackid = Guid.NewGuid().ToString("N"); 
                //oXmlOut.Authenticationfactor = oXmlIn.Authenticationfactor;
                //oXmlOut.Minutiaetype = oXmlIn.Minutiaetype;
                //oXmlOut.Bodypart = oXmlIn.Bodypart;
                //oXmlOut.Clientid = oXmlIn.Clientid;
                //oXmlOut.Ipenduser = oXmlIn.Ipenduser;
                //oXmlOut.Enduser = oXmlIn.Enduser;
                //oXmlOut.Companyid = oXmlIn.Companyid;
                //oXmlOut.Userid = oXmlIn.Userid;
                //oXmlOut.PersonalData = oXmlIn.PersonalData;
                //oXmlOut.Timestampend = DateTime.Now;
                //oXmlOut.Origin = oXmlIn.Origin;
                //DynamicData adata = new DynamicData();
                //adata.AddValue("ISOCompactTemplate", _isoConverted);
                //oXmlOut.Additionaldata = adata;
                //  //Serializo
                //xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                iret = 0;

            }
            catch (Exception ex)
            {
                oXmlOut.Message = "BioPortal_Server_Plugin_Process.ConvertISOToISOCompact Error [" + ex.Message + "]";
                LOG.Error(oXmlOut.Message);
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                return Errors.IERR_UNKNOWN;
            }

            try
            {
                Common.Entities.Database.BpTx bptx;
                iret = DatabaseHelper.SaveTx(iret, 1, oXmlIn, false, start, end, out bptx);

                DynamicData adata = new DynamicData();  
                adata.AddValue("ISOCompactTemplate", _isoConverted);
                oXmlOut.Additionaldata = adata;
                xmlparamout = XmlParamOut.GetXML(oXmlIn.Actionid, bptx.Trackid, oXmlIn.Clientid, oXmlIn.Ipenduser, oXmlIn.Ipenduser,
                    oXmlIn.Companyid, oXmlIn.Userid, end, bptx.BpOrigin.Id, adata); 
            }
            catch (Exception ex)
            {
                oXmlOut.Message = "BioPortal_Server_Plugin_Process.ConvertISOToISOCompact Error [" + ex.Message + "]";
                LOG.Error(oXmlOut.Message);
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                return Errors.IERR_SAVING_TX;
            }

            LOG.Info("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact OUT!");
            return Errors.IERR_OK;
        }


        /// <summary>
        /// Added 29-04-2016 para chequeo de verificación de clientes que se conectan
        /// </summary>
        /// <param name="oparamin"></param>
        /// <returns></returns>
        internal static bool ClientAuthorized(XmlParamIn oparamin, out string msg)
        {
            bool _ret = false;
            msg = "";
            try
            {
                LOG.Debug("ServiceManager.ClientAuthorized - TipoCheck = " + Global.CONFIG.AutoRegisterClientConnected.ToString()
                           + " - oparamin.ClientId = " + oparamin.Clientid);

                _ret = DatabaseHelper.ClientAuthorized(oparamin.Companyid, oparamin.Clientid, out msg);
            }
            catch (Exception ex)
            {
                LOG.Error("ServiceManager.ClientAuthorized Error", ex);
            }
            LOG.Debug("ServiceManager.ClientAuthorized out => " + _ret.ToString());
            return _ret;
        }
    }
}
