﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using Bio.Core.Constant;
using Bio.Core.Matcher.Constant;
using Bio.Portal.Server.Services.Core.Services;
using BioPortal.Server.Api;
using log4net;
using Action = Bio.Core.Matcher.Constant.Action;

namespace Bio.Portal.Server.Services
{
    /// <summary>
    /// Summary description for BioPortal.Server.WS.Web
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class BioPortalServerWSWeb : System.Web.Services.WebService {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(BioPortalServerWSWeb));

#region Verify

        /// <summary>
        /// Recibe los parámetros para hacer la verificacion, y devuelve resultado
        /// de la misma, o mensaje de error si existe.
        /// Los pasos son:
        ///         0.- Chequeo licencia valida
        ///         1.- Deserializo objeto xmlparamin
        ///             1.1.- Chequeo que venga Token, y rearmo oParamIn sino return
        ///         2.- Chequeo que esté toda la información necesaria sino retorno con ERR
        ///         3.- Llamo a ServicesManager.Verify con parametros deserializados.
        ///         4.- Retorno resultado recibido desde ServicesManager.Verify.
        /// </summary>
        /// <param name="xmlparamin">XML con parametros</param>
        /// <param name="xmlparamout">XML con resultado</param>
        /// <returns>Codigo de ejecución</returns>
        [WebMethod]
        public int Verify(string xmlparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;

            try
            {
                //0.- Chequeo licencia valida
                if (!Global.LicenseValid(Global.SERVICE_WSWEB))
                {
                    oXmlOut.Message = Errors.SERR_LICENSE;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_LICENSE;
                }

                //1.- Deserializo objeto xmlparamin
                xmlparamin = xmlparamin.Replace("&#x0;", "");
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //1.1.- Chequeo que venga Token, y rearmo oParamIn sino return 
                string s_copy_typeyid = oXmlIn.PersonalData.Typeid;
                string s_copy_valueid = oXmlIn.PersonalData.Valueid;
                if (ServicesManager.VerifyAndExtractToken(oXmlIn, out oXmlIn, out msgerr) != Errors.IERR_OK)
                {
                    oXmlOut.Message = "Error verificando token biometrico [" +
                        msgerr + "]";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //Added 12-08-2016 - Por error en SML
                //1.2.- Verifico que el valor que viene informado en oXmlIn sea igual al que viene en Token
                //      Sino sale con error
                if (!s_copy_typeyid.Equals(oXmlIn.PersonalData.Typeid) || !s_copy_valueid.Equals(oXmlIn.PersonalData.Valueid)) {
                    oXmlOut.Message = "typeid/valueid informado en parametro difiere al informado en token  [Param = " +
                        s_copy_typeyid + "/" + s_copy_valueid + " - Token=" + oXmlIn.PersonalData.Typeid + "/" + oXmlIn.PersonalData.Valueid + "]";
                    LOG.Warn("BioPortalServerWSWeb.Verify - " + oXmlOut.Message);
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                if (!ServicesManager.IsCorrectParamInForAction(oXmlIn, Action.ACTION_VERIFY, out msgerr))
                {
                    oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                        msgerr + "]";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //3.- Llamo a ServicesManager.Verify con parametros deserializados.
                res = ServicesManager.Verify(oXmlIn, out xmlparamout);

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                oXmlOut.Message = Errors.SERR_UNKNOWN;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                LOG.Error("BioPortalServerWSWeb.Verify", ex);
            }

            return res;
        }

#endregion Verify

#region Identify

        /// <summary>
        /// Recibe los parámetros para hacer la identificacion, y devuelve resultado
        /// de la misma, o mensaje de error si existe.
        /// Los pasos son:
        ///         0.- Chequeo licencia valida
        ///         1.- Deserializo objeto xmlparamin
        ///             1.1.- Chequeo que venga Token, y rearmo oParamIn sino return
        ///         2.- Chequeo que esté toda la información necesaria sino retorno con ERR
        ///         3.- Llamo a ServicesManager.Identify con parametros deserializados.
        ///         4.- Retorno resultado recibido desde ServicesManager.Identify.
        /// </summary>
        /// <param name="xmlparamin">XML con parametros</param>
        /// <param name="xmlparamout">XML con resultado</param>
        /// <returns>Codigo de ejecución</returns>
        [WebMethod]
        public int Identify(string xmlparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;

            try
            {
                //0.- Chequeo licencia valida
                if (!Global.LicenseValid(Global.SERVICE_WSWEB))
                {
                    oXmlOut.Message = Errors.SERR_LICENSE;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_LICENSE;
                }

                //1.- Deserializo objeto xmlparamin
                xmlparamin = xmlparamin.Replace("&#x0;", "");
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //1.1.- Chequeo que venga Token, y rearmo oParamIn sino return
                string s_copy_typeyid = oXmlIn.PersonalData.Typeid;
                string s_copy_valueid = oXmlIn.PersonalData.Valueid;
                if (ServicesManager.VerifyAndExtractToken(oXmlIn, out oXmlIn, out msgerr) != Errors.IERR_OK)
                {
                    oXmlOut.Message = "Error verificando token biometrico [" +
                        msgerr + "]";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //Added 12-08-2016 - Por error en SML
                //1.2.- Verifico que el valor que viene informado en oXmlIn sea igual al que viene en Token
                //      Sino sale con error
                if (!s_copy_typeyid.Equals(oXmlIn.PersonalData.Typeid) || !s_copy_valueid.Equals(oXmlIn.PersonalData.Valueid))
                {
                    oXmlOut.Message = "typeid/valueid informado en parametro difiere al informado en token  [Param = " +
                        s_copy_typeyid + "/" + s_copy_valueid + " - Token=" + oXmlIn.PersonalData.Typeid + "/" + oXmlIn.PersonalData.Valueid + "]";
                    LOG.Warn("BioPortalServerWSWeb.Identify - " + oXmlOut.Message);
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                if (!ServicesManager.IsCorrectParamInForAction(oXmlIn, Action.ACTION_IDENTIFY, out msgerr))
                {
                    oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                        msgerr + "]";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //3.- Llamo a ServicesManager.Verify con parametros deserializados.
                res = ServicesManager.Identify(oXmlIn, out xmlparamout);

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                oXmlOut.Message = Errors.SERR_UNKNOWN;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                LOG.Error("BioPortalServerWSWeb.Identify", ex);
            }

            return res;
        }

#endregion Identify

#region Enroll

        /// <summary>
        /// Recibe los parámetros para hacer el Enroll, y devuelve resultado
        /// de la misma, o mensaje de error si existe.
        /// Los pasos son:
        ///         0.- Chequeo licencia valida
        ///         1.- Deserializo objeto xmlparamin
        ///             1.1.- Chequeo que venga Token, y rearmo oParamIn sino return
        ///         2.- Chequeo que esté toda la información necesaria sino retorno con ERR
        ///         3.- Llamo a ServicesManager.Enroll con parametros deserializados.
        ///         4.- Retorno resultado recibido desde ServicesManager.Enroll.
        /// </summary>
        /// <param name="xmlparamin">XML con parametros</param>
        /// <param name="xmlparamout">XML con resultado</param>
        /// <returns>Codigo de ejecución</returns>
        [WebMethod]
        public int Enroll(string xmlparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;

            try
            {
                //0.- Chequeo licencia valida
                if (!Global.LicenseValid(Global.SERVICE_WSWEB))
                {
                    oXmlOut.Message = Errors.SERR_LICENSE;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_LICENSE;
                }

                //1.- Deserializo objeto xmlparamin
                xmlparamin = xmlparamin.Replace("&#x0;", "");
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //1.1.- Chequeo que venga Token, y rearmo oParamIn sino return
                //      1.2.- Si es Delete no extraigo    
                if (oXmlIn.Actionid != Action.ACTION_DELETE && oXmlIn.Actionid != Action.ACTION_DELETEBIR)
                {
                    if (oXmlIn.SampleCollection != null && oXmlIn.SampleCollection.Count > 0)
                    {
                        string s_copy_typeyid = oXmlIn.PersonalData.Typeid;
                        string s_copy_valueid = oXmlIn.PersonalData.Valueid;
                        if (ServicesManager.VerifyAndExtractToken(oXmlIn, out oXmlIn, out msgerr) != Errors.IERR_OK)
                        {
                            //if (oXmlIn.Actionid != Action.ACTION_MODIFY && oXmlIn.SampleCollection == null)
                            ////Si es modify y no viene data, deja pasar porque puedo modificar otros datos
                            //{
                                oXmlOut.Message = "Error verificando token biometrico [" +
                                                  msgerr + "]";
                                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                return Errors.IERR_BAD_PARAMETER;
                            //}
                        }

                        //Added 12-08-2016 - Por error en SML
                        //1.2.- Verifico que el valor que viene informado en oXmlIn sea igual al que viene en Token
                        //      Sino sale con error
                        if (!s_copy_typeyid.Equals(oXmlIn.PersonalData.Typeid) || !s_copy_valueid.Equals(oXmlIn.PersonalData.Valueid))
                        {
                            oXmlOut.Message = "typeid/valueid informado en parametro difiere al informado en token  [Param = " +
                                s_copy_typeyid + "/" + s_copy_valueid + " - Token=" + oXmlIn.PersonalData.Typeid + "/" + oXmlIn.PersonalData.Valueid + "]";
                            LOG.Warn("BioPortalServerWSWeb.Enroll - " + oXmlOut.Message);
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                            return Errors.IERR_BAD_PARAMETER;
                        }
                    }
                }

                //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                if (!ServicesManager.IsCorrectParamInForAction(oXmlIn, oXmlIn.Actionid, out msgerr))
                {
                        oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                                          msgerr + "]";
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_BAD_PARAMETER;
                }
    
                //3.- Llamo a ServicesManager.Verify con parametros deserializados.
                res = ServicesManager.Enroll(oXmlIn, out xmlparamout);

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                oXmlOut.Message = Errors.SERR_UNKNOWN;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                LOG.Error("BioPortalServerWSWeb.Enroll", ex);
            }

            return res;
        }

#endregion Enroll

    }
}
