﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Bio.Portal.Server.Services.Core.Services;
using BioPortal.Server.Api;
using log4net;
using Bio.Core.Matcher.Constant;
using Action = Bio.Core.Matcher.Constant.Action;
using Bio.Core.Constant;
using Bio.Core.Api;

namespace Bio.Portal.Server.Services
{
    /// <summary>
    /// Summary description for BioPortal_Server_WS_CL
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BioPortal_Server_WS_CL : System.Web.Services.WebService
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BioPortal_Server_WS_CL));

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

#region Verify

        /// <summary>
        /// Recibe los parámetros para hacer la verificacion, y devuelve resultado
        /// de la misma, o mensaje de error si existe.
        /// Los pasos son:
        ///         0.- Chequeo licencia valida
        ///         1.- Deserializo objeto paramin
        ///         2.- Chequeo que esté toda la información necesaria sino retorno con ERR
        ///         3.- Llamo a ServicesManager.Verify con parametros deserializados.
        ///         4.- Retorno resultado recibido desde ServicesManager.Verify.
        /// </summary>
        /// <param name="xmlparamin">parametros en string para que sea mas facil desde los moviles</param>
        /// <param name="xmlparamout">resultado como string con lo basico para PERU por ahora
        ///         Devuelve:
        ///         Con Error: 
        ///                    typeId + "|" + valueId + "|" + msgerr
        ///         Sin Error:
        ///               typeId + "|" + valueId + "|" + oXmlOut.Result + "|" + 
        ///               oXmlOut.PersonalData.Name + "|" + oXmlOut.PersonalData.Patherlastname + "|" + oXmlOut.PersonalData.Motherlastname + "|" +
        ///               oXmlOut.PersonalData.Documentexpirationdate;
        /// </param>
        /// <returns>Codigo de ejecución</returns>
        [WebMethod]
        public int Verify(string paramin, out string paramout)
        {
            paramout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;
            DateTime start, end;
            start = DateTime.Now;
            try
            {
                LOG.Error("BioPortalServerWSCL.Verify In...[Start Time = " + start.ToString("ss/MM/yyyy HH:mm:ss.fff") + "]");
                //0.- Chequeo licencia valida
                if (!Global.LicenseValid(Global.SERVICE_WS))
                {
                    LOG.Error("BioPortalServerWSCL.Verify Error de Licencia");
                    paramout = "DNI|0|" + Errors.SERR_LICENSE;
                    return Errors.IERR_LICENSE;
                }

                //1.- Deserializo objeto paramin
                if (String.IsNullOrEmpty(paramin))
                {
                    LOG.Error("BioPortalServerWSCL.Verify paramin no puede ser vacio");
                    paramout = "DNI|0|" + Errors.SERR_BAD_PARAMETER;
                    return Errors.IERR_BAD_PARAMETER;
                }

                LOG.Debug("BioPortalServerWSCL.Verify paramin=" + paramin);
                string[] oParamIn = paramin.Split('|');
                if (oParamIn.Length != 8)
                {
                    LOG.Error("BioPortalServerWSCL.Verify paramin incompleto length != 8");
                    paramout = "DNI|0|" + Errors.SERR_BAD_PARAMETER;
                    return Errors.IERR_BAD_PARAMETER;
                }


                XmlParamIn oXmlIn = ArmaXmlParamIn(oParamIn); 
                if (oXmlIn == null)
                {
                    LOG.Error("BioPortalServerWSCL.Verify Error armando XmlParamIn");
                    paramout = oParamIn[1] + "|" + oParamIn[2] + "|" + Errors.SERR_DESERIALIZING_DATA;
                    return Errors.IERR_DESERIALIZING_DATA;
                }

                //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                if (!ServicesManager.IsCorrectParamInForAction(oXmlIn, Action.ACTION_VERIFY, out msgerr))
                {
                    LOG.Error("BioPortalServerWSCL.Verify Error IsCorrectParamInForAction - msgerr = " + msgerr);
                    paramout = oParamIn[1] + "|" + oParamIn[2] + "|" + "Error de parametros para la accion solicidata [" + msgerr + "]";
                    return Errors.IERR_BAD_PARAMETER;
                }

                //3.- Llamo a ServicesManager.Verify con parametros deserializados.
                string xmlparamout = null;
                res = ServicesManager.Verify(oXmlIn, out xmlparamout);

                if (res == 0)
                {
                    paramout = ArmaSalidaCL(xmlparamout);
                }
                else
                {
                    oXmlOut = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                    paramout = oParamIn[1] + "|" + oParamIn[2] + "|" + oXmlOut.Message;
                }
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                paramout = Errors.SERR_UNKNOWN;
                LOG.Error("BioPortalServerWSCL.Verify", ex);
            }
            end = DateTime.Now;
            LOG.Error("BioPortalServerWSCL.Verify Out...[End Time = " + end.ToString("ss/MM/yyyy HH:mm:ss.fff") + "]");
            LOG.Error("BioPortalServerWSCL.Verify Tiempo consumido [Total milisegundos = " + (end-start).TotalMilliseconds.ToString() + "]");
            return res;
        }

#endregion Verify

        private string ArmaSalidaCL(string xmlparamout)
        {
            string ret = null;
            try
            {
                LOG.Error("BioPortalServerWSCL.ArmaSalidaCL In - xmlparamout = " + xmlparamout);
                XmlParamOut oXmlOut = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                if (oXmlOut == null)
                {
                    LOG.Error("BioPortalServerWSCL.ArmaSalidaCL Error deserialize oXmlOut == null");
                    ret = null;
                }
                else
                {
                    ret = oXmlOut.PersonalData.Typeid + "|" +  oXmlOut.PersonalData.Valueid + "|" + oXmlOut.Result + "|" + 
                          oXmlOut.PersonalData.Name + "|" + oXmlOut.PersonalData.Patherlastname + "|" + oXmlOut.PersonalData.Motherlastname + "|" +
                          oXmlOut.PersonalData.Documentexpirationdate;
                }

            }
            catch (Exception ex)
            {
                LOG.Error("BioPortalServerWSCL.ArmaSalidaCL", ex);
                ret = null;                
            }
            LOG.Error("BioPortalServerWSCL.ArmaSalidaCL Out"); 
            return ret;
        }

        private XmlParamIn ArmaXmlParamIn(string[] oParamIn)
        {
            /*
                 Pos > 0 - Id company
                 Pos > 1 - Type Id
                 Pos > 2 - Nro Id
                 Pos > 3 - Serial Number Lector
                 Pos > 4 - Template en B64 ANSI Solo
                 Pos > 5 - Operation order
                 Pos > 6 - Origin
                 Pos > 7 - Connector
             */
            XmlParamIn oXmlIn = null;
            try
            {
                LOG.Error("BioPortalServerWSCL.ArmaXmlParamIn In...");
                oXmlIn = new XmlParamIn();
                oXmlIn.Actionid = Action.ACTION_VERIFY;
                oXmlIn.Bodypart = 1;
                oXmlIn.Clientid = oParamIn[3];
                oXmlIn.Companyid = Convert.ToInt32(oParamIn[0]);
                oXmlIn.Enduser = "ClienteLivianoForP";
                oXmlIn.InsertOption = 0;
                oXmlIn.Ipenduser = "127.0.0.1";
                oXmlIn.Matchingtype = 0;
                oXmlIn.Minutiaetype = 13;
                oXmlIn.OperationOrder = Convert.ToInt32(oParamIn[5]);
                oXmlIn.Origin = Convert.ToInt32(oParamIn[6]);
                oXmlIn.PersonalData = new Bio.Core.Api.PersonalData();
                oXmlIn.PersonalData.Typeid = oParamIn[1];
                oXmlIn.PersonalData.Valueid = oParamIn[2];
                oXmlIn.SaveVerified = 0;
                oXmlIn.Threshold = 10000;
                oXmlIn.Userid = 1;
                oXmlIn.Verifybyconnectorid = oParamIn[7];
                oXmlIn.SampleCollection = new List<Bio.Core.Api.Sample>();
                Sample sample;
                sample = new Sample
                {
                    Data = oParamIn[4],
                    Additionaldata = "", //aData[10],
                    Device = 2, //Convert.ToInt32(aData[11]),
                    Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ANSI_INSITS_378_2004
                };
                oXmlIn.SampleCollection.Add(sample);
            }
            catch (Exception ex)
            {
                LOG.Error("BioPortalServerWSCL.ArmaSalidaCL", ex);
                oXmlIn = null;                
            }
            LOG.Error("BioPortalServerWSCL.ArmaSalidaCL Out");
            return oXmlIn;
        }
    }
}
