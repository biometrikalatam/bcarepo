﻿using System.Web;
using System.Web.Mvc;

namespace Bio.Portal.Server.Services
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
