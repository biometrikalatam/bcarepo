﻿using System;
using System.Web.Services;
using Bio.Portal.Server.Common.Config;
using Bio.Core.Constant;
using Bio.Portal.Server.Services.Core.Database;
using log4net;
using Bio.Portal.Server.Common.Common;

namespace Bio.Portal.Server.Services
{
    /// <summary>
    /// Summary description for BioPortal.Server.Admin
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class BioPortalServerAdmin : System.Web.Services.WebService
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BioPortalServerAdmin));

        /// <summary>
        /// Obtener los valores de configuración desde este sitio.
        /// </summary>
        /// <param name="msg">mensaje de retorno</param>
        /// <returns></returns>
        [WebMethod]
        public int GetConfig(out string xmloutput, out string msg)
        {

            int res = 0;
            try
            {
                xmloutput = Bio.Core.Utils.SerializeHelper.SerializeToXml(Global.CONFIG);
                res = Errors.IERR_OK;
                msg = Errors.GetDescription(res);
            }
            catch (Exception exe)
            {
                res = Errors.IERR_UNKNOWN;
                msg = exe.Message;
                xmloutput = "";
                LOG.Error("BioPortalServerAdmin", exe);
            }
            return res;
        }

        /// <summary>
        /// Este método permite actualizar los valores modificados desde el Manager.
        /// </summary>
        /// <param name="parametros">xml del tipo BPConfig</param>
        /// <param name="msg">mensaje de salida en caso de error</param>
        /// <returns>retorna un entero que esta asociado a un error.</returns>
        [WebMethod]
        public int UpdateConfig(string parametros, out string msg)
        {
            msg = "S/E";
            int res = -1;
            BpConfig config;
            try
            {
                config = Bio.Core.Utils.SerializeHelper.DeserializeFromXml<BpConfig>(parametros);

                // si resulta nulo es porque no pudo deserializar correctamente los parámetros.
                if (config == null)
                {
                    LOG.Debug("El archivo de configuración es nulo");
                    res = Errors.IERR_BAD_SERIALIZER_CONFIG_FILE;
                }
                else
                {
                    // 1. Verificamos que el archivo de configuración existe.
                    // 2. Si existe actualizamos los datos.
                    lock (Global.CONFIG)
                    {
                        //2.1Se reemplazan los datos.
                        Global.CONFIG = config;
                        //2.2Se graba archivo.

                        if (Bio.Core.Utils.SerializeHelper.SerializeToFile(Global.CONFIG, Bio.Portal.Server.Services.Properties.Settings.Default.PathConfigFile))
                        {
                            res = Errors.IERR_OK;    
                        } else
                        {
                            res = Errors.IERR_SAVE_CONFIG_FILE;    
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                LOG.Error("BioPortalServerAdmin.UpdateConfig", ex);
                res = Errors.IERR_UNKNOWN;
            }

            return res;
        }

        /// <summary>
        /// Este método permite actualizar los valores asociados a una compañía.
        /// </summary>
        /// <param name="xmlparamin"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        [WebMethod]
        public int UpdateCompanys(string xmlparamin, out string msg)
        {
            int res = Errors.IERR_OK;
            
            try
            {

                LOG.Debug("Updating ArrCompany desde WS Admin.UpdateCompany...");
                msg = Global.LoadCompanies();
                //arrcompanys = Bio.Core.Utils.SerializeHelper.DeserializeFromXml<Companys>(xmlparamin);
                ////Si tenemos resultados, modificamos las variable.
                //if (arrcompanys.companys.Length > 0)
                //{
                //    lock (Global.ArrCompanys)
                //    {
                //        //Traemos el Identificador de la compañía modificada.
                //        company = arrcompanys.getCompany(arrcompanys.companys[0].id);
                //        //Buscamos la compañía asociada.

                //    }
                //}
            }
            catch (Exception exe)
            {
                res = Errors.IERR_UNKNOWN;
                msg = exe.Message;
                LOG.Error("BioPortalServerAdmin.Updatecompany Error", exe);
            }
            return res;
        }

        /// <summary>
        /// Dado el RUT de una compañía, devuelve el id de la misma en BioPortal. 
        /// Retorna 0 si no hay error, sino informa el número de error y la descripcion
        /// en msgerror 
        /// </summary>
        /// <param name="companyLegalId">Id Legal de la empresa (Ej.: en Chile = RUT, Argetnina = CUIT, etc) </param>
        /// <param name="outCompanyId">Id Compañia en Bioportal si existe</param>
        /// <param name="msgError">Mensaje de error si hubiere</param>
        /// <returns></returns>
        [WebMethod]
        public int GetCompanyId(string companyLegalId, out int outCompanyId, out string msgError)
        {
            msgError = "S/E";
            outCompanyId = -1;
            int res = -1;
            BpConfig config;
            try
            {
                if (String.IsNullOrEmpty(companyLegalId))
                {
                    res = Errors.IERR_BAD_PARAMETER;
                    msgError = "El parámetro companyLegalId es incorrecto"; 
                } else
                {
                    res = DatabaseHelper.GetCompanyIdByLegalId(companyLegalId, out outCompanyId, out msgError);
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
                LOG.Error("BioPortalServerAdmin.GetCompanyId", ex);
                res = Errors.IERR_UNKNOWN;
            }

            return res;
        }
    }
}
