﻿using System;
using System.Text;
using System.Web.Security;
using System.Web.Services;
using Bio.Core.Constant;
using Bio.Portal.Server.Services.Core.Services;
using BioPortal.Server.Api;
using Bio.Core.Matcher.Constant;
using log4net;
using Action = Bio.Core.Matcher.Constant.Action;

namespace Bio.Portal.Server.Services
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class BioPortalServerWS : System.Web.Services.WebService
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BioPortalServerWS));

        #region Admin
            
            [WebMethod]
            public int PingService(out string xmlparamout)
            {
                xmlparamout = null;
                return 0;
            }

        #endregion Admin

#region Verify
            
            /// <summary>
            /// Recibe los parámetros para hacer la verificacion, y devuelve resultado
            /// de la misma, o mensaje de error si existe.
            /// Los pasos son:
            ///         0.- Chequeo licencia valida
            ///         1.- Deserializo objeto xmlparamin
            ///         2.- Chequeo que esté toda la información necesaria sino retorno con ERR
            ///         3.- Llamo a ServicesManager.Verify con parametros deserializados.
            ///         4.- Retorno resultado recibido desde ServicesManager.Verify.
            /// </summary>
            /// <param name="xmlparamin">XML con parametros</param>
            /// <param name="xmlparamout">XML con resultado</param>
            /// <returns>Codigo de ejecución</returns>
            [WebMethod]
            public int Verify(string xmlparamin, out string xmlparamout)
            {
                xmlparamout = null;
                XmlParamOut oXmlOut = new XmlParamOut();
                int res = -1;
                string msgerr = null;

                try
                {
                    //0.- Chequeo licencia valida
                    if (!Global.LicenseValid(Global.SERVICE_WS))
                    {
                        oXmlOut.Message = Errors.SERR_LICENSE;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_LICENSE;
                    }
 
                    //1.- Deserializo objeto xmlparamin
                    XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                    if (oXmlIn == null)
                    {
                        oXmlOut.Message = "Error deserializando parametros de entrada";
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_BAD_PARAMETER;
                    }

                    //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                    if (!ServicesManager.IsCorrectParamInForAction(oXmlIn, Action.ACTION_VERIFY, out msgerr))
                    {
                        oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                            msgerr + "]";
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_BAD_PARAMETER;
                    }

                    //2.1 - Debo chequear que es PASSWOD y si es debo encriptar loque viene para que la comparacion sea correcta
                    if (oXmlIn.Authenticationfactor == AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
                    {
                        oXmlIn = EncryptPsw(oXmlIn);
                    }
                    //3.- Llamo a ServicesManager.Verify con parametros deserializados.
                    res = ServicesManager.Verify(oXmlIn, out xmlparamout);

                } catch (Exception ex)
                {
                    res = Errors.IERR_UNKNOWN;
                    oXmlOut.Message = Errors.SERR_UNKNOWN;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Error("BioPortalServerWS.Verify",ex);
                }

                return res;
            }

        private XmlParamIn EncryptPsw(XmlParamIn oXmlIn)
        {
            XmlParamIn aux = oXmlIn;
            try
            {
                foreach (Bio.Core.Api.Sample sample in aux.SampleCollection)
                {
                    if (sample.Minutiaetype == MinutiaeType.MINUTIAETYPE_PASSWORD)
                    {
                        byte[] byaux = Convert.FromBase64String(sample.Data);
                        string dataaux = Encoding.ASCII.GetString(byaux);
                        sample.Data = FormsAuthentication.HashPasswordForStoringInConfigFile(dataaux, "SHA1");
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BioPortalServerWS.EncryptPsw", ex);
            }
            return aux;
        }

#endregion Verify

#region Identify

            /// <summary>
            /// Recibe los parámetros para hacer la identificacion, y devuelve resultado
            /// de la misma, o mensaje de error si existe.
            /// Los pasos son:
            ///         0.- Chequeo licencia valida
            ///         1.- Deserializo objeto xmlparamin
            ///         2.- Chequeo que esté toda la información necesaria sino retorno con ERR
            ///         3.- Llamo a ServicesManager.Identify con parametros deserializados.
            ///         4.- Retorno resultado recibido desde ServicesManager.Identify.
            /// </summary>
            /// <param name="xmlparamin">XML con parametros</param>
            /// <param name="xmlparamout">XML con resultado</param>
            /// <returns>Codigo de ejecución</returns>
            [WebMethod]
            public int Identify(string xmlparamin, out string xmlparamout)
            {
                xmlparamout = null;
                XmlParamOut oXmlOut = new XmlParamOut();
                int res = -1;
                string msgerr = null;

                try
                {
                    //0.- Chequeo licencia valida
                    if (!Global.LicenseValid(Global.SERVICE_WS))
                    {
                        oXmlOut.Message = Errors.SERR_LICENSE;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_LICENSE;
                    }

                    //1.- Deserializo objeto xmlparamin
                    XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                    if (oXmlIn == null)
                    {
                        oXmlOut.Message = "Error deserializando parametros de entrada";
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_BAD_PARAMETER;
                    }

                    //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                    if (!ServicesManager.IsCorrectParamInForAction(oXmlIn, Action.ACTION_IDENTIFY, out msgerr))
                    {
                        oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                            msgerr + "]";
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_BAD_PARAMETER;
                    }

                    //2.1 - Debo chequear que es PASSWOD y si es debo encriptar loque viene para que la comparacion sea correcta
                    if (oXmlIn.Authenticationfactor == AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
                    {
                        oXmlIn = EncryptPsw(oXmlIn);
                    }

                    //3.- Llamo a ServicesManager.Verify con parametros deserializados.
                    res = ServicesManager.Identify(oXmlIn, out xmlparamout);

                }
                catch (Exception ex)
                {
                    res = Errors.IERR_UNKNOWN;
                    oXmlOut.Message = Errors.SERR_UNKNOWN;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Error("BioPortalServerWS.Identify", ex);
                }

                return res;
            }

#endregion Identify

#region Enroll

            /// <summary>
            /// Recibe los parámetros para hacer la operacion de Enroll/Modify/Get, y devuelve resultado
            /// de la misma, o mensaje de error si existe.
            /// Los pasos son:
            ///         0.- Chequeo licencia valida
            ///         1.- Deserializo objeto xmlparamin
            ///         2.- Chequeo que esté toda la información necesaria sino retorno con ERR
            ///         3.- Llamo a ServicesManager.Enroll con parametros deserializados.
            ///         4.- Retorno resultado recibido desde ServicesManager.Enroll.
            /// </summary>
            /// <param name="xmlparamin">XML con parametros</param>
            /// <param name="xmlparamout">XML con resultado</param>
            /// <returns>Codigo de ejecución</returns>
            [WebMethod]
            public int Enroll(string xmlparamin, out string xmlparamout)
            {
                xmlparamout = null;
                XmlParamOut oXmlOut = new XmlParamOut();
                int res = -1;
                string msgerr = null;

                try
                {
                    //0.- Chequeo licencia valida
                    if (!Global.LicenseValid(Global.SERVICE_WS))
                    {
                        oXmlOut.Message = Errors.SERR_LICENSE;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_LICENSE;
                    }

                    //1.- Deserializo objeto xmlparamin
                    XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                    if (oXmlIn == null)
                    {
                        oXmlOut.Message = "Error deserializando parametros de entrada";
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_BAD_PARAMETER;
                    }

                    //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                    if (!ServicesManager.IsCorrectParamInForAction(oXmlIn, oXmlIn.Actionid, out msgerr))
                    {
                        oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                            msgerr + "]";
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_BAD_PARAMETER;
                    }

                    //2.1 - Debo chequear que es PASSWOD y si es debo encriptar loque viene para que la comparacion sea correcta
                    if (oXmlIn.Authenticationfactor == AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
                    {
                        oXmlIn = EncryptPsw(oXmlIn);
                    }

                    //3.- Llamo a ServicesManager.Verify con parametros deserializados.
                    res = ServicesManager.Enroll(oXmlIn, out xmlparamout);

                }
                catch (Exception ex)
                {
                    res = Errors.IERR_UNKNOWN;
                    oXmlOut.Message = Errors.SERR_UNKNOWN;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Error("BioPortalServerWS.Enroll", ex);
                }

                return res;
            }

#endregion Enroll

#region Get
            /// <summary>
            /// Recibe los parámetros para hacer la operacion de Enroll/Modify/Get, y devuelve resultado
            /// de la misma, o mensaje de error si existe.
            /// Los pasos son:
            ///         0.- Chequeo licencia valida
            ///         1.- Deserializo objeto xmlparamin
            ///         2.- Chequeo que esté toda la información necesaria sino retorno con ERR
            ///         3.- Llamo a ServicesManager.Enroll con parametros deserializados.
            ///         4.- Retorno resultado recibido desde ServicesManager.Enroll.
            /// </summary>
            /// <param name="xmlparamin">XML con parametros</param>
            /// <param name="xmlparamout">XML con resultado</param>
            /// <returns>Codigo de ejecución</returns>
            [WebMethod]
            public int Get(string xmlparamin, out string xmlparamout)
            {
                xmlparamout = null;
                XmlParamOut oXmlOut = new XmlParamOut();
                int res = -1;
                string msgerr = null;

                try
                {
                    //0.- Chequeo licencia valida
                    if (!Global.LicenseValid(Global.SERVICE_WS))
                    {
                        oXmlOut.Message = Errors.SERR_LICENSE;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_LICENSE;
                    }

                    //1.- Deserializo objeto xmlparamin
                    XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                    if (oXmlIn == null)
                    {
                        oXmlOut.Message = "Error deserializando parametros de entrada";
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_BAD_PARAMETER;
                    }

                    //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                    if (!ServicesManager.IsCorrectParamInForAction(oXmlIn, oXmlIn.Actionid, out msgerr))
                    {
                        oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                            msgerr + "]";
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_BAD_PARAMETER;
                    }

                    //3.- Llamo a ServicesManager.Get con parametros deserializados.
                    res = ServicesManager.Get(oXmlIn, out xmlparamout);

                }
                catch (Exception ex)
                {
                    res = Errors.IERR_UNKNOWN;
                    oXmlOut.Message = Errors.SERR_UNKNOWN;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Error("BioPortalServerWS.Get", ex);
                }

                return res;
            }

            /// <summary>
            /// Recibe los parámetros para hacer el Get de diferentes variables del sistema (ACTIONS, MINUTIAETYPE, etc)
            /// y devuelve resultado de la misma, o mensaje de error si existe. El resultado es devuelto en xml con 
            /// key/value como resultado
            /// Los pasos son:
            ///         0.- Chequeo licencia valida
            ///         1.- Deserializo objeto xmlparamin
            ///         2.- Chequeo que esté toda la información necesaria sino retorno con ERR
            ///         3.- Llamo a ServicesManager.GetOp con parametros deserializados.
            ///         4.- Retorno resultado recibido desde ServicesManager.GetOp.
            /// </summary>
            /// <param name="xmlparamin">XML con parametros</param>
            /// <param name="xmlparamout">XML con resultado</param>
            /// <returns>Codigo de ejecución</returns>
            [WebMethod]
            public int GetOp(string xmlparamin, out string xmlparamout)
            {
                xmlparamout = null;
                XmlParamOut oXmlOut = new XmlParamOut();
                int res = -1;
                string msgerr = null;

                try
                {
                    //0.- Chequeo licencia valida
                    if (!Global.LicenseValid(Global.SERVICE_WS))
                    {
                        oXmlOut.Message = Errors.SERR_LICENSE;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_LICENSE;
                    }

                    //1.- Deserializo objeto xmlparamin
                    XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                    if (oXmlIn == null)
                    {
                        oXmlOut.Message = "Error deserializando parametros de entrada";
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_BAD_PARAMETER;
                    }

                    //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                    if (!ServicesManager.IsCorrectParamInForAction(oXmlIn, oXmlIn.Actionid, out msgerr))
                    {
                        oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                            msgerr + "]";
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_BAD_PARAMETER;
                    }

                    //3.- Llamo a ServicesManager.Get con parametros deserializados.
                    res = ServicesManager.GetOp(oXmlIn, out xmlparamout);

                }
                catch (Exception ex)
                {
                    res = Errors.IERR_UNKNOWN;
                    oXmlOut.Message = Errors.SERR_UNKNOWN;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Error("BioPortalServerWS.Get", ex);
                }

                return res;
            }
#endregion Get
    }
}