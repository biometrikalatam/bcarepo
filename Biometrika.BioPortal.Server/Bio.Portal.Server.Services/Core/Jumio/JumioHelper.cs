﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bio.Portal.Server.Services.APIRest.Models;
using log4net;
using Biometrika.Jumio.Api;
using Bio.Core.Constant;

namespace Bio.Portal.Server.Services.Core.Jumio
{
    public class JumioHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(JumioHelper));
        private JumioClient _JUMIO_CLIENT;

        #region Properties & Contructor 

        public string BaseURLOnBoarding { get; set; } = "https://netverify.com/api/v4/";
        public string BaseURLAuthentication { get; set; } = "https://netverify.com/api/authentication/v1/web/";
        public string BaseURLGet { get; set; } = "https://netverify.com/api/netverify/v2/scans/";
        public string BaseURLGetAuth { get; set; } = "https://netverify.com/api/netverify/v2/authentications/";
        public string Token { get; set; } = "3197f9ae-9d8a-43a0-9d33-5ac873e09a52";
        public string Secret { get; set; } = "xQY2RccapX85Lru30pLRyrW93UgNiQeO";
        public int Timeout { get; set; } = 30; //Segundos

        public JumioHelper() { }

        public bool Initialization(string _BaseURLOnBoarding, string _BaseURLAuthentication, string _BaseURLGet, string BaseURLGetAuth,
                           string _Token, string _Secret, int _Timeout) {
            BaseURLOnBoarding = string.IsNullOrEmpty(_BaseURLOnBoarding) ? BaseURLOnBoarding : _BaseURLOnBoarding;
            BaseURLAuthentication = string.IsNullOrEmpty(_BaseURLAuthentication) ? BaseURLAuthentication : _BaseURLAuthentication;
            BaseURLGet = string.IsNullOrEmpty(_BaseURLGet) ? BaseURLGet : _BaseURLGet; 
            Token = string.IsNullOrEmpty(_Token)  ? Token : _Token;
            Secret = string.IsNullOrEmpty(_Secret) ? Secret : _Secret;
            Timeout = (_Timeout > 0) ? _Timeout : Timeout;

            _JUMIO_CLIENT = new JumioClient(BaseURLOnBoarding, BaseURLAuthentication, BaseURLGet, BaseURLGetAuth, Timeout, Token, Secret);
            return PingToJumio();
        }


        #endregion  Properties & Contructor

        #region Public 


        internal CreateModelR CrateTx(int typeverify, string new_trackid, string enrollTrackId,  
                                      out string _url3ro)
        {
            InitiateResponse iResponse = null;
            _url3ro = null;
            CreateModelR ret = null;
            try
            {
                if (typeverify == 1) //Es OnBoarding
                {
                    iResponse = _JUMIO_CLIENT.initiateEnrollSync(new_trackid, "BiometrikaLatam", Properties.Settings.Default.Jumio_CallbackUrl);
                }
                else if (typeverify == 0) //Es Authentication
                {
                    iResponse = _JUMIO_CLIENT.initiateAuthSync(enrollTrackId, new_trackid, Properties.Settings.Default.Jumio_CallbackAuthUrl);
                }

                if (iResponse != null)
                {
                    _url3ro = iResponse.RedirectUrl;
                    ret = new CreateModelR(Errors.IERR_OK, null, iResponse.TransactionReference);
                } else
                {
                    ret = new CreateModelR(Errors.IERR_3RO_RESPONSE_NULL, "Service3ro CrateTx [Response Null]", null);
                }
            }
            catch (Exception ex)
            {
                ret = new CreateModelR(Errors.IERR_UNKNOWN, "Service3ro CrateTx [" + ex.Message + "]", null);
                LOG.Error("JumioHelper.CrateTx Error: " + ex.Message);
            }
            return ret;
        }

        internal int GetStatus(int typeverify, string trackid3ro, out string msgerr)
        {
            msgerr = null;
            int ret = 0;
            try
            {
                if (typeverify == 1) //Fue onboarding mandatorio
                {
                    StatusResponse sr = _JUMIO_CLIENT.scansStatusSync(trackid3ro, null);
                    if (sr != null)
                    {
                        if (!string.IsNullOrEmpty(sr.Status) && sr.Status.Equals("PENDING")) ret = 0;
                        if (!string.IsNullOrEmpty(sr.Status) && sr.Status.Equals("DONE")) ret = 1;
                        if (!string.IsNullOrEmpty(sr.Status) && sr.Status.Equals("FAILED")) ret = 2;
                    }
                    else
                    {
                        ret = 0;
                    }
                } else //Es 1 => Fue Auth
                {
                    DataAuthResponse dar = _JUMIO_CLIENT.scansDataAuthSync(trackid3ro, null);
                    if (dar != null)
                    {
                        if (!string.IsNullOrEmpty(dar.transactionResult) && 
                            (dar.transactionResult.Equals("STARTED") || dar.transactionResult.Equals("CREATED"))) ret = 0;
                        if (!string.IsNullOrEmpty(dar.transactionResult) && 
                            dar.transactionResult.Equals("PASSED")) ret = 1;
                        if (!string.IsNullOrEmpty(dar.transactionResult) && 
                            (dar.transactionResult.Equals("FAILED") || dar.transactionResult.Equals("INVALID") ||
                             dar.transactionResult.Equals("EXPIRED"))) ret = 2;
                    }
                    else
                    {
                        ret = 0;
                    }
                }

               
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("JumioHelper.CrateTx Error: " + ex.Message);
            }
            return ret;
        }

        internal DataResponse GetData(string trackid3ro, out string msgerr)
        {
            DataResponse sr = null;
            msgerr = null;
            int ret = 0;
            try
            {

                sr = _JUMIO_CLIENT.scansDataSync(trackid3ro, null);

            }
            catch (Exception ex)
            {
                ret = -1;
                sr = null;
                LOG.Error("JumioHelper.GetData Error: " + ex.Message);
            }
            return sr;
        }

        internal DataAuthResponse GetDataAuth(string trackid3ro, out string msgerr)
        {
            DataAuthResponse sr = null;
            msgerr = null;
            int ret = 0;
            try
            {
                sr = _JUMIO_CLIENT.scansDataAuthSync(trackid3ro, null);
            }
            catch (Exception ex)
            {
                ret = -1;
                sr = null;
                LOG.Error("JumioHelper.GetAuthData Error: " + ex.Message);
            }
            return sr;
        }


        internal ImagesResponse GetImages(string trackid3ro, out string msgerr)
        {
            ImagesResponse ir = null;
            msgerr = null;
            int ret = 0;
            try
            {

                ir = _JUMIO_CLIENT.scansImageSync(trackid3ro, null);

            }
            catch (Exception ex)
            {
                ret = -1;
                ir = null;
                LOG.Error("JumioHelper.GetImages Error: " + ex.Message);
            }
            return ir;
        }

        internal byte[] DownloadImage(string scanreference, string options)
        {
            try
            {
                return _JUMIO_CLIENT.downloadSync(scanreference, options);
            }
            catch (Exception ex)
            {
                LOG.Error("JumioHelper.DownloadImage Error: " + ex.Message);
                return null;
            }
        }

        internal byte[] DownloadImageLiveness(string scanReference, string options)
        {
            try
            {
                return _JUMIO_CLIENT.downloadLivenessImageSync(scanReference, options);
            }
            catch (Exception ex)
            {
                LOG.Error("JumioHelper.DownloadImage Error: " + ex.Message);
                return null;
            }
        }

        internal string DownloadAuthImage(string scanreference, string options)
        {
            try
            {
                byte[] byImg = _JUMIO_CLIENT.downloadAuthSync(scanreference, options);
                return (byImg == null) ? null : Convert.ToBase64String(byImg);
            }
            catch (Exception ex)
            {
                LOG.Error("JumioHelper.DownloadImage Error: " + ex.Message);
                return null;
            }
        }

        #endregion Public 

        #region Private

        private bool PingToJumio()
        {
            bool ping = false;
            try
            {
                ping = true;
            }
            catch (Exception ex)
            {
                ping = false;
                LOG.Error("JumioHelper.PingToJumio Error: " + ex.Message);
            }
            return ping;
        }

        









        #endregion Private
    }
}