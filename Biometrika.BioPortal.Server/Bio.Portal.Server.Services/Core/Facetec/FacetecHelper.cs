﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bio.Portal.Server.Services.APIRest.Models;
using log4net;
using Biometrika.Facetec.Api;
using Bio.Core.Constant;
using Bio.Portal.Server.Common.Entities.Database;
using Newtonsoft.Json;

namespace Bio.Portal.Server.Services.Core.Jumio
{
    public class FacetecHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(FacetecHelper));
        private FacetecClient _FACETEC_CLIENT;

        #region Properties & Contructor 

        public string BaseURL { get; set; } = "https://api.zoomauth.com/api/v2/biometrics/";
        public string LicenseKey { get; set; } = "dMeiNPuIqXC40TIETZ4Vk4j8SEyMj5n2";
        public string LicenseFaceMapEncryptionKey { get; set; } = @"-----BEGIN PUBLIC KEY-----\n\"+
        @"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5PxZ3DLj+zP6T6HFgzzk\n\" +
        @"M77LdzP3fojBoLasw7EfzvLMnJNUlyRb5m8e5QyyJxI+wRjsALHvFgLzGwxM8ehz\n\" +
        @"DqqBZed+f4w33GgQXFZOS4AOvyPbALgCYoLehigLAbbCNTkeY5RDcmmSI/sbp+s6\n\" +
        @"mAiAKKvCdIqe17bltZ/rfEoL3gPKEfLXeN549LTj3XBp0hvG4loQ6eC1E1tRzSkf\n\" +
        @"GJD4GIVvR+j12gXAaftj3ahfYxioBH7F7HQxzmWkwDyn3bqU54eaiB7f0ftsPpWM\n\" +
        @"ceUaqkL2DZUvgN0efEJjnWy5y1/Gkq5GGWCROI9XG/SwXJ30BbVUehTbVcD70+ZF\n\" +
        @"8QIDAQAB\n\" +
        @"-----END PUBLIC KEY-----";
        public int Timeout { get; set; } = 60000; //MiliSegundos
        internal ZoomResponse<ZoomSession> ActiveSession { get; set; }

        public FacetecHelper() { }

        public bool Initialization(string _BaseURL, string _LicenseKey, string _LicenseFaceMapEncryptionKey, int _Timeout) {
            BaseURL = string.IsNullOrEmpty(_BaseURL) ? BaseURL : _BaseURL;
            LicenseKey = string.IsNullOrEmpty(_LicenseKey)  ? LicenseKey : _LicenseKey;
            LicenseFaceMapEncryptionKey = string.IsNullOrEmpty(_LicenseFaceMapEncryptionKey) ?
                                            LicenseFaceMapEncryptionKey : _LicenseFaceMapEncryptionKey;
            Timeout = (_Timeout > 0) ? _Timeout : Timeout;

            _FACETEC_CLIENT = new FacetecClient(BaseURL, LicenseKey, Timeout);

            ActiveSession = _FACETEC_CLIENT.sessionTokenSync(LicenseKey);
            return (_FACETEC_CLIENT != null && ActiveSession!=null &&
                    ActiveSession.Data!=null && !string.IsNullOrEmpty(ActiveSession.Data.SessionToken));
        }


        #endregion  Properties & Contructor

        #region Public 


        internal CreateModelR CrateTx(out string _sessionToken)
        {
            _sessionToken = null;
            CreateModelR ret = null;
            try
            {
                if (ActiveSession == null) _FACETEC_CLIENT.sessionTokenSync(LicenseKey);

                if (ActiveSession != null && ActiveSession.Data != null)
                {
                    _sessionToken = ActiveSession.Data.SessionToken;
                }

                if (_sessionToken != null)
                {
                    ret = new CreateModelR(Errors.IERR_OK, null, null, null);
                }
                else
                {
                    ret = new CreateModelR(Errors.IERR_3RO_RESPONSE_NULL, "Service3ro CrateTx [Response Null]", null);
                }
            }
            catch (Exception ex)
            {
                ret = new CreateModelR(Errors.IERR_UNKNOWN, "Facetec Sesion CrateTx [" + ex.Message + "]", null);
                LOG.Error("FacetecHelper.CrateTx Error: " + ex.Message);
            }
            return ret;
        }

        internal VerifyModelR Verify(BpTx tx, out string jsonResponse3ro , out string continuousLearningFaceMap, out string msgerr)
        {
            msgerr = null;
            VerifyModelR ret = null;
            continuousLearningFaceMap = null;
            jsonResponse3ro = null;
            try
            {
                LOG.Debug("FacetecHelper.Verify IN...");
                if (tx.BpTxConx[0].OnboardingMandatory == 0) //Es verificacion Selfie vs BD
                {
                    LOG.Debug("FacetecHelper.Verify tx.BpTxConx[0].OnboardingMandatory == 0 => Llama a _FACETEC_CLIENT.verify3D3DSync...");
                    ZoomResponse<ZoomVerify3D3D_2D> response = 
                        _FACETEC_CLIENT.verify3D3DSync(Properties.Settings.Default.Facetec_LicenseKey,
                                                    tx.BpTxConx[0].XUserAgent,
                                                    tx.BpTxConx[0].Session,
                                                    tx.BpTxConx[0].Sample3roSource,
                                                    tx.BpTxConx[0].Sample3roTarget,
                                                    tx.BpTxConx[0].EnrollmentIdentifier,
                                                    tx.BpTxConx[0].AuditTrailImage,
                                                    tx.BpTxConx[0].LowQualityAudittrailImage);
                    jsonResponse3ro = JsonConvert.SerializeObject(response);
                    if (response != null && response.Meta != null && (bool)response.Meta["ok"])
                    {
                        LOG.Debug("FacetecHelper.Verify Retorno _FACETEC_CLIENT.verify3D3DSync...");
                        double score = response.Data.matchLevel * 10; // //(double)(new Random()).Next(7500, 9800) / 100; 
                        if (score >= tx.Threshold)
                        {
                            LOG.Debug("FacetecHelper.Verify - Positivo => Score = " + score.ToString());
                            ret = new VerifyModelR(tx.Trackid, "DONE", 1, score, 0, null);
                        } else
                        {
                            LOG.Debug("FacetecHelper.Verify - Negativo => Score = " + score.ToString());
                            ret = new VerifyModelR(tx.Trackid, "DONE", 2, score, 0, null);
                        }
                        continuousLearningFaceMap = response.Data.continuousLearningFaceMap;
                    } else
                    {
                        LOG.Debug("FacetecHelper.Verify Retorno FAILED [msg=" +
                                        (string.IsNullOrEmpty((string)response.Meta["message"]) ? "Null" : (string)response.Meta["message"]));
                        msgerr = (string)response.Meta["code"] + "-" + (string)response.Meta["message"];
                        ret = new VerifyModelR(tx.Trackid, "FAILED", 2, 0, 
                                               Convert.ToInt32(response.Meta["code"]), 
                                               (string)response.Meta["message"]);
                    }
                } else //Es Cedula vs Selfie
                {
                    LOG.Debug("FacetecHelper.Verify tx.BpTxConx[0].OnboardingMandatory == 0 => Llama a _FACETEC_CLIENT.idCheckSync...");
                    ZoomResponse<ZoomIDCheck> response =
                        _FACETEC_CLIENT.idCheckSync(Properties.Settings.Default.Facetec_LicenseKey,
                                                    tx.BpTxConx[0].XUserAgent,
                                                    tx.BpTxConx[0].Session,
                                                    tx.BpTxConx[0].Sample3roSource,
                                                    tx.BpTxConx[0].EnrollmentIdentifier,
                                                    tx.BpTxConx[0].IdScan,
                                                    tx.BpTxConx[0].IdScanFrontImage,
                                                    tx.BpTxConx[0].IdScanBackImage);
                    jsonResponse3ro = JsonConvert.SerializeObject(response);
                    if (response != null && response.Meta != null && (bool)response.Meta["ok"])
                    {
                        LOG.Debug("FacetecHelper.Verify Retorno _FACETEC_CLIENT.idCheckSync...");
                        double score = response.Data.MatchLevel * 10;
                        //(double)(new Random()).Next(7500, 9800) / 100; 
                        if (score >= tx.Threshold)
                        {
                            LOG.Debug("FacetecHelper.Verify - Positivo => Score = " + score.ToString());
                            ret = new VerifyModelR(tx.Trackid, "DONE", 1, score, 0, null);
                        }
                        else
                        {
                            LOG.Debug("FacetecHelper.Verify - Negativo => Score = " + score.ToString());
                            ret = new VerifyModelR(tx.Trackid, "DONE", 2, score, 0, null);
                        }
                    }
                    else
                    {
                        LOG.Debug("FacetecHelper.Verify Retorno FAILED [msg=" +
                                        (string.IsNullOrEmpty((string)response.Meta["message"]) ? "Null" : (string)response.Meta["message"]));
                        msgerr = (string)response.Meta["code"] + "-" + (string)response.Meta["message"];
                        ret = new VerifyModelR(tx.Trackid, "FAILED", 2, 0,
                                               Convert.ToInt32(response.Meta["code"]),
                                               (string)response.Meta["message"]);
                    }

                }
            }
            catch (Exception ex)
            {
                ret = new VerifyModelR(tx.Trackid, "FAILED", 2, 0, Errors.IERR_UNKNOWN,
                                       "Exception [" + ex.Message + "]");
                LOG.Error("FacetecHelper.Verify Error: " + ex.Message);
            }
            LOG.Debug("FacetecHelper.Verify OUT!");
            return ret;
        }

        internal int Verify3D2D(BpTx tx, out bool canEnroll, out string jsonResponse3ro, out string msgerr)
        {
            msgerr = null;
            canEnroll = false;
            int ret = 0;
            //continuousLearningFaceMap = null;
            jsonResponse3ro = null;
            try
            {
                LOG.Debug("FacetecHelper.Verify3D2D IN...Varificando: " + tx.Companyidtx.ToString() + "_" + tx.Typeid + "_" + tx.Valueid);
                LOG.Debug("FacetecHelper.Verify3D2D Ingresando a verify3D2DSync...");
                ZoomResponse <ZoomVerify3D3D_2D> response =
                            _FACETEC_CLIENT.verify3D2DSync(Properties.Settings.Default.Facetec_LicenseKey,
                                                            tx.BpTxConx[0].XUserAgent,
                                                            tx.BpTxConx[0].Session,
                                                            tx.BpTxConx[0].Sample3roSource,
                                                            tx.BpTxConx[0].Sample3ro,
                                                            tx.Companyidtx.ToString() + "_" + tx.Typeid + "_" + tx.Valueid,
                                                            tx.BpTxConx[0].AuditTrailImage,
                                                            tx.BpTxConx[0].LowQualityAudittrailImage);
                LOG.Debug("FacetecHelper.Verify3D2D Salio de verify3D2DSync => response != null => " + (response!=null).ToString());
                jsonResponse3ro = JsonConvert.SerializeObject(response);
                LOG.Debug("FacetecHelper.Verify3D2D - Serializo response => jsonResponse3ro!=null => " + (jsonResponse3ro!=null).ToString());
                if (response != null && response.Meta != null && (bool)response.Meta["ok"])
                {
                    LOG.Debug("FacetecHelper.Verify3D2D - Positivo => chequeo contra threshold...");
                    double score = response.Data.matchLevel * 10; // //(double)(new Random()).Next(7500, 9800) / 100; 
                    LOG.Debug("FacetecHelper.Verify3D2D - score=" + score.ToString() + "/Threshold=" + tx.Threshold);
                    if (score >= tx.Threshold)
                    {
                        LOG.Debug("FacetecHelper.Verify3D2D - Retorono OK...");
                        canEnroll = true;
                        ret = Errors.IERR_OK; 
                    }
                    else
                    {
                        LOG.Debug("FacetecHelper.Verify3D2D - Retorono NO Match => " + Errors.IERR_NO_MATCH_ENROLL_COMBINED.ToString());
                        ret = Errors.IERR_NO_MATCH_ENROLL_COMBINED;
                    }
                    //continuousLearningFaceMap = response.Data.continuousLearningFaceMap;
                }
                else
                {
                    msgerr = (string)response.Meta["code"] + "-" + (string)response.Meta["message"];
                    LOG.Debug("FacetecHelper.Verify3D2D - Retorono Error en proceso de match => " +
                                                Errors.IERR_ERROR_IN_MATCH_ENROLL_COMBINED.ToString());
                    ret = Errors.IERR_ERROR_IN_MATCH_ENROLL_COMBINED;
                }
            }
            catch (Exception ex)
            {
                canEnroll = false;
                ret = -1;
                LOG.Error("FacetecHelper.Verify3D2D Error: " + ex.Message);
            }
            LOG.Debug("FacetecHelper.Verify3D2D OUT!");
            return ret;
        }

        //internal int GetStatus(int typeverify, string trackid3ro, out string msgerr)
        //{
        //    msgerr = null;
        //    int ret = 0;
        //    try
        //    {
        //        if (typeverify == 1) //Fue onboarding mandatorio
        //        {
        //            StatusResponse sr = _JUMIO_CLIENT.scansStatusSync(trackid3ro, null);
        //            if (sr != null)
        //            {
        //                if (!string.IsNullOrEmpty(sr.Status) && sr.Status.Equals("PENDING")) ret = 0;
        //                if (!string.IsNullOrEmpty(sr.Status) && sr.Status.Equals("DONE")) ret = 1;
        //                if (!string.IsNullOrEmpty(sr.Status) && sr.Status.Equals("FAILED")) ret = 2;
        //            }
        //            else
        //            {
        //                ret = 0;
        //            }
        //        } else //Es 1 => Fue Auth
        //        {
        //            DataAuthResponse dar = _JUMIO_CLIENT.scansDataAuthSync(trackid3ro, null);
        //            if (dar != null)
        //            {
        //                if (!string.IsNullOrEmpty(dar.transactionResult) && 
        //                    (dar.transactionResult.Equals("STARTED") || dar.transactionResult.Equals("CREATED"))) ret = 0;
        //                if (!string.IsNullOrEmpty(dar.transactionResult) && 
        //                    dar.transactionResult.Equals("PASSED")) ret = 1;
        //                if (!string.IsNullOrEmpty(dar.transactionResult) && 
        //                    (dar.transactionResult.Equals("FAILED") || dar.transactionResult.Equals("INVALID") ||
        //                     dar.transactionResult.Equals("EXPIRED"))) ret = 2;
        //            }
        //            else
        //            {
        //                ret = 0;
        //            }
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        ret = -1;
        //        LOG.Error("JumioHelper.CrateTx Error: " + ex.Message);
        //    }
        //    return ret;
        //}

        //internal DataResponse GetData(string trackid3ro, out string msgerr)
        //{
        //    DataResponse sr = null;
        //    msgerr = null;
        //    int ret = 0;
        //    try
        //    {

        //        sr = _JUMIO_CLIENT.scansDataSync(trackid3ro, null);

        //    }
        //    catch (Exception ex)
        //    {
        //        ret = -1;
        //        sr = null;
        //        LOG.Error("JumioHelper.GetData Error: " + ex.Message);
        //    }
        //    return sr;
        //}

        //internal DataAuthResponse GetDataAuth(string trackid3ro, out string msgerr)
        //{
        //    DataAuthResponse sr = null;
        //    msgerr = null;
        //    int ret = 0;
        //    try
        //    {
        //        sr = _JUMIO_CLIENT.scansDataAuthSync(trackid3ro, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = -1;
        //        sr = null;
        //        LOG.Error("JumioHelper.GetAuthData Error: " + ex.Message);
        //    }
        //    return sr;
        //}


        //internal ImagesResponse GetImages(string trackid3ro, out string msgerr)
        //{
        //    ImagesResponse ir = null;
        //    msgerr = null;
        //    int ret = 0;
        //    try
        //    {

        //        ir = _JUMIO_CLIENT.scansImageSync(trackid3ro, null);

        //    }
        //    catch (Exception ex)
        //    {
        //        ret = -1;
        //        ir = null;
        //        LOG.Error("JumioHelper.GetImages Error: " + ex.Message);
        //    }
        //    return ir;
        //}

        //internal byte[] DownloadImage(string scanreference, string options)
        //{
        //    try
        //    {
        //        return _JUMIO_CLIENT.downloadSync(scanreference, options);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("JumioHelper.DownloadImage Error: " + ex.Message);
        //        return null;
        //    }
        //}

        //internal byte[] DownloadImageLiveness(string scanReference, string options)
        //{
        //    try
        //    {
        //        return _JUMIO_CLIENT.downloadLivenessImageSync(scanReference, options);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("JumioHelper.DownloadImage Error: " + ex.Message);
        //        return null;
        //    }
        //}

        //internal string DownloadAuthImage(string scanreference, string options)
        //{
        //    try
        //    {
        //        byte[] byImg = _JUMIO_CLIENT.downloadAuthSync(scanreference, options);
        //        return (byImg == null) ? null : Convert.ToBase64String(byImg);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("JumioHelper.DownloadImage Error: " + ex.Message);
        //        return null;
        //    }
        //}

        #endregion Public 

        #region Private

        private bool PingToFacetec()
        {
            bool ping = false;
            try
            {
                ping = true;
            }
            catch (Exception ex)
            {
                ping = false;
                LOG.Error("FacetecHelper.PingToFacetec Error: " + ex.Message);
            }
            return ping;
        }

        



        #endregion Private
    }
}