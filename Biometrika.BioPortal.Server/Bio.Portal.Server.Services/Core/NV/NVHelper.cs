﻿using Bio.Core.Api.Constant;
using Bio.Portal.Server.Services.NVWS;
using log4net;
using System;
using Bio.Digital.Receipt.Api;
using Bio.Portal.Server.Services.Core.Database;

namespace Bio.Portal.Server.Services.Core.NV
{
    /// <summary>
    /// 
    /// </summary>
    public class NVHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NVHelper));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idtx"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="xmlabs"></param>
        /// <param name="xmlRDenvelope"></param>
        /// <returns></returns>
        internal static int Notarize(string idtx, string typeid, string valueid, string xmlabs, out string xmlRDenvelope)
        {
            int ret = Bio.Core.Api.Constant.Errors.IERR_OK;
            xmlRDenvelope = null;
            RdExtensionItem item;
            string xmlparamin;
            string xmlparamout;
            XmlParamOut oParamout;
            try
            {
                XmlParamIn oParamin = new XmlParamIn();
                oParamin.Actionid = 1;
                oParamin.ApplicationId = Properties.Settings.Default.NV_IDApp;
                oParamin.Description = "Biometric Advanced Signature Notarize";
                oParamin.Origin = 2; //Biometrika BioPortal
                oParamin.Extensiones = new RdExtensionItem[5];

                item = new RdExtensionItem();
                item.key = "TxID";
                item.value = idtx;
                oParamin.Extensiones[0] = item;
                item = new RdExtensionItem();
                item.key = "TypeID";
                item.value = typeid;
                oParamin.Extensiones[1] = item;
                item = new RdExtensionItem();
                item.key = "ValueID";
                item.value = valueid;
                oParamin.Extensiones[2] = item;
                item = new RdExtensionItem();
                item.key = "Date";
                item.value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                oParamin.Extensiones[3] = item;
                item = new RdExtensionItem();
                item.key = "ABS";
                item.value = xmlabs;        // "<BioSignature id=\"IDREF.v1\"><TrackID>id1129182</TrackID><Timestamp>31-01-2015 19:10:13.297</Timestamp><Source>SRCeI</Source><Score>13200</Score><ImageSample>/6DakjaksabKJHDKJGKJDGKG</ImageSample><Barcode></Barcode><Photografy></Photografy><DynamicData></DynamicData><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\" /><Reference URI=\"#IDREF.v1\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\" /></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\" /><DigestValue>FTOHSerLZEz16qcTgBQ3SNtXnno=</DigestValue></Reference></SignedInfo><SignatureValue>d4u+YKSuXzCqIqKPtrVPYh0FF+NS4t8+eJ07u3ErRJaCocQrVUKHoU7Mm9WjaYYxoqlCJ+9IqJtp1P8rfyCf0ZJpCpDqb7DpK6lnaLtG+z8ok7o5CSbVCleoNDkfcCfXqPOndp8PwnSjsT0cgFTk/DalsG4bsth6ZrmXv8U2f8E=</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>t6VZ7kkNFK7lTzHx9kTkexNhnC7MDVc+phMNG28Ye7Geet4cLhhZ6uqJPaBtgv6wFwbXfV4XlS95lOXjEgUSm+izc4qpgrhYC6OfZJG1bpok4UP7wLH80gjWn9b7mXWsMEUQiwMVQlhdFMpgE3JeIZRyFEpBs4OBOaGMmmG8QIM=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue><X509Data><X509Certificate>MIIGBTCCBO2gAwIBAgIKK4HM0wAAAADIeDANBgkqhkiG9w0BAQUFADCB0jELMAkGA1UEBhMCQ0wxHTAbBgNVBAgTFFJlZ2lvbiBNZXRyb3BvbGl0YW5hMREwDwYDVQQHEwhTYW50aWFnbzEUMBIGA1UEChMLRS1DRVJUQ0hJTEUxIDAeBgNVBAsTF0F1dG9yaWRhZCBDZXJ0aWZpY2Fkb3JhMTAwLgYDVQQDEydFLUNFUlRDSElMRSBDQSBGSVJNQSBFTEVDVFJPTklDQSBTSU1QTEUxJzAlBgkqhkiG9w0BCQEWGHNjbGllbnRlc0BlLWNlcnRjaGlsZS5jbDAeFw0xMzA3MjkyMTA1MTBaFw0xNjA3MjgyMTA1MTBaMIHEMQswCQYDVQQGEwJDTDEjMCEGA1UECBMaTUVUUk9QT0xJVEFOQSBERSBTQU5USUFHTyAxETAPBgNVBAcTCFNhbnRpYWdvMSkwJwYDVQQKEyBCQ1IgVEVDTk9MT0dJQSBFIElOTk9WQUNJT04gUy5BLjEKMAgGA1UECwwBKjEhMB8GA1UEAxMYR3VzdGF2byBHZXJhcmRvICBTdWhpdCAuMSMwIQYJKoZIhvcNAQkBFhRnc3VoaXRAYmlvbWV0cmlrYS5jbDCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAt6VZ7kkNFK7lTzHx9kTkexNhnC7MDVc+phMNG28Ye7Geet4cLhhZ6uqJPaBtgv6wFwbXfV4XlS95lOXjEgUSm+izc4qpgrhYC6OfZJG1bpok4UP7wLH80gjWn9b7mXWsMEUQiwMVQlhdFMpgE3JeIZRyFEpBs4OBOaGMmmG8QIMCAwEAAaOCAmswggJnMD0GCSsGAQQBgjcVBwQwMC4GJisGAQQBgjcVCILcgy+Fk4xmhdWdF4Li3CeB7+w8YYHLnhSGqIJYAgFkAgEDMB0GA1UdDgQWBBSA5w018UhLsrsObq8J0cfPWzVhWDALBgNVHQ8EBAMCBPAwHwYDVR0jBBgwFoAUeOE+n9ISs3o8jc0wDlOzQykHs1UwPgYDVR0fBDcwNTAzoDGgL4YtaHR0cDovL2NybC5lLWNlcnRjaGlsZS5jbC9lY2VydGNoaWxlY2FGRVMuY3JsMCMGA1UdEQQcMBqgGAYIKwYBBAHBAQGgDBYKMjEyODQ0MTUtMjAjBgNVHRIEHDAaoBgGCCsGAQQBwQECoAwWCjk2OTI4MTgwLTUwggFNBgNVHSAEggFEMIIBQDCCATwGCCsGAQQBw1IFMIIBLjAtBggrBgEFBQcCARYhaHR0cDovL3d3dy5lLWNlcnRjaGlsZS5jbC9DUFMuaHRtMIH8BggrBgEFBQcCAjCB7x6B7ABDAGUAcgB0AGkAZgBpAGMAYQBkAG8AIABGAGkAcgBtAGEAIABTAGkAbQBwAGwAZQAuACAASABhACAAcwBpAGQAbwAgAHYAYQBsAGkAZABhAGQAbwAgAGUAbgAgAGYAbwByAG0AYQAgAHAAcgBlAHMAZQBuAGMAaQBhAGwALAAgAHEAdQBlAGQAYQBuAGQAbwAgAGgAYQBiAGkAbABpAHQAYQBkAG8AIABlAGwAIABDAGUAcgB0AGkAZgBpAGMAYQBkAG8AIABwAGEAcgBhACAAdQBzAG8AIAB0AHIAaQBiAHUAdABhAHIAaQBvMA0GCSqGSIb3DQEBBQUAA4IBAQBF2HPkNASvnFBt+Acko6B411BPvsEeJPyvg50HNkI5e18pRLV5efeN50mTGh9CFElRvbjNlIDH8VscM/jZ36G/i/GIsa/urIEZP42J+v/0iTlLmI7PJqKDR1f5HQWBY4FviK6jbNf8Jm3GV8l8O74gBTyyDqxQXJRkwGcbsU8LUZMVfy+XR+AL6lZVJ9SzSglIyUNuL/h6jgTB1Z+3kpfUz7t5FtkI2VYkhugaZR7tfwD8K7xjnRnDnLQQLr5kAsj743+B/ljMhakplCnUy7vIx0MnNbE04Y9WDjvSuKN7C2JllAtSBKUNSEFHFGvmq/YcRad/yCI5Fl9HXTK688GT</X509Certificate></X509Data></KeyInfo></Signature></BioSignature>";
                oParamin.Extensiones[4] = item;
                xmlparamin = XmlUtils.SerializeAnObject(oParamin);

                bool _yaNotarizado = false;
                if (Properties.Settings.Default.NVType == 0) //es online
                {
                    using (NVWS.WSBDR NV = new NVWS.WSBDR())
                    {
                        NV.Url = Properties.Settings.Default.NVURL;
                        NV.Timeout = Properties.Settings.Default.NVTimeout;
                        int rdRet = NV.Process(xmlparamin, out xmlparamout);

                        if (rdRet == 0)
                        {
                            oParamout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                            xmlRDenvelope = oParamout.Receipt;
                            _yaNotarizado = true; //Si notarizo bien => corto cadena de proceso
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(xmlparamout))
                            {
                                oParamout = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                                LOG.Error("NVHelper.Notarize Error - " + "de NV [" + rdRet + " - " + oParamout.Message + "]");
                            }
                            else
                            {
                                LOG.Error("NVHelper.Notarize Error de NV [" + rdRet + "]");
                            }

                        }
                        ret = rdRet;
                    }
                }


                //Si no se notarizo o la notarizacion es async, se guarda el abs sin notarizar y se graba el id de la tx apra notarizar luego. 
                if (Properties.Settings.Default.NVType == 1 ||                          //es solo Async (via agente)
                    (!_yaNotarizado &&  Properties.Settings.Default.NVType == 2))    //es primero OnLine luego OffLine y no logro notarizar antes
                {
                    try
                    {
                        ////Grabo XML para enviar a NV por agente de Async
                        //System.IO.StreamWriter sw =
                        //        new System.IO.StreamWriter(Properties.Settings.Default.NVAsyncPath + idtx + ".xml");
                        //sw.Write(xmlparamin);
                        //sw.Close();
                        ////Grabo semaforo para procesar con agente Async
                        //sw = new System.IO.StreamWriter(Properties.Settings.Default.NVAsyncPath + idtx + ".xml.sem");
                        //sw.Write("To Process idtx = " + idtx);
                        //sw.Close();
                        xmlRDenvelope = xmlabs;
                        int res = DatabaseHelper.SaveIdToNotarizeAsync(idtx);
                        if (res != 0)
                        {
                            LOG.Error("NVHelpe.Notarize Error Salvando en BD for Noytary Idtx = " + idtx);
                            ret = -4;
                        }
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("NVHelpe.Notarize Error Salvando for Noytary Idtx = " + idtx + " [" + ex.Message + "]");
                        ret = -4;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Bio.Core.Api.Constant.Errors.IERR_UNKNOWN;
                LOG.Error("NVHelper.Notarize Error - " + ex.Message);
            }

            return ret;
        }

    }
}