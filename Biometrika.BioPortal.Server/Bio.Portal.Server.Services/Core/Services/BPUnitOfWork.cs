﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Bio.Portal.Server.Services.APIRest.Models;
using System.Web.Http;
using Bio.Portal.Server.Common.Common;
using log4net;
using Bio.Portal.Server.Common.Entities;
using Bio.Portal.Server.Common.Entities.Database;
using System.Collections;
using Bio.Core.Constant;
using Biometrika.Jumio.Api;
using BioPortal.Server.Api;
using Bio.Core.Api;
using RestSharp;
using Newtonsoft.Json;
using System.Net.Http;
using Biometrika.Regula.WebAPI.Api;
using Bio.Portal.Server.Services.Core.Database;
using BioPortal.Server.Api.Model;

namespace Bio.Portal.Server.Services.Core.Services
{
    public class BPUnitOfWork : ApiController
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BPUnitOfWork));


        internal static bool Authorized(HttpRequestMessage request, out BpCompany company, out string msg)
        {
            bool ret = false;
            msg = null;
            company = null;
            try
            {
                LOG.Debug("BPUnitOfWork.Authorized IN...");
                IEnumerable<string> values = new List<string>();
                if (request.Headers.TryGetValues("Authorization", out values))
                {
                    foreach (string item in values)
                    {
                        if (CredentialOk(item, out company, out msg))
                        {
                            ret = true;
                            break;
                        }
                    }
                } else
                {
                    LOG.Warn("BPUnitOfWork.Authorized => No recibio Authentication en el Header!");
                    msg = "No recibio Authentication en el Header";
                }

            }
            catch (Exception ex)
            {
                ret = false;
                msg = "BPUnitOfWork.Authorized Excp: " + ex.Message;
                LOG.Error("BPUnitOfWork.Authorized Excp: " + ex.Message);
            }
            return ret;
        }

        private static bool CredentialOk(string encryptcredential, out BpCompany company, out string msg)
        {
            bool ret = false;
            company = null;
            msg = "";
            try
            {
                string[] arrS = encryptcredential.Split(' ');
                string plaincredential = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(arrS[1]));

                string[] arrCredential = plaincredential.Split(':');

                string msg1;
                company = AdminBpCompany.RetrieveAuthCredential(arrCredential[0], out msg1);

                if (company != null)
                {
                    if (company.Status == 1)
                    { //Si está deshabilitado
                        msg = "La compañia esta inhabilitada para hacer consultas";
                        if (company.Endate.HasValue)
                        {
                            msg = msg + " desde el " + company.Endate.Value.ToString("dd/MM/yyyy");
                        }
                        LOG.Debug("BPUnitOfWork.CredentialOk - Acceso NOOK de Username = " + arrCredential[0] + 
                                     "[" + msg + "]") ;
                    } else
                    {
                        if (company.SecretKey.Equals(arrCredential[1].Trim()))
                        {
                            LOG.Debug("BPUnitOfWork.CredentialOk - Acceso OK de Username = " + arrCredential[0]); 
                            ret = true;
                        } else
                        {
                            LOG.Debug("BPUnitOfWork.CredentialOk - Acceso NOOK de Username = " + arrCredential[0] +
                                     "[No coinciden SecretKeys => Param=" + arrCredential[1].Trim() + " <> BD=" +
                                     company.SecretKey + "]");
                        } 
                    }
                } else
                {
                    LOG.Warn("BPUnitOfWork.CredentialOk - Username no encontrado => " + arrCredential[0] + "[msg=" + msg + "]");
                }
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("BPUnitOfWork.CredentialOk Excp: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Added 16-08-2021 para chequeo de verificación de clientes que se conectan via POSController
        /// </summary>
        /// <param name="oparamin"></param>
        /// <returns></returns>
        internal static int ClientAuthorized(int companyid, string clientid, out string msg)
        {
            int _ret = Errors.IERR_OK;
            msg = "";
            try
            {
                LOG.Debug("BPUnitOfWork.ClientAuthorized IN - TipoCheck = " + 
                           Global.CONFIG.AutoRegisterClientConnected.ToString()
                           + " - CompanyId = " + companyid.ToString() + " - ClientId = " + clientid);

                bool bret = DatabaseHelper.ClientAuthorized(companyid, clientid, out msg);

                if (bret) _ret = Errors.IERR_OK;
                else
                {
                    if (string.IsNullOrEmpty(msg))
                    {
                        if (msg.Contains("Deshabilitado"))
                        {
                            _ret = Errors.IERR_CLIENT_NOT_AUTHORIZED;
                        } else
                        {
                            _ret = Errors.IERR_UNKNOWN;
                        }
                    } else
                    {
                        _ret = Errors.IERR_UNKNOWN;
                        msg = "Error desconocido intentando chequear Client = " + clientid;
                    }
                }
            }
            catch (Exception ex)
            {
                _ret = Errors.IERR_UNKNOWN;
                msg = "Error desconocido intentando chequear Client = " + clientid + " [" + ex.Message + "]";
                LOG.Error("BPUnitOfWork.ClientAuthorized Error", ex);
            }
            LOG.Debug("BPUnitOfWork.ClientAuthorized out => " + _ret.ToString());
            return _ret;
        }

        /// <summary>
        /// Verifica que si viene CompanyId != al seteado en settings, lo cambia por ese.
        /// Esto es para arreglar por ahor aun bug del componente Android de multicaja. 
        /// </summary>
        /// <param name="_Company"></param>
        /// <param name="CompanyIdReplace"></param>
        /// <param name="_CompanyOut"></param>
        /// <returns></returns>
        internal static bool AuthorizedPOSFix(BpCompany _Company, int CompanyIdReplace, out BpCompany _CompanyOut)
        {
            bool ret = false;
            _CompanyOut = null;
            try
            {
                if (_Company != null && _Company.Id != CompanyIdReplace)
                {
                    LOG.Debug("BPUnitOfWork.AuthorizedPOSFix - Cambiando _Company.Id = " + _Company.Id.ToString() + " por Id = " +
                                    CompanyIdReplace.ToString() + "...");
                    _Company = null;
                    string msg = null;
                    _CompanyOut = AdminBpCompany.GetCompanyById(CompanyIdReplace, out msg);

                    if (_CompanyOut == null || _CompanyOut.Status == 1)
                    {
                        LOG.Debug("BPUnitOfWork.AuthorizedPOSFix - Acceso NOOK de _Company.Id = " + CompanyIdReplace.ToString() +
                                         "[" + msg + "] - _Company <> null => " + (_Company != null).ToString());
                        if (_CompanyOut != null && _CompanyOut.Endate.HasValue)
                        {
                            msg = "La compañia esta inhabilitada para hacer consultas desde el " + _CompanyOut.Endate.Value.ToString("dd/MM/yyyy");
                        }
                        ret = false;
                    }
                    else
                    {
                        ret = true;
                    }
                }
                else
                {
                    _CompanyOut = _Company;
                    ret = true;
                }
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("BPUnitOfWork.AuthorizedPOSFix Excp Error: " + ex.Message);
            }
            return ret;

        }

        public static object VerifiyInit(object param)
        {
            return null;
            //string trackId = null;
            //string msgerr = null;
            //VerifyInitResponseModel response = null;
            //try
            //{
            //    LOG.Debug("NotarizeController.InitProcedureOne - IN...");
            //    //1) Deserializo parámetros     
            //    if (param == null)
            //    {
            //        return Req uest.CreateResponse(HttpStatusCode.BadRequest,
            //        (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null)));
            //    }
            //    string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
            //    LOG.Debug("NotarizeController.InitProcedureOne - Deserializo ParamIn => " + strIn);
            //    InitProcedureOneRequestModel paramIn = JsonConvert.DeserializeObject<InitProcedureOneRequestModel>(strIn);

            //    if (paramIn == null)
            //    {
            //        LOG.Debug("NotarizeController.InitProcedureOne - Error deserializando parametros de entrada [Null]");
            //        return Request.CreateResponse(HttpStatusCode.BadRequest,
            //        (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST, "Error deserializando parametros de entrada [Null]", null)));
            //    }
            //    else
            //    {
            //        string msgRet = "";
            //        if (!IsCorrectDocument(paramIn.document)) //String.IsNullOrEmpty(paramIn.document))
            //        {
            //            msgRet += (string.IsNullOrEmpty(msgRet) ? "Documento Nulo o de formato incorrecto. Debe ser PDF!" :
            //                "|Documento Nulo o de formato incorrecto. Debe ser Word o PDF!");
            //        }
            //        if (!IsCorrectMail(paramIn.mail)) //String.IsNullOrEmpty(paramIn.mail))
            //        {
            //            msgRet += (string.IsNullOrEmpty(msgRet) ? "Mail Nulo o Mal Formado" : "|Mail Nulo o Mal Formado"); ;
            //        }
            //        if (!IsCorrectRut(paramIn.rut)) // (String.IsNullOrEmpty(paramIn.rut))
            //        {
            //            msgRet += (string.IsNullOrEmpty(msgRet) ? "Rut Nulo o Incorrecto" : "|Ru Nulo o Incorrecto");
            //        }
            //        if (!string.IsNullOrEmpty(msgRet))
            //        {
            //            return Request.CreateResponse(HttpStatusCode.BadRequest,
            //                    (new NotarizeResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
            //                        "Problemas para notarizar. [" + msgRet + "]", null)));
            //        }
            //    }

            //    LOG.Debug("NotarizeController.InitProcedureOne - Inicio procedure para mail = " + paramIn.mail);
            //    //1.1) Chequeo si existe, sino lo creo en NvUser
            //    NvUsers _NvUser = AdministradorNvUsers.CheckOrCreateNvUser(paramIn.mail, paramIn.rut);
            //    if (_NvUser == null)
            //    {
            //        LOG.Error("NotarizeController.InitProcedureOne - Imposible registrar mail...");
            //        return Request.CreateResponse(HttpStatusCode.Conflict,
            //            (new NotarizeResponseModel(Errors.IRET_API_CONFLIC, "Problemas para registrar mail. Reintente...", trackId)));
            //    }

            //    //2) Get QR de CI para retornar
            //    string sQRCI;
            //    string sTrackIdCI = GetQRTokenCI(_NvUser.Rut, _NvUser.Mail, out sQRCI);
            //    if (string.IsNullOrEmpty(sQRCI) || string.IsNullOrEmpty(sTrackIdCI))
            //    {
            //        LOG.Error("NotarizeController.InitProcedureOne - Error creando TraciIdCI o QR para CI");
            //        return Request.CreateResponse(HttpStatusCode.Conflict,
            //            (new NotarizeResponseModel(Errors.IRET_API_ERR_INIT_CI, "Error creando TraciIdCI o QR para CI", null)));
            //    }

            //    //2) Genero TraciId Unico de trámite
            //    trackId = AdministradorNvUsersProcedures.GetTrackIdUnique();
            //    if (string.IsNullOrEmpty(trackId))
            //    {
            //        LOG.Error("NotarizeController.InitProcedureOne - TrackId generado invalido [Null]");
            //        return Request.CreateResponse(HttpStatusCode.Conflict,
            //            (new NotarizeResponseModel(Errors.IRET_API_CONFLIC, "Problemas para crear id de transaccion unico. Reintente...",
            //            null)));
            //    }

            //    //3) Grabo NvUsersProcedures con status = 0 => Recibido
            //    NvUsersProcedures NvUsersProceduresCreated;
            //    //string msgerr;
            //    int provideridout;

            //    int ret = AdministradorNvUsersProcedures.CreateNvUserProcedure(trackId, _NvUser.Id, paramIn.notaryId, paramIn.procedureId,
            //                                                  paramIn.document, paramIn.message, sTrackIdCI, sQRCI,
            //                                                  out provideridout, out msgerr, out NvUsersProceduresCreated);

            //    if (ret < 0 || NvUsersProceduresCreated == null)
            //    {
            //        LOG.Error("NotarizeController.InitProcedureOne - Error creando procedure para iniciar proceso [" + msgerr + "]");
            //        return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
            //            (new NotarizeResponseModel(Errors.IRET_ERR_NVPROCEDURE_NO_SAVED, "Error creando procedure para iniciar proceso[" + msgerr + "]",
            //            null)));
            //    }

            //    //Si todo funciono ok => Genro respuesta para retornar.
            //    response = new InitNotarizeResponseModel(Errors.IRET_API_OK, null, trackId, sTrackIdCI, null);

            //    //Informo x mail
            //    string[] strbody = new string[2];
            //    strbody[0] = "Usted a iniciado el trámite <b>" + NvUsersProceduresCreated.NameProcedure + "</b>";
            //    strbody[1] = "El codigo unico del tramite para consultas es: <b>" + NvUsersProceduresCreated.TrackId + "</b>";
            //    libs.Utils.NVNotify(paramIn.mail, "Inicio de tramite", strbody, null, null, null);

            //}
            //catch (Exception ex)
            //{
            //    LOG.Error("NotarizeController.InitProcedureOne Error Iniciando trámite: " + ex.Message);
            //    return Request.CreateResponse(HttpStatusCode.InternalServerError,
            //        (new NotarizeResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconcido procesando inicio de tramite ["
            //                                      + ex.Message + "]", null)));
            //}

            ////return  JsonConvert.SerializeObject
            ////(new NotarizeResponseModel(0, "Proceso de notarizacion iniciado!"));
            //return Request.CreateResponse(HttpStatusCode.OK, response);
        }

       


        //Documentacion en https://docs.google.com/spreadsheets/d/1wu6b9mD4Aid3TguNULWyt26Y03D0ufaADj2BWqyPzlY/edit#gid=767171387
        /// <summary>
        /// Chequea la logica de onboarding y/o autenticacion, para armar luego el wrokflow para BPWeb  
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="company"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="type"></param>
        /// <param name="trackid3roEnroll"></param>
        /// <returns></returns>
        internal static int CheckLogicalOnboardingAuth(int company, string typeid, string valueid, TypeVerify typeonboarding,
                                                        TypeVerify typeauth, OnboardingType onboardingtypeparam,
                                                        out OnboardingType obtype, out string trackid3roEnroll)
        {
            int iRet = 0;
            string msgerr;
            trackid3roEnroll = null;
            obtype = OnboardingType.single;
            try
            {
                LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth IN....");
                BpIdentity identity = null;
                LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - Recupera Identity company/typeid/valueid = "
                    + company + "/" + typeid + "/" + valueid);
                iRet = AdminBpIdentity.Retrieve(company, typeid, valueid, out identity, out msgerr, false);
                LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - iRet = " + iRet);
                if (iRet == 0 && identity != null)
                {
                    if (identity.Documentexpirationdate.HasValue && identity.Documentexpirationdate.Value < DateTime.Now)
                    {
                        LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - identity.Documentexpirationdate.Value = " +
                                identity.Documentexpirationdate.Value.ToString("dd/MM/yyyy") +
                                " => Sale para enroll porque fecha venc vencida...");
                        //Si la fecha de expiracion del documento es menor que el dia en que se hace esta verificacion
                        //fuerza a hacer un nuevo onboarding para renovar la informacion de la persona

                        ////Cehqueo si piden cosas inconsistentes => Retorno Facetec OB + Jumio Auth x ej
                        //if (typeonboarding == TypeVerify.tv3d && 
                        //    (typeauth == TypeVerify.tv3dplus || typeauth == TypeVerify.tv3dstrong)) {
                        //    LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - Sale por enrolamiento no soportado (Ft/J) => " +
                        //        "typeonboarding=" + typeonboarding.ToString() + "/typeauth=" + typeauth.ToString());
                        //    return Errors.IERR_NO_ENROLLTYPE_ENABLED;
                        //}

                        ////Si es todo facetec o todo jumio => Coloco Single
                        //    if ((typeonboarding == TypeVerify.tv3d && typeauth == TypeVerify.tv3d) ||
                        //    ((typeonboarding == TypeVerify.tv3dplus || typeonboarding == TypeVerify.tv3dstrong) &&
                        //      typeauth == TypeVerify.tv3dplus || typeauth == TypeVerify.tv3dstrong))
                        //{
                        //    LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - Setea OnboardingType.single => " +
                        //        "typeonboarding=" + typeonboarding.ToString() + "/typeauth=" + typeauth.ToString());
                        //    obtype = OnboardingType.single;
                        //}

                        ////Si es Jumio en OB y Auth Facetec => Coloco Combined
                        //if ((typeonboarding == TypeVerify.tv3dplus || typeonboarding == TypeVerify.tv3dstrong) &&
                        //    typeauth == TypeVerify.tv3d)
                        //{
                        //    LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - Setea OnboardingType.single => " +
                        //        "typeonboarding=" + typeonboarding.ToString() + "/typeauth=" + typeauth.ToString());
                        //    obtype = onboardingtype; //Coloco el OnboardingType pedido porque ambos son factibles
                        //}
                        //LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth Retorna 1 para forzar onboarding...");
                        return SetOBandOBType(typeonboarding, typeauth, onboardingtypeparam, out obtype); // 1;
                    }

                    //Added para soporte a 2D
                    if (typeauth == TypeVerify.tv2d) {
                        LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - Recuperando Bir 2d para ident.Id = " + identity.Id);
                        BpBir bir = AdminBpBir.RetrieveLastBir(identity.Companyidenroll.Id, identity.Typeid, identity.Valueid, 
                                                          Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL,
                                                          Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_FACIAL_NAMKU,
                                                          Bio.Core.Matcher.Constant.BodyPart.BODYPART_FACE);
                        LOG.Debug("BPUnitOfWork.IsIdentitiyEnrolled - AdminBpBir.RetrieveLastBir bir!=null => " + (bir!=null).ToString());
                        if (bir != null)
                        {
                            trackid3roEnroll = bir.BpIdentity.Id.ToString();
                            iRet = 0; //Esto indica que está enrolado para retornar
                            LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - Enrolado iRet = 0 =>  trackid3roEnroll = " + trackid3roEnroll);
                        }
                        else
                        {
                            LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - Indica que debe enrolar => iRet = 1...");
                            //Esto indica que NO está enrolado para retornar y hacer onboarding obligatorio
                            iRet = SetOBandOBType(typeonboarding, typeauth, onboardingtypeparam, out obtype);
                        }
                    } else
                    {
                        //Debo chequear ahora que si no está enrolado para lo que piden hacer Auth => Debe setear Enroll en ese Type
                        BpIdentity3ro oBpIdentity3ro = null;
                        LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - Recuperando AdminBpIdentity3ro ident.Id = " + identity.Id);
                        iRet = AdminBpIdentity3ro.Retrieve(identity.Id, (int)typeauth, out oBpIdentity3ro);
                        LOG.Debug("BPUnitOfWork.IsIdentitiyEnrolled - iRet = " + iRet);
                        if (iRet == 0 && oBpIdentity3ro != null && !string.IsNullOrEmpty(oBpIdentity3ro.TrackId3ro))
                        {
                            trackid3roEnroll = oBpIdentity3ro.TrackId3ro;
                            iRet = 0; //Esto indica que está enrolado para retornar
                            LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - Enrolado iRet = 0 =>  trackid3roEnroll = " + trackid3roEnroll);
                        }
                        else
                        {
                            LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - Indica que debe enrolar => iRet = 1...");
                            //Esto indica que NO está enrolado para retornar y hacer onboarding obligatorio
                            iRet = SetOBandOBType(typeonboarding, typeauth, onboardingtypeparam, out obtype);
                        }
                    }
                }
                else if (iRet == -31) //Es Identity_NOT_FOUND => Debe enrolar
                {
                    LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth - Persona no exist => Enrolar...iRet = 1");
                    //Fuerza OnBoarding para enrolar
                    iRet = SetOBandOBType(typeonboarding, typeauth, onboardingtypeparam, out obtype); 
                }
            }
            catch (Exception ex)
            {
                iRet = -1;
                LOG.Error("BPUnitOfWork.CheckLogicalOnboardingAuth Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.CheckLogicalOnboardingAuth OUT!");
            return iRet;
        }

        /// <summary>
        /// Dado los parametros de entrada y definiciones realizadas, se devuelve si como se debe enrolar, 
        /// con que type, y si es solo en esa type o comibnado.
        /// </summary>
        /// <param name="typeonboarding"></param>
        /// <param name="typeauth"></param>
        /// <param name="obtypeparam"></param>
        /// <param name="obtype"></param>
        /// <returns></returns>
        internal static int SetOBandOBType(TypeVerify typeonboarding, TypeVerify typeauth, OnboardingType obtypeparam,
                                          out OnboardingType obtype)
        {
            obtype = OnboardingType.single;
            try
            {
                //Cehqueo si piden cosas inconsistentes => Retorno Facetec OB + Jumio Auth x ej
                if ((typeonboarding == TypeVerify.tv3d || typeonboarding == TypeVerify.tv2d) &&
                    (typeauth == TypeVerify.tv3dplus || typeauth == TypeVerify.tv3dstrong))
                {
                    LOG.Debug("BPUnitOfWork.SetOBandOBType - Sale por enrolamiento no soportado (Ft/J) => " +
                        "typeonboarding=" + typeonboarding.ToString() + "/typeauth=" + typeauth.ToString());
                    return Errors.IERR_NO_ENROLLTYPE_ENABLED;
                }

                //Si es todo facetec o todo jumio => Coloco Single
                if ((typeonboarding == TypeVerify.tv3d && typeauth == TypeVerify.tv3d) ||
                ((typeonboarding == TypeVerify.tv3dplus || typeonboarding == TypeVerify.tv3dstrong) &&
                  typeauth == TypeVerify.tv3dplus || typeauth == TypeVerify.tv3dstrong) || 
                  (typeonboarding == TypeVerify.tv2d && typeauth == TypeVerify.tv2d))
                {
                    LOG.Debug("BPUnitOfWork.SetOBandOBType - Setea OnboardingType.single => " +
                        "typeonboarding=" + typeonboarding.ToString() + "/typeauth=" + typeauth.ToString());
                    obtype = OnboardingType.single;
                }

                //Si es Jumio en OB y Auth Facetec => Coloco Combined
                if ((typeonboarding == TypeVerify.tv3dplus || typeonboarding == TypeVerify.tv3dstrong) &&
                    (typeauth == TypeVerify.tv3d || typeauth == TypeVerify.tv2d))
                {
                    LOG.Debug("BPUnitOfWork.SetOBandOBType - Setea OnboardingType.single => " +
                        "typeonboarding=" + typeonboarding.ToString() + "/typeauth=" + typeauth.ToString());
                    obtype = obtypeparam; //Coloco el OnboardingType pedido porque ambos son factibles
                }
                LOG.Debug("BPUnitOfWork.SetOBandOBType Retorna 1 para forzar onboarding...");
                return 1;
            }
            catch (Exception ex)
            {
                LOG.Error("BPUnitOfWork.SetOBandOBType Error: " + ex.Message);
                return -1;
            }
        }

      

        /// <summary>
        /// Verifica si la identidad (company,typeid,valueid) está enrolada o no.
        /// Si lo está, devuelve el id para poder usarlo en Authenticacion en servicio de 3ro.
        /// </summary>
        /// <param name="company"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="type"></param>
        /// <param name="trackid3roEnroll"></param>
        /// <returns></returns>
        internal static int IsIdentitiyEnrolled(int company, string typeid, string valueid, TypeVerify typeonboarding,
                                                TypeVerify typeauth, OnboardingType onboardingtype,
                                                out OnboardingType obtype, out string trackid3roEnroll)
        {
            int iRet = 0;
            string msgerr;
            trackid3roEnroll = null;
            obtype = OnboardingType.single;
            try
            {
                LOG.Debug("BPUnitOfWork.IsIdentitiyEnrolled IN....");
                BpIdentity identity = null;
                LOG.Debug("BPUnitOfWork.IsIdentitiyEnrolled - Recupera Identity company/typeid/valueid = " 
                    + company + "/" + typeid + "/" + valueid);
                iRet = AdminBpIdentity.Retrieve(company, typeid, valueid, out identity, out msgerr, false);
                LOG.Debug("BPUnitOfWork.IsIdentitiyEnrolled - iRet = " + iRet);
                if (iRet == 0 && identity != null) {
                    if (identity.Documentexpirationdate.HasValue && identity.Documentexpirationdate.Value < DateTime.Now)
                    {
                        LOG.Debug("BPUnitOfWork.IsIdentitiyEnrolled - identity.Documentexpirationdate.Value = " +
                                identity.Documentexpirationdate.Value.ToString("dd/MM/yyyy") + 
                                " => Sale para enroll porque fecha venc vencida...");
                        //Si la fecha de expiracion del documento es menor que el dia en que se hace esta verificacion
                        //fuerza a hacer un nuevo onboarding para renovar la informacion de la persona
                        return 1;  
                    }

                    //Debo chequear ahora que si no está enrolado para lo que piden hacer Auth => Debe setear Enroll en ese Type
                    BpIdentity3ro oBpIdentity3ro = null;
                    LOG.Debug("BPUnitOfWork.IsIdentitiyEnrolled - Recuperando AdminBpIdentity3ro ident.Id = " + identity.Id);  
                    iRet = AdminBpIdentity3ro.Retrieve(identity.Id, (int)typeauth, out oBpIdentity3ro);
                    LOG.Debug("BPUnitOfWork.IsIdentitiyEnrolled - iRet = " + iRet);
                    if (iRet == 0 && oBpIdentity3ro != null && !string.IsNullOrEmpty(oBpIdentity3ro.TrackId3ro))
                    { 
                        trackid3roEnroll = oBpIdentity3ro.TrackId3ro;
                        iRet = 0; //Esto indica que está enrolado para retornar
                        LOG.Debug("BPUnitOfWork.IsIdentitiyEnrolled - Enrolado iRet = 0 =>  trackid3roEnroll = " + trackid3roEnroll);
                    } else 
                    {
                        LOG.Debug("BPUnitOfWork.IsIdentitiyEnrolled - Indica que debe enrolar => iRet = 1...");
                        iRet = 1; //Esto indica que NO está enrolado para retornar y hacer onboarding obligatorio
                    } 
                } else if (iRet == -31) //Es Identity_NOT_FOUND => Debe enrolar
                {
                    LOG.Debug("BPUnitOfWork.IsIdentitiyEnrolled - Persona no exist => Enrolar...iRet = 1");
                    iRet = 1; //Fuerza OnBoarding para enrolar
                }
            }
            catch (Exception ex)
            {
                iRet = -1;
                LOG.Error("BPUnitOfWork.IsIdentitiyEnrolled Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.IsIdentitiyEnrolled OUT!");
            return iRet;
        }

        internal static HttpStatusCode CheckUserEnrolled(UserEnrolledModel param, out UserEnrolledModelR ueResponse, out string msgerr)
        {
            ueResponse = null;
            msgerr = null;
            HttpStatusCode ret = HttpStatusCode.OK;
            BpIdentity ident;
            ueResponse = new UserEnrolledModelR();
            try
            {
                LOG.Debug("BPUnitOfWork.CheckUserEnrolled IN...[company/typeid/valueid = " +
                            param.companyid + "/" + param.typeid + "/" + param.valueid);
                int retI = AdminBpIdentity.Retrieve(param.companyid, param.typeid, param.valueid, out ident, out msgerr, false);
                if (ret < 0)
                {
                    LOG.Debug("BPUnitOfWork.CheckUserEnrolled - Error recuperando identity => ret = " + ret);
                    if (retI == Errors.IERR_IDENTITY_NOT_FOUND)
                    {
                        ret = HttpStatusCode.NotFound;
                    } else
                    {
                        ret = HttpStatusCode.InternalServerError;
                    }
                    ueResponse.code = retI;
                    LOG.Debug("BPUnitOfWork.CheckUserEnrolled - OUT!");
                    return ret;
                }
                //List<BpIdentity3ro>
                IList listBpIdentity3ro = null;
                retI = 0;
                retI = AdminBpIdentity3ro.Retrieve(ident, out listBpIdentity3ro);

                if (retI == 0 && listBpIdentity3ro != null)
                {
                    LOG.Debug("BPUnitOfWork.CheckUserEnrolled - Entra a llenar salida con todo ok..."); 
                    ueResponse.code = 0;
                    ueResponse.exist = 1;
                    foreach (BpIdentity3ro item in listBpIdentity3ro)
                    {
                        if (item.Type3roService == (int)TypeVerify.tv2d) ueResponse.isenrolledtv2d = 1;
                        if (item.Type3roService == (int)TypeVerify.tv3d) ueResponse.isenrolledtv3d = 1;
                        if (item.Type3roService == (int)TypeVerify.tv3dplus ||
                            item.Type3roService == (int)TypeVerify.tv3dstrong) ueResponse.isenrolledtv3dplus = 1;
                    }
                } else
                {
                    if (retI == Errors.IERR_IDENTITY_NOT_FOUND || retI == Errors.IERR_BIR_NOT_FOUND)
                    {
                        ret = HttpStatusCode.NotFound;
                    } else
                    {
                        ret = HttpStatusCode.InternalServerError;
                    }
                    ueResponse.code = retI;
                }
            }
            catch (Exception ex)
            {
                ret = HttpStatusCode.InternalServerError;
                LOG.Error("BPUnitOfWork.CheckUserEnrolled Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.CheckUserEnrolled - OUT!");
            return ret;
        }


        /// <summary>
        /// Esta función arma el workflow paso a paso que debe usar BPWeb para la captura de evidencias, 
        /// de acuerdo con los parametros enviados y estado de enrolamientos si se requiere.
        /// </summary>
        /// <param name="paramIn"></param>
        /// <param name="jsonWorkflow"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int CreaWorkflow(CreateModel paramIn, TypeVerify tv, int onboardingmandatory, 
                                         string session, string url3ro, OnboardingType obtype,
                                         out string jsonWorkflow, out string msgerr)
        {
            int ret = 0;
            jsonWorkflow = null;
            msgerr = null;
            List<Step> workflow = new List<Step>();
            Step step;
            bool existIdcardIncluded = false;
            try
            {
                LOG.Debug("BPUnitOfWork.CreaWorkflow IN...");
                //1.- Autorization
                if (paramIn.autorizationinclude == 1)
                {
                    LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding autorization...");
                    step = new Step(StepName.autorization.ToString());
                    //if (!string.IsNullOrEmpty(paramIn.autorizationmessage))
                    //{
                        step.metadata = new Dictionary<string, string>();
                        string auxMessageAut = (paramIn.autorizationinclude == 1) ?
                                       ((string.IsNullOrEmpty(paramIn.autorizationmessage) ?
                                       Global._DISCLAIMER_AUTORIZATION_BPWEB :
                                       paramIn.autorizationmessage)) : null;
                        step.metadata.Add("messagetodisplay", auxMessageAut);
                    //}
                    workflow.Add(step);
                }

                //2.- geolocalization
                LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding geolocation...");
                if (paramIn.georefmandatory == 1)
                {
                    step = new Step(StepName.geolocation.ToString(), 0);
                    LOG.Debug("BPUnitOfWork.CreaWorkflow - geolocation Mandatory...");
                }
                else
                {
                    step = new Step(StepName.geolocation.ToString(), 1); //Pongo Opcional en Activo para que no bloquee el proceso
                    LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding geolocation Opcional...");
                }
                workflow.Add(step);

                //3.-Signature manual
                if (paramIn.signerinclude == 1)
                {
                    LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding signaturemanual...");
                    step = new Step(StepName.signaturemanual.ToString());
                    workflow.Add(step);
                }

                //4.- Jumio y/o Facetec con sabores
                if (tv == TypeVerify.tv3dplus || tv == TypeVerify.tv3dstrong) //Si es Facetec
                {
                    LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding jumio...");
                    //Agrego primero Jumio 
                    step = new Step(StepName.jumio.ToString());
                    step.metadata = new Dictionary<string, string>();
                    step.metadata.Add("url3ro", url3ro);
                    workflow.Add(step);
                    //Si es onboarding y combinado => Agrego liveness Facetec
                    if (onboardingmandatory == 1 && obtype == OnboardingType.combined && 
                        paramIn.typeverifyauth == TypeVerify.tv3d) //Es combinado con Facetec
                    {
                        LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding facetec x onboarding combined...");
                        step = new Step(StepName.facetec.ToString());
                        step.metadata = new Dictionary<string, string>();
                        step.metadata.Add("type", "authentication");
                        step.metadata.Add("licensefacetec", Properties.Settings.Default.Facetec_LicenseKey);
                        step.metadata.Add("licenseforexncrypt", Properties.Settings.Default.Facetec_LicenseFacemapEncriptionKey);
                        step.metadata.Add("session", session);
                        workflow.Add(step);
                    } else if (onboardingmandatory == 1 && obtype == OnboardingType.combined &&
                        paramIn.typeverifyauth == TypeVerify.tv2d) //Es combinado con Namku
                    {
                        LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding selfie 2D x onboarding combined...");
                        step = new Step(StepName.selfie.ToString());
                        step.metadata = new Dictionary<string, string>();
                        //step.metadata.Add("??", "??");
                        workflow.Add(step);
                    }
                } else if (tv == TypeVerify.tv3d) //Si es Facetec
                {
                    LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding facetec...");
                    step = new Step(StepName.facetec.ToString());
                    step.metadata = new Dictionary<string, string>();
                    if (onboardingmandatory == 1) //Es Cedula vs Selfie
                    {
                        LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding factec onboarding metadata...");
                        step.metadata.Add("type", "onboarding");
                    } else  //Es authentication
                    {
                        LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding factec authentication metadata...");
                        step.metadata.Add("type", "authentication");
                    }
                    step.metadata.Add("licensefacetec", Properties.Settings.Default.Facetec_LicenseKey);
                    step.metadata.Add("licenseforexncrypt", Properties.Settings.Default.Facetec_LicenseFacemapEncriptionKey);
                    step.metadata.Add("session", session);
                    workflow.Add(step);
                }
                else if (tv == TypeVerify.tv2d) //Si es Namku
                {
                    LOG.Debug("BPUnitOfWork.CreaWorkflow - Adding 2D...");
                    if (onboardingmandatory == 1)
                    {
                        LOG.Debug("BPUnitOfWork.CreaWorkflow - Add idcard por es onboarding mandatory");
                        step = new Step(StepName.idcard.ToString());
                        step.metadata = new Dictionary<string, string>();
                        //step.metadata.Add("??", "??");
                        workflow.Add(step);
                        existIdcardIncluded = true; //Pongo flag por si se setio usar idcard. Si tv2d no es onboarding luego lo agrega igual
                    }
                    LOG.Debug("BPUnitOfWork.CreaWorkflow - Add selfie2d...");
                    step = new Step(StepName.selfie.ToString());
                    step.metadata = new Dictionary<string, string>();
                    //step.metadata.Add("??", "??");
                    workflow.Add(step);
                }

                //4.- Video
                if (paramIn.videoinclude == 1)
                {
                    LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding videoinclude...");
                    step = new Step(StepName.video.ToString());
                    step.metadata = new Dictionary<string, string>();
                    step.metadata.Add("messagetodisplay", paramIn.videomessage);
                    workflow.Add(step);
                }

                //7.- IdCard (Si no se agrego ya)
                if (paramIn.idcardinclude == 1 && !existIdcardIncluded)
                {
                    LOG.Debug("BPUnitOfWork.CreaWorkflow - Add idcard por pedido separado");
                    step = new Step(StepName.idcard.ToString());
                    step.metadata = new Dictionary<string, string>();
                    //step.metadata.Add("??", "??");
                    workflow.Add(step);
                }
                
                //8.- Indica ingreso de imagen (padron) para tramitestag
                if (paramIn.carregisterinclude == 1)
                {
                    LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding carregisterinclude...");
                    step = new Step(StepName.carregister.ToString(), 0, "document");
                    step.metadata = new Dictionary<string, string>();
                    step.metadata.Add("title", "Padron del Vehiculo");
                    step.metadata.Add("description", Properties.Settings.Default.MsgCarRegisterImage);
                    workflow.Add(step);
                }

                //9.- Indica ingreso de imagen (escritura) para tramitestag si es que es empresa la que se representa
                if (!string.IsNullOrEmpty(paramIn.taxidcompany) && paramIn.writinginclude == 1)
                {
                    LOG.Debug("BPUnitOfWork.CreaWorkflow -Adding writinginclude...");
                    step = new Step(StepName.writing.ToString(), 1, "document");
                    step.metadata = new Dictionary<string, string>();
                    step.metadata.Add("title", "Acredita poder");
                    step.metadata.Add("description", "Por favor ingrese comprobante que acredite que es representante legal de la empresa. " + 
                        "En caso de no tenerlo en este momento, presiones seguir y envie el comprobante al mail " + 
                        Properties.Settings.Default.MailTTag + " para completar el proceso...");
                    workflow.Add(step);
                }

                //10.- Indica ingreso de form para tramitestag
                /*
                    {
                      "name": "form-tramitestag",
                      "sampler": "form-generator",
                      "optional": 0,
                      "metadata": {
                        "title": "Formulario de inscripción",
                        "description": "Verifique y complete los datos necesarios.",
                        "schema": "url to schema | json schema"
                      }
                */
                if (!string.IsNullOrEmpty(paramIn.formid))
                {
                        //Y se enviará en json para que en BPWeb 
                    LOG.Debug("BPUnitOfWork.CreaWorkflow - Adding form...");
                    step = new Step(StepName.form.ToString(), 1, "form-generator");
                    step.metadata = new Dictionary<string, string>();
                    step.metadata.Add("title", "Formulario de Ingreso");
                    step.metadata.Add("description", "Verifique y complete los datos necesarios...");
                    workflow.Add(step);
                }

                LOG.Debug("BPUnitOfWork.CreaWorkflow - Cantidad de pasos agregados => " + workflow.Count.ToString());
                jsonWorkflow = JsonConvert.SerializeObject(workflow);
            }
            catch (Exception ex)
            {
                ret = -1;
                jsonWorkflow = null;
                msgerr = "CreaWorkflow Exc: " + ex.Message;
                LOG.Error("BPUnitOfWork.CreaWorkflow Error: " + ex.Message);
            }
            LOG.Error("BPUnitOfWork.CreaWorkflow Out [ret=" + ret + "]"); 
            return ret;
        }

        public static object VerifiyGetStatus(object param)
        {
            return null;
        }

        public static object WebhookJumio(object param)
        {
            return null;
        }

#region Global

        internal static string CheckStatusCompanyAndUser(int companyid, string msgerr)
        {
            string lmsgerr = msgerr;
            Company com;
            try
            {
                switch (Global.CONFIG.CheckAccess)
                {
                    case 0: //Check Disabled
                        break;
                    case 1: //Chek Company
                        com = Global.ArrCompanys.getCompany(companyid);
                        if (com == null)
                        {
                            lmsgerr = "La compañia no existe";
                        }
                        else if (com.status == 1)
                        { //Si está deshabilitado
                            lmsgerr = "La compañia esta inhabilitada para hacer consultas";
                            if (com.fecha.HasValue)
                            {
                                lmsgerr = lmsgerr + " desde el " + com.fecha.Value.ToString("dd/MM/yyyy");
                            }
                        }
                        break;
                    case 2: //Check User
                        break;
                    case 3: //Check Company and User
                        com = Global.ArrCompanys.getCompany(companyid);
                        if (com == null)
                        {
                            lmsgerr = "La compañia no existe";
                        }
                        else if (com.status == 1)
                        { //Si está deshabilitado
                            lmsgerr = "La compañia esta inhabilitada para hacer consultas";
                            if (com.fecha.HasValue)
                            {
                                lmsgerr = lmsgerr + " desde el " + com.fecha.Value.ToString("dd/MM/yyyy");
                            }
                        }
                        break;
                    default: //Si no está especificado correctametne se usa el mas restrictivo = 3
                        lmsgerr = "";
                        break;
                }

            }
            catch (Exception ex)
            {
                lmsgerr = "BPUnitOfWork.CheckStatusCompanyAndUser [" + ex.Message + "]";
                LOG.Error("BPUnitOfWork.CheckStatusCompanyAndUser", ex);
            }

            return lmsgerr;
        }

        internal static HttpStatusCode Verify(string trackid, out VerifyModelR vfResponse, out string msgerr)
        {
            HttpStatusCode ret;
            vfResponse = null;
            msgerr = null;
            string jsonResponse = null;
            string continuousLearningFaceMap = null;
            bool mustUpdateSourceFaceMap = false;
            try
            {
                LOG.Debug("BPUnitOfWork.Verify IN...");
                BpTx tx = AdminBpTx.RetrieveTxByTrackid(trackid, out msgerr, true);

                if (tx != null)
                {
                    LOG.Debug("BPUnitOfWork.Verify tx != null => id = " + tx.Id);
                    if (tx.BpTxConx != null && 
                        (Convert.ToInt32(tx.BpTxConx[0].Consultationtype) == (int)TypeVerify.tv3dplus ||
                         Convert.ToInt32(tx.BpTxConx[0].Consultationtype) == (int)TypeVerify.tv3dstrong))
                    {
                        LOG.Debug("BPUnitOfWork.Verify - Entra xq Consultationtype = " + tx.BpTxConx[0].Consultationtype);
                        string status = "PENDING";
                        if (tx.BpTxConx[0].Status == 1) status = "DONE";
                        if (tx.BpTxConx[0].Status == 2) status = "FAILED";
                        vfResponse = new VerifyModelR(trackid, status,
                                        tx.BpTxConx[0].Result.HasValue ? tx.BpTxConx[0].Result.Value : 0,
                                        tx.BpTxConx[0].Score.HasValue ? tx.BpTxConx[0].Score.Value : 0,
                                        tx.BpTxConx[0].CodeReject, tx.BpTxConx[0].DescriptionReject);
                        LOG.Debug("BPUnitOfWork.Verify - Sale con HttpStatusCode.OK y status = " + status ) ;
                        ret = HttpStatusCode.OK;
                        return ret;
                    }

                    //Chequeo que si ya se hizo no se vuelva a hacer sino retorna lo mismo que dio antes
                    if (tx.BpTxConx != null && tx.BpTxConx[0].Status != 0)
                    {
                        LOG.Debug("BPUnitOfWork.Verify - Sale porque status = " + (tx.BpTxConx[0].Status == 1 ? "DONE" : "FAILED"));
                        vfResponse = new VerifyModelR(trackid, (tx.BpTxConx[0].Status == 1 ? "DONE" : "FAILED"),
                                        tx.BpTxConx[0].Result.HasValue ? tx.BpTxConx[0].Result.Value : 0,
                                        tx.BpTxConx[0].Score.HasValue ? tx.BpTxConx[0].Score.Value : 0,
                                        tx.BpTxConx[0].CodeReject, tx.BpTxConx[0].DescriptionReject);
                        ret = HttpStatusCode.OK;
                    }
                    else 
                    //if (tx.BpTxConx[0].Consultationtype.Equals("1"))  //Es tv2d
                    //{

                    //} else //Es Facetec
                    {
                        LOG.Debug("BPUnitOfWork.Verify - Entra a Verify con consultationtype = " + tx.BpTxConx[0].Consultationtype);
                        bool bCheck = CheckDataCompleteFromVerify(tx, msgerr);
                        LOG.Debug("BPUnitOfWork.Verify -CheckDataCompleteFromVerify = " + bCheck.ToString());
                        if (bCheck)
                        {
                            tx.Timestampclient = DateTime.Now; //Solo para poder saber cuando tarda en realidad la verify con Facetec o Namku
                            if (tx.BpTxConx[0].Consultationtype.Equals("1"))
                            { //Es tv2d
                                LOG.Debug("BPUnitOfWork.Verify - Entra a Verify2D...");
                                vfResponse = Verify2D(tx, out jsonResponse, out msgerr);
                            }
                            else
                            {   //Es Facetec
                                LOG.Debug("BPUnitOfWork.Verify - Entra a FACETEC_HELPER.Verify...");
                                vfResponse = Global.FACETEC_HELPER.Verify(tx, out jsonResponse, out continuousLearningFaceMap, out msgerr);
                            }
                            if (vfResponse == null)
                            {
                               LOG.Debug("BPUnitOfWork.Verify - Sale con vfResponse = null => HttpStatusCode.InternalServerError");
                               ret = HttpStatusCode.InternalServerError;
                            }
                            else
                            {
                                LOG.Debug("BPUnitOfWork.Verify - Entra a llenar datos...");
                                //jsonResponse = JsonConvert.SerializeObject(vfResponse);
                                //Update de tablas y enroll/update si aplica
                                if (vfResponse.status.Equals("DONE"))
                                {
                                    LOG.Debug("BPUnitOfWork.Verify - Entra por DONE...");
                                    if (vfResponse.result == 1) //Positivo
                                    {
                                        tx.Result = 1; //Positivo
                                        tx.Score = vfResponse.score;
                                        tx.BpTxConx[0].Result = 1;
                                        tx.BpTxConx[0].Score = vfResponse.score;
                                        tx.BpTxConx[0].Status = 1; //DONE
                                        LOG.Debug("BPUnitOfWork.Verify - Positivo => Score = " + tx.Score.ToString());
                                        mustUpdateSourceFaceMap = true;
                                    }
                                    else
                                    {
                                        tx.Result = 2; //Negativo
                                        tx.Score = vfResponse.score;
                                        tx.BpTxConx[0].Result = 2;
                                        tx.BpTxConx[0].Score = vfResponse.score;
                                        LOG.Debug("BPUnitOfWork.Verify - Negativo => Score = " + tx.Score.ToString());
                                        tx.BpTxConx[0].Status = 1; //DONE
                                    }
                                }
                                else if (vfResponse.status.Equals("FAILED"))
                                {
                                    LOG.Debug("BPUnitOfWork.Verify - Entra por FAILED...");
                                    tx.Operationcode = Errors.IERR_3RO_FAILED_RESULT;
                                    tx.Result = 2; //Negativo
                                    tx.Score = vfResponse.score;
                                    tx.BpTxConx[0].Result = 2;
                                    tx.BpTxConx[0].Score = vfResponse.score;
                                    tx.BpTxConx[0].Status = 2; //FAILED
                                }
                                tx.Timestampend = DateTime.Now;
                                tx.BpTxConx[0].Response3ro = jsonResponse;

                                LOG.Debug("BPUnitOfWork.Verify - Entra AdminBpTx.Update...");
                                if (AdminBpTx.Update(tx, out msgerr))
                                {
                                    //Si es onboarding => MAndo a hacer EnrollOrUpdateIdentity
                                    if (tx.BpTxConx[0].OnboardingMandatory == 1)
                                    {
                                        LOG.Debug("BPUnitOfWork.Verify - Entra por OnBoardingMandatory...");
                                        string msgerrE;
                                        int retE = 0;
                                        if (tx.BpTxConx[0].Consultationtype.Equals("1"))
                                        {
                                            LOG.Debug("BPUnitOfWork.Verify - Entrando a EnrollOrUpdateFrom2D...");
                                            //Es Enroll desde Namku/2D
                                            retE = EnrollOrUpdateFrom2D(tx, out msgerrE);
                                        }
                                        else
                                        {
                                            LOG.Debug("BPUnitOfWork.Verify - Entrando a EnrollOrUpdateFromFacetec...");
                                            //Es Enroll desde Facetec
                                            retE = EnrollOrUpdateFromFacetec(tx, out msgerrE);
                                        }

                                        if (retE < 0)
                                        {
                                            LOG.Debug("BPUnitOfWork.Verify - Negativo retE = " + retE);
                                            if (string.IsNullOrEmpty(vfResponse.rejectDescription))
                                            {
                                                vfResponse.rejectDescription = "Error Enrolando/Modificando Identity [" +
                                                    retE + " - " +
                                                    (string.IsNullOrEmpty(msgerrE) ? "Desconocido" : msgerrE) + "]";
                                            }
                                            else
                                            {
                                                vfResponse.rejectDescription += " - Error Enrolando/Modificando Identity [" +
                                                    retE + " - " +
                                                    (string.IsNullOrEmpty(msgerrE) ? "Desconocido" : msgerrE) + "]";
                                            }
                                            LOG.Debug("BPUnitOfWork.Verify - Desc = " + vfResponse.rejectDescription);
                                        }
                                    }
                                    else
                                    {
                                        //Si es auth y devuelve una template mejorado despues de la verificacion => lo updateo
                                        // en bpidentity3ro => Solo para Facetec es esto
                                        if (mustUpdateSourceFaceMap && !string.IsNullOrEmpty(continuousLearningFaceMap))
                                        {
                                            LOG.Debug("BPUnitOfWork.Verify - Entra a UpdateFacemapConituousLearning...");
                                            UpdateFacemapConituousLearning(tx.Companyidtx, tx.Typeid, tx.Valueid,
                                                                           continuousLearningFaceMap);
                                        }
                                        else
                                        {
                                            LOG.Debug("BPUnitOfWork.Verify - NO Entra a UpdateFacemapConituousLearning...");
                                        }
                                    }

                                    //Send Callback si existe URL
                                    if (!string.IsNullOrEmpty(tx.BpTxConx[0].CallbackUrl))
                                    {
                                        LOG.Debug("BPUnitOfWork.UpdateStatus - Informando en Callbak Url = " + tx.BpTxConx[0].CallbackUrl +
                                                    " - TrackId = " + tx.Trackid + "...");
                                        int ret1 = InformPostCallback(tx.BpTxConx[0].CallbackUrl, tx.Trackid);
                                        LOG.Debug("BPUnitOfWork.UpdateStatus - Callbak Url return = " + ret1 + "!");
                                    }
                                    ret = HttpStatusCode.OK;
                                }
                                else
                                {
                                    LOG.Debug("BPUnitOfWork.Verify - Error Update...");
                                    if (string.IsNullOrEmpty(vfResponse.rejectDescription))
                                    {
                                        vfResponse.rejectDescription = "Error actualizado resultados [" +
                                            (string.IsNullOrEmpty(msgerr) ? "Desconocido" : msgerr) + "]";
                                    }
                                    else
                                    {
                                        vfResponse.rejectDescription += " - Error actualizado resultados [" +
                                            (string.IsNullOrEmpty(msgerr) ? "Desconocido" : msgerr) + "]";
                                    }
                                    LOG.Debug("BPUnitOfWork.Verify - Desc = " + vfResponse.rejectDescription);
                                    ret = HttpStatusCode.PartialContent;
                                }
                            }
                        }
                        else
                        {
                            LOG.Debug("BPUnitOfWork.Verify - Sale porque no están los valore sen BD para realizar Verify! [HttpStatusCode.PreconditionFailed]");
                            ret = HttpStatusCode.PreconditionFailed;
                        }
                    }
                } else
                {
                    LOG.Debug("BPUnitOfWork.Verify - Sale por HttpStatusCode.NotFound!");
                    if (string.IsNullOrEmpty(msgerr)) ret = HttpStatusCode.NotFound;
                    else ret = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                ret = HttpStatusCode.InternalServerError;
                msgerr = "Verify Exception [" + ex.Message + "]";
                LOG.Error("BPUnitOfWork.Verify Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.Verify OUT!");
            return ret;
        }

        private static VerifyModelR Verify2D(BpTx tx, out string jsonResponse3ro, out string msgerr)
        {
            msgerr = null;
            VerifyModelR ret = null;
            jsonResponse3ro = null;
            int result = 0;
            double score = 0;
            try
            {
                LOG.Debug("BPUnitOfWork.Verify2D IN...");
                int response = Verify2DviaPlugin(tx, out jsonResponse3ro, out result, out score, out msgerr);
                if (response == 0)
                {
                    if (result == 1)
                    {
                        LOG.Debug("BPUnitOfWork.Verify2D - Verify2DviaPlugin OK => result = " + result + " / score = " + score.ToString());
                        double _score = GetPorcentajeScore(score, Properties.Settings.Default.TV2D_Threshold); // score * 10; // //(double)(new Random()).Next(7500, 9800) / 100; 
                        LOG.Debug("BPUnitOfWork.Verify2D - Verify2DviaPlugin OK => GetPorcentajeScore _score =  " + _score.ToString());
                        if (_score >= tx.Threshold)
                        {
                            LOG.Debug("BPUnitOfWork.Verify2D -  score (" + _score.ToString() + ") >= th (" + tx.Threshold.ToString() + ") => " +
                                "Sale con estado DONE y Result = 1 (Positivo)");
                            ret = new VerifyModelR(tx.Trackid, "DONE", 1, _score, 0, null);
                        }
                        else
                        {
                            LOG.Debug("BPUnitOfWork.Verify2D -  score (" + _score.ToString() + ") < th (" + tx.Threshold.ToString() + ") => " +
                                "Sale con estado DONE y Result = 2 (Negativo)");
                            ret = new VerifyModelR(tx.Trackid, "DONE", 2, _score, 0, null);
                        }
                    } else
                    {
                        LOG.Debug("BPUnitOfWork.Verify2D -  score (" + score.ToString() + ") desde Verify2DviaPlugin => " +
                                "Sale con estado DONE y Result = 2 (Negativo)");
                        double _score2 = GetPorcentajeScoreNegativo(score, tx.Threshold, Properties.Settings.Default.TV2D_Threshold);
                        ret = new VerifyModelR(tx.Trackid, "DONE", 2, _score2, 0, null);
                    }
                    //continuousLearningFaceMap = response.Data.continuousLearningFaceMap;
                }
                else
                {
                    LOG.Debug("BPUnitOfWork.Verify2D - Verify2DviaPlugin error => ret = " + response);
                    msgerr = response.ToString();
                    LOG.Debug("BPUnitOfWork.Verify2D - Sale con estado FAILED");
                    ret = new VerifyModelR(tx.Trackid, "FAILED", 2, 0, response,null);
                }
            }
            catch (Exception ex)
            {
                ret = new VerifyModelR(tx.Trackid, "FAILED", 2, 0, Errors.IERR_UNKNOWN,
                                       "Exception [" + ex.Message + "]");
                LOG.Error("BPUnitOfWork.Varify2D Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.Verify2D OUT!");
            return ret;
        }

        private static double GetPorcentajeScoreNegativo(double score, double threshold, double tV2D_Threshold)
        {
            double ret = 0;
            double _BASE = 50;
            try
            {
                try
                {
                    LOG.Debug("BPUnitOfWork.GetPorcentajeScoreNegativo IN => score = " + score.ToString() + "/threshold = " + tV2D_Threshold);
                    _BASE = threshold;
                    if (_BASE == 0)
                        _BASE = 50;
                    LOG.Debug("BPUnitOfWork.GetPorcentajeScoreNegativo - _BASE = " + _BASE.ToString());
                }
                catch (Exception ex1)
                {
                    LOG.Error("BPHelper.GetPorcentajeScoreNegativo - " +
                        "Falta configurar o mal configurado en web.config CIScorePorcentajeBase [Error=" +
                        ex1.Message + "] => Default=75");
                }

                //double rts = (25 * (treshold - score)) / treshold;
                double rts = ((100 - _BASE) * (tV2D_Threshold - score)) / tV2D_Threshold;
                LOG.Debug("BPUnitOfWork.GetPorcentajeScoreNegativo => rts = " + rts.ToString());

                //ret = 75 + rts;
                ret = _BASE + rts;
                if (ret < 0)
                    ret = 0;
                LOG.Debug("BPHelper.GetPorcentajeScoreNegativo Porcentaje Seguridad => " + ret.ToString("##.##") + "%");
            }
            catch (Exception ex)
            {
                ret = threshold - 0.5; // 75;
                LOG.Error("BPHelper.GetPorcentajeScoreNegativo Error => " + ex.Message);
            }
            LOG.Debug("BPHelper.GetPorcentajeScoreNegativo OUT! ret = " + ret.ToString());
            return ret;
        }

        internal static HttpStatusCode GetDataForFormTxFromTrackId(string trackid, out FormData dataReturn)
        {
            string msgerr;
            BpTx tx = null;
            //strJsonReturn = null;
            dataReturn = new FormData("form");
            try
            {
                LOG.Debug("BPUnitOfWork.GetDataForFormTxFromTrackId IN - trackid=" +
                    (string.IsNullOrEmpty(trackid) ? "Null" : trackid) + "...");
            
                LOG.Debug("BPUnitOfWork.GetDataForFormTxFromTrackId - Recupera tx...");
                int ret = AdminBpTx.RetrieveTxByTrackid(trackid, out tx, out msgerr, true);
                if (ret < 0)  //Error trayendo Tx desde BD
                {
                    LOG.Error("BPUnitOfWork.GetDataForFormTxFromTrackId - Error recuperando tx = " + ret.ToString());
                    if (ret == -601)
                        return HttpStatusCode.NotFound;    //Err Tx no encontrada
                    else
                        return HttpStatusCode.InternalServerError;            //Err desconocido
                }
                else if (ret == 0 && tx == null) //Tx nulo entonces salgo con error
                {
                    LOG.Debug("BPUnitOfWork.GetDataForFormTxFromTrackId - Recuperando tx con Tx == NULL");
                    return HttpStatusCode.NotFound;
                    //return HttpStatusCode.ExpectationFailed;
                }
                else if (tx != null)
                {
                    LOG.Debug("BPUnitOfWork.GetDataForFormTxFromTrackId - Recupero nombre de dll para recuperar data a pintar...");
                    //TODO - Hacer modelo de proceso dinamico. 
                    //  1.- Agregar columna en bp_company_form para esto 
                    //  2.- Cargar todas las dlls e intanciarlas en el inicio
                    //Con esto luego se puede aqui procesar y retornar el valor
                    //Para esta primera version lo hago directo.
                    Dictionary<string, string> dicReturn = new Dictionary<string, string>();
                    dicReturn.Add("rutfirmante", tx.Valueid);
                    if (!string.IsNullOrEmpty(tx.TaxIdCompany))
                    {
                        dicReturn.Add("rutempresa", tx.TaxIdCompany);
                    }
                    if (!string.IsNullOrEmpty(tx.BpTxConx[0].ResponseOCR))
                    {
                        LOG.Debug("BPUnitOfWork.GetDataForFormTxFromTrackId - Deserializa tx.BpTxConx[0].ResponseOCR...");
                        Biometrika.Regula.WebAPI.Api._2021.Document document = 
                            JsonConvert.DeserializeObject<Biometrika.Regula.WebAPI.Api._2021.Document>(tx.BpTxConx[0].ResponseOCR);
                        if (document != null && document._DocInfo != null)
                        {
                            string strAux = (string.IsNullOrEmpty(document._DocInfo.ft_Given_Names) ? "" : document._DocInfo.ft_Given_Names.Trim()) + " " +
                                            (string.IsNullOrEmpty(document._DocInfo.ft_Surname) ? "" : document._DocInfo.ft_Surname.Trim()) + " " +
                                            (string.IsNullOrEmpty(document._DocInfo.ft_Mothers_Name) ? "" : document._DocInfo.ft_Mothers_Name.Trim());
                            //"ft_Surname":"MANSILLA ULLOA","ft_Given_Names":"GONZALO EDUARD","ft_Mothers_Name":null
                            dicReturn.Add("nombrefirmante", strAux);
                            LOG.Debug("BPUnitOfWork.GetDataForFormTxFromTrackId - Incluye nombrefirmante = " + strAux);
                        }
                    }
                    dataReturn.value = JsonConvert.SerializeObject(dicReturn);
                    //strJsonReturn = JsonConvert.SerializeObject(dicReturn);
                }

                //Set code y msg final
                LOG.Debug("BPUnitOfWork.GetDataForFormTxFromTrackId OUT!");
                return HttpStatusCode.OK;

            }
            catch (Exception ex)
            {
                LOG.Error("BPUnitOfWork.GetDataForFormTxFromTrackId Excp: " + ex.Message);
                return HttpStatusCode.InternalServerError;
            }
        }

        internal static double GetPorcentajeScore(double score, double treshold)
        {
            double ret = 0;
            double _BASE = 75;
            try
            {
                try
                {
                    LOG.Debug("BPUnitOfWork.GetPorcentajeScore IN => score = " + score.ToString() + "/threshold = " + treshold);
                    _BASE = Properties.Settings.Default.BPWebScorePorcentajeBase;
                    if (_BASE == 0)
                        _BASE = 75;
                    LOG.Debug("BPUnitOfWork.GetPorcentajeScore - _BASE = " + _BASE.ToString()); 
                }
                catch (Exception ex1)
                {
                    LOG.Error("BPHelper.GetPorcentajeScore - " +
                        "Falta configurar o mal configurado en web.config CIScorePorcentajeBase [Error=" +
                        ex1.Message + "] => Default=75");
                }

                //double rts = (25 * (treshold - score)) / treshold;
                double rts = ((100 - _BASE) * (treshold - score)) / treshold;
                LOG.Debug("BPUnitOfWork.GetPorcentajeScore => rts = " + rts.ToString());

                //ret = 75 + rts;
                ret = _BASE + rts;
                LOG.Debug("BPHelper.GetPorcentajeScore Porcentaje Seguridad => " + ret.ToString("##.##") + "%");
            }
            catch (Exception ex)
            {
                ret = Properties.Settings.Default.BPWebScorePorcentajeBase; // 75;
                LOG.Error("BPHelper.GetPorcentajeScore Error => " + ex.Message);
            }
            LOG.Debug("BPHelper.GetPorcentajeScore OUT!");
            return ret;
        }

        private static int Verify2DviaPlugin(BpTx tx, out string jsonResponse3ro, out int result, 
                                             out double score, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            result = 0;
            score = 0;
            jsonResponse3ro = null;
            try
            {
                LOG.Debug("BPUnitOfWork.Verify2DviaPlugin IN..."); 
                BioPortal.Server.Api.XmlParamIn paramin = new BioPortal.Server.Api.XmlParamIn();
                paramin.Actionid = 1201;
                paramin.Companyid = tx.Companyidtx;
                paramin.Clientid = "127.0.0.1";
                paramin.Origin = 45;
                paramin.Authenticationfactor = 4;
                paramin.Bodypart = 16;
                paramin.Companyid = tx.Companyidtx;
                paramin.Minutiaetype = 44;

                paramin.SampleCollection = new List<Bio.Core.Api.Sample>();
                Bio.Core.Api.Sample sample2;
                if (tx.BpTxConx[0].OnboardingMandatory == 1)  //Es cedula contra selfie
                {
                    LOG.Debug("BPUnitOfWork.Verify2DviaPlugin - Set Onboarding cedula vs selfie...");
                    sample2 = new Bio.Core.Api.Sample();
                    sample2.Minutiaetype = 41;
                    sample2.Data = tx.BpTxConx[0].IdCardFrontImage;
                    LOG.Debug("BPUnitOfWork.Verify2DviaPlugin - Cedula != null => " + 
                                    (string.IsNullOrEmpty(sample2.Data)? sample2.Data:"Null"));
                    Global.LOGExtended("BPUnitOfWork.Verify2DviaPlugin - IdCardFrontImage = " + 
                                             (string.IsNullOrEmpty(sample2.Data)?"NULL":sample2.Data));
                    paramin.SampleCollection.Add(sample2);

                    Bio.Core.Api.Sample sample = new Bio.Core.Api.Sample();
                    sample.Minutiaetype = 41;
                    sample.Data = tx.BpTxConx[0].Selfie2d;
                    LOG.Debug("BPUnitOfWork.Verify2DviaPlugin - Selfie2d != null => " + 
                                    (string.IsNullOrEmpty(sample.Data)?sample.Data : "Null"));
                    Global.LOGExtended("BPUnitOfWork.Verify2DviaPlugin - Selfie2d = " +
                                             (string.IsNullOrEmpty(sample.Data) ? "NULL" : sample.Data));
                    paramin.SampleCollection.Add(sample);
                }
                else if (tx.BpTxConx[0].OnboardingMandatory == 0)  //Es selfie vs BD
                {
                    sample2 = new Bio.Core.Api.Sample();
                    sample2.Minutiaetype = 41;
                    sample2.Data = tx.BpTxConx[0].Selfie2d;
                    LOG.Debug("BPUnitOfWork.Verify2DviaPlugin - Selfie2d != null => " + 
                                    (string.IsNullOrEmpty(sample2.Data)?sample2.Data:"Null"));
                    Global.LOGExtended("BPUnitOfWork.Verify2DviaPlugin - Selfie2d = " +
                                             (string.IsNullOrEmpty(sample2.Data) ? "NULL" : sample2.Data));
                    paramin.SampleCollection.Add(sample2);

                    Bio.Core.Api.Sample sample = new Bio.Core.Api.Sample();
                    sample.Minutiaetype = 44;
                    BpBir retB = AdminBpBir.RetrieveLastBir(tx.Companyidtx, tx.Typeid, tx.Valueid, 4, 44, 16);
                    sample.Data = retB.Data;
                    LOG.Debug("BPUnitOfWork.Verify2DviaPlugin - SelfieFromBD != null => " + 
                                    (string.IsNullOrEmpty(sample.Data) ? sample.Data : "Null"));
                    Global.LOGExtended("BPUnitOfWork.Verify2DviaPlugin - > LastBIR = " +
                                             (string.IsNullOrEmpty(sample.Data) ? "NULL" : sample.Data));
                    paramin.SampleCollection.Add(sample);
                }
                paramin.Threshold = Properties.Settings.Default.TV2D_Threshold; // Convert.ToDouble(txtTH.Text);
                LOG.Debug("BPUnitOfWork.Verify2DviaPlugin - Threshold = " + paramin.Threshold);
                paramin.PersonalData = new Bio.Core.Api.PersonalData();
                paramin.PersonalData.Typeid = tx.Typeid;
                paramin.PersonalData.Valueid = tx.Valueid;
                string xmlin = BioPortal.Server.Api.XmlUtils.SerializeAnObject(paramin);
                string xmlout;
                LOG.Debug("BPUnitOfWork.Verify2DviaPlugin - Ingresando a ServicesPluginManager.Matcher para verificar a company/typeid/valueid => " +
                    paramin.Companyid + "/" + paramin.PersonalData.Typeid + "/" + paramin.PersonalData.Valueid);
                ret = ServicesPluginManager.Matcher(paramin, out xmlout);
                if (ret == 0)
                {
                    LOG.Debug("BPUnitOfWork.Verify2DviaPlugin - ServicesPluginManager.Matcher ret == 0 => OK");
                    XmlParamOut paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<XmlParamOut>(xmlout);
                    result = (int)paramout.Result;
                    score = paramout.Score;
                    LOG.Debug("BPUnitOfWork.Verify2DviaPlugin - ServicesPluginManager.Matcher Result = " + result.ToString() +
                                    " - Score = " + score.ToString());
                    jsonResponse3ro = JsonConvert.SerializeObject(paramout);
                } else
                {
                    LOG.Warn("BPUnitOfWork.Verify2DviaPlugin - ServicesPluginManager.Matcher Err Ret == " + ret.ToString());
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BPUnitOfWork.Verify2DviaPlugin Excp: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.Verify2DviaPlugin OUT!");
            return ret;
        }

        private static int EnrollOrUpdateFrom2D(BpTx tx, out string msgerr)
        {
            msgerr = null;
            int ret = 0;
            BpIdentity identity = null;
            try
            {
                LOG.Debug("BPUnitOfWork.EnrollOrUpdateFrom2D IN...");

                //Chequeo si tengo autorizacion para enrolar
                if (tx.BpTxConx[0].Autorization < 1) //== -1) //Si es negado por el usuario o no se mostro pedido de autorizacion
                {
                    LOG.Debug("BPUnitOfWork.EnrollOrUpdateFrom2D - tx.BpTxConx[0].Autorization => " +
                        tx.BpTxConx[0].Autorization +
                        "sale porque no está autorizado para enrolar!");
                    return Errors.IERR_OK;  //Si ya esta enrolada el servicio de 3ro retorno 
                }

                //TODO - Determinar cuando y como updatar
                //   Si esta vencida la cedula
                //   Si el ultimo enroll fue hace XX Meses
                //   O como ?
                ret = AdminBpIdentity.Retrieve(tx.Companyidtx, tx.Typeid, tx.Valueid, out identity, out msgerr, true);
                LOG.Debug("BPUnitOfWork.EnrollOrUpdateFrom2D - AdminBpIdentity.Retrieve ret = " + ret);
                XmlParamIn xmlParamIn = new XmlParamIn();
                if (ret == 0 && identity != null)
                {
                    //Esto control que este vencido 
                    if (identity.Documentexpirationdate.HasValue && identity.Documentexpirationdate.Value < DateTime.Now)
                    {
                        //Sigue porque debe actualizar la info de la nueva verificacion porque cedula esta vencida
                    }
                    else
                    {
                        LOG.Debug("BPUnitOfWork.EnrollOrUpdateFrom2D - Identidad ya enrolada en este servicio => sale!");
                        return Errors.IERR_OK;  //Si ya esta enrolada y no esta vencida la cedula
                        ////Si ya está enrolado con este 
                        //if (IsEnrolled3ro(identity, tx.BpTxConx[0].Connectorid))
                        //{
                        //    LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromFacetec - Identidad ya enrolada en este servicio => sale!");
                        //    return Errors.IERR_OK;  //Si ya esta enrolada el servicio de 3ro retorno 
                        //}
                    }
                    xmlParamIn.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_MODIFY;
                }
                else
                {
                    xmlParamIn.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_ENROLL;
                }

                //Recupero desde la tx el json de lo reconocido desde regula
                Biometrika.Regula.WebAPI.Api._2021.Document _document =
                    JsonConvert.DeserializeObject<Biometrika.Regula.WebAPI.Api._2021.Document>(tx.BpTxConx[0].ResponseOCR);

                xmlParamIn.Authenticationfactor = Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL;
                xmlParamIn.Bodypart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_FACE;
                xmlParamIn.Clientid = "BPWeb"; //Ver que poner
                xmlParamIn.Companyid = tx.Companyidtx;
                xmlParamIn.Enduser = "BPWeb";
                xmlParamIn.InsertOption = 2;
                xmlParamIn.Ipenduser = "127.0.0.1";
                xmlParamIn.OperationOrder = Bio.Core.Matcher.Constant.OperationOrder.OPERATIONORDER_LOCALONLY;
                xmlParamIn.Origin = 45;
                xmlParamIn.Verifybyconnectorid = "FINAL";
                //Data personal
                Bio.Core.Api.PersonalData pd = new Bio.Core.Api.PersonalData();
                pd.Birthdate = Bio.Core.Utils.DateTimeHelper.Parse(_document._DocInfo.ft_Date_of_Birth); //TODO - Parser arreglar!!!!!!!!!!!!!!!
                pd.Companyidenroll = tx.Companyidtx;
                pd.Creation = DateTime.Now;
                pd.Documentexpirationdate = Bio.Core.Utils.DateTimeHelper.Parse(_document._DocInfo.ft_Date_of_Expiry);
                pd.Typeid = "RUT";
                //if (dr.document.type.Equals("ID_CARD")) pd.Typeid = "RUT";
                //else if (dr.document.type.Equals("PASSPORT")) pd.Typeid = "PAS";
                //else if (dr.document.type.Equals("DRIVING_LICENSE")) pd.Typeid = "DRL";
                string _serial;
                pd.Valueid = _document._DocInfo.ft_Personal_Number; // SelectValueId(dr.document.number, dr.document.optionalData2, out _serial);
                pd.Documentseriesnumber = _document._DocInfo.ft_Document_Number; // _serial;
                pd.Enrollinfo = tx.BpTxConx[0].Consultationtype;
                pd.Name = _document._DocInfo.ft_Given_Names;
                pd.Patherlastname = _document._DocInfo.ft_Surname;
                pd.Motherlastname = _document._DocInfo.ft_Mothers_Name;
                pd.Nationality = _document._DocInfo.ft_Nationality;
                pd.Sex = _document._DocInfo.ft_Sex;
                pd.Verificationsource = pd.Typeid;
                pd.Visatype = _document._DocInfo.ft_Visa;
                //Images
                pd.DocImageFront = tx.BpTxConx[0].IdCardFrontImage; // GetImage(ir, 0);
                pd.DocImageBack = tx.BpTxConx[0].IdCardBackImage; // GetImage(ir, 1);
                pd.Selfie = tx.BpTxConx[0].Selfie2d; // GetImage(ir, 2);
                pd.Signatureimage = tx.BpTxConx[0].IdScanSignature; // GetImage(ir, 3); //Comparte esto con Facetec
                pd.Photography = tx.BpTxConx[0].IdScanPortrait; // GetImage(ir, 4);     //Comparte esto con Facetec
                xmlParamIn.PersonalData = pd;
                //Samples
                Bio.Core.Api.Sample sample = new Bio.Core.Api.Sample();
                sample.Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_JPG;
                sample.BodyPart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_FACE;
                sample.Data = tx.BpTxConx[0].AuditTrailImage;
                ; // GetImage(ir, 4);
                xmlParamIn.SampleCollection = new List<Bio.Core.Api.Sample>();
                xmlParamIn.SampleCollection.Add(sample);
                Bio.Core.Api.Sample sample1 = new Bio.Core.Api.Sample();
                sample1.Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_FACIAL_FACEMAP;
                sample1.BodyPart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_FACE;
                sample1.Data = tx.BpTxConx[0].Selfie2d;
                ; // GetImage(ir, 4);
                xmlParamIn.SampleCollection.Add(sample1);

                string xmlparamout;
                LOG.Debug("BPUnitOfWork.EnrollOrUpdateFrom2D - ServicesManager.Enroll IN => ActionId = " + xmlParamIn.Actionid);
                ret = Services.ServicesManager.Enroll(xmlParamIn, out xmlparamout);

                if (ret == 0)
                {
                    LOG.Debug("BPUnitOfWork.EnrollOrUpdateFrom2D - Enrolado OK company/typeid/valueid => " + 
                        xmlParamIn.PersonalData.Companyidenroll + "/" + xmlParamIn.PersonalData.Typeid +
                        xmlParamIn.PersonalData.Valueid);
                }
                else
                {
                    LOG.Warn("BPUnitOfWork.EnrollOrUpdateFrom2D - [ERROR] Enrolando Identity company/typeid/valueid => " + 
                        xmlParamIn.PersonalData.Companyidenroll + "/" + xmlParamIn.PersonalData.Typeid +
                        xmlParamIn.PersonalData.Valueid + " [ret = " + ret + "]" );
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("BPUnitOfWork.EnrollOrUpdateFrom2D Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.EnrollOrUpdateFrom2D OUT!");
            return ret;
        }

        private static void UpdateFacemapConituousLearning(int companyidtx, string typeid, string valueid, string continuousLearningFaceMap)
        {
            string msg;
            BpIdentity3ro identity3ro = null;
            try
            {
                LOG.Debug("BPUnitOfWork.UpdateFacemapConituousLearning IN...");
                BpIdentity identity = null;
                int ret = AdminBpIdentity.Retrieve(companyidtx, typeid, valueid, out identity, out msg, false);
                if (ret == 0 && identity != null)
                {
                    LOG.Debug("BPUnitOfWork.UpdateFacemapConituousLearning - Retrieve AdminBpIdentity3ro...");
                    ret = AdminBpIdentity3ro.Retrieve(identity.Id, 2, out identity3ro);
                    if (ret == 0 && identity3ro != null)
                    {
                        identity3ro.TrackId3ro = continuousLearningFaceMap;
                        ret = AdminBpIdentity3ro.Update(identity3ro, out msg);
                        if (ret == 0) LOG.Debug("BPUnitOfWork.UpdateFacemapConituousLearning Actualizacion Continuous Learning para [" +
                                                 companyidtx.ToString() + "," + typeid + "," + valueid + "] OK!");
                        else LOG.Warn("BPUnitOfWork.UpdateFacemapConituousLearning Actualizacion Continuous Learning para [" +
                                                 companyidtx.ToString() + "," + typeid + "," + valueid + "] FAILED => err = " + 
                                                 ret.ToString());
                    }
                } else
                {
                    LOG.Debug("BPUnitOfWork.UpdateFacemapConituousLearning - No encontro identity companyidtx/typeid/valueid => " +
                                companyidtx + "/" + typeid + "/" + valueid);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BPUnitOfWork.UpdateFacemapConituousLearning Excp: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.UpdateFacemapConituousLearning OUT!");
        }

        private static int EnrollOrUpdateFromFacetec(BpTx tx, out string msgerr)
        {
            msgerr = null;
            int ret = 0;
            BpIdentity identity = null;
            try
            {
                LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromFacetec IN...");

                //Chequeo si tengo autorizacion para enrolar
                if (tx.BpTxConx[0].Autorization < 1) //== -1) //Si es negado por el usuario o no se mostro pedido de autorizacion
                {
                    LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromFacetec - tx.BpTxConx[0].Autorization => " +
                        tx.BpTxConx[0].Autorization +
                        "sale porque no está autorizado para enrolar!");
                    return Errors.IERR_OK;  //Si ya esta enrolada el servicio de 3ro retorno 
                }

                //TODO - Determinar cuando y como updatar
                //   Si esta vencida la cedula
                //   Si el ultimo enroll fue hace XX Meses
                //   O como ?
                ret = AdminBpIdentity.Retrieve(tx.Companyidtx, tx.Typeid, tx.Valueid, out identity, out msgerr, true);
                LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromFacetec - AdminBpIdentity.Retrieve ret = " + ret);
                XmlParamIn xmlParamIn = new XmlParamIn();
                if (ret == 0 && identity != null)
                {
                                                                    //Esto control que este vencido 
                    if (identity.Documentexpirationdate.HasValue && identity.Documentexpirationdate.Value < DateTime.Now)
                    {
                        //Sigue porque debe actualizar la info de la nueva verificacion porque cedula esta vencida
                    }
                    else
                    {
                        //Si ya está enrolado con este 
                        if (IsEnrolled3ro(identity, tx.BpTxConx[0].Connectorid))
                        {
                            LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromFacetec - Identidad ya enrolada en este servicio => sale!");
                            return Errors.IERR_OK;  //Si ya esta enrolada el servicio de 3ro retorno 
                        }
                    }
                    xmlParamIn.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_MODIFY;
                }
                else
                {
                    xmlParamIn.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_ENROLL;
                }

                //Recupero desde la tx el json de lo reconocido desde regula
                Biometrika.Regula.WebAPI.Api._2021.Document _document =
                    JsonConvert.DeserializeObject<Biometrika.Regula.WebAPI.Api._2021.Document>(tx.BpTxConx[0].ResponseOCR);

                xmlParamIn.Authenticationfactor = Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL;
                xmlParamIn.Bodypart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_FACE;
                xmlParamIn.Clientid = "BPWeb"; //Ver que poner
                xmlParamIn.Companyid = tx.Companyidtx;
                xmlParamIn.Enduser = "BPWeb";
                xmlParamIn.InsertOption = 2;
                xmlParamIn.Ipenduser = "127.0.0.1";
                xmlParamIn.OperationOrder = Bio.Core.Matcher.Constant.OperationOrder.OPERATIONORDER_LOCALONLY;
                xmlParamIn.Origin = 45;
                xmlParamIn.Verifybyconnectorid = "FINAL";
                //Data personal
                Bio.Core.Api.PersonalData pd = new Bio.Core.Api.PersonalData();
                pd.Birthdate = Bio.Core.Utils.DateTimeHelper.Parse(_document._DocInfo.ft_Date_of_Birth); //TODO - Parser arreglar!!!!!!!!!!!!!!!
                pd.Companyidenroll = tx.Companyidtx;
                pd.Creation = DateTime.Now;
                pd.Documentexpirationdate = Bio.Core.Utils.DateTimeHelper.Parse(_document._DocInfo.ft_Date_of_Expiry);
                pd.Typeid = "RUT";
                //if (dr.document.type.Equals("ID_CARD")) pd.Typeid = "RUT";
                //else if (dr.document.type.Equals("PASSPORT")) pd.Typeid = "PAS";
                //else if (dr.document.type.Equals("DRIVING_LICENSE")) pd.Typeid = "DRL";
                string _serial;
                pd.Valueid = _document._DocInfo.ft_Personal_Number; // SelectValueId(dr.document.number, dr.document.optionalData2, out _serial);
                pd.Documentseriesnumber = _document._DocInfo.ft_Document_Number; // _serial;
                pd.Enrollinfo = tx.BpTxConx[0].Consultationtype;
                pd.Name = _document._DocInfo.ft_Given_Names;
                pd.Patherlastname = _document._DocInfo.ft_Surname;
                pd.Motherlastname = _document._DocInfo.ft_Mothers_Name;
                pd.Nationality = _document._DocInfo.ft_Nationality;
                pd.Sex = _document._DocInfo.ft_Sex;
                pd.Verificationsource = pd.Typeid;
                pd.Visatype = _document._DocInfo.ft_Visa;
                //Images
                pd.DocImageFront = tx.BpTxConx[0].IdScanFrontImage; // GetImage(ir, 0);
                pd.DocImageBack = tx.BpTxConx[0].IdScanBackImage; // GetImage(ir, 1);
                pd.Selfie = tx.BpTxConx[0].AuditTrailImage; // GetImage(ir, 2);
                pd.Signatureimage = tx.BpTxConx[0].IdScanSignature; // GetImage(ir, 3);
                pd.Photography = tx.BpTxConx[0].IdScanPortrait; // GetImage(ir, 4);
                xmlParamIn.PersonalData = pd;
                //Samples
                Bio.Core.Api.Sample sample = new Bio.Core.Api.Sample();
                sample.Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_JPG;
                sample.BodyPart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_FACE;
                sample.Data = tx.BpTxConx[0].AuditTrailImage; ; // GetImage(ir, 4);
                xmlParamIn.SampleCollection = new List<Bio.Core.Api.Sample>();
                xmlParamIn.SampleCollection.Add(sample);
                Bio.Core.Api.Sample sample1 = new Bio.Core.Api.Sample();
                sample1.Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_FACIAL_FACEMAP;
                sample1.BodyPart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_FACE;
                sample1.Data = tx.BpTxConx[0].Sample3roSource; ; // GetImage(ir, 4);
                xmlParamIn.SampleCollection.Add(sample1);

                string xmlparamout;
                LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromFacetec - ServicesManager.Enroll IN => ActionId = " + xmlParamIn.Actionid);
                ret = Services.ServicesManager.Enroll(xmlParamIn, out xmlparamout);

                if (ret == 0)
                {
                    LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromFacetec - ret = 0");
                    BpIdentity identity2 = null;
                    ret = AdminBpIdentity.Retrieve(tx.Companyidtx, tx.Typeid, tx.Valueid, out identity2, out msgerr, false);
                    if (identity2 != null)
                    {
                        LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromFacetec - Creando enroll del servicio => ConsultationType = " +
                            tx.BpTxConx[0].Consultationtype + " - Trackid3ro = " + tx.BpTxConx[0].Trackid3ro);

                        ret = AdminBpIdentity3ro.InsertOrUpdate(identity2, Convert.ToInt32(tx.BpTxConx[0].Consultationtype),
                                                                tx.BpTxConx[0].Sample3roSource, out msgerr);
                        if (ret < 0)
                        {
                            LOG.Warn("BPUnitOfWork.EnrollOrUpdateFromFacetec - Error AdminBpIdentity3ro.InsertOrUpdate => ret = " +
                                ret.ToString() + " [" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]");
                        }

                        //                                        tx.BpTxConx[0].Sample3roSource)
                        //BpIdentity3ro bp3ro = new BpIdentity3ro(identity2, Convert.ToInt32(tx.BpTxConx[0].Consultationtype),
                        //                                        tx.BpTxConx[0].Sample3roSource);
                        //identity2.BpIdentity3ro = new List<BpIdentity3ro>();
                        //identity2.BpIdentity3ro.Add(bp3ro);
                        //if (!AdminBpIdentity.Write(identity2, out msgerr))
                        //{
                        //    LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromFacetec - Error salvando Enroll de servicio [" + msgerr + "]");
                        //    ret = Errors.IERR_DATABASE;
                        //}
                        //else
                        //{
                        //    LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromFacetec - Enroll del servicio 3ro OK!");
                        //    ret = Errors.IERR_OK;
                        //}
                    }
                    else
                    {
                        LOG.Warn("BPUnitOfWork.EnrollOrUpdateFromFacetec - [ERROR] No pudo recuperar identity para enrolar el servicio de tercero!");
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("BPUnitOfWork.EnrollOrUpdateFromFacetec Error: " + ex.Message);
            }

            return ret;
        }

        private static bool CheckDataCompleteFromVerify(BpTx tx, string msgerr)
        {
            bool ret = false;
            try
            {
                LOG.Debug("BPUnitOfWork.CheckDataCompleteFromVerify IN ...");
                if (tx == null || tx.BpTxConx == null)
                {
                    LOG.Warn("BPUnitOfWork.CheckDataCompleteFromVerify Sale porque tx o tx.BpTxConx son nulos!");
                    return false;
                }

                if (tx.BpTxConx[0].Consultationtype.Equals("1"))
                {
                    LOG.Debug("BPUnitOfWork.CheckDataCompleteFromVerify - Checking para tv2d...");
                    if (tx.BpTxConx[0].OnboardingMandatory == 1) //Es Selfie vs Cedula
                    {
                        LOG.Debug("BPUnitOfWork.CheckDataCompleteFromVerify - 2d Onboarding...");
                        ret = !string.IsNullOrEmpty(tx.BpTxConx[0].Selfie2d) &&
                                !string.IsNullOrEmpty(tx.BpTxConx[0].IdCardFrontImage) &&
                                !string.IsNullOrEmpty(tx.BpTxConx[0].IdCardBackImage);
                    }
                    else //Es selfie vs BD
                    {
                        LOG.Debug("BPUnitOfWork.CheckDataCompleteFromVerify - 2d Authentication...");
                        ret = !string.IsNullOrEmpty(tx.BpTxConx[0].Selfie2d);
                    }
                } else if (tx.BpTxConx[0].OnboardingMandatory == 1) //Es Selfie vs Cedula
                {
                    LOG.Debug("BPUnitOfWork.CheckDataCompleteFromVerify - Checking para tv3d Onboarding...");
                    ret = !string.IsNullOrEmpty(tx.BpTxConx[0].Sample3roSource) &&
                            !string.IsNullOrEmpty(tx.BpTxConx[0].AuditTrailImage) &&
                            !string.IsNullOrEmpty(tx.BpTxConx[0].LowQualityAudittrailImage) &&
                            !string.IsNullOrEmpty(tx.BpTxConx[0].IdScan) &&
                            !string.IsNullOrEmpty(tx.BpTxConx[0].IdScanFrontImage) &&
                            !string.IsNullOrEmpty(tx.BpTxConx[0].IdScanBackImage) &&
                            !string.IsNullOrEmpty(tx.BpTxConx[0].XUserAgent);
                }
                else //Es selfie vs BD
                {
                    LOG.Debug("BPUnitOfWork.CheckDataCompleteFromVerify - Checking para tv3d Authentication...");
                    ret = !string.IsNullOrEmpty(tx.BpTxConx[0].Sample3roSource) &&
                            !string.IsNullOrEmpty(tx.BpTxConx[0].AuditTrailImage) &&
                            !string.IsNullOrEmpty(tx.BpTxConx[0].LowQualityAudittrailImage) &&
                            (!string.IsNullOrEmpty(tx.BpTxConx[0].Sample3roTarget) ||
                            !string.IsNullOrEmpty(tx.BpTxConx[0].EnrollmentIdentifier)) &&
                            !string.IsNullOrEmpty(tx.BpTxConx[0].XUserAgent);
                }
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("BPUnitOfWork.CheckDataCompleteFromVerify Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.CheckDataCompleteFromVerify OUT! ret = " + ret.ToString());
            return ret;
        }

        internal static int UpdateAuthenticationResult(string transactionStatus, string transactionReference, string errorCode)
        {
            int ret = 0;
            string msgerr;
            /*
                1.- Recupero BpTxConx
                2.- Recupero BpTx
                3.- Actualizo campos
                4.- Grabo en BD
            */
            try
            {
                string trackid = AdminBpTxConx.RetrieveTrackIdFromTrackid3ro(transactionReference, out msgerr);

                if (!string.IsNullOrEmpty(trackid))
                {
                    BpTx tx = AdminBpTx.RetrieveTxByTrackid(trackid, out msgerr, true);
                    if (tx == null) ret = Errors.IERR_TX_NOT_EXIST;
                    else
                    {
                        tx.Operationcode = 0;
                        tx.Timestampend = DateTime.Now;
                        if (transactionStatus.Equals("ERROR"))
                        {
                            tx.Score = 0;
                            tx.Result = 2;
                            tx.BpTxConx[0].Result = 2;
                            tx.BpTxConx[0].Result = 0;
                            //tx.BpTxConx[0].Status = errorCode; //TODO Revisar que poner exacto
                            //tx.BpTxConx[0].CodeReject = GetCodeError(errorcode);
                            //tx.BpTxConx[0].DescriptionReject = GetDescCodeError(errorcode);
                        }
                        else
                        {

                            tx.Score = (double)(new Random()).Next(7500, 9800) / 100; 
                            tx.Result = 1;
                            tx.BpTxConx[0].Result = 1;
                            tx.BpTxConx[0].Result = 0;
                            tx.BpTxConx[0].Status = 0; //TODO Revisar que poner exacto
                        }

                        //TODO - Send Callback si existe URL
                        if (!string.IsNullOrEmpty(tx.BpTxConx[0].CallbackUrl))
                        {
                            LOG.Debug("BPUnitOfWork.UpdateUthenticationResult - Informando en Callbak Url = " + tx.BpTxConx[0].CallbackUrl +
                                        " - TrackId = " + tx.Trackid + "...");
                            int ret1 = InformPostCallback(tx.BpTxConx[0].CallbackUrl, tx.Trackid);
                            LOG.Debug("BPUnitOfWork.UpdateUthenticationResult - Callbak Url return = " + ret1 + "!");
                        }
                    }
                } else
                {
                    ret = Errors.IERR_TX_NOT_EXIST;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_DATABASE;
                LOG.Error("BPUnitOfWork.UpdateUthenticationResult Error: " + ex.Message);
            }

            LOG.Debug("BPUnitOfWork.UpdateUthenticationResult ");
            return 0;
        }

        /// <summary>
        /// Dado un trackid recupera todo lo necesario para enviar la informacion a BPWeb
        /// para su correcto funcionamiento
        /// </summary>
        /// <param name="trackid"></param>
        /// <param name="imResponse"></param>
        /// <returns></returns>
        internal static HttpStatusCode GetTxFromTrackId(string trackid, out InitializationModel imResponse)
        {
            string msgerr;
            BpTx tx = null;
            try
            {
                LOG.Debug("BPUnitOfWork.GetTxFromTrackId IN...");
                int ret = AdminBpTx.RetrieveTxByTrackid(trackid, out tx, out msgerr, true);
                if (ret < 0)
                {
                    LOG.Warn("BPUnitOfWork.GetTxFromTrackId Error desde AdminBpTx.RetrieveTxByTrackid = " + ret.ToString());
                    imResponse = new InitializationModel(ret, msgerr);
                    if (ret == -601) return HttpStatusCode.NotFound;    //Err Tx no encontrada
                    else return HttpStatusCode.InternalServerError;            //Err desconocido
                } else
                {
                    LOG.Debug("BPUnitOfWork.GetTxFromTrackId Recupera Theme y FormSchema si existe...");
                    string jsontheme = BPUnitOfWork.GetJsonTheme(tx.Companyidtx, tx.BpTxConx[0].Theme);
                    List<Step> workflow = JsonConvert.DeserializeObject<List<Step>>(tx.BpTxConx[0].Workflow);

                    //Si se va a pedir Form => Inyectar el Schema en el envio
                    if (!string.IsNullOrEmpty(tx.BpTxConx[0].FormInclude))
                    {
                        LOG.Debug("BPUnitOfWork.GetTxFromTrackId - Recupera SchemaForm des Company = " + tx.Companyidtx.ToString() +
                                    " con FormId = " + tx.BpTxConx[0].FormInclude);
                        string jsonformschema = BPUnitOfWork.GetJsonForm(tx.Companyidtx, tx.BpTxConx[0].FormInclude);
                        
                        foreach (Step item in workflow)
                        {
                            if (item.name.Equals("form"))
                            {
                                LOG.Debug("BPUnitOfWork.GetTxFromTrackId - Update metadata de form..."); 
                                if (item.metadata == null) item.metadata = new Dictionary<string, string>(); 
                                item.metadata.Add("schema", jsonformschema);
                                break;
                            }
                        }
                    }
                    imResponse = new InitializationModel(0, tx.Typeid, tx.Valueid, tx.BpTxConx[0].RedirectUrl, tx.BpTxConx[0].SuccessUrl,
                                                         jsontheme, tx.TitleBpWeb, tx.MessageBpWeb,
                                                         workflow);
                    LOG.Debug("BPUnitOfWork.GetTxFromTrackId OUT!");
                    return HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                imResponse = new InitializationModel(-1, "Get Tx From TrackId Error [" + ex.Message + "]");
                LOG.Error("BPUnitOfWork.GetTxFromTrackId Error: " + ex.Message);
                return HttpStatusCode.InternalServerError;
            }
        }

       

        /// <summary>
        /// Metodo para actualizar valore sd eTx cuando es necesario desde BPWeb
        /// </summary>
        /// <param name="paramIn"></param>
        /// <param name="umResponse"></param>
        /// <returns></returns>
        internal static HttpStatusCode UpdateTxFromTrackId(UpdateModel paramIn, string browser, string so, out UpdateModelR umResponse)
        {
            string msgerr = null;
            BpTx tx = null;
            string _photoFromIdCard = null;
            string _signatureFromIdCard = null;
            string _idscanfront = null;
            string _idscanback = null;
            Biometrika.Regula.WebAPI.Api._2021.Document _Document;
            try
            {
                LOG.Debug("BPUnitOfWork.UpdateTxFromTrackId IN - Trackid = " + paramIn.trackid);
                int ret = AdminBpTx.RetrieveTxByTrackid(paramIn.trackid, out tx, out msgerr, true);
                if (ret < 0)
                {
                    LOG.Warn("BPUnitOfWork.UpdateTxFromTrackId - Error recuperando tx by trackid => ret = " + ret.ToString());
                    umResponse = new UpdateModelR(ret, msgerr, null);
                    if (ret == -601) return HttpStatusCode.NotFound;    //Err Tx no encontrada
                    else return HttpStatusCode.InternalServerError;            //Err desconocido
                }
                else
                {
                    LOG.Debug("BPUnitOfWork.UpdateTxFromTrackId - Salvando en BD..."); 
                    tx.Enduser = (string.IsNullOrEmpty(browser) ? "UnKnow" : browser) + "/" +
                                 (string.IsNullOrEmpty(so) ? "UnKnow" : so);
                    LOG.Debug("BPUnitOfWork.UpdateTxFromTrackId IN - tx.Enduser = " + tx.Enduser);
                    if (paramIn.samplelist != null)
                    {
                        bool _havoToProcessRecognizedIdCard = false;
                        foreach (APIRest.Models.Sample item in paramIn.samplelist)
                        {
                            LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - Metadata a modificar => " + item.name);

                            if (item.name.Equals("autorization"))
                            {
                                tx.BpTxConx[0].Autorization = Convert.ToInt32(item.value);
                                tx.BpTxConx[0].AutorizationDate = DateTime.Now;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - autorization OK! = " + tx.BpTxConx[0].Autorization);
                            }
                            if (item.name.Equals("signaturemanual"))
                            {
                                tx.SignatureManual = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - signaturemanual OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - signaturemanual = " + 
                                                        (string.IsNullOrEmpty(item.value)?"NULL":item.value));
                            }

                            //Para tv2d
                            if (item.name.Equals("selfie"))
                            {
                                tx.BpTxConx[0].Selfie2d = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - selfie OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - selfie = " +
                                                        (string.IsNullOrEmpty(item.value) ? "NULL" : item.value));
                            }
                            if (item.name.Equals("idcard-front"))
                            {
                                _havoToProcessRecognizedIdCard = true;
                                tx.BpTxConx[0].IdCardFrontImage = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - idcard-front OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - idcard-front = " +
                                                        (string.IsNullOrEmpty(item.value) ? "NULL" : item.value));
                            }
                            if (item.name.Equals("idcard-back"))
                            {
                                _havoToProcessRecognizedIdCard = true;
                                tx.BpTxConx[0].IdCardBackImage = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - idcard-back OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - idcard-back = " +
                                                        (string.IsNullOrEmpty(item.value) ? "NULL" : item.value));
                            }
 
                            //if (item.name.Equals("xuseragent"))
                            //{
                            //    tx.BpTxConx[0].XUserAgent = item.value;
                            //}

                            //Para Jumio y Facetec
                            if (item.name.Equals("sample3rosource"))
                            {
                                tx.BpTxConx[0].Sample3roSource = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - sample3rosource OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - sample3rosource = " +
                                                       (string.IsNullOrEmpty(item.value) ? "NULL" : item.value));
                            }
                            if (item.name.Equals("sample3rosourcetype"))
                            {
                                tx.BpTxConx[0].Sample3roSourceType = Convert.ToInt32(item.value);
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - sample3rosourcetype OK!");
                            }
                            if (item.name.Equals("sample3rotarget"))
                            {
                                tx.BpTxConx[0].Sample3roTarget = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - sample3rotarget OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - sample3rotarget = " +
                                                       (string.IsNullOrEmpty(item.value) ? "NULL" : item.value));
                            }
                            if (item.name.Equals("sample3rotargettype"))
                            {
                                tx.BpTxConx[0].Sample3roTargetType = Convert.ToInt32(item.value);
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - sample3rotargettype OK!");
                            }
                            if (item.name.Equals("audittrailimage"))
                            {
                                tx.BpTxConx[0].AuditTrailImage = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - audittrailimage OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - audittrailimage = " +
                                                       (string.IsNullOrEmpty(item.value) ? "NULL" : item.value));
                            }
                            if (item.name.Equals("lowqualityaudittrailimage"))
                            {
                                tx.BpTxConx[0].LowQualityAudittrailImage = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - lowqualityaudittrailimage OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - lowqualityaudittrailimage = " +
                                                       (string.IsNullOrEmpty(item.value) ? "NULL" : item.value));
                            }
                            //if (item.name.Equals("enrollmentIdentifier"))
                            //{
                            //    tx.BpTxConx[0].EnrollmentIdentifier = item.value;
                            //}

                            //Geoloc
                            if (item.name.Equals("geolocation"))
                            {
                                tx.GeoLocation = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - geolocation OK! => " +
                                    (string.IsNullOrEmpty(item.value) ? "NULL" : item.value));
                            }

                            //Para Facetec
                            if (item.name.Equals("idscan"))
                            {
                                tx.BpTxConx[0].IdScan = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - idscan OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - idscan = " +
                                                       (string.IsNullOrEmpty(item.value) ? "NULL" : item.value));
                            }
                            if (item.name.Equals("idscanfrontimage"))
                            {
                                _havoToProcessRecognizedIdCard = true;
                                tx.BpTxConx[0].IdScanFrontImage = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - idscanfrontimage OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - idscanfrontimage = " +
                                                       (string.IsNullOrEmpty(item.value) ? "NULL" : item.value));
                            }
                            if (item.name.Equals("idscanbackimage"))
                            {
                                _havoToProcessRecognizedIdCard = true;
                                tx.BpTxConx[0].IdScanBackImage = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - idscanbackimage OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - idscanbackimage = " +
                                                       (string.IsNullOrEmpty(item.value) ? "NULL" : item.value));
                            }

                            //Video
                            if (item.name.Equals("video"))
                            {
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - Uploading video...");
                                tx.BpTxConx[0].VideoUrl = UploadVideo(tx.Companyidtx, paramIn.trackid, item.value);
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - UploadVideo Fin!");
                            }

                            //Padron (TramitesTag)
                            if (item.name.Equals("carregister"))
                            {
                                //Added 06-11-2020 - Debo separar por si vienen front y back 
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - Deserializo carregister...");
                                List<string> arrSamples = JsonConvert.DeserializeObject<List<string>>(item.value);
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - arrSamples!=null => " + (arrSamples!=null).ToString());
                                if (arrSamples!=null)
                                    LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - arrSamples.length = " + (arrSamples.Count).ToString());
                                ImageSample isample = ConvertStringToImageSample("carregisterfront", arrSamples[0]);
                                tx.BpTxConx[0].CarRegisterImageFront = isample.data;
                                if (arrSamples.Count > 1 && !string.IsNullOrEmpty(arrSamples[1]))
                                {
                                    isample = ConvertStringToImageSample("carregisterback", arrSamples[1]);
                                    tx.BpTxConx[0].CarRegisterImageBack = isample.data;
                                }
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - carregister OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - carregister = " +
                                                        (string.IsNullOrEmpty(item.value) ? "NULL" : item.value.Substring(0,10) + "..."));
                            }

                            //Writing (Escritura TramitesTag)
                            if (item.name.Equals("writing"))
                            {
                                tx.BpTxConx[0].WritingImage = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - writing OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - writing = " +
                                                        (string.IsNullOrEmpty(item.value) ? "NULL" : item.value.Substring(0, 10) + "..."));
                            }

                            //Form (TramitesTag)
                            if (item.name.Equals("form"))
                            {
                                tx.BpTxConx[0].Form = item.value;
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - form OK!");
                                Global.LOGExtended("BPUnitOfWorks.UpdateTxFromTrackId - form = " +
                                                        (string.IsNullOrEmpty(item.value) ? "NULL" : item.value));
                            }

                        }
                        if (_havoToProcessRecognizedIdCard)
                        {
                            LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - ProcessRecognizeOCRIdCard IN...");
                            if (tx.BpTxConx[0].Consultationtype.Equals("1"))
                            {
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - Desde 2D...");
                                //Es cedula que viene desde 2D
                                ret = ProcessRecognizeOCRIdCard(tx.BpTxConx[0].IdCardFrontImage, tx.BpTxConx[0].IdCardBackImage,
                                                          out _idscanfront, out _idscanback,
                                                          out _photoFromIdCard, out _signatureFromIdCard, out _Document,
                                                          out msgerr);

                            } else
                            {
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - Desde 3D...");
                                //Es cedula que viene desde Facetec
                                ret = ProcessRecognizeOCRIdCard(tx.BpTxConx[0].IdScanFrontImage, tx.BpTxConx[0].IdScanBackImage,
                                                          out _idscanfront, out _idscanback,
                                                          out _photoFromIdCard, out _signatureFromIdCard, out _Document,
                                                          out msgerr);
                            }
                            if (ret == 0)
                            {
                                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - Procesando salida de reconocimeinto OCR...");
                                if (!_Document._DocInfo.ft_Personal_Number.Equals(tx.Valueid))
                                {
                                    umResponse = new UpdateModelR(ret, "Documento reconocido diferente al pedido para verificacion [" +
                                        "Pedido: " + tx.Valueid + " - Reconocido: " + _Document._DocInfo.ft_Personal_Number + "]",
                                        tx.Trackid);
                                    LOG.Warn("BPUnitOfWorks.UpdateTxFromTrackId - Sale => " + "Documento reconocido diferente al pedido para verificacion [" +
                                        "Pedido: " + tx.Valueid + " - Reconocido: " + _Document._DocInfo.ft_Personal_Number + "]");
                                    return HttpStatusCode.Conflict;
                                } else
                                {
                                    LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - Setenado fotos reconocidas desde Regula..:");
                                    tx.BpTxConx[0].IdScanPortrait = GetFoto(_Document._DocImages, 201);
                                    tx.BpTxConx[0].IdScanSignature = GetFoto(_Document._DocImages, 204);
                                    string sAux = null;
                                    sAux = GetFoto(_Document._DocImages, 199);
                                    if (tx.BpTxConx[0].Consultationtype.Equals("1"))
                                    {   //Es cedula que viene desde 2D
                                        tx.BpTxConx[0].IdCardFrontImage = string.IsNullOrEmpty(sAux) ?
                                                                      tx.BpTxConx[0].IdCardFrontImage : sAux;
                                    } else
                                    {
                                        tx.BpTxConx[0].IdScanFrontImage = string.IsNullOrEmpty(sAux) ?
                                                                      tx.BpTxConx[0].IdScanFrontImage : sAux;
                                    }
                                    sAux = null;
                                    sAux = GetFoto(_Document._DocImages, 200);
                                    if (tx.BpTxConx[0].Consultationtype.Equals("1"))
                                    {   //Es cedula que viene desde 2D
                                        tx.BpTxConx[0].IdCardBackImage = string.IsNullOrEmpty(sAux) ?
                                                                      tx.BpTxConx[0].IdCardBackImage : sAux;
                                    }
                                    else
                                    {
                                        tx.BpTxConx[0].IdScanBackImage = string.IsNullOrEmpty(sAux) ?
                                                                      tx.BpTxConx[0].IdScanBackImage : sAux;
                                    }
                                    //Elimino la parte de imagenes para que el json sea mas chico. Solo guardo los datos
                                    // para enorlar si es necesario sino de auditoria
                                    _Document._DocImages = null;
                                    LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - Serialize datos doc, para usarse luego en enroll si hace falta...");
                                    tx.BpTxConx[0].ResponseOCR = JsonConvert.SerializeObject(_Document);
                                }
                            }
                        }
                        if (ret == 0)
                        {
                            if (paramIn.metadata!=null)
                            {
                                if (paramIn.metadata.ContainsKey("xuseragent"))
                                {
                                    LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - xuseragent = " + 
                                        (string.IsNullOrEmpty(paramIn.metadata["xuseragent"])?"null":
                                        paramIn.metadata["xuseragent"]));
                                    tx.BpTxConx[0].XUserAgent = paramIn.metadata["xuseragent"];
                                }
                            }
                            LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - UpdateTx...");
                            ret = BPUnitOfWork.UpdateTx(tx, out msgerr);
                            LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - UpdateTx ret = " + ret.ToString());
                        } else
                        {
                            umResponse = new UpdateModelR(ret, "Error Recognized [" + msgerr + "]", tx.Trackid);
                            LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - Sale por error en recognized [" + msgerr + "]");
                            return HttpStatusCode.Conflict;
                        }
                    } else
                    {
                        ret = Bio.Core.Constant.Errors.IERR_BAD_PARAMETER;
                        msgerr = "Lista de datos a modificar vacia!";
                        LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - Sale por IERR_BAD_PARAMETER [" + msgerr + "]");
                    }
                }
                umResponse = new UpdateModelR(ret, "", tx.Trackid);
                LOG.Debug("BPUnitOfWorks.UpdateTxFromTrackId - OUT!");
                return HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                umResponse = new UpdateModelR(-1, "UpdateTxFromTrackId Error [" + ex.Message + "]", paramIn.trackid);
                LOG.Error("BPUnitOfWork.UpdateTxFromTrackId Error: " + ex.Message);
                return HttpStatusCode.InternalServerError;
            }
        }


        /// <summary>
        /// Hace upload de video en disco local o Azure
        /// </summary>
        /// <param name="company"></param>
        /// <param name="trackid"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string UploadVideo(int company, string trackid, string value)
        {
            string msg;
            string ret = null;
            try
            {
                LOG.Debug("BPUnitOfWork.UploadVideo IN...");
                int retI = Bio.Core.Utils.StorageHelper.UploadToStorage(Convert.FromBase64String(value), company.ToString(), trackid, "webm",
                    Bio.Core.Utils.StorageType.azure, Properties.Settings.Default.StorageConnectionString, "storagebp", 
                    out ret, out msg);
                LOG.Debug("BPUnitOfWork.UploadVideo - Retorno = " + retI);
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("BPUnitOfWork.UploadVideo Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.UploadVideo OUT! ret = " + (string.IsNullOrEmpty(ret)?"Null":ret));
            return ret;
        }

        private static int ProcessRecognizeOCRIdCard(string idScanFrontImage, string idScanBackImage,
                            out string idScanFrontImageCropped, out string idScanBackImageCropped,
                            out string photoFromIdCard, out string signatureFromIdCard, 
                            out Biometrika.Regula.WebAPI.Api._2021.Document document, out string msgerr)
        {
            msgerr = null;
            photoFromIdCard = null;
            signatureFromIdCard = null;
            idScanFrontImageCropped = null;
            idScanBackImageCropped = null;
            document = null;
            int ret = 0;
            try
            {
                LOG.Debug("BPUnitOfWork.ProcessRecognizeOCRIdCard - IN..."); 
                ret = Global.REGULA_HANDLER.RecognizeDocument(idScanFrontImage, idScanBackImage, out document, out msgerr);
                LOG.Debug("BPUnitOfWork.ProcessRecognizeOCRIdCard RecognizeDocument ret = " + ret);
                if (ret == 0 && document != null && document._DocImages != null)
                {
                    LOG.Debug("BPUnitOfWork.ProcessRecognizeOCRIdCard - Recuperando imagenes desde Regula..."); 
                    idScanFrontImageCropped = GetFoto(document._DocImages, 199);
                    idScanBackImageCropped = GetFoto(document._DocImages, 200);
                    photoFromIdCard = GetFoto(document._DocImages, 201);
                    signatureFromIdCard = GetFoto(document._DocImages, 204); ;
                } else
                {
                    LOG.Debug("BPUnitOfWork.ProcessRecognizeOCRIdCard - Error => No recupera imagenes");
                }
            } 
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BPUnitOfWork.ProcessRecognizeOCRIdCard Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.ProcessRecognizeOCRIdCard OUT!");
            return ret;
        }

        private static int UpdateTx(BpTx tx, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            try
            {
                LOG.Debug("BPUnitOfWork.UpdateTx IN...");
                ret = (AdminBpTx.Update(tx, out msgerr)?0: Bio.Core.Constant.Errors.IERR_DATABASE);
                LOG.Debug("BPUnitOfWork.UpdateTx AdminBpTx.Update ret = " + ret);
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BPUnitOfWork.UpdateTx Excp: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.UpdateTx OUT!");
            return ret;
        }

        internal static HttpStatusCode GetStatusTxFromTrackId(string trackid, out StatusModelR smResponse)
        {
            int resp = 0;
            string msgerr;
            BpTx tx = null;
            try
            {
                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId IN...");
                int ret = AdminBpTx.RetrieveTxByTrackid(trackid, out tx, out msgerr, true);
                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId RetrieveTxByTrackid ret = " + ret);
                if (ret < 0)
                {
                    smResponse = new StatusModelR(ret, msgerr);
                    if (ret == -601)
                    {
                        LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Response HttpStatusCode.NotFound!");
                        return HttpStatusCode.NotFound;    //Err Tx no encontrada
                    }
                    else
                    {
                        LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Response HttpStatusCode.InternalServerError!");
                        return HttpStatusCode.InternalServerError;            //Err desconocido
                    }
                }
                else
                {
                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Ingresando a check de status...");
                    if (tx.BpTxConx != null && tx.BpTxConx[0] != null && tx.BpTxConx[0].Status != null
                        && tx.BpTxConx[0].Status.HasValue
                        && tx.BpTxConx[0].Status.Value == 0)
                    {
                        switch (Convert.ToInt32(tx.BpTxConx[0].Consultationtype))
                        {
                            case (int)TypeVerify.tv2d:
                                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Check status tv2d...");
                                break;
                            case (int)TypeVerify.tv3d: //Facetec
                                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Check status tv3d...");
                                break;
                            case (int)TypeVerify.tv3dplus: //Jumio Automatico
                                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Check status tv3dplus...");
                                resp = Global.JUMIO_HELPER.GetStatus(tx.BpTxConx[0].OnboardingMandatory, tx.BpTxConx[0].Trackid3ro, out msgerr);
                                break;
                            case (int)TypeVerify.tv3dstrong: //Jumio Hibrido
                                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Check status tv3dstrong...");
                                resp = Global.JUMIO_HELPER.GetStatus(tx.BpTxConx[0].OnboardingMandatory, tx.BpTxConx[0].Trackid3ro, out msgerr);
                                break;
                            default:
                                break;
                        }
                        if (resp != tx.BpTxConx[0].Status.Value)
                        {
                            LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Respuesta 1= estado => UpdateStatus...");
                            ret = UpdateStatus(tx.Trackid, tx.BpTxConx[0].Trackid3ro, tx.BpTxConx[0].OnboardingMandatory);
                            if (ret < 0)
                            {
                                LOG.Warn("BPUnitOfWork.GetStatusTxFromTrackId - Error updateando resultado [" + ret + "]");
                            } else
                            {
                                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - UpdateStatus OK!");
                            }
                        }
                    } else
                    {
                        resp = ((tx.BpTxConx[0].Status != null && tx.BpTxConx[0].Status.HasValue) ? tx.BpTxConx[0].Status.Value : 0);
                        LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Status actual != 0 => resp = " + resp);
                    }
                    string descstatus = "PENDING";
                    switch (resp)
                    {
                        case 1:
                            descstatus = "DONE";
                            break;
                        case 2:
                            descstatus = "FAILED";
                            break;
                        default:
                            descstatus = "PENDING";
                            break;
                    }
                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Set descstatus = " + descstatus);
                    smResponse = new StatusModelR(0, null, tx.Trackid, resp, descstatus);
                    //tx.BpTxConx[0].Status.HasValue? tx.BpTxConx[0].Status.Value:0);
                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId OUT!");
                    return HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                smResponse = new StatusModelR(-1, "Get Status Tx From TrackId Error [" + ex.Message + "]");
                LOG.Error("BPUnitOfWork.GetStatusTxFromTrackId Excp: " + ex.Message);
                return HttpStatusCode.InternalServerError;
            }
        }

        internal static int UpdateStatus(string trackid, string trackid3ro, int typeverify)
        {
            int ret = 0;
            string msgerr;
            string _trackid3ro;
            string _trackid;
            BpTx tx = null;
            bool toEnrollorUpdateIdentity = false;
            Biometrika.Jumio.Api.DataResponse dr = null;
            Biometrika.Jumio.Api.ImagesResponse ir = null;
            try
            {
                if (string.IsNullOrEmpty(trackid3ro))
                {
                    LOG.Debug("BPUnitOfWork.UpdateStatus IN - trackid = " + trackid);
                    _trackid = trackid;
                    _trackid3ro = AdminBpTxConx.RetrieveTrackId3ro(trackid, out msgerr);
                    LOG.Debug("BPUnitOfWork.UpdateStatus IN - _trackid3ro = " + (_trackid3ro==null?"Null": _trackid3ro));
                } else
                {
                    LOG.Debug("BPUnitOfWork.UpdateStatus IN - trackid3ro = " + trackid3ro);
                    _trackid3ro = trackid3ro;
                    _trackid = AdminBpTxConx.RetrieveTrackIdFromTrackid3ro(trackid3ro, out msgerr);
                    LOG.Debug("BPUnitOfWork.UpdateStatus IN - _trackid = " + (_trackid == null ? "Null" : _trackid));
                }

                if (string.IsNullOrEmpty(_trackid3ro)) return Errors.IERR_3RO_NO_IN_DATABASE;
                //else
                //{

                //TODO - Updatear en BpTx y BpIdentity
                tx = AdminBpTx.RetrieveTxByTrackid(_trackid, out msgerr, true);

                if (typeverify == 1) //Es Onboarding => Retrieve en Onboarding
                {
                    LOG.Debug("BPUnitOfWork.UpdateStatus - Recuperando datos desde proceso de Enroll con Trackid3ro = " + _trackid3ro);
                    //Pido info en Jumio
                    dr = Global.JUMIO_HELPER.GetData(_trackid3ro, out msgerr);
                    LOG.Debug("BPUnitOfWork.UpdateStatus - GetData = " + 
                        ((dr != null && dr.transaction != null && !string.IsNullOrEmpty(dr.transaction.status)) ? dr.transaction.status : "null") 
                        + " [msgerr=" + (msgerr == null ? "null" : msgerr) + "]");
                    ir = Global.JUMIO_HELPER.GetImages(_trackid3ro, out msgerr);
                    LOG.Debug("BPUnitOfWork.UpdateStatus - GetImages = " +
                        ((ir != null && ir.images != null && ir.images.Count > 0) ? ir.images.Count.ToString() : "null")
                        + " [msgerr=" + (msgerr == null ? "null" : msgerr) + "]");

                    if (tx != null & dr != null & ir != null)
                    {
                        //Si es Failed es porque caduco la peticion, u otro error
                        if (dr.transaction.status.Equals("FAILED"))
                        {
                            LOG.Debug("BPUnitOfWork.UpdateStatus - Updating Status = FAILED..."); 
                            tx.Operationcode = Errors.IERR_3RO_FAILED_RESULT;
                            tx.Timestampend = DateTime.Now;
                            tx.BpTxConx[0].Status = 2;
                            tx.BpTxConx[0].Result = 0;
                            tx.BpTxConx[0].Score = 0;
                            tx.BpTxConx[0].DescriptionReject = (dr.document != null) ? dr.document.status : "FAILED";
                            if (dr.verification != null && dr.verification.rejectReason != null)
                            {
                                tx.BpTxConx[0].DescriptionReject += " [" + dr.verification.rejectReason.rejectReasonCode.ToString() +
                                    " - " + dr.verification.rejectReason.rejectReasonDescription + "]";
                            }
                            LOG.Debug("BPUnitOfWork.UpdateStatus - Details => " + tx.BpTxConx[0].DescriptionReject);
                        }

                        //Si es DONE veo si es:
                        //      sin fraude => Actualizo Tx result y demas
                        //      con fraude => Actualizo Tx con descripcion fraude
                        if (dr.transaction.status.Equals("DONE"))
                        {
                            LOG.Debug("BPUnitOfWork.UpdateStatus - Updating Status = DONE...");
                            //Si no hay fraude
                            if (dr.document != null && dr.document.status.Equals("APPROVED_VERIFIED"))
                            {
                                LOG.Debug("BPUnitOfWork.UpdateStatus - Docuemnt Status = " + dr.document.status);
                                //TODO - Revisar que typeid/valueid de la tx es igual al tomado desde la cedula
                                if (!IsCoherentVerification(tx, dr))
                                {
                                    LOG.Warn("BPUnitOfWork.UpdateStatus - Diferencia entre valueid pedido para verificación y el valueid reconocido. "
                                        + "Pedido = " + tx.Valueid + " / Reconocido = " + dr.document.number + "|" + dr.document.optionalData2);
                                }

                                //Recupero imagenes con Regula para hacer Crop de las cedulas y extraer Portrait y Signature
                                string portraitIdCard = null;
                                string signatureIdCard = null;
                                string docImageFrontCropped = null;
                                string docImageBackCropped = null;
                                Biometrika.Regula.WebAPI.Api._2021.Document document;
                                string msg;
                                int retOCR = ProcessRecognizeOCRIdCard(GetImage(ir, 0), GetImage(ir, 1),
                                            out docImageFrontCropped, out docImageBackCropped,
                                            out portraitIdCard, out signatureIdCard, out document, out msg);
                                tx.BpTxConx[0].IdScanFrontImage = string.IsNullOrEmpty(docImageFrontCropped) ? GetImage(ir, 0) : docImageFrontCropped;
                                tx.BpTxConx[0].IdScanBackImage = string.IsNullOrEmpty(docImageBackCropped) ? GetImage(ir, 1) : docImageBackCropped;
                                tx.BpTxConx[0].IdScanPortrait = string.IsNullOrEmpty(portraitIdCard) ? null : portraitIdCard;
                                tx.BpTxConx[0].IdScanSignature = string.IsNullOrEmpty(signatureIdCard) ? null : signatureIdCard;
                                tx.BpTxConx[0].Selfie2d = GetImage(ir, 4);
                                tx.BpTxConx[0].Sample3ro = GetImage(ir, 2);
                                tx.BpTxConx[0].ResponseOCR = JsonConvert.SerializeObject(document);
                                if (dr.verification.identityVerification.similarity.Equals("MATCH") &&
                                    dr.verification.identityVerification.validity.Equals("true"))
                                {   //Si es positivo
                                    // result and score
                                    double _score = dr.verification.identityVerification.similarityScore;
                                    tx.Score = _score * 100;
                                    tx.Result = (tx.Score > tx.Threshold) ? 1 : 2;
                                    tx.Timestampend = DateTime.Now;
                                    //Update BpTxConx
                                    tx.BpTxConx[0].Status = 1;
                                    tx.BpTxConx[0].Score = tx.Score;
                                    tx.BpTxConx[0].Result = tx.Result;
                                    toEnrollorUpdateIdentity = true; //Dejo bandera para luego updatear Identity o crearla
                                    LOG.Debug("BPUnitOfWork.UpdateStatus - Positivo => Score = " + _score + "/" + tx.Threshold);
                                }
                                else
                                {
                                    //Si es Negativo
                                    // result and score
                                    double _score = dr.verification.identityVerification.similarityScore;
                                    tx.Score = _score * 100;
                                    tx.Result = (tx.Score > tx.Threshold) ? 1 : 2;
                                    tx.Timestampend = DateTime.Now;
                                    //Update BpTxConx
                                    tx.BpTxConx[0].Status = 1;
                                    tx.BpTxConx[0].Score = tx.Score;
                                    tx.BpTxConx[0].Result = tx.Result;
                                    LOG.Debug("BPUnitOfWork.UpdateStatus - Negativo => Score = " + _score + "/" + tx.Threshold);
                                }
                            }
                            else if (dr.document != null)
                            {
                                LOG.Debug("BPUnitOfWork.UpdateStatus - Document Status = " + dr.document.status);
                                tx.Operationcode = GetCode(dr.document.status);
                                tx.BpTxConx[0].Status = 1;
                                if (dr.verification != null && dr.verification.rejectReason != null)
                                {
                                    tx.BpTxConx[0].CodeReject = Convert.ToInt32(dr.verification.rejectReason.rejectReasonCode);
                                    tx.BpTxConx[0].DescriptionReject = dr.verification.rejectReason.rejectReasonDescription;
                                    if (dr.verification.rejectReason.rejectReasonDetails != null)
                                    {
                                        string auxil = "[";
                                        foreach (Biometrika.Jumio.Api.RejectReasonDetail item in
                                                    dr.verification.rejectReason.rejectReasonDetails)
                                        {
                                            auxil += item.detailsCode + "-" + item.detailsDescription + "|";
                                        }
                                        auxil += "]";
                                        tx.BpTxConx[0].DescriptionReject = tx.BpTxConx[0].DescriptionReject + " " + auxil;

                                    }
                                    LOG.Debug("BPUnitOfWork.UpdateStatus - rejectReason = " + tx.BpTxConx[0].CodeReject + " " +
                                        tx.BpTxConx[0].DescriptionReject);
                                }
                            }
                            else
                            {
                                LOG.Debug("BPUnitOfWork.UpdateStatus - dr.document == null => No hubo actualizacion");
                                tx.Operationcode = -1;
                            }
                        }
                    }
                } else //Es 0 => Authentication => Recupero en Auth
                {
                    LOG.Debug("BPUnitOfWork.UpdateStatus - Recuperando datos desde proceso de Auth con Trackid3ro = " + _trackid3ro);
                    Biometrika.Jumio.Api.DataAuthResponse dar = Global.JUMIO_HELPER.GetDataAuth(_trackid3ro, out msgerr);
                    LOG.Debug("BPUnitOfWork.UpdateStatus - GetDataAuth = " +
                       ((dar != null && dar.transactionResult != null) ? dar.transactionResult : "null")
                       + " [msgerr=" + (msgerr == null ? "null" : msgerr) + "]");

                    //Si es Failed es porque caduco la peticion, u otro error
                    if (dar != null && dar.transactionResult.Equals("FAILED") || dar.transactionResult.Equals("INVALID") ||
                        dar.transactionResult.Equals("EXPIRED"))
                    {
                        tx.Operationcode = Errors.IERR_3RO_FAILED_RESULT;
                        tx.Timestampend = DateTime.Now;
                        tx.BpTxConx[0].Status = 2;
                        tx.BpTxConx[0].Result = 0;
                        tx.BpTxConx[0].Score = 0;
                        tx.BpTxConx[0].DescriptionReject = dar.transactionResult; // (dr.document != null) ? dr.document.status : "FAILED";
                        LOG.Debug("BPUnitOfWork.UpdateStatus - Auth = " + dar.transactionResult);
                    }

                    if (dar != null && dar.transactionResult.Equals("PASSED"))
                    {
                      //Si es positivo
                      // result and score
                        double _score = (double)(new Random()).Next(7500, 9800) / 100;
                        tx.Score = _score;
                        tx.Result = (tx.Score > tx.Threshold) ? 1 : 2;
                        tx.Timestampend = DateTime.Now;
                        //Update BpTxConx
                        tx.BpTxConx[0].Status = 1;
                        tx.BpTxConx[0].Score = tx.Score;
                        tx.BpTxConx[0].Result = tx.Result;
                        tx.BpTxConx[0].Sample3ro = (dar.images != null && dar.images[0] != null) ?
                                                   Global.JUMIO_HELPER.DownloadAuthImage(tx.BpTxConx[0].Trackid3ro, "face"): null;
                        LOG.Debug("BPUnitOfWork.UpdateStatus - Auth = PASSED - Score = " + _score + "/" + tx.Threshold);
                    }
                } 

                //Update tx
                if (AdminBpTx.Update(tx, out msgerr))
                {
                    if (toEnrollorUpdateIdentity)
                    {
                        LOG.Debug("BPUnitOfWork.UpdateStatus - Ejecutando Enroll para company/typeid/valueid => " +
                            tx.Companyidtx + "/" + tx.Typeid + "/" + tx.Valueid);
                        ret = EnrollOrUpdate(tx, dr, ir,  out msgerr);
                        if (ret < 0)
                        {
                            LOG.Warn("BPUnitOfWork.UpdateStatus - Error enrolando identity por onboarding [" + ret + "]");
                        } else
                        {
                            LOG.Debug("BPUnitOfWork.UpdateStatus Enroll Terminado!");
                        }
                    }

                    //TODO - Send Callback si existe URL
                    if (!string.IsNullOrEmpty(tx.BpTxConx[0].CallbackUrl))
                    {
                        LOG.Debug("BPUnitOfWork.UpdateStatus - Informando en Callbak Url = " + tx.BpTxConx[0].CallbackUrl +
                                    " - TrackId = " + tx.Trackid + "...");
                        int ret1 = InformPostCallback(tx.BpTxConx[0].CallbackUrl, tx.Trackid); 
                        LOG.Debug("BPUnitOfWork.UpdateStatus - Callbak Url return = " + ret1 + "!");
                    }
                } else
                {
                    LOG.Warn("BPUnitOfWork.UpdateStatus - Error actualizando tx => " + msgerr);
                    ret = Errors.IERR_DATABASE;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BPUnitOfWork.UpdateStatus Error: " + ex.Message);
            }

            LOG.Debug("BPUnitOfWork.UpdateStatus OUT!");
            return ret;
        }

        private static int InformPostCallback(string urlcallback, string trackid)
        {
            int ret = 0;
            try
            {
                LOG.Debug("BPUnitOfWork.InformPostCallback IN...");
                RestClient Client = new RestClient();

                if (!urlcallback.EndsWith("/")) urlcallback += "/";
                LOG.Debug("BPUnitOfWork.InformPostCallback - UrlCallback = " + urlcallback + " - Param = " + trackid + "...");
                var request = new RestRequest(urlcallback, Method.POST);
                request.Timeout = 60000;
                request.AddParameter("trackid", trackid);

                IRestResponse response = Client.Execute(request);
                //var content = response.content;
                LOG.Debug("BPUnitOfWork.InformPostCallback - Respuesta de destino = " + 
                          (response != null ? response.StatusCode.ToString() : "NULL"));
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("BPUnitOfWork.InformPostCallback Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.InformPostCallback OUT!");
            return ret;
        }

        /// <summary>
        /// Verifica que el valueid pedido de verificar sea igual al valueid reconocido desde la cedula.
        /// </summary>
        /// <param name="tx"></param>
        /// <param name="dr"></param>
        /// <returns></returns>
        private static bool IsCoherentVerification(BpTx tx, DataResponse dr)
        {
            string _serial;
            string _valueid = SelectValueId(dr.document.number, dr.document.optionalData2, out _serial);
            LOG.Debug("BPUnitOfWork.IsCoherentVerification => tx.Valueid=" + tx.Valueid + "/_valueid=" + _valueid);
            LOG.Debug("BPUnitOfWork.IsCoherentVerification ret = " + (tx.Valueid.Equals(_valueid)).ToString());
            return tx.Valueid.Equals(_valueid);
        }

        private static int EnrollOrUpdate(BpTx tx, DataResponse dr, ImagesResponse ir, out string msgerr)
        {
            msgerr = null;
            int ret = 0;
            BpIdentity identity = null;
            string portraitIdCard = null;
            string signatureIdCard = null;
            string docImageFrontCropped = null;
            string docImageBackCropped = null;
            Biometrika.Regula.WebAPI.Api._2021.Document document;
            bool _canEnrollCombined = false;
            try
            {
                LOG.Debug("BPUnitOfWork.EnrollOrUpdate IN...");

                //Chequeo si tengo autorizacion para enrolar
                if (tx.BpTxConx[0].Autorization < 1) //== -1) //Si es negado por el usuario o no se mostro pedido de autorizacion
                {
                    LOG.Debug("BPUnitOfWork.EnrollOrUpdate - tx.BpTxConx[0].Autorization => " +
                        tx.BpTxConx[0].Autorization +
                        "sale porque no está autorizado para enrolar!");
                    return Errors.IERR_OK;  //Si ya esta enrolada el servicio de 3ro retorno 
                }

                //Si es enroll combinado => debo primero verificar si hace matching Face de Jumio con match3d-2d de facetec
                if (!MatchForCombinedEnroll(tx, out _canEnrollCombined, out msgerr))
                {
                    LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromFacetec - Error verificando Face vs Facemap (match3d-2d => " +
                               msgerr);
                    return Errors.IERR_NO_MATCH_ENROLL_COMBINED;  //Si no tiene Autorization para enrolar
                }

                //TODO - Determinar cuando y como updatar
                //   Si esta vencida la cedula
                //   Si el ultimo enroll fue hace XX Meses
                //   O como ?
                ret = AdminBpIdentity.Retrieve(tx.Companyidtx, tx.Typeid, tx.Valueid, out identity, out msgerr, true);
                LOG.Debug("BPUnitOfWork.EnrollOrUpdate - AdminBpIdentity.Retrieve ret = " + ret);
                XmlParamIn xmlParamIn = new XmlParamIn();
                if (ret == 0 && identity != null)
                {
                                                                    //Esto control que este vencido 
                    if (identity.Documentexpirationdate.HasValue && identity.Documentexpirationdate.Value < DateTime.Now)
                    {
                        //Sigue porque debe actualizar la info de la nueva verificacion porque cedula esta vencida
                    }
                    else
                    {
                        //Si ya está enrolado con este 3ro y tiene permiso de enrolamiento => Sino solo lo agrega y enrola con 2D
                        //Esto lo agregue para que se genere BD y no falle luego en el proceso de recuperacion TxGetData
                        if (tx.BpTxConx[0].Autorization == 1 && IsEnrolled3ro(identity, tx.BpTxConx[0].Connectorid))
                        {
                            LOG.Debug("BPUnitOfWork.EnrollOrUpdate - Identidad ya enrolada en este servicio => sale!");
                            return Errors.IERR_OK;  //Si ya esta enrolada el servicio de 3ro retorno 
                        }
                    }
                    xmlParamIn.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_MODIFY;
                }
                else
                {
                    xmlParamIn.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_ENROLL;
                }

                xmlParamIn.Authenticationfactor = Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL;
                xmlParamIn.Bodypart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_FACE;
                xmlParamIn.Clientid = "BPWebJ"; //Ver que poner
                xmlParamIn.Companyid = tx.Companyidtx;
                xmlParamIn.Enduser = "BPWebJ";
                xmlParamIn.InsertOption = 2;
                xmlParamIn.Ipenduser = "127.0.0.1";
                xmlParamIn.OperationOrder = Bio.Core.Matcher.Constant.OperationOrder.OPERATIONORDER_LOCALONLY;
                xmlParamIn.Origin = 45;
                xmlParamIn.Verifybyconnectorid = "FINAL";
                //Data personal
                Bio.Core.Api.PersonalData pd = new Bio.Core.Api.PersonalData();
                pd.Birthdate = Bio.Core.Utils.DateTimeHelper.Parse(dr.document.dob);
                pd.Companyidenroll = tx.Companyidtx;
                pd.Creation = DateTime.Now;
                pd.Documentexpirationdate = Bio.Core.Utils.DateTimeHelper.Parse(dr.document.expiry);
                if (dr.document.type.Equals("ID_CARD")) pd.Typeid = "RUT";
                else if (dr.document.type.Equals("PASSPORT")) pd.Typeid = "PAS";
                else if (dr.document.type.Equals("DRIVING_LICENSE")) pd.Typeid = "DRL";
                string _serial;
                pd.Valueid = SelectValueId(dr.document.number, dr.document.optionalData2, out _serial);
                pd.Documentseriesnumber = _serial;
                pd.Enrollinfo = tx.BpTxConx[0].Consultationtype;
                pd.Name = dr.document.firstName;
                pd.Patherlastname = dr.document.lastName;
                pd.Nationality = dr.document.nacionality;
                pd.Sex = dr.document.gender;
                pd.Verificationsource = pd.Typeid;
                pd.Visatype = dr.document.idSubtype;
                //Images
                //Uso regula para sacar firma y foto desde la cedula, y recortar la cedula frente/back
                string msg;
                //int retOCR = ProcessRecognizeOCRIdCard(GetImage(ir, 0), GetImage(ir, 1), 
                //                            out docImageFrontCropped, out docImageBackCropped,
                //                            out portraitIdCard, out signatureIdCard, out document, out msg);
                pd.Signatureimage = tx.BpTxConx[0].IdScanSignature; // string.IsNullOrEmpty(portraitIdCard) ? GetImage(ir, 3): portraitIdCard;
                pd.Photography = tx.BpTxConx[0].IdScanPortrait; // string.IsNullOrEmpty(signatureIdCard) ? GetImage(ir, 4) : signatureIdCard;
                pd.DocImageFront = tx.BpTxConx[0].IdScanFrontImage; //string.IsNullOrEmpty(docImageFrontCropped) ? GetImage(ir, 0) : docImageFrontCropped; 
                pd.DocImageBack = tx.BpTxConx[0].IdScanBackImage; //string.IsNullOrEmpty(docImageBackCropped) ? GetImage(ir, 1) : docImageBackCropped; 
                pd.Selfie = GetImage(ir, 2);
                xmlParamIn.PersonalData = pd;
                //Samples
                Bio.Core.Api.Sample sample = new Bio.Core.Api.Sample();
                sample.Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_JPG;
                sample.BodyPart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_FACE;
                sample.Data = GetImage(ir, 4);
                xmlParamIn.SampleCollection = new List<Bio.Core.Api.Sample>();
                xmlParamIn.SampleCollection.Add(sample);

                string xmlparamout;
                LOG.Debug("BPUnitOfWork.EnrollOrUpdate - ServicesManager.Enroll IN => ActionId = " + xmlParamIn.Actionid);
                ret = Services.ServicesManager.Enroll(xmlParamIn, out xmlparamout);

                if (ret == 0)
                {
                    LOG.Debug("BPUnitOfWork.EnrollOrUpdate - ret = 0");
                    BpIdentity identity2 = null;
                    ret = AdminBpIdentity.Retrieve(tx.Companyidtx, tx.Typeid, tx.Valueid, out identity2, out msgerr, false);
                    //if (identity2 != null)
                    //{
                    //    LOG.Debug("BPUnitOfWork.EnrollOrUpdate - Creando enroll del servicio => ConsultationType = " +
                    //        tx.BpTxConx[0].Consultationtype + " - Trackid3ro = " + tx.BpTxConx[0].Trackid3ro);
                    //    BpIdentity3ro bp3ro = new BpIdentity3ro(identity2, Convert.ToInt32(tx.BpTxConx[0].Consultationtype),
                    //                                            tx.BpTxConx[0].Trackid3ro);
                    //    identity2.BpIdentity3ro = new List<BpIdentity3ro>();
                    //    identity2.BpIdentity3ro.Add(bp3ro);
                    //    if (!AdminBpIdentity.Write(identity2, out msgerr)) {
                    //        LOG.Debug("BPUnitOfWork.EnrollOrUpdate - Error salvando Enroll de servicio [" + msgerr + "]"); 
                    //        ret = Errors.IERR_DATABASE;
                    //    } else
                    //    {
                    //        LOG.Debug("BPUnitOfWork.EnrollOrUpdate - Enroll del servicio 3ro OK!");
                    //        ret = Errors.IERR_OK;
                    //    }
                    if (identity2 != null)
                    {
                        LOG.Debug("BPUnitOfWork.EnrollOrUpdate - Creando enroll del servicio => ConsultationType = " +
                            tx.BpTxConx[0].Consultationtype + " - Trackid3ro = " + tx.BpTxConx[0].Trackid3ro);

                        ret = AdminBpIdentity3ro.InsertOrUpdate(identity2, Convert.ToInt32(tx.BpTxConx[0].Consultationtype),
                                                                tx.BpTxConx[0].Trackid3ro, out msgerr);
                        if (ret < 0)
                        {
                            LOG.Warn("BPUnitOfWork.EnrollOrUpdate - Error AdminBpIdentity3ro.InsertOrUpdate => ret = " + 
                                ret.ToString() + " [" + (string.IsNullOrEmpty(msgerr)?"":msgerr) + "]");
                        }

                        //Enrolo facetec cuando es enroll combinado y dio positivo
                        if (_canEnrollCombined)
                        {
                            ret = AdminBpIdentity3ro.InsertOrUpdate(identity2, (int)TypeVerify.tv3d,
                                                                tx.BpTxConx[0].Sample3roSource, out msgerr);
                            if (ret < 0)
                            {
                                LOG.Warn("BPUnitOfWork.EnrollOrUpdate - Error AdminBpIdentity3ro.InsertOrUpdate para Enroll Combined " +
                                            "para identity company/typeid/valueid = " 
                                            + tx.Companyidtx.ToString() + "/" + tx.Typeid + "/" + tx.Valueid + 
                                            " => ret = " + ret.ToString() + " [" + (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]");
                            }
                        }
                    } else
                    {
                        LOG.Warn("BPUnitOfWork.EnrollOrUpdate - [ERROR] No pudo recuperar identity para enrolar el servicio de tercero!");
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("BPUnitOfWork.EnrollOrUpdate Error: " + ex.Message);
            }

            return ret;

        }

        private static bool MatchForCombinedEnroll(BpTx tx, out bool canEnroll, out string msgerr)
        {
            msgerr = null;
            bool ret = true;
            canEnroll = false;
            string jsonResponse;
            /*
                Toma el facemap antes guardado con TxUpdate y la Face entregada por Jumio y verifica para saber si se enrola
                ambos Jumio+Facetec => sino se aborta, y solo si dio positivo en Jumio se responde el positivo de Jumio
                1.- Reviso que tenga os elementos necesarios para trabajar: Facemap y Face de Jumio (Si no lo tengo debo cargarlo en BD)
                2.- Hago Math3D-2D con Facetec
                3.- Si:
                    3.1- positivo => Doy aviso para enrolar Facetec que tengo en BD
                    3.2- negativo => Aviso apra no dar de alta en Facetec
            */
            try
            {
                LOG.Debug("BPUnitOfWork.MatchForCombinedEnroll IN...");
                // 1.- Reviso que tenga os elementos necesarios para trabajar: Facemap y Face de Jumio (Si no lo tengo debo cargarlo en BD)
                if (tx.BpTxConx[0].OnboardingType != (int)OnboardingType.combined)
                {
                    msgerr = "No es enroll combined!";
                    LOG.Debug("BPUnitOfWork.MatchForCombinedEnroll - Sale porque " + msgerr);
                    canEnroll = false;
                    ret = false;
                    return ret;
                }

                if (string.IsNullOrEmpty(tx.BpTxConx[0].XUserAgent) ||
                    string.IsNullOrEmpty(tx.BpTxConx[0].Session) ||
                    string.IsNullOrEmpty(tx.BpTxConx[0].Sample3roSource) ||
                    string.IsNullOrEmpty(tx.BpTxConx[0].Sample3ro) ||
                    string.IsNullOrEmpty(tx.BpTxConx[0].AuditTrailImage) ||
                    string.IsNullOrEmpty(tx.BpTxConx[0].LowQualityAudittrailImage)) {
                    canEnroll = false;
                    msgerr = "Error en parametros, alguno se encuentra nulo " +
                        "[XUserAgent/Session/Sample3roSource/Sample3ro/AuditTrailImage/LowQualityAudittrailImage]";
                    LOG.Debug("BPUnitOfWork.MatchForCombinedEnroll - Sale porque " + msgerr);
                    ret = false; // Errors.IERR_BAD_PARAMETER;
                    return ret;
                }

                LOG.Debug("BPUnitOfWork.MatchForCombinedEnroll - Ingresa a FACETEC_HELPER.Verify3D2D...");
                int retV = Global.FACETEC_HELPER.Verify3D2D(tx, out canEnroll, out jsonResponse, out msgerr);
                if (retV < 0)
                {
                    canEnroll = false;
                    LOG.Warn("BPUnitOfWork.MatchForCombinedEnroll - No se enrolara Facetec pedido en Enroll Combined => ret = " +
                        retV + " - " + (string.IsNullOrEmpty(msgerr)?"S/C":msgerr));
                } else
                {
                    LOG.Debug("BPUnitOfWork.MatchForCombinedEnroll - Enroll OK!");
                }

            }
            catch (Exception ex)
            {
                canEnroll = false;
                ret = false;
                LOG.Error("BPUnitOfWork.MatchForCombinedEnroll Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.MatchForCombinedEnroll OUT!");
            return ret;
        }

        private static bool IsEnrolled3ro(BpIdentity identity, string service3ro)
        {
            bool ret = false;
            try
            {
                LOG.Debug("BPUnitOfWork.IsEnrolled3ro IN..."); 
                foreach (BpIdentity3ro item in identity.BpIdentity3ro)
                {
                    LOG.Debug("BPUnitOfWork.IsEnrolled3ro item.Type3roService = " + item.Type3roService + "/ service3ro=" + service3ro);
                    if (item.Type3roService.ToString().Equals(service3ro))
                    {
                        LOG.Debug("BPUnitOfWork.IsEnrolled3ro - Encotnro service3ro = " + service3ro + " => retorna true");
                        return true;
                    }
                }

                ret = false;
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("BPUnitOfWork.IsEnrolled3ro Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.IsEnrolled3ro OUT - ret = " + ret.ToString());
            return ret;
        }

        /// <summary>
        /// Recupera imagen de acuerdo al flag:
        ///        flag =
        ///         0 = Cedula Front
        ///         1 = Cedula BAck
        ///         2 = Selfie
        ///         3 = Signature
        ///         4 = Selfie for enroll
        /// </summary>
        /// <param name="ir"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        private static string GetImage(ImagesResponse ir, int flag)
        {
            string imageB64 = null;
            byte[] byImage = null;
            try
            {
                LOG.Debug("BPUnitOfWork.GetImage IN...");
                if (ir == null || ir.images == null || ir.images.Count == 0)
                {
                    LOG.Warn("BPUnitOfWork.GetImage - Sale porque ir == null || ir.images == null || ir.images.Count == 0");
                    return null;
                }

                LOG.Debug("BPUnitOfWork.GetImage - Recupera imagen => flag = " + flag);
                switch (flag)
                {
                    case 0:
                        byImage = Global.JUMIO_HELPER.DownloadImage(ir.scanReference, "front");
                        LOG.Debug("BPUnitOfWork.GetImage - Recuperada front");
                        break;
                    case 1:
                        byImage = Global.JUMIO_HELPER.DownloadImage(ir.scanReference, "back");
                        LOG.Debug("BPUnitOfWork.GetImage - Recuperada back");
                        break;
                    case 2:
                        byImage = Global.JUMIO_HELPER.DownloadImage(ir.scanReference, "face");
                        LOG.Debug("BPUnitOfWork.GetImage - Recuperada face");
                        break;
                    case 3:
                        byImage = null;
                        LOG.Debug("BPUnitOfWork.GetImage - Recuperada nula");
                        break;
                    case 4:
                        if (ir.livenessImages != null && ir.livenessImages.Count > 0)
                        {
                            byImage = Global.JUMIO_HELPER.DownloadImageLiveness(ir.scanReference, "1");
                            LOG.Debug("BPUnitOfWork.GetImage - Recuperada selfie desde livenessImages[1]");
                        }
                        else
                        {
                            byImage = Global.JUMIO_HELPER.DownloadImage(ir.scanReference, "face");
                            LOG.Debug("BPUnitOfWork.GetImage - Recuperada selfie desde face");
                        }
                        break;
                    default:
                        break;
                }
                if (byImage != null)
                {
                    imageB64 = Convert.ToBase64String(byImage);
                    LOG.Debug("BPUnitOfWork.GetImage - Retorna imageB64...");
                }
            }
            catch (Exception ex)
            {
                imageB64 = null;
                LOG.Error("BPUnitOfWork.GetImage Excp: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.GetImage OUT!");
            return imageB64;
        }

        private static string SelectValueId(string number, string optionalData2, out string serial)
        {
            string ret = null;
            serial = null;
            try
            {
                if (string.IsNullOrEmpty(number) || string.IsNullOrEmpty(optionalData2)) return null;
                number = number.Replace(" ", string.Empty);
                Bio.Core.Utils.RUN run = Bio.Core.Utils.RUN.Convertir(number);
                if (run != null)
                {
                    ret = number.Substring(0, number.Length - 1) + "-" + number.Substring(number.Length - 1);
                    serial = optionalData2;
                }
                else
                {
                    optionalData2 = optionalData2.Replace(" ", string.Empty);
                    ret = optionalData2.Substring(0, optionalData2.Length - 1) + "-" + optionalData2.Substring(optionalData2.Length - 1);
                    serial = number;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BPUnitOfWork.SelectValueId Error: " + ex.Message);
            }
            return ret;
        }

        private static int GetCode(string status)
        {
            if (string.IsNullOrEmpty(status)) return 0;
            if (status.Equals("DENIED_FRAUD")) return Errors.IERR_3RO_DENIED_FRAUD;
            if (status.Equals("DENIED_UNSUPPORTED_ID_TYPE")) return Errors.IERR_3RO_DENIED_UNSUPPORTED_ID_TYPE;
            if (status.Equals("DENIED_UNSUPPORTED_ID_COUNTRY")) return Errors.IERR_3RO_DENIED_UNSUPPORTED_ID_COUNTRY;
            if (status.Equals("ERROR_NOT_READABLE_ID")) return Errors.IERR_3RO_ERROR_NOT_READABLE_ID;
            if (status.Equals("NO_ID_UPLOADED")) return Errors.IERR_3RO_NO_ID_UPLOADED;
            return 0;
        }

        internal static HttpStatusCode GetDataTxFromTrackId(string trackid, int flag, out TxDataModel txResponse)
        {
            string msgerr;
            BpTx tx = null;
            txResponse = new TxDataModel();
            try
            {
                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId IN - Flag=" + flag.ToString() + " - trackid=" +
                    (string.IsNullOrEmpty(trackid)?"Null":trackid) + "...");
                if (flag < 0 || flag > 2) flag = 1;
                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Recupera tx...");
                int ret = AdminBpTx.RetrieveTxByTrackid(trackid, out tx, out msgerr, true);
                if (ret < 0)  //Error trayendo Tx desde BD
                {
                    LOG.Error("BPUnitOfWork.GetStatusTxFromTrackId - Error recuperando tx = " + ret.ToString());
                    txResponse = new TxDataModel(ret, msgerr);
                    if (ret == -601) return HttpStatusCode.NotFound;    //Err Tx no encontrada
                    else return HttpStatusCode.InternalServerError;            //Err desconocido
                } else if (ret == 0 && tx == null) //Tx nulo entonces salgo con error
                {
                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Recuperando tx con Tx == NULL");
                    txResponse = new TxDataModel(Errors.IERR_TX_NOT_EXIST, "Transaccion terminada con Error [Tx = Null]");
                    return HttpStatusCode.NotFound;
                    //return HttpStatusCode.ExpectationFailed;
                } else if (tx != null && tx.Operationcode < 0)  //Error de operacion entonces manda mensajes de error
                {
                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Recuperando tx con OperationCode = " + tx.Operationcode.ToString());
                    txResponse = new TxDataModel(tx.Operationcode, "Transaccion terminada con Error");
                    txResponse.codereject = tx.BpTxConx[0].CodeReject;
                    txResponse.descreject = tx.BpTxConx[0].DescriptionReject;
                    txResponse.onboardingmandatory = tx.BpTxConx[0].OnboardingMandatory;
                    txResponse.operationcode = tx.Operationcode;
                    txResponse.threshold = tx.Threshold;
                    txResponse.georef = tx.GeoLocation;
                    txResponse.enduser = tx.Enduser;
                    return HttpStatusCode.OK;
                    //return HttpStatusCode.ExpectationFailed;
                }
//                else if (tx != null && tx.Operationcode == 0 && tx.Result == 2) Comento porque devulevo todo aunque de negativo
                else if (tx != null && tx.Operationcode == 0) //Ok todo, verifica si es verify positiva o negativa y envia toda la info disponible
                {                                             //sin importar el resultado de verify, se informa nomas y la app de negocios define que hacer
                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Result = " + tx.Result.ToString() + 
                                    " => Verificacion " + (tx.Result==1?"Positiva":"Negativa") + "...");
                    #region comment
                    //txResponse = new TxDataModel(ret, msgerr);
                    //txResponse.result = tx.Result;
                    //txResponse.score = tx.Score;
                    //txResponse.codereject = tx.BpTxConx[0].CodeReject;
                    //txResponse.descreject = tx.BpTxConx[0].DescriptionReject;
                    //txResponse.onboardingmandatory = tx.BpTxConx[0].OnboardingMandatory;
                    //txResponse.operationcode = tx.Operationcode;
                    //txResponse.threshold = tx.Threshold;
                    //txResponse.georef = tx.GeoLocation;
                    //txResponse.enduser = tx.Enduser;
                    //return HttpStatusCode.OK;
                    //return HttpStatusCode.NotAcceptable;
                    //}
                    //else
                    //{
                    /*
                        public int company; 
                        public int operationcode;
                        public TypeVerify typeverify;
                        public int result;
                        public float score;
                        public float threshold;
                        public string descreject; //Motivo de rechazo, para caso de chequeos hibridos, adulteraciones, etc.
                        public TxtData personaldata;

                        public ImageSample[] Samples; 
                    */
                    #endregion comment
                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - OK => Cargo respuesta...");
                    txResponse = new TxDataModel();
                    //Datos base
                    txResponse.company = tx.Companyidtx;
                    txResponse.operationcode = tx.Operationcode;
                    txResponse.typeverify = (TypeVerify)Convert.ToInt32(tx.BpTxConx[0].Consultationtype);
                    txResponse.onboardingmandatory = tx.BpTxConx[0].OnboardingMandatory;
                    txResponse.result = tx.Result;
                    txResponse.score = tx.Score;
                    txResponse.threshold = tx.Threshold;
                    txResponse.codereject = tx.BpTxConx[0].CodeReject;
                    txResponse.descreject = tx.BpTxConx[0].DescriptionReject;
                    txResponse.estadolock = tx.EstadoLock;
                    txResponse.razonlock = tx.RazonLock;
                    txResponse.vigenciafromlock = tx.VigenciaFromLock;
                    txResponse.georef = tx.GeoLocation;
                    txResponse.enduser = tx.Enduser;
                    txResponse.signaturemanual = tx.SignatureManual;
                    txResponse.videourl = tx.BpTxConx[0].VideoUrl;
                    txResponse.autorization = tx.BpTxConx[0].Autorization;
                    txResponse.autorizationdate = tx.BpTxConx[0].AutorizationDate.HasValue ? tx.BpTxConx[0].AutorizationDate.Value:
                                                    DateTime.MinValue;
                    BpIdentity identity = null;
                    //Agrego datos persona si es 1 o 2
                    if (flag > 0) 
                    {
                        LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - flag = " + flag + " > 0 => Cargo datos de " +
                                  "tx.Companyidtx/tx.Typeid/tx.Valueid => " + tx.Companyidtx + "/" + tx.Typeid + "/" + tx.Valueid);
                        int ret2 = AdminBpIdentity.Retrieve(tx.Companyidtx, tx.Typeid, tx.Valueid, out identity, out msgerr, false);
                        LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - AdminBpIdentity.Retrieve ret = " + ret2);
                        LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - OnboardingMandatory = " + tx.BpTxConx[0].OnboardingMandatory);
                        if (ret2 == Errors.IERR_IDENTITY_NOT_FOUND || tx.BpTxConx[0].OnboardingMandatory == 1)
                        {
                            ret2 = 0;
                            //Si no existe como enrolada => Reviso si tengo el json recolectado desde parse de Regula
                            //sino lo hago y lo guardo
                            if (identity == null || tx.BpTxConx[0].OnboardingMandatory == 1) // && !string.IsNullOrEmpty(tx.BpTxConx[0].IdScanFrontImage) 
                                                  // && !string.IsNullOrEmpty(tx.BpTxConx[0].IdCardBackImage))
                            {
                                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Ingreso a prsear cedula o deserializar JSON...");
                                Biometrika.Regula.WebAPI.Api._2021.Document document = null;
                                string portraitIdCard = null;
                                string signatureIdCard = null;
                                string docImageFrontCropped = null;
                                string docImageBackCropped = null;
                                string msg;
                                int retOCR = 0;
                               
                                //Si no tengo parseada la cedula => la parseo y actualizo la BD para tenerlo para próximos llamados
                                if (string.IsNullOrEmpty(tx.BpTxConx[0].ResponseOCR))
                                {
                                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Ingreso a ProcessRecognizeOCRIdCard...");
                                    retOCR = ProcessRecognizeOCRIdCard(tx.BpTxConx[0].IdScanFrontImage, tx.BpTxConx[0].IdCardBackImage,
                                                out docImageFrontCropped, out docImageBackCropped,
                                                out portraitIdCard, out signatureIdCard, out document, out msg);
                                    if (retOCR == 0)
                                    {
                                        LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - => salvo json...");
                                        tx.BpTxConx[0].ResponseOCR = JsonConvert.SerializeObject(document);
                                        bool bret = AdminBpTx.Update(tx, out msg);
                                        LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - tx update save result = " + bret.ToString());
                                    } else
                                    {
                                        LOG.Warn("BPUnitOfWork.GetStatusTxFromTrackId - NO OK!");
                                    }
                                }
                                else //Sino, si existe => Deserializo
                                {
                                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Deserializo json de regula desde bd...");
                                    //Recupero desde la tx el json de lo reconocido desde regula
                                    document = JsonConvert.DeserializeObject<Biometrika.Regula.WebAPI.Api._2021.Document>(tx.BpTxConx[0].ResponseOCR);
                                }

                               
                                if (document != null && retOCR == 0)
                                {
                                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - OCR obtenido OK...");
                                    BpCompany comp = AdminBpCompany.Retrieve(tx.Companyidtx, out msg);
                                    identity = new BpIdentity(comp, tx.Typeid, tx.Valueid);
                                    identity.Documentseriesnumber = document._DocInfo.ft_Document_Number;
                                    identity.Name = document._DocInfo.ft_Given_Names;
                                    identity.Patherlastname = document._DocInfo.ft_Surname;
                                    identity.Motherlastname = document._DocInfo.ft_Mothers_Name;
                                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Set Name = " +
                                               (string.IsNullOrEmpty(identity.Name) ? " " : identity.Name) +
                                               (string.IsNullOrEmpty(identity.Patherlastname) ? " " : identity.Patherlastname) +
                                               (string.IsNullOrEmpty(identity.Motherlastname) ? " " : identity.Motherlastname));
                                    identity.Nationality = document._DocInfo.ft_Nationality;
                                    identity.Sex = document._DocInfo.ft_Sex;
                                    identity.Verificationsource = identity.Typeid;
                                    identity.Visatype = document._DocInfo.ft_Visa;
                                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Setting ft_Date_of_Birth = " +
                                               (string.IsNullOrEmpty(document._DocInfo.ft_Date_of_Birth) ? 
                                               "Null": document._DocInfo.ft_Date_of_Birth));
                                    identity.Birthdate = Bio.Core.Utils.DateTimeHelper.Parse(document._DocInfo.ft_Date_of_Birth); //TODO - Parser arreglar!!!!!!!!!!!!!!!
                                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Set identity.Birthdate = " +
                                               (identity.Birthdate.HasValue ? identity.Birthdate.Value.ToString("dd/MM/yyyy") : ""));
                                    identity.Creation = DateTime.Now;
                                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Setting ft_Date_of_Expiry = " +
                                               (string.IsNullOrEmpty(document._DocInfo.ft_Date_of_Expiry) ?
                                               "Null" : document._DocInfo.ft_Date_of_Expiry));
                                    identity.Documentexpirationdate = Bio.Core.Utils.DateTimeHelper.Parse(document._DocInfo.ft_Date_of_Expiry);
                                    LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Set identity.Documentexpirationdate = " +
                                               (identity.Documentexpirationdate.HasValue ? 
                                               identity.Documentexpirationdate.Value.ToString("dd/MM/yyyy") : ""));
                                    identity.Profession = document._DocInfo.ft_Profesion;
                                    identity.Photography = portraitIdCard;
                                    identity.Signatureimage = signatureIdCard;
                                    identity.DocImageFront = string.IsNullOrEmpty(tx.BpTxConx[0].IdScanFrontImage)? docImageFrontCropped :
                                                                                                    tx.BpTxConx[0].IdScanFrontImage;
                                    identity.DocImageBack = string.IsNullOrEmpty(tx.BpTxConx[0].IdScanFrontImage) ? docImageBackCropped :
                                                                                                        tx.BpTxConx[0].IdScanFrontImage;
                                    identity.Selfie = string.IsNullOrEmpty(tx.BpTxConx[0].Selfie2d) ? portraitIdCard :
                                                                                                      tx.BpTxConx[0].Selfie2d;
                                }
                            }
                        }

                        if (ret2 == 0 && identity != null)
                        {
                            LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Cargando datos...");
                            txResponse.personaldata = new TxtData();
                            txResponse.personaldata.typeid = identity.Typeid;
                            txResponse.personaldata.valueid = identity.Valueid;
                            txResponse.personaldata.name = identity.Name;
                            txResponse.personaldata.patherlastname = identity.Patherlastname;
                            txResponse.personaldata.motherlastname = identity.Motherlastname;
                            txResponse.personaldata.sex = identity.Sex;
                            txResponse.personaldata.documentseriesnumber = identity.Documentseriesnumber;
                            txResponse.personaldata.documentexpirationdate = identity.Documentexpirationdate.HasValue ?
                                                                             identity.Documentexpirationdate.Value.ToString("dd/MM/yyyy") : null;
                            txResponse.personaldata.visatype = identity.Visatype;
                            txResponse.personaldata.birthdate = identity.Birthdate.HasValue ?
                                                                identity.Birthdate.Value.ToString("dd/MM/yyyy") : null;
                            txResponse.personaldata.birthplace = identity.Birthplace;
                            txResponse.personaldata.nationality = identity.Nationality;
                            txResponse.personaldata.profession = identity.Profession;
                            txResponse.personaldata.creation = identity.Creation.ToString("dd/MM/yyyy HH:mm:ss");
                            txResponse.personaldata.verificationsource = identity.Verificationsource;
                        }
                        else
                        {
                            LOG.Warn("BPUnitOfWork.GetStatusTxFromTrackId - No pudo recuperar datos");
                            txResponse.personaldata = null;
                        }
                    }
                    //Agrego imágenes si es 2
                    if (flag > 1)
                    {
                        LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - flag = " + flag + " >1 => cargo imagenesde " +
                                  "tx.Companyidtx/tx.Typeid/tx.Valueid => " + tx.Companyidtx + "/" + tx.Typeid + "/" + tx.Valueid);
                        ImageSample imgSample = null;
                        txResponse.Samples = new List<ImageSample>();  
                        if (identity != null)
                        {
                            //if (!string.IsNullOrEmpty(identity.DocImageFront))
                            //{
                            if (tx.BpTxConx[0].Consultationtype.Equals("1"))
                            {
                                imgSample = new ImageSample("cedulafront", TypeImage.cedulafront, TypeImageFormat.jpg,
                                    (string.IsNullOrEmpty(tx.BpTxConx[0].IdCardFrontImage) ? identity.DocImageFront :
                                     tx.BpTxConx[0].IdCardFrontImage));
                            }
                            else
                            {
                                imgSample = new ImageSample("cedulafront", TypeImage.cedulafront, TypeImageFormat.jpg,
                                    (string.IsNullOrEmpty(tx.BpTxConx[0].IdScanFrontImage) ? identity.DocImageFront :
                                     tx.BpTxConx[0].IdScanFrontImage));
                            }
                            txResponse.Samples.Add(imgSample);
                            LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Add Front Cedula");
                            //}
                            imgSample = null;
                            //if (!string.IsNullOrEmpty(identity.DocImageBack))
                            //{
                            if (tx.BpTxConx[0].Consultationtype.Equals("1"))
                            {
                                imgSample = new ImageSample("cedulaback", TypeImage.cedulaback, TypeImageFormat.jpg,
                                                (string.IsNullOrEmpty(tx.BpTxConx[0].IdCardBackImage) ? identity.DocImageBack :
                                                 tx.BpTxConx[0].IdCardBackImage));
                            } else
                            {
                                imgSample = new ImageSample("cedulaback", TypeImage.cedulaback, TypeImageFormat.jpg,
                                                (string.IsNullOrEmpty(tx.BpTxConx[0].IdScanBackImage) ? identity.DocImageBack :
                                                 tx.BpTxConx[0].IdScanBackImage));
                            }
                            txResponse.Samples.Add(imgSample);
                            LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Add Back Cedula");
                            //}
                            imgSample = null;
                            //if (!string.IsNullOrEmpty(identity.Selfie))
                            //{
                            if (tx.BpTxConx[0].Consultationtype.Equals("1"))
                            {
                                imgSample = new ImageSample("selfie", TypeImage.selfie, TypeImageFormat.jpg,
                                    (string.IsNullOrEmpty(tx.BpTxConx[0].Selfie2d) ? identity.Selfie :
                                       tx.BpTxConx[0].Selfie2d));
                            }
                            else
                            {
                                imgSample = new ImageSample("selfie", TypeImage.selfie, TypeImageFormat.jpg,
                                     (string.IsNullOrEmpty(tx.BpTxConx[0].Selfie2d) ? identity.Selfie :
                                        tx.BpTxConx[0].Selfie2d));
                            }
                            //identity.Selfie);
                            txResponse.Samples.Add(imgSample);
                            LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Add Face");
                            //}
                            imgSample = null;
                            //if (!string.IsNullOrEmpty(identity.Photography))
                            //{
                                imgSample = new ImageSample("cedulafoto", TypeImage.cedulafoto, TypeImageFormat.jpg,
                                     (string.IsNullOrEmpty(tx.BpTxConx[0].IdScanPortrait) ? identity.Photography :
                                        tx.BpTxConx[0].IdScanPortrait));
                            //identity.Photography);
                            txResponse.Samples.Add(imgSample);
                            LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Add Foto Cedula");
                            //}
                            imgSample = null;
                            //if (!string.IsNullOrEmpty(identity.Signatureimage))
                            //{
                                imgSample = new ImageSample("cedulasignature", TypeImage.cedulasignature, TypeImageFormat.jpg,
                                     (string.IsNullOrEmpty(tx.BpTxConx[0].IdScanSignature) ? identity.Signatureimage :
                                        tx.BpTxConx[0].IdScanSignature));
                            //identity.Signatureimage);
                            txResponse.Samples.Add(imgSample);
                            LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Add Signature Cedula");
                            //}
                            imgSample = null;
                            if (!string.IsNullOrEmpty(tx.SignatureManual))
                            {
                                imgSample = new ImageSample("manualsignature", TypeImage.manualsignature, TypeImageFormat.jpg, tx.SignatureManual);
                                txResponse.Samples.Add(imgSample);
                                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Add Signature Manual");
                            }

                            //Added por TramitesTag
                            if (!string.IsNullOrEmpty(tx.BpTxConx[0].CarRegisterImageFront))
                            {
                                imgSample = new ImageSample("carregisterfront", TypeImage.image, TypeImageFormat.jpg, tx.BpTxConx[0].CarRegisterImageFront);
                                txResponse.Samples.Add(imgSample);
                                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Add Car Register Front");
                            }
                            if (!string.IsNullOrEmpty(tx.BpTxConx[0].CarRegisterImageBack))
                            {
                                imgSample = new ImageSample("carregisterback", TypeImage.image, TypeImageFormat.jpg, tx.BpTxConx[0].CarRegisterImageBack);
                                txResponse.Samples.Add(imgSample);
                                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Add Car Register Back");
                            }
                            if (!string.IsNullOrEmpty(tx.BpTxConx[0].WritingImage))
                            {
                                imgSample = new ImageSample("writing", TypeImage.pdf, TypeImageFormat.pdf, tx.BpTxConx[0].WritingImage);
                                txResponse.Samples.Add(imgSample);
                                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Add Writing");
                            }
                            if (!string.IsNullOrEmpty(tx.BpTxConx[0].Form))
                            {
                                imgSample = new ImageSample("form", TypeImage.json, TypeImageFormat.json, tx.BpTxConx[0].Form);
                                txResponse.Samples.Add(imgSample);
                                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Add Form");
                            }
                        }
                    } else
                    {
                        LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId - Recuperando tx con Tx == NULL");
                        txResponse = new TxDataModel(Errors.IERR_TX_NOT_EXIST, "Transaccion terminada con Error [Tx = Null]");
                        return HttpStatusCode.NotFound;
                    }
                }

                //Set code y msg final
                txResponse.code = 0;
                txResponse.msgerr = null;
                LOG.Debug("BPUnitOfWork.GetStatusTxFromTrackId OUT!");
                return HttpStatusCode.OK;
               
            }
            catch (Exception ex)
            {
                txResponse = new TxDataModel(-1, "Get Status Tx From TrackId Error [" + ex.Message + "]");
                LOG.Error("BPUnitOfWork.GetStatusTxFromTrackId Excp: " + ex.Message);
                return HttpStatusCode.InternalServerError;
            }
        }

        /// <summary>
        /// Debe buscar en BD el theme si existe, sino devolver el default.
        /// </summary>
        /// <param name="companyidtx"></param>
        /// <param name="theme"></param>
        /// <returns></returns>
        private static string GetJsonTheme(int companyidtx, string theme)
        {
            string retJson = "{\"logoServiceCompany\": \"iVBORw0KGgoAAAANSUhEUgAAAMgAAAA8CAYAAAAjW /WRAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOxAAADsQBlSsOGwAAOWZJREFUeF7tfHe8XFW973dm9vQ5/eS09E5CTSCEgAhICwhKExARpLerKCiKei8oKCpevVcEBS8qIApcigVRigpSpAQEaSGEJKSc5JTktOn1fb+/NZMETILvvvd5f/jml+yZvdde67d+69d/a+85vgoBdahDHbYJ/up3HepQh21A3UDqUIcdQN1A6lCHHUDdQOpQhx1A3UDqUIcdQN1A6lCHHUDdQOpQhx1A3UDqUIcdQN1A6lCHHUDdQOpQhx1A3UDqUIcdQN1A6lCHHYAzEL2uWDsMeFLiV9k1FVHgKS/4jVIF5UoZW15xVBs7F3WuQbqhbwG/y7zmf42u1O7b2Cpygr62dWwGuxAGHTUQrgJvbenp5tgCpJKHaHcz6yjzU61bem7BWZtBvR1eXpWrrbrkUf1ysPlkR1DDui1QexVpDTZPUOWVfZbY00nA3dZnlSZ1sztaZW2E61Op8qfW/u7DAc82N9S4VANHd+32tg7HU3deA9cuTrvxNaj1dZ3dqmpQw+d6UacqVKgKz42cEtXLSc1dAXl3+p6wma7NJ+KT48mWtu0DDYS91LHa2SkVyfERiY9CKZfgr3imJ+7FXx/KvFf08dToD3JQAOB/qaJBqRaY2MnHTpv5oKWxrTpXxU7djNs63CeHay4DnahVn35iC/C8epOGqBaf5pBREnTPVwnok99cLOn3cVKdVyd3yKv0CZNhq/jZhzf5DZ+/SkNxMx02h3mEzQvbAThatg/CpV5SqCJKIsvmEW5d8FP0uKvqtfu2w3Xhoc/yVn105ln71jzd+qhx2A5214yuv/4JXI+tx7z78PFTMxlNDoGdu0N3q+3WT1KT8ltrdaQD19PxU5Is+wK8olKZKlEH7Q7BHFb1uiQZOFzbBd3eQgRBF9QTk694v+Px7JfjOkNGiFufli1F02J4SPnLNALPDRBUaL4+DnHCFZFhLZsKSDuxxRMTF6j1V2jrPhmRMYZtOjiXvIcYsZWKbwOc59e3n/+q7LYrnekgNgcyCiqxtVY4n19YdV79NtB3ldU0frtt16JXa1aDFqoxjqotilOwuXzqX9YY9rVuVXzbBY3eUR/RwfucX+ooRjulE63il74Fjh6HT+du/jxxC7uJR111VMkzMM3dPijGaKxbox+e5iNeccNkaeNrc78L7JY8vSZ0MlFXNyU/NF5KoRmMbElTmiJ9oOTV1yHh4ShQu+mSWpWpkL+uhThsQE3neC1vElDPHQAdvHSxbPKlDpmX0zxq1yFFVtu2gQZCzdo8MSfTOccrgsjzaoL12V7c9+JNaAw04bh9zkcMCYw9dTtW//5WVHKbEJu6H6adfwUNp4Vrp1ch08R2eb4S8ZgSm+UTv08RSCQ5Rhlpone7kOOhcRLjFnYaGNP5UfXyNV3QEoxtZLB1sgFm7jZe98R8jTUvxVaHs2gzOIrlfd1QTWFgPNIFBVyj2SbaPryDXhsr2DJIog6wU41266Lb/qo8ahi0KJ2zo6PWuTL58ICiZA11wCU3blSAGGRk7tbfAdFoloDNWZ1HBPG8Qgfjk8ze0wFonOhyBm5KaHOKZo6vGkjNzqVXmqc2nVsuL/i/TN1Qs659ptjqI7lLn1wmYIiIo2i8MTPZMZicxSnJlKPkAG1u56A55Q6BPKhUXCcpLKFIBNLgkCNd8PMl38UvV/4U+VHg6g9ej+nJItZddxFCkxvoQPxoHsmgv3Uupl96O+nn4nx5YgvSVjheuEWQgOdKVRSdfCKUqZijt9bhnaChTvA8YxcLQCZSCYJKqrUbpx0LJRyHi+zklwzUhpohuB4ap1UZ3pqUCMZ3nYiZ1WZTRIZhKVlVrka/kWSt6iHGbx+2cFEg2gVujFAqLZXpesKm9ZiS8J+mZkOghr82v+ZUH10T/D7KTYwRr21hok+zEi8jqTxwtes2gSgdL4hTqbOPXtlH5hUqOQR9Yeux9fh3yoocd8xwc0upyVOjkXwukZEe2y019+XI1yjncBwJikYussR5HYVu3Ua3nTmHptWrVYe5QYve+nLuwWOrxm4PhMmniqWitYiOEstoYtV6eV110dsFmpCmFiiz5mSiiFHnzdwL+OvaR3lRRCQStaiRCLcgHo4i+ez9CHTGcEfCh591enhuQhOCy59k3xzndZHBcUwgovjFw7yH8jOB6hSuV01+Mmpbh3JYS3w0XmsSKo7xU/FVGxUVqajAOjwatYTlsbNXElOkZK5EN1o4n1eiR+U4sUYMtlJct82oRQy7CQ9P7B+VRAlMRd6RyKXevjJFQgZ7CrxmSTr+QZDBaj6OKVfz3xDnC1i7o7dI/hW5UFEpSqRb0jstQdNVuGYlVsqhnW9QpCM/qNTOEYhuRu9ihJ1l2Fv4ua3D1SqOxz7K2hwALzyfoqSExvatjneOF1u5FiNEB42cDlaGWSLRWpYkoY0dX4kVI7sEhJvrKZekB/LoBVtrkfcU1WUEUmCtSTWl+gjcWolK5BKvxCV87wXmSCohq/ltMA2soqyE7Z4FhR0jYSAVEQQSpaJXXEgTwYXf/zi+/fvLceMj30FzsMUsv0glEcqyF8baiIc3OxJ4rS2CVQzrpZAoF0G0LhJB36WexMcRxKmgJJsX+EpkoGJjiHNvzf2/O8gEMYPeB8jatc8vGhVuqaSMVNZWZJtSAvbVNQJkMJmhqBFkH2NmgBMSmY3n+vxUMiUgbowGcT4eltIKJ7/8rL3Mo5K76q/76u9Xf+KxKGg9tw+b7xp6XUn4Wjd5Jpyi25BSIbgubSQEqFAeaZYo7ZYu2JtdOFprp0pZjSWQFxZGt1tXKYherkukVdeyhZ/bOqg57GfdFMeoiULtK4jP7z1e1IhOy0CEp8oSj2sJykjkfNgLpbjdF+9UnvqZ+8uIFA09Uh/UnLwhny55+ORA5Pw4WlM51XH1li58/oL1t6m3Cxwt7yJ8Qa7TeMKoRJlqC6AipXwP8JXpBs2L0HpLGsyBSZrIJ362PxqjUcyfegjmRubgzldvwGiuhC8d/h/YJdKIm2hAS3Zq4kI9HPzWeuzefShmnfJtWr/SG+KTyVvOoOXILzhG2JZwIIQRjOLqn5+LTFlRR0rrgEupnrmVSyFKYhbdRSyaQHdDD+ZP2A/7zz6KDFaJSbx+D2tGl+Kr91xCxnn47BFXYlbHfBTLRd4TS/14auVv8Vrfi1gzuApj6THIgXmKRP40SQpTaCFEwwE0xiOY3DwZe+90KGa27CXnxxyd83MuBCO45akf4vnVD2PvSfvi1H0vNRp3DByrkF4VpGTiszCfpxKF8NUHL0ZqMInj9z0NC6YeQGWQJlS1jP9fHngCS1b8ESs3rMRoSkgKLE9GUEqF8enTvoWpselEXZTJSL2dw6VBPbv2Efz4oR8gGKIK11hKEAZdyrP6qHkeB6T9cUayDILFBM8HMW/Sfjj3gC/j8Tfvwu1P3IZQiNHIwC3CcNh48dDcEI00hHg8hNZEHDuP2wVH7P4RUsOaVCD/xgznsv8+GUPpNA7f6RCcsPBTdmv0b7/FG7/8CRp8GbTusT86jr6QrY2mJvJl4sbmSCHCs6vxwjX/gjANu+fES9Ay9xB3b5tglOKuZ27CfX+7k3wr499OugazW/ZhRJFOvZM324LAv11xxZVSSnl3eTKdl6m8d7zwIypLC9oaJ2JiuAvPbniSOl/CPpM+hK72XdHbuh4v972JUrGAA2cci3kfuoqrydJg6MVYYBbJHk3ucnRHhbC7Rj/6Mmtw9ws3ItBKmYayGPUPIBMeQio0yGMYqeAY0l4SGQosGUliNLgRSf8G9BVX4vllT+CF5X/BITsfZ/TKCz39xoN4dvRBFDiuOzQZs7vmkRY/BvKr8JmbT8VjvQ9gfeEVbMBaDAdGUQyOIB3ox3BkE5JRzuelMOTrw0h5NVaNvoEHlvwWq/pXYP+ZEoBcn4c+Cudbj3wBvrYRvLn+VYykhzCPxirvTXZTabhCWyoFU/NOlnLqXMbIO+SRvH+A+f2X6SCW4QWuexNe/NsL+PCeHxcSHj6MkpYv/ewC/PbVX6A3vwyDgXXY5I2RL+RTrB+jlX5Ey53YbfyeHKLk2G0qyIBUl935zM14q7IEqdgwMkG6vNAIvzchH8qhGM0iH00jHxxFyr8Ro+Fh3u8j3zch5w1j3epeHLvXGbhjyffRR37l4iMYC0gGo0hTDllvCGNhnpOesfA6JENpjIRGMeLvxabs21i64UX84o+3o7Etihmtu1HLqNe+JG58+mqE24DBwV4cvsspRu2GZ36D8NKHMNHbiGLvcqx+4E4EaRHR6fOrXCMrjSW0fDaMvPEoIs/+Gu10sSOhOJrnHMj7FmfsfkFpGh2Q0l/p8/JNT+O6P16FcE8ZsQYfHn32QRy758m8FzbjM19u0lMKXY1Qzq4Mqls1DlmZiaDPo7HQ8waoEEWFeq6uSEwV5h7lAlFwcsEz63sRCnoYGRnD3APOsTaLGkxLlAsriri+JF15tnZltGVsFAARhdKQZ/iD+QgOmX0Mcim6moCMlTk/I5O2mAOMAMVSAQF6oIHh9XhpxXMIjwtgefYV/PBPV+D8g75i+DzSEijHpVvw6BlqcOU9FyHfPszaqR3e6jxmTJ+LrnHjEcxxXtJWZMFV9I2RlBi92xBWrXkVg4UBNE7x4YV1v8NzKw7CgmmHG66KUq1YiYrZinKiQuW9G7uP3x3zJx7OiMCQbW5ZDFVaqaJQg2q5joD3mKb6uPob/vA1rAm8jEZfE4oBpnxNSuHUQ27Ej68/9EVsDL+F5iamJht82IV0NzfOtHSxgjF4LT4q8SkmZBMr162ZTZbk6akLL0L81TiCTIcDxShKAdYnwTzGhvvw2sArTMsr6IxNxtzW+chJS5iKFv1UwrQf8/aeL0zwhWImD2Wyi7oOQkOwkzzO2VZwmumxZR6cL1gMU+Ys7MtprFj9BlbTyURnlPHDR76NeePfj87oBFLIgtrXwLQrikrMnhFIJzH5Q5/GyMTpePu+G9CYWc+I6EPq4f/E0j/dhonHfwrxecfLn1b5yCLAC6HgZ02hgrMa2eSUpelav84VS60wJx++8pvPoGniOKRGN7E0IM1tbHvgc7jiyOvZXyNkCcY5O6ud19pZ07JBxQsNgzZhEGaIC5ai4o1yMFu8mKGHNxUWVYKNw+vYn235Ilr87dVMIkK0xCNDMJDg3LTvBkcaGU3P0NkQxsm7fdLdeA9IHbQaZ/30JDS1N2LJ+j+yxRmIgQTGKeVRBCXG9t7RNeiYNA75AQ83XfAztjJkvQfc8eINuG/pHYg1teKl/iXOQIhSDJXRq9hXcdo4LoqvP/Al/OfHpmB8YrZllG5d9DvkmVGh9EosMNLY4ovhoWU/xzNr70GgRQrIQazJjHAD9730jZfRNaUZg0MZ3H3OE9b2d+D8Dw8lxtW5qTxKSTsaJ+G8Rf/GlnfCc6sfxCvrnpa6Yi5TqY/NP7d65+8hr9qSSMupEo6aczSmtu5dvbMD2Ae4+blr8OjahxFpacJLrz+Nw+afwPmoQ3SSqmL9CgkE25sg4U3zjrEjs+ROLP/ljYwOGzDF24T+X3wNvQ/diknHn4fwjCM5QnUydUwOhWv2lZ1TUWh2m1s59qBjUsVPQ7ruT5ej3FREauMIPrrgQjy/6imszr/KtPUv+NPKu3DQ1BONaSVaoOpR2Zy210vE7TRYmwhSKir6w6/eTGs7Hz947EoSXuRiaAjGcRWBDFtmoTwU6gi9m9bqJsY1dtm15CrTqVBxhFLfZUsvHDO2DVQlFtieeQfCVl2dGRJqbSKeEMckpo5R5PMyxVpuLJCmGMH8cp0L9HaBMKNEvoSu1g620DjYRfKRwPKsA/QaTUGM1rIsOFYwp2uu7bRoxysfqApBaDlKQVLKHw1HUEnnkZjYgM/87DQOHbDi09VzNQfh0gJxzi3Dj9Wjf8X1f7gGseYGRMptKBY0sdJam7zajy300FKntmija9ANLYvdbHXqrmmkGJxDX7qUb9DzBOtvfYlf66sqZZbpQImRBB7rNydKw1exVyU4I5N/ZRACOccgI3mJmpMtOD5kiM/mtw+HtkIr0jxWKxK6wlMQyJSRIFH58qi1ibpKkTWh9TFtNvBTz7QLWSCe6F4nYeerH0b4+H/FsvJ4xBgFulPL0HvjZXj9e2ehMvgKYuGQ1RJlPSKoZQq2fkdQWYiozyvGXsajb/0OQdZXE32TcPROp+Oyxf+OZH8aHW0tuOF330Gq2G9j3QfHiwXURcV4t0BVz/ZACnjglXuwvLgMv3rjDi4lweWq8NM9MVb1iTMQP40liyHW2RQquTOxfZqNd5KVIlC1id+UkJNZ8/aAN0NcaIj/VPco7x6joiXRixTWI01PMuIb4B22+TdhHXPxO176T3geB1KDw2ThFrCVuq/aKeVQZlhUAeuMlcCh1oXfAa5JD9mC2tIUmNzoEFiQa9dK0cL26zcDeUC3l2WqOaljJ5z5/kuQHEiieXwcZ992Mu87JdLWgTFXhk9l05VEOVpah8/edTZ6xneib2Ma3zrmRxgZHabSUOjWYyvV8TiOU/v9OY7Psz7agDH/WtYCa8mPtzEaeIvfGyiJ1eTTaiou7+vw9VrqaMBFMt4RiXaGXJOsuKBtT177felqI4F1kW0vqyKWFyBEqLjKGNQ3WE1nosRnNFYJtR0vphqqgQLyEIT+4ip4cbogssNnT6rVmfe1u1ijowq+Cjlshqi58uRaAM0LPordvvoA4sddhjWYglYO7+l/BmuvOQPJ+29gupzgSD0dr8pfOA1xeHMW9NX7LkbbuHaMbcjhylO+Rz4U7FHFxYu/jvVDA5RZCJfd8y/W1+oXiZnRXHoharXxIZpVcagPfMEQ87sCQuEor5gHi3Cu1zwmr6tPE+ExhC9fvxThSJCet4TuhqqBSJkUM0vM/zjOWEXlq+7MW5d3g6V3kQLWja3Fx24+DAGOtTYl2hpFArQJleO538uY5wg2MK+OBZDNj+HsD/yrQ7Q1aDlVWvWsgHpGhVE41laKGvUhmqiSopcDzGNIMRQpxR6O126c8l5P2502QkMr5CEFHgxgZGMR+8//CJ7rfA5/G3kchcYoLr/3HFxz3C1kK2mXcjE6+oVXBTsd0SW3fBSJjjYMjG7ERR/4MsXVgZIKPEZl4d8aSuzv4+LLgQw+/l/vN1fl59zaOjWj92fJrwgLZiBGr6lnD5lgFoXsGE7c83ycuOhCOiiZhzRmC//1+kiJ6/KrZqq26a6TkVN+0xX7Jv+YTgaiQfzmtXvQHHicRpRBjE6tzPCTJR8CdADa7q54HrK5PDb0v423M0sRaKugb/0Qdp2zkJikUdQFLqJC5ZA+CUz3qCParZI7DlCHVDCXNCdr3sg+p2EOj9Hn7oT/4R+iKZBGafBNRld6+ayPvBgzPBYJ7eEKEdFir3/iCmRjGZSGPVx44CWI+pk9lFTflrDfhMV4qu1hvJp51pzNj5/8Os7c74vm+PSsS+t2a6ebodERnWOenriGKxnWHpota8RbusBvFdwarGEei89V/W/QUIJWPHdGJ2q4jVd/KZ6qupIvKa5wAseMbYGwl6j0fj1DoV0WE0x4mnMoNhZ4zmjUwPvhJLxYCpVwHqEEVU+1L3EWCx4SiWbD46Am7ncBDU66riJVoKfFloIYiBUyfn7xcBh0oqc4Ctq8V5HDsKWwXXmqUsjaA1HgksXfQjQ/Dg2FMFYX38B3//QFty728zHSFWXh/P+Fu09FoMlDIZ3FvpOOwMFTjrXxisp6ruFosYYqyMhEfpgKwV4NJWTJl1xLHgWel5i/lBtyKPG8wDw720Se8b6/oYBM0SmOvDopsXMjihCQ0VBTZSi17VP7Ir1KLsQmEzVBLRrnRfx4pf9Z/GHdPXi07348tP5u/LH/Pjy2+gE81PtLPMzjofX34rGBexnLXkIlksamlWmcc8DnMCk2W2pDWvIuC9lcnwpElLkn3leFxgKewtCzGO3zGsnlYYz09yGdoQmRHBXhRVkEXXtJe8EE6WpJsuG4VamlePj1exCNhDBr6mwcPOtYxo4h2mGGM6VoiGP43JGsW8nXRCiIB16+E2+OvmjsL9qrNdJ/4eXsJMOzrVdCkUpXrjCM+kbsWncDLB6lPHpyrGGiW09YB4rrmduFkMyNYFr3DOutZaqoF0+ve/QqPPHqnzC5bRq+dfKtdv/dYLPKahkWxyVa8Ml9r2JUSNMJEBPDel4JFOnN0zOoTpGbSeVH8dRrj2BZ7hVEQ1HceP/VuPbkewyfA7eWGuhK9ZMr4JzUrZaS5kkr7JvrlAbrWlziqa4tWSQtOc+lTQJzJvTsAQmompqW6Zn+4+O348zrFyMyOYan1jyKOUtvwWLmvGUyXA/MfvDHf8Oa0goKLYG2fCsuef/VjhzOJb7puYi9vrA1kDeKgKlcAZ8/8nqM5QZJGPGVlTJKdZXMmjsyEDpbUt6PhVP2tzbDTjxap5vO4iYPR7v7rC6d69GaLS+tgmYoeZRQOYbMyEbWTXG2xZhtKGFJY9a4uQhnG5FOpllrjJn8WyLdmNw9CwcffwRa0GPOyQydyhmg5VXsudQWKJuLlpsw6um8qslmZQB9v/wJ+v98LyYnWJSXwlg74kPHvAXwLX2BRsK1+eLqiSLTBI9OTfCNOz+Njq5OE8+agbdx6k2HUeZcv8d1a3fWDMxDtDVMWvJMtTrw9f/+FG466z5qXJvh0ANcK9Ml7ip7uAg9KadV2gIYHVRo6pTfASqZHggFaF0FCnxD6i0LgflsEdPbZzrp0ITsFXjCS+ufRXSCHyuSS12D2oVPIIERigHVKlQ+GkNjuQm7duyLBRMOwcKuw7B3zwfxvp5DMJ/X+3Qdjr34vVfPQThgyodx+ZHXIZVMIUI6hivOUwrEYG0g+LTB4JZEWkkamRGiUeihnMAe1JFRZW1pinB6tZwZit02CGmPnG1GsS9rbQIV0nqTzO1yOPXSA8YwGvC107+P4fUjaGlM4OYnrsMrw3+hR/Pj98vuxOOrHkCsIYQR1ivXfvQWjtLcNdDcMsgthijQVjm0QUBvv0v7Xlg0fjEWdR2JBT2HYZ8e8qSH1/xeWD10vq++Jx1KRYhY2mGRUR+MDjWQjBQdS6wH8tUo6CQiY6LxcV4zFEIxwGvKPTs2hPMPuxjzGvbHyIYkclm9p+XHmvW96BjXg7OOPgPXHPNjXH3Mrbj0iG/ihD3OQiONQ0SYanBObaoXKmkaDAmzx+2iQ5yo8pFUiNwABtF7/7fx18uOQuOSWzCD2UNvqoShnQ/GTt98AKH9PspULkd5kk4N4Ax6Z02Df/L4tcg0ZJChXqQzKUbSIUbXUZTa0sjGR5BrTSLbkEKe36ncEFKZHPWAUbexhG89cLmzBlu6TkgnSWNQcOwJaleA3/ZWA7N+5e0qOvNUpELV8gpU6DzdxGtrXkUlJO/InJisEChN0Aj6PIymNyHECNPT6tIvQ+x4vhm0V6+dk0qAi9262LZFbx9KLN61uyMVqwlSUBHTyTFlkYqUAqOHKUWJxejGsV5rs9UyPbT0g4ouA2ES4zo7VuDNoefhRYmE9UG44GiTKsksvDI5U2LaVaXTaGDXKYl5OGPRZzCSGkJTZxOuvetSPDfwS/zkT99EtHMcBtYP4pqTbiS/miTSzePFdRXN1am5BLemckFC4j3m9Vt8/T8AGsbDdiE1j+VLNfUjXhpFWK8C5SMUvlPUzUD+aT3aiBFoAyNQjtJZFjDO2wkXHHw1bj73tzh62mnw9wcxVhzAQ6vuwWU/PQ8X334S7n35NtvAsbH6UORXdLTF6VUT1RuqB2vr0bdVbJx7Ewbv/yZeuvSDiD11G+YkKOk0nfGUQ7HTV+7FxI99g0jbWUow/demQFnx061NL1WuSr6M3y39BdP1AKYE5+KMhZfhE/O+hHPnX46zdr8MF+75BZy32+U4h9fn7XYZzlrwWVy44MuoDDGLiYfx0sCL+MPy/5Y6kHbGENKtDQtyyIlGgrGX1WxvOYbSRmB4cATjJnWgOd6NgTczTALC6GjsojLsgtdeW4LpU/e0sSYI3pUIlvUtQyDiQ6GYw7yeXeyuQNbuUn8JLoRQKcH+IQqMKVtpA+5/9XaaJQkqZ02pQzID5v8yoLIKf6YyfelR/Hn5I2hta0WWHmJe+wIhrAJp0PtW0j19E8JUxknR8UiXN6EcLeL0Hx+BRTPfj9aGFkYgFqR6N6uShb/YgGQwhVQxiRWrXsOq0aVINDViaGQTdu7ax3ApgEscVhyzOKrFACWJ+k1GkPMetcupeH7lk6xFXkF8XBuue/jf0TyhiUX5CE7f67OY0bAHkeidKkZo8kJU2nMmppEBlweSJ44/05t2RrrYi6bmZvzLbUdhwez3IR4bR3pDFCCVWGkDv2OcN6fi0typH0Pky0G7H4vucCuRiWp20C6WdJH6WiIvlctbSlN7mGngUhSrvTRE8ioq0irRCjDKOqNpYOJ0wl7n8jgRf1jxOH79/O3Y6H8LGxN9uPut63DnM9/Fzj174ojdjmdGsJgI00REORK/njdoG1Vvewv0AmMokEXfAzdi6M93oZ1168xEAJvSPvR3zMeM8z8Nf8+u1rfI9XpUIKXfFfKwTNk5ozelwjX3XUrH1IyRvlFccc4PyFtXO74XtEXa8dmHzsaEji58/6FrsXDqAUgElGoxynI+ljoSCGsN5mNimlfN6+767GNki/yAxFjGbz6/hN/OQ1/14et5rnF6XM9S1vJxx+A3Nrxizx4yhQx2atnd2ly+TlxkvH64YqrAqFQqshArhTBYXIu73vgWMizgAkqRiFeFZLmcsBBYDqR4TUboSXkz05xNPjRXunDBiV8VJoNSmeG7pDzXT+NMVVuBK064EZ+/5QwkY2l4bQN4so9eYkOIHoJrUfQqks00lkogTVoCVE3WYQ0VDPblcOSsk7Bw+sFVhdGbwz4kvRTiHunLu10xpQghE7x4U8AVR/8AF9xyGDKNSYSjAYwxHdkzPh/H7n4WqG/wMxd2nHNQLFHNaZiFag6tHJBlBK788DfwuR+fg6Fm0s3j4d47UKHXZwlC/hQtMBSkJEovymFG+FF+s1YgrSka5LmHfpk489UtbKdE7IAg8af9SQSDNNJystquO66eoX8nP2T+dCAFj4Y8ikCR6WbeeX3tXinI+XyNOHja0XasHFmCu5+8A89teAKhlhDepIN4+ZFnqOy/xtdPuIFyZ03pyzAhYZ1T4upL1edXTE/f/OJiNJd7MYU6M5gNYTg8HT2nnosJMw9VBwVRyiULz1OKLKuN0HAZBRmJhqs0/fSJf0c6Ooz8UBqfOvJaZxxiC+OZn/2ld9uDOV2LcHD3kXh59HE0jW/E1371WVxz3K3u1x6cjjyxYEgclkCYEAVlTqLXMKihvKnAS8lo21UUa0w5ZG2OrcRk7cDrw8+T+WEUc0XMHC/rVzujk/siqOAvIBGPoyU4GaGxBsRT7WhOjkdPahbGpaehOT0B8WwnWnPdaEtNRGd6BppyUzGuPB0Lmz+IC953Ba77xD2kLr45nZrcuRMaUxMQGu7CzC5nmNrkaAtOwH+d/Xt8cu8vY7+mw9AV2BkNucloznegJTkJjYWpaMl0o3l0GroKczAzPA+HTDoB3znjJpx94GXkg0vmtL7uBGkszkJgYwJ7tOxrc+i9ZSm1+ydnUsG3T/8FOoudyPcVsGtwF3zpQ99lH7qW6kNH61Xl15zWBcTXil1bF9m12xYHWsMTcfOFv8eFe30O8xsXk7ad0UK+tKQnoy09Be2pKWhNzkJzdiLaMhPRkJ1Gfk1CItmG3ac7XNogUCJiJbptWZWw04RdkRjr5pwt2KPKJ8nGr3VKIbQ9Xa1Z9piwCKFNrWjKd2PGeNaabJYzcW9UE6e6cR1Tm/bC5478Nm448x4snng6Gga70VKZiNldswyPcj6lsZPDc+BtjGG37ve5dkJ06jz0RaZjVXgu2k/6KmZ88V7EZi4matEhA+AkNAbtKOoq2j0bA4kpWO7vRnya20KeMHkGUisq2Lf7cLxv/BHsS31l5Cv7WYtxcfbwunq4Xcwth1hzyaFXIbCpARvXbsJO4xjlBbY2pnMUlE7x6V99BMnKINOqIdx99os20EDS0oQ8ET5y0DXqvuxCX7xhrzXTk555x0Hwx/1I9yXx87Of4aIUc9SRPWVsHF8gczU8bEt+1+7N/0UQbZrHuYB3g+64usuW+B6wrX5q0yEXUQOLqO9ocbFW/bTSGh7no/8edF9c2da9/x0QDq279i28gu2ttUaXoHa+dZvorfFya346Lm4ftsbxj4DordFa44GuNb/m0eFqjx3N6ujS8V58rL5oTKhgA/rRhU4KjGlhIIaCsplyKV/xMUe9+N5jkPOG0LdxEPed8QKxc5jtFJA87UDJCPivtm2o6fUgT6s3JrBvARmc+JOF6GrtQiTTiu+efJdN7ZbDT6FiKqIdbtsMyK5BaSUL/nDEtupCOS6HtYZeP8gz5dC7XoEi6xQ97VM70x+lQnqQJJK8PPPaUIG4SGclRvsTezmXDNGXtTzV6NNvJKz64sy8F9D2lp9RTkV6gExgyLb3hNimB4ryAWWlmqw3ZPgB5v1FFvr+SiMDAdU/xHSuyGigIlcS0HyMJEoHjV285zEFUW1RKdCLh3IIMc0oF5iS8pxkEDc9GHlctl/u6f0pPbjkOomrYg8tk6RB+bq2lRmlinoYFyY94rtWyb7s5y8xvxdN3hjTF8qMpGgLVpsqlVKMvGTqSGYVWdgGimHkSHu40ERvquf9XB/nFj4VpHpGUtFmDXHoIafHghc53tTPqFmbukcYvKkHhdXtWq7C0pEiUuRhnPSwxGVdUVJBbhEgaK8SqXbQewl+ybKg7CPPdVIuTO1zzFakH2F6bPG3xLRIz8+8ot66Zt8i0ypt9zPVyisqiO9M2cJM/fKBAlnJbIc6HKCMytQDpc0VT6rP+Wu7p9sE6aJ4lSCtdGxM6bHz3hxDWbE+0/4CI4i9vIIL7/wwlS2J/pEB3PeJvxB3nLhNEuxEZpDhev3E/WRVZx7PJFAqK/VNCvjc24/i2qdZLAUi2Hv80Thn0Rc42vXV2yF6lcF+xaWn0aUMnv3MIozXJhEXXPSoXGR8NtKBSqKBCsaFjq6goaWRoTCLUYbUsQGUgqwZ7CU6MqjQyOJRSV6SxV6UmFnmkxjl3XptIY8Y8fnRkvMjRyPQA6ixUBNipQ28zzpC+T/pKqgSi7bBGxtDgoaVpiKWmTYFqYxSyEysi8b4NqJU9jznjyh3I09G4i2IJkeohDIo5eZkqhxAMMsGeian76SKQmcxGEqO0rhGpVKmWB7rr5KeAPNabyUEvTwCObfZ0R9qRri1E+m3l6E7Sp5xnWkKLehLIUIFKCoFJl88GmM2zpQpM0qeZZCjMXmNHSiOrkbI30A6K/xOY0xbrkppy2sQyLJmCVORWQ/YjhXXkgtQRuSH9tMKVDqvpOpKTlKKHSIvZLRVJ8kULMe0R6oXKvgxTENLNUxCONaAcu9SNKsE0HrIjwodkH59WaCM/X46GjoEVlJmCAUyTiqqHcUc79NcEY+FEEglKTsP5YY2+CjzuAyc3tzjGgpelLUra7AQdUl/b4R6o80cF1XESxkzV0Fe+Ct0vCzWFNO5SPapbm6oZmSLAc/9rIVLrA2L1DXfghMw5dSrSJ9cgWq9as+Oxm5TCh+9WpaRwMVRLYAd5Nn4LZaoWbm23Zbh8Mo9cQfe7nuDwpCXraDJcw9dZMFmZmSun/gNsb5GN2B8eBRNiTgaG6JoitEDsHaZcson0dXWiJ6eBOJNrQg30pOxQJvxkfPNY7bFPcRDIUa7HvRHuxCLxdGaaEYwEsZYsA1xFsY5fxMSsWYkeB6cMA3hlmbEWPP4aaWzPnomVhQTNJwY2qIempsiaKCBlKeyNolHEQwHEVt0ENqmzSFuntPAmufMRQMVKNESJ50+ZLwEVqENM0+4GDEqboI0xmIxpEMRO2I0toYE6U/QMwY9bPI3o+GY8xAOFtlvHGlptTU30svqDYgEaWltiCMaiiPRFEWgeQpmn3AmxnH98z55OWVYIW0JNOrpuRfDYLiNdPoRa4gh1BhGYo/d0Th7N0QSMcToXPxTWWfEEqzzQog2RpCiwfdXmjHj1IssyrS0RbnmMtLBDgyHutEQDaIlGkFjnA4l1oJBbzxGQp2Ik6ZYognNlFGM69FPYX10PnHO2xon38j7AOdoWHQMdjvo/Rg/ZQJ2Ou0SRoIo2mId5EEU2UAcGyiXEPvGYtQLrjuvnxZEWzBCHYkQd7uUndF70scvQ47zqz5NRCjnmXNtDc3keYJyH6XT2BgkXynXeCSEcFMTnWIzMpEWZOn4GqKNaIk1IspAMJiYjD62+aLNbPOR5gQqQdYZ0R7GgwiauNZEvIF8iqK5QboSQ0sL6+FB9+xOmi3V57edYnbrLNpBhcg9PPvG49ZmZmbRQ+agfs7ypPAyHHs/S3FIkYTw4siTVGZ6ulQF+8xayB6650bK8yh1MVDqFlD8SdD6lbbRX9B45FWzWUYE7bBwsb6xDfRC9CRCk6eh0EDy9N45Rpeu487BtKM+iFxrOwniTJk8Zp16DkY6FmDi0ccRj1KMAho7JwFUcjp+lAolZEsR7HnIhzDl+IswROUscH3BEgU3aw7758yTMLiRNnlyzkfa2ubshUpa7xJVkEyX0X3KxZh7yAFIl0knhVvK0VsGW9Fz/LloP/FMZqcNti2Zk/IfeAImHH40xk2cWf2JqSKHD3lGtfyeB2DyN+7GGOfSC5IhPXwljV6MNMTovT0Pz9/3O0SowAUqZ2GsiO7jLkXP4qPg2/0DDE1kjFJhpaYSCnlYzqXRQYMpZhTRmPSmRjHppIux80FHIMV01efFkc8zsWBaMeH4MzDhqGMx3CLZu1Szeadd0XngUZh1/hc4NlXVASZQqY3o/vT30HjSF5GipxXP5fTGckl073UQ1t37I+CvT2Doj7+FP6K4kMfI2AjGf+xizDr6eGSnLrLfG5VyzBT2PwnTD/swZp1+kb1dnKO8mUkh3D0FCYZdOdgyaemcvTPK6SLTKkZuRqpJHzmbvDwRmDIfuTx1j2ttOeE8TNx7ESac+VnSkrG0OZD30LPPQky96CtonLUbioUyStkSug84HE2LDkTX4hORTTGuU38DTOv0zEOlREH6rC3xKuhtYQY5ZyC7TFiI4eKIvYT469dvszblv3L6ChAWWh1PLAA4ztU+WZSXe/Hy+hfgEb/+aMKkpt3YV8bFcK2x/Mf0lIqnkpUGQhZqrBvPb4ZjEau/0tG7YT02rt5AtdGOP1MdKor+SFiJKUWGhuXNmo9QPoTYwFqEps1AplykRwnihRu+jikX/ggv33oTfPQaeoGumGcU9MdsGzjgp/GVglhz90+wYZQKQQ+pd8j01kCeR5BC0TMYHws0vWyodeZJ15hfnpDMYooVb2lD38AQNt5HhWBeXmCKGQ7FMMD0xNc3gMYMa4bGJtYZAYxQ0LGuPZC67ftYu2INU1K9wpBkzpuiN2bNQk+JUDsC+pGZ1R/M2IMBZApJLLv9v2gUOex2yseRG2FqxlQh0dyK1YNj2HTrd9C2834o5fXkiBKUFFWb6JBq5lTbKGJz/oYO9G7KYeBXP0Uwn6bDYQ3HGiywy0Kmc4xCg2sQnrOA61S9U8bw03/AuO42rPjev9K7Mp0mzjJTQZRZFzS3oUKvqz+CUGKb0q6oHmiWVR9FKFkqd2c7DS2JItPl9p0WYuD5v6L35qvQvvdiZCl6PVMpM1KuffA36Fv6IqKkryTDzXLMyuXw1ryJMOvBnB7WlPSunY/2z/SL+DMbR9G6qQ/ptk7QZyBL5cwQ58Cff4d1q9ayZtKfpWN9SG+Y/MvjCK19FX3P/5k+hFGZzmLdaBITN65CpXUyZcx6Rb/uVCqn7IZ8i1CeAeZPAr3irx1AZnZUUf7fdfx+zGoZMqkVvSMb8Niqu9nNJVSSnBjvfj6qOsQVu1J6VQAys+/8+kq0UjEy9FyH7noc+6mrLEveTb6YPTmPnnHoHVM/hZ8oqpAiw1RkMXQH6A19DKVNA6+ifeR1jqPgadUFPXNg+lBmoRqhUIprlyHFOVckmdps2GC57TC98+6nXYy+n12OmR85k/U1TVBKLtekgpC4KAf4W5l6HPBB9LTQIOnh9PDK/vQMl2p/GYTLsV+rGWdUM9Hj+5M8slbb5AYH0dnD+mD/45n+NTIvZg1Ar9g2dzdsZEE75mM+3NnNOq+EOIWYH12J6P5HYMIus7lOpn8snAv0ItrWrzz1IJZ97khEK0nOz0JUv04qZOC1dWHXY4mfCp2n4lSUinEtY8NDmDSFKciBJ2OkfwVTRjkg4vFxfVQqn56LcC0+FdeKhLweS46hu70Z0YVHItjWzEK9QCfIaVYvR5L3V6RYk739EsVUQoqhs/u8L2D4zdXomLMHClyP3ioOso5IMIVccdUpGLz1i4gTQZB1YI7zBmJMIV98HK2LT4OPEbGJXl8/CpNMNq1Yhtb5e2DcEWch9dYTCJEfenLmi5A35DXzSTqsHEJUUM/HGogpWZp80KsoelCsP+qh2kM0N7W2Mv1qx1AwwmEdKGfpNLTxwFQzSMUKkY4II3OQbRlGpUmXXI/1f1uO+LRdEGQESRcKmDT/A3h9hCbRPhkZZS6MNspiVDzIQCTtjJ74E6Sv1Fw6Hf3ahd1UKD675kF8+7HLmXN2Y9PGXlx5wo+wS9NeNkBFl35LLVXXf9vSkP0Qvv/gVViy8RFEWWANDSTxs3Me44TqKaV3ffQLPPFE4Yg1JrxCH16/4H3oaqd1c37tTKjgLic6ER5ZSfQRY2aBlq9XpFPMKeNj69i3ZG1jsR4ytBmV9cvQxBCdkTGNm4j02tfQ3DMX5UHh4DIjcYZYFoaMHHpwNsr6IBpvRnLgbfRQMbMUpHZQ8vHxCGZ6aUiMbsyfy9kUC1oWi6Q1zLw1z7n9rJEUQUcKFGbnZJTGyM7CSuL2YxO9aMv42VjRuxrjO1nv9DM9JL8G8mWEZ+yK4f4RdGbWIMJCW2+NamdOqZYMsMB+xUqMHpllKhWPNTXWNU9C+7geDK54Gd2hHHvpFY0Co5KH2KSdkVm+hAVtI6MEPWa0lU4kS31idJDFJMazxqNHVeHJcSlGvkD3ZBprkhGuFxEW4YVCBUPs10Be5Htfo9L7qRyMIi2TabxuV83Pdn+EDlGv2Vc3NGSAEmmRxuUL5FmkszZgypZrnU7DY6Re+zcEmc/HyOsKo+Qg642Gpm5k+peikfIIcO25eIulWmWm0dFsHw2AeQWdR5pzh0dXWOagV/Wz1IVQsp9aFMJYifg65mJ0uBcTGlpR3LSMvGJ6GeuCb2Qtyi3jER7aiBxrlzLlXereGVE9VU2thy/VRz7QkAPj0NDWhOzICFpy67mOiBlCiFHDzwg+TMNOT3k/Zl7wH07HZUTM03VquZse5X/j/kvxcvZpJEIehgeyOGbuSfjoojPZY+tXywVlbCy/jWt+eTUGC2Qki53hNX34ygnXYdeODzCfp7BZeMlQBHoxzvZnGPbFOG2vFlc+hdE1LIoCVFIKXw922ExvpjG0aKZUYCh3Zikj5rd2HWhkAaYKthukkMc0wE+r03tGqiGC9BYlhmSreRipSsRv7/aT8fZHE+gTSmE9ndGf9ZEnYaqi31XTY2mbUluvnIHtnJ6KEKInzbPQtd0pCjLMtELbniXmq0o/VZkp6pQZMe31lSJpZ1qguKto6y/m6VwiVCqpr+jnfeXZTN1k8PplkbZ09Zq9X3/iiBFXKZ6URH8jgEmbpWHaRvbomIr+DO8zffTHES4xNeF91Ynimv4iYoApToW43TYx1ZnCt9czSK+29LWdrBRXBuej4pU9Ohrt/PFfNNfHlCZBo43qVW/epxens/LYz5yj5uBZgTwNMuLor9pwBVyjtpM5H5VW9YeP/NBvLFSnKtUrMmrqtyOaQ/MWfXEE9SSfSmkRW9iV4nAdltaL/7ymuBnhlX5Tjuymh596ml8JRNmJ9/XzDK5Jb+oWPb0ur3cEua6CXgASK0lrKEq5Ui4VFpeUt9/Pe+UWRnJZfJHrUPTwMfll7bLoSJ63kW+KKcRTYeVUJtMNGfms7y/fdSbWlN9iRKggw1QlPwwsmLkvpjXNQYThdTDTh1fW/xUr+95AooMekWFirHcEFxx4KQ6deZoRrv1vWaDSL6U49r6SBGlPJ4P03G4njL3MI6lXDZwYZCJkiDt9B+i+KUP1W7D1uUBjqwFuu/A/GfN/Cu+m+d1rlBuoXb+bPsG7efU/ha3xaB7Bu+f6R0BJt2RYg3evT+fbWsf/S5A2KtEW7Git4okOPVeT7uqNZ1+hrOBGkcjDKB+ipemnkTc/fi0eeu1uRjB6d3rDfEG71PQKDIcer0MsljVblilEuT+Iz3/sauzWtD8LpyIDl94WotcjFfLC+gEWs0RaO+sAhlmPHkS73yV6nyBzFnt2wH5Wo9iLbCrOZfEunLu/jiJRSHXkryla9aW30ErlcXRS2+PWpTykfseifpbesU1+Qn+VUfe1TWB46fX0ark9bGO+La9n29ba2eAow8X75j2JzzwgadbzFmMnPZrA/o4tvZFAuaxo1HrUX7gUkcQ/PbRjiLGQL0+udlckck10kTlFLOHj4fxadYzQqJHeUbWM1qm/LiIP7f6WlkAqT3koPVIuWAWRqkTa5GENolt9SRFvOkMRsZQJL4idaIinGoHsDz7zv96jE9T4qgWKcvc3fMlx473wsVjnp+4pQtpbFrZOIuE4o5J4JQs16K6LTrVPB6ZvvFCvIj/d79mFg72MfodP+wTiuB4+aldPt0SqHL7AZRK8NoGQoxymWwquQm63+cHg6y6Ix73/wYPxQ6+oVHvrS+yhcrEwXFdZhjse/TGWvPYkEOeMUYZAGocek5RYIDeGW3HQHkfh5Hmnc6ReCiua0gtflae2OOcXSRIbqYcmUCv6tSwTME/Z2XlPtbNBQlAvW4xbSbUbgcLjtXA4Za71d/O6OWlQjFRSbjG35uWEQ4eUzx50GmslUMU35rm8dhicQUpBnQAdVlEiGhwFDoujyajlp/ptOTOjEJ3VeYSpKh6HTD0oGcPOa6UbNXxurJo1VqPUQkfGa51LnUSF6KitWlzUH3JTgqdVag3CotU4wxOmWpwqWQ/HOX6Sl8ZO9tZGilNrx2Oltupb7WkydG+A88qsT1i5SqEXOMK5HtHI21XnJXB80R6Rdu+ckasOdHf0rfui1HFflBpuo1uItfrqWqggVmvaGKXIpJu06GGm+CqoZnA8uAIapebT+BrHnITVxfUXX6ovSHGdovA9oYy3ki9jdf9ypLMptMXHYcbk2WjHFN7jNI7aOtThnw7e00C0yWWWJQOomZwBh8mLmMthD4XPOtThnwzeofLbAuWNlioqECnXVMVvO1E8ZTqmv7xRhzr8s8I/kGIpQ+OnrIRBQr03F3u1tJEgNPUoUod/NngP97/Fdmq6r+/NrVvZQ9046vDPCO8RQbR15ip8A+upSFLbaZCN1FOsOvzzwj+4i1WHOvz/CXX3X4c67ADqBlKHOuwA6gZShzrsAOoGUoc67ADqBlKHOuwA6gZShzrsAOoGUoc6bBeA/wXKiseKsnLpjgAAAABJRU5ErkJggg==\","
                            + "\"logoCustomer\": null,"
                            + "\"background\": \"#F7F8FB\","
                            + "\"primary\": \"#97BE0D\","
                            + "\"secondary\": \"#424242\","
                            + "\"accent\": \"#82B1FF\","
                            + "\"error\": \"#FF5252\","
                            + "\"info\": \"#2196F3\","
                            + "\"success\": \"#4CAF50\","
                            + "\"warning\": \"#FFC107\""
                            + "}";
            BpCompanyTheme bpCompanyTheme;

            try
            {
                int ret = AdminBpCompanyTheme.Retrieve(companyidtx, theme, out bpCompanyTheme);
                if (ret == 0 && bpCompanyTheme != null && !string.IsNullOrEmpty(bpCompanyTheme.JsonConfig))
                {
                    retJson = bpCompanyTheme.JsonConfig;
                    //retJson = retJson.Replace("\r\n", "");
                    //retJson = retJson.Replace("\\", "");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BPUnitOfWork.GetJsonTheme Error: " + ex.Message);
            }
            return retJson;


            //    if (string.IsNullOrEmpty(theme) || theme.Equals("nv"))
            //{
            //    return "{\"themeHeaderColor\": \"#ffffff\",\"themeHeaderLogo\": \"/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABAExAAIAAAARAAAAPlEQAAEAAAABAQAAAFERAAQAAAABAAAAAFESAAQAAAABAAAAAAAAAABBZG9iZSBJbWFnZVJlYWR5AAD / 2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz / 2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz / wAARCAAxAMgDASIAAhEBAxEB / 8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL / 8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4 + Tl5ufo6erx8vP09fb3 + Pn6 / 8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL / 8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3 + Pn6 / 9oADAMBAAIRAxEAPwD9 / Kr6tq9roOmXF7fXVvZWdqhlmnnkEcUKDkszNgKB6nivGP2xf25PD / 7IsWk2N5azap4k8RxzPpViJBBFKIigdnlOcAeYuFUMx9AMsPz++M37R / j79qrxJb2usXl1fLcTqtjoemxOtsJCcIEhXLSSZJwzl35IBA4r7Ph3grF5nFYibVOj / M93bey / V2Xqfm / F / iVl + STeEpp1cRp7i0SvquZ + d9km / JXufX / xj / 4Kw + FfBniWGw8J6TceL7eGXbe33n / ZLfb3EBZS0jDnkqqHAwzA5HtnwD / ap8F / tIaaZPDeqD7dEm + 40y6AhvrUcctHk7l5A3oWTJxuzXwVp3 / BOLxxBo9veeI9a8D + CzdD91bazq + y4P1CIyfk5I7gVzvxW / ZD + In7OFla + KW + z3Wl2rrLDr / h2 / aWO0cnCtvASWPnjftC5IG7JGfrq3CnDmIpxwuDrqNXZS5r8z7dIvy5bfM / P8Nx5xjg608bmOFcqG7hy8rgu63kvPnTXofq5RX5 +/ s//wDBWPUPAFpHY/E6NtX0mEbf7ZtkVbyAD/nrGMLN25Ta+AeJGNfeXibxVpngvw5eaxrGoWWk6Tp8Rnury8mWCC2jHJd3YhVUdySBX51nXDuNyusqWJj8XwtaqVrbdeq0aTP2Hhvi7Lc8w8sRgpfDbmUtHG99+nR6ptaPXRmhRXP/AA4+K/hf4w6FJqnhLxJoXijTYZ2tpLrSb+K8hjlUKxjLxswDBWU7Sc4YHoRS+B/ir4Y+JsupR+G/EWh6++i3Js9QXTr6K6NjOM5il2MdjjB+VsHivHlRqRvzRfu76bevY+hjiaUlFxknzbarX07/ACN+iijNZmwUUUUAFFFcD8Nf2oPAfxf+Jnivwd4b8RW+qeJPBMog1qzSCWM2b7mQgO6BJMMrK3lswUjBwSK1p0ak4ynCLajq2lole1321016mNTE0ac406kkpSdoptJtpXsl1dtdOmp31FFNnnS2heSRlSONSzMTgKBySayNh1Fc/wDDf4s+FvjJoMmqeEfEmg+KNMhmNtJd6TfxXkKShVYxl42YBgrodpOcMp7iuY8dftffDD4caX4ou9W8deHI18FIr67BbXa3l1pYaRYlEsEO+VWMjqgXbkswGM10Rwlec/ZRg3LtZ310Wnm9DlqY7DU6ftp1IqNm7tpKyV273tZJXfkej0Vi/Dr4haN8WfAmk+JvDt9HqWh65ape2N0isgmicZU7WAZT6qwDA5BAIIrarGcZQk4yVmtGn0OinUjOKnB3T1TWqaezTCiiipKCiiodQv4tKsJ7q4by4LeNpZGwW2qoyTgcngdqN9EDaSuyaiuG/Z9/aR8F/tTeBZPE3gPWG1zRI7p7Jrk2VxaYmRVZl2Toj8B15xjnrwaK0rUalKbp1YuMlumrNeqZjh8RSr01WoSUoy1TTTTXk1oz87v+DicZ8c/Cn/rw1P8A9GWtcv8A8E7PiHJ+zt+xV8U/jzqirr2reHbyLwx4ciuAGks55RAJJdx+8P8AS4Tyc7IZFDAOa6j/AIOJj/xXXwp/68NT/wDRlrXnv/BOuHT/ANqH9i74ufs7LqFlpfi/WrtPFPhs3L7V1CaIW5eEHoMG0jDdWCTuwVhG2P2/Lf8AklMPz/w+Zc/+D2r5vO1t7dL9D+Yc4/5L/F+zt7Xkfsr/APP32K5LX0vf4b/at1se+eCf2ZvAPxh+G3w9+JfxC+IXjA33xTeK2VWijluLi9mbESh/Lfy4lAYEMpQZUBkAAMX7Npuv2af+CiV78Elu5Nd8M6959lqFtPGPs1xE+nNeRu0X3TIECxuQACpftgD528c/tQeKvh/4b+B/wv8AFnwt1vwrqnwZ1uHUTPfXTf8AE5NvvJSNPJChXPSRJJFI5BYEGvbPjB+3z8NfCfxc8SfETwP4I8XD4t6/aR2YvPE0ccFroI8iOLfHbq7kymNI+CMEE4cKzK2k8LmdSnOhNOpCrGooJODjF8y9lJNbJRd73b0SSukjbB4jJadWliqbVKrQnSc21UU5rkbrRalfmcpq1rJatt2bb+R/j9oS+Evid410OO4kurXw/q2o6ZbSO27dFBPJEp9OVQHPev1+/wCCi3/JhnxW/wCxZu//AEXX4va5cS3lnezTyyTzzLJJLLI255XOSzMe5JJJPcmv3I/au+E+pfHb9mPxr4N0eaxt9U8SaPNYWsl7I0dukjrgF2VWYL6kKT7VzeIUlRr5dKrL4ZSu/R07s9Xwwp+3wma08PH4oRUYrzVWy/Q/Ob9gLxzff8E9Piv8NJtVupj8Kv2hdBspjc3DKI9L1ZVVGYn+ECV8E/KPKuYyxYwGu9/Yx+Pa/sqfBX9rbx1NZ/bJvDnjm7aG1clVluXmaGFHPUKZZEDEcgZxzXvPjb/gnlN8Vf8Agmtofwb8QXGk/wDCVeG9Jg/s7UIJJHtLbUoFYI4Zow/lMGaNz5e7ZK+BnFcX+yZ/wS88TeDf2W/i38O/ihrmi6hcfEy7F2NQ0m6nvHhlCBlnkM0URaRZ1WTHIbHJ5rwcVnGWYqlWrYiS5pThGSX24QqJqS8+TR+ifU9nA8O53gK2Hw2Eg+SNOpOEna1OpOk06b8vae8uiUmuh5fr/wC1B+1T8GP2bfDf7QmueLvCfiHwlrU1vdXPg8aRDbrbWdy2ISs6IJctuTGZGKF1LeZhhVb/AIKEeMvHnxm/bQ/Z1n8M+JtJ0vR/E6waz4JMmnF30yWZIC1xeKxZZpCSNoUBVQYxkszdHqf/AATo/aS+JXwR8P8AwS8UeNfhxb/C3QLqLOqWCXMmr3VpExMMRRkEZ2fLtXK4Krl328+qfts/sFeMvG3jH4OeLPhBeeGbHVvhAotrTTteeUWs0Efk+SA0asTt8sqwJUsrZDKy86U8wyyji4STpKT9qk4xSiouP7vm0te+jdm7NqW5liMpzrE5fVpuNdwXsJOM5tzc4zvV5Pevy2s0rpcyTgtDQ/af8d/EX4D/ALNfgu11749fD7wL4ykvJV1rxFqOkxMdSgHmNmytCpDSJugVl2YwSxZTgN5//wAE0v27PGPxk/ar8ZfDLXvHWj/FbQdM0htZ0nxXaaSulvcBJLZGjMKKi7c3JByuQ0RwzKwIm/aO/YY+Ofxo8ZfCX4nSX/wr1L4leB0mTVNKu1uv+EflP2qSW3aFShkbajqH3bW3RqysSBXY/su/sTfE74eft06/8ZvH/iLwbrN14v8AC7afqMGjR3FutleGa1CQwRyKd9ukFqgEruJGYnKdWPl82WRyypGrKnKpKMmrKKanz6KNoc1rbPmUbaKPb3FHO55zRnQhVhRjKCfNKTUqfs9XK8+Xm5mlJcjndNuffxn9hT9o/wDam/bj0uyvNO8UeFdF8N+E/EccWv6tc2MK3mtwlo5JLOGNYHjXy4SfmxGxMyfPwcdb+zx+1ZfeDP2of2utU1ix0WbSfh7DLqax2Gk2ljeX62xuSqTXEcYkmbamxWlZsbvc16t/wS2/Y28VfsT/AAX8ReHfF174fvr7V/EEmqwvpFxNNCsTW8EQDGWKMht0TcAEYI57DD+C3/BPTWtC+PX7ROreMLzRLnwh8aop7OCHTbqU30NvM04fzA8Koj7JeCrPhh7VeMzDK518XTjGCp2iocsUnL34uVnu3ZO3ZaKyMstyfPKeEy+rOdSVe8nU55NqL9nOMbrZK7jfe71d3dngNn+1H+1Zq/7Js37SUPjfwZH4Whu3nXwb/YkZie0S7Nq/70p5/EgY7fODFBuD7iEr9Av2evjFa/tBfA3wp42s7drOHxNpkN+bYyeYbV3UF4t2Bu2NuXdgZ25wM18Or/wTi/aS0z9n2f4C2fjf4ZyfCia+Mi6u8FzHrCWjXX2hovKClOZMyGPecklPOCHA+7Pgr8KdO+Bfwj8N+DdJaaTTvDOnQ6dBJLjzJhGgUyPgAbmILHAAyxwBXBxPUy+VJfVuRy55cvs1a1Ky5VLRXlfvd73Z6nBNDN6eIf1z2qh7KHP7WXNetd87hq7Qt2tHayPhv9jrxpY/8E//ANrz9pH4c6tvt/Cen2M3xB0aKNdsUVpFH5skcSnlm8maKPj/AJ829K8/+AOna58O/wDglH8bvjffwaXceLvipq39pSm+sYb61uLcagsJElvMrRsGmmvThgQVdD2Br3z/AIKg/wDBODxl+154+8N+Kfh5rHh7RNbtNIu9B1d9Wu54Eu7OQkxIvlQy5x5tyGDAZEi45FepftG/seXXjT/gn3efBjwXcaba3EOkWGk6fPqTvDAVtpYGLytGjtuZYmJIU5ZuepNer/bWClGhWclz1p0va36Km7O/lJ2l52PD/wBW8zhPFYaMGqeGhX+rtfalWV4qPW8E5QXa+jtofOfxy/bg+IvwB/ZG/Zd17woNNmvfFsFpHqWlQ6fb29vqiCCAraxqqBbdW3FR5QULkYGBiqv7cGi/tJeAv+CefxE1D4k+PPB2of2lfWy3llpNjt+z6dPNDAbOGQRREZkl+cyeaWjXaHyST3nxr/4Jx+PPiP8AAX9nXwvYal4Rj1H4Ry2smsvcXlwsNwIlgDfZysBZ8+W2N6p1HTt77+3V+znfftY/sp+LfAOm6ha6XqWtxQPaXF0rGBZYLmK4RX2gsFYxBCwDFQ2QrY2nCGaZfQrYV04w/iyc5cqbUVUvHW2nu9tbW7I6p5Fm2Jw+OVadXSjGNKKm1FzdG0tL6vnSWrte76tny54T8Y/Gj9lz/glPqfji68d6PqSw+FPDk/gyG30aFH0G3cwo8Uu5MTMYZYk3NnBjJ6mtz9qf9s34i/DP/glP8PfidouuQ2vjTXoNJe+vmsIJFlM8DPLiJkMa5YA8KMdsVveA/wBj/wCMvxK/Yo8VfB34p6p4BsdPj8P6fofha40D7RLLE1pkrLeGRVDAmK1H7vGQsnCkjPkHjn/gmv8AtMfGn9lTR/hT4k8XfCuz0HwP5R0SO1N40uqvGfKjF1P5PyRxQSS7CkRZmVA65/eL1UJ5ZVxHPip0rxrc0nZWlTcVZK0bPX4l3bb6s4sTRzuhhPZYGnXtLDckVzO8aqnJtu8rxfK1yv8AlSitkj3f9tP47+IvBXws+Hd5YfG/wT8HpdXthcatd6tpsWoX18phjYNa2zBgwVywcbQP3i/MuNrec/8ABPT9ubxZ8fPFXxa+H/ibxXo/xGtPDelSaho/iux00acdQgOUYPCqoo+/GQAgKkSAtICrDa/aZ/YH+J+s/tMfC/4rfD65+H2p614J8PQaJPpfij7Q1issSzhZ08tdzqPtLsB8jI8SMN2SF1v2XP2EfiV8LP2mvid8QvGniLwjrlx8RtB+zyNpqz2/2a9fy2eIRMhAt49pjRy7OyorMoYkDjhLKoZXKDlCU3G60ipKXPtpHm0j1c7NaKPU76kc+nncKkYVI01Lll70nCUPZ/FZz5FeXRU+ZPVy1s8X/ggP/wAmNXn/AGM95/6ItqK9Q/4JgfskeJv2Lf2brjwf4svNBvtUk1mfUFk0meWa38t44VALSRRtuzG2RtxjHPoV83xNiKdfNK9ai+aMpNprqj7LgvB1sJkeFw2Ii4zjBJp7pnZftY/sT/D/APbP8KW+m+NNMme608P/AGfqllL5F/pxfG7y3wQVOBlHVkJAJUkAj8nv2t/+CW3xU/Yh1b/hK9DnvPEnhnSZhd23iLRN8F7pJT5xLNEhMkBTaT5qMyLgEuhIWv27oru4f4vx2Vfu4PnpdYS287dvy7pnl8XeHmV59++qL2ddbVI6S025v5kvPVdGj8SPhL/wWB+P2mT2ulzeMrbV7fYUSW/0q2lmUKhx+8VFLnjOX3E9yayzD8QP2zvjZeXMNnfeMPGmuGOS5NpaxQjaiJCjybAkUUaqqLvfavAySTk/o/8AH3/gj58Jvjb8SbPxVY2974K1LzzJqSaEI4bfU1ZSGzEylI5D18yMDJLFg7EMPoD4N/Azwl+z/wCEU0PwfodlomnqdziFS0ty/TfLIxLyvjjc7E4wM4AFfcS48yfCUfb5ZhVGtJWfuqKXq1q1fWyte2tmfneD8M+IcXVeEzvGudCDunzSk5aaWjL4WldXd7X05kfJv7J3/BHPQvBYtta+KU1t4o1VcSJokGf7Ltj1xKSA1yemQQsfLAq4w1fW/wAY20VfhrqS+ItGs/EGiyrHDdaddW8dxDcq8iKFaOQFGAJBwR2rp6yPHng6H4geE7zSLi4urSK8C5mttnmxFWDgrvVlyCo+8pHtX5zjc8xWYYuGIx1RuzW2iirq/Klt8tX1bZ+vZfw7g8qwM8NllNJuL31cnZ25m99ej0V9EkeZ6n+yt8B9G8QWOmXXwn+GsN1qJ225bwda+TI2GOzzfI8sOQrEKWDHBwDVDU/2fv2dtG1qbT7r4a/CyG4t3SKTd4StPLV2KAJ5nk7N482NiucqrhiAvNdld/s+2mreONL8QajrutalqGkmJomuI7TkxvMy42wAx584q3llN4jjzyCWLv8AZu8O3/iHX76eITL4jinjuYmtbfennxCKUpP5fnruXdx5hALtjAwBtHMEmufE1HprZve/S/S3f71sctTLqrT9nhKS97S6j8Nt3brfs/k9zibr4Efs5WeppZv8N/hV5zTm2bb4StHSF/OMAEjiErGDMrRBnIDSKyAllIFfwr8Dv2f9e0KO6uvhT8K9NnZ4Ee3fwxYu0ZuLuS0t8kQ/8tJo2Ue/X1ruV/Zs0uKKOOPVtcVJ7O3sdV+eAnXEinluN058rh5JZ7hnaLy93nv0whVh/Zn0sX9tLHrOvRwxPYyTW4a3KXbWd/JfwbyYiwxNK4OwruXAPTJ1/tCny2WIqXuteaW3X5vpvstVdpYf2bieZSeFpWs9OWO7217J2vtvKydot8T/AMKS/Z3dYriP4X/C3+zHWeRtQl8K2UNr5cMZkeVJHhCyxhQTvjLKAM5qrD8Kf2c7XR/tWqfDD4XaXMrXBltZfCNsbi0SEoztNH9nDRbYpYZX3ABElDbinzn0Ifs1aZJ4P0/Qp9Y16407RrV7HTVZ4FeyhMQiRQwiBYxgKVZ9xyi7i3zbpv8AhnfTrm61W6vNW1u+vtasr6yvLiRoVaRbuK0idgqxBVZI7KEJgY+8WDls01mVK9nXqW12k79beWq300skr6sTyvFWvHDUr6bxjbpfZ30d7a6ptu1kjgfi78DPgF8IvDd9eXfwl+GNxeW2n3GoQ2cfhSzLTrEufmZYGEasxVA74Xc6jJJApnin4JfADQP3dv8ACb4X310NStNPMQ8LWcauZr6CykeNzBtl8mSdRIEJ2NhG2sRXpPj34FWfju3uIf7X1nS49Q0htDvxZmD/AE21O7areZG+1l3yYZNvEr5z8u2jB+zF4ft9WvrxHmSS81RNW3pbWqzRyjUItQZPOEPmtG88S5VnPy8AjCFVRzKn7OLqV6nNfX3parTTy67PXunoPEZXiPa1FSw1LltaPux0dnr59N0rNWSavI85174S/Abw94t13Rp/gr8PvtWjRwyo3/CK6f5d8HMIkEZ8vrF9ogLBscSrjPOLHiD4Ufs3+Hbe6kf4X/Di6a0nS3aO28F20ryk3KWrNEBB++WOaRUkMe7YxCnDEKfTvGPwH0fxvLeS3VxqMc93qdrqySwyIr2s0EaxAR5U/K8Ssjhs5WVwCMjGbcfs0abcRbG1rXdtnE8OljdBjSEa7hu8RjyvnxLbQAed5mFiC93LOnmdKSg51qienN70uyvbfrf8X0Sc1spxUZVFTw9JrXlbjHTV2vt0t32SdrykuR1n4Dfs6+HDeDUfhj8M9PNj5W/7T4Mt4fM8yRIk8vdAPNzJJGh8vdhnVTgkA11+Cv7O9vDG178L/hbZtNcXEADeFLOQRrDcNbmSUrCRCnmLt3SbQGO0nIIrsX/ZZ0W48UzavPqeszXU16t8Wb7PuLLqFtqCo0nleY6LJaxxqHY7Icou3gizqH7N2lXsWpQpqmtW9rrq3MGrQo0JXUree7numgctGSqq11cIDGVfZMwLFgjqLMKSik8RUvfX3ntb/P16LTVjeW4pybWEpJW0XLFu9+vyt1XV6u0Sp/wxJ8GP+iRfDD/wlrH/AONUf8MSfBj/AKJF8MP/AAlrH/41Xp9FeN/amN/5/T/8Cf8AmfR/2PgP+fEP/AY/5HmH/DEnwY/6JF8MP/CWsf8A41R/wxJ8GP8AokXww/8ACWsf/jVen0Uf2pjf+f0//An/AJh/Y+A/58Q/8Bj/AJHmH/DEnwY/6JF8MP8AwlrH/wCNUf8ADEnwY/6JF8MP/CWsf/jVen0Uf2pjf+f0/wDwJ/5h/Y+A/wCfEP8AwGP+Rg/D34W+GfhHosmm+FPDmg+GNOmnNzJa6Tp8VlDJKVVTIUjVVLFVUFiM4UDsKK3qK46lSU5Oc2231erO2nThTioU0klslogoooqDQKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//Z\"}";
            //}
            //else
            //{
            //    return "{\"themeHeaderColor\": \"#068335\",\"themeHeaderLogo\": \"iVBORw0KGgoAAAANSUhEUgAAAMgAAAA8CAYAAAAjW/WRAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOxAAADsQBlSsOGwAAOWZJREFUeF7tfHe8XFW973dm9vQ5/eS09E5CTSCEgAhICwhKExARpLerKCiKei8oKCpevVcEBS8qIApcigVRigpSpAQEaSGEJKSc5JTktOn1fb+/NZMETILvvvd5f/jml+yZvdde67d+69d/a+85vgoBdahDHbYJ/up3HepQh21A3UDqUIcdQN1A6lCHHUDdQOpQhx1A3UDqUIcdQN1A6lCHHUDdQOpQhx1A3UDqUIcdQN1A6lCHHUDdQOpQhx1A3UDqUIcdQN1A6lCHHYAzEL2uWDsMeFLiV9k1FVHgKS/4jVIF5UoZW15xVBs7F3WuQbqhbwG/y7zmf42u1O7b2Cpygr62dWwGuxAGHTUQrgJvbenp5tgCpJKHaHcz6yjzU61bem7BWZtBvR1eXpWrrbrkUf1ysPlkR1DDui1QexVpDTZPUOWVfZbY00nA3dZnlSZ1sztaZW2E61Op8qfW/u7DAc82N9S4VANHd+32tg7HU3deA9cuTrvxNaj1dZ3dqmpQw+d6UacqVKgKz42cEtXLSc1dAXl3+p6wma7NJ+KT48mWtu0DDYS91LHa2SkVyfERiY9CKZfgr3imJ+7FXx/KvFf08dToD3JQAOB/qaJBqRaY2MnHTpv5oKWxrTpXxU7djNs63CeHay4DnahVn35iC/C8epOGqBaf5pBREnTPVwnok99cLOn3cVKdVyd3yKv0CZNhq/jZhzf5DZ+/SkNxMx02h3mEzQvbAThatg/CpV5SqCJKIsvmEW5d8FP0uKvqtfu2w3Xhoc/yVn105ln71jzd+qhx2A5214yuv/4JXI+tx7z78PFTMxlNDoGdu0N3q+3WT1KT8ltrdaQD19PxU5Is+wK8olKZKlEH7Q7BHFb1uiQZOFzbBd3eQgRBF9QTk694v+Px7JfjOkNGiFufli1F02J4SPnLNALPDRBUaL4+DnHCFZFhLZsKSDuxxRMTF6j1V2jrPhmRMYZtOjiXvIcYsZWKbwOc59e3n/+q7LYrnekgNgcyCiqxtVY4n19YdV79NtB3ldU0frtt16JXa1aDFqoxjqotilOwuXzqX9YY9rVuVXzbBY3eUR/RwfucX+ooRjulE63il74Fjh6HT+du/jxxC7uJR111VMkzMM3dPijGaKxbox+e5iNeccNkaeNrc78L7JY8vSZ0MlFXNyU/NF5KoRmMbElTmiJ9oOTV1yHh4ShQu+mSWpWpkL+uhThsQE3neC1vElDPHQAdvHSxbPKlDpmX0zxq1yFFVtu2gQZCzdo8MSfTOccrgsjzaoL12V7c9+JNaAw04bh9zkcMCYw9dTtW//5WVHKbEJu6H6adfwUNp4Vrp1ch08R2eb4S8ZgSm+UTv08RSCQ5Rhlpone7kOOhcRLjFnYaGNP5UfXyNV3QEoxtZLB1sgFm7jZe98R8jTUvxVaHs2gzOIrlfd1QTWFgPNIFBVyj2SbaPryDXhsr2DJIog6wU41266Lb/qo8ahi0KJ2zo6PWuTL58ICiZA11wCU3blSAGGRk7tbfAdFoloDNWZ1HBPG8Qgfjk8ze0wFonOhyBm5KaHOKZo6vGkjNzqVXmqc2nVsuL/i/TN1Qs659ptjqI7lLn1wmYIiIo2i8MTPZMZicxSnJlKPkAG1u56A55Q6BPKhUXCcpLKFIBNLgkCNd8PMl38UvV/4U+VHg6g9ej+nJItZddxFCkxvoQPxoHsmgv3Uupl96O+nn4nx5YgvSVjheuEWQgOdKVRSdfCKUqZijt9bhnaChTvA8YxcLQCZSCYJKqrUbpx0LJRyHi+zklwzUhpohuB4ap1UZ3pqUCMZ3nYiZ1WZTRIZhKVlVrka/kWSt6iHGbx+2cFEg2gVujFAqLZXpesKm9ZiS8J+mZkOghr82v+ZUH10T/D7KTYwRr21hok+zEi8jqTxwtes2gSgdL4hTqbOPXtlH5hUqOQR9Yeux9fh3yoocd8xwc0upyVOjkXwukZEe2y019+XI1yjncBwJikYussR5HYVu3Ua3nTmHptWrVYe5QYve+nLuwWOrxm4PhMmniqWitYiOEstoYtV6eV110dsFmpCmFiiz5mSiiFHnzdwL+OvaR3lRRCQStaiRCLcgHo4i+ez9CHTGcEfCh591enhuQhOCy59k3xzndZHBcUwgovjFw7yH8jOB6hSuV01+Mmpbh3JYS3w0XmsSKo7xU/FVGxUVqajAOjwatYTlsbNXElOkZK5EN1o4n1eiR+U4sUYMtlJct82oRQy7CQ9P7B+VRAlMRd6RyKXevjJFQgZ7CrxmSTr+QZDBaj6OKVfz3xDnC1i7o7dI/hW5UFEpSqRb0jstQdNVuGYlVsqhnW9QpCM/qNTOEYhuRu9ihJ1l2Fv4ua3D1SqOxz7K2hwALzyfoqSExvatjneOF1u5FiNEB42cDlaGWSLRWpYkoY0dX4kVI7sEhJvrKZekB/LoBVtrkfcU1WUEUmCtSTWl+gjcWolK5BKvxCV87wXmSCohq/ltMA2soqyE7Z4FhR0jYSAVEQQSpaJXXEgTwYXf/zi+/fvLceMj30FzsMUsv0glEcqyF8baiIc3OxJ4rS2CVQzrpZAoF0G0LhJB36WexMcRxKmgJJsX+EpkoGJjiHNvzf2/O8gEMYPeB8jatc8vGhVuqaSMVNZWZJtSAvbVNQJkMJmhqBFkH2NmgBMSmY3n+vxUMiUgbowGcT4eltIKJ7/8rL3Mo5K76q/76u9Xf+KxKGg9tw+b7xp6XUn4Wjd5Jpyi25BSIbgubSQEqFAeaZYo7ZYu2JtdOFprp0pZjSWQFxZGt1tXKYherkukVdeyhZ/bOqg57GfdFMeoiULtK4jP7z1e1IhOy0CEp8oSj2sJykjkfNgLpbjdF+9UnvqZ+8uIFA09Uh/UnLwhny55+ORA5Pw4WlM51XH1li58/oL1t6m3Cxwt7yJ8Qa7TeMKoRJlqC6AipXwP8JXpBs2L0HpLGsyBSZrIJ362PxqjUcyfegjmRubgzldvwGiuhC8d/h/YJdKIm2hAS3Zq4kI9HPzWeuzefShmnfJtWr/SG+KTyVvOoOXILzhG2JZwIIQRjOLqn5+LTFlRR0rrgEupnrmVSyFKYhbdRSyaQHdDD+ZP2A/7zz6KDFaJSbx+D2tGl+Kr91xCxnn47BFXYlbHfBTLRd4TS/14auVv8Vrfi1gzuApj6THIgXmKRP40SQpTaCFEwwE0xiOY3DwZe+90KGa27CXnxxyd83MuBCO45akf4vnVD2PvSfvi1H0vNRp3DByrkF4VpGTiszCfpxKF8NUHL0ZqMInj9z0NC6YeQGWQJlS1jP9fHngCS1b8ESs3rMRoSkgKLE9GUEqF8enTvoWpselEXZTJSL2dw6VBPbv2Efz4oR8gGKIK11hKEAZdyrP6qHkeB6T9cUayDILFBM8HMW/Sfjj3gC/j8Tfvwu1P3IZQiNHIwC3CcNh48dDcEI00hHg8hNZEHDuP2wVH7P4RUsOaVCD/xgznsv8+GUPpNA7f6RCcsPBTdmv0b7/FG7/8CRp8GbTusT86jr6QrY2mJvJl4sbmSCHCs6vxwjX/gjANu+fES9Ay9xB3b5tglOKuZ27CfX+7k3wr499OugazW/ZhRJFOvZM324LAv11xxZVSSnl3eTKdl6m8d7zwIypLC9oaJ2JiuAvPbniSOl/CPpM+hK72XdHbuh4v972JUrGAA2cci3kfuoqrydJg6MVYYBbJHk3ucnRHhbC7Rj/6Mmtw9ws3ItBKmYayGPUPIBMeQio0yGMYqeAY0l4SGQosGUliNLgRSf8G9BVX4vllT+CF5X/BITsfZ/TKCz39xoN4dvRBFDiuOzQZs7vmkRY/BvKr8JmbT8VjvQ9gfeEVbMBaDAdGUQyOIB3ox3BkE5JRzuelMOTrw0h5NVaNvoEHlvwWq/pXYP+ZEoBcn4c+Cudbj3wBvrYRvLn+VYykhzCPxirvTXZTabhCWyoFU/NOlnLqXMbIO+SRvH+A+f2X6SCW4QWuexNe/NsL+PCeHxcSHj6MkpYv/ewC/PbVX6A3vwyDgXXY5I2RL+RTrB+jlX5Ey53YbfyeHKLk2G0qyIBUl935zM14q7IEqdgwMkG6vNAIvzchH8qhGM0iH00jHxxFyr8Ro+Fh3u8j3zch5w1j3epeHLvXGbhjyffRR37l4iMYC0gGo0hTDllvCGNhnpOesfA6JENpjIRGMeLvxabs21i64UX84o+3o7Etihmtu1HLqNe+JG58+mqE24DBwV4cvsspRu2GZ36D8NKHMNHbiGLvcqx+4E4EaRHR6fOrXCMrjSW0fDaMvPEoIs/+Gu10sSOhOJrnHMj7FmfsfkFpGh2Q0l/p8/JNT+O6P16FcE8ZsQYfHn32QRy758m8FzbjM19u0lMKXY1Qzq4Mqls1DlmZiaDPo7HQ8waoEEWFeq6uSEwV5h7lAlFwcsEz63sRCnoYGRnD3APOsTaLGkxLlAsriri+JF15tnZltGVsFAARhdKQZ/iD+QgOmX0Mcim6moCMlTk/I5O2mAOMAMVSAQF6oIHh9XhpxXMIjwtgefYV/PBPV+D8g75i+DzSEijHpVvw6BlqcOU9FyHfPszaqR3e6jxmTJ+LrnHjEcxxXtJWZMFV9I2RlBi92xBWrXkVg4UBNE7x4YV1v8NzKw7CgmmHG66KUq1YiYrZinKiQuW9G7uP3x3zJx7OiMCQbW5ZDFVaqaJQg2q5joD3mKb6uPob/vA1rAm8jEZfE4oBpnxNSuHUQ27Ej68/9EVsDL+F5iamJht82IV0NzfOtHSxgjF4LT4q8SkmZBMr162ZTZbk6akLL0L81TiCTIcDxShKAdYnwTzGhvvw2sArTMsr6IxNxtzW+chJS5iKFv1UwrQf8/aeL0zwhWImD2Wyi7oOQkOwkzzO2VZwmumxZR6cL1gMU+Ys7MtprFj9BlbTyURnlPHDR76NeePfj87oBFLIgtrXwLQrikrMnhFIJzH5Q5/GyMTpePu+G9CYWc+I6EPq4f/E0j/dhonHfwrxecfLn1b5yCLAC6HgZ02hgrMa2eSUpelav84VS60wJx++8pvPoGniOKRGN7E0IM1tbHvgc7jiyOvZXyNkCcY5O6ud19pZ07JBxQsNgzZhEGaIC5ai4o1yMFu8mKGHNxUWVYKNw+vYn235Ilr87dVMIkK0xCNDMJDg3LTvBkcaGU3P0NkQxsm7fdLdeA9IHbQaZ/30JDS1N2LJ+j+yxRmIgQTGKeVRBCXG9t7RNeiYNA75AQ83XfAztjJkvQfc8eINuG/pHYg1teKl/iXOQIhSDJXRq9hXcdo4LoqvP/Al/OfHpmB8YrZllG5d9DvkmVGh9EosMNLY4ovhoWU/xzNr70GgRQrIQazJjHAD9730jZfRNaUZg0MZ3H3OE9b2d+D8Dw8lxtW5qTxKSTsaJ+G8Rf/GlnfCc6sfxCvrnpa6Yi5TqY/NP7d65+8hr9qSSMupEo6aczSmtu5dvbMD2Ae4+blr8OjahxFpacJLrz+Nw+afwPmoQ3SSqmL9CgkE25sg4U3zjrEjs+ROLP/ljYwOGzDF24T+X3wNvQ/diknHn4fwjCM5QnUydUwOhWv2lZ1TUWh2m1s59qBjUsVPQ7ruT5ej3FREauMIPrrgQjy/6imszr/KtPUv+NPKu3DQ1BONaSVaoOpR2Zy210vE7TRYmwhSKir6w6/eTGs7Hz947EoSXuRiaAjGcRWBDFtmoTwU6gi9m9bqJsY1dtm15CrTqVBxhFLfZUsvHDO2DVQlFtieeQfCVl2dGRJqbSKeEMckpo5R5PMyxVpuLJCmGMH8cp0L9HaBMKNEvoSu1g620DjYRfKRwPKsA/QaTUGM1rIsOFYwp2uu7bRoxysfqApBaDlKQVLKHw1HUEnnkZjYgM/87DQOHbDi09VzNQfh0gJxzi3Dj9Wjf8X1f7gGseYGRMptKBY0sdJam7zajy300FKntmija9ANLYvdbHXqrmmkGJxDX7qUb9DzBOtvfYlf66sqZZbpQImRBB7rNydKw1exVyU4I5N/ZRACOccgI3mJmpMtOD5kiM/mtw+HtkIr0jxWKxK6wlMQyJSRIFH58qi1ibpKkTWh9TFtNvBTz7QLWSCe6F4nYeerH0b4+H/FsvJ4xBgFulPL0HvjZXj9e2ehMvgKYuGQ1RJlPSKoZQq2fkdQWYiozyvGXsajb/0OQdZXE32TcPROp+Oyxf+OZH8aHW0tuOF330Gq2G9j3QfHiwXURcV4t0BVz/ZACnjglXuwvLgMv3rjDi4lweWq8NM9MVb1iTMQP40liyHW2RQquTOxfZqNd5KVIlC1id+UkJNZ8/aAN0NcaIj/VPco7x6joiXRixTWI01PMuIb4B22+TdhHXPxO176T3geB1KDw2ThFrCVuq/aKeVQZlhUAeuMlcCh1oXfAa5JD9mC2tIUmNzoEFiQa9dK0cL26zcDeUC3l2WqOaljJ5z5/kuQHEiieXwcZ992Mu87JdLWgTFXhk9l05VEOVpah8/edTZ6xneib2Ma3zrmRxgZHabSUOjWYyvV8TiOU/v9OY7Psz7agDH/WtYCa8mPtzEaeIvfGyiJ1eTTaiou7+vw9VrqaMBFMt4RiXaGXJOsuKBtT177felqI4F1kW0vqyKWFyBEqLjKGNQ3WE1nosRnNFYJtR0vphqqgQLyEIT+4ip4cbogssNnT6rVmfe1u1ijowq+Cjlshqi58uRaAM0LPordvvoA4sddhjWYglYO7+l/BmuvOQPJ+29gupzgSD0dr8pfOA1xeHMW9NX7LkbbuHaMbcjhylO+Rz4U7FHFxYu/jvVDA5RZCJfd8y/W1+oXiZnRXHoharXxIZpVcagPfMEQ87sCQuEor5gHi3Cu1zwmr6tPE+ExhC9fvxThSJCet4TuhqqBSJkUM0vM/zjOWEXlq+7MW5d3g6V3kQLWja3Fx24+DAGOtTYl2hpFArQJleO538uY5wg2MK+OBZDNj+HsD/yrQ7Q1aDlVWvWsgHpGhVE41laKGvUhmqiSopcDzGNIMRQpxR6O126c8l5P2502QkMr5CEFHgxgZGMR+8//CJ7rfA5/G3kchcYoLr/3HFxz3C1kK2mXcjE6+oVXBTsd0SW3fBSJjjYMjG7ERR/4MsXVgZIKPEZl4d8aSuzv4+LLgQw+/l/vN1fl59zaOjWj92fJrwgLZiBGr6lnD5lgFoXsGE7c83ycuOhCOiiZhzRmC//1+kiJ6/KrZqq26a6TkVN+0xX7Jv+YTgaiQfzmtXvQHHicRpRBjE6tzPCTJR8CdADa7q54HrK5PDb0v423M0sRaKugb/0Qdp2zkJikUdQFLqJC5ZA+CUz3qCParZI7DlCHVDCXNCdr3sg+p2EOj9Hn7oT/4R+iKZBGafBNRld6+ayPvBgzPBYJ7eEKEdFir3/iCmRjGZSGPVx44CWI+pk9lFTflrDfhMV4qu1hvJp51pzNj5/8Os7c74vm+PSsS+t2a6ebodERnWOenriGKxnWHpota8RbusBvFdwarGEei89V/W/QUIJWPHdGJ2q4jVd/KZ6qupIvKa5wAseMbYGwl6j0fj1DoV0WE0x4mnMoNhZ4zmjUwPvhJLxYCpVwHqEEVU+1L3EWCx4SiWbD46Am7ncBDU66riJVoKfFloIYiBUyfn7xcBh0oqc4Ctq8V5HDsKWwXXmqUsjaA1HgksXfQjQ/Dg2FMFYX38B3//QFty728zHSFWXh/P+Fu09FoMlDIZ3FvpOOwMFTjrXxisp6ruFosYYqyMhEfpgKwV4NJWTJl1xLHgWel5i/lBtyKPG8wDw720Se8b6/oYBM0SmOvDopsXMjihCQ0VBTZSi17VP7Ir1KLsQmEzVBLRrnRfx4pf9Z/GHdPXi07348tP5u/LH/Pjy2+gE81PtLPMzjofX34rGBexnLXkIlksamlWmcc8DnMCk2W2pDWvIuC9lcnwpElLkn3leFxgKewtCzGO3zGsnlYYz09yGdoQmRHBXhRVkEXXtJe8EE6WpJsuG4VamlePj1exCNhDBr6mwcPOtYxo4h2mGGM6VoiGP43JGsW8nXRCiIB16+E2+OvmjsL9qrNdJ/4eXsJMOzrVdCkUpXrjCM+kbsWncDLB6lPHpyrGGiW09YB4rrmduFkMyNYFr3DOutZaqoF0+ve/QqPPHqnzC5bRq+dfKtdv/dYLPKahkWxyVa8Ml9r2JUSNMJEBPDel4JFOnN0zOoTpGbSeVH8dRrj2BZ7hVEQ1HceP/VuPbkewyfA7eWGuhK9ZMr4JzUrZaS5kkr7JvrlAbrWlziqa4tWSQtOc+lTQJzJvTsAQmompqW6Zn+4+O348zrFyMyOYan1jyKOUtvwWLmvGUyXA/MfvDHf8Oa0goKLYG2fCsuef/VjhzOJb7puYi9vrA1kDeKgKlcAZ8/8nqM5QZJGPGVlTJKdZXMmjsyEDpbUt6PhVP2tzbDTjxap5vO4iYPR7v7rC6d69GaLS+tgmYoeZRQOYbMyEbWTXG2xZhtKGFJY9a4uQhnG5FOpllrjJn8WyLdmNw9CwcffwRa0GPOyQydyhmg5VXsudQWKJuLlpsw6um8qslmZQB9v/wJ+v98LyYnWJSXwlg74kPHvAXwLX2BRsK1+eLqiSLTBI9OTfCNOz+Njq5OE8+agbdx6k2HUeZcv8d1a3fWDMxDtDVMWvJMtTrw9f/+FG466z5qXJvh0ANcK9Ml7ip7uAg9KadV2gIYHVRo6pTfASqZHggFaF0FCnxD6i0LgflsEdPbZzrp0ITsFXjCS+ufRXSCHyuSS12D2oVPIIERigHVKlQ+GkNjuQm7duyLBRMOwcKuw7B3zwfxvp5DMJ/X+3Qdjr34vVfPQThgyodx+ZHXIZVMIUI6hivOUwrEYG0g+LTB4JZEWkkamRGiUeihnMAe1JFRZW1pinB6tZwZit02CGmPnG1GsS9rbQIV0nqTzO1yOPXSA8YwGvC107+P4fUjaGlM4OYnrsMrw3+hR/Pj98vuxOOrHkCsIYQR1ivXfvQWjtLcNdDcMsgthijQVjm0QUBvv0v7Xlg0fjEWdR2JBT2HYZ8e8qSH1/xeWD10vq++Jx1KRYhY2mGRUR+MDjWQjBQdS6wH8tUo6CQiY6LxcV4zFEIxwGvKPTs2hPMPuxjzGvbHyIYkclm9p+XHmvW96BjXg7OOPgPXHPNjXH3Mrbj0iG/ihD3OQiONQ0SYanBObaoXKmkaDAmzx+2iQ5yo8pFUiNwABtF7/7fx18uOQuOSWzCD2UNvqoShnQ/GTt98AKH9PspULkd5kk4N4Ax6Z02Df/L4tcg0ZJChXqQzKUbSIUbXUZTa0sjGR5BrTSLbkEKe36ncEFKZHPWAUbexhG89cLmzBlu6TkgnSWNQcOwJaleA3/ZWA7N+5e0qOvNUpELV8gpU6DzdxGtrXkUlJO/InJisEChN0Aj6PIymNyHECNPT6tIvQ+x4vhm0V6+dk0qAi9262LZFbx9KLN61uyMVqwlSUBHTyTFlkYqUAqOHKUWJxejGsV5rs9UyPbT0g4ouA2ES4zo7VuDNoefhRYmE9UG44GiTKsksvDI5U2LaVaXTaGDXKYl5OGPRZzCSGkJTZxOuvetSPDfwS/zkT99EtHMcBtYP4pqTbiS/miTSzePFdRXN1am5BLemckFC4j3m9Vt8/T8AGsbDdiE1j+VLNfUjXhpFWK8C5SMUvlPUzUD+aT3aiBFoAyNQjtJZFjDO2wkXHHw1bj73tzh62mnw9wcxVhzAQ6vuwWU/PQ8X334S7n35NtvAsbH6UORXdLTF6VUT1RuqB2vr0bdVbJx7Ewbv/yZeuvSDiD11G+YkKOk0nfGUQ7HTV+7FxI99g0jbWUow/demQFnx061NL1WuSr6M3y39BdP1AKYE5+KMhZfhE/O+hHPnX46zdr8MF+75BZy32+U4h9fn7XYZzlrwWVy44MuoDDGLiYfx0sCL+MPy/5Y6kHbGENKtDQtyyIlGgrGX1WxvOYbSRmB4cATjJnWgOd6NgTczTALC6GjsojLsgtdeW4LpU/e0sSYI3pUIlvUtQyDiQ6GYw7yeXeyuQNbuUn8JLoRQKcH+IQqMKVtpA+5/9XaaJQkqZ02pQzID5v8yoLIKf6YyfelR/Hn5I2hta0WWHmJe+wIhrAJp0PtW0j19E8JUxknR8UiXN6EcLeL0Hx+BRTPfj9aGFkYgFqR6N6uShb/YgGQwhVQxiRWrXsOq0aVINDViaGQTdu7ax3ApgEscVhyzOKrFACWJ+k1GkPMetcupeH7lk6xFXkF8XBuue/jf0TyhiUX5CE7f67OY0bAHkeidKkZo8kJU2nMmppEBlweSJ44/05t2RrrYi6bmZvzLbUdhwez3IR4bR3pDFCCVWGkDv2OcN6fi0typH0Pky0G7H4vucCuRiWp20C6WdJH6WiIvlctbSlN7mGngUhSrvTRE8ioq0irRCjDKOqNpYOJ0wl7n8jgRf1jxOH79/O3Y6H8LGxN9uPut63DnM9/Fzj174ojdjmdGsJgI00REORK/njdoG1Vvewv0AmMokEXfAzdi6M93oZ1168xEAJvSPvR3zMeM8z8Nf8+u1rfI9XpUIKXfFfKwTNk5ozelwjX3XUrH1IyRvlFccc4PyFtXO74XtEXa8dmHzsaEji58/6FrsXDqAUgElGoxynI+ljoSCGsN5mNimlfN6+767GNki/yAxFjGbz6/hN/OQ1/14et5rnF6XM9S1vJxx+A3Nrxizx4yhQx2atnd2ly+TlxkvH64YqrAqFQqshArhTBYXIu73vgWMizgAkqRiFeFZLmcsBBYDqR4TUboSXkz05xNPjRXunDBiV8VJoNSmeG7pDzXT+NMVVuBK064EZ+/5QwkY2l4bQN4so9eYkOIHoJrUfQqks00lkogTVoCVE3WYQ0VDPblcOSsk7Bw+sFVhdGbwz4kvRTiHunLu10xpQghE7x4U8AVR/8AF9xyGDKNSYSjAYwxHdkzPh/H7n4WqG/wMxd2nHNQLFHNaZiFag6tHJBlBK788DfwuR+fg6Fm0s3j4d47UKHXZwlC/hQtMBSkJEovymFG+FF+s1YgrSka5LmHfpk489UtbKdE7IAg8af9SQSDNNJystquO66eoX8nP2T+dCAFj4Y8ikCR6WbeeX3tXinI+XyNOHja0XasHFmCu5+8A89teAKhlhDepIN4+ZFnqOy/xtdPuIFyZ03pyzAhYZ1T4upL1edXTE/f/OJiNJd7MYU6M5gNYTg8HT2nnosJMw9VBwVRyiULz1OKLKuN0HAZBRmJhqs0/fSJf0c6Ooz8UBqfOvJaZxxiC+OZn/2ld9uDOV2LcHD3kXh59HE0jW/E1371WVxz3K3u1x6cjjyxYEgclkCYEAVlTqLXMKihvKnAS8lo21UUa0w5ZG2OrcRk7cDrw8+T+WEUc0XMHC/rVzujk/siqOAvIBGPoyU4GaGxBsRT7WhOjkdPahbGpaehOT0B8WwnWnPdaEtNRGd6BppyUzGuPB0Lmz+IC953Ba77xD2kLr45nZrcuRMaUxMQGu7CzC5nmNrkaAtOwH+d/Xt8cu8vY7+mw9AV2BkNucloznegJTkJjYWpaMl0o3l0GroKczAzPA+HTDoB3znjJpx94GXkg0vmtL7uBGkszkJgYwJ7tOxrc+i9ZSm1+ydnUsG3T/8FOoudyPcVsGtwF3zpQ99lH7qW6kNH61Xl15zWBcTXil1bF9m12xYHWsMTcfOFv8eFe30O8xsXk7ad0UK+tKQnoy09Be2pKWhNzkJzdiLaMhPRkJ1Gfk1CItmG3ac7XNogUCJiJbptWZWw04RdkRjr5pwt2KPKJ8nGr3VKIbQ9Xa1Z9piwCKFNrWjKd2PGeNaabJYzcW9UE6e6cR1Tm/bC5478Nm448x4snng6Gga70VKZiNldswyPcj6lsZPDc+BtjGG37ve5dkJ06jz0RaZjVXgu2k/6KmZ88V7EZi4matEhA+AkNAbtKOoq2j0bA4kpWO7vRnya20KeMHkGUisq2Lf7cLxv/BHsS31l5Cv7WYtxcfbwunq4Xcwth1hzyaFXIbCpARvXbsJO4xjlBbY2pnMUlE7x6V99BMnKINOqIdx99os20EDS0oQ8ET5y0DXqvuxCX7xhrzXTk555x0Hwx/1I9yXx87Of4aIUc9SRPWVsHF8gczU8bEt+1+7N/0UQbZrHuYB3g+64usuW+B6wrX5q0yEXUQOLqO9ocbFW/bTSGh7no/8edF9c2da9/x0QDq279i28gu2ttUaXoHa+dZvorfFya346Lm4ftsbxj4DordFa44GuNb/m0eFqjx3N6ujS8V58rL5oTKhgA/rRhU4KjGlhIIaCsplyKV/xMUe9+N5jkPOG0LdxEPed8QKxc5jtFJA87UDJCPivtm2o6fUgT6s3JrBvARmc+JOF6GrtQiTTiu+efJdN7ZbDT6FiKqIdbtsMyK5BaSUL/nDEtupCOS6HtYZeP8gz5dC7XoEi6xQ97VM70x+lQnqQJJK8PPPaUIG4SGclRvsTezmXDNGXtTzV6NNvJKz64sy8F9D2lp9RTkV6gExgyLb3hNimB4ryAWWlmqw3ZPgB5v1FFvr+SiMDAdU/xHSuyGigIlcS0HyMJEoHjV285zEFUW1RKdCLh3IIMc0oF5iS8pxkEDc9GHlctl/u6f0pPbjkOomrYg8tk6RB+bq2lRmlinoYFyY94rtWyb7s5y8xvxdN3hjTF8qMpGgLVpsqlVKMvGTqSGYVWdgGimHkSHu40ERvquf9XB/nFj4VpHpGUtFmDXHoIafHghc53tTPqFmbukcYvKkHhdXtWq7C0pEiUuRhnPSwxGVdUVJBbhEgaK8SqXbQewl+ybKg7CPPdVIuTO1zzFakH2F6bPG3xLRIz8+8ot66Zt8i0ypt9zPVyisqiO9M2cJM/fKBAlnJbIc6HKCMytQDpc0VT6rP+Wu7p9sE6aJ4lSCtdGxM6bHz3hxDWbE+0/4CI4i9vIIL7/wwlS2J/pEB3PeJvxB3nLhNEuxEZpDhev3E/WRVZx7PJFAqK/VNCvjc24/i2qdZLAUi2Hv80Thn0Rc42vXV2yF6lcF+xaWn0aUMnv3MIozXJhEXXPSoXGR8NtKBSqKBCsaFjq6goaWRoTCLUYbUsQGUgqwZ7CU6MqjQyOJRSV6SxV6UmFnmkxjl3XptIY8Y8fnRkvMjRyPQA6ixUBNipQ28zzpC+T/pKqgSi7bBGxtDgoaVpiKWmTYFqYxSyEysi8b4NqJU9jznjyh3I09G4i2IJkeohDIo5eZkqhxAMMsGeian76SKQmcxGEqO0rhGpVKmWB7rr5KeAPNabyUEvTwCObfZ0R9qRri1E+m3l6E7Sp5xnWkKLehLIUIFKCoFJl88GmM2zpQpM0qeZZCjMXmNHSiOrkbI30A6K/xOY0xbrkppy2sQyLJmCVORWQ/YjhXXkgtQRuSH9tMKVDqvpOpKTlKKHSIvZLRVJ8kULMe0R6oXKvgxTENLNUxCONaAcu9SNKsE0HrIjwodkH59WaCM/X46GjoEVlJmCAUyTiqqHcUc79NcEY+FEEglKTsP5YY2+CjzuAyc3tzjGgpelLUra7AQdUl/b4R6o80cF1XESxkzV0Fe+Ct0vCzWFNO5SPapbm6oZmSLAc/9rIVLrA2L1DXfghMw5dSrSJ9cgWq9as+Oxm5TCh+9WpaRwMVRLYAd5Nn4LZaoWbm23Zbh8Mo9cQfe7nuDwpCXraDJcw9dZMFmZmSun/gNsb5GN2B8eBRNiTgaG6JoitEDsHaZcson0dXWiJ6eBOJNrQg30pOxQJvxkfPNY7bFPcRDIUa7HvRHuxCLxdGaaEYwEsZYsA1xFsY5fxMSsWYkeB6cMA3hlmbEWPP4aaWzPnomVhQTNJwY2qIempsiaKCBlKeyNolHEQwHEVt0ENqmzSFuntPAmufMRQMVKNESJ50+ZLwEVqENM0+4GDEqboI0xmIxpEMRO2I0toYE6U/QMwY9bPI3o+GY8xAOFtlvHGlptTU30svqDYgEaWltiCMaiiPRFEWgeQpmn3AmxnH98z55OWVYIW0JNOrpuRfDYLiNdPoRa4gh1BhGYo/d0Th7N0QSMcToXPxTWWfEEqzzQog2RpCiwfdXmjHj1IssyrS0RbnmMtLBDgyHutEQDaIlGkFjnA4l1oJBbzxGQp2Ik6ZYognNlFGM69FPYX10PnHO2xon38j7AOdoWHQMdjvo/Rg/ZQJ2Ou0SRoIo2mId5EEU2UAcGyiXEPvGYtQLrjuvnxZEWzBCHYkQd7uUndF70scvQ47zqz5NRCjnmXNtDc3keYJyH6XT2BgkXynXeCSEcFMTnWIzMpEWZOn4GqKNaIk1IspAMJiYjD62+aLNbPOR5gQqQdYZ0R7GgwiauNZEvIF8iqK5QboSQ0sL6+FB9+xOmi3V57edYnbrLNpBhcg9PPvG49ZmZmbRQ+agfs7ypPAyHHs/S3FIkYTw4siTVGZ6ulQF+8xayB6650bK8yh1MVDqFlD8SdD6lbbRX9B45FWzWUYE7bBwsb6xDfRC9CRCk6eh0EDy9N45Rpeu487BtKM+iFxrOwniTJk8Zp16DkY6FmDi0ccRj1KMAho7JwFUcjp+lAolZEsR7HnIhzDl+IswROUscH3BEgU3aw7758yTMLiRNnlyzkfa2ubshUpa7xJVkEyX0X3KxZh7yAFIl0knhVvK0VsGW9Fz/LloP/FMZqcNti2Zk/IfeAImHH40xk2cWf2JqSKHD3lGtfyeB2DyN+7GGOfSC5IhPXwljV6MNMTovT0Pz9/3O0SowAUqZ2GsiO7jLkXP4qPg2/0DDE1kjFJhpaYSCnlYzqXRQYMpZhTRmPSmRjHppIux80FHIMV01efFkc8zsWBaMeH4MzDhqGMx3CLZu1Szeadd0XngUZh1/hc4NlXVASZQqY3o/vT30HjSF5GipxXP5fTGckl073UQ1t37I+CvT2Doj7+FP6K4kMfI2AjGf+xizDr6eGSnLrLfG5VyzBT2PwnTD/swZp1+kb1dnKO8mUkh3D0FCYZdOdgyaemcvTPK6SLTKkZuRqpJHzmbvDwRmDIfuTx1j2ttOeE8TNx7ESac+VnSkrG0OZD30LPPQky96CtonLUbioUyStkSug84HE2LDkTX4hORTTGuU38DTOv0zEOlREH6rC3xKuhtYQY5ZyC7TFiI4eKIvYT469dvszblv3L6ChAWWh1PLAA4ztU+WZSXe/Hy+hfgEb/+aMKkpt3YV8bFcK2x/Mf0lIqnkpUGQhZqrBvPb4ZjEau/0tG7YT02rt5AtdGOP1MdKor+SFiJKUWGhuXNmo9QPoTYwFqEps1AplykRwnihRu+jikX/ggv33oTfPQaeoGumGcU9MdsGzjgp/GVglhz90+wYZQKQQ+pd8j01kCeR5BC0TMYHws0vWyodeZJ15hfnpDMYooVb2lD38AQNt5HhWBeXmCKGQ7FMMD0xNc3gMYMa4bGJtYZAYxQ0LGuPZC67ftYu2INU1K9wpBkzpuiN2bNQk+JUDsC+pGZ1R/M2IMBZApJLLv9v2gUOex2yseRG2FqxlQh0dyK1YNj2HTrd9C2834o5fXkiBKUFFWb6JBq5lTbKGJz/oYO9G7KYeBXP0Uwn6bDYQ3HGiywy0Kmc4xCg2sQnrOA61S9U8bw03/AuO42rPjev9K7Mp0mzjJTQZRZFzS3oUKvqz+CUGKb0q6oHmiWVR9FKFkqd2c7DS2JItPl9p0WYuD5v6L35qvQvvdiZCl6PVMpM1KuffA36Fv6IqKkryTDzXLMyuXw1ryJMOvBnB7WlPSunY/2z/SL+DMbR9G6qQ/ptk7QZyBL5cwQ58Cff4d1q9ayZtKfpWN9SG+Y/MvjCK19FX3P/5k+hFGZzmLdaBITN65CpXUyZcx6Rb/uVCqn7IZ8i1CeAeZPAr3irx1AZnZUUf7fdfx+zGoZMqkVvSMb8Niqu9nNJVSSnBjvfj6qOsQVu1J6VQAys+/8+kq0UjEy9FyH7noc+6mrLEveTb6YPTmPnnHoHVM/hZ8oqpAiw1RkMXQH6A19DKVNA6+ifeR1jqPgadUFPXNg+lBmoRqhUIprlyHFOVckmdps2GC57TC98+6nXYy+n12OmR85k/U1TVBKLtekgpC4KAf4W5l6HPBB9LTQIOnh9PDK/vQMl2p/GYTLsV+rGWdUM9Hj+5M8slbb5AYH0dnD+mD/45n+NTIvZg1Ar9g2dzdsZEE75mM+3NnNOq+EOIWYH12J6P5HYMIus7lOpn8snAv0ItrWrzz1IJZ97khEK0nOz0JUv04qZOC1dWHXY4mfCp2n4lSUinEtY8NDmDSFKciBJ2OkfwVTRjkg4vFxfVQqn56LcC0+FdeKhLweS46hu70Z0YVHItjWzEK9QCfIaVYvR5L3V6RYk739EsVUQoqhs/u8L2D4zdXomLMHClyP3ioOso5IMIVccdUpGLz1i4gTQZB1YI7zBmJMIV98HK2LT4OPEbGJXl8/CpNMNq1Yhtb5e2DcEWch9dYTCJEfenLmi5A35DXzSTqsHEJUUM/HGogpWZp80KsoelCsP+qh2kM0N7W2Mv1qx1AwwmEdKGfpNLTxwFQzSMUKkY4II3OQbRlGpUmXXI/1f1uO+LRdEGQESRcKmDT/A3h9hCbRPhkZZS6MNspiVDzIQCTtjJ74E6Sv1Fw6Hf3ahd1UKD675kF8+7HLmXN2Y9PGXlx5wo+wS9NeNkBFl35LLVXXf9vSkP0Qvv/gVViy8RFEWWANDSTxs3Me44TqKaV3ffQLPPFE4Yg1JrxCH16/4H3oaqd1c37tTKjgLic6ER5ZSfQRY2aBlq9XpFPMKeNj69i3ZG1jsR4ytBmV9cvQxBCdkTGNm4j02tfQ3DMX5UHh4DIjcYZYFoaMHHpwNsr6IBpvRnLgbfRQMbMUpHZQ8vHxCGZ6aUiMbsyfy9kUC1oWi6Q1zLw1z7n9rJEUQUcKFGbnZJTGyM7CSuL2YxO9aMv42VjRuxrjO1nv9DM9JL8G8mWEZ+yK4f4RdGbWIMJCW2+NamdOqZYMsMB+xUqMHpllKhWPNTXWNU9C+7geDK54Gd2hHHvpFY0Co5KH2KSdkVm+hAVtI6MEPWa0lU4kS31idJDFJMazxqNHVeHJcSlGvkD3ZBprkhGuFxEW4YVCBUPs10Be5Htfo9L7qRyMIi2TabxuV83Pdn+EDlGv2Vc3NGSAEmmRxuUL5FmkszZgypZrnU7DY6Re+zcEmc/HyOsKo+Qg642Gpm5k+peikfIIcO25eIulWmWm0dFsHw2AeQWdR5pzh0dXWOagV/Wz1IVQsp9aFMJYifg65mJ0uBcTGlpR3LSMvGJ6GeuCb2Qtyi3jER7aiBxrlzLlXereGVE9VU2thy/VRz7QkAPj0NDWhOzICFpy67mOiBlCiFHDzwg+TMNOT3k/Zl7wH07HZUTM03VquZse5X/j/kvxcvZpJEIehgeyOGbuSfjoojPZY+tXywVlbCy/jWt+eTUGC2Qki53hNX34ygnXYdeODzCfp7BZeMlQBHoxzvZnGPbFOG2vFlc+hdE1LIoCVFIKXw922ExvpjG0aKZUYCh3Zikj5rd2HWhkAaYKthukkMc0wE+r03tGqiGC9BYlhmSreRipSsRv7/aT8fZHE+gTSmE9ndGf9ZEnYaqi31XTY2mbUluvnIHtnJ6KEKInzbPQtd0pCjLMtELbniXmq0o/VZkp6pQZMe31lSJpZ1qguKto6y/m6VwiVCqpr+jnfeXZTN1k8PplkbZ09Zq9X3/iiBFXKZ6URH8jgEmbpWHaRvbomIr+DO8zffTHES4xNeF91Ynimv4iYoApToW43TYx1ZnCt9czSK+29LWdrBRXBuej4pU9Ohrt/PFfNNfHlCZBo43qVW/epxens/LYz5yj5uBZgTwNMuLor9pwBVyjtpM5H5VW9YeP/NBvLFSnKtUrMmrqtyOaQ/MWfXEE9SSfSmkRW9iV4nAdltaL/7ymuBnhlX5Tjuymh596ml8JRNmJ9/XzDK5Jb+oWPb0ur3cEua6CXgASK0lrKEq5Ui4VFpeUt9/Pe+UWRnJZfJHrUPTwMfll7bLoSJ63kW+KKcRTYeVUJtMNGfms7y/fdSbWlN9iRKggw1QlPwwsmLkvpjXNQYThdTDTh1fW/xUr+95AooMekWFirHcEFxx4KQ6deZoRrv1vWaDSL6U49r6SBGlPJ4P03G4njL3MI6lXDZwYZCJkiDt9B+i+KUP1W7D1uUBjqwFuu/A/GfN/Cu+m+d1rlBuoXb+bPsG7efU/ha3xaB7Bu+f6R0BJt2RYg3evT+fbWsf/S5A2KtEW7Git4okOPVeT7uqNZ1+hrOBGkcjDKB+ipemnkTc/fi0eeu1uRjB6d3rDfEG71PQKDIcer0MsljVblilEuT+Iz3/sauzWtD8LpyIDl94WotcjFfLC+gEWs0RaO+sAhlmPHkS73yV6nyBzFnt2wH5Wo9iLbCrOZfEunLu/jiJRSHXkryla9aW30ErlcXRS2+PWpTykfseifpbesU1+Qn+VUfe1TWB46fX0ark9bGO+La9n29ba2eAow8X75j2JzzwgadbzFmMnPZrA/o4tvZFAuaxo1HrUX7gUkcQ/PbRjiLGQL0+udlckck10kTlFLOHj4fxadYzQqJHeUbWM1qm/LiIP7f6WlkAqT3koPVIuWAWRqkTa5GENolt9SRFvOkMRsZQJL4idaIinGoHsDz7zv96jE9T4qgWKcvc3fMlx473wsVjnp+4pQtpbFrZOIuE4o5J4JQs16K6LTrVPB6ZvvFCvIj/d79mFg72MfodP+wTiuB4+aldPt0SqHL7AZRK8NoGQoxymWwquQm63+cHg6y6Ix73/wYPxQ6+oVHvrS+yhcrEwXFdZhjse/TGWvPYkEOeMUYZAGocek5RYIDeGW3HQHkfh5Hmnc6ReCiua0gtflae2OOcXSRIbqYcmUCv6tSwTME/Z2XlPtbNBQlAvW4xbSbUbgcLjtXA4Za71d/O6OWlQjFRSbjG35uWEQ4eUzx50GmslUMU35rm8dhicQUpBnQAdVlEiGhwFDoujyajlp/ptOTOjEJ3VeYSpKh6HTD0oGcPOa6UbNXxurJo1VqPUQkfGa51LnUSF6KitWlzUH3JTgqdVag3CotU4wxOmWpwqWQ/HOX6Sl8ZO9tZGilNrx2Oltupb7WkydG+A88qsT1i5SqEXOMK5HtHI21XnJXB80R6Rdu+ckasOdHf0rfui1HFflBpuo1uItfrqWqggVmvaGKXIpJu06GGm+CqoZnA8uAIapebT+BrHnITVxfUXX6ovSHGdovA9oYy3ki9jdf9ypLMptMXHYcbk2WjHFN7jNI7aOtThnw7e00C0yWWWJQOomZwBh8mLmMthD4XPOtThnwzeofLbAuWNlioqECnXVMVvO1E8ZTqmv7xRhzr8s8I/kGIpQ+OnrIRBQr03F3u1tJEgNPUoUod/NngP97/Fdmq6r+/NrVvZQ9046vDPCO8RQbR15ip8A+upSFLbaZCN1FOsOvzzwj+4i1WHOvz/CXX3X4c67ADqBlKHOuwA6gZShzrsAOoGUoc67ADqBlKHOuwA6gZShzrsAOoGUoc6bBeA/wXKiseKsnLpjgAAAABJRU5ErkJggg==\"}";
        
            //#068335 
            // Theme Biometrika => "{\"themeHeaderColor\": \"#068335\",\"themeHeaderLogo\": \"iVBORw0KGgoAAAANSUhEUgAAAMgAAAA8CAYAAAAjW/WRAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOxAAADsQBlSsOGwAAOWZJREFUeF7tfHe8XFW973dm9vQ5/eS09E5CTSCEgAhICwhKExARpLerKCiKei8oKCpevVcEBS8qIApcigVRigpSpAQEaSGEJKSc5JTktOn1fb+/NZMETILvvvd5f/jml+yZvdde67d+69d/a+85vgoBdahDHbYJ/up3HepQh21A3UDqUIcdQN1A6lCHHUDdQOpQhx1A3UDqUIcdQN1A6lCHHUDdQOpQhx1A3UDqUIcdQN1A6lCHHUDdQOpQhx1A3UDqUIcdQN1A6lCHHYAzEL2uWDsMeFLiV9k1FVHgKS/4jVIF5UoZW15xVBs7F3WuQbqhbwG/y7zmf42u1O7b2Cpygr62dWwGuxAGHTUQrgJvbenp5tgCpJKHaHcz6yjzU61bem7BWZtBvR1eXpWrrbrkUf1ysPlkR1DDui1QexVpDTZPUOWVfZbY00nA3dZnlSZ1sztaZW2E61Op8qfW/u7DAc82N9S4VANHd+32tg7HU3deA9cuTrvxNaj1dZ3dqmpQw+d6UacqVKgKz42cEtXLSc1dAXl3+p6wma7NJ+KT48mWtu0DDYS91LHa2SkVyfERiY9CKZfgr3imJ+7FXx/KvFf08dToD3JQAOB/qaJBqRaY2MnHTpv5oKWxrTpXxU7djNs63CeHay4DnahVn35iC/C8epOGqBaf5pBREnTPVwnok99cLOn3cVKdVyd3yKv0CZNhq/jZhzf5DZ+/SkNxMx02h3mEzQvbAThatg/CpV5SqCJKIsvmEW5d8FP0uKvqtfu2w3Xhoc/yVn105ln71jzd+qhx2A5214yuv/4JXI+tx7z78PFTMxlNDoGdu0N3q+3WT1KT8ltrdaQD19PxU5Is+wK8olKZKlEH7Q7BHFb1uiQZOFzbBd3eQgRBF9QTk694v+Px7JfjOkNGiFufli1F02J4SPnLNALPDRBUaL4+DnHCFZFhLZsKSDuxxRMTF6j1V2jrPhmRMYZtOjiXvIcYsZWKbwOc59e3n/+q7LYrnekgNgcyCiqxtVY4n19YdV79NtB3ldU0frtt16JXa1aDFqoxjqotilOwuXzqX9YY9rVuVXzbBY3eUR/RwfucX+ooRjulE63il74Fjh6HT+du/jxxC7uJR111VMkzMM3dPijGaKxbox+e5iNeccNkaeNrc78L7JY8vSZ0MlFXNyU/NF5KoRmMbElTmiJ9oOTV1yHh4ShQu+mSWpWpkL+uhThsQE3neC1vElDPHQAdvHSxbPKlDpmX0zxq1yFFVtu2gQZCzdo8MSfTOccrgsjzaoL12V7c9+JNaAw04bh9zkcMCYw9dTtW//5WVHKbEJu6H6adfwUNp4Vrp1ch08R2eb4S8ZgSm+UTv08RSCQ5Rhlpone7kOOhcRLjFnYaGNP5UfXyNV3QEoxtZLB1sgFm7jZe98R8jTUvxVaHs2gzOIrlfd1QTWFgPNIFBVyj2SbaPryDXhsr2DJIog6wU41266Lb/qo8ahi0KJ2zo6PWuTL58ICiZA11wCU3blSAGGRk7tbfAdFoloDNWZ1HBPG8Qgfjk8ze0wFonOhyBm5KaHOKZo6vGkjNzqVXmqc2nVsuL/i/TN1Qs659ptjqI7lLn1wmYIiIo2i8MTPZMZicxSnJlKPkAG1u56A55Q6BPKhUXCcpLKFIBNLgkCNd8PMl38UvV/4U+VHg6g9ej+nJItZddxFCkxvoQPxoHsmgv3Uupl96O+nn4nx5YgvSVjheuEWQgOdKVRSdfCKUqZijt9bhnaChTvA8YxcLQCZSCYJKqrUbpx0LJRyHi+zklwzUhpohuB4ap1UZ3pqUCMZ3nYiZ1WZTRIZhKVlVrka/kWSt6iHGbx+2cFEg2gVujFAqLZXpesKm9ZiS8J+mZkOghr82v+ZUH10T/D7KTYwRr21hok+zEi8jqTxwtes2gSgdL4hTqbOPXtlH5hUqOQR9Yeux9fh3yoocd8xwc0upyVOjkXwukZEe2y019+XI1yjncBwJikYussR5HYVu3Ua3nTmHptWrVYe5QYve+nLuwWOrxm4PhMmniqWitYiOEstoYtV6eV110dsFmpCmFiiz5mSiiFHnzdwL+OvaR3lRRCQStaiRCLcgHo4i+ez9CHTGcEfCh591enhuQhOCy59k3xzndZHBcUwgovjFw7yH8jOB6hSuV01+Mmpbh3JYS3w0XmsSKo7xU/FVGxUVqajAOjwatYTlsbNXElOkZK5EN1o4n1eiR+U4sUYMtlJct82oRQy7CQ9P7B+VRAlMRd6RyKXevjJFQgZ7CrxmSTr+QZDBaj6OKVfz3xDnC1i7o7dI/hW5UFEpSqRb0jstQdNVuGYlVsqhnW9QpCM/qNTOEYhuRu9ihJ1l2Fv4ua3D1SqOxz7K2hwALzyfoqSExvatjneOF1u5FiNEB42cDlaGWSLRWpYkoY0dX4kVI7sEhJvrKZekB/LoBVtrkfcU1WUEUmCtSTWl+gjcWolK5BKvxCV87wXmSCohq/ltMA2soqyE7Z4FhR0jYSAVEQQSpaJXXEgTwYXf/zi+/fvLceMj30FzsMUsv0glEcqyF8baiIc3OxJ4rS2CVQzrpZAoF0G0LhJB36WexMcRxKmgJJsX+EpkoGJjiHNvzf2/O8gEMYPeB8jatc8vGhVuqaSMVNZWZJtSAvbVNQJkMJmhqBFkH2NmgBMSmY3n+vxUMiUgbowGcT4eltIKJ7/8rL3Mo5K76q/76u9Xf+KxKGg9tw+b7xp6XUn4Wjd5Jpyi25BSIbgubSQEqFAeaZYo7ZYu2JtdOFprp0pZjSWQFxZGt1tXKYherkukVdeyhZ/bOqg57GfdFMeoiULtK4jP7z1e1IhOy0CEp8oSj2sJykjkfNgLpbjdF+9UnvqZ+8uIFA09Uh/UnLwhny55+ORA5Pw4WlM51XH1li58/oL1t6m3Cxwt7yJ8Qa7TeMKoRJlqC6AipXwP8JXpBs2L0HpLGsyBSZrIJ362PxqjUcyfegjmRubgzldvwGiuhC8d/h/YJdKIm2hAS3Zq4kI9HPzWeuzefShmnfJtWr/SG+KTyVvOoOXILzhG2JZwIIQRjOLqn5+LTFlRR0rrgEupnrmVSyFKYhbdRSyaQHdDD+ZP2A/7zz6KDFaJSbx+D2tGl+Kr91xCxnn47BFXYlbHfBTLRd4TS/14auVv8Vrfi1gzuApj6THIgXmKRP40SQpTaCFEwwE0xiOY3DwZe+90KGa27CXnxxyd83MuBCO45akf4vnVD2PvSfvi1H0vNRp3DByrkF4VpGTiszCfpxKF8NUHL0ZqMInj9z0NC6YeQGWQJlS1jP9fHngCS1b8ESs3rMRoSkgKLE9GUEqF8enTvoWpselEXZTJSL2dw6VBPbv2Efz4oR8gGKIK11hKEAZdyrP6qHkeB6T9cUayDILFBM8HMW/Sfjj3gC/j8Tfvwu1P3IZQiNHIwC3CcNh48dDcEI00hHg8hNZEHDuP2wVH7P4RUsOaVCD/xgznsv8+GUPpNA7f6RCcsPBTdmv0b7/FG7/8CRp8GbTusT86jr6QrY2mJvJl4sbmSCHCs6vxwjX/gjANu+fES9Ay9xB3b5tglOKuZ27CfX+7k3wr499OugazW/ZhRJFOvZM324LAv11xxZVSSnl3eTKdl6m8d7zwIypLC9oaJ2JiuAvPbniSOl/CPpM+hK72XdHbuh4v972JUrGAA2cci3kfuoqrydJg6MVYYBbJHk3ucnRHhbC7Rj/6Mmtw9ws3ItBKmYayGPUPIBMeQio0yGMYqeAY0l4SGQosGUliNLgRSf8G9BVX4vllT+CF5X/BITsfZ/TKCz39xoN4dvRBFDiuOzQZs7vmkRY/BvKr8JmbT8VjvQ9gfeEVbMBaDAdGUQyOIB3ox3BkE5JRzuelMOTrw0h5NVaNvoEHlvwWq/pXYP+ZEoBcn4c+Cudbj3wBvrYRvLn+VYykhzCPxirvTXZTabhCWyoFU/NOlnLqXMbIO+SRvH+A+f2X6SCW4QWuexNe/NsL+PCeHxcSHj6MkpYv/ewC/PbVX6A3vwyDgXXY5I2RL+RTrB+jlX5Ey53YbfyeHKLk2G0qyIBUl935zM14q7IEqdgwMkG6vNAIvzchH8qhGM0iH00jHxxFyr8Ro+Fh3u8j3zch5w1j3epeHLvXGbhjyffRR37l4iMYC0gGo0hTDllvCGNhnpOesfA6JENpjIRGMeLvxabs21i64UX84o+3o7Etihmtu1HLqNe+JG58+mqE24DBwV4cvsspRu2GZ36D8NKHMNHbiGLvcqx+4E4EaRHR6fOrXCMrjSW0fDaMvPEoIs/+Gu10sSOhOJrnHMj7FmfsfkFpGh2Q0l/p8/JNT+O6P16FcE8ZsQYfHn32QRy758m8FzbjM19u0lMKXY1Qzq4Mqls1DlmZiaDPo7HQ8waoEEWFeq6uSEwV5h7lAlFwcsEz63sRCnoYGRnD3APOsTaLGkxLlAsriri+JF15tnZltGVsFAARhdKQZ/iD+QgOmX0Mcim6moCMlTk/I5O2mAOMAMVSAQF6oIHh9XhpxXMIjwtgefYV/PBPV+D8g75i+DzSEijHpVvw6BlqcOU9FyHfPszaqR3e6jxmTJ+LrnHjEcxxXtJWZMFV9I2RlBi92xBWrXkVg4UBNE7x4YV1v8NzKw7CgmmHG66KUq1YiYrZinKiQuW9G7uP3x3zJx7OiMCQbW5ZDFVaqaJQg2q5joD3mKb6uPob/vA1rAm8jEZfE4oBpnxNSuHUQ27Ej68/9EVsDL+F5iamJht82IV0NzfOtHSxgjF4LT4q8SkmZBMr162ZTZbk6akLL0L81TiCTIcDxShKAdYnwTzGhvvw2sArTMsr6IxNxtzW+chJS5iKFv1UwrQf8/aeL0zwhWImD2Wyi7oOQkOwkzzO2VZwmumxZR6cL1gMU+Ys7MtprFj9BlbTyURnlPHDR76NeePfj87oBFLIgtrXwLQrikrMnhFIJzH5Q5/GyMTpePu+G9CYWc+I6EPq4f/E0j/dhonHfwrxecfLn1b5yCLAC6HgZ02hgrMa2eSUpelav84VS60wJx++8pvPoGniOKRGN7E0IM1tbHvgc7jiyOvZXyNkCcY5O6ud19pZ07JBxQsNgzZhEGaIC5ai4o1yMFu8mKGHNxUWVYKNw+vYn235Ilr87dVMIkK0xCNDMJDg3LTvBkcaGU3P0NkQxsm7fdLdeA9IHbQaZ/30JDS1N2LJ+j+yxRmIgQTGKeVRBCXG9t7RNeiYNA75AQ83XfAztjJkvQfc8eINuG/pHYg1teKl/iXOQIhSDJXRq9hXcdo4LoqvP/Al/OfHpmB8YrZllG5d9DvkmVGh9EosMNLY4ovhoWU/xzNr70GgRQrIQazJjHAD9730jZfRNaUZg0MZ3H3OE9b2d+D8Dw8lxtW5qTxKSTsaJ+G8Rf/GlnfCc6sfxCvrnpa6Yi5TqY/NP7d65+8hr9qSSMupEo6aczSmtu5dvbMD2Ae4+blr8OjahxFpacJLrz+Nw+afwPmoQ3SSqmL9CgkE25sg4U3zjrEjs+ROLP/ljYwOGzDF24T+X3wNvQ/diknHn4fwjCM5QnUydUwOhWv2lZ1TUWh2m1s59qBjUsVPQ7ruT5ej3FREauMIPrrgQjy/6imszr/KtPUv+NPKu3DQ1BONaSVaoOpR2Zy210vE7TRYmwhSKir6w6/eTGs7Hz947EoSXuRiaAjGcRWBDFtmoTwU6gi9m9bqJsY1dtm15CrTqVBxhFLfZUsvHDO2DVQlFtieeQfCVl2dGRJqbSKeEMckpo5R5PMyxVpuLJCmGMH8cp0L9HaBMKNEvoSu1g620DjYRfKRwPKsA/QaTUGM1rIsOFYwp2uu7bRoxysfqApBaDlKQVLKHw1HUEnnkZjYgM/87DQOHbDi09VzNQfh0gJxzi3Dj9Wjf8X1f7gGseYGRMptKBY0sdJam7zajy300FKntmija9ANLYvdbHXqrmmkGJxDX7qUb9DzBOtvfYlf66sqZZbpQImRBB7rNydKw1exVyU4I5N/ZRACOccgI3mJmpMtOD5kiM/mtw+HtkIr0jxWKxK6wlMQyJSRIFH58qi1ibpKkTWh9TFtNvBTz7QLWSCe6F4nYeerH0b4+H/FsvJ4xBgFulPL0HvjZXj9e2ehMvgKYuGQ1RJlPSKoZQq2fkdQWYiozyvGXsajb/0OQdZXE32TcPROp+Oyxf+OZH8aHW0tuOF330Gq2G9j3QfHiwXURcV4t0BVz/ZACnjglXuwvLgMv3rjDi4lweWq8NM9MVb1iTMQP40liyHW2RQquTOxfZqNd5KVIlC1id+UkJNZ8/aAN0NcaIj/VPco7x6joiXRixTWI01PMuIb4B22+TdhHXPxO176T3geB1KDw2ThFrCVuq/aKeVQZlhUAeuMlcCh1oXfAa5JD9mC2tIUmNzoEFiQa9dK0cL26zcDeUC3l2WqOaljJ5z5/kuQHEiieXwcZ992Mu87JdLWgTFXhk9l05VEOVpah8/edTZ6xneib2Ma3zrmRxgZHabSUOjWYyvV8TiOU/v9OY7Psz7agDH/WtYCa8mPtzEaeIvfGyiJ1eTTaiou7+vw9VrqaMBFMt4RiXaGXJOsuKBtT177felqI4F1kW0vqyKWFyBEqLjKGNQ3WE1nosRnNFYJtR0vphqqgQLyEIT+4ip4cbogssNnT6rVmfe1u1ijowq+Cjlshqi58uRaAM0LPordvvoA4sddhjWYglYO7+l/BmuvOQPJ+29gupzgSD0dr8pfOA1xeHMW9NX7LkbbuHaMbcjhylO+Rz4U7FHFxYu/jvVDA5RZCJfd8y/W1+oXiZnRXHoharXxIZpVcagPfMEQ87sCQuEor5gHi3Cu1zwmr6tPE+ExhC9fvxThSJCet4TuhqqBSJkUM0vM/zjOWEXlq+7MW5d3g6V3kQLWja3Fx24+DAGOtTYl2hpFArQJleO538uY5wg2MK+OBZDNj+HsD/yrQ7Q1aDlVWvWsgHpGhVE41laKGvUhmqiSopcDzGNIMRQpxR6O126c8l5P2502QkMr5CEFHgxgZGMR+8//CJ7rfA5/G3kchcYoLr/3HFxz3C1kK2mXcjE6+oVXBTsd0SW3fBSJjjYMjG7ERR/4MsXVgZIKPEZl4d8aSuzv4+LLgQw+/l/vN1fl59zaOjWj92fJrwgLZiBGr6lnD5lgFoXsGE7c83ycuOhCOiiZhzRmC//1+kiJ6/KrZqq26a6TkVN+0xX7Jv+YTgaiQfzmtXvQHHicRpRBjE6tzPCTJR8CdADa7q54HrK5PDb0v423M0sRaKugb/0Qdp2zkJikUdQFLqJC5ZA+CUz3qCParZI7DlCHVDCXNCdr3sg+p2EOj9Hn7oT/4R+iKZBGafBNRld6+ayPvBgzPBYJ7eEKEdFir3/iCmRjGZSGPVx44CWI+pk9lFTflrDfhMV4qu1hvJp51pzNj5/8Os7c74vm+PSsS+t2a6ebodERnWOenriGKxnWHpota8RbusBvFdwarGEei89V/W/QUIJWPHdGJ2q4jVd/KZ6qupIvKa5wAseMbYGwl6j0fj1DoV0WE0x4mnMoNhZ4zmjUwPvhJLxYCpVwHqEEVU+1L3EWCx4SiWbD46Am7ncBDU66riJVoKfFloIYiBUyfn7xcBh0oqc4Ctq8V5HDsKWwXXmqUsjaA1HgksXfQjQ/Dg2FMFYX38B3//QFty728zHSFWXh/P+Fu09FoMlDIZ3FvpOOwMFTjrXxisp6ruFosYYqyMhEfpgKwV4NJWTJl1xLHgWel5i/lBtyKPG8wDw720Se8b6/oYBM0SmOvDopsXMjihCQ0VBTZSi17VP7Ir1KLsQmEzVBLRrnRfx4pf9Z/GHdPXi07348tP5u/LH/Pjy2+gE81PtLPMzjofX34rGBexnLXkIlksamlWmcc8DnMCk2W2pDWvIuC9lcnwpElLkn3leFxgKewtCzGO3zGsnlYYz09yGdoQmRHBXhRVkEXXtJe8EE6WpJsuG4VamlePj1exCNhDBr6mwcPOtYxo4h2mGGM6VoiGP43JGsW8nXRCiIB16+E2+OvmjsL9qrNdJ/4eXsJMOzrVdCkUpXrjCM+kbsWncDLB6lPHpyrGGiW09YB4rrmduFkMyNYFr3DOutZaqoF0+ve/QqPPHqnzC5bRq+dfKtdv/dYLPKahkWxyVa8Ml9r2JUSNMJEBPDel4JFOnN0zOoTpGbSeVH8dRrj2BZ7hVEQ1HceP/VuPbkewyfA7eWGuhK9ZMr4JzUrZaS5kkr7JvrlAbrWlziqa4tWSQtOc+lTQJzJvTsAQmompqW6Zn+4+O348zrFyMyOYan1jyKOUtvwWLmvGUyXA/MfvDHf8Oa0goKLYG2fCsuef/VjhzOJb7puYi9vrA1kDeKgKlcAZ8/8nqM5QZJGPGVlTJKdZXMmjsyEDpbUt6PhVP2tzbDTjxap5vO4iYPR7v7rC6d69GaLS+tgmYoeZRQOYbMyEbWTXG2xZhtKGFJY9a4uQhnG5FOpllrjJn8WyLdmNw9CwcffwRa0GPOyQydyhmg5VXsudQWKJuLlpsw6um8qslmZQB9v/wJ+v98LyYnWJSXwlg74kPHvAXwLX2BRsK1+eLqiSLTBI9OTfCNOz+Njq5OE8+agbdx6k2HUeZcv8d1a3fWDMxDtDVMWvJMtTrw9f/+FG466z5qXJvh0ANcK9Ml7ip7uAg9KadV2gIYHVRo6pTfASqZHggFaF0FCnxD6i0LgflsEdPbZzrp0ITsFXjCS+ufRXSCHyuSS12D2oVPIIERigHVKlQ+GkNjuQm7duyLBRMOwcKuw7B3zwfxvp5DMJ/X+3Qdjr34vVfPQThgyodx+ZHXIZVMIUI6hivOUwrEYG0g+LTB4JZEWkkamRGiUeihnMAe1JFRZW1pinB6tZwZit02CGmPnG1GsS9rbQIV0nqTzO1yOPXSA8YwGvC107+P4fUjaGlM4OYnrsMrw3+hR/Pj98vuxOOrHkCsIYQR1ivXfvQWjtLcNdDcMsgthijQVjm0QUBvv0v7Xlg0fjEWdR2JBT2HYZ8e8qSH1/xeWD10vq++Jx1KRYhY2mGRUR+MDjWQjBQdS6wH8tUo6CQiY6LxcV4zFEIxwGvKPTs2hPMPuxjzGvbHyIYkclm9p+XHmvW96BjXg7OOPgPXHPNjXH3Mrbj0iG/ihD3OQiONQ0SYanBObaoXKmkaDAmzx+2iQ5yo8pFUiNwABtF7/7fx18uOQuOSWzCD2UNvqoShnQ/GTt98AKH9PspULkd5kk4N4Ax6Z02Df/L4tcg0ZJChXqQzKUbSIUbXUZTa0sjGR5BrTSLbkEKe36ncEFKZHPWAUbexhG89cLmzBlu6TkgnSWNQcOwJaleA3/ZWA7N+5e0qOvNUpELV8gpU6DzdxGtrXkUlJO/InJisEChN0Aj6PIymNyHECNPT6tIvQ+x4vhm0V6+dk0qAi9262LZFbx9KLN61uyMVqwlSUBHTyTFlkYqUAqOHKUWJxejGsV5rs9UyPbT0g4ouA2ES4zo7VuDNoefhRYmE9UG44GiTKsksvDI5U2LaVaXTaGDXKYl5OGPRZzCSGkJTZxOuvetSPDfwS/zkT99EtHMcBtYP4pqTbiS/miTSzePFdRXN1am5BLemckFC4j3m9Vt8/T8AGsbDdiE1j+VLNfUjXhpFWK8C5SMUvlPUzUD+aT3aiBFoAyNQjtJZFjDO2wkXHHw1bj73tzh62mnw9wcxVhzAQ6vuwWU/PQ8X334S7n35NtvAsbH6UORXdLTF6VUT1RuqB2vr0bdVbJx7Ewbv/yZeuvSDiD11G+YkKOk0nfGUQ7HTV+7FxI99g0jbWUow/demQFnx061NL1WuSr6M3y39BdP1AKYE5+KMhZfhE/O+hHPnX46zdr8MF+75BZy32+U4h9fn7XYZzlrwWVy44MuoDDGLiYfx0sCL+MPy/5Y6kHbGENKtDQtyyIlGgrGX1WxvOYbSRmB4cATjJnWgOd6NgTczTALC6GjsojLsgtdeW4LpU/e0sSYI3pUIlvUtQyDiQ6GYw7yeXeyuQNbuUn8JLoRQKcH+IQqMKVtpA+5/9XaaJQkqZ02pQzID5v8yoLIKf6YyfelR/Hn5I2hta0WWHmJe+wIhrAJp0PtW0j19E8JUxknR8UiXN6EcLeL0Hx+BRTPfj9aGFkYgFqR6N6uShb/YgGQwhVQxiRWrXsOq0aVINDViaGQTdu7ax3ApgEscVhyzOKrFACWJ+k1GkPMetcupeH7lk6xFXkF8XBuue/jf0TyhiUX5CE7f67OY0bAHkeidKkZo8kJU2nMmppEBlweSJ44/05t2RrrYi6bmZvzLbUdhwez3IR4bR3pDFCCVWGkDv2OcN6fi0typH0Pky0G7H4vucCuRiWp20C6WdJH6WiIvlctbSlN7mGngUhSrvTRE8ioq0irRCjDKOqNpYOJ0wl7n8jgRf1jxOH79/O3Y6H8LGxN9uPut63DnM9/Fzj174ojdjmdGsJgI00REORK/njdoG1Vvewv0AmMokEXfAzdi6M93oZ1168xEAJvSPvR3zMeM8z8Nf8+u1rfI9XpUIKXfFfKwTNk5ozelwjX3XUrH1IyRvlFccc4PyFtXO74XtEXa8dmHzsaEji58/6FrsXDqAUgElGoxynI+ljoSCGsN5mNimlfN6+767GNki/yAxFjGbz6/hN/OQ1/14et5rnF6XM9S1vJxx+A3Nrxizx4yhQx2atnd2ly+TlxkvH64YqrAqFQqshArhTBYXIu73vgWMizgAkqRiFeFZLmcsBBYDqR4TUboSXkz05xNPjRXunDBiV8VJoNSmeG7pDzXT+NMVVuBK064EZ+/5QwkY2l4bQN4so9eYkOIHoJrUfQqks00lkogTVoCVE3WYQ0VDPblcOSsk7Bw+sFVhdGbwz4kvRTiHunLu10xpQghE7x4U8AVR/8AF9xyGDKNSYSjAYwxHdkzPh/H7n4WqG/wMxd2nHNQLFHNaZiFag6tHJBlBK788DfwuR+fg6Fm0s3j4d47UKHXZwlC/hQtMBSkJEovymFG+FF+s1YgrSka5LmHfpk489UtbKdE7IAg8af9SQSDNNJystquO66eoX8nP2T+dCAFj4Y8ikCR6WbeeX3tXinI+XyNOHja0XasHFmCu5+8A89teAKhlhDepIN4+ZFnqOy/xtdPuIFyZ03pyzAhYZ1T4upL1edXTE/f/OJiNJd7MYU6M5gNYTg8HT2nnosJMw9VBwVRyiULz1OKLKuN0HAZBRmJhqs0/fSJf0c6Ooz8UBqfOvJaZxxiC+OZn/2ld9uDOV2LcHD3kXh59HE0jW/E1371WVxz3K3u1x6cjjyxYEgclkCYEAVlTqLXMKihvKnAS8lo21UUa0w5ZG2OrcRk7cDrw8+T+WEUc0XMHC/rVzujk/siqOAvIBGPoyU4GaGxBsRT7WhOjkdPahbGpaehOT0B8WwnWnPdaEtNRGd6BppyUzGuPB0Lmz+IC953Ba77xD2kLr45nZrcuRMaUxMQGu7CzC5nmNrkaAtOwH+d/Xt8cu8vY7+mw9AV2BkNucloznegJTkJjYWpaMl0o3l0GroKczAzPA+HTDoB3znjJpx94GXkg0vmtL7uBGkszkJgYwJ7tOxrc+i9ZSm1+ydnUsG3T/8FOoudyPcVsGtwF3zpQ99lH7qW6kNH61Xl15zWBcTXil1bF9m12xYHWsMTcfOFv8eFe30O8xsXk7ad0UK+tKQnoy09Be2pKWhNzkJzdiLaMhPRkJ1Gfk1CItmG3ac7XNogUCJiJbptWZWw04RdkRjr5pwt2KPKJ8nGr3VKIbQ9Xa1Z9piwCKFNrWjKd2PGeNaabJYzcW9UE6e6cR1Tm/bC5478Nm448x4snng6Gga70VKZiNldswyPcj6lsZPDc+BtjGG37ve5dkJ06jz0RaZjVXgu2k/6KmZ88V7EZi4matEhA+AkNAbtKOoq2j0bA4kpWO7vRnya20KeMHkGUisq2Lf7cLxv/BHsS31l5Cv7WYtxcfbwunq4Xcwth1hzyaFXIbCpARvXbsJO4xjlBbY2pnMUlE7x6V99BMnKINOqIdx99os20EDS0oQ8ET5y0DXqvuxCX7xhrzXTk555x0Hwx/1I9yXx87Of4aIUc9SRPWVsHF8gczU8bEt+1+7N/0UQbZrHuYB3g+64usuW+B6wrX5q0yEXUQOLqO9ocbFW/bTSGh7no/8edF9c2da9/x0QDq279i28gu2ttUaXoHa+dZvorfFya346Lm4ftsbxj4DordFa44GuNb/m0eFqjx3N6ujS8V58rL5oTKhgA/rRhU4KjGlhIIaCsplyKV/xMUe9+N5jkPOG0LdxEPed8QKxc5jtFJA87UDJCPivtm2o6fUgT6s3JrBvARmc+JOF6GrtQiTTiu+efJdN7ZbDT6FiKqIdbtsMyK5BaSUL/nDEtupCOS6HtYZeP8gz5dC7XoEi6xQ97VM70x+lQnqQJJK8PPPaUIG4SGclRvsTezmXDNGXtTzV6NNvJKz64sy8F9D2lp9RTkV6gExgyLb3hNimB4ryAWWlmqw3ZPgB5v1FFvr+SiMDAdU/xHSuyGigIlcS0HyMJEoHjV285zEFUW1RKdCLh3IIMc0oF5iS8pxkEDc9GHlctl/u6f0pPbjkOomrYg8tk6RB+bq2lRmlinoYFyY94rtWyb7s5y8xvxdN3hjTF8qMpGgLVpsqlVKMvGTqSGYVWdgGimHkSHu40ERvquf9XB/nFj4VpHpGUtFmDXHoIafHghc53tTPqFmbukcYvKkHhdXtWq7C0pEiUuRhnPSwxGVdUVJBbhEgaK8SqXbQewl+ybKg7CPPdVIuTO1zzFakH2F6bPG3xLRIz8+8ot66Zt8i0ypt9zPVyisqiO9M2cJM/fKBAlnJbIc6HKCMytQDpc0VT6rP+Wu7p9sE6aJ4lSCtdGxM6bHz3hxDWbE+0/4CI4i9vIIL7/wwlS2J/pEB3PeJvxB3nLhNEuxEZpDhev3E/WRVZx7PJFAqK/VNCvjc24/i2qdZLAUi2Hv80Thn0Rc42vXV2yF6lcF+xaWn0aUMnv3MIozXJhEXXPSoXGR8NtKBSqKBCsaFjq6goaWRoTCLUYbUsQGUgqwZ7CU6MqjQyOJRSV6SxV6UmFnmkxjl3XptIY8Y8fnRkvMjRyPQA6ixUBNipQ28zzpC+T/pKqgSi7bBGxtDgoaVpiKWmTYFqYxSyEysi8b4NqJU9jznjyh3I09G4i2IJkeohDIo5eZkqhxAMMsGeian76SKQmcxGEqO0rhGpVKmWB7rr5KeAPNabyUEvTwCObfZ0R9qRri1E+m3l6E7Sp5xnWkKLehLIUIFKCoFJl88GmM2zpQpM0qeZZCjMXmNHSiOrkbI30A6K/xOY0xbrkppy2sQyLJmCVORWQ/YjhXXkgtQRuSH9tMKVDqvpOpKTlKKHSIvZLRVJ8kULMe0R6oXKvgxTENLNUxCONaAcu9SNKsE0HrIjwodkH59WaCM/X46GjoEVlJmCAUyTiqqHcUc79NcEY+FEEglKTsP5YY2+CjzuAyc3tzjGgpelLUra7AQdUl/b4R6o80cF1XESxkzV0Fe+Ct0vCzWFNO5SPapbm6oZmSLAc/9rIVLrA2L1DXfghMw5dSrSJ9cgWq9as+Oxm5TCh+9WpaRwMVRLYAd5Nn4LZaoWbm23Zbh8Mo9cQfe7nuDwpCXraDJcw9dZMFmZmSun/gNsb5GN2B8eBRNiTgaG6JoitEDsHaZcson0dXWiJ6eBOJNrQg30pOxQJvxkfPNY7bFPcRDIUa7HvRHuxCLxdGaaEYwEsZYsA1xFsY5fxMSsWYkeB6cMA3hlmbEWPP4aaWzPnomVhQTNJwY2qIempsiaKCBlKeyNolHEQwHEVt0ENqmzSFuntPAmufMRQMVKNESJ50+ZLwEVqENM0+4GDEqboI0xmIxpEMRO2I0toYE6U/QMwY9bPI3o+GY8xAOFtlvHGlptTU30svqDYgEaWltiCMaiiPRFEWgeQpmn3AmxnH98z55OWVYIW0JNOrpuRfDYLiNdPoRa4gh1BhGYo/d0Th7N0QSMcToXPxTWWfEEqzzQog2RpCiwfdXmjHj1IssyrS0RbnmMtLBDgyHutEQDaIlGkFjnA4l1oJBbzxGQp2Ik6ZYognNlFGM69FPYX10PnHO2xon38j7AOdoWHQMdjvo/Rg/ZQJ2Ou0SRoIo2mId5EEU2UAcGyiXEPvGYtQLrjuvnxZEWzBCHYkQd7uUndF70scvQ47zqz5NRCjnmXNtDc3keYJyH6XT2BgkXynXeCSEcFMTnWIzMpEWZOn4GqKNaIk1IspAMJiYjD62+aLNbPOR5gQqQdYZ0R7GgwiauNZEvIF8iqK5QboSQ0sL6+FB9+xOmi3V57edYnbrLNpBhcg9PPvG49ZmZmbRQ+agfs7ypPAyHHs/S3FIkYTw4siTVGZ6ulQF+8xayB6650bK8yh1MVDqFlD8SdD6lbbRX9B45FWzWUYE7bBwsb6xDfRC9CRCk6eh0EDy9N45Rpeu487BtKM+iFxrOwniTJk8Zp16DkY6FmDi0ccRj1KMAho7JwFUcjp+lAolZEsR7HnIhzDl+IswROUscH3BEgU3aw7758yTMLiRNnlyzkfa2ubshUpa7xJVkEyX0X3KxZh7yAFIl0knhVvK0VsGW9Fz/LloP/FMZqcNti2Zk/IfeAImHH40xk2cWf2JqSKHD3lGtfyeB2DyN+7GGOfSC5IhPXwljV6MNMTovT0Pz9/3O0SowAUqZ2GsiO7jLkXP4qPg2/0DDE1kjFJhpaYSCnlYzqXRQYMpZhTRmPSmRjHppIux80FHIMV01efFkc8zsWBaMeH4MzDhqGMx3CLZu1Szeadd0XngUZh1/hc4NlXVASZQqY3o/vT30HjSF5GipxXP5fTGckl073UQ1t37I+CvT2Doj7+FP6K4kMfI2AjGf+xizDr6eGSnLrLfG5VyzBT2PwnTD/swZp1+kb1dnKO8mUkh3D0FCYZdOdgyaemcvTPK6SLTKkZuRqpJHzmbvDwRmDIfuTx1j2ttOeE8TNx7ESac+VnSkrG0OZD30LPPQky96CtonLUbioUyStkSug84HE2LDkTX4hORTTGuU38DTOv0zEOlREH6rC3xKuhtYQY5ZyC7TFiI4eKIvYT469dvszblv3L6ChAWWh1PLAA4ztU+WZSXe/Hy+hfgEb/+aMKkpt3YV8bFcK2x/Mf0lIqnkpUGQhZqrBvPb4ZjEau/0tG7YT02rt5AtdGOP1MdKor+SFiJKUWGhuXNmo9QPoTYwFqEps1AplykRwnihRu+jikX/ggv33oTfPQaeoGumGcU9MdsGzjgp/GVglhz90+wYZQKQQ+pd8j01kCeR5BC0TMYHws0vWyodeZJ15hfnpDMYooVb2lD38AQNt5HhWBeXmCKGQ7FMMD0xNc3gMYMa4bGJtYZAYxQ0LGuPZC67ftYu2INU1K9wpBkzpuiN2bNQk+JUDsC+pGZ1R/M2IMBZApJLLv9v2gUOex2yseRG2FqxlQh0dyK1YNj2HTrd9C2834o5fXkiBKUFFWb6JBq5lTbKGJz/oYO9G7KYeBXP0Uwn6bDYQ3HGiywy0Kmc4xCg2sQnrOA61S9U8bw03/AuO42rPjev9K7Mp0mzjJTQZRZFzS3oUKvqz+CUGKb0q6oHmiWVR9FKFkqd2c7DS2JItPl9p0WYuD5v6L35qvQvvdiZCl6PVMpM1KuffA36Fv6IqKkryTDzXLMyuXw1ryJMOvBnB7WlPSunY/2z/SL+DMbR9G6qQ/ptk7QZyBL5cwQ58Cff4d1q9ayZtKfpWN9SG+Y/MvjCK19FX3P/5k+hFGZzmLdaBITN65CpXUyZcx6Rb/uVCqn7IZ8i1CeAeZPAr3irx1AZnZUUf7fdfx+zGoZMqkVvSMb8Niqu9nNJVSSnBjvfj6qOsQVu1J6VQAys+/8+kq0UjEy9FyH7noc+6mrLEveTb6YPTmPnnHoHVM/hZ8oqpAiw1RkMXQH6A19DKVNA6+ifeR1jqPgadUFPXNg+lBmoRqhUIprlyHFOVckmdps2GC57TC98+6nXYy+n12OmR85k/U1TVBKLtekgpC4KAf4W5l6HPBB9LTQIOnh9PDK/vQMl2p/GYTLsV+rGWdUM9Hj+5M8slbb5AYH0dnD+mD/45n+NTIvZg1Ar9g2dzdsZEE75mM+3NnNOq+EOIWYH12J6P5HYMIus7lOpn8snAv0ItrWrzz1IJZ97khEK0nOz0JUv04qZOC1dWHXY4mfCp2n4lSUinEtY8NDmDSFKciBJ2OkfwVTRjkg4vFxfVQqn56LcC0+FdeKhLweS46hu70Z0YVHItjWzEK9QCfIaVYvR5L3V6RYk739EsVUQoqhs/u8L2D4zdXomLMHClyP3ioOso5IMIVccdUpGLz1i4gTQZB1YI7zBmJMIV98HK2LT4OPEbGJXl8/CpNMNq1Yhtb5e2DcEWch9dYTCJEfenLmi5A35DXzSTqsHEJUUM/HGogpWZp80KsoelCsP+qh2kM0N7W2Mv1qx1AwwmEdKGfpNLTxwFQzSMUKkY4II3OQbRlGpUmXXI/1f1uO+LRdEGQESRcKmDT/A3h9hCbRPhkZZS6MNspiVDzIQCTtjJ74E6Sv1Fw6Hf3ahd1UKD675kF8+7HLmXN2Y9PGXlx5wo+wS9NeNkBFl35LLVXXf9vSkP0Qvv/gVViy8RFEWWANDSTxs3Me44TqKaV3ffQLPPFE4Yg1JrxCH16/4H3oaqd1c37tTKjgLic6ER5ZSfQRY2aBlq9XpFPMKeNj69i3ZG1jsR4ytBmV9cvQxBCdkTGNm4j02tfQ3DMX5UHh4DIjcYZYFoaMHHpwNsr6IBpvRnLgbfRQMbMUpHZQ8vHxCGZ6aUiMbsyfy9kUC1oWi6Q1zLw1z7n9rJEUQUcKFGbnZJTGyM7CSuL2YxO9aMv42VjRuxrjO1nv9DM9JL8G8mWEZ+yK4f4RdGbWIMJCW2+NamdOqZYMsMB+xUqMHpllKhWPNTXWNU9C+7geDK54Gd2hHHvpFY0Co5KH2KSdkVm+hAVtI6MEPWa0lU4kS31idJDFJMazxqNHVeHJcSlGvkD3ZBprkhGuFxEW4YVCBUPs10Be5Htfo9L7qRyMIi2TabxuV83Pdn+EDlGv2Vc3NGSAEmmRxuUL5FmkszZgypZrnU7DY6Re+zcEmc/HyOsKo+Qg642Gpm5k+peikfIIcO25eIulWmWm0dFsHw2AeQWdR5pzh0dXWOagV/Wz1IVQsp9aFMJYifg65mJ0uBcTGlpR3LSMvGJ6GeuCb2Qtyi3jER7aiBxrlzLlXereGVE9VU2thy/VRz7QkAPj0NDWhOzICFpy67mOiBlCiFHDzwg+TMNOT3k/Zl7wH07HZUTM03VquZse5X/j/kvxcvZpJEIehgeyOGbuSfjoojPZY+tXywVlbCy/jWt+eTUGC2Qki53hNX34ygnXYdeODzCfp7BZeMlQBHoxzvZnGPbFOG2vFlc+hdE1LIoCVFIKXw922ExvpjG0aKZUYCh3Zikj5rd2HWhkAaYKthukkMc0wE+r03tGqiGC9BYlhmSreRipSsRv7/aT8fZHE+gTSmE9ndGf9ZEnYaqi31XTY2mbUluvnIHtnJ6KEKInzbPQtd0pCjLMtELbniXmq0o/VZkp6pQZMe31lSJpZ1qguKto6y/m6VwiVCqpr+jnfeXZTN1k8PplkbZ09Zq9X3/iiBFXKZ6URH8jgEmbpWHaRvbomIr+DO8zffTHES4xNeF91Ynimv4iYoApToW43TYx1ZnCt9czSK+29LWdrBRXBuej4pU9Ohrt/PFfNNfHlCZBo43qVW/epxens/LYz5yj5uBZgTwNMuLor9pwBVyjtpM5H5VW9YeP/NBvLFSnKtUrMmrqtyOaQ/MWfXEE9SSfSmkRW9iV4nAdltaL/7ymuBnhlX5Tjuymh596ml8JRNmJ9/XzDK5Jb+oWPb0ur3cEua6CXgASK0lrKEq5Ui4VFpeUt9/Pe+UWRnJZfJHrUPTwMfll7bLoSJ63kW+KKcRTYeVUJtMNGfms7y/fdSbWlN9iRKggw1QlPwwsmLkvpjXNQYThdTDTh1fW/xUr+95AooMekWFirHcEFxx4KQ6deZoRrv1vWaDSL6U49r6SBGlPJ4P03G4njL3MI6lXDZwYZCJkiDt9B+i+KUP1W7D1uUBjqwFuu/A/GfN/Cu+m+d1rlBuoXb+bPsG7efU/ha3xaB7Bu+f6R0BJt2RYg3evT+fbWsf/S5A2KtEW7Git4okOPVeT7uqNZ1+hrOBGkcjDKB+ipemnkTc/fi0eeu1uRjB6d3rDfEG71PQKDIcer0MsljVblilEuT+Iz3/sauzWtD8LpyIDl94WotcjFfLC+gEWs0RaO+sAhlmPHkS73yV6nyBzFnt2wH5Wo9iLbCrOZfEunLu/jiJRSHXkryla9aW30ErlcXRS2+PWpTykfseifpbesU1+Qn+VUfe1TWB46fX0ark9bGO+La9n29ba2eAow8X75j2JzzwgadbzFmMnPZrA/o4tvZFAuaxo1HrUX7gUkcQ/PbRjiLGQL0+udlckck10kTlFLOHj4fxadYzQqJHeUbWM1qm/LiIP7f6WlkAqT3koPVIuWAWRqkTa5GENolt9SRFvOkMRsZQJL4idaIinGoHsDz7zv96jE9T4qgWKcvc3fMlx473wsVjnp+4pQtpbFrZOIuE4o5J4JQs16K6LTrVPB6ZvvFCvIj/d79mFg72MfodP+wTiuB4+aldPt0SqHL7AZRK8NoGQoxymWwquQm63+cHg6y6Ix73/wYPxQ6+oVHvrS+yhcrEwXFdZhjse/TGWvPYkEOeMUYZAGocek5RYIDeGW3HQHkfh5Hmnc6ReCiua0gtflae2OOcXSRIbqYcmUCv6tSwTME/Z2XlPtbNBQlAvW4xbSbUbgcLjtXA4Za71d/O6OWlQjFRSbjG35uWEQ4eUzx50GmslUMU35rm8dhicQUpBnQAdVlEiGhwFDoujyajlp/ptOTOjEJ3VeYSpKh6HTD0oGcPOa6UbNXxurJo1VqPUQkfGa51LnUSF6KitWlzUH3JTgqdVag3CotU4wxOmWpwqWQ/HOX6Sl8ZO9tZGilNrx2Oltupb7WkydG+A88qsT1i5SqEXOMK5HtHI21XnJXB80R6Rdu+ckasOdHf0rfui1HFflBpuo1uItfrqWqggVmvaGKXIpJu06GGm+CqoZnA8uAIapebT+BrHnITVxfUXX6ovSHGdovA9oYy3ki9jdf9ypLMptMXHYcbk2WjHFN7jNI7aOtThnw7e00C0yWWWJQOomZwBh8mLmMthD4XPOtThnwzeofLbAuWNlioqECnXVMVvO1E8ZTqmv7xRhzr8s8I/kGIpQ+OnrIRBQr03F3u1tJEgNPUoUod/NngP97/Fdmq6r+/NrVvZQ9046vDPCO8RQbR15ip8A+upSFLbaZCN1FOsOvzzwj+4i1WHOvz/CXX3X4c67ADqBlKHOuwA6gZShzrsAOoGUoc67ADqBlKHOuwA6gZShzrsAOoGUoc6bBeA/wXKiseKsnLpjgAAAABJRU5ErkJggg==\"}";
            // Theme NV => "{\"themeHeaderColor\": \"#ffffff\",\"themeHeaderLogo\": \"/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABAExAAIAAAARAAAAPlEQAAEAAAABAQAAAFERAAQAAAABAAAAAFESAAQAAAABAAAAAAAAAABBZG9iZSBJbWFnZVJlYWR5AAD / 2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz / 2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz / wAARCAAxAMgDASIAAhEBAxEB / 8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL / 8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4 + Tl5ufo6erx8vP09fb3 + Pn6 / 8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL / 8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3 + Pn6 / 9oADAMBAAIRAxEAPwD9 / Kr6tq9roOmXF7fXVvZWdqhlmnnkEcUKDkszNgKB6nivGP2xf25PD / 7IsWk2N5azap4k8RxzPpViJBBFKIigdnlOcAeYuFUMx9AMsPz++M37R / j79qrxJb2usXl1fLcTqtjoemxOtsJCcIEhXLSSZJwzl35IBA4r7Ph3grF5nFYibVOj / M93bey / V2Xqfm / F / iVl + STeEpp1cRp7i0SvquZ + d9km / JXufX / xj / 4Kw + FfBniWGw8J6TceL7eGXbe33n / ZLfb3EBZS0jDnkqqHAwzA5HtnwD / ap8F / tIaaZPDeqD7dEm + 40y6AhvrUcctHk7l5A3oWTJxuzXwVp3 / BOLxxBo9veeI9a8D + CzdD91bazq + y4P1CIyfk5I7gVzvxW / ZD + In7OFla + KW + z3Wl2rrLDr / h2 / aWO0cnCtvASWPnjftC5IG7JGfrq3CnDmIpxwuDrqNXZS5r8z7dIvy5bfM / P8Nx5xjg608bmOFcqG7hy8rgu63kvPnTXofq5RX5 +/ s//wDBWPUPAFpHY/E6NtX0mEbf7ZtkVbyAD/nrGMLN25Ta+AeJGNfeXibxVpngvw5eaxrGoWWk6Tp8Rnury8mWCC2jHJd3YhVUdySBX51nXDuNyusqWJj8XwtaqVrbdeq0aTP2Hhvi7Lc8w8sRgpfDbmUtHG99+nR6ptaPXRmhRXP/AA4+K/hf4w6FJqnhLxJoXijTYZ2tpLrSb+K8hjlUKxjLxswDBWU7Sc4YHoRS+B/ir4Y+JsupR+G/EWh6++i3Js9QXTr6K6NjOM5il2MdjjB+VsHivHlRqRvzRfu76bevY+hjiaUlFxknzbarX07/ACN+iijNZmwUUUUAFFFcD8Nf2oPAfxf+Jnivwd4b8RW+qeJPBMog1qzSCWM2b7mQgO6BJMMrK3lswUjBwSK1p0ak4ynCLajq2lole1321016mNTE0ac406kkpSdoptJtpXsl1dtdOmp31FFNnnS2heSRlSONSzMTgKBySayNh1Fc/wDDf4s+FvjJoMmqeEfEmg+KNMhmNtJd6TfxXkKShVYxl42YBgrodpOcMp7iuY8dftffDD4caX4ou9W8deHI18FIr67BbXa3l1pYaRYlEsEO+VWMjqgXbkswGM10Rwlec/ZRg3LtZ310Wnm9DlqY7DU6ftp1IqNm7tpKyV273tZJXfkej0Vi/Dr4haN8WfAmk+JvDt9HqWh65ape2N0isgmicZU7WAZT6qwDA5BAIIrarGcZQk4yVmtGn0OinUjOKnB3T1TWqaezTCiiipKCiiodQv4tKsJ7q4by4LeNpZGwW2qoyTgcngdqN9EDaSuyaiuG/Z9/aR8F/tTeBZPE3gPWG1zRI7p7Jrk2VxaYmRVZl2Toj8B15xjnrwaK0rUalKbp1YuMlumrNeqZjh8RSr01WoSUoy1TTTTXk1oz87v+DicZ8c/Cn/rw1P8A9GWtcv8A8E7PiHJ+zt+xV8U/jzqirr2reHbyLwx4ciuAGks55RAJJdx+8P8AS4Tyc7IZFDAOa6j/AIOJj/xXXwp/68NT/wDRlrXnv/BOuHT/ANqH9i74ufs7LqFlpfi/WrtPFPhs3L7V1CaIW5eEHoMG0jDdWCTuwVhG2P2/Lf8AklMPz/w+Zc/+D2r5vO1t7dL9D+Yc4/5L/F+zt7Xkfsr/APP32K5LX0vf4b/at1se+eCf2ZvAPxh+G3w9+JfxC+IXjA33xTeK2VWijluLi9mbESh/Lfy4lAYEMpQZUBkAAMX7Npuv2af+CiV78Elu5Nd8M6959lqFtPGPs1xE+nNeRu0X3TIECxuQACpftgD528c/tQeKvh/4b+B/wv8AFnwt1vwrqnwZ1uHUTPfXTf8AE5NvvJSNPJChXPSRJJFI5BYEGvbPjB+3z8NfCfxc8SfETwP4I8XD4t6/aR2YvPE0ccFroI8iOLfHbq7kymNI+CMEE4cKzK2k8LmdSnOhNOpCrGooJODjF8y9lJNbJRd73b0SSukjbB4jJadWliqbVKrQnSc21UU5rkbrRalfmcpq1rJatt2bb+R/j9oS+Evid410OO4kurXw/q2o6ZbSO27dFBPJEp9OVQHPev1+/wCCi3/JhnxW/wCxZu//AEXX4va5cS3lnezTyyTzzLJJLLI255XOSzMe5JJJPcmv3I/au+E+pfHb9mPxr4N0eaxt9U8SaPNYWsl7I0dukjrgF2VWYL6kKT7VzeIUlRr5dKrL4ZSu/R07s9Xwwp+3wma08PH4oRUYrzVWy/Q/Ob9gLxzff8E9Piv8NJtVupj8Kv2hdBspjc3DKI9L1ZVVGYn+ECV8E/KPKuYyxYwGu9/Yx+Pa/sqfBX9rbx1NZ/bJvDnjm7aG1clVluXmaGFHPUKZZEDEcgZxzXvPjb/gnlN8Vf8Agmtofwb8QXGk/wDCVeG9Jg/s7UIJJHtLbUoFYI4Zow/lMGaNz5e7ZK+BnFcX+yZ/wS88TeDf2W/i38O/ihrmi6hcfEy7F2NQ0m6nvHhlCBlnkM0URaRZ1WTHIbHJ5rwcVnGWYqlWrYiS5pThGSX24QqJqS8+TR+ifU9nA8O53gK2Hw2Eg+SNOpOEna1OpOk06b8vae8uiUmuh5fr/wC1B+1T8GP2bfDf7QmueLvCfiHwlrU1vdXPg8aRDbrbWdy2ISs6IJctuTGZGKF1LeZhhVb/AIKEeMvHnxm/bQ/Z1n8M+JtJ0vR/E6waz4JMmnF30yWZIC1xeKxZZpCSNoUBVQYxkszdHqf/AATo/aS+JXwR8P8AwS8UeNfhxb/C3QLqLOqWCXMmr3VpExMMRRkEZ2fLtXK4Krl328+qfts/sFeMvG3jH4OeLPhBeeGbHVvhAotrTTteeUWs0Efk+SA0asTt8sqwJUsrZDKy86U8wyyji4STpKT9qk4xSiouP7vm0te+jdm7NqW5liMpzrE5fVpuNdwXsJOM5tzc4zvV5Pevy2s0rpcyTgtDQ/af8d/EX4D/ALNfgu11749fD7wL4ykvJV1rxFqOkxMdSgHmNmytCpDSJugVl2YwSxZTgN5//wAE0v27PGPxk/ar8ZfDLXvHWj/FbQdM0htZ0nxXaaSulvcBJLZGjMKKi7c3JByuQ0RwzKwIm/aO/YY+Ofxo8ZfCX4nSX/wr1L4leB0mTVNKu1uv+EflP2qSW3aFShkbajqH3bW3RqysSBXY/su/sTfE74eft06/8ZvH/iLwbrN14v8AC7afqMGjR3FutleGa1CQwRyKd9ukFqgEruJGYnKdWPl82WRyypGrKnKpKMmrKKanz6KNoc1rbPmUbaKPb3FHO55zRnQhVhRjKCfNKTUqfs9XK8+Xm5mlJcjndNuffxn9hT9o/wDam/bj0uyvNO8UeFdF8N+E/EccWv6tc2MK3mtwlo5JLOGNYHjXy4SfmxGxMyfPwcdb+zx+1ZfeDP2of2utU1ix0WbSfh7DLqax2Gk2ljeX62xuSqTXEcYkmbamxWlZsbvc16t/wS2/Y28VfsT/AAX8ReHfF174fvr7V/EEmqwvpFxNNCsTW8EQDGWKMht0TcAEYI57DD+C3/BPTWtC+PX7ROreMLzRLnwh8aop7OCHTbqU30NvM04fzA8Koj7JeCrPhh7VeMzDK518XTjGCp2iocsUnL34uVnu3ZO3ZaKyMstyfPKeEy+rOdSVe8nU55NqL9nOMbrZK7jfe71d3dngNn+1H+1Zq/7Js37SUPjfwZH4Whu3nXwb/YkZie0S7Nq/70p5/EgY7fODFBuD7iEr9Av2evjFa/tBfA3wp42s7drOHxNpkN+bYyeYbV3UF4t2Bu2NuXdgZ25wM18Or/wTi/aS0z9n2f4C2fjf4ZyfCia+Mi6u8FzHrCWjXX2hovKClOZMyGPecklPOCHA+7Pgr8KdO+Bfwj8N+DdJaaTTvDOnQ6dBJLjzJhGgUyPgAbmILHAAyxwBXBxPUy+VJfVuRy55cvs1a1Ky5VLRXlfvd73Z6nBNDN6eIf1z2qh7KHP7WXNetd87hq7Qt2tHayPhv9jrxpY/8E//ANrz9pH4c6tvt/Cen2M3xB0aKNdsUVpFH5skcSnlm8maKPj/AJ829K8/+AOna58O/wDglH8bvjffwaXceLvipq39pSm+sYb61uLcagsJElvMrRsGmmvThgQVdD2Br3z/AIKg/wDBODxl+154+8N+Kfh5rHh7RNbtNIu9B1d9Wu54Eu7OQkxIvlQy5x5tyGDAZEi45FepftG/seXXjT/gn3efBjwXcaba3EOkWGk6fPqTvDAVtpYGLytGjtuZYmJIU5ZuepNer/bWClGhWclz1p0va36Km7O/lJ2l52PD/wBW8zhPFYaMGqeGhX+rtfalWV4qPW8E5QXa+jtofOfxy/bg+IvwB/ZG/Zd17woNNmvfFsFpHqWlQ6fb29vqiCCAraxqqBbdW3FR5QULkYGBiqv7cGi/tJeAv+CefxE1D4k+PPB2of2lfWy3llpNjt+z6dPNDAbOGQRREZkl+cyeaWjXaHyST3nxr/4Jx+PPiP8AAX9nXwvYal4Rj1H4Ry2smsvcXlwsNwIlgDfZysBZ8+W2N6p1HTt77+3V+znfftY/sp+LfAOm6ha6XqWtxQPaXF0rGBZYLmK4RX2gsFYxBCwDFQ2QrY2nCGaZfQrYV04w/iyc5cqbUVUvHW2nu9tbW7I6p5Fm2Jw+OVadXSjGNKKm1FzdG0tL6vnSWrte76tny54T8Y/Gj9lz/glPqfji68d6PqSw+FPDk/gyG30aFH0G3cwo8Uu5MTMYZYk3NnBjJ6mtz9qf9s34i/DP/glP8PfidouuQ2vjTXoNJe+vmsIJFlM8DPLiJkMa5YA8KMdsVveA/wBj/wCMvxK/Yo8VfB34p6p4BsdPj8P6fofha40D7RLLE1pkrLeGRVDAmK1H7vGQsnCkjPkHjn/gmv8AtMfGn9lTR/hT4k8XfCuz0HwP5R0SO1N40uqvGfKjF1P5PyRxQSS7CkRZmVA65/eL1UJ5ZVxHPip0rxrc0nZWlTcVZK0bPX4l3bb6s4sTRzuhhPZYGnXtLDckVzO8aqnJtu8rxfK1yv8AlSitkj3f9tP47+IvBXws+Hd5YfG/wT8HpdXthcatd6tpsWoX18phjYNa2zBgwVywcbQP3i/MuNrec/8ABPT9ubxZ8fPFXxa+H/ibxXo/xGtPDelSaho/iux00acdQgOUYPCqoo+/GQAgKkSAtICrDa/aZ/YH+J+s/tMfC/4rfD65+H2p614J8PQaJPpfij7Q1issSzhZ08tdzqPtLsB8jI8SMN2SF1v2XP2EfiV8LP2mvid8QvGniLwjrlx8RtB+zyNpqz2/2a9fy2eIRMhAt49pjRy7OyorMoYkDjhLKoZXKDlCU3G60ipKXPtpHm0j1c7NaKPU76kc+nncKkYVI01Lll70nCUPZ/FZz5FeXRU+ZPVy1s8X/ggP/wAmNXn/AGM95/6ItqK9Q/4JgfskeJv2Lf2brjwf4svNBvtUk1mfUFk0meWa38t44VALSRRtuzG2RtxjHPoV83xNiKdfNK9ai+aMpNprqj7LgvB1sJkeFw2Ii4zjBJp7pnZftY/sT/D/APbP8KW+m+NNMme608P/AGfqllL5F/pxfG7y3wQVOBlHVkJAJUkAj8nv2t/+CW3xU/Yh1b/hK9DnvPEnhnSZhd23iLRN8F7pJT5xLNEhMkBTaT5qMyLgEuhIWv27oru4f4vx2Vfu4PnpdYS287dvy7pnl8XeHmV59++qL2ddbVI6S025v5kvPVdGj8SPhL/wWB+P2mT2ulzeMrbV7fYUSW/0q2lmUKhx+8VFLnjOX3E9yayzD8QP2zvjZeXMNnfeMPGmuGOS5NpaxQjaiJCjybAkUUaqqLvfavAySTk/o/8AH3/gj58Jvjb8SbPxVY2974K1LzzJqSaEI4bfU1ZSGzEylI5D18yMDJLFg7EMPoD4N/Azwl+z/wCEU0PwfodlomnqdziFS0ty/TfLIxLyvjjc7E4wM4AFfcS48yfCUfb5ZhVGtJWfuqKXq1q1fWyte2tmfneD8M+IcXVeEzvGudCDunzSk5aaWjL4WldXd7X05kfJv7J3/BHPQvBYtta+KU1t4o1VcSJokGf7Ltj1xKSA1yemQQsfLAq4w1fW/wAY20VfhrqS+ItGs/EGiyrHDdaddW8dxDcq8iKFaOQFGAJBwR2rp6yPHng6H4geE7zSLi4urSK8C5mttnmxFWDgrvVlyCo+8pHtX5zjc8xWYYuGIx1RuzW2iirq/Klt8tX1bZ+vZfw7g8qwM8NllNJuL31cnZ25m99ej0V9EkeZ6n+yt8B9G8QWOmXXwn+GsN1qJ225bwda+TI2GOzzfI8sOQrEKWDHBwDVDU/2fv2dtG1qbT7r4a/CyG4t3SKTd4StPLV2KAJ5nk7N482NiucqrhiAvNdld/s+2mreONL8QajrutalqGkmJomuI7TkxvMy42wAx584q3llN4jjzyCWLv8AZu8O3/iHX76eITL4jinjuYmtbfennxCKUpP5fnruXdx5hALtjAwBtHMEmufE1HprZve/S/S3f71sctTLqrT9nhKS97S6j8Nt3brfs/k9zibr4Efs5WeppZv8N/hV5zTm2bb4StHSF/OMAEjiErGDMrRBnIDSKyAllIFfwr8Dv2f9e0KO6uvhT8K9NnZ4Ee3fwxYu0ZuLuS0t8kQ/8tJo2Ue/X1ruV/Zs0uKKOOPVtcVJ7O3sdV+eAnXEinluN058rh5JZ7hnaLy93nv0whVh/Zn0sX9tLHrOvRwxPYyTW4a3KXbWd/JfwbyYiwxNK4OwruXAPTJ1/tCny2WIqXuteaW3X5vpvstVdpYf2bieZSeFpWs9OWO7217J2vtvKydot8T/AMKS/Z3dYriP4X/C3+zHWeRtQl8K2UNr5cMZkeVJHhCyxhQTvjLKAM5qrD8Kf2c7XR/tWqfDD4XaXMrXBltZfCNsbi0SEoztNH9nDRbYpYZX3ABElDbinzn0Ifs1aZJ4P0/Qp9Y16407RrV7HTVZ4FeyhMQiRQwiBYxgKVZ9xyi7i3zbpv8AhnfTrm61W6vNW1u+vtasr6yvLiRoVaRbuK0idgqxBVZI7KEJgY+8WDls01mVK9nXqW12k79beWq300skr6sTyvFWvHDUr6bxjbpfZ30d7a6ptu1kjgfi78DPgF8IvDd9eXfwl+GNxeW2n3GoQ2cfhSzLTrEufmZYGEasxVA74Xc6jJJApnin4JfADQP3dv8ACb4X310NStNPMQ8LWcauZr6CykeNzBtl8mSdRIEJ2NhG2sRXpPj34FWfju3uIf7X1nS49Q0htDvxZmD/AE21O7areZG+1l3yYZNvEr5z8u2jB+zF4ft9WvrxHmSS81RNW3pbWqzRyjUItQZPOEPmtG88S5VnPy8AjCFVRzKn7OLqV6nNfX3parTTy67PXunoPEZXiPa1FSw1LltaPux0dnr59N0rNWSavI85174S/Abw94t13Rp/gr8PvtWjRwyo3/CK6f5d8HMIkEZ8vrF9ogLBscSrjPOLHiD4Ufs3+Hbe6kf4X/Di6a0nS3aO28F20ryk3KWrNEBB++WOaRUkMe7YxCnDEKfTvGPwH0fxvLeS3VxqMc93qdrqySwyIr2s0EaxAR5U/K8Ssjhs5WVwCMjGbcfs0abcRbG1rXdtnE8OljdBjSEa7hu8RjyvnxLbQAed5mFiC93LOnmdKSg51qienN70uyvbfrf8X0Sc1spxUZVFTw9JrXlbjHTV2vt0t32SdrykuR1n4Dfs6+HDeDUfhj8M9PNj5W/7T4Mt4fM8yRIk8vdAPNzJJGh8vdhnVTgkA11+Cv7O9vDG178L/hbZtNcXEADeFLOQRrDcNbmSUrCRCnmLt3SbQGO0nIIrsX/ZZ0W48UzavPqeszXU16t8Wb7PuLLqFtqCo0nleY6LJaxxqHY7Icou3gizqH7N2lXsWpQpqmtW9rrq3MGrQo0JXUree7numgctGSqq11cIDGVfZMwLFgjqLMKSik8RUvfX3ntb/P16LTVjeW4pybWEpJW0XLFu9+vyt1XV6u0Sp/wxJ8GP+iRfDD/wlrH/AONUf8MSfBj/AKJF8MP/AAlrH/41Xp9FeN/amN/5/T/8Cf8AmfR/2PgP+fEP/AY/5HmH/DEnwY/6JF8MP/CWsf8A41R/wxJ8GP8AokXww/8ACWsf/jVen0Uf2pjf+f0//An/AJh/Y+A/58Q/8Bj/AJHmH/DEnwY/6JF8MP8AwlrH/wCNUf8ADEnwY/6JF8MP/CWsf/jVen0Uf2pjf+f0/wDwJ/5h/Y+A/wCfEP8AwGP+Rg/D34W+GfhHosmm+FPDmg+GNOmnNzJa6Tp8VlDJKVVTIUjVVLFVUFiM4UDsKK3qK46lSU5Oc2231erO2nThTioU0klslogoooqDQKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//Z\"}";
            
        }

        private static string GetJsonForm(int companyidtx, string formid)
        {
            string retJson = null;
            BpCompanyForm bpCompanyForm;

            try
            {
                LOG.Debug("BPUnitOfWork.GetJsonForm IN...");
                int ret = AdminBpCompanyForm.Retrieve(companyidtx, formid, out bpCompanyForm);
                LOG.Debug("BPUnitOfWork.GetJsonForm AdminBpCompanyForm.Retrieve ret = " + ret.ToString());
                if (ret == 0 && bpCompanyForm != null && !string.IsNullOrEmpty(bpCompanyForm.JsonSchema))
                {
                    retJson = bpCompanyForm.JsonSchema;
                    //retJson = retJson.Replace("\r\n", "");
                    //retJson = retJson.Replace("\\", "");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BPUnitOfWork.GetJsonForm Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.GetJsonForm OUT! retJson = " + (string.IsNullOrEmpty(retJson)?"NULL":retJson.Substring(0,50)));
            return retJson;
        }

        ///   199 - Front Cedula Cropped (Inventado por mi este codigo)
        ///   200 - Back Cedula Cropped (Inventado por mi este codigo)
        ///   201 - Photo
        ///   202 - Fingerprint
        ///   204 - Signature
        ///   205 - Barcode
        ///   210 - Ghost Photo
        private static string GetFoto(List<Biometrika.Regula.WebAPI.Api._2021.DOCInfoImage> docImages, int v)
        {
            foreach (Biometrika.Regula.WebAPI.Api._2021.DOCInfoImage item in docImages)
            {
                if (item.code.Equals(v.ToString())) return item.b64image;
            }
            return null;
        }

        /// <summary>
        /// Genera un TrickId y revisa que no este duplicado
        /// </summary>
        /// <returns></returns>
        internal static string GetTrackIdUnique()
        {
            return AdminBpTx.GetTrackIdUnique();
        }

        /// <summary>
        /// Dado un string de un sample desde BPWeb parsea para convertirlo en ImageSample
        /// </summary>
        /// <param name="_code"></param>
        /// <param name="_sSample"></param>
        /// <returns></returns>
        internal static ImageSample ConvertStringToImageSample(string _code, string _sSample)
        {
            ImageSample ret = new ImageSample();
            try
            {
                LOG.Debug("BPUnitOfWork.ConvertStringToImageSample IN...");
                if (string.IsNullOrEmpty(_sSample))
                {
                    LOG.Debug("BPUnitOfWork.ConvertStringToImageSample - Param In nulo => Sale!");
                    return null;
                }

                string[] arrAux1 = _sSample.Split(',');
                if (arrAux1!= null)
                {
                    if (arrAux1.Length != 2)
                    {
                        LOG.Debug("BPUnitOfWork.ConvertStringToImageSample - Split 1 con len=" + arrAux1.Length.ToString() +
                                    " != 2  => Sale!");
                        return null;
                    }
                } else
                {
                    LOG.Debug("BPUnitOfWork.ConvertStringToImageSample - Split 1 con return Null => Sale!");
                    return null;
                }

                LOG.Debug("BPUnitOfWork.ConvertStringToImageSample - Set Code = " + _code);
                ret.code = _code;
                LOG.Debug("BPUnitOfWork.ConvertStringToImageSample - Set Data = " + arrAux1[1].Substring(0,10) + "...");
                ret.data = arrAux1[1];

                string[] arrAux2 = _sSample.Split(';')[0].Split(':');

                if (arrAux2[1].Contains("pdf"))
                {
                    LOG.Debug("BPUnitOfWork.ConvertStringToImageSample - Set typeimage = pdf y typeimageformat = pdf...");
                    ret.typeimage = TypeImage.pdf;
                    ret.typeimageformat = TypeImageFormat.pdf;
                }
                else if (arrAux2[1].Contains("jpg") || arrAux2[1].Contains("jpeg"))
                {
                    LOG.Debug("BPUnitOfWork.ConvertStringToImageSample - Set typeimage = image y typeimageformat = jpg...");
                    ret.typeimage = TypeImage.image;
                    ret.typeimageformat = TypeImageFormat.jpg;
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("BPUnitOfWork.ConvertStringToImageSample Error: " + ex.Message);
            }
            LOG.Debug("BPUnitOfWork.ConvertStringToImageSample OUT!");
            return ret;
        }

        #endregion Global

#region POSController

        internal static int GenerateTx(BpCompany company, POSTxCreateIn param, out string trackid, out string msg)
        {
            int ret = Errors.IERR_OK;
            trackid = null;
            msg = null;
            try
            {
                LOG.Debug("BPUnitOfWork.GenerateTx IN...");

                trackid = AdminBpTx.GetTrackIdUnique();
                LOG.Debug("BPUnitOfWork.GenerateTx - Ingresando a AdminBpTx.CreateTx para crear tx con Trackid = " + trackid + "...");
                ret = AdminBpTx.CreateTx(Bio.Core.Api.Constant.Action.ACTION_VERIFYANDGET, 
                                         (param.type==1?Errors.IERR_CREATING_PENDING_CN: Errors.IERR_CREATING_PENDING_CA), 
                                         company.Id, "RUT", "NA", null, trackid, 0, param.authenticationfactor, param.minutiaetype, 0,
                                         param.clientid, out msg);
                LOG.Debug("BPUnitOfWork.GenerateTx - AdminBpTx.CreateTx ret = " + ret.ToString());
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                trackid = null;
                msg = "Generate Tx Excp Error Desconocido [" + ex.Message + "]";
                LOG.Error("BPUnitOfWork.GenerateTx Excp Error:", ex);
            }
            LOG.Debug("BPUnitOfWork.GenerateTx OUT => ret = " + ret.ToString());
            return ret;
        }

        internal static BPResponse Verify(POSVerifyIn param, BpCompany company)
        {
            /*
                0.-Recupero BpTx para saber como procesar
                1.-Segun sea:
                    1.1.- Cedula nueva => Actualizo BpTx y retorno
                    1.2.- Cedula Antigua:
                        1.2.1.- Realizo verify entre PDF417 y Huella enviada
                            1.2.1.1.- Update en BpTx
                            1.2.1.2.- Armo salida con datos mas parseo de datos desde PDF417
                2.- Armo BPResonde y Retorno
            */
            BPResponse _response = new BPResponse();
            int ret = Errors.IERR_OK;
            string msg;
            string msgaux;
            bool isVerifyPositive = false;
            try
            {
                LOG.Debug("BPUnitOfWork.Verify IN...");

                LOG.Debug("BPUnitOfWork.Verify - Resucperando Tx con Trackid = " + param.trackid);
                BpTx _TX = AdminBpTx.RetrieveTxByTrackid(param.trackid, out msg, true);
                if (_TX == null)
                {
                    LOG.Warn("BPUnitOfWork.Verify - Error recuperando Trackid [" + msg + "]");
                    _response.Code = Errors.IERR_TX_NOT_EXIST;
                    _response.Message = Errors.GetDescription(_response.Code) + "[" + msg + "]";
                    return _response;
                } else
                {
                    LOG.Debug("BPUnitOfWork.Verify - BpTx recuperada OK!");
                }

                if (isInvalidParam(param, _TX))
                {
                    LOG.Warn("BPUnitOfWork.Verify - Error verificando parametros para la TX");
                    _response.Code = Errors.IERR_BAD_PARAMETER;
                    _response.Message = Errors.GetDescription(_response.Code) + 
                        " [Los parametros enviados no son correctos para el tipod e transaccion (operationcode=" + 
                        _TX.Operationcode.ToString() + ")]";
                    return _response;
                } else
                {
                    LOG.Debug("BPUnitOfWork.Verify -Parametros correctos para el tipod e TX!");
                }

                if (_TX.Operationcode == Errors.IERR_CREATING_PENDING_CN) //Es tranbsaccion de cedula nueva
                {
                    LOG.Debug("BPUnitOfWork.Verify - Actualizando Tx de cedula nueva con trackid = " + param.trackid);
                   
                    _TX.Clientid = param.clientid;
                    _TX.Result = param.resultmoc == 1 ? 1 : 2;  //Si es verify erronea => viene 0 => pongo 2 para ser coherente con BioPortal
                    _TX.Score = _TX.Result == 1 ? 100 : 0;
                    _TX.Bodypart = param.bodypart;
                    _TX.Valueid = param.perdonaldata.valueid;
                    DynamicData dd = new DynamicData();
                    dd.AddValue("QR", param.barcode);
                    BioPortal.Server.Api.Model.PersonalData pd = param.perdonaldata;
                    pd.photo = null;
                    pd.signature = null;
                    dd.AddValue("PersonaData", Bio.Core.Utils.Utils.ToStringFromProperty(pd, 1));
                    _TX.Dynamicdata = XmlUtils.SerializeAnObject(dd);
                    //Dice que se completo ok con valor 800 para diferenciar que es verify con cedula nueva
                    _TX.Operationcode = Errors.IERR_CREATING_PENDING_CN * -1; 
                    if (AdminBpTx.Update(_TX, out msg))
                    {
                        _response.Code = Errors.IERR_OK;
                        _response.Message = Errors.GetDescription(_response.Code);
                        isVerifyPositive = true;
                        LOG.Debug("BPUnitOfWork.Verify - Updated TX OK!");
                    }
                    else
                    {
                        _response.Code = Errors.IERR_DATABASE;
                        _response.Message = Errors.GetDescription(_response.Code) + "[" + msg + "]";
                        LOG.Error("BPUnitOfWork.Verify - Error grabando TX => " + _response.Message);
                    }
                } else if (_TX.Operationcode == Errors.IERR_CREATING_PENDING_CA) //Es cedula antigua
                {
                    bool isParsedPDF417 = false;
                    //Realizo verificación
                    PDF417 _PDF417;
                    ret = ServicesManager.ParsePDF417(param.barcode, out _PDF417);
                    if (ret != 0)
                    {
                        _response.Code = Errors.IERR_DESERIALIZING_DATA;
                        _response.Message = Errors.GetDescription(_response.Code) + " [Parseando PDF417 por datos]";
                        LOG.Error("BPUnitOfWork.Verify - Error parseando PDF417 => " + _response.Message);
                    }
                    else
                    {
                        LOG.Error("BPUnitOfWork.Verify - PDF417 parseado OK! valueid = " + _PDF417.valueid +
                                  " - Lastname=" + _PDF417.lastname);
                        isParsedPDF417 = true;
                    }

                    if (isParsedPDF417)
                    {
                        LOG.Error("BPUnitOfWork.Verify - iniciando Verify...");
                        BioPortal.Server.Api.XmlParamIn pin = new BioPortal.Server.Api.XmlParamIn();
                        pin.Actionid = Bio.Core.Api.Constant.Action.ACTION_VERIFY;  //Valor 1
                        pin.Additionaldata = null;
                        pin.Authenticationfactor = Bio.Core.Api.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;  //Valor 2
                        pin.Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_NEC; //Valor 1
                        pin.Bodypart = param.bodypart; //Valor 1
                        pin.Clientid = param.clientid;
                        pin.Companyid = company.Id;
                        pin.Enduser = param.clientid;
                        pin.Ipenduser = "127.0.0.1";
                        pin.Matchingtype = 1;
                        BpOrigin orig = AdminBpOrigin.Retrieve(61, out msgaux);
                        pin.Origin = (orig != null ? 61 : 1);  //Luego en produccion les inidcamos un valor 

                        //Primero agrego la muestra
                        pin.SampleCollection = new List<Bio.Core.Api.Sample>();
                        Bio.Core.Api.Sample sample = new Bio.Core.Api.Sample();
                        sample.Data = param.sample;
                        sample.Minutiaetype = param.minutiaetype; // Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_WSQ;  //Valor 21 o RAW Valor 22
                        sample.Additionaldata = null;
                        pin.SampleCollection.Add(sample);

                        //Segundo agrego PDF417
                        Bio.Core.Api.Sample sample1 = new Bio.Core.Api.Sample();
                        sample1.Data = param.barcode;
                        sample1.Minutiaetype = Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_PDF417CEDULA;  //Valor 25
                        sample1.Additionaldata = null;
                        pin.SampleCollection.Add(sample1);

                        pin.SaveVerified = 2;
                        pin.Threshold = param.threshold;
                        if (pin.Threshold < 1000)
                        {
                            pin.Threshold = 1000;
                            param.threshold = 1000;
                        }
                        pin.Userid = 0;
                        pin.Verifybyconnectorid = "DMANec";
                        pin.InsertOption = 2;
                        pin.PersonalData = new Bio.Core.Api.PersonalData();
                        pin.PersonalData.Typeid = "RUT";
                        pin.PersonalData.Valueid = _PDF417.valueid;
                        pin.OperationOrder = Bio.Core.Api.Constant.OperationOrder.OPERATIONORDER_REMOTEONLY; //Valor 3

                        string xmlparamout;
                        _response.Code = ServicesManager.Verify(pin, out xmlparamout);
                        XmlParamOut oXmlParamOut = XmlUtils.DeserializeObject<XmlParamOut>(xmlparamout);
                        if (_response.Code == Errors.IERR_OK)
                        {
                            _TX.Clientid = param.clientid;
                            _TX.Result = Convert.ToInt32(oXmlParamOut.Result);
                            _TX.Score = oXmlParamOut.Score;
                            _TX.Threshold = param.threshold;
                            isVerifyPositive = (_TX.Score >= _TX.Threshold);
                            _TX.Bodypart = Convert.ToInt32(_PDF417.finger);
                            _TX.Valueid = _PDF417.valueid;
                            DynamicData dd = new DynamicData();
                            dd.AddValue("PDF417", param.barcode);
                            BioPortal.Server.Api.Model.PersonalData pd = 
                                (param.perdonaldata != null?param.perdonaldata:new BioPortal.Server.Api.Model.PersonalData());
                            pd.valueid = _PDF417.valueid;
                            pd.expirationdate = _PDF417.expirationdate;
                            pd.nationality = _PDF417.nationality;
                            pd.phaterlastname = _PDF417.lastname;
                            pd.serialnumber = _PDF417.serialnumber;
                            pd.photo = null;
                            pd.signature = null;
                            param.perdonaldata = pd; //Copio para la salida
                            dd.AddValue("PersonaData", Bio.Core.Utils.Utils.ToStringFromProperty(pd));
                            _TX.Dynamicdata = XmlUtils.SerializeAnObject(dd);
                            //Dice que se completo ok con valor 801 para diferenciar que es verify con cedula vieja
                            _TX.Operationcode = Errors.IERR_CREATING_PENDING_CA * -1;
                            if (AdminBpTx.Update(_TX, out msg))
                            {
                                _response.Code = Errors.IERR_OK;
                                _response.Message = Errors.GetDescription(_response.Code);
                                POSVerifyOutData posVOD = new POSVerifyOutData();
                                posVOD.result = _TX.Result;
                                posVOD.score = _TX.Score;
                                posVOD.perdonaldata = param.perdonaldata;
                                _response.Data = posVOD;
                                isVerifyPositive = true;
                                LOG.Debug("BPUnitOfWork.Verify - Updated TX OK!");
                            }
                            else
                            {
                                _response.Code = Errors.IERR_DATABASE;
                                _response.Message = Errors.GetDescription(_response.Code) + "[" + msg + "]";
                                LOG.Error("BPUnitOfWork.Verify - Error grabando TX => " + _response.Message);
                            }
                        }
                        else
                        {
                            _response.Code = Errors.IERR_VERIFY;
                            _response.Message = Errors.GetDescription(_response.Code);
                            LOG.Error("BPUnitOfWork.Verify - Error Verificando cedula antigua => " + _response.Message);
                        }
                    }
                } else if (_TX.Operationcode == Errors.IERR_CREATING_PENDING_CN * -1 ||
                           _TX.Operationcode == Errors.IERR_CREATING_PENDING_CA * -1)
                {
                    _response.Code = Errors.IERR_TX_COMPLETED;
                    _response.Message = Errors.GetDescription(_response.Code) + 
                        " [El trackid " + param.trackid + 
                        " enviado ya se completo con exito anteriormente (OperationCode=" +
                        _TX.Operationcode.ToString() + ")]";
                    LOG.Error("BPUnitOfWork.Verify - " + _response.Message);
                } else //El operationcode es diferente => Solo niforma pero no hace nada
                {
                    _response.Code = Errors.IERR_BAD_PARAMETER;
                    _response.Message = Errors.GetDescription(_response.Code) +
                        " [El trackid " + param.trackid +
                        " enviado no corresponde a una transaccion de verificacion de cedula pendiente (OperationCode=" +
                        _TX.Operationcode.ToString() + ")]";
                    LOG.Error("BPUnitOfWork.Verify - " + _response.Message);
                }

                if (isVerifyPositive && !string.IsNullOrEmpty(Properties.Settings.Default.POSControllerCompanyIdEnroll))
                {
                    //Chequeo si debo enrolar y enrolo con el CompanyId configurado
                    LOG.Debug("BPUnitOfWork.Verify - Ingreso a enrolar => valueid = " + param.perdonaldata.valueid +
                               " - Name = " + param.perdonaldata.phaterlastname + " - Companies=" +
                               Properties.Settings.Default.POSControllerCompanyIdEnroll);

                    //Solo enrolo y si hay error mando mail pero no corta el flujo
                    EnrollOrUpdateFromPOS(param, _TX);

                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msg = "Verify Excp Error Desconocido [" + ex.Message + "]";
                LOG.Error("BPUnitOfWork.Verify Excp Error:", ex);
            }
            LOG.Debug("BPUnitOfWork.Verify OUT => ret = " + ret.ToString());
            return _response;
        }

        private static bool isInvalidParam(POSVerifyIn param, BpTx tx)
        {
            bool ret = false;
            try
            {
                if (tx.Operationcode == Errors.IERR_CREATING_PENDING_CN)
                {
                    // Hasta 26/07/2022 => Esperaba 2 en verify negativa de MoC, pero venia 0
                    //if ((param.resultmoc != 1 && param.resultmoc != 2)  
                    if ((param.resultmoc != 1 && param.resultmoc != 0) ||
                         param.perdonaldata == null || string.IsNullOrEmpty(param.perdonaldata.valueid) ||
                         string.IsNullOrEmpty(param.sample))
                    {
                        LOG.Debug("BPUnitOfWork.isInvalidParam - tx.Operationcode = " + tx.Operationcode.ToString());
                        LOG.Debug("BPUnitOfWork.isInvalidParam - param.resultmoc = " + param.resultmoc.ToString());
                        LOG.Debug("BPUnitOfWork.isInvalidParam - (param.perdonaldata == null) = " + (param.perdonaldata == null).ToString());
                        if ((param.perdonaldata != null) && !string.IsNullOrEmpty(param.perdonaldata.valueid))
                        {
                            LOG.Debug("BPUnitOfWork.isInvalidParam - param.perdonaldata.valueid = " + param.perdonaldata.valueid);
                        }
                        LOG.Debug("BPUnitOfWork.isInvalidParam - (string.IsNullOrEmpty(param.sample)) = " + 
                                                                (string.IsNullOrEmpty(param.sample)).ToString());

                        ret = true;
                    } else
                    {

                        LOG.Debug("BPUnitOfWork.isInvalidParam - Validado resultado MoC => valueid = " + param.perdonaldata.valueid + " => " +
                                                "param.resultmoc = " + param.resultmoc.ToString());
                        if ((param.perdonaldata != null) && !string.IsNullOrEmpty(param.perdonaldata.phaterlastname))
                        {
                            LOG.Debug("BPUnitOfWork.isInvalidParam - phaterlastname = " + param.perdonaldata.phaterlastname);
                        }
                    }
                } else if (tx.Operationcode == Errors.IERR_CREATING_PENDING_CA)
                {
                    if (string.IsNullOrEmpty(param.sample) || string.IsNullOrEmpty(param.barcode))
                    {
                        LOG.Debug("BPUnitOfWork.isInvalidParam - tx.Operationcode = " + tx.Operationcode.ToString());
                        LOG.Debug("BPUnitOfWork.isInvalidParam - (string.IsNullOrEmpty(param.barcode)) = " +
                                                                (string.IsNullOrEmpty(param.barcode)).ToString());
                        LOG.Debug("BPUnitOfWork.isInvalidParam - (string.IsNullOrEmpty(param.sample)) = " +
                                                                (string.IsNullOrEmpty(param.sample)).ToString());
                        ret = true;
                    } else
                    {
                        LOG.Debug("BPUnitOfWork.isInvalidParam - Validado resultado Ced Old!");
                        if ((param.perdonaldata != null) && !string.IsNullOrEmpty(param.perdonaldata.valueid))
                        {
                            LOG.Debug("BPUnitOfWork.isInvalidParam - param.perdonaldata.valueid = " + param.perdonaldata.valueid);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("BPUnitOfWork Error Excp:", ex);
            }
            return ret;
        }

        private static void EnrollOrUpdateFromPOS(POSVerifyIn param, BpTx tx)
        {
            XmlParamIn pin;
            string msgaux;
            LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromPOS IN...");
            try
            {
                string[] strCompId = Properties.Settings.Default.POSControllerCompanyIdEnroll.Split(',');
                for (int i = 0; i < strCompId.Length; i++)
                {
                    //Add 27-07 - Chequeo que tenga el Clientid habilitado para esa 

                    pin = new XmlParamIn();
                    pin.Actionid = Api.Constant.Action.ACTION_ENROLL_FORCED;
                    pin.Additionaldata = null;
                    pin.Authenticationfactor = tx.Authenticationfactor;
                    pin.Minutiaetype = tx.Minutiaetype;
                    pin.Bodypart = tx.Bodypart;
                    pin.Clientid = param.clientid;
                    pin.Companyid = Convert.ToInt32(strCompId[i]); 
                    pin.Enduser = "POSController";
                    pin.Ipenduser = "127.0.0.1";
                    pin.Matchingtype = 1;
                    BpOrigin orig = AdminBpOrigin.Retrieve(61, out msgaux);
                    pin.Origin = (orig != null ? 61 : 1);  //Luego en produccion les inidcamos un valor 

                  
                    pin.SampleCollection = new List<Bio.Core.Api.Sample>();
                    Bio.Core.Api.Sample sample = new Bio.Core.Api.Sample();
                    sample.Data = param.sample;
                    sample.Minutiaetype = param.minutiaetype;
                    sample.Additionaldata = null;
                    pin.SampleCollection.Add(sample);
                    
                    pin.SaveVerified = 1;
                    pin.Threshold = 0;
                    pin.Userid = 0;
                    pin.Verifybyconnectorid = null;
                    pin.InsertOption = 1;
                    pin.PersonalData = new Bio.Core.Api.PersonalData();
                    pin.PersonalData.Typeid = "RUT";
                    pin.PersonalData.Valueid = param.perdonaldata.valueid;
                    pin.PersonalData.Patherlastname = param.perdonaldata.phaterlastname;
                    pin.PersonalData.Motherlastname = param.perdonaldata.motherlastname;
                    pin.PersonalData.Name = param.perdonaldata.name;
                    pin.PersonalData.Photography = param.perdonaldata.photo;
                    pin.PersonalData.Signatureimage = param.perdonaldata.signature;
                    pin.PersonalData.Sex = param.perdonaldata.sex;
                    pin.PersonalData.Documentseriesnumber = param.perdonaldata.serialnumber;
                    if (!string.IsNullOrEmpty(param.perdonaldata.expirationdate))
                    {
                        pin.PersonalData.Documentexpirationdate = Bio.Core.Utils.Utils.Parse(param.perdonaldata.expirationdate);
                    }
                    if (!string.IsNullOrEmpty(param.perdonaldata.birthdate))
                    {
                        pin.PersonalData.Birthdate = Bio.Core.Utils.Utils.Parse(param.perdonaldata.birthdate);
                    }
                    pin.PersonalData.Companyidenroll = Convert.ToInt32(strCompId[i]);
                    pin.PersonalData.Verificationsource = "CI";
                    pin.OperationOrder = Bio.Core.Matcher.Constant.OperationOrder.OPERATIONORDER_LOCALONLY;

                    string xmlparamout;
                    int ret = ServicesManager.Enroll(pin, out xmlparamout);
                    if (ret == Errors.IERR_OK)
                    {
                        LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromPOS - Enrolled OK => ValueID = " + pin.PersonalData.Valueid +
                            " - PhaterLastName = " + pin.PersonalData.Patherlastname + "!");
                    } else if (ret == Errors.IERR_IDENTITY_EXIST)
                    {
                        LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromPOS - Ya existe la identidad => ValueID = " + pin.PersonalData.Valueid +
                            " - PhaterLastName = " + pin.PersonalData.Patherlastname + ". Verifica si modifica...");
                        //Solo si es la fecha de expiracion del documento que esta en al BD es manor a la que viene informada => Modify
                        if (pin.PersonalData.Documentexpirationdate != null &&
                            CumpleFiltroForUpdate(pin.Companyid, pin.PersonalData.Typeid, pin.PersonalData.Valueid, 
                                                  pin.PersonalData.Documentexpirationdate))
                        {
                            pin.Actionid = Api.Constant.Action.ACTION_MODIFY;
                            ret = 0;
                            ret = ServicesManager.Enroll(pin, out xmlparamout);
                            if (ret == Errors.IERR_OK)
                            {
                                LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromPOS - Moodify OK => ValueID = " + pin.PersonalData.Valueid +
                                    " - PhaterLastName = " + pin.PersonalData.Patherlastname + "!");
                            } else
                            {
                                LOG.Warn("BPUnitOfWork.EnrollOrUpdateFromPOS - Moodify NOOK [ret=" + ret.ToString() + "]" +
                                           " => ValueID = " + pin.PersonalData.Valueid +
                                           " - PhaterLastName = " + pin.PersonalData.Patherlastname + "!");
                            }
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BPUnitOfWork.EnrollOrUpdateFromPOS Error Excp:", ex);
            }
            LOG.Debug("BPUnitOfWork.EnrollOrUpdateFromPOS OUT!");
        }

        private static bool CumpleFiltroForUpdate(int company, string typeid, string valueid, DateTime documentexpirationdate)
        {
            bool ret = false;
            try
            {
                string msg;
                BpIdentity identity;
                int reti = AdminBpIdentity.Retrieve(company, typeid, valueid, out identity, out msg, false);
                if (reti == Errors.IERR_OK && identity != null)
                {
                    ret = identity.Documentexpirationdate.HasValue ?
                            (identity.Documentexpirationdate.Value < documentexpirationdate) : false;
                } else
                {
                    LOG.Warn("BPUnitOfWork.CumpleFiltroForUpdate - Error recuperando identity [ret = " + reti.ToString() + "] " +
                                "company/typeid/valueid = " + company.ToString() + "/" + typeid + "/" + valueid);
                }
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("BPUnitOfWork.CumpleFiltroForUpdate Error Excp:", ex);
            }
            return ret;
        }

        //Facial - Added 09-08-2022
        internal static BPResponse VerifyFacial(POSVerifyFacialIn param, BpCompany company)
        {
            /*
                0.- Verifico parametros 
                1.- Proceso cédula con regula o metamap
                    1.1.- Si no recupera rut valido, retorna error 
                2.- Verifica cedula vs selfie
                3.- Graba BpTx con resultado
                    3.1.- Enroal si está habilitado (ver tema de cuando enrolar x tiempo)
                4.- Armo BPResponse y Retorno
            */
            BPResponse _response = new BPResponse();
            int ret = Errors.IERR_OK;
            string msg;
            string msgaux;
            try
            {
                LOG.Debug("BPUnitOfWork.VerifyFacial IN...");

                //0.- Verifico parametros 
                LOG.Debug("BPUnitOfWork.VerifyFacial - Verificando parámetros...");
                if (param == null || 
                    string.IsNullOrEmpty(param.frontcardid) ||
                    string.IsNullOrEmpty(param.selfie))
                {
                    _response.Code = (int)HttpStatusCode.BadRequest;
                    _response.Message = "Parametros no pueden ser nulos [Front Card y Selfie]";
                    LOG.Warn("BPUnitOfWork.VerifyFacial - " + _response.Message);
                    return _response;
                }

                //1.- Proceso cédula con regula o metamap
                //    1.1.- Si no recupera rut valido, retorna error
                Bio.Core.Api.PersonalData oPersonalData;
                string trackidocr;
                LOG.Debug("BPUnitOfWork.VerifyFacial - Iniciando OCRIDCard...");
                ret = OCRIDCard(company.Id, param.clientid, param.frontcardid, param.backcardid, out msg, out oPersonalData, out trackidocr);
                LOG.Debug("BPUnitOfWork.VerifyFacial - Salio OCRIDCard = " + ret.ToString());
                if (ret < 0)
                {
                    _response.Code = ret;
                    if (_response.Code == -7) //Error en conector Regula (Quiza mala imagen)
                    {
                        _response.Code = (int)HttpStatusCode.NotAcceptable;  //Retorna 
                    } 
                    _response.Message = "Error reconociendo cédula [" + msg + "]";
                    LOG.Warn("BPUnitOfWork.VerifyFacial - " + _response.Message);
                    return _response;
                }
                else
                {
                    LOG.Debug("BPUnitOfWork.VerifyFacial - iniciando Verify...");
                    ret = 0;
                    int resultverify = 0;
                    double score = 0;
                    string trackidverify;
                    LOG.Debug("BPUnitOfWork.VerifyFacial - Ingresando a VerifyFacial...");
                    ret = VerifyFacial(company.Id, param.clientid, param.frontcardid, param.selfie, param.threshold,
                                        oPersonalData.Valueid, out resultverify, out score, out msg, out trackidverify);
                    LOG.Debug("BPUnitOfWork.VerifyFacial - Salio de VerifyFacial = " + ret.ToString());
                    if (ret < 0)
                    {
                        _response.Code = ret;
                        _response.Message = "Error verificando cedula vs selfie [" + msg + "]";
                        LOG.Warn("BPUnitOfWork.VerifyFacial - " + _response.Message);
                        return _response;
                    } else
                    {
                        LOG.Debug("BPUnitOfWork.VerifyFacial - Armando respuesta...");
                        _response.Code = 0;
                        _response.Message = null;
                        POSVerifyFacialOutData pout = new POSVerifyFacialOutData();
                        pout.personaldata = oPersonalData;
                        pout.result = resultverify;
                        pout.trackid = trackidverify;
                        pout.score = score;
                        pout.threshold = param.threshold;
                        _response.Data = JsonConvert.SerializeObject(pout);
                    }

                    //if (isVerifyPositive && !string.IsNullOrEmpty(Properties.Settings.Default.POSControllerCompanyIdEnroll))
                    //{
                    //    //Chequeo si debo enrolar y enrolo con el CompanyId configurado
                    //    LOG.Debug("BPUnitOfWork.Verify - Ingreso a enrolar => valueid = " + param.perdonaldata.valueid +
                    //               " - Name = " + param.perdonaldata.phaterlastname + " - Companies=" +
                    //               Properties.Settings.Default.POSControllerCompanyIdEnroll);

                    //    //Solo enrolo y si hay error mando mail pero no corta el flujo
                    //    EnrollOrUpdateFromPOS(param, _TX);

                    //}
                }
            }
            catch (Exception ex)
            {
                ret = (int)HttpStatusCode.InternalServerError;
                _response.Code = ret;
                _response.Message = "Verify Excp Error Desconocido [" + ex.Message + "]";
                LOG.Error("BPUnitOfWork.VerifyFacial Excp Error:", ex);
            }
            LOG.Debug("BPUnitOfWork.VerifyFacial OUT => ret = " + ret.ToString());
            return _response;
        }

        /// <summary>
        /// Dada una cedula de frente sola o de back tambien, realiza el reconocimiento
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="msg"></param>
        /// <param name="personalData"></param>
        /// <param name="trackIdBP"></param>
        /// <returns></returns>
        internal static int OCRIDCard(int _company, string _client, string _front, string _back,
                                      out string msg, out Bio.Core.Api.PersonalData personalData, 
                                      out string trackIdBP)
        {
            //msg = null;
            //mRZIDCard = new CIParam();
            //mRZIDCard.TypeId = "RUT";
            //mRZIDCard.ValueId = "21284415-2";
            //mRZIDCard.Name = "Gustavo Suhit";
            //mRZIDCard.Nacionality = "ARG";
            //mRZIDCard.Sex = "M";
            //mRZIDCard.BirthDate = "28/09/1969";

            //trackIdBP = "1950ac4827c14334a807c3be19dd470a";
            //return 0;
            int ret = 0;
            msg = null;
            trackIdBP = null;
            personalData = null;
            try
            {
                LOG.Debug("BPUnitOfWork.OCRIDCard IN...");

                BioPortal.Server.Api.XmlParamOut paramout = null;
                XmlParamIn xmlParamIn = new XmlParamIn();
                BioPortal.Server.Api.XmlParamIn paramin = new BioPortal.Server.Api.XmlParamIn();
                paramin.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_GET;
                paramin.Companyid = _company;
                paramin.Clientid = _client;
                string msgaux;
                BpOrigin orig = AdminBpOrigin.Retrieve(61, out msgaux);
                paramin.Origin = (orig != null ? 61 : 1);  //Luego en produccion les inidcamos un valor 
                paramin.Authenticationfactor = Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL;
                paramin.Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_FACIAL_NAMKU;
                paramin.Bodypart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_FACE;
                paramin.Verifybyconnectorid = string.IsNullOrEmpty(Properties.Settings.Default.POSOCRDocument) ?
                                                "Regula" : Properties.Settings.Default.POSOCRDocument; // "MRZNamku";
                paramin.OperationOrder = 3;
                paramin.InsertOption = 0; //No hace enroll porque es solo reconocimeinto de datos

                paramin.PersonalData = new Bio.Core.Api.PersonalData();
                paramin.PersonalData.Typeid = "RUT"; // oXmlIn.CIParam.TypeId;
                paramin.PersonalData.Valueid = "NA"; // oXmlIn.CIParam.ValueId;
                paramin.PersonalData.DocImageFront = _front;
                paramin.PersonalData.DocImageBack = _back;
                LOG.Debug("BPUnitOfWork.OCRIDCard _front = " + _front.Substring(0, 15) + "...");
                LOG.Debug("BPUnitOfWork.OCRIDCard _back = " + (string.IsNullOrEmpty(_back)?"Null":_back.Substring(0, 15)) + "...");

                //string xmlin = BioPortal.Server.Api.XmlUtils.SerializeAnObject(paramin);
                string xmlout;
                DateTime start = DateTime.Now;
                int iret = ServicesManager.Get(paramin, out xmlout);
                DateTime end = DateTime.Now;
                LOG.Debug("BPUnitOfWork.OCRIDCard Tiempo = " + (end - start).TotalMilliseconds.ToString() + " - ret = " + iret);
                if (iret != 0)
                {
                    ret = iret;
                    msg = "BPUnitOfWork.OCRIDCard - Error retornado desde Connector = " + iret;
                    if (!string.IsNullOrEmpty(xmlout))
                    {
                        paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
                        if (paramout != null)
                        {
                            msg = msg + " - MsgErr = " +
                                (string.IsNullOrEmpty(paramout.Message) ? "S/C" : paramout.Message);
                        }
                    }
                    LOG.Error(msg);
                }
                else
                {
                    if (string.IsNullOrEmpty(xmlout))
                    {
                        ret = Errors.IERR_CONNECTOR_NOT_AVAILABLE;
                        msg = "BPUnitOfWork.OCRIDCard - Retorno desde Conector Null";
                        LOG.Error(msg);
                    }
                    else
                    {
                        paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
                        if (paramout != null)
                        {
                            //Rechaza si el rut no es valido
                            //if (!string.IsNullOrEmpty(paramout.PersonalData.Valueid) && !paramout.PersonalData.Valueid.Equals("NA") &&
                            //    !string.IsNullOrEmpty(oXmlIn.CIParam.ValueId) && !oXmlIn.CIParam.ValueId.Equals("NA") &&
                            //    !paramout.PersonalData.Valueid.Equals(oXmlIn.CIParam.ValueId))
                            //{
                            //    ret = Errors.IRET_ERR_CI_IDCARD_INCONSISTENCE_DATA_OCR_RECOGNIZED;
                            //    msg = "BPHelper.VerifyAndOCRIDCard - Error de inconsistencia entre RUT pedido [" + oXmlIn.CIParam.ValueId +
                            //        "] y RUT reconocido [" + paramout.PersonalData.Valueid + "]";
                            //    LOG.Error(msg);
                            //    return ret;
                            //}

                            //Chequeo ciertos valores básicos para rechazar si no reconoció bien
                            if (string.IsNullOrEmpty(paramout.PersonalData.Valueid) || paramout.PersonalData.Valueid.Equals("NA") ||
                            string.IsNullOrEmpty(paramout.PersonalData.Patherlastname) ||
                            string.IsNullOrEmpty(paramout.PersonalData.Name) ||
                            string.IsNullOrEmpty(paramout.PersonalData.Photography))
                            {
                                ret = Errors.IERR_CONNECTOR_MALFORMED_ANSWER;
                                msg = "BPUnitOfWork.OCRIDCard - Parseo XmlParamOut Null [" + Errors.SERR_CONNECTOR_MALFORMED_ANSWER + "]";
                                LOG.Error(msg);
                                return ret;
                            }

                            LOG.Debug("BPUnitOfWork.OCRIDCard - ValueId = " + paramout.PersonalData.Valueid + " - Nombre = " +
                                (string.IsNullOrEmpty(paramout.PersonalData.Name) ? "" : paramout.PersonalData.Name) + " " +
                                (string.IsNullOrEmpty(paramout.PersonalData.Patherlastname) ? "" : paramout.PersonalData.Patherlastname) +
                                (string.IsNullOrEmpty(paramout.PersonalData.Motherlastname) ? "" : paramout.PersonalData.Motherlastname));
                            trackIdBP = paramout.Trackid;
                            personalData = paramout.PersonalData;
                        }
                        else
                        {
                            ret = Errors.IERR_CONNECTOR_NOT_AVAILABLE;
                            msg = "BPUnitOfWork.OCRIDCard - Parseo xmlout Null";
                            LOG.Error(msg);
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msg = "BPUnitOfWork.OCRIDCard Error = " + ex.Message;
                LOG.Error(msg);
            }
            return ret;
        }

        internal static int VerifyFacial(int _company, string _client, string _front, string _selfie, double _threshold, string _valueid,
                                         out int resultverify, out double score, out string msg, out string trackidbp)
        {
            int ret = 0;
            resultverify = 2;
            score = -1;
            trackidbp = null;
            msg = null;
            try
            {
                LOG.Debug("BPUnitOfWork.VerifyFacial IN...");
                BioPortal.Server.Api.XmlParamOut paramout = null;
                LOG.Debug("BPUnitOfWork.VerifyFacial Verificando RUT = " + _valueid);                
                BioPortal.Server.Api.XmlParamIn paramin = new BioPortal.Server.Api.XmlParamIn();
                paramin.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_PLUGIN_MATCHER; // Properties.Settings.Default.CIBPActionId; // 1201;
                paramin.Companyid = _company;
                paramin.Clientid = _client;
                string msgaux;
                BpOrigin orig = AdminBpOrigin.Retrieve(61, out msgaux);
                paramin.Origin = (orig != null ? 61 : 1);  //Luego en produccion les inidcamos un valor 
                paramin.Authenticationfactor = Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL;
                paramin.Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_FACIAL_NAMKU;
                paramin.Bodypart = Bio.Core.Matcher.Constant.BodyPart.BODYPART_FACE;
                paramin.OperationOrder = 1;
                paramin.Threshold = Properties.Settings.Default.TV2D_Threshold; // _threshold => Esto lo uso para verificar aca, pero uso el threshold generico de config para match

                paramin.SampleCollection = new List<Bio.Core.Api.Sample>();
                #region Data para verificar con Namku
                //Selfie
                Bio.Core.Api.Sample sample = new Bio.Core.Api.Sample();
                sample.Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_JPG; // 41;
                LOG.Debug("BPUnitOfWork.VerifyFacial Selfie = " + _selfie.Substring(0, 15) + "...");
                sample.Data = _selfie;
                paramin.SampleCollection.Add(sample);
                //IDCard
                Bio.Core.Api.Sample sample2 = new Bio.Core.Api.Sample();
                sample2.Minutiaetype = Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_JPG; // 41;
                LOG.Debug("BPUnitOfWork.VerifyFacial _front = " + _front.Substring(0, 15) + "...");
                sample2.Data = _front;
                paramin.SampleCollection.Add(sample2);

                #endregion Data para verificar con Namku

                paramin.PersonalData = new Bio.Core.Api.PersonalData();
                paramin.PersonalData.Typeid = "RUT";
                paramin.PersonalData.Valueid = _valueid;

                //string xmlin = BioPortal.Server.Api.XmlUtils.SerializeAnObject(paramin);
                string xmlout;
                DateTime start = DateTime.Now;
                int iret = ServicesPluginManager.Process(paramin, out xmlout);
                DateTime end = DateTime.Now;
                LOG.Debug("BPUnitOfWork.VerifyFacial Tiempo = " + (end - start).TotalMilliseconds.ToString() + " - ret = " + iret);
                if (iret != 0)
                {
                    ret = iret;
                    msg = "BPUnitOfWork.VerifyFacial - Error retornado desde BioPOrtal = " + iret;
                    LOG.Error(msg);
                }
                else
                {
                    if (string.IsNullOrEmpty(xmlout))
                    {
                        ret = iret;
                        msg = "BPUnitOfWork.VerifyFacial - Error retornado desde BioPOrtal = " + iret;
                        LOG.Error(msg);
                    }
                    else
                    {
                        paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
                        if (paramout != null)
                        {
                            resultverify = Convert.ToInt32(paramout.Result);
                            score = GetPorcentajeScoreFacial(resultverify, Properties.Settings.Default.BPWebScorePorcentajeBase, paramout.Score, 
                                                             Properties.Settings.Default.TV2D_Threshold);
                            resultverify = score >= _threshold ? 1 : 2;
                            trackidbp = paramout.Trackid;
                            //Actualizo valores de tx de acuerdo al cliente => Score + Threshold + Result
                            LOG.Debug("BPHelper.VerifyCIvsBD - Updating tx = " + trackidbp);
                            BpTx tx = AdminBpTx.RetrieveTxByTrackid(trackidbp, out msg, false);
                            if (tx != null) {
                                LOG.Debug("BPHelper.VerifyCIvsBD - [ORIG] score/threshold = " + tx.Score.ToString() + "/" + tx.Threshold.ToString() +
                                            " - Result = " + tx.Result.ToString());
                                tx.Score = score;
                                tx.Threshold = _threshold;
                                tx.Result = resultverify;
                                tx.Dynamicdata = "{ \"origvalues\" : { \"score\": \"" + tx.Score.ToString() + "\"," +
                                                                       "\"threshold\": \"" + tx.Threshold.ToString() + "\"," +
                                                                       "\"result\": \"" + tx.Result.ToString() + "\" }}";
                                msg = "";
                                if (AdminBpTx.Update(tx, out msg))
                                {
                                    LOG.Debug("BPHelper.VerifyCIvsBD - [NEW] score/threshold = " + tx.Score.ToString() + "/" + tx.Threshold.ToString() +
                                            " - Result = " + tx.Result.ToString());
                                } else
                                {
                                    LOG.Warn("BPHelper.VerifyCIvsBD - Error actualizando valores de Tx [" + msg + "]");
                                }
                            } else
                            {
                                LOG.Warn("BPHelper.VerifyCIvsBD - Error recuperando Tx = " + trackidbp + " [" + msg + "]");
                            }
                            LOG.Debug("BPHelper.VerifyCIvsBD Retorno => resultverify = " + resultverify + " - score/threshold = " + score + "/" + _threshold + 
                                                            " - TrackId = " + trackidbp);
                        }
                        else
                        {
                            ret = Errors.IERR_DESERIALIZING_DATA;
                            msg = "BPUnitOfWork.VerifyFacial - Parseo xmlout de verify facial Null";
                            LOG.Error(msg);
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msg = "BPUnitOfWork.VerifyFacial Error = " + ex.Message;
                LOG.Error(msg);
            }
            return ret;
        }

        internal static double GetPorcentajeScoreFacial(int result, double thresholdp, double score, double treshold)
        {
            double ret = 0;
            double _BASE = 75;
            try
            {
                try
                {
                    LOG.Debug("BPUnitOfWork.GetPorcentajeScore IN => score = " + score.ToString() + "/threshold = " + treshold);
                    _BASE = thresholdp; // Properties.Settings.Default.BPWebScorePorcentajeBase;
                    if (_BASE < 40 || _BASE > 100)
                        _BASE = 75;
                    LOG.Debug("BPUnitOfWork.GetPorcentajeScore - _BASE = " + _BASE.ToString());
                }
                catch (Exception ex1)
                {
                    LOG.Error("BPHelper.GetPorcentajeScore - " +
                        "Falta configurar o mal configurado en web.config CIScorePorcentajeBase [Error=" +
                        ex1.Message + "] => Default=75");
                }

                if (result == 1) //Si es positivo => Sumo el delta al score base
                {
                    //double rts = (25 * (treshold - score)) / treshold;
                    double rts = ((100 - _BASE) * (treshold - score)) / treshold;
                    LOG.Debug("BPUnitOfWork.GetPorcentajeScore => rts = " + rts.ToString());

                    //ret = 75 + rts;
                    ret = _BASE + rts;
                    if (ret > 100) ret = 100;
                } else if (result == 2) //Si es negativo => Resto el delta al score base
                {
                    //double rts = (25 * (treshold - score)) / treshold;
                    double rts = ((100 - _BASE) * (treshold - score)) / treshold;
                    LOG.Debug("BPUnitOfWork.GetPorcentajeScore => rts = " + rts.ToString());

                    //ret = 75 + rts;
                    ret = rts; // _BASE - rts;
                    if (ret < 0) ret = 0;
                }
                LOG.Debug("BPHelper.GetPorcentajeScore Porcentaje Seguridad => " + ret.ToString("##.##") + "%");
            }
            catch (Exception ex)
            {
                ret = Properties.Settings.Default.BPWebScorePorcentajeBase; // 75;
                LOG.Error("BPHelper.GetPorcentajeScore Error => " + ex.Message);
            }
            LOG.Debug("BPHelper.GetPorcentajeScore OUT!");
            return ret;
        }

        #endregion POSController

    }
}