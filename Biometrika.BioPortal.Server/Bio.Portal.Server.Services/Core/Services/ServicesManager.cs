using System;
using System.Collections;
using System.Collections.Generic;
using Bio.Core.Api;
using Bio.Core.Api.Matcher.Interface;
using Bio.Core.Constant;
using Bio.Core.Matcher;
using Bio.Core.Matcher.Interface;
using Bio.Core.Matcher.Token;
using Bio.Core.Serialize;
using Bio.Portal.Server.Common.Common;
using Bio.Portal.Server.Common.Entities.Database;
using Bio.Portal.Server.Services.Core.Database;
using BioPortal.Server.Api;
using Bio.Core.Matcher.Constant;
using log4net;
using Action = Bio.Core.Matcher.Constant.Action;
using XmlUtils = BioPortal.Server.Api.XmlUtils;
using System.Xml;
using System.Text;

namespace Bio.Portal.Server.Services.Core.Services
{
    ///<summary>
    /// Clase destinada a atender los requerimientos que vienen desde los web services.
    /// En los Web Services se chequean los parámetros, en el Web se descomprime el TOKEN,
    /// y con el xmlparamin deserializado se llama a la rutina de aqui.
    /// Es la misma para ambos WS.
    ///</summary>
    public class ServicesManager
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(ServicesManager));

#region Verify Region

        /// <summary>
        /// Recibe los parámetros para hacer la verificacion, y devuelve resultado
        /// de la misma, o mensaje de error si existe.
        /// Los pasos son:
        ///         1.- Genero Matcher, Extractor y Template correspondioente de acuerdo a AF y MT. 
        ///             Esto lo hago clonando desde el MatcherManager global. 
        ///             1.1.- Si no existe retorno con error.
        ///         2.- Genero template correspondiente desde el sample que viene en Input.
        ///             2.1.- Genero template desde Input
        ///             2.2.- Llamo a Extractor con el template generado y este retorna el mismo
        ///                   template si ya es minucia, o si es sample, lo transforma de acuerdo a
        ///                   como lo entiende cada algoritmo, y genera el template con las minucias 
        ///                   correspondientes a ese template.
        ///         3.- De acuerdo al tipoid/valueid, y tipo de verificación pedida (si es contra un 
        ///             bodypart o todos, etc, recupero la data biométrica con BioPOrtal.Server.Common.Database
        ///         4.- Por cada BIR recuperado desde la BD, genero un Template, y usando el Matcher 
        ///             hago la comparación, hasta encontrar el resultado satisfactorio (first/best) o
        ///             recorrer la lista recuperada.  
        ///         5.- Generar respuesta en Xml.
        /// </summary>
        /// <param name="oparamin">Objeto de parametros</param>
        /// <param name="xmlparamout">XML con resultado</param>
        /// <returns>Retorna el xml de respuesta</returns>
        static public int Verify(XmlParamIn oparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut paramout = new XmlParamOut();
            string msg;
            int iErr = 0;
            DateTime start, end;
            BpTx txcreated;
            Parameters oparamOutVerify;
            int operationsource = 0;
            BpIdentity identity = null;
            int operationOrder = Global.CONFIG.OperationOrder;
            string xmlparamverifyout = null;
            bool toRemote = false;
            DynamicData ddataFromConnector = null;
            IMatcher matcher = null;
            string xmlparamverify = null;
            PersonalData pdout;
            MatcherInstance matcherintance = null;
            IExtractor extractor = null;
            ITemplate templateCurrent = null;
            ITemplate templateCurrentExtracted = null;
            XmlParamIn oparaminout;

            try
            {
                LOG.Debug("ServiceManager.Verify In...");
                LOG.Debug("ServiceManager.Verify -- XmlParamIn");
                LOG.Debug(XmlUtils.SerializeObject<XmlParamIn>(oparamin));

                start = DateTime.Now;

                //Added 02-2015 para verificar BioSignature
                if (oparamin.Actionid == Action.ACTION_VERIFY_BIOSIGNATURE)
                {
                    return VerifyBioSignature(oparamin, out xmlparamout);
                }

                //Added 29-04-2016
                //Si debe chequear serial de device, se fija en tabla bp_devices para esa compañia si esá y está habilitado.
                if (Global.CONFIG.CheckClientConnected == 1) 
                {
                    //Added 29-04-2016
                    //Para control de cliente via IP y no x Serial ID del lector
                    if (Global.CONFIG.AutoRegisterClientConnected == 2)
                    {
                        oparamin.Clientid = oparamin.Ipenduser;
                    }

                    if (!ClientAuthorized(oparamin, out msg))
                    {
                        msg = "Device no habilitado en la plataforma! [Serial ID = " + oparamin.Clientid + " - " + 
                                                                       "(Msg=" + msg + ")]";
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        return Errors.IERR_CLIENT_NOT_AUTHORIZED;
                    }
                }


                if (oparamin.OperationOrder > 0)
                {
                    operationOrder = oparamin.OperationOrder;
                }

                //Added 02-2015 - Si es oprationroder solo remoto, solo puede hacer verify, y el get aparte
                if (operationOrder == OperationOrder.OPERATIONORDER_REMOTEONLY &&
                    oparamin.Actionid == Action.ACTION_VERIFYANDGET)
                {     
                    oparamin.Actionid = Action.ACTION_VERIFY;
                }

                LOG.Debug("ServiceManager.Verify -- OperationOrder = " + operationOrder.ToString());


                //Si es Verify Local solo o Local Primero => trato de hacer extract sino sigo porque
                //va por connector
                if (operationOrder == OperationOrder.OPERATIONORDER_LOCALFIRST ||
                        operationOrder == OperationOrder.OPERATIONORDER_LOCALONLY)
                {
                    //1.- Genero Matcher, Extractor y Template correspondioente de acuerdo a AF y MT. 
                    //    Esto lo hago clonando desde el MatcherManager global. 
                    LOG.Debug("ServiceManager.Verify GetMatcehrInstance => AF=" + oparamin.Authenticationfactor +
                                                                      " - MT= " + oparamin.Minutiaetype);
                    matcherintance =
                        Global.MATCHER_MANAGER.GetMatcherInstance(oparamin.Authenticationfactor,
                                                                  oparamin.Minutiaetype);

                    LOG.Debug("ServiceManager.Verify -- GetMatcherInstance - matcherintance != null : " +
                              (matcherintance != null).ToString());

                    //    1.1.- Si no existe retorno con error.
                    if (matcherintance == null)
                    {
                        msg = "No existe Matcher configurado para esta tecnología y minucia [" +
                                           "AF=" + oparamin.Authenticationfactor + "-MT=" +
                                           oparamin.Minutiaetype + "]";
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        end = DateTime.Now;
                        DatabaseHelper.SaveTx(Errors.IERR_MATCHER_NOT_AVAILABLE,
                                              OperationOrder.OPERATIONORDER_LOCALONLY, oparamin, null,
                                              start, end, out oparamOutVerify, out txcreated, out identity);
                        paramout.PersonalData = FillPersonalData(oparamin, identity);
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        return Errors.IERR_MATCHER_NOT_AVAILABLE;
                    }
                    LOG.Debug("ServiceManager.Verify -- GetMatcherInstance OK!");

                    //2.- Genero template correspondiente desde el sample que viene en Input.
                    //    2.1.- Genero template desde Input
                    //    2.2.- Llamo a Extractor con el template generado y este retorna el mismo
                    //          template si ya es minucia, o si es sample, lo transforma de acuerdo a
                    //          como lo entiende cada algoritmo, y genera el template con las minucias 
                    //          correspondientes a ese template.
                    LOG.Debug("ServiceManager.Verify -- MatcherInstance.Extractor IN...");
                    extractor = matcherintance.Extractor;
                    templateCurrent = matcherintance.Template;
                    templateCurrent.AuthenticationFactor = oparamin.Authenticationfactor;
                    templateCurrent.MinutiaeType = oparamin.SampleCollection[0].Minutiaetype;
                    templateCurrent.SetData = oparamin.SampleCollection[0].Data;
                    templateCurrent.AdditionalData = oparamin.Additionaldata;
                    LOG.Debug("ServiceManager.Verify -- templateCurrent.AuthenticationFactor = " + templateCurrent.AuthenticationFactor);
                    LOG.Debug("ServiceManager.Verify -- templateCurrent.MinutiaeType = " + templateCurrent.MinutiaeType);
                    LOG.Debug("ServiceManager.Verify -- templateCurrent.Data = " + oparamin.SampleCollection[0].Data);
                    LOG.Debug("ServiceManager.Verify -- templateCurrent.AdditionalData = " + (!String.IsNullOrEmpty(templateCurrent.AdditionalData)? templateCurrent.AdditionalData:""));
                    
                    //extractor.Threshold = extractor.Threshold != oparamin. No viene el THExtractor, ver si lo agrego
                    iErr = extractor.Extract(templateCurrent, TemplateDestination.TEMPLATEDESTINATION_TOVERIFY,
                                             out templateCurrentExtracted);
                    //Si hay error retorno
                    if (iErr != 0)
                    {
                        msg = "Error extrayendo minucias [" + iErr + "]";
                        LOG.Error(msg);
                        paramout.Message = msg;
                        end = DateTime.Now;
                        DatabaseHelper.SaveTx(Errors.IERR_EXTRACTING, OperationOrder.OPERATIONORDER_LOCALONLY,
                                              oparamin, null, start, end, out oparamOutVerify, out txcreated, out identity);
                        paramout.PersonalData = FillPersonalData(oparamin, identity);
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        return Errors.IERR_EXTRACTING;
                    }

                    LOG.Debug("ServiceManager.Verify -- Extractor.Extract = OK");
                }

                //3.- De acuerdo al tipoid/valueid, y tipo de verificación pedida (si es contra un 
                //    bodypart o todos, etc, recupero la data biométrica con BioPortal.Server.Common.Database
                if (oparamin.PersonalData == null ||
                    oparamin.PersonalData.Typeid == null || oparamin.PersonalData.Valueid == null)
                {
                    msg = "No existe typeid/valueid para determinar identidad de verificacion";
                    LOG.Error(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(Errors.IERR_BAD_PARAMETER, OperationOrder.OPERATIONORDER_LOCALONLY,
                                          oparamin, null, start, end, out oparamOutVerify, out txcreated, out identity);
                    paramout.PersonalData = FillPersonalData(oparamin, identity);
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    LOG.Debug("ServiceManager.Verify -- Entrando a OPERATIONORDER_LOCALFIRST o OPERATIONORDER_LOCALONLY...");

                    if (operationOrder == OperationOrder.OPERATIONORDER_LOCALFIRST ||
                        operationOrder == OperationOrder.OPERATIONORDER_LOCALONLY)
                    {
                        List<BpBir> listBirs = null;
                        LOG.Debug("ServiceManager.Verify -- DatabaseHelper.GetBirs in...");
                        iErr = DatabaseHelper.GetBirs(oparamin, out listBirs);
                        LOG.Debug("ServiceManager.Verify -- DatabaseHelper.GetBirs out! [iErr=" + iErr + "]");
                        //Si hay error informo y salgo
                        if (iErr != Errors.IERR_OK)
                        {
                            msg = "Problemas para conseguir muestras con que comparar";
                            LOG.Error(msg);
                            paramout.Message = msg;
                            end = DateTime.Now;
                            DatabaseHelper.SaveTx(iErr, OperationOrder.OPERATIONORDER_LOCALONLY, oparamin, null,
                                                  start, end, out oparamOutVerify, out txcreated, out identity);
                            paramout.PersonalData = FillPersonalData(oparamin, identity);
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            if (operationOrder == OperationOrder.OPERATIONORDER_LOCALFIRST)
                            {
                                toRemote = true; //Si debe consultar remoto pasa, sino sale con error de no existencia
                            }
                            else
                            {
                                return iErr;
                            }
                        }
                        else
                        {
                            //Si no hay Birs de esa identity para ese MT
                            if (listBirs == null || listBirs.Count == 0)
                            {
                                msg = "No existen muestras con que comparar para [" +
                                      "AF=" + oparamin.Authenticationfactor + "-MT=" +
                                      oparamin.Minutiaetype + " de typeid/valueid = " +
                                      oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid + "]";
                                LOG.Error(msg);
                                paramout.Message = msg;
                                end = DateTime.Now;
                                DatabaseHelper.SaveTx(Errors.IERR_BIR_NOT_FOUND,
                                                      OperationOrder.OPERATIONORDER_LOCALONLY,
                                                      oparamin, null, start, end, out oparamOutVerify, out txcreated,
                                                      out identity);
                                paramout.PersonalData = FillPersonalData(oparamin, identity);
                                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                                if (operationOrder == OperationOrder.OPERATIONORDER_LOCALFIRST)
                                {
                                    toRemote = true;
                                    //Si debe consultar remoto pasa, sino sale con error de no existencia
                                }
                                else
                                {
                                    return Errors.IERR_BIR_NOT_FOUND;
                                }
                            }
                            else //Sino preparo el proceso de verificación
                            {
                                LOG.Debug("ServiceManager.Verify -- Verificando listBirs.Count = " +
                                          listBirs.Count.ToString());

                                //Instancio Matcher
                                LOG.Debug("ServiceManager.Verify - Matcherintance.Matcher IN...");
                                matcher = matcherintance.Matcher;
                                //Seteo los parametros de umbrales diferentes si hay diferencias
                                matcher.Threshold = oparamin.Threshold != 0
                                                        ? oparamin.Threshold
                                                        : matcher.Threshold;
                                LOG.Debug("ServiceManager.Verify - matcher.Threshold = " + matcher.Threshold);
                                matcher.MatchingType = oparamin.Matchingtype != 0
                                                           ? oparamin.Matchingtype
                                                           : matcher.MatchingType;
                                if (matcher.MatchingType != 1 && matcher.MatchingType != 2)
                                {
                                    matcher.MatchingType = 1;
                                }
                                LOG.Debug("ServiceManager.Verify - matcher.MatchingType = " + matcher.MatchingType);
                                
                                //Preparo xml de parametros para llamar al Matcher
                                Parameters parameters = new Parameters();
                                parameters.AddValue("currentTemplate", templateCurrentExtracted.GetData);
                                parameters.AddValue("additionalData", oparamin.Additionaldata);

                                parameters.AddValue("currentTemplateAF", templateCurrentExtracted.AuthenticationFactor);
                                parameters.AddValue("currentTemplateBP", templateCurrentExtracted.BodyPart);
                                parameters.AddValue("currentTemplateMT", templateCurrentExtracted.MinutiaeType);
                                parameters.AddValue("currentTemplateType", templateCurrentExtracted.Type);

                                string xmltemplates = null;
                                LOG.Debug("ServiceManager.Verify -- DatabaseHelper.ConvertListBirsToListTemplate in...");
                                iErr = DatabaseHelper.ConvertListBirsToListTemplate(listBirs, out xmltemplates);
                                if (iErr == 0)
                                {
                                    parameters.AddValue("listBaseTemplates", xmltemplates);
                                    LOG.Debug("ServiceManager.Verify -- DatabaseHelper.ConvertListBirsToListTemplate OK");
                                }
                                else
                                {
                                    msg = "Error serializando lista templates para verificar [iErr=" + iErr + "]";
                                    LOG.Warn(msg);
                                    paramout.Message = msg;
                                    end = DateTime.Now;
                                    DatabaseHelper.SaveTx(Errors.IERR_SERIALIZING_DATA,
                                                          OperationOrder.OPERATIONORDER_LOCALONLY,
                                                          oparamin, null, start, end, out oparamOutVerify,
                                                          out txcreated,
                                                          out identity);
                                    paramout.PersonalData = FillPersonalData(oparamin, identity);
                                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                                    return Errors.IERR_SERIALIZING_DATA;
                                }

                                //Llamo a Matcher Verify
                                LOG.Debug("ServiceManager.Verify -- Parameters.SerializeToXml(parameters) in...");
                                xmlparamverify = Parameters.SerializeToXml(parameters);
                                LOG.Debug("ServiceManager.Verify -- Parameters.SerializeToXml(parameters) out");
                                LOG.Debug("ServiceManager.Verify -- xmlparamverify = " + xmlparamverify);
                            }
                        }
                    }

                    start = DateTime.Now;

                    //0. Determino cual es la operation order entre param y global
                    //1.-Si es solo local => Local
                    //2.-Si es solo remoto o 1local2remoto y fallo => Remoto

                    //Si es local solo o primero local, y toRemote = false (queire decir que se pudo obtener data
                    // de esa identidad para verificar) => chequeo 
                    if ((operationOrder == OperationOrder.OPERATIONORDER_LOCALFIRST ||
                        operationOrder == OperationOrder.OPERATIONORDER_LOCALONLY) && !toRemote)
                    {
                        operationsource = Source.SOURCE_LOCAL;
                        LOG.Debug("ServiceManager.Verify -- matcher.Verify in...");
                        iErr = matcher.Verify(xmlparamverify, out xmlparamverifyout);
                        LOG.Debug("ServiceManager.Verify -- matcher.Verify iErr = " + iErr.ToString());
                        LOG.Debug("ServiceManager.Verify -- out xmlparamverifyout = " + xmlparamverifyout);

                        //Si es localfirst, debo chequear si fallo para ir a remote
                        if (operationOrder == OperationOrder.OPERATIONORDER_LOCALFIRST)
                        {
                            if (iErr == Errors.IERR_OK)
                            {
                                //Parameters oparamoutverify = Parameters.DeserializeFromXml(xmlparamverifyout);
                                oparamOutVerify = Parameters.DeserializeFromXml(xmlparamverifyout);
                                int result = Convert.ToInt32(oparamOutVerify.GetValue("result"));
                                if (result == 2) //Fue negativo 
                                {
                                    toRemote = true;  //Mando a remoto
                                }
                            }
                            else toRemote = true;  //Mando a remoto
                        }
                    }

                    //Si es solo remoto o bien fallo en Localfirst
                    if (operationOrder == OperationOrder.OPERATIONORDER_REMOTEONLY || toRemote)
                    {
                        LOG.Debug("ServiceManager.Verify -- Entrando en OPERATIONORDER_REMOTEONLY [toRemote=" + toRemote + "]...");
                        operationsource = Source.SOURCE_REMOTE;

                        //i.-Tomo instancia de conector que viene informado, si no esta, devuelvo error
                        string conxid = oparamin.Verifybyconnectorid == null ||
                                        oparamin.Verifybyconnectorid.Trim().Length == 0
                                            ? Global.CONFIG.ConnectorDefault
                                            : oparamin.Verifybyconnectorid;
                        LOG.Debug("ServiceManager.Verify -- conxid = " + conxid);
                        if (conxid == null || conxid.Trim().Length == 0)
                        {
                            msg = "No hay ConnectorId configurado ni por parametro ni por default [" +
                                                            oparamin.Verifybyconnectorid + "]";
                            LOG.Warn(msg);
                            paramout.Message = msg;
                            end = DateTime.Now;
                            DatabaseHelper.SaveTx(Errors.IERR_CONNECTOR_NOT_AVAILABLE,
                                                  operationOrder, oparamin, null,
                                                  start, end, out oparamOutVerify, out txcreated, out identity);
                            paramout.PersonalData = FillPersonalData(oparamin, identity);
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            return Errors.IERR_CONNECTOR_NOT_AVAILABLE;
                        }

                        LOG.Debug("ServiceManager.Verify -- GetConnectorInstance IN...");
                        ConnectorInstance connectorintance =
                            Global.CONNECTOR_MANAGER.GetConnectorInstance(conxid);

                        LOG.Debug("ServiceManager.Verify -- GetConnectorInstance = " +
                                    connectorintance != null ? "OK" : "NO OK");

                        //    1.1.- Si no existe retorno con error.
                        if (connectorintance == null)
                        {
                            msg = "No existe Connector configurado para este ConnectorId [" +
                                                            oparamin.Verifybyconnectorid + "]";
                            LOG.Warn(msg);
                            paramout.Message = msg;
                            end = DateTime.Now;
                            DatabaseHelper.SaveTx(Errors.IERR_CONNECTOR_NOT_AVAILABLE,
                                                  operationOrder, oparamin, null,
                                                  start, end, out oparamOutVerify, out txcreated, out identity);
                            paramout.PersonalData = FillPersonalData(oparamin, identity);
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            return Errors.IERR_CONNECTOR_NOT_AVAILABLE;
                        }

                        IConnector connector = connectorintance.Connector;
                        string strxmlout;
                        //TO-DO Sacar luego
                        //string aux = oparamin.Verifybyconnectorid;
                        //oparamin.Verifybyconnectorid = "0";
                        string strxmlin = XmlUtils.SerializeObject(oparamin);

                        LOG.Debug("ServiceManager.Verify -- connector.Verify IN...");
                        iErr = connector.Verify(strxmlin, out strxmlout);
                        LOG.Debug("ServiceManager.Verify -- connector.Verify OUT = " + iErr.ToString());

                        //TO-DO Sacar luego
                        //oparamin.Verifybyconnectorid = aux;
                        //Si viene error informo
                        if (iErr != Errors.IERR_OK)
                        {
                            msg = "Error verificando typeid/valueid = [" +
                                  oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid + "] en " +
                                  "ConnectorId = " + oparamin.Verifybyconnectorid;
                            LOG.Warn(msg);
                            paramout.Message = msg;
                            end = DateTime.Now;
                            DatabaseHelper.SaveTx(iErr, operationsource, oparamin, null,
                                                  start, end, out oparamOutVerify, out txcreated, out identity);
                            paramout.PersonalData = FillPersonalData(oparamin, identity);
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            return iErr;
                        }
                        else //Si no hay error, armo salida
                        {
                            /* Viene esta data:
                                oXmlout.SetValue("message", "");
                                oXmlout.SetValue("status", iretremoto.ToString());
                                oXmlout.AddValue("trackid", oXmlParamOut.Trackid);
                                oXmlout.AddValue("result", oXmlParamOut.Result.ToString());
                                oXmlout.AddValue("score", oXmlParamOut.Score.ToString());
                                oXmlout.AddValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
                                oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
                            */
                            LOG.Debug("ServiceManager.Verify -- DynamicData.DeserializeFromXml=");
                            LOG.Debug("ServiceManager.Verify -- " + strxmlout);
                            ddataFromConnector = DynamicData.DeserializeFromXml(strxmlout);
                            //Si no puede deserializar devuelve error 
                            if (ddataFromConnector == null)
                            {
                                msg = "Error deserializando respuesta de ConnectorId = " + oparamin.Verifybyconnectorid;
                                LOG.Warn(msg);
                                paramout.Message = msg;
                                end = DateTime.Now;
                                DatabaseHelper.SaveTx(iErr, operationsource, oparamin, null,
                                                      start, end, out oparamOutVerify, out txcreated, out identity);
                                paramout.PersonalData = FillPersonalData(oparamin, identity);
                                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                                return Errors.IERR_CONNECTOR_MALFORMED_ANSWER;
                            }
                            else //Sino formatea la salida como parameter para ser igaul a Matcher local, asi puede grabar SaveTx mas adelante
                            {
                                //ddataFromConnector.AddValue("varificationsource", oparamin.Verifybyconnectorid);
                                //ddataFromConnector.AddValue("bodypart", oparamin.Bodypart);
                                
                                xmlparamverifyout = ServicesManager.FormatDynamicDataToParameter(ddataFromConnector, oparamin, out ddataFromConnector, out oparaminout);
                                oparamin = oparaminout;
                            }
                        }
                    }
                    end = DateTime.Now;

                    LOG.Debug("ServiceManager.Verify -- Time of Verify = " + (end - start).Milliseconds + " milisegundos");

                    //Interpreto resultado y armo salida
                    if (iErr != Errors.IERR_OK)
                    {
                        msg = "Error verificando typeid/valueid = " +
                                   oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid + "]";
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        DatabaseHelper.SaveTx(iErr, operationsource, oparamin, null,
                                              start, end, out oparamOutVerify, out txcreated, out identity);
                        paramout.PersonalData = FillPersonalData(oparamin, identity);
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        return iErr;
                    }
                    else
                    {
                        //                            end = DateTime.Now;
                        LOG.Debug("ServiceManager.Verify -- DatabaseHelper.SaveTx in...");
                        //I.- Grabo transaccion en log para auditorias
                        iErr = DatabaseHelper.SaveTx(Errors.IERR_OK, operationsource, oparamin,
                                                     xmlparamverifyout, start, end,
                                                     out oparamOutVerify, out txcreated, out identity);
                        //Si no graba, devuelve error
                        if (iErr != Errors.IERR_OK)
                        {
                            msg = "No pudo grabar la BpTx";
                            LOG.Warn(msg);
                            paramout.Message = msg;
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            return iErr;
                        } //Sino, sigo
                        LOG.Debug("ServiceManager.Verify -- DatabaseHelper.SaveTx out >>    txcreated != null : " + (txcreated != null).ToString());
                        if (txcreated != null)
                        {
                            LOG.Debug("ServiceManager.Verify -- DatabaseHelper.SaveTx out >>    txcreated.BpOrigin != null : " + (txcreated.BpOrigin != null).ToString());
                        }
                        LOG.Debug("ServiceManager.Verify -- DatabaseHelper.SaveTx out >>    oparamOutVerify != null : " + (oparamOutVerify != null).ToString());

                        //II.- Grabo Save Data si aplica (Si es 0 => toma el default
                        int isave = oparamin.SaveVerified != 0
                                        ? oparamin.SaveVerified
                                        : Global.CONFIG.SaveVerified;
                        LOG.Debug("ServiceManager.Verify -- HAbilita Auditoria [1-Si | 0-no] => isave = " + isave.ToString());
                        //Si es = 1 => Se graba
                        if (isave == 1)
                        {
                            LOG.Debug("ServiceManager.Verify -- DatabaseHelper.SaveDataVerified in...");
                            //Si da error, lo informo pero no bloqueo
                            int ierrsave = DatabaseHelper.SaveDataVerified(oparamin, txcreated);
                            if (ierrsave != Errors.IERR_OK)
                            {
                                msg = "Problemas para grabar SaveVerified [" + ierrsave.ToString() + "]";
                                LOG.Warn(msg);
                                paramout.Message = paramout.Message + " - " + msg;
                            }
                            LOG.Debug("ServiceManager.Verify -- DatabaseHelper.SaveDataVerified OK");
                            LOG.Debug("ServiceManager.Verify -- DatabaseHelper.SaveDataVerified out >>    txcreated != null : " + (txcreated != null).ToString());
                            LOG.Debug("ServiceManager.Verify -- DatabaseHelper.SaveDataVerified out >>    oparamOutVerify != null : " + (oparamOutVerify != null).ToString());  
                        }
                        

                        //III.- Enrolo si pide enrolar ante verificación positiva en remoto
                        int ienroll = oparamin.InsertOption != 0
                                          ? oparamin.InsertOption
                                          : Global.CONFIG.InsertOption;
                        LOG.Debug("ServiceManager.Verify -- ienroll = " + ienroll.ToString());
                        //Enrola si es = 1 y verifico remoto (externo)              y el resultado positivo 
                        //&& DatabaseHelper.CumpleFiltroToEnroll(oparamin, out oXmlAux)) => Ver si se agrega control de Add BIRs
                        if (ienroll == 1 && operationsource == Source.SOURCE_REMOTE && txcreated.Result == 1)
                        {
                            LOG.Debug("ServiceManager.Verify -- ServicesManager.EnrollAfterRemoteVerification in...");
                            //Si da error, lo informo pero no bloqueo
                            int ierrenroll = ServicesManager.EnrollAfterRemoteVerification(oparamin, txcreated);
                            if (ierrenroll != Errors.IERR_OK)
                            {
                                msg = "Problemas para Enrolar luego de verificación remota en ConnectorId = " +
                                    oparamin.Verifybyconnectorid + " [" + ierrenroll.ToString() + "]";
                                LOG.Warn(msg);
                                paramout.Message = paramout.Message + " - " + msg;
                            }
                            LOG.Debug("ServiceManager.Verify -- ServicesManager.EnrollAfterRemoteVerification OK");
                            LOG.Debug("ServiceManager.Verify -- DatabaseHelper.EnrollAfterRemoteVerification out >>    txcreated != null : " + (txcreated != null).ToString());
                            LOG.Debug("ServiceManager.Verify -- DatabaseHelper.EnrollAfterRemoteVerification out >>    oparamOutVerify != null : " + (oparamOutVerify != null).ToString());  
                        }

                        //IV.- Armo salida y retorno
                        //Si ademas de verify pide get, lleno PersonalData con Identity
                        LOG.Debug("ServiceManager.Verify -- FillPersonalData in...");
                        pdout = FillPersonalData(oparamin, identity);
                        LOG.Debug("ServiceManager.Verify -- FillPersonalData out >>    txcreated != null : " + (txcreated != null).ToString());
                        LOG.Debug("ServiceManager.Verify -- FillPersonalData out >>    oparamOutVerify != null : " + (oparamOutVerify != null).ToString());  


                        LOG.Debug("ServiceManager.Verify -- XmlParamOut.GetXML in...");
                        LOG.Debug("ServiceManager.Verify -- XmlParamOut.GetXML   Param...");
                        LOG.Debug("ServiceManager.Verify -- XmlParamOut.GetXML      oparamin != null : " + (oparamin != null).ToString());
                        LOG.Debug("ServiceManager.Verify -- XmlParamOut.GetXML      txcreated != null : " + (txcreated != null).ToString());
                        if (txcreated != null)
                        {
                            LOG.Debug("ServiceManager.Verify -- XmlParamOut.GetXML      txcreated.BpOrigin != null : " + (txcreated.BpOrigin != null).ToString());
                        }
                        LOG.Debug("ServiceManager.Verify -- XmlParamOut.GetXML      oparamOutVerify != null : " + (oparamOutVerify != null).ToString());
                        LOG.Debug("ServiceManager.Verify -- XmlParamOut.GetXML      ddataFromConnector != null : " + (ddataFromConnector != null).ToString());
                        LOG.Debug("ServiceManager.Verify -- XmlParamOut.GetXML      pdout != null : " + (pdout != null).ToString());
                        LOG.Debug("ServiceManager.Verify -- XmlParamOut.GetXML      end : " + end.ToString());

                        
                        xmlparamout = XmlParamOut.GetXML(oparamin.Actionid, txcreated.Trackid,
                                           oparamin.Authenticationfactor, oparamin.Minutiaetype,
                                           Convert.ToInt32(oparamOutVerify.GetValue("bodypart")),
                                           Convert.ToInt32(oparamOutVerify.GetValue("result")),
                                           Convert.ToDouble(oparamOutVerify.GetValue("score")),
                                           Convert.ToDouble(oparamOutVerify.GetValue("threshold")),
                                           oparamin.Matchingtype, oparamin.Clientid, oparamin.Ipenduser,
                                           oparamin.Enduser, oparamin.Companyid, oparamin.Userid,
                                           Convert.ToInt32(oparamOutVerify.GetValue("companyidenroll")),
                                           Convert.ToInt32(oparamOutVerify.GetValue("useridenroll")),
                                           pdout, end, txcreated.BpOrigin.Id, ddataFromConnector,
                                           (string)oparamOutVerify.GetValue("verificationsource"),
                                           (string)oparamOutVerify.GetValue("BioSignature"));
                        LOG.Debug("ServiceManager.Verify -- End OK!");
                        return Errors.IERR_OK;
                    }
                }
                //}

            }
            catch (Exception ex)
            {
                msg = "ServiceManager.Verify Exception [" + ex.Message + "]";
                LOG.Warn(msg, ex);
                paramout.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                return Errors.IERR_UNKNOWN;
            }
            LOG.Debug("ServiceManager.Verify Out!");
        }


        private static int VerifyBioSignature(XmlParamIn oparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut paramout = new XmlParamOut();
            string msg;
            int iErr = 0;
            DateTime start, end;
            BpTx txcreated;

            try
            {
                start = DateTime.Now;

                //Parseo Biosignature
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = false;
                xmlDoc.LoadXml(oparamin.Additionaldata);

                if (xmlDoc == null)
                {
                    msg = "ServiceManager.VerifyBioSignature - Parseo BioSignature null";
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    return Errors.IERR_BAD_PARAMETER;
                }
                else
                {
                    string trackid = GetTrackIdfromXML(xmlDoc, out msg);
                    if (String.IsNullOrEmpty(trackid)) {
                        msg = "ServiceManager.VerifyBioSignature - TrackId en BioSignature null [" + msg + "]";
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        return Errors.IERR_BAD_PARAMETER;
                    }
                    string xmlbiosignature;
                    iErr = Bio.Portal.Server.Services.Core.Database.DatabaseHelper.GetBioSignature(trackid, out xmlbiosignature);

                    if (iErr != Bio.Core.Constant.Errors.IERR_OK || String.IsNullOrEmpty(xmlbiosignature)) {
                        msg = "ServiceManager.VerifyBioSignature - Error recuperando BioSignature [null o iErr=" + iErr + "]";
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        return Errors.IERR_BAD_PARAMETER;
                    }

                    //Parseo nBioSignature recuperado de BD
                    XmlDocument xmlDocBD = new XmlDocument();
                    xmlDocBD.PreserveWhitespace = false;
                    xmlDocBD.LoadXml(xmlbiosignature);

                    string msgErr;
                    bool isBioSignatureOK;
                    iErr = Global.BIOSIGNATURE_FACTORY.VerifySignature(oparamin.Additionaldata, out isBioSignatureOK, out msgErr);

                    if (xmlDocBD.OuterXml.Equals(xmlDoc.OuterXml) && isBioSignatureOK)
                    {  //Es igual el xml enviado y el recuperado desde BD y ademas la firma está ok
                        msg = "ServiceManager.VerifyBioSignature - BioSignature OK";
                        LOG.Debug(msg);
                        paramout.Message = msg; 
                        end = DateTime.Now;
                        iErr = DatabaseHelper.SaveTx(Errors.IERR_OK, Bio.Core.Matcher.Constant.OperationOrder.OPERATIONORDER_LOCALONLY, 
                                                    oparamin, isBioSignatureOK, start, end, out txcreated);
                        paramout.Result = 1;  //Verify OK
                        xmlparamout = XmlParamOut.GetXML(oparamin.Actionid, txcreated.Trackid,
                                           oparamin.Authenticationfactor, oparamin.Minutiaetype,
                                           0,1,0,0,0, oparamin.Clientid, oparamin.Ipenduser,
                                           oparamin.Enduser, oparamin.Companyid, oparamin.Userid,
                                           0,0,null, end, txcreated.BpOrigin.Id, null,
                                           "LOCAL", "");
                        //xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    } else {
                        msg = "ServiceManager.VerifyBioSignature - BioSignature Inválida [" + 
                            (isBioSignatureOK?"Firma distinta entre parámetro y BD":"BioSignature fue modificada" + 
                            " [iErr=" + iErr + "]");
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        end = DateTime.Now;
                        iErr = DatabaseHelper.SaveTx(iErr, Bio.Core.Matcher.Constant.OperationOrder.OPERATIONORDER_LOCALONLY,
                                                    oparamin, isBioSignatureOK, start, end, out txcreated);
                        paramout.Result = 2;  //Verify NOOK
                        xmlparamout = XmlParamOut.GetXML(oparamin.Actionid, txcreated.Trackid,
                                           oparamin.Authenticationfactor, oparamin.Minutiaetype,
                                           0, 2, 0, 0, 0, oparamin.Clientid, oparamin.Ipenduser,
                                           oparamin.Enduser, oparamin.Companyid, oparamin.Userid,
                                           0, 0, null, end, txcreated.BpOrigin.Id, null,
                                           "LOCAL", "");
                        //xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    }
                    return Errors.IERR_OK;   
                }
            }
            catch (Exception ex)
            {
                msg = "ServiceManager.Verify Exception [" + ex.Message + "]";
                LOG.Warn(msg, ex);
                paramout.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                return Errors.IERR_UNKNOWN;
            }
            return iErr;
            LOG.Debug("VerifyBioSignature.Verify Out!");
        }

        private static string GetTrackIdfromXML(XmlDocument xmlDoc, out string msg)
        {
            msg = null;
            string retTrackid = null;
            try
            {
                string rd = xmlDoc.InnerText;
                rd = rd.Replace("&gt;", ">").Replace("&lt;", "<");
                int s = rd.IndexOf("<TrackID>");
                int e = rd.IndexOf("</TrackID>");
                retTrackid = rd.Substring(s + 9, e - s - 9);
                //xmlDoc.InnerText.Substring()
                //if ()
                //xmlDoc.GetElementsByTagName("TrackID")[0].InnerText
            }
            catch (Exception ex)
            {
               msg = "ServiceManager.GetTrackIdfromXML Exception [" + ex.Message + "]";
               LOG.Warn(msg, ex);
               retTrackid = null;
            }
            return retTrackid;
        }

#endregion Verify Region

#region Identify Region

        /// <summary>
        /// Identificacion de una persona
        /// </summary>
        /// <param name="oparamin">Parametros de entrada</param>
        /// <param name="xmlparamout">Parametros de salida</param>
        /// <returns>codigo de operacion</returns>
        static public int Identify(XmlParamIn oparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut paramout = new XmlParamOut();
            string msg;
            int iErr;
            DateTime start, end;
            BpTx txcreated;
            Parameters oparamOutIdentify;
            int operationsource = Source.SOURCE_LOCAL;
            int operationOrder = OperationOrder.OPERATIONORDER_LOCALONLY;
            bool toRemote = false;
            BpIdentity identity = null;
            DynamicData ddataFromConnector = null;
            string xmlparamidentifyout = null;
            PersonalData pd = null;

            try
            {
                LOG.Debug("ServiceManager.Identify -- XmlParamIn");
                LOG.Debug(XmlUtils.SerializeObject<XmlParamIn>(oparamin));

                //Added 29-04-2016
                //Si debe chequear serial de device, se fija en tabla bp_devices para esa compañia si esá y está habilitado.
                if (Global.CONFIG.CheckClientConnected == 1)
                {
                    //Added 29-04-2016
                    //Para control de cliente via IP y no x Serial ID del lector
                    if (Global.CONFIG.AutoRegisterClientConnected == 2)
                    {
                        oparamin.Clientid = oparamin.Ipenduser;
                    }

                    if (!ClientAuthorized(oparamin, out msg))
                    {
                        msg = "Device no habilitado en la plataforma! [Serial ID = " + oparamin.Clientid + " - " +
                                                                       "(Msg=" + msg + ")]";
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        return Errors.IERR_CLIENT_NOT_AUTHORIZED;
                    }
                }

                start = DateTime.Now;
                //1.- Genero Matcher, Extractor y Template correspondioente de acuerdo a AF y MT. 
                //    Esto lo hago clonando desde el MatcherManager global. 
                MatcherInstance matcherintance =
                    Global.MATCHER_MANAGER.GetMatcherInstance(oparamin.Authenticationfactor,
                                                              oparamin.Minutiaetype);

                //    1.1.- Si no existe retorno con error.
                if (matcherintance == null)
                {
                    msg = "No existe Matcher configurado para esta tecnología y minucia [" +
                                       "AF=" + oparamin.Authenticationfactor + "-MT=" +
                                       oparamin.Minutiaetype + "]";
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(Errors.IERR_MATCHER_NOT_AVAILABLE,
                                          OperationOrder.OPERATIONORDER_LOCALONLY, oparamin, null,
                                          start, end, out oparamOutIdentify, out txcreated, out identity);
                    return Errors.IERR_MATCHER_NOT_AVAILABLE;
                }
                LOG.Debug("ServiceManager.Identify Instanciado matcherinstance = " + matcherintance.Name);

                //2.- Genero template correspondiente desde el sample que viene en Input.
                //    2.1.- Genero template desde Input
                //    2.2.- Llamo a Extractor con el template generado y este retorna el mismo
                //          template si ya es minucia, o si es sample, lo transforma de acuerdo a
                //          como lo entiende cada algoritmo, y genera el template con las minucias 
                //          correspondientes a ese template.
                IExtractor extractor = matcherintance.Extractor;
                ITemplate templateCurrent = matcherintance.Template;
                templateCurrent.AuthenticationFactor = oparamin.Authenticationfactor;
                templateCurrent.MinutiaeType = oparamin.SampleCollection[0].Minutiaetype;
                templateCurrent.SetData = oparamin.SampleCollection[0].Data;
                templateCurrent.AdditionalData = oparamin.Additionaldata;
                ITemplate templateCurrentExtracted;
                //extractor.Threshold = extractor.Threshold != oparamin. No viene el THExtractor, ver si lo agrego
                LOG.Debug("ServiceManager.Identify extractor.Extract in...");
                iErr = extractor.Extract(templateCurrent, TemplateDestination.TEMPLATEDESTINATION_TOIDENTIFY,
                                         out templateCurrentExtracted);
                //Si hay error retorno
                if (iErr != 0)
                {
                    msg = "Error extrayendo minucias [" + iErr + "]";
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(Errors.IERR_EXTRACTING, OperationOrder.OPERATIONORDER_LOCALONLY,
                                          oparamin, null, start, end, out oparamOutIdentify, out txcreated, out identity);
                    return Errors.IERR_EXTRACTING;
                }
                LOG.Debug("ServiceManager.Identify extractor.Extract out...");

                ////3.- De acuerdo al tipoid/valueid, y tipo de verificación pedida (si es contra un 
                ////    bodypart o todos, etc, recupero la data biométrica con BioPortal.Server.Common.Database
                //if (oparamin.PersonalData == null ||
                //    oparamin.PersonalData.Typeid == null || oparamin.PersonalData.Valueid == null)
                //{
                //    msg = "No existe typeid/valueidpara determinar identidad de verificacion";
                //    LOG.Warn(msg);
                //    paramout.Message = msg;
                //    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                //    end = DateTime.Now;
                //    DatabaseHelper.SaveTx(Errors.IERR_BAD_PARAMETER, OperationOrder.OPERATIONORDER_LOCALONLY,
                //                          oparamin, null, start, end, out oparamOutIdentify, out txcreated);
                //    return Errors.IERR_BAD_PARAMETER;
                //}
                //else
                //{
                List<BpBir> listBirs = null;
                LOG.Debug("ServiceManager.Identify DatabaseHelper.GetBirs in...");
                oparamin.PersonalData = null; //Elimino para tomar todas las uellas de esa minucia, sino filtra por typeid/valueid
                iErr = DatabaseHelper.GetBirs(oparamin, out listBirs);
                //Si hay error informo y salgo
                if (iErr != Errors.IERR_OK)
                {
                    msg = "Problemas para conseguir muestras con que comparar";
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(iErr, OperationOrder.OPERATIONORDER_LOCALONLY, oparamin, null,
                                          start, end, out oparamOutIdentify, out txcreated, out identity);
                    return iErr;
                }
                else if (listBirs == null || listBirs.Count == 0)
                {
                    msg = "No existen muestras con que comparar para [" +
                                   "AF=" + oparamin.Authenticationfactor + "-MT=" +
                                   oparamin.Minutiaetype;
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(Errors.IERR_BIR_NOT_FOUND, OperationOrder.OPERATIONORDER_LOCALONLY,
                                          oparamin, null, start, end, out oparamOutIdentify, out txcreated, out identity);
                    return Errors.IERR_BIR_NOT_FOUND;
                }
                else //Sino preparo el proceso de identificación
                {
                    LOG.Debug("ServiceManager.Identify DatabaseHelper.GetBirs Out [listBirs.Count = " + 
                        listBirs.Count.ToString() + "]");

                    LOG.Debug("ServiceManager.Identify Set matcher in...");
                    //Instancio Matcher
                    IMatcher matcher = matcherintance.Matcher;
                    //Seteo los parametros de umbrales diferentes si hay diferencias
                    matcher.Threshold = oparamin.Threshold != 0
                                            ? oparamin.Threshold
                                            : matcher.Threshold;
                    matcher.MatchingType = oparamin.Matchingtype != 0
                                               ? oparamin.Matchingtype
                                               : matcher.MatchingType;
                    if (matcher.MatchingType != 1 && matcher.MatchingType != 2)
                    {
                        matcher.MatchingType = 1;
                    }
                    LOG.Debug("ServiceManager.Identify Set matcher out.");
                    //Preparo xml de parametros para llamar al Matcher
                    Parameters parameters = new Parameters();
                    parameters.AddValue("currentTemplate", templateCurrentExtracted.GetData);
                    parameters.AddValue("additionalData", oparamin.Additionaldata);

                    string xmltemplates = null;
                    LOG.Debug("ServiceManager.Identify DatabaseHelper.ConvertListBirsToListTemplate in...");
                    iErr = DatabaseHelper.ConvertListBirsToListTemplate(listBirs, out xmltemplates);
                    if (iErr == 0)
                    {
                        LOG.Debug("ServiceManager.Identify DatabaseHelper.ConvertListBirsToListTemplate out");
                        parameters.AddValue("listBaseTemplates", xmltemplates);
                    }
                    else
                    {
                        msg = "Error serializando lista templates para verificar";
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        end = DateTime.Now;
                        DatabaseHelper.SaveTx(Errors.IERR_SERIALIZING_DATA, OperationOrder.OPERATIONORDER_LOCALONLY,
                                              oparamin, null, start, end, out oparamOutIdentify, out txcreated, out identity);
                        return Errors.IERR_SERIALIZING_DATA;
                    }

                    //Llamo a Matcher Verify
                    LOG.Debug("ServiceManager.Identify Parameters.SerializeToXml(parameters) in...");
                    string xmlparamvidentify = Parameters.SerializeToXml(parameters);
                    LOG.Debug("ServiceManager.Identify Parameters.SerializeToXml(parameters) out");

                    start = DateTime.Now;

                    //// //TO-DO : Hacer que uno u otro segun sea OperationOrder
                    //if (oparamin.OperationOrder == OperationOrder.OPERATIONORDER_REMOTEONLY)
                    //{
                    //    operationsource = OperationOrder.OPERATIONORDER_REMOTEONLY;
                    //    //TO-DO
                    //    //iErr = connector.Verify(xmlparamverify, out xmlparamverifyout);
                    //    //Por ahora pongo lo mismo ---- SACAR
                    //    iErr = matcher.Identify(xmlparamvidentify, out xmlparamidentifyout);
                    //    //Por ahora pongo lo mismo ---- SACAR
                    //}
                    //else //Sino Local en esta primera version 
                    //{
                    //    operationsource = OperationOrder.OPERATIONORDER_LOCALONLY;
                    //    iErr = matcher.Identify(xmlparamvidentify, out xmlparamidentifyout);
                    //}

                    //0. Determino cual es la operation order entre param y global
                    //1.-Si es solo local => Local
                    //2.-Si es solo remoto o 1local2remoto y fallo => Remoto

                    if (oparamin.OperationOrder > 0)
                    {
                        operationOrder = oparamin.OperationOrder;
                    }
                    LOG.Debug("ServiceManager.Identify operationOrder=" + operationOrder.ToString());

                    
                    //Si es local solo o primero local, chequeo 
                    if (operationOrder == OperationOrder.OPERATIONORDER_LOCALFIRST ||
                        operationOrder == OperationOrder.OPERATIONORDER_LOCALONLY)
                    {
                        LOG.Debug("ServiceManager.Identify Local in...");

                        operationsource = Source.SOURCE_LOCAL;
                        LOG.Debug("ServiceManager.Identify matcher.Identify in...");
                        iErr = matcher.Identify(xmlparamvidentify, out xmlparamidentifyout);
                        LOG.Debug("ServiceManager.Identify matcher.Identify out");

                        //Si es localfirst, debo chequear si fallo para ir a remote
                        if (operationOrder == OperationOrder.OPERATIONORDER_LOCALFIRST)
                        {
                            if (iErr == Errors.IERR_OK)
                            {
                                LOG.Debug("ServiceManager.Identify Parameters.DeserializeFromXml(xmlparamidentifyout) in...");
                                Parameters oparamoutverify = Parameters.DeserializeFromXml(xmlparamidentifyout);
                                int result = Convert.ToInt32(oparamoutverify.GetValue("result"));
                                if (result == 2) //Fue negativo 
                                {
                                    toRemote = true;  //Mando a remoto
                                }
                                LOG.Debug("ServiceManager.Identify Parameters.DeserializeFromXml(xmlparamidentifyout) out");
                            }
                        }
                        LOG.Debug("ServiceManager.Identify Local out");
                    }

                    //Si es solo remoto o bien fallo en Localfirst
                    if (operationOrder == OperationOrder.OPERATIONORDER_REMOTEONLY || toRemote)
                    {
                        LOG.Debug("ServiceManager.Identify Reemote in...");
                        operationsource = Source.SOURCE_REMOTE;

                        //i.-Tomo instancia de conector que viene informado, si no esta, devuelvo error
                        string conxid = oparamin.Verifybyconnectorid == null ||
                                         oparamin.Verifybyconnectorid.Trim().Length == 0
                                             ? Global.CONFIG.ConnectorDefault
                                             : oparamin.Verifybyconnectorid;
                        LOG.Debug("ServiceManager.Identify Reemote ConnectorId = " + conxid);

                        LOG.Debug("ServiceManager.Identify Get Connector Instance...");
                        ConnectorInstance connectorintance =
                            Global.CONNECTOR_MANAGER.GetConnectorInstance(conxid);
                        //    1.1.- Si no existe retorno con error.
                        if (connectorintance == null)
                        {
                            msg = "No existe Connector configurado para este ConnectorId [" +
                                                            oparamin.Verifybyconnectorid + "]";
                            LOG.Warn(msg);
                            paramout.Message = msg;
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            end = DateTime.Now;
                            DatabaseHelper.SaveTx(Errors.IERR_CONNECTOR_NOT_AVAILABLE,
                                                  operationOrder, oparamin, null,
                                                  start, end, out oparamOutIdentify, out txcreated, out identity);
                            return Errors.IERR_CONNECTOR_NOT_AVAILABLE;
                        }
                        LOG.Debug("ServiceManager.Identify Get Connector Instanced!");

                        IConnector connector = connectorintance.Connector;
                        string strxmlout;
                        string strxmlin = XmlUtils.SerializeObject(oparamin);
                        
                        LOG.Debug("ServiceManager.Identify connector.Verify in...");
                        iErr = connector.Verify(strxmlin, out strxmlout);
                        LOG.Debug("ServiceManager.Identify connector.Verify out");

                        //Si viene error informo
                        if (iErr != Errors.IERR_OK)
                        {
                            msg = "Error identificando en ConnectorId = " + oparamin.Verifybyconnectorid;

                            LOG.Warn(msg);
                            paramout.Message = msg;
                            end = DateTime.Now;
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            DatabaseHelper.SaveTx(iErr, operationsource, oparamin, null,
                                                  start, end, out oparamOutIdentify, out txcreated, out identity);
                            return iErr;
                        }
                        else //Si no hay error, armo salida
                        {
                            /* Viene esta data:
                                oXmlout.SetValue("message", "");
                                oXmlout.SetValue("status", iretremoto.ToString());
                                oXmlout.AddValue("trackid", oXmlParamOut.Trackid);
                                oXmlout.AddValue("result", oXmlParamOut.Result.ToString());
                                oXmlout.AddValue("score", oXmlParamOut.Score.ToString());
                                oXmlout.AddValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
                            */
                            LOG.Debug("ServiceManager.Identify DynamicData.DeserializeFromXml(strxmlout) in...");
                            ddataFromConnector = DynamicData.DeserializeFromXml(strxmlout);
                            LOG.Debug("ServiceManager.Identify DynamicData.DeserializeFromXml(strxmlout) out");

                            //Si no puede deserializar devuelve error 
                            if (ddataFromConnector == null)
                            {
                                msg = "Error deserializando respuesta de ConnectorId = " + oparamin.Verifybyconnectorid;
                                LOG.Warn(msg);
                                paramout.Message = msg;
                                end = DateTime.Now;
                                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                                DatabaseHelper.SaveTx(iErr, operationsource, oparamin, null,
                                                      start, end, out oparamOutIdentify, out txcreated, out identity);
                                return Errors.IERR_CONNECTOR_MALFORMED_ANSWER;
                            }
                            else //Sino formatea la salida como parameter para ser igaul a Matcher local, asi puede grabar SaveTx mas adelante
                            {
                                LOG.Debug("ServiceManager.Identify ServicesManager.FormatDynamicDataToParameter(ddataFromConnector, oparamin) in...");
                                //ddataFromConnector.AddValue("ConnectorId", oparamin.Verifybyconnectorid);
                                XmlParamIn oparamout;
                                xmlparamidentifyout = ServicesManager.FormatDynamicDataToParameter(ddataFromConnector, oparamin, out ddataFromConnector, out oparamout);
                                oparamin = oparamout;
                                LOG.Debug("ServiceManager.Identify ServicesManager.FormatDynamicDataToParameter(ddataFromConnector, oparamin) out");
                            }
                        }
                    }

                    end = DateTime.Now;

                    //Interpreto resultado y armo salida
                    if (iErr != Errors.IERR_OK)
                    {
                        msg = "Error identificando";
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        DatabaseHelper.SaveTx(iErr, operationsource, oparamin, null,
                                              start, end, out oparamOutIdentify, out txcreated, out identity);
                        return iErr;
                    }
                    else
                    {
                        //                            end = DateTime.Now;
                        LOG.Debug("ServiceManager.Identify DatabaseHelper.SaveTx in...");

                        //I.- Grabo transaccion en log para auditorias
                        iErr = DatabaseHelper.SaveTx(Errors.IERR_OK, operationsource, oparamin,
                                                     xmlparamidentifyout, start, end,
                                                     out oparamOutIdentify, out txcreated, out identity);
                        //Si no graba, devuelve error
                        if (iErr != Errors.IERR_OK)
                        {
                            msg = "No pudo grabar la BpTx";
                            LOG.Warn(msg);
                            paramout.Message = msg;
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            return iErr;
                        } //Sino, sigo
                        LOG.Debug("ServiceManager.Identify DatabaseHelper.SaveTx out");

                        //II.- Grabo Save Data si aplica (Si es 0 => toma el default
                        int isave = oparamin.SaveVerified != 0
                                        ? oparamin.SaveVerified
                                        : Global.CONFIG.SaveVerified;
                        LOG.Debug("ServiceManager.Identify isave = " + isave.ToString());

                        //Si es = 1 => Se graba
                        if (isave == 1)
                        {
                            LOG.Debug("ServiceManager.Identify DatabaseHelper.SaveDataVerified(oparamin, txcreated) in..." );
                            //Si da error, lo informo pero no bloqueo
                            if (DatabaseHelper.SaveDataVerified(oparamin, txcreated) != Errors.IERR_OK)
                            {
                                msg = "Problemas para grabar SaveVerified";
                                LOG.Warn(msg);
                                paramout.Message = msg;
                            } 
                            LOG.Debug("ServiceManager.Identify DatabaseHelper.SaveDataVerified(oparamin, txcreated) out");
                        }

                        //III.- Armo salida y retorno
                        //Si fue a remoto => Saco el PersonalData de ahi sino lo lleno don FillPersona...
                        LOG.Debug("ServiceManager.Identify FillPersonalData in...");
                        pd = toRemote ?
                            XmlUtils.DeserializeObject<PersonalData>((string)oparamOutIdentify.GetValue("personaldata")) :
                            DatabaseHelper.FillPersonalDataFromBpIdentity(identity);


                        LOG.Debug("ServiceManager.Identify XmlParamOut.GetXML in...");
                        xmlparamout = XmlParamOut.GetXML(Action.ACTION_IDENTIFY, txcreated.Trackid,
                                           oparamin.Authenticationfactor, oparamin.Minutiaetype,
                                           Convert.ToInt32(oparamOutIdentify.GetValue("bodypart")),
                                           Convert.ToInt32(oparamOutIdentify.GetValue("result")),
                                           Convert.ToDouble(oparamOutIdentify.GetValue("score")),
                                           Convert.ToDouble(oparamOutIdentify.GetValue("threshold")),
                                           oparamin.Matchingtype, oparamin.Clientid, oparamin.Ipenduser,
                                           oparamin.Enduser, oparamin.Companyid, oparamin.Userid,
                                           Convert.ToInt32(oparamOutIdentify.GetValue("companyidenroll")),
                                           Convert.ToInt32(oparamOutIdentify.GetValue("useridenroll")),
                                           pd, end, txcreated.BpOrigin.Id, null,
                                           (string)oparamOutIdentify.GetValue("verificationsource"),
                                           (string)oparamOutIdentify.GetValue("BioSignature"));
                        LOG.Debug("ServiceManager.Identify XmlParamOut.GetXML out");
                        return Errors.IERR_OK;
                    }
                }
                //}

            }
            catch (Exception ex)
            {
                msg = "ServiceManager.Verify Exception [" + ex.Message + "]";
                LOG.Warn(msg, ex);
                paramout.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                return Errors.IERR_UNKNOWN;
            }

        }

#endregion Identify Region

#region Enroll Region

        /// <summary>
        /// Recibe los parámetros para hacer el Enroll, y devuelve resultado
        /// de la misma, o mensaje de error si existe.
        /// Los pasos son:
        ///         1.- Genero BpIdentity desde PersonalData y la lleno con datos desde XmlParamIn. 
        ///             1.1.- Si es Enroll => Genero
        ///             1.2.- Si es Modify/AddBir => Recupero BpIdentity => Si no existe => Error 
        ///         2.- Foreach de SampleCollection en XmlParamIn
        ///             2.1.- Foreach de MatchersAvailables
        ///                   2.1.1.- Genero BIR si es factible
        ///                   2.1.2.- Si generó BIR => Agrego en BpIdentity
        ///         3.- Grabo cambios
        ///         4.- Generar respuesta en Xml.
        /// </summary>
        /// <param name="oparamin">Objeto de parametros</param>
        /// <param name="xmlparamout">XML con resultado</param>
        /// <returns>Retorna el xml de respuesta</returns>
        static public int Enroll(XmlParamIn oparamin, out string xmlparamout)
        {
            XmlParamOut paramout = new XmlParamOut();
            string msg;
            int iErr;
            DateTime start, end;
            BpTx txcreated;
            Parameters oparamOutEnroll;
            //BpIdentity con la que se trabajará
            BpIdentity oBpIdentity;
            Parameters oparamEnroll;
            MatcherInstance matcherintance;
            BpIdentity identity = null;
            Parameters oparamOutVerify = null;

            try
            {
                LOG.Debug("ServiceManager.Enroll - IN - [ActionId = " + oparamin.Actionid.ToString() + "]..."); 
                //Si es delete, deribo en Delete
                if (oparamin.Actionid == Action.ACTION_DELETE ||
                    oparamin.Actionid == Action.ACTION_DELETEBIR)
                {
                    LOG.Debug("ServiceManager.Enroll - Deriva a Delete...");
                    return Delete(oparamin, out xmlparamout);
                }

                //Added 29-04-2016
                //Si debe chequear serial de device, se fija en tabla bp_devices para esa compañia si esá y está habilitado.
                //Added 27-07-2022 - Si es  ACTION_ENROLL_FORCED [333] entonces es enroll desde POSController, no chequea el clientid
                //                   y enrola igual. SOLO USO INTERNO de Biometrika este tipo de enroll
                if (Global.CONFIG.CheckClientConnected == 1 && oparamin.Actionid != Action.ACTION_ENROLL_FORCED)
                {
                    //Added 29-04-2016
                    //Para control de cliente via IP y no x Serial ID del lector
                    if (oparamin.SampleCollection != null && oparamin.SampleCollection.Count > 0)
                    {
                        if (Global.CONFIG.AutoRegisterClientConnected == 2)
                        {
                            oparamin.Clientid = oparamin.Ipenduser;
                        }

                        if (!ClientAuthorized(oparamin, out msg))
                        {
                            msg = "Device no habilitado en la plataforma! [Serial ID = " + oparamin.Clientid + " - " +
                                                                           "(Msg=" + msg + ")]";
                            LOG.Warn(msg);
                            paramout.Message = msg;
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            return Errors.IERR_CLIENT_NOT_AUTHORIZED;
                        }
                    }
                }
                start = DateTime.Now;

                LOG.Debug("ServiceManager.Enroll - Evalua si conector externo [Verifybyconnectorid=" +
                          oparamin.Verifybyconnectorid == null
                              ? "NULL"
                              : oparamin.Verifybyconnectorid);
                //Si Verifybyconnectorid!= null y no viene con bloqueador (final)
                //=> verifico antes de enroll
                if (oparamin.Verifybyconnectorid != null &&
                    !oparamin.Verifybyconnectorid.Trim().ToUpper().Equals("FINAL"))
                {
                    //i.-Tomo instancia de conector que viene informado, si no esta, devuelvo error
                    string conxid = oparamin.Verifybyconnectorid.Trim().Length == 0
                                                ? Global.CONFIG.ConnectorDefault
                                                : oparamin.Verifybyconnectorid;
                    LOG.Debug("ServiceManager.Enroll - Tomando Connector Instance desde CONNECTOR_MANAGER con conxid = " + conxid);
                    ConnectorInstance connectorintance =
                        Global.CONNECTOR_MANAGER.GetConnectorInstance(conxid);
                    //    1.1.- Si no existe retorno con error.
                    if (connectorintance == null)
                    {
                        msg = "No existe Connector configurado para este ConnectorId [" +
                                                        oparamin.Verifybyconnectorid + "]";
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        end = DateTime.Now;
                        DatabaseHelper.SaveTx(Errors.IERR_CONNECTOR_NOT_AVAILABLE,
                                              OperationOrder.OPERATIONORDER_DEFAULT, oparamin, null,
                                              start, end, out oparamOutVerify, out txcreated, out identity);
                        return Errors.IERR_CONNECTOR_NOT_AVAILABLE;
                    }

                    IConnector connector = connectorintance.Connector;
                    string strxmlout;
                    XmlParamIn pin = (XmlParamIn)oparamin.Clone();
                    pin.Actionid = Action.ACTION_VERIFY;
                    //Esto sacar o ver si se saca, ahora solo para que funque porque en remoto es int.
                    //pin.Verifybyconnectorid = "0";
                    pin.InsertOption = 2; //No Enroll

                    string strxmlin = XmlUtils.SerializeObject(pin);
                    LOG.Debug("ServiceManager.Enroll - Verificando en Connector externo con strxmlin = " + strxmlin);
                    iErr = connector.Verify(strxmlin, out strxmlout);
                    LOG.Debug("ServiceManager.Enroll - Salio de Connector externo con strxmlout = " + strxmlout);
                    //Si viene error informo
                    if (iErr != Errors.IERR_OK)
                    {
                        msg = "Error verificando typeid/valueid = [" +
                              oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid + "] en " +
                              "ConnectorId = " + oparamin.Verifybyconnectorid;
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        end = DateTime.Now;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        DatabaseHelper.SaveTx(iErr, OperationOrder.OPERATIONORDER_DEFAULT, oparamin, null,
                                              start, end, out oparamOutVerify, out txcreated, out identity);
                        return iErr;
                    }
                    else
                    {
                        //Si es ERR_OK => Chequeo que result = 1
                        DynamicData ddata = XmlUtils.DeserializeObject<DynamicData>(strxmlout);
                        int result = Convert.ToInt32(ddata.GetValue("result"));
                        if (result == 2) //Fue negativo => Grabo y salgo 
                        {
                            msg = "Error verificando typeid/valueid = [" +
                              oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid + "] en " +
                              "ConnectorId = " + oparamin.Verifybyconnectorid + " - Enroll cancelado";
                            LOG.Warn(msg);
                            paramout.Message = msg;
                            end = DateTime.Now;
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            DatabaseHelper.SaveTx(Errors.IERR_VERIFY, OperationOrder.OPERATIONORDER_DEFAULT, oparamin, null,
                                                  start, end, out oparamOutVerify, out txcreated, out identity);
                            return Errors.IERR_VERIFY;
                        }
                    }
                }

                LOG.Debug("ServiceManager.Enroll - DatabaseHelper.GetIdentity con Company/TypeId/ValueId = " +
                          oparamin.Companyid.ToString() + "/" + oparamin.PersonalData.Typeid + "/" +
                          oparamin.PersonalData.Valueid);
                // 1.- Genero BpIdentity desde PersonalData y la lleno con datos desde XmlParamIn. 
                //     1.1.- Si es Enroll => Genero
                //     1.2.- Si es Modify/AddBir => Recupero BpIdentity => Si no existe => Error 
                iErr = DatabaseHelper.GetIdentity(true, oparamin.Actionid, oparamin.Companyid,
                                                  oparamin.PersonalData, out oBpIdentity, true);

                //Si da error retorno con error
                if (iErr != Errors.IERR_OK)
                {
                    msg = "ServiceManager.Enroll - Problemas para crear u obtener una identidad (typeid/valueid = " +
                        oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid + ")";
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(iErr, OperationOrder.OPERATIONORDER_LOCALONLY, oparamin, null,
                                          start, end, out oparamEnroll, out txcreated, out identity);
                    return iErr;
                }


                LOG.Debug("ServiceManager.Enroll - Procesando SampleCollection.Count = " +
                          (oparamin.SampleCollection != null ? oparamin.SampleCollection.Count.ToString() : "Null"));
                // 2.- Foreach de SampleCollection en XmlParamIn
                //     2.1.- Foreach de MatchersAvailables
                //           2.1.1.- Genero BIR si es factible
                //           2.1.2.- Si generó BIR => Agrego en BpIdentity
                if (oparamin.SampleCollection != null && oparamin.SampleCollection.Count > 0)
                {
                    //Genero Matcher, Extractor y Template correspondioente de acuerdo a AF y MT. 
                    //Esto lo hago clonando desde el MatcherManager global. Esto lo hago si hay 
                    //samples para agregar
                    Hashtable htMa = Global.MATCHER_MANAGER.GetMatchersAvailables();
                    LOG.Debug("ServiceManager.Enroll - MatchersDisponibles => htMa != null = " + (htMa != null).ToString());
                    if (htMa == null || htMa.Count == 0)
                    {
                        msg = "No existen Matchers disponibles para enrolamiento]";
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        end = DateTime.Now;
                        DatabaseHelper.SaveTx(Errors.IERR_MATCHER_NOT_AVAILABLE,
                                              OperationOrder.OPERATIONORDER_LOCALONLY, oparamin, null,
                                              start, end, out oparamEnroll, out txcreated, out identity);
                        return Errors.IERR_MATCHER_NOT_AVAILABLE;
                    }

                    //2.1.1.- Genero BIR si es factible
                    MatcherInstance mi = null;
                    IExtractor extractor;
                    ITemplate templateCurrent = null;
                    ITemplate templateCurrentExtracted = null;
                    bool _birAdded = false;
                    bool _descarto = false;
                    foreach (Sample sample in oparamin.SampleCollection)
                    {
                        LOG.Debug("ServiceManager.Enroll - Procesando sample => MT:" + sample.Minutiaetype.ToString() +
                                        "/BP:" + sample.BodyPart.ToString());
                        foreach (DictionaryEntry made in htMa)
                        {
                            try
                            {
                                try
                                {
                                    _descarto = false;
                                    mi = (MatcherInstance)made.Value;
                                    LOG.Debug("ServiceManager.Enroll - Procesan sample con Matcher = " + mi.Name);
                                    extractor = mi.Extractor;
                                    iErr = 0;
                                    if (mi.Authenticationfactor == oparamin.Authenticationfactor)
                                    {
                                        LOG.Debug("ServiceManager.Enroll Entrando a setear templateCurrent para extract...");
                                        templateCurrent = null;
                                        templateCurrent = mi.Template;
                                        templateCurrent.AuthenticationFactor = oparamin.Authenticationfactor;
                                        templateCurrent.MinutiaeType = sample.Minutiaetype;
                                        templateCurrent.SetData = sample.Data;
                                        templateCurrent.AdditionalData = sample.Additionaldata;
                                        templateCurrent.BodyPart = (sample.BodyPart <= 0 ? oparamin.Bodypart : sample.BodyPart);
                                        templateCurrentExtracted = null;
                                        LOG.Debug("ServiceManager.Enroll Entrando a extractor.Extract...");
                                        iErr = extractor.Extract(templateCurrent, TemplateDestination.TEMPLATEDESTINATION_TOENROLL,
                                                                 out templateCurrentExtracted);
                                        LOG.Debug("ServiceManager.Enroll - Salida Extractor = " + iErr.ToString());
                                    } else
                                    {
                                        _descarto = true;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    iErr = -1111;
                                    LOG.Error("ServiceManager.Enroll Error usando extractor.Extract...[iErr = " + iErr + " - MsgErr=" + ex.Message + "]");
                                }
                                //Si extrae bien => Genero BIR y lo agrego a la identidad
                                if (iErr == Errors.IERR_OK && !_descarto)
                                {
                                    LOG.Debug("Enroll >> MI:AF=" + mi.Authenticationfactor +
                                          " - MI:MT=" + mi.Minutiaetype +
                                          " - Sample:MT=" + sample.Minutiaetype +
                                          " - Sample:BodyPart=" + templateCurrent.BodyPart +
                                          " - Extract result = " + iErr);
                                    LOG.Debug("ServiceManager.Enroll - Ingresando a DatabaseHelper.AddBirToIdentity...");
                                    iErr = DatabaseHelper.AddBirToIdentity(oBpIdentity, templateCurrentExtracted,
                                                                           oparamin.Companyid, oparamin.Userid);
                                    _birAdded = (iErr==0);
                                } else
                                {
                                    LOG.Debug("No Processed Enroll >> MI:AF=" + mi.Authenticationfactor + " != " + oparamin.Authenticationfactor + " [iErr=" + iErr + "]");    
                                 }
                            }
                            catch (Exception ex)
                            {
                                LOG.Error("ServiceManager.Enroll Error usando DatabaseHelper.AddBirToIdentity o mandando a log... [iErr = " + iErr + " - MsgErr=" + ex.Message + "]");
                            }
                        }
                    }

                    //Added 09/2018 - POr Verification source
                    // Si agrego al menos un bit a la Identity => Actualizo el verification source
                    if (_birAdded)
                    {
                        LOG.Debug("ServiceManager.Enroll - Ajustando Verification Source... Actual = " + oBpIdentity.Verificationsource);
                        //Si no viene informado => Seteo no verificado si es que no estaba de antes sino queda como verificado
                        if (String.IsNullOrEmpty(oparamin.PersonalData.Verificationsource)) {
                            oBpIdentity.Verificationsource = DatabaseHelper.SetVerificationSource(oBpIdentity.Verificationsource, oparamin.Authenticationfactor,
                                                                                                  0, "A", null);
                        } else //Si viene indicado => Update a verificado con esa fuente
                        {
                            oBpIdentity.Verificationsource = DatabaseHelper.SetVerificationSource(oBpIdentity.Verificationsource, oparamin.Authenticationfactor,
                                                                                                  1, "A", oparamin.PersonalData.Verificationsource);
                        }
                        LOG.Debug("ServiceManager.Enroll - Actualiuzado => " + oBpIdentity.Verificationsource);
                    } else
                    {
                        LOG.Debug("ServiceManager.Enroll - No Ajusta Verification Source => Ningun BIR Added...");
                    }

                }

                LOG.Debug("ServiceManager.Enroll - Salvando Identity...");
                // 3.- Grabo cambios
                iErr = DatabaseHelper.SaveIdentity(oBpIdentity);

                //Si hay error retorno
                if (iErr != 0)
                {
                    msg = "Error salvando la Identidad [" + iErr + "]";
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(iErr, OperationOrder.OPERATIONORDER_LOCALONLY,
                                          oparamin, null, start, end, out oparamEnroll, out txcreated, out identity);
                    return iErr;
                }

                LOG.Debug("ServiceManager.Enroll - Grabando Tx...");
                //I.- Grabo transaccion en log para auditorias
                end = DateTime.Now;
                 
                LOG.Debug("ServiceManager.Enroll -- Time of Enroll = " + (end - start).Milliseconds + " milisegundos");

                iErr = DatabaseHelper.SaveTx(Errors.IERR_OK, 0, oparamin, null, start, end,
                                             out oparamEnroll, out txcreated, out identity);
                //Si no graba, devuelve error
                if (iErr != Errors.IERR_OK)
                {
                    msg = "No pudo grabar la BpTx [iErr=" + iErr + "]";
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    return iErr;
                } //Sino, sigo

                //III.- Armo salida y retorno
                xmlparamout = XmlParamOut.GetXML(oparamin.Actionid, txcreated.Trackid,
                                   oparamin.Authenticationfactor, oparamin.Minutiaetype,
                                   oparamin.Bodypart, 0, 0, 0,
                                   oparamin.Matchingtype, oparamin.Clientid, oparamin.Ipenduser,
                                   oparamin.Enduser, oparamin.Companyid, oparamin.Userid,
                                   oparamin.Companyid, oparamin.Userid,
                                   oparamin.PersonalData.Typeid, oparamin.PersonalData.Valueid,
                                   end, txcreated.BpOrigin.Id, null, null);
                LOG.Debug("ServiceManager.Enroll - Retorna xmlparamout = " + xmlparamout);
                return Errors.IERR_OK;

            }
            catch (Exception ex)
            {
                msg = "ServiceManager.Enroll Exception [" + ex.Message + "]";
                LOG.Warn(msg, ex);
                paramout.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                return Errors.IERR_UNKNOWN;
            }
            LOG.Debug("ServiceManager.Enroll - OUT!");
        }

        /// <summary>
        /// Metodo disponible par aeliminación de identidad o de birs.
        /// </summary>
        /// <param name="oparamin">Parámetros de entrada</param>
        /// <param name="xmlparamout">Parámetros de salida</param>
        /// <returns></returns>
        private static int Delete(XmlParamIn oparamin, out string xmlparamout)
        {
            XmlParamOut paramout = new XmlParamOut();
            string msg;
            int iErr;
            DateTime start, end;
            BpTx txcreated;
            Parameters oparamOutEnroll;
            //BpIdentity con la que se trabajará
            BpIdentity oBpIdentity;
            Parameters oparamEnroll;
            MatcherInstance matcherintance;
            BpIdentity identity = null;

            try
            {
                start = DateTime.Now;

                // 1.- Si es DELETE => Elimino Identity
                if (oparamin.Actionid == Action.ACTION_DELETE)
                {
                    iErr = DatabaseHelper.DeleteIdentity(oparamin.Companyid, oparamin.PersonalData);
                }
                else
                {
                    iErr = Errors.IERR_ACTION_NOT_SUPPORTED;
                }

                //Si da error retorno con error
                if (iErr != Errors.IERR_OK)
                {
                    msg = "Problemas para eliminar [typeid/valueid = " +
                        oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid + "] o datos derivados";
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(iErr, OperationOrder.OPERATIONORDER_LOCALONLY, oparamin, null,
                                          start, end, out oparamEnroll, out txcreated, out identity);
                    return iErr;
                }

                //I.- Grabo transaccion en log para auditorias
                end = DateTime.Now;
                iErr = DatabaseHelper.SaveTx(Errors.IERR_OK, 0, oparamin, null, start, end,
                                                out oparamEnroll, out txcreated, out identity);

                //Si no graba, devuelve error
                if (iErr != Errors.IERR_OK)
                {
                    msg = "No pudo grabar la BpTx";
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    return iErr;
                } //Sino, sigo

                //III.- Armo salida y retorno
                xmlparamout = XmlParamOut.GetXML(oparamin.Actionid, txcreated.Trackid,
                                   oparamin.Authenticationfactor, oparamin.Minutiaetype,
                                   oparamin.Bodypart, 0, 0, 0,
                                   oparamin.Matchingtype, oparamin.Clientid, oparamin.Ipenduser,
                                   oparamin.Enduser, oparamin.Companyid, oparamin.Userid,
                                   oparamin.Companyid, oparamin.Userid,
                                   oparamin.PersonalData.Typeid, oparamin.PersonalData.Valueid,
                                   end, txcreated.BpOrigin.Id, null, null);
                return Errors.IERR_OK;

            }
            catch (Exception ex)
            {
                msg = "ServiceManager.Enroll->Delete Exception [" + ex.Message + "]";
                LOG.Error(msg, ex);
                paramout.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                return Errors.IERR_UNKNOWN;
            }

        }

#endregion Enroll Region

#region Get Region

        /// <summary>
        /// Recibe los parámetros para hacer el Get, y devuelve resultado
        /// de la misma, o mensaje de error si existe.
        /// Los pasos son:
        ///         1.- Genero BpIdentity desde PersonalData y la lleno con datos desde XmlParamIn. 
        ///             1.1.- Si es GetAll/GetFoto/GetFirma => Lleno lo que corresponda
        ///         2.- Grabo Tx
        ///         3.- Generar respuesta en Xml.
        /// </summary>
        /// <param name="oparamin">Objeto de parametros</param>
        /// <param name="xmlparamout">XML con resultado</param>
        /// <returns>Retorna el xml de respuesta</returns>
        static public int Get(XmlParamIn oparamin, out string xmlparamout)
        {
            XmlParamOut paramout = new XmlParamOut();
            string msg;
            int iErr = Errors.IERR_OK;
            DateTime start, end;
            BpTx txcreated;
            Parameters oparamOutEnroll;
            //BpIdentity con la que se trabajará
            BpIdentity oBpIdentity;
            Parameters oparamEnroll;
            MatcherInstance matcherintance;
            BpIdentity identity = null;
            int operationOrder = OperationOrder.OPERATIONORDER_LOCALONLY;
            int operationsource = Source.SOURCE_LOCAL;
            bool toRemote = false;
            DynamicData ddataFromConnector = null;
            PersonalData pdret = null;
            string xmlparamgetout = null;
            DynamicData ddataInfoBirs = null; //Para enviar info de BIRs si Action es 100 - Added 11-11-2014

            try
            {

             //Added from 29-04-2016 por Proxy
                if (oparamin.Actionid >= 200 && oparamin.Actionid <= 300)
                {
                    return GetPROXY(oparamin, out xmlparamout);
                }

                start = DateTime.Now;

                //0. Determino cual es la operation order entre param y global
                //1.-Si es solo local => Local
                //2.-Si es solo remoto o 1local2remoto y fallo => Remoto

                if (oparamin.OperationOrder > 0)
                {
                    operationOrder = oparamin.OperationOrder;
                }

                //Si es local solo o primero local, chequeo 
                if (operationOrder == OperationOrder.OPERATIONORDER_LOCALFIRST ||
                    operationOrder == OperationOrder.OPERATIONORDER_LOCALONLY)
                {
                    operationsource = Source.SOURCE_LOCAL;
                    //// 1.- Genero BpIdentity desde PersonalData y la lleno con datos desde XmlParamIn. 
                    ////     Si o si, chequeo que exista, sino retorno error
                    iErr = DatabaseHelper.GetIdentity(true, oparamin.Actionid, oparamin.Companyid, oparamin.PersonalData, out oBpIdentity);

                    //Si da error retorno con error
                    if (iErr != Errors.IERR_OK)
                    {
                        if (operationOrder == OperationOrder.OPERATIONORDER_LOCALONLY)
                        {
                            msg = "Problemas para obtener una identidad (typeid/valueid = " +
                                  oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid;
                            LOG.Warn(msg);
                            paramout.Message = msg;
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            end = DateTime.Now;
                            DatabaseHelper.SaveTx(iErr, operationOrder, oparamin, null,
                                                  start, end, out oparamEnroll, out txcreated, out identity);
                            return iErr;
                        }
                        else
                        {
                            toRemote = true;
                        }
                    }
                    else
                    {
                        //1.1.- Si es GetAll/GetFoto/GetFirma => Lleno lo que corresponda 
                        //      Viene PersonalData lleno, segun Actionid, borro lo que no piden
                        oparamin.PersonalData = DatabaseHelper.FillPersonalDataFromBpIdentity(oBpIdentity);
                        iErr = DatabaseHelper.SelectDataToReturn(oparamin.Actionid, oparamin.PersonalData, out pdret);

                        //Si da error retorno con error
                        if (iErr != Errors.IERR_OK)
                        {
                            msg = "Problemas para formatear informacion a retornar para typeid/valueid = " +
                                  oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid;
                            LOG.Warn(msg);
                            paramout.Message = msg;
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            end = DateTime.Now;
                            DatabaseHelper.SaveTx(iErr, operationOrder, oparamin, null,
                                                  start, end, out oparamEnroll, out txcreated, out identity);
                            return iErr;
                        }

                        //Si no da error seteo salida
                        oparamin.PersonalData = pdret;
                           //Si accion es 100 = GetBirs => Devuelve la info de los Birs sin datos biometricos
                           //    Added 11-11-2014
                        if (oparamin.Actionid == Action.ACTION_SYNC_GETBIRS) {
                            iErr = ServicesManager.FormatDDataInfoBirs(oBpIdentity, out ddataInfoBirs);
                            //oparamin.Additionaldata = DynamicData.SerializeToXml(ddataInfoBirs);
                        }
                    }
                }

                //Si es solo remoto o bien fallo en Localfirst
                if (operationOrder == OperationOrder.OPERATIONORDER_REMOTEONLY || toRemote) 
                {
                    operationsource = Source.SOURCE_REMOTE;

                    //i.-Tomo instancia de conector que viene informado, si no esta, devuelvo error
                    string conxid = oparamin.Verifybyconnectorid == null ||
                                            oparamin.Verifybyconnectorid.Trim().Length == 0
                                                ? Global.CONFIG.ConnectorDefault
                                                : oparamin.Verifybyconnectorid;
                    ConnectorInstance connectorintance =
                        Global.CONNECTOR_MANAGER.GetConnectorInstance(conxid);
                    //    1.1.- Si no existe retorno con error.
                    if (connectorintance == null)
                    {
                        msg = "No existe Connector configurado para este ConnectorId [" +
                                                        oparamin.Verifybyconnectorid + "]";
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        end = DateTime.Now;
                        DatabaseHelper.SaveTx(Errors.IERR_CONNECTOR_NOT_AVAILABLE,
                                              operationOrder, oparamin, null,
                                              start, end, out oparamEnroll, out txcreated, out identity);
                        return Errors.IERR_CONNECTOR_NOT_AVAILABLE;
                    }

                    IConnector connector = connectorintance.Connector;
                    string strxmlout;
                    string strxmlin = XmlUtils.SerializeObject(oparamin);
                    iErr = connector.Get(strxmlin, out strxmlout);

                    //Si viene error informo
                    if (iErr != Errors.IERR_OK)
                    {
                        msg = "Error en Get para typeid/valueid = [" +
                              oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid + "] en " +
                              "ConnectorId = " + oparamin.Verifybyconnectorid;
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        end = DateTime.Now;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        DatabaseHelper.SaveTx(iErr, operationsource, oparamin, null,
                                              start, end, out oparamEnroll, out txcreated, out identity);
                        return iErr;
                    }
                    else //Si no hay error, armo salida
                    {
                        /* Viene esta data:
                            oXmlout.SetValue("message", "");
                            oXmlout.SetValue("status", iretremoto.ToString());
                            oXmlout.AddValue("trackid", oXmlParamOut.Trackid);
                            oXmlout.AddValue("result", oXmlParamOut.Result.ToString());
                            oXmlout.AddValue("score", oXmlParamOut.Score.ToString());
                            oXmlout.AddValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
                            oXmlout.SetValue("personaldata", XmlUtils.SerializeObject(oXmlParamOut.PersonalData));
                        */
                        ddataFromConnector = DynamicData.DeserializeFromXml(strxmlout);
                        //Si no puede deserializar devuelve error 
                        if (ddataFromConnector == null)
                        {
                            msg = "Error deserializando respuesta de ConnectorId = " + oparamin.Verifybyconnectorid;
                            LOG.Warn(msg);
                            paramout.Message = msg;
                            end = DateTime.Now;
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            DatabaseHelper.SaveTx(iErr, operationsource, oparamin, null,
                                                  start, end, out oparamEnroll, out txcreated, out identity);
                            return Errors.IERR_CONNECTOR_MALFORMED_ANSWER;
                        }
                        else //Sino formatea la salida como parameter para ser igaul a Matcher local, asi puede grabar SaveTx mas adelante
                        {
                            ddataFromConnector.AddValue("ConnectorId", oparamin.Verifybyconnectorid);
                            XmlParamIn oparamout;
                            xmlparamgetout = ServicesManager.FormatDynamicDataToParameter(ddataFromConnector, oparamin, out ddataFromConnector, out oparamout);
                            oparamin = oparamout;
                            if (oparamin.PersonalData == null)
                            {
                                pdret = XmlUtils.DeserializeObject<PersonalData>((string)ddataFromConnector.GetValue("personaldata"));

                                //Si no da error seteo salida
                                oparamin.PersonalData = pdret;
                            }
                        }
                    }
                }

                //I.- Grabo transaccion en log para auditorias
                end = DateTime.Now;
                iErr = DatabaseHelper.SaveTx(Errors.IERR_OK, operationsource, oparamin, xmlparamgetout, start, end,
                                             out oparamEnroll, out txcreated, out identity);
                //Si no graba, devuelve error
                if (iErr != Errors.IERR_OK)
                {
                    msg = "No pudo grabar la BpTx";
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    return iErr;
                } //Sino, sigo

                //III.- Enrolo si pide enrolar ante get positiva en remoto o update si es
                //      GETANDMODIFY
                int ienroll = oparamin.InsertOption != 0
                                  ? oparamin.InsertOption
                                  : Global.CONFIG.InsertOption;
                //Si es = 1 => Enrola ante resultado positivo si hizo get en remoto
                if (ienroll == 1 && operationsource == Source.SOURCE_REMOTE &&
                    oparamin!= null && oparamin.PersonalData != null &&
                    !string.IsNullOrEmpty(oparamin.PersonalData.Typeid) &&
                    !string.IsNullOrEmpty(oparamin.PersonalData.Valueid) && !oparamin.PersonalData.Valueid.Equals("NA") &&
                    oparamin.Companyid != 0)
                {
                    int ierrenroll = ServicesManager.EnrollAfterRemoteVerification(oparamin, txcreated);
                    //Si da error, lo informo pero no bloqueo
                    if (ierrenroll != Errors.IERR_OK)
                    {
                        msg = "Problemas para Enrolar luego de get remota en ConnectorId = " +
                            oparamin.Verifybyconnectorid + " [" + ierrenroll.ToString() + "]";
                        LOG.Warn(msg);
                        paramout.Message = paramout.Message + " - " + msg;
                    }
                }

                //IV.- Armo salida y retorno
                xmlparamout = XmlParamOut.GetXML(oparamin.Actionid, txcreated.Trackid,
                                   oparamin.Authenticationfactor, oparamin.Minutiaetype,
                                   oparamin.Bodypart, 0, 0, 0,
                                   oparamin.Matchingtype, oparamin.Clientid, oparamin.Ipenduser,
                                   oparamin.Enduser, oparamin.Companyid, oparamin.Userid,
                                   oparamin.Companyid, oparamin.Userid,
                                   oparamin.PersonalData, end, txcreated.BpOrigin.Id,
                                   ddataFromConnector,
                                   oparamEnroll != null ?
                                   (string)oparamEnroll.GetValue("verificationsource") :
                                   oparamin.PersonalData.Verificationsource, ddataInfoBirs);
                return Errors.IERR_OK;

            }
            catch (Exception ex)
            {
                msg = "ServiceManager.Get Exception [" + ex.Message + "]";
                LOG.Warn(msg, ex);
                paramout.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                return Errors.IERR_UNKNOWN;
            }
        }

        /// <summary>
        /// Added from 29-04-2016 por Proxy
        /// Este Get es utilizado por el Proxy, para dado un trinomio companyid,typeid,valueid => 
        /// devuelva los datos de la Transaccion de aucerdo a la accion indicada, de acuerdo a:
        ///         ACTION_PROXY_GET = 200;
        ///         ACTION_PROXY_GET_JUSTTXDATA = 202;
        ///         ACTION_PROXY_GET_JUSTABS = 203;
        ///         ACTION_PROXY_GET_TXDATAABS = 204;
        /// O consumir (Marcar como usada)
        ///         ACTION_PROXY_CONSUME_TX = 201;
        /// </summary>
        /// <param name="oparamin"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        private static int GetPROXY(XmlParamIn oparamin, out string xmlparamout)
        {
            XmlParamOut paramout = new XmlParamOut();
            xmlparamout = null;
            string msg;
            int iErr = Errors.IERR_OK;
            DateTime start, end;
            //BpIdentity con la que se trabajará
            BpIdentity oBpIdentity;
            BpIdentity identity = null;
            PersonalData pdOut = null;
            string xmlparamgetout = null;
            DynamicData ddataTx = null; //Para enviar info de ABS si Action es 200/203/204
            BpTx txcreated;
            Parameters oparamEnroll;

            try
            {

                LOG.Debug("ServicesManager.GetPROXY In...");
                if (oparamin.Actionid == Action.ACTION_PROXY_CONSUME_TX)
                {
                    LOG.Debug("ServicesManager.GetPROXY Deribando a ConsumePROXY...");
                    return ConsumePROXY(oparamin, xmlparamout);
                }
                
                //0. Recupero de BD la Tx que corresponda
                //1. Si es solo data saco la ABS
                //2. Si es con All => Agrego la data d ela persona en PersonalData
                //3. Grabo nueva Tx
                //4. Armo XML retorno y respondo

                start = DateTime.Now;

                LOG.Debug("ServicesManager.GetPROXY GetTx In...");
                BpTx tx = DatabaseHelper.GetTx(oparamin, out msg);
                LOG.Debug("ServicesManager.GetPROXY Out [Tx==Null => " + (tx==null) + "]");

                if (tx == null)
                {
                    iErr = Errors.IERR_TX_NOT_EXIST;
                    msg = "Get PROXY - No existe transacción disponible para (company/typeid/valueid = " +
                                 oparamin.Companyid + "/" + oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid;
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(iErr, 0, oparamin, null,
                                          start, end, out oparamEnroll, out txcreated, out identity);
                    return iErr;
                }
                else  //Sigue proceso
                {
                    LOG.Debug("ServicesManager.GetPROXY Tx != null [TrackId = " + tx.Trackid + "]");
                    if (oparamin.Actionid == Action.ACTION_PROXY_GET_JUSTTXDATA) //ACTION_PROXY_GET_JUSTTXDATA = 202;
                    {
                        LOG.Debug("ServicesManager.GetPROXY Actionid == 202 => Elimino ABS...");
                        tx.Abs = null;  //Elimino ABS porque solo pide los datos de la transacción
                    }

                    if (oparamin.Actionid == Action.ACTION_PROXY_GET) //ACTION_PROXY_GET = 200;
                    {
                        //Leo BpIdentity porque pide todo
                        iErr = DatabaseHelper.GetIdentity(true, oparamin.Actionid, oparamin.Companyid, oparamin.PersonalData, out oBpIdentity);
                        if (iErr != Errors.IERR_OK && oBpIdentity != null)
                        {
                            msg = "Get PROXY - Problemas para obtener una identidad (company/typeid/valueid = " +
                                  oparamin.Companyid + "/" + oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid;
                            LOG.Warn(msg);
                            paramout.Message = msg;
                            xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                            end = DateTime.Now;
                            DatabaseHelper.SaveTx(iErr, 0, oparamin, null,
                                                  start, end, out oparamEnroll, out txcreated, out identity);
                            return iErr;
                        }
                        else
                        {
                            //oparamin.PersonalData = DatabaseHelper.FillPersonalDataFromBpIdentity(oBpIdentity);
                            pdOut = FillPersonalData(oparamin, oBpIdentity);
                        }
                    }
                    else
                    {
                        pdOut = oparamin.PersonalData;
                    }

                    //Serializo la BpTx como Additionaldata
                    string xmlToSend;
                    switch (oparamin.Actionid)
                    {
                        case 200:
                        case 202:
                        case 204:
                            xmlToSend = DatabaseHelper.ArmaXMLTx(tx);  //XmlUtils.SerializeObject<BpTx>(tx);  //TODO - Arreglar Serializacion de este objeto!!! 
                            if (!String.IsNullOrEmpty(xmlToSend))
                            {
                                ddataTx = new DynamicData();
                                ddataTx.AddValue("Tx", xmlToSend);
                                //paramout.Additionaldata = ddataTx;
                            }
                            break;
                        case 203:
                            xmlToSend = tx.Abs;
                            if (!String.IsNullOrEmpty(xmlToSend))
                            {
                                ddataTx = new DynamicData();
                                ddataTx.AddValue("BioSignature", xmlToSend);
                                //paramout.Additionaldata = ddataTx;
                            }
                            break;
                        default:
                            break;
                    }

                    if (ddataTx == null)
                    {
                        msg = "Get PROXY - Problemas para serializar respuesta (company/typeid/valueid = " +
                              oparamin.Companyid + "/" + oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid;
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        end = DateTime.Now;
                        DatabaseHelper.SaveTx(iErr, 0, oparamin, null,
                                              start, end, out oparamEnroll, out txcreated, out identity);
                        return Errors.IERR_SERIALIZING_DATA;
                    }

                    //Grabo actual Tx
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(iErr, 0, oparamin, null, start, end, out oparamEnroll, out txcreated, out identity);

                    //Armo salida y retorno
                    xmlparamout = XmlParamOut.GetXML(oparamin.Actionid, txcreated.Trackid,
                                       oparamin.Authenticationfactor, oparamin.Minutiaetype,
                                       oparamin.Bodypart, 0, 0, 0,
                                       oparamin.Matchingtype, oparamin.Clientid, oparamin.Ipenduser,
                                       oparamin.Enduser, oparamin.Companyid, oparamin.Userid,
                                       oparamin.Companyid, oparamin.Userid,
                                       pdOut, end, txcreated.BpOrigin.Id,
                                       null, null, ddataTx);
                    return Errors.IERR_OK;
                }
            }
            catch (Exception ex)
            {
                msg = "ServiceManager.Get PROXY Exception [" + ex.Message + "]";
                LOG.Warn(msg, ex);
                paramout.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                return Errors.IERR_UNKNOWN;
            }
        }


        /// <summary>
        /// Dada un 
        /// </summary>
        /// <param name="oparamin"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        private static int ConsumePROXY(XmlParamIn oparamin, string xmlparamout)
        {
            XmlParamOut paramout = new XmlParamOut();
            xmlparamout = null;
            string msg;
            int iErr = Errors.IERR_OK;
            DateTime start, end;
            //BpIdentity con la que se trabajará
            BpIdentity oBpIdentity;
            BpIdentity identity = null;
            PersonalData pdOut = null;
            string xmlparamgetout = null;
            DynamicData ddataTx = null; //Para enviar info de ABS si Action es 200/203/204
            BpTx txcreated;
            Parameters oparamEnroll;

            try
            {
                start = DateTime.Now;

                BpTx tx = DatabaseHelper.GetTx(oparamin, out msg);

                if (tx == null)
                {
                    iErr = Errors.IERR_TX_NOT_EXIST;
                    msg = "Get PROXY - No existe transacción disponible para (company/typeid/valueid = " +
                                 oparamin.Companyid + "/" + oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid;
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(iErr, 0, oparamin, null,
                                          start, end, out oparamEnroll, out txcreated, out identity);
                    return iErr;
                }
                else  //Sigue proceso
                {
                    iErr = DatabaseHelper.ConsumeTx(oparamin, out msg);

                    //Grabo actual Tx
                    end = DateTime.Now;

                    if (iErr == Errors.IERR_OK)
                    {
                        DatabaseHelper.SaveTx(iErr, 0, oparamin, null, start, end, out oparamEnroll, out txcreated, out identity);

                        //Armo salida y retorno
                        xmlparamout = XmlParamOut.GetXML(oparamin.Actionid, txcreated.Trackid,
                                           oparamin.Authenticationfactor, oparamin.Minutiaetype,
                                           oparamin.Bodypart, 0, 0, 0,
                                           oparamin.Matchingtype, oparamin.Clientid, oparamin.Ipenduser,
                                           oparamin.Enduser, oparamin.Companyid, oparamin.Userid,
                                           oparamin.Companyid, oparamin.Userid,
                                           pdOut, end, txcreated.BpOrigin.Id,
                                           null, null, ddataTx);
                        return Errors.IERR_OK;
                    }
                    else
                    {
                        msg = "Error Cosumiendo TX con Trackid = " + oparamin.Additionaldata;
                        LOG.Error(msg);
                        paramout.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        end = DateTime.Now;
                        DatabaseHelper.SaveTx(iErr, 0, oparamin, null,
                                              start, end, out oparamEnroll, out txcreated, out identity);
                        return iErr;
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "ServiceManager.Get PROXY Exception [" + ex.Message + "]";
                LOG.Warn(msg, ex);
                paramout.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                return Errors.IERR_UNKNOWN;
            }
        }

        

        /// <summary>
        /// Recibe los parámetros para hacer el Get de diferentes variables del sistema (ACTIONS, MINUTIAETYPE, etc)
        /// y devuelve resultado de la misma, o mensaje de error si existe. El resultado es devuelto en xml con 
        /// key/value como resultado
        /// Los pasos son:
        ///         1.- Obtengo la informacion pedida. 
        ///         2.- Grabo Tx
        ///         3.- Generar respuesta en Xml.
        /// </summary>
        /// <param name="oparamin">Objeto de parametros</param>
        /// <param name="xmlparamout">XML con resultado</param>
        /// <returns>Retorna el codigo de ejecución</returns>
        static public int GetOp(XmlParamIn oparamin, out string xmlparamout)
        {
            XmlParamOut paramout = new XmlParamOut();
            string msg;
            int iErr;
            DateTime start, end;
            BpTx txcreated;
            DynamicData oparamOut;
            Parameters oparamEnroll;
            BpIdentity identity;

            try
            {
                start = DateTime.Now;

                //1.- Obtengo la informacion pedida. 
                iErr = DatabaseHelper.GetDataOp(oparamin, out oparamOut, out msg);

                //Si da error retorno con error
                if (iErr != Errors.IERR_OK)
                {
                    msg = "Problemas para obtener datos de la accion = " + oparamin.Actionid;
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(iErr, OperationOrder.OPERATIONORDER_LOCALONLY, oparamin, null,
                                          start, end, out oparamEnroll, out txcreated, out identity);
                    return iErr;
                }

                //I.- Grabo transaccion en log para auditorias
                end = DateTime.Now;
                iErr = DatabaseHelper.SaveTx(Errors.IERR_OK, 0, oparamin, null, start, end,
                                             out oparamEnroll, out txcreated, out identity);
                //Si no graba, devuelve error
                if (iErr != Errors.IERR_OK)
                {
                    msg = "No pudo grabar la BpTx";
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    return iErr;
                } //Sino, sigo

                //III.- Armo salida y retorno
                //string xmlout = Parameters.SerializeToXml(oparamOut); //XmlUtils.SerializeObject<Parameters>(oparamOut);
                xmlparamout = XmlParamOut.GetXML(oparamin.Actionid, txcreated.Trackid, oparamin.Clientid,
                                   oparamin.Ipenduser, oparamin.Enduser, oparamin.Companyid, oparamin.Userid,
                                   end, txcreated.BpOrigin.Id, oparamOut);
                return Errors.IERR_OK;

            }
            catch (Exception ex)
            {
                msg = "ServiceManager.GetOp Exception [" + ex.Message + "]";
                LOG.Warn(msg, ex);
                paramout.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                return Errors.IERR_UNKNOWN;
            }

        }

#endregion Get Region

#region Help

        /// <summary>
        /// Dato un BpIdentity, genera el DynamicData desde la lista de Birs, para devolver si el GET viene
        /// Con Action = 100 - Added 11-11-2014
        /// </summary>
        /// <param name="oBpIdentity">Identity de la BD</param>
        /// <param name="ddataInfoBirs">DynamicaData de los Birs</param>
        /// <returns></returns>
        private static int FormatDDataInfoBirs(BpIdentity oBpIdentity, out DynamicData ddataInfoBirs)
        {
            ddataInfoBirs = null;
            try
            {
                //Si el Nulo Identity o Birs => Devuelve Nulo
                if (oBpIdentity == null || oBpIdentity.BpBir == null || oBpIdentity.BpBir.Count == 0)
                {
                    ddataInfoBirs = null;
                }
                else
                {
                    ddataInfoBirs = new DynamicData();
                    foreach (BpBir item in oBpIdentity.BpBir)
                    {
                        ddataInfoBirs.AddValue(item.Id.ToString(), "AF=" + item.Authenticationfactor +
                                                                   "-MT=" + item.Minutiaetype +
                                                                   "-BP=" + item.Bodypart +
                                                                   "-TY=" + item.Type +
                                                                   "-TS=" + item.Creation.ToString("dd/MM/yyyy HH:mm:ss") +
                                                                   "-CO=" + item.Companyidenroll +
                                                                   "-US=" + item.Useridenroll);
                    }
                }

                
                return Errors.IERR_OK;

            }
            catch (Exception ex)
            {
                LOG.Error("ServicesManager.FormatDDataInfoBirs Error", ex);
                return Errors.IERR_UNKNOWN;
            }
        }


        /// <summary>
        /// Chequea que los parámetros seteados de acuerdo a la operación estén correctos.
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="action"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static bool IsCorrectParamInForAction(XmlParamIn oXmlIn, int action, out string msgerr)
        {
            msgerr = null;
            string msgret = "";
            if (oXmlIn == null)
            {
                msgerr = "Parametros nulos";
                return false;
            }
            
            LOG.Debug("ServiceManager.IsCorrectParamInForAction IN...");
            LOG.Debug("ServiceManager.IsCorrectParamInForAction - oXmlIn.Actionid = " + oXmlIn.Actionid + " - Action = " + action); 
            msgret = CheckStatusCompanyAndUser(oXmlIn, msgret);
            LOG.Debug("ServiceManager.IsCorrectParamInForAction - CheckStatusCompanyAndUser msgret = " + msgret);   
            msgret = CheckOrigin(oXmlIn, msgret);
            LOG.Debug("ServiceManager.IsCorrectParamInForAction - CheckOrigin msgret = " + msgret); 
            switch (action)
            {
                case 1: //Verify
                    if (oXmlIn.Actionid != Action.ACTION_VERIFY &&
                        oXmlIn.Actionid != Action.ACTION_VERIFYANDGET &&
                        oXmlIn.Actionid != Action.ACTION_VERIFY_BIOSIGNATURE) msgret = "Actionid inconsistente";

                    //msgret = CheckStatusCompanyAndUser(oXmlIn, msgret);

                    if (oXmlIn.Minutiaetype != MinutiaeType.MINUTIAETYPE_TOKEN &&
                         oXmlIn.Actionid != Action.ACTION_VERIFY_BIOSIGNATURE)
                    {
                        if (oXmlIn.PersonalData == null ||
                            oXmlIn.PersonalData.Typeid == null || oXmlIn.PersonalData.Typeid.Trim().Length == 0 ||
                            oXmlIn.PersonalData.Valueid == null || oXmlIn.PersonalData.Valueid.Trim().Length == 0)
                        {
                            msgret += "|typeid/valueid nulos";
                        }
                    }

                    if ((oXmlIn.SampleCollection == null || oXmlIn.SampleCollection.Count == 0) &&
                         oXmlIn.Actionid != Action.ACTION_VERIFY_BIOSIGNATURE)
                    {
                        msgret += "|collection sample nulo";
                    }

                    if (String.IsNullOrEmpty(oXmlIn.Additionaldata) &&
                        oXmlIn.Actionid == Action.ACTION_VERIFY_BIOSIGNATURE)
                    {
                        msgret += "|Additionaldata (BioSignature) nulo";
                        //LOG.Debug("ServiceManager.IsCorrectParamInForAction - Case 40 msgret = " + msgret);
                    }

                    msgerr = msgret;
                    LOG.Debug("ServiceManager.IsCorrectParamInForAction - Case 1 / 40 msgret = " + msgret);   
                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia
                case 2: //Identify
                    if (oXmlIn.Actionid != Action.ACTION_IDENTIFY) msgret = "Actionid inconsistente";


                    if (oXmlIn.SampleCollection == null || oXmlIn.SampleCollection.Count == 0)
                    {
                        msgret += "|collection sample nulo";
                    }

                    msgerr = msgret;
                    LOG.Debug("ServiceManager.IsCorrectParamInForAction - Case 2 msgret = " + msgret);   

                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia

                case 3: //Enroll
                case 7: //Add BIR
                    //msgret = CheckStatusCompanyAndUser(oXmlIn, msgret);
                    if (oXmlIn.Actionid != Action.ACTION_ENROLL &&
                        oXmlIn.Actionid != Action.ACTION_ADDBIR) msgret = "Actionid inconsistente";

                    if (oXmlIn.PersonalData == null ||
                        oXmlIn.PersonalData.Typeid == null || oXmlIn.PersonalData.Typeid.Trim().Length == 0 ||
                        oXmlIn.PersonalData.Valueid == null || oXmlIn.PersonalData.Valueid.Trim().Length == 0)
                    {
                        msgret += "|typeid/valueid nulos";
                    }

                    if ((oXmlIn.SampleCollection == null || oXmlIn.SampleCollection.Count == 0) &&
                        oXmlIn.Actionid != Action.ACTION_ENROLL)
                    {
                        msgret += "|collection sample nulo";
                    }

                    msgerr = msgret;
                    LOG.Debug("ServiceManager.IsCorrectParamInForAction - Case 3/7 msgret = " + msgret);   

                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia

                case 4: //Get
                case 5: //Modify
                case 6: //Delete Identity
                case 8: //Delete Bir
                case 10: //Get Solo Datos
                case 11: //Get Solo Foto
                case 12: //Get Solo Firma
                //case 100: //Get Sync Data

                    //msgret = CheckStatusCompanyAndUser(oXmlIn, msgret);
                    if (oXmlIn.Actionid != Action.ACTION_GET &&
                        oXmlIn.Actionid != Action.ACTION_GETDATA &&
                        oXmlIn.Actionid != Action.ACTION_GETPHOTO &&
                        oXmlIn.Actionid != Action.ACTION_GETSIGNATURE &&
                        oXmlIn.Actionid != Action.ACTION_MODIFY &&
                        oXmlIn.Actionid != Action.ACTION_DELETE &&
                        oXmlIn.Actionid != Action.ACTION_DELETEBIR) msgret = "Actionid inconsistente";

                    if (oXmlIn.PersonalData == null ||
                        oXmlIn.PersonalData.Typeid == null || oXmlIn.PersonalData.Typeid.Trim().Length == 0 ||
                        oXmlIn.PersonalData.Valueid == null || oXmlIn.PersonalData.Valueid.Trim().Length == 0)
                    {
                        msgret += "|typeid/valueid nulos";
                    }

                    msgerr = msgret;
                    LOG.Debug("ServiceManager.IsCorrectParamInForAction - Case 4/5/6/8/10/11/12/100 msgret = " + msgret);   

                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia

                case 20: //Get Actions
                case 21: //Get Origin
                case 22: //Get Matchers Availables
                case 23: //Get Connectors Availables
                case 24: //Get AF Avaiñables
                case 25: //Get MT Availables
                case 26: //Get Birs Enroled
                    //msgret = CheckStatusCompanyAndUser(oXmlIn, msgret);
                    if (oXmlIn.Actionid != Action.ACTION_GETACTIONS &&
                        oXmlIn.Actionid != Action.ACTION_GETORIGINS &&
                        oXmlIn.Actionid != Action.ACTION_GETMATCHESRAVAILABLES &&
                        oXmlIn.Actionid != Action.ACTION_GETCONNECTORSAVAILABLES &&
                        oXmlIn.Actionid != Action.ACTION_GETAUTHENTICATIONFACTORSAVAILABLES &&
                        oXmlIn.Actionid != Action.ACTION_GETBODYPARTENROLLED &&
                        oXmlIn.Actionid != Action.ACTION_GETMINUTIAETYPESAVAILABLES) msgret = "Actionid inconsistente";

                    if (oXmlIn.Actionid == Action.ACTION_GETBODYPARTENROLLED && oXmlIn.Authenticationfactor <= 0)
                    {
                        msgret += "AuthenticationFactor debe ser mayor a 0";
                    }
                    msgerr = msgret;
                    LOG.Debug("ServiceManager.IsCorrectParamInForAction - Case 20 a 26 msgret = " + msgret);   

                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia

                case 40: //Verify BioSignature
                    //msgret = CheckStatusCompanyAndUser(oXmlIn, msgret);
                    if (oXmlIn.Actionid != Action.ACTION_VERIFY_BIOSIGNATURE) msgret = "Actionid inconsistente";

                    if (String.IsNullOrEmpty(oXmlIn.Additionaldata))
                    {
                        msgret += "|Additionaldata (BioSignature) nulo";
                    }

                    msgerr = msgret;
                    LOG.Debug("ServiceManager.IsCorrectParamInForAction - Case 40 msgret = " + msgret);

                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia

                case 100: //Get MT Availables
                    //msgret = CheckStatusCompanyAndUser(oXmlIn, msgret);
                    if (oXmlIn.Actionid != Action.ACTION_SYNC_GETBIRS) msgret = "Actionid inconsistente";

                    if (oXmlIn.PersonalData == null ||
                        oXmlIn.PersonalData.Typeid == null || oXmlIn.PersonalData.Typeid.Trim().Length == 0 ||
                        oXmlIn.PersonalData.Valueid == null || oXmlIn.PersonalData.Valueid.Trim().Length == 0)
                    {
                        msgret += "|typeid/valueid nulos";
                    }

                    msgerr = msgret;
                    LOG.Debug("ServiceManager.IsCorrectParamInForAction - Case 100 msgret = " + msgret);   

                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia

            //Added from 29-04-2016
                case 200: //Get de Proxy
                case 202:
                case 203:
                case 204:
                    //msgret = CheckStatusCompanyAndUser(oXmlIn, msgret);
                    if (oXmlIn.Actionid != Action.ACTION_PROXY_GET &&
                        oXmlIn.Actionid != Action.ACTION_PROXY_GET_JUSTTXDATA &&
                        oXmlIn.Actionid != Action.ACTION_PROXY_GET_JUSTABS &&
                        oXmlIn.Actionid != Action.ACTION_PROXY_GET_TXDATAABS) msgret = "Actionid inconsistente";

                    if (oXmlIn.Companyid <= 0)
                    {
                        msgret += "|company no informada (de be ser > a 0)";
                    }

                    if (oXmlIn.PersonalData == null ||
                        oXmlIn.PersonalData.Typeid == null || oXmlIn.PersonalData.Typeid.Trim().Length == 0 ||
                        oXmlIn.PersonalData.Valueid == null || oXmlIn.PersonalData.Valueid.Trim().Length == 0)
                    {
                        msgret += "|typeid/valueid nulos";
                    }

                    msgerr = msgret;
                    LOG.Debug("ServiceManager.IsCorrectParamInForAction - Case 200 Get Proxy msgret = " + msgret);

                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia

                case 201: //Consume Tx by Proxy
                    //msgret = CheckStatusCompanyAndUser(oXmlIn, msgret);
                    if (oXmlIn.Actionid != Action.ACTION_PROXY_CONSUME_TX) msgret = "Actionid inconsistente";

                    if (String.IsNullOrEmpty(oXmlIn.Additionaldata))
                    {
                        msgret += "|Additionaldata (TrackId) nulo";
                    }

                    msgerr = msgret;
                    LOG.Debug("ServiceManager.IsCorrectParamInForAction - Case 201 Consume Tx by Proxy msgret = " + msgret);

                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia

                default:
                    msgerr = "Accion no documentada";
                    LOG.Debug("ServiceManager.IsCorrectParamInForAction - default msgret = " + msgret);   

                    return false;
            }
            //LOG.Debug("ServiceManager.IsCorrectParamInForAction OUT!"); 

        }

        /// <summary>
        /// Chequea si esta definido el origen, sino rechaza
        /// </summary>
        /// <param name="oXmlIn">Parametros</param>
        /// <param name="msgerr">Concatena error sino</param>
        internal static string CheckOrigin(XmlParamIn oXmlIn, string msgerr)
        {
            string lmsgerr = msgerr;
            string aux = "";
            try
            {
                if (!DatabaseHelper.ExistOrigin(oXmlIn.Origin, out aux))
                {
                    lmsgerr = lmsgerr + "|Origin No Existe";
                    if (!aux.Equals("S/C")) lmsgerr = lmsgerr + "[" + aux + "]";
                }

            }
            catch (Exception ex)
            {
                lmsgerr = lmsgerr + "|Origin = " + ex.Message;
            }
            return lmsgerr;
        }

        /// <summary>
        /// Chequea que la compañia y el usuario tengan permiso para usar los WS. 
        /// Dependiendo de CONFIG.CheckAccess = [0|1|2|3] hace el chequeo correspondiente.
        /// </summary>
        /// <param name="oXmlIn">Parametros</param>
        /// <param name="msgerr">Concatena error sino</param>
        internal static string CheckStatusCompanyAndUser(XmlParamIn oXmlIn, string msgerr)
        {
            string lmsgerr = msgerr;
            Company com;
            try
            {
                switch (Global.CONFIG.CheckAccess)
                {
                    case 0: //Check Disabled
                        break;
                    case 1: //Chek Company
                        com = Global.ArrCompanys.getCompany(oXmlIn.Companyid);
                        if (com == null)
                        {
                            lmsgerr = "La compañia no existe";
                        }
                        else if (com.status == 1)
                        { //Si está deshabilitado
                            lmsgerr = "La compañia esta inhabilitada para hacer consultas";
                            if (com.fecha.HasValue)
                            {
                                lmsgerr = lmsgerr + " desde el " + com.fecha.Value.ToString("dd/MM/yyyy");
                            }
                        }
                        break;
                    case 2: //Check User
                        break;
                    case 3: //Check Company and User
                        com = Global.ArrCompanys.getCompany(oXmlIn.Companyid);
                        if (com == null)
                        {
                            lmsgerr = "La compañia no existe";
                        }
                        else if (com.status == 1)
                        { //Si está deshabilitado
                            lmsgerr = "La compañia esta inhabilitada para hacer consultas";
                            if (com.fecha.HasValue)
                            {
                                lmsgerr = lmsgerr + " desde el " + com.fecha.Value.ToString("dd/MM/yyyy");
                            }
                        }
                        break;
                    default: //Si no está especificado correctametne se usa el mas restrictivo = 3
                        lmsgerr = "";
                        break;
                }

            }
            catch (Exception ex)
            {
                lmsgerr = "ServicesManager.CheckStatusCompanyAndUser [" + ex.Message + "]";
                LOG.Error("ServicesManager.CheckStatusCompanyAndUser", ex);
            }

            return lmsgerr;
        }


        /// <summary>
        /// Debe verificar que es un token válido, y extraer de el los datos empaquetados 
        /// allí, y rellenar oXmlIn con esos datos.
        ///     1.- Chequeo tipo de Minutiaetype
        ///     2.- Verifico que version de token se presenta
        ///     3.- Desencripto información y split
        ///     4.- Relleno datos en oXmlInNew
        ///     5.- 
        /// </summary>
        /// <param name="oXmlIn">Parámetros originales</param>
        /// <param name="oXmlInNew">Parametros updateados con informacion incluida en el token</param>
        /// <param name="msgerr">Mensaje de error</param>
        /// <returns>Codigo de error</returns>
        public static int VerifyAndExtractToken(XmlParamIn oXmlIn, out XmlParamIn oXmlInNew, out string msgerr)
        {
            int iErr = Errors.IERR_OK;
            IToken oToken;
            oXmlInNew = oXmlIn;
            msgerr = null;
            List<Sample> listSamplesCopy;
            try
            {
                listSamplesCopy = oXmlIn.SampleCollection;
                iErr = Global.TOKEN_MANAGER.HandleToken(oXmlIn.SampleCollection[0].Data, out oToken);

                //Si da error retorno con error
                if (iErr != Errors.IERR_OK)
                {
                    msgerr = "Error extrayendo datos del token";
                    LOG.Warn(msgerr);
                    return iErr;
                }

                //Chequeo si OperationType corresponde con operacion declarada en oXmlIn
                if (oToken.OperationType == 1)
                {
                    if (oXmlIn.Actionid != Action.ACTION_VERIFY && 
                        oXmlIn.Actionid != Action.ACTION_VERIFYANDGET &&
                        oXmlIn.Actionid != Action.ACTION_IDENTIFY)
                    {
                        msgerr = "Operación en Token difiere con operacion en parámetros [Param=" +
                            oXmlIn.Actionid.ToString() + " - Op.Type=" + oToken.OperationType.ToString() + "]";
                        LOG.Warn(msgerr);
                        return Errors.IERR_INCONSISTENT_ACTION;
                    }
                }
                else
                {
                    if (oXmlIn.Actionid != Action.ACTION_MODIFY &&
                        oXmlIn.Actionid != Action.ACTION_ADDBIR &&
                        oXmlIn.Actionid != Action.ACTION_ENROLL)
                    {
                        msgerr = "Operación en Token difiere con operacion en parámetros [Param=" +
                             oXmlIn.Actionid.ToString() + " - Op.Type=" + oToken.OperationType.ToString() + "]";
                        LOG.Warn(msgerr);
                        return Errors.IERR_INCONSISTENT_ACTION;
                    }
                }

                iErr = ServicesManager.FillXmlParamInFromToken(oToken, oXmlIn, out oXmlInNew);

                bool _agrego = true;
                //Added 11/2018 para enroll de multiples token en un unico Token
                if (listSamplesCopy.Count > 1) //Si hay mas token, agrego los samples tomados desde los token
                {
                    for (int i = 1; i < listSamplesCopy.Count; i++)
                    {
                        oToken = null;
                        _agrego = true;
                        iErr = Global.TOKEN_MANAGER.HandleToken(listSamplesCopy[i].Data, out oToken);

                        //Si da error retorno con error
                        if (iErr != Errors.IERR_OK)
                        {
                            msgerr = "Error extrayendo datos del token";
                            _agrego = false;

                            if (_agrego)
                            {
                                //Chequeo si OperationType corresponde con operacion declarada en oXmlIn
                                if (oToken.OperationType == 1)
                                {
                                    if (oXmlIn.Actionid != Action.ACTION_VERIFY &&
                                        oXmlIn.Actionid != Action.ACTION_VERIFYANDGET &&
                                        oXmlIn.Actionid != Action.ACTION_IDENTIFY)
                                    {
                                        _agrego = false;
                                    }
                                }
                                else
                                {
                                    if (oXmlIn.Actionid != Action.ACTION_MODIFY &&
                                        oXmlIn.Actionid != Action.ACTION_ADDBIR &&
                                        oXmlIn.Actionid != Action.ACTION_ENROLL)
                                    {
                                        _agrego = false;
                                    }
                                }
                            }
                        }

                        //Add list sample
                        if (_agrego)
                        {
                            iErr = ServicesManager.AddSamplesToXmlParamInFromToken(oToken, oXmlIn, out oXmlInNew);
                         }
                    }
                }

            }
            catch (Exception ex)
            {
                iErr = Errors.IERR_UNKNOWN;
                msgerr = ex.Message;
            }
            return iErr;
        }


        /// <summary>
        /// Dado un token, agrega la lista de samples del token en un XmlParamIn. Esto es para cuando se envia
        /// a enrolar mas de un token a la vez, para poder hacer un enroll multimple en un unico request.
        /// </summary>
        /// <param name="oToken"></param>
        /// <param name="oXmlIn"></param>
        /// <param name="oXmlInNew"></param>
        /// <returns></returns>
        private static int AddSamplesToXmlParamInFromToken(IToken oToken, XmlParamIn oXmlIn, out XmlParamIn oXmlInNew)
        {
            int iErr = Errors.IERR_OK;
            oXmlInNew = oXmlIn;

            try
            {
                if (oXmlIn == null || oToken == null) return Errors.IERR_BAD_PARAMETER;

                if (oXmlInNew.SampleCollection == null)
                {
                    oXmlInNew.SampleCollection = oToken.Samples;
                } else
                {
                    foreach (Sample item in oToken.Samples)
                    {
                        oXmlInNew.SampleCollection.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                iErr = Errors.IERR_UNKNOWN;
                LOG.Error("ServiceManager.FillXmlParamInFromToken", ex);
            }
            return iErr;
        }

        /// <summary>
        /// Dadoun token, llena los parametros del XmlParamIn, para seguir sistema normal de verificacion
        /// </summary>
        /// <param name="oToken">Objeto Token completo</param>
        /// <param name="oXmlInNew">Parametros </param>
        /// <returns></returns>
        private static int FillXmlParamInFromToken(IToken oToken, XmlParamIn oXmlIn, out XmlParamIn oXmlInNew)
        {
            int iErr = Errors.IERR_OK;
            oXmlInNew = oXmlIn;

            try
            {
                if (oXmlIn == null || oToken == null) return Errors.IERR_BAD_PARAMETER;

                if (oXmlInNew.PersonalData == null) oXmlInNew.PersonalData = new PersonalData();

                //Lleno Typeid/Valueid solo si es verificacion, si es Identificacion, lo deja nulo
                if (oXmlInNew.Actionid != Action.ACTION_IDENTIFY)
                {
                    oXmlInNew.PersonalData.Typeid = oToken.Typeid;
                    oXmlInNew.PersonalData.Valueid = oToken.Valueid;
                }

                oXmlInNew.Authenticationfactor = oToken.AuthenticationFactor;
                //Si viene template en token, pongo minucia del token, sino dejola de paramin
                if (oToken.DataTypeToken == 0 || oToken.DataTypeToken == 2)
                {
                    oXmlInNew.Minutiaetype = oToken.MinutiaeType; //Aunque no se usa y se use la de Sample
                }
                //Si es BODYPART_ALL => Debe ignorar el dedo del token
                if (oXmlIn.Bodypart != BodyPart.BODYPART_ALL)
                {
                    oXmlInNew.Bodypart = oToken.BodyPart; //Aunque no se usa y se use la de Sample
                }
                oXmlInNew.SampleCollection = oToken.Samples;
                if (String.IsNullOrEmpty(oXmlInNew.Additionaldata))
                {
                    oXmlInNew.Additionaldata = oToken.AdditionalData;   //Aunque no se usa y se use la de Sample
                }
                else
                {
                    if (!String.IsNullOrEmpty(oToken.AdditionalData)) {
                        oXmlInNew.Additionaldata = oXmlInNew.Additionaldata + "|" + oToken.AdditionalData; 
                    }
                }
                oXmlInNew.Clientid = oToken.Clientid;

                ////Added 29-04-2016
                ////Para control de cliente via IP y no x Serial ID del lector
                //if (Global.CONFIG.CheckClientConnected == 1 && Global.CONFIG.CheckClientConnected == 2) {
                //    oXmlInNew.Clientid = oToken.Ipenduser;
                //}

                oXmlInNew.Ipenduser = oToken.Ipenduser;
                oXmlInNew.Enduser = oToken.Enduser;
            }
            catch (Exception ex)
            {
                iErr = Errors.IERR_UNKNOWN;
                LOG.Error("ServiceManager.FillXmlParamInFromToken", ex);
            }
            return iErr;
        }

        /// <summary>
        /// Formatea una DynamicData en Parameter
        /// </summary>
        /// <param name="ddataFromConnector">DynamicData con lista de pares key/value</param>
        /// <returns>Xml de Parameter serializado con los datos de ddataFromConnector</returns>
        private static string FormatDynamicDataToParameter(DynamicData ddataFromConnector, XmlParamIn oparamin,
                                                           out DynamicData ddataFromConnectorOut, out XmlParamIn oparaminout)
        {
            /*
                paramout.AddValue("score", bestscore);
                paramout.AddValue("result", verifyOk ? 1 : 2);
                paramout.AddValue("threshold", threshold);
                if (verifyOk)
                {
                    paramout.AddValue("bodypart", templateVerified == null ? 0 : templateVerified.BodyPart);
                    paramout.AddValue("companyidenroll", templateVerified == null ? 0 : templateVerified.Companyidenroll);
                    paramout.AddValue("useridenroll", templateVerified == null ? 0 : templateVerified.Useridenroll);
                    paramout.AddValue("verificationsource", templateVerified == null ? null : templateVerified.Verificationsource); 
            --------------------
                        oXmlout.SetValue("message", "");
                        oXmlout.SetValue("status", iretremoto.ToString());
                        oXmlout.AddValue("trackid", oXmlParamOut.Trackid);
                        oXmlout.AddValue("result", oXmlParamOut.Result.ToString());
                        oXmlout.AddValue("score", oXmlParamOut.Score.ToString());
                        oXmlout.AddValue("timestamp", oXmlParamOut.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
                        oXmlout.AddValue("personaldata", XmlUtils.Serialoze Object(oXmlParamIn.PersonalData));
        
            */
            string ret = null;
            ddataFromConnectorOut = ddataFromConnector;
            oparaminout = oparamin;
            try
            {
                if (ddataFromConnector == null || ddataFromConnector.GetItems().Count == 0) return null;

                Parameters param = new Parameters();
                foreach (DynamicDataItem dditem in ddataFromConnector.GetItems())
                {
                    param.AddValue(dditem.key, dditem.value);
                }
                param.AddValue("bodypart", oparamin.Bodypart);
                param.AddValue("verificationsource", oparamin.Verifybyconnectorid);
                //param.AddValue("verificationsource", oparamin.Verifybyconnectorid);
                ddataFromConnectorOut = ddataFromConnector;
                ddataFromConnectorOut.AddValue("bodypart", oparamin.Bodypart.ToString());
                ddataFromConnectorOut.AddValue("verificationsource", oparamin.Verifybyconnectorid);

                //Added 20/05/2016 - por RENIEC 
                // Si viene info adicional de Personal Data, lleno el XmlParamIn para devolver los datos en la respuesta 
                //  y mejorar el enroll si está habilitado
                object obj = ddataFromConnector.GetValue("personaldata"); 
                LOG.Debug("ServiceManager.FormatDynamicDataToParameter obj != null (personaldata from connector) => " + (obj!=null).ToString()); 
                string spersonaldata = (obj != null) ? (string)obj : null;
                LOG.Debug("ServiceManager.FormatDynamicDataToParameter spersonalda " + spersonaldata);
                if (!String.IsNullOrEmpty(spersonaldata))
                {
                    try
                    {
                        PersonalData pd = XmlUtils.DeserializeObject<PersonalData>(spersonaldata);

                        //if (!string.IsNullOrEmpty(pd.Valueid) &&
                        //    !pd.Valueid.Equals(oparaminout.PersonalData.Valueid) &&
                        //    oparaminout.PersonalData.Valueid.Equals("NA"))
                        if (!string.IsNullOrEmpty(pd.Valueid) &&
                            (!pd.Valueid.Equals(oparaminout.PersonalData.Valueid) ||
                             oparaminout.PersonalData.Valueid.Equals("NA")))
                        {
                            oparaminout.PersonalData.Valueid = pd.Valueid;
                        }
                        if (!string.IsNullOrEmpty(pd.Typeid) &&
                            !pd.Valueid.Equals(oparaminout.PersonalData.Typeid) &&
                            oparaminout.PersonalData.Typeid.Equals("NA"))
                        {
                            oparaminout.PersonalData.Typeid = pd.Typeid;
                        }
                        oparaminout.PersonalData.Name = !String.IsNullOrEmpty(pd.Name) ? pd.Name : oparaminout.PersonalData.Name;
                        oparaminout.PersonalData.Patherlastname = !String.IsNullOrEmpty(pd.Patherlastname) ? pd.Patherlastname : oparaminout.PersonalData.Patherlastname;
                        oparaminout.PersonalData.Motherlastname = !String.IsNullOrEmpty(pd.Motherlastname) ? pd.Motherlastname : oparaminout.PersonalData.Motherlastname;
                        oparaminout.PersonalData.Birthdate = !(pd.Birthdate == null) ? pd.Birthdate : oparaminout.PersonalData.Birthdate;
                        oparaminout.PersonalData.Documentexpirationdate = !(pd.Documentexpirationdate == null) ? pd.Documentexpirationdate : oparaminout.PersonalData.Documentexpirationdate;
                        oparaminout.PersonalData.Birthplace = !String.IsNullOrEmpty(pd.Birthplace) ? pd.Birthplace : oparaminout.PersonalData.Birthplace;
                        oparaminout.PersonalData.Nationality = !String.IsNullOrEmpty(pd.Nationality) ? pd.Nationality : oparaminout.PersonalData.Nationality;
                        oparaminout.PersonalData.Documentseriesnumber = !String.IsNullOrEmpty(pd.Documentseriesnumber) ? pd.Documentseriesnumber : oparaminout.PersonalData.Documentseriesnumber;
                        oparaminout.PersonalData.Photography = !String.IsNullOrEmpty(pd.Photography) ? pd.Photography : oparaminout.PersonalData.Photography;
                        oparaminout.PersonalData.Signatureimage = !String.IsNullOrEmpty(pd.Signatureimage) ? pd.Signatureimage : oparaminout.PersonalData.Signatureimage;
                        oparaminout.PersonalData.DocImageFront = !String.IsNullOrEmpty(pd.DocImageFront) ? pd.DocImageFront : oparaminout.PersonalData.DocImageFront;
                        oparaminout.PersonalData.DocImageBack = !String.IsNullOrEmpty(pd.DocImageBack) ? pd.DocImageBack : oparaminout.PersonalData.DocImageBack;
                        oparaminout.PersonalData.Visatype = !String.IsNullOrEmpty(pd.Visatype) ? pd.Visatype : oparaminout.PersonalData.Visatype;
                        oparaminout.PersonalData.Sex = !String.IsNullOrEmpty(pd.Sex) ? pd.Sex : oparaminout.PersonalData.Sex;

                        oparaminout.PersonalData.Creation = pd.Creation!=DateTime.MinValue ? pd.Creation : oparaminout.PersonalData.Creation;
                        oparaminout.PersonalData.Profession = !String.IsNullOrEmpty(pd.Profession) ? pd.Profession : oparaminout.PersonalData.Profession;

                        LOG.Debug("ServiceManager.FormatDynamicDataToParameter Oparamin actualizado con data desde remoto!");
                        LOG.Debug("ServiceManager.FormatDynamicDataToParameter Nombre = " + (!String.IsNullOrEmpty(oparaminout.PersonalData.Name)?oparaminout.PersonalData.Name:""));
                        LOG.Debug("ServiceManager.FormatDynamicDataToParameter Apellido Paterno = " + (!String.IsNullOrEmpty(oparaminout.PersonalData.Patherlastname) ? oparaminout.PersonalData.Patherlastname : ""));
                        LOG.Debug("ServiceManager.FormatDynamicDataToParameter Apellido Materno = " + (!String.IsNullOrEmpty(oparaminout.PersonalData.Motherlastname) ? oparaminout.PersonalData.Motherlastname : ""));
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("ServiceManager.FormatDynamicDataToParameter Error " + ex.Message);
                    }
                }
                ret = Parameters.SerializeToXml(param);
            }
            catch (Exception ex)
            {
                LOG.Error("ServiceManager.FillXmlParamInFromToken", ex);
            }
            return ret;
        }

        /// <summary>
        /// Enrola o Modifica una identidad que fue previamente verificado via algun conector externo.
        /// </summary>
        /// <param name="oparamin">Parametros iniciales </param>
        /// <param name="txcreated"></param>
        /// <returns></returns>
        internal static int EnrollAfterRemoteVerification(XmlParamIn oparamin, BpTx txcreated)
        {
            int ierr = Errors.IERR_OK;
            string oput;
            try
            {
                if (txcreated == null)
                {
                    LOG.Error("ServicesManager.EnrollAfterRemoteVerification txcreated = null");
                    return Errors.IERR_BAD_PARAMETER;
                }

                if (txcreated.Operationsource != Source.SOURCE_REMOTE)
                {
                    LOG.Error("ServicesManager.EnrollAfterRemoteVerification Source incorrecto apra Enrolar");
                    return Errors.IERR_BAD_PARAMETER;
                }

                XmlParamIn oin = (XmlParamIn)oparamin.Clone();
                oin.Actionid = Action.ACTION_ENROLL;
                oin.OperationOrder = OperationOrder.OPERATIONORDER_LOCALONLY;
                oin.SaveVerified = 2;
                oin.InsertOption = 2;
                DynamicData ddata = new DynamicData();
                DynamicDataItem ddataitem = new DynamicDataItem("source", oin.Verifybyconnectorid);
                ddata.DynamicDataItems = new List<DynamicDataItem> { ddataitem };
                if (txcreated.BpTxConx != null)
                {
                    ddataitem = new DynamicDataItem("threshold", txcreated.BpTxConx[0].Threshold.ToString());
                    ddata.DynamicDataItems.Add(ddataitem);
                    ddataitem = new DynamicDataItem("score", txcreated.BpTxConx[0].Score.ToString());
                    ddata.DynamicDataItems.Add(ddataitem);
                    ddataitem = new DynamicDataItem("timestamp", txcreated.BpTxConx[0].Timestamp);
                    ddata.DynamicDataItems.Add(ddataitem);
                }
                oin.Additionaldata = DynamicData.SerializeToXml(ddata);

                oin.PersonalData.Companyidenroll = oin.Companyid;
                oin.PersonalData.Typeid = oparamin.PersonalData.Typeid;
                oin.PersonalData.Valueid = oparamin.PersonalData.Valueid;
                oin.PersonalData.Name = !String.IsNullOrEmpty(oparamin.PersonalData.Name) ? oparamin.PersonalData.Name : oin.PersonalData.Name;
                oin.PersonalData.Patherlastname = !String.IsNullOrEmpty(oparamin.PersonalData.Patherlastname) ? oparamin.PersonalData.Patherlastname : oin.PersonalData.Patherlastname;
                oin.PersonalData.Motherlastname = !String.IsNullOrEmpty(oparamin.PersonalData.Motherlastname) ? oparamin.PersonalData.Motherlastname : oin.PersonalData.Motherlastname;
                oin.PersonalData.Birthdate = !(oparamin.PersonalData.Birthdate == null) ? oparamin.PersonalData.Birthdate : oin.PersonalData.Birthdate;
                oin.PersonalData.Documentexpirationdate = !(oparamin.PersonalData.Documentexpirationdate == null) ? oparamin.PersonalData.Documentexpirationdate : oin.PersonalData.Documentexpirationdate;
                oin.PersonalData.Birthplace = !String.IsNullOrEmpty(oparamin.PersonalData.Birthplace) ? oparamin.PersonalData.Birthplace : oin.PersonalData.Birthplace;
                oin.PersonalData.Nationality = !String.IsNullOrEmpty(oparamin.PersonalData.Nationality) ? oparamin.PersonalData.Nationality : oin.PersonalData.Nationality;
                oin.PersonalData.Photography = !String.IsNullOrEmpty(oparamin.PersonalData.Photography) ? oparamin.PersonalData.Photography : oin.PersonalData.Photography;
                oin.PersonalData.Signatureimage = !String.IsNullOrEmpty(oparamin.PersonalData.Signatureimage) ? oparamin.PersonalData.Signatureimage : oin.PersonalData.Signatureimage;
                oin.PersonalData.DocImageFront = !String.IsNullOrEmpty(oparamin.PersonalData.DocImageFront) ? oparamin.PersonalData.DocImageFront : oin.PersonalData.DocImageFront;
                oin.PersonalData.DocImageBack = !String.IsNullOrEmpty(oparamin.PersonalData.DocImageBack) ? oparamin.PersonalData.DocImageBack : oin.PersonalData.DocImageBack;
                oin.PersonalData.Visatype = !String.IsNullOrEmpty(oparamin.PersonalData.Visatype) ? oparamin.PersonalData.Visatype : oin.PersonalData.Visatype;

                if (oparamin.Actionid == Action.ACTION_VERIFY ||
                    oparamin.Actionid == Action.ACTION_VERIFYANDGET ||
                    oparamin.Actionid == Action.ACTION_IDENTIFY)
                {
                    oin.PersonalData.Verificationsource = oparamin.Verifybyconnectorid;
                        //DatabaseHelper.SetVerificationSource(DatabaseHelper.GetVerificationSource(oparamin.Companyid, oparamin.PersonalData.Typeid, 
                        //                                                                          oparamin.PersonalData.Valueid), 
                        //                                     oparamin.Authenticationfactor, 1, "A", oin.Verifybyconnectorid);
                }

                oin.Verifybyconnectorid = null;

                //Added 17-06-2016 - Chequeo que typeid/valueid/company no exista => Enrolo 
                //                   Sino AddBir
                if (!DatabaseHelper.ExistIdentity(oparamin.Companyid, oparamin.PersonalData.Typeid, oparamin.PersonalData.Valueid))
                {
                    LOG.Debug("ServicesManager.EnrollAfterRemoteVerification - !ExistIdentity => Enroll after verify en : " + oparamin.Verifybyconnectorid);
                    ierr = ServicesManager.Enroll(oin, out oput);
                }
                else
                {
                    //if (oin.SampleCollection == null || oin.SampleCollection.Count == 0)
                    //{
                    oin.Actionid = Action.ACTION_MODIFY;
                    //}
                    //else
                    //{
                    //    oin.Actionid = Action.ACTION_ADDBIR;
                    //}
                    LOG.Debug("ServicesManager.EnrollAfterRemoteVerification - ExistIdentity => AddBir or Modify after verify en : " + oparamin.Verifybyconnectorid);
                    LOG.Debug("ServicesManager.EnrollAfterRemoteVerification oin.Actionid = " + oin.Actionid.ToString());
                    ierr = ServicesManager.Enroll(oin, out oput);
                }

            }
            catch (Exception ex)
            {
                ierr = Errors.IERR_UNKNOWN;
                LOG.Error("ServicesManager.EnrollAfterRemoteVerification Error", ex);
            }
            return ierr;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oparamin"></param>
        /// <param name="identity"></param>
        /// <returns></returns>
        internal static PersonalData FillPersonalData(XmlParamIn oparamin, BpIdentity identity)
        {
            PersonalData pd = new PersonalData();
            try
            {
                LOG.Debug("ServiceManager.FillPersonalData oparamin.Actionid = " + oparamin.Actionid.ToString());
                if (oparamin.Actionid == Action.ACTION_VERIFYANDGET ||
                    oparamin.Actionid == Action.ACTION_PROXY_GET) //Added 29-04-2016 para Proxy
                {
                    pd = DatabaseHelper.FillPersonalDataFromBpIdentity(identity)
                        ?? new PersonalData
                                        {
                                            Typeid = oparamin.PersonalData.Typeid,
                                            Valueid = oparamin.PersonalData.Valueid
                                        };
                }
                else
                {
                    pd.Typeid = oparamin.PersonalData.Typeid;
                    pd.Valueid = oparamin.PersonalData.Valueid;
                    pd.Name = !String.IsNullOrEmpty(oparamin.PersonalData.Name) ? oparamin.PersonalData.Name : pd.Name;
                    pd.Patherlastname = !String.IsNullOrEmpty(oparamin.PersonalData.Patherlastname) ? oparamin.PersonalData.Patherlastname : pd.Patherlastname;
                    pd.Motherlastname = !String.IsNullOrEmpty(oparamin.PersonalData.Motherlastname) ? oparamin.PersonalData.Motherlastname : pd.Motherlastname;
                    pd.Birthdate = !(oparamin.PersonalData.Birthdate == null) ? oparamin.PersonalData.Birthdate : pd.Birthdate;
                    pd.Documentexpirationdate = !(oparamin.PersonalData.Documentexpirationdate == null) ? oparamin.PersonalData.Documentexpirationdate : pd.Documentexpirationdate;
                    pd.Birthplace = !String.IsNullOrEmpty(oparamin.PersonalData.Birthplace) ? oparamin.PersonalData.Birthplace : pd.Birthplace;
                    pd.Nationality = !String.IsNullOrEmpty(oparamin.PersonalData.Nationality) ? oparamin.PersonalData.Name : pd.Nationality;
                    pd.Photography = !String.IsNullOrEmpty(oparamin.PersonalData.Photography) ? oparamin.PersonalData.Photography : pd.Photography;
                    pd.Signatureimage = !String.IsNullOrEmpty(oparamin.PersonalData.Signatureimage) ? oparamin.PersonalData.Signatureimage : pd.Signatureimage;
                    pd.DocImageFront = !String.IsNullOrEmpty(oparamin.PersonalData.DocImageFront) ? oparamin.PersonalData.DocImageFront : pd.DocImageFront;
                    pd.DocImageBack = !String.IsNullOrEmpty(oparamin.PersonalData.DocImageBack) ? oparamin.PersonalData.DocImageBack : pd.DocImageBack;
                    pd.Visatype = !String.IsNullOrEmpty(oparamin.PersonalData.Visatype) ? oparamin.PersonalData.Visatype : pd.Visatype;
                }
                
            }
            catch (Exception ex)
            {
                pd.Typeid = oparamin.PersonalData.Typeid;
                pd.Valueid = oparamin.PersonalData.Valueid;
                LOG.Error("ServiceManager.FillPersonalData Error", ex);
            }
            LOG.Debug("ServiceManager.FillPersonalData out [pd.Typeid = " + pd.Typeid + 
                       " - pd.Valueid = " + pd.Valueid + "]");
            return pd;
        }

        /// <summary>
        /// Added 29-04-2016 para chequeo de verificación de clientes que se conectan
        /// </summary>
        /// <param name="oparamin"></param>
        /// <returns></returns>
        internal static bool ClientAuthorized(XmlParamIn oparamin, out string msg)
        {
            bool _ret = false;
            msg = "";
            try
            {
                LOG.Debug("ServiceManager.ClientAuthorized - TipoCheck = " + Global.CONFIG.AutoRegisterClientConnected.ToString() 
                           + " - oparamin.ClientId = " + oparamin.Clientid);

                _ret = DatabaseHelper.ClientAuthorized(oparamin.Companyid, oparamin.Clientid, out msg);
            }
            catch (Exception ex)
            {
                LOG.Error("ServiceManager.ClientAuthorized Error", ex);
            }
            LOG.Debug("ServiceManager.ClientAuthorized out => " + _ret.ToString());
            return _ret;
        }

        /// <summary>
        /// Added 29-04-2016 para chequeo de verificación de clientes que se conectan
        /// </summary>
        /// <param name="oparamin"></param>
        /// <returns></returns>
        internal static bool ClientAuthorized(int companyid, string clientid, out string msg)
        {
            bool _ret = false;
            msg = "";
            try
            {
                LOG.Debug("ServiceManager.ClientAuthorized - TipoCheck = " + Global.CONFIG.AutoRegisterClientConnected.ToString()
                           + " - CompanyId = " + companyid.ToString()  + " - ClientId = " + clientid);

                _ret = DatabaseHelper.ClientAuthorized(companyid, clientid, out msg);
            }
            catch (Exception ex)
            {
                LOG.Error("ServiceManager.ClientAuthorized Error", ex);
            }
            LOG.Debug("ServiceManager.ClientAuthorized out => " + _ret.ToString());
            return _ret;
        }

        public static int ParsePDF417(string pdf417, out BioPortal.Server.Api.Model.PDF417 datos)
        {
            datos = new BioPortal.Server.Api.Model.PDF417();
            byte[] pdf = null;
            int ret = 0;
            try
            {
                LOG.Debug("ServiceManager.ParsePDF417 IN...");
                if (!string.IsNullOrEmpty(pdf417))
                {
                    pdf = Convert.FromBase64String(pdf417);
                    datos.valueid = Encoding.UTF7.GetString(pdf, 0, 9).Replace('\0', ' ').TrimEnd();
                    datos.valueid = datos.valueid.Substring(0, datos.valueid.Length - 1) + "-" +
                                    datos.valueid.Substring(datos.valueid.Length - 1, 1);
                    datos.lastname = Encoding.UTF7.GetString(pdf, 19, 30).Replace('\0', ' ').TrimEnd();
                    datos.nationality = Encoding.UTF7.GetString(pdf, 49, 3).Replace('\0', ' ').TrimEnd();
                    datos.serialnumber = Encoding.UTF7.GetString(pdf, 58, 10).Replace('\0', ' ').TrimEnd();
                    datos.expirationdate = Encoding.UTF7.GetString(pdf, 52, 6);

                    //string aa = Encoding.UTF7.GetString(pdf, 52, 2);
                    //string mm = Encoding.UTF7.GetString(pdf, 54, 2);
                    //string dd = Encoding.UTF7.GetString(pdf, 56, 2);
                    //string expiracion = dd + "/" + mm + "/" + aa;

                    int finger = (pdf[73] << 24) + (pdf[72] << 16) + (pdf[71] << 8) + (pdf[70]);
                    datos.finger = finger.ToString();
                    //int pcLen = (pdf[77] << 24) + (pdf[76] << 16) + (pdf[75] << 8) + (pdf[74]);
                    //byte[] pc1 = new Byte[400];
                    //Buffer.BlockCopy(pdf, 78, pc1, 0, pcLen);

                    //datos = rut + "|" +
                    //        apellido + "|" +
                    //        pais + "|" +
                    //        numeroDeSerie + "|" +
                    //        expiracion + "|" +
                    //        appNumber + "|" +
                    //        disability + "|" +
                    //        docType + "|" +
                    //        finger.ToString() + "|" +
                    //        Convert.ToBase64String(pc1) + "|" +
                    //        pcLen.ToString();
                    ret = 0;
                }
                else
                {
                    LOG.Error("ServiceManager.ParsePDF417 - PDF417 ingreado null");
                    ret = Errors.IERR_BAD_PARAMETER;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("ServiceManager.ParsePDF417 Err - " + ex.Message);
            }
            LOG.Debug("ServiceManager.ParsePDF417 OUT! => ret = " + ret.ToString());
            return ret;
        }


        #endregion Help


        #region Sync

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oparamin"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        public static int Sync(XmlParamIn oparamin, out string xmlparamout)
        {

            XmlParamOut paramout = new XmlParamOut();
            string msg;
            int iErr = Errors.IERR_OK;
            DateTime start, end;
            BpTx txcreated;
            //BpIdentity con la que se trabajará
            BpIdentity oBpIdentity;
            Parameters oparamEnroll;
            DynamicData ddata = null;
            BpIdentity identity = null;
            int operationOrder = OperationOrder.OPERATIONORDER_LOCALONLY;
            string xmlparamgetout = null;

            try
            {
                start = DateTime.Now;

                //0. Determino cual es la operation order entre param y global
                //// 1.- Genero BpIdentity desde PersonalData y la lleno con datos desde XmlParamIn. 
                ////     Si o si, chequeo que exista, sino retorno error
                iErr = DatabaseHelper.GetIdentity(true, oparamin.Actionid, oparamin.Companyid, oparamin.PersonalData,
                                                  out oBpIdentity, true);

                //Si da error retorno con error
                if (iErr != Errors.IERR_OK)
                {
                    msg = "Problemas para obtener una identidad (typeid/valueid = " +
                            oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid;
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(iErr, operationOrder, oparamin, null,
                                            start, end, out oparamEnroll, out txcreated, out identity);
                    return iErr;
                }
                else
                {
                    //Genero salida de Data Biometrica de acuerdo a parámetros
                    string xmlIdentityOut = null;
                    iErr = DatabaseHelper.GetBirsFroSync(oparamin.Authenticationfactor,
                                                         oparamin.Minutiaetype, oparamin.Bodypart, 
                                                         oBpIdentity,
                                                         out xmlIdentityOut,
                                                         out msg);
                    if (iErr != Errors.IERR_OK)
                    {
                        msg = "Problemas para serializar la informacion (typeid/valueid = " +
                                oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid;
                        LOG.Warn(msg);
                        paramout.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                        end = DateTime.Now;
                        DatabaseHelper.SaveTx(iErr, operationOrder, oparamin, null,
                                                start, end, out oparamEnroll, out txcreated, out identity);
                        return iErr;
                    } else
                    {
                        ddata = new DynamicData();
                        ddata.AddValue("Identity", xmlIdentityOut);
                    }
                }

                //I.- Grabo transaccion en log para auditorias
                end = DateTime.Now;
                iErr = DatabaseHelper.SaveTx(Errors.IERR_OK, OperationOrder.OPERATIONORDER_LOCALONLY,
                                             oparamin, xmlparamgetout, start, end,
                                             out oparamEnroll, out txcreated, out identity);
                //Si no graba, devuelve error
                if (iErr != Errors.IERR_OK)
                {
                    msg = "No pudo grabar la BpTx";
                    LOG.Warn(msg);
                    paramout.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                    return iErr;
                } //Sino, sigo

                //IV.- Armo salida y retorno
                //GetXML(int action, string trackid, string clientid, string ipenduser,
            //string enduser, int companyid, int userid, DateTime end, int originid, DynamicData additionaldata)
                xmlparamout = XmlParamOut.GetXML(oparamin.Actionid, txcreated.Trackid,
                                   oparamin.Clientid, oparamin.Ipenduser,
                                   oparamin.Enduser, oparamin.Companyid, oparamin.Userid,
                                   end, txcreated.BpOrigin.Id, ddata);
                return Errors.IERR_OK;

            }
            catch (Exception ex)
            {
                msg = "ServiceManager.Get Exception [" + ex.Message + "]";
                LOG.Warn(msg, ex);
                paramout.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(paramout);
                return Errors.IERR_UNKNOWN;
            }
        }
#endregion Sync
    }
    
}