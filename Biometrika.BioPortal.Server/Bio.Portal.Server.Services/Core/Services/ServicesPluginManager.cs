﻿using System;
using Bio.Core.Api;
using Bio.Core.Constant;
using Bio.Portal.Server.Services.Core.Database;
using BioPortal.Server.Api;
using Bio.Core.Matcher.Constant;
using log4net;
using Action = Bio.Core.Matcher.Constant.Action;
using XmlUtils = BioPortal.Server.Api.XmlUtils;
using Innovatrics.AnsiIso;
using Innovatrics.AnsiIso.Enums;
using Bio.Core.Matcher.Interface;
using Bio.Core.Matcher;
using Bio.Portal.Server.Common.Entities.Database;
using Bio.Core.Serialize;
using System.Collections.Generic;

namespace Bio.Portal.Server.Services.Core.Services
{
    /// <summary>
    /// Manager de WS Plugin
    /// </summary>
    public class ServicesPluginManager
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ServicesPluginManager));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlparamin"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        static public int Process(XmlParamIn xmlparamin, out string xmlparamout)
        {
            xmlparamout = null;

            /*
                100 Register Transaction de MoC
                101 Register Transaction de MoC + Enroll

                1000 Convert ISO to ISO Compact
                1001 Convert ISO to ANSI template
                1002 Convert ANSI to ISO template
                1100 Extract ISO from Sample
                1101 Extract ANSI from Sample
                1200 Matcher
                1201 Matcher ISO
                1202 Matcher ANSI

                
            */
            switch (xmlparamin.Actionid)
            {
                case 100:
                case 101:
                    return RegisterTx(xmlparamin, out xmlparamout);
                    break;

                case 1000:
                    return ConvertISOToISOCompact(xmlparamin, out xmlparamout);
                    break;

                case 1100:
                case 1101:
                    return Extract(xmlparamin, out xmlparamout);
                    break;

                case 1200:
                case 1201:
                    return Matcher(xmlparamin, out xmlparamout);
                    break;

                default:
                    break;
            }


            return 0;

        }

        #region Action == 100

        /// <summary>
        /// Permite registrar transaccion realizada fuera, como por ejemplo MoC, y dependiendo si es 
        /// </summary>
        /// <param name="xmlparamin"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        private static int RegisterTx(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /*
               1. Registra transaccion en BD en bp_tx
               2. Auditoria si está habilitada
               3. Si Es 101 = Con Enroll 
                    3.1. Realizo Enroll
               4. Return 
            */
            int iret = -1;
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            string msg = null;
            DateTime start, end;
            int iErr = 0;
            BpIdentity identity = null;
            int operationOrder = Global.CONFIG.OperationOrder;
            string xmlparamverifyout = null;
            DynamicData ddataFromConnector = null;
            PersonalData pdout = null;
            MatcherInstance matcherintance = null;
            IExtractor extractor = null;
            ITemplate templateCurrent = null;
            ITemplate templateCurrentExtracted = null;
            BpTx txcreated;
            Parameters oparamOutVerify;
            DynamicData templateGenerated;

            try
            {
                LOG.Info("ServicesPluginManager.RegisterTx IN...");
                start = DateTime.Now;

                //Added 29-04-2016
                //Si debe chequear serial de device, se fija en tabla bp_devices para esa compañia si esá y está habilitado.
                if (Global.CONFIG.CheckClientConnected == 1)
                {
                    //Added 29-04-2016
                    //Para control de cliente via IP y no x Serial ID del lector
                    if (Global.CONFIG.AutoRegisterClientConnected == 2)
                    {
                        oXmlIn.Clientid = oXmlIn.Ipenduser;
                    }
                    LOG.Debug("ServicesPluginManager.RegisterTx - checking ClientId Autorized [" + oXmlIn.Clientid + "]");
                    if (!ClientAuthorized(oXmlIn, out msg))
                    {
                        msg = "Device no habilitado en la plataforma! [Serial ID = " + oXmlIn.Clientid + " - " +
                                                                       "(Msg=" + msg + ")]";
                        LOG.Warn(msg);
                        oXmlOut.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_CLIENT_NOT_AUTHORIZED;
                    }
                }
                end = DateTime.Now;
                //1. Registra transaccion en BD en bp_tx
                LOG.Debug("ServicesPluginManager.RegisterTx -- DatabaseHelper.SaveTxRegister in...");
                //I.- Grabo transaccion en log para auditorias
                iErr = DatabaseHelper.SaveTxRegister(Errors.IERR_OK, OperationOrder.OPERATIONORDER_LOCALONLY, 
                                             oXmlIn, start, end, out txcreated);
                //Si no graba, devuelve error
                if (iErr != Errors.IERR_OK)
                {
                    msg = "No pudo grabar la BpTx";
                    oXmlOut.Message = msg;
                    LOG.Debug("ServicesPluginManager.RegisterTx -- iErr=" + iErr.ToString() + "[" + msg + "]");
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return iErr;
                } //Sino, sigo
                LOG.Debug("ServicesPluginManager.RegisterTx -- DatabaseHelper.SaveTx out >>    txcreated != null : " + (txcreated != null).ToString());
                if (txcreated != null)
                {
                    LOG.Debug("ServicesPluginManager.RegisterTx -- DatabaseHelper.SaveTxRegister out >>    txcreated.BpOrigin != null : " + (txcreated.BpOrigin != null).ToString());
                    LOG.Debug("ServicesPluginManager.RegisterTx -- DatabaseHelper.SaveTxRegister out >>    txcreated.trackid = " + txcreated.Trackid);
                }
                else
                {
                    LOG.Debug("ServicesPluginManager.RegisterTx -- DatabaseHelper.SaveTxRegister out >>    txcreated = null! (Warn)");
                }


                //II.- Grabo Save Data si aplica (Si es 0 => toma el default
                int isave = oXmlIn.SaveVerified != 0
                                ? oXmlIn.SaveVerified
                                : Global.CONFIG.SaveVerified;
                string msgSaveAudit = null;
                LOG.Debug("ServicesPluginManager.RegisterTx -- Habilita Auditoria [1-Si | 0-no] => isave = " + isave.ToString());
                //Si es = 1 => Se graba
                if (isave == 1)
                {
                    LOG.Debug("ServicesPluginManager.RegisterTx -- DatabaseHelper.SaveDataVerified in...");
                    //Si da error, lo informo pero no bloqueo
                    int ierrsave = DatabaseHelper.SaveDataVerified(oXmlIn, txcreated);
                    if (ierrsave != Errors.IERR_OK)
                    {
                        msgSaveAudit = "Problemas para grabar SaveVerified [" + ierrsave.ToString() + "]";
                        LOG.Warn("ServicesPluginManager.RegisterTx -- " + msgSaveAudit);
                    }
                    else
                    {
                        LOG.Debug("ServicesPluginManager.RegisterTx -- DatabaseHelper.SaveDataVerified OK");
                    }
                }

                //5.Si es ActionId = 101 => es Register y Enroll
                XmlParamIn oXmlAux = null;
                if (oXmlIn.Actionid == 101 && DatabaseHelper.CumpleFiltroToEnroll(oXmlIn, out oXmlAux))
                {
                    LOG.Debug("ServicesPluginManager.RegisterTx -- EnrollAfterRemoteVerification in...");
                    //Si da error, lo informo pero no bloqueo
                    int ierrenroll = EnrollAfterVerification(oXmlAux, txcreated);
                    if (ierrenroll != Errors.IERR_OK)
                    {
                        msg = "Problemas para Enrolar luego de verificación =" + " [" + ierrenroll.ToString() + "]";
                        LOG.Warn("ServicesPluginManager.RegisterTx -- " + msg);
                        oXmlOut.Message = oXmlOut.Message + " - " + msg;
                    }
                    LOG.Debug("ServicesPluginManager.RegisterTx -- ServicesManager.EnrollAfterRemoteVerification OK");
                    LOG.Debug("ServicesPluginManager.RegisterTx -- ServicesManager.EnrollAfterRemoteVerification out >>    txcreated != null : " + (txcreated != null).ToString());
                    LOG.Debug("ServicesPluginManager.RegisterTx -- ServicesManager.EnrollAfterRemoteVerification out >>    txcreated.TrackId = " + txcreated.Trackid);

                    //IV.- Armo salida y retorno
                    //Copio Personaldata enviado al retorno
                    LOG.Debug("ServicesPluginManager.RegisterTx -- copy Personal data in...");
                    pdout = oXmlIn.PersonalData; // ServicesManager.FillPersonalData(oXmlIn, identity);
                    LOG.Debug("ServicesPluginManager.RegisterTx -- FillPersonalData out >>    txcreated != null : " + (txcreated != null).ToString());
                }

                //6. Formateo salida
                xmlparamout = XmlParamOut.GetXML(oXmlIn.Actionid, txcreated.Trackid,
                                  oXmlIn.Authenticationfactor, oXmlIn.Minutiaetype, oXmlIn.Bodypart, 
                                  txcreated.Result, txcreated.Score, txcreated.Threshold,
                                  oXmlIn.Matchingtype, oXmlIn.Clientid, oXmlIn.Ipenduser,
                                  oXmlIn.Enduser, oXmlIn.Companyid, oXmlIn.Userid,
                                  oXmlIn.Companyid, oXmlIn.Userid, pdout, end, 
                                  txcreated.BpOrigin.Id, ddataFromConnector,
                                  oXmlIn.Verifybyconnectorid, null,
                                  null);
                LOG.Debug("ServiceManager.RegisterTx -- End OK!");

            }
            catch (Exception ex)
            {
                msg = "ServicesPluginManager.RegisterTx Exception [" + ex.Message + "]";
                LOG.Warn(msg, ex);
                oXmlOut.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                return Errors.IERR_UNKNOWN;
            }
            LOG.Debug("ServiceManager.RegisterTx OUT!");
            return Errors.IERR_OK;
        }

        #endregion Action == 100



        #region Action == 1000

        //(int companyid, string typeid, string valueid, string serial, string _iso, out string _isoConverted) //Recibe una huella iso en base64
        /// <summary>
        /// Conveirte minucias ISO en ISO Compact
        /// </summary>
        /// <param name="oXmlIn">Parametros de entrada de BioPortal
        ///            En Sample, en CMLParamIN debe venir la minucia ISO
        ///            En AdditionalData se devuelve la minucia ISO Compact si se realizo con exito
        /// </param>
        /// <param name="xmlparamout">Parámetros de Salida de BioPortal</param>
        /// <returns></returns>
        static internal int ConvertISOToISOCompact(XmlParamIn oXmlIn, out string xmlparamout)
        {
            int iret = -1;
            //_isoConverted = null;
            //string msg;
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msg = null;
            DateTime start, end;
            //XmlParamIn oXmlIn = null;
            string _isoConverted = null;

            try
            {
                LOG.Info("ServicesPluginManager.ConvertISOToISOCompact IN...");
                start = DateTime.Now;

                //0.- Chequeo licencia valida
                //if (Global.PLUGIN_PROCESS < 1)
                //{
                //    oXmlOut.Message = "BioPortal_Server_Plugin_Process.ConvertISOToISOCompact Saliendo por licencia inválida [PLUGIN_PROCESS = " +
                //        Global.PLUGIN_PROCESS.ToString();
                //    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    LOG.Info("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact Saliendo por licencia inválida [PLUGIN_PROCESS = " +
                //        Global.PLUGIN_PROCESS.ToString());
                //    return Errors.IERR_LICENSE;
                //}

                ////1.- Deserializo objeto xmlparamin
                //oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                //if (oXmlIn == null)
                //{
                //    oXmlOut.Message = "Error deserializando parametros de entrada";
                //    LOG.Info("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact Error deserializando entrada...");
                //    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IERR_BAD_PARAMETER;
                //}

                //Added 29-04-2016
                //Si debe chequear serial de device, se fija en tabla bp_devices para esa compañia si esá y está habilitado.
                if (Global.CONFIG.CheckClientConnected == 1)
                {
                    //Added 29-04-2016
                    //Para control de cliente via IP y no x Serial ID del lector
                    if (Global.CONFIG.AutoRegisterClientConnected == 2)
                    {
                        oXmlIn.Clientid = oXmlIn.Ipenduser;
                    }
                    LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact - checking ClientId Autorized [" + oXmlIn.Clientid + "]");
                    if (!ClientAuthorized(oXmlIn, out msg))
                    {
                        msg = "Device no habilitado en la plataforma! [Serial ID = " + oXmlIn.Clientid + " - " +
                                                                       "(Msg=" + msg + ")]";
                        LOG.Warn(msg);
                        oXmlOut.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_CLIENT_NOT_AUTHORIZED;
                    }
                }

                ////Chequeo que venga template ISO
                //if (oXmlIn.SampleCollection == null || oXmlIn.SampleCollection.Count == 0 ||
                //    oXmlIn.SampleCollection[0].Data == null || oXmlIn.SampleCollection[0].Data.Length == 0)
                //{
                //    oXmlOut.Message = "Sample collection vacio o template informado (Data) nulo";
                //    LOG.Info("ServicesPluginManager.ConvertISOToISOCompact Error Sample collection vacio o template informado (Data) nulo...");
                //    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IERR_NULL_TEMPLATE;
                //}

                ////Chequeo que venga template ISO
                //if (oXmlIn.Actionid != Bio.Core.Matcher.Constant.Action.ACTION_PLUGIN_ISOTOISOCOMPACT)
                //{
                //    oXmlOut.Message = "Acción indicada no corresponse (Debe ser = " + Bio.Core.Matcher.Constant.Action.ACTION_PLUGIN_ISOTOISOCOMPACT +
                //        " Indicado = " + oXmlIn.Actionid + ")";
                //    LOG.Info("ServicesPluginManager.ConvertISOToISOCompact Error " + oXmlOut.Message);
                //    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IERR_NULL_TEMPLATE;
                //}

                //Added 09-04 - Si llega WSQ o RAW => Extraigo ISO y luego convierto
                string _SampleISO = oXmlIn.SampleCollection[0].Data;
                if (oXmlIn.SampleCollection[0].Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_WSQ ||
                    oXmlIn.SampleCollection[0].Minutiaetype == Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_RAW)
                {
                    LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact - Es WSQ o RAW => Extract a ISO primero...");
                    //1.-Extraigo ISO
                    MatcherInstance matcherintance = null;
                    IExtractor extractor = null;
                    ITemplate templateCurrent = null;
                    ITemplate templateCurrentExtracted = null;

                    LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact -- MatcherInstance.Extractor Sample IN...");
                    matcherintance = Global.MATCHER_MANAGER.GetMatcherInstance(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT,
                                                                               Bio.Core.Matcher.Constant.MinutiaeType.MINUTIAETYPE_ISO_IEC_19794_2_2005);

                    if (matcherintance == null)
                    {
                        msg = "ServicesPluginManager.ConvertISOToISOCompact -- Debe extraer ISO y no hay matcher configurado!";
                        LOG.Error(msg);
                        oXmlOut.Message = msg;
                        end = DateTime.Now;
                        oXmlOut.PersonalData = new PersonalData
                        {
                            Typeid = oXmlIn.PersonalData.Typeid,
                            Valueid = oXmlIn.PersonalData.Valueid
                        };//FillPersonalData(oparamin, identity);
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_MATCHER_NOT_AVAILABLE;
                    }

                    extractor = matcherintance.Extractor;
                    templateCurrent = matcherintance.Template;
                    templateCurrent.AuthenticationFactor = oXmlIn.Authenticationfactor;
                    templateCurrent.MinutiaeType = oXmlIn.SampleCollection[0].Minutiaetype;
                    templateCurrent.SetData = oXmlIn.SampleCollection[0].Data;
                    templateCurrent.AdditionalData = oXmlIn.Additionaldata;
                    LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact -- templateCurrent.AuthenticationFactor = " + templateCurrent.AuthenticationFactor);
                    LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact -- templateCurrent.MinutiaeType = " + templateCurrent.MinutiaeType);
                    LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact -- templateCurrent.Data = " + oXmlIn.SampleCollection[0].Data);
                    LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact -- templateCurrent.AdditionalData = " + templateCurrent.AdditionalData);

                    //extractor.Threshold = extractor.Threshold != oparamin. No viene el THExtractor, ver si lo agrego
                    int iErr = extractor.Extract(templateCurrent, TemplateDestination.TEMPLATEDESTINATION_TOVERIFY,
                                             out templateCurrentExtracted);

                    if (iErr != 0)
                    {
                        msg = "ServicesPluginManager.ConvertISOToISOCompact -- Error extrayendo minucias sample [" + iErr + "]";
                        LOG.Error(msg);
                        oXmlOut.Message = msg;
                        end = DateTime.Now;
                        Parameters oparamOutVerify;
                        BpTx txcreated;
                        BpIdentity identity;
                        DatabaseHelper.SaveTx(Errors.IERR_EXTRACTING, OperationOrder.OPERATIONORDER_LOCALONLY,
                                              oXmlIn, null, start, end, out oparamOutVerify, out txcreated, out identity);
                        oXmlOut.PersonalData = new PersonalData
                        {
                            Typeid = oXmlIn.PersonalData.Typeid,
                            Valueid = oXmlIn.PersonalData.Valueid
                        };//FillPersonalData(oparamin, identity);
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_EXTRACTING;
                    }
                    else  //Extrajo bien armo salida
                    {
                        LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact -- Template ISO generado ok!");
                        _SampleISO = templateCurrentExtracted.GetData;
                        LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact -- TemplateISO = " + _SampleISO);
                    }
                } else
                {
                    LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact - Formato ingresado es ISO => Sigue proceso de convert...");
                }


                LOG.Debug("ServicesPluginManager.ISOToISOC Param ISO IN = " + _SampleISO);
                Byte[] _isoTemplate = Convert.FromBase64String(_SampleISO);

                LOG.Debug("ServicesPluginManager.ISOToISOC IEngine initializing...");
                //IEngine.Init(); //Inicialización Innovatrics
                IEngine iEngine = IEngine.Instance;
                Iso iso = Iso.Instance;
                iEngine.Init();
                LOG.Debug("BioPortal_Server_Plugin_Process.ISOToISOC IEngine initializated!");

                byte[] _temp = new Byte[4000];
                Int32 _isoLength = 0, _isocLength = 0;

                LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact ConvertToISOCardCC IN...");
                // iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
                //LOG.Debug("BioPortal_Server_Plugin_Process.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

                //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

                //LOG.Debug("BioPortal_Server_Plugin_Process.ISOToISOC ConvertToISOCardCC IN 2...");
                byte[] _isocTemplateWrapped = iso.ConvertToISOCardCC(_isoTemplate, 63, IEngineSortOrder.SortNone, IEngineSortOrder.SortXAsc);
                LOG.Debug("ServicesPluginManager.ISOToISOC ConvertToISOCardCC OUT!");

                iEngine.Terminate(); //Finalización Innovatrics
                LOG.Debug("BioPortal_Server_Plugin_Process.ConvertISOToISOCompact IEngine Terminated!");

                LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact Starting Reorder Minutiae...");
                //Re-ordenamiento de minucias
                for (Int32 n = 0; n < _isocTemplateWrapped.Length; n++)
                {
                    _temp[n] = _isocTemplateWrapped[n + 28];

                    if (n > 0 && _temp[n - 1] == 0x00)
                    {
                        _isocLength = n;

                        break;
                    }
                }

                _isocLength = _isocLength - 1;

                Byte[] _isocTemplate = new Byte[_isocLength];

                for (Int32 n = 0; n < _isocLength; n++)
                {
                    _isocTemplate[n] = _temp[n];
                }
                LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact Reorder Minutiae OUT!");

                _isoConverted = BitConverter.ToString(_isocTemplate);
                LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact _isoConverted = " + _isoConverted);
                end = DateTime.Now;

                ////LLeno Salida
                //oXmlOut.Actionid = oXmlIn.Actionid;
                //oXmlOut.Trackid = Guid.NewGuid().ToString("N"); 
                //oXmlOut.Authenticationfactor = oXmlIn.Authenticationfactor;
                //oXmlOut.Minutiaetype = oXmlIn.Minutiaetype;
                //oXmlOut.Bodypart = oXmlIn.Bodypart;
                //oXmlOut.Clientid = oXmlIn.Clientid;
                //oXmlOut.Ipenduser = oXmlIn.Ipenduser;
                //oXmlOut.Enduser = oXmlIn.Enduser;
                //oXmlOut.Companyid = oXmlIn.Companyid;
                //oXmlOut.Userid = oXmlIn.Userid;
                //oXmlOut.PersonalData = oXmlIn.PersonalData;
                //oXmlOut.Timestampend = DateTime.Now;
                //oXmlOut.Origin = oXmlIn.Origin;
                //DynamicData adata = new DynamicData();
                //adata.AddValue("ISOCompactTemplate", _isoConverted);
                //oXmlOut.Additionaldata = adata;
                //  //Serializo
                //xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                iret = 0;

            }
            catch (Exception ex)
            {
                oXmlOut.Message = "ServicesPluginManager.ConvertISOToISOCompact Error [" + ex.Message + "]";
                LOG.Error(oXmlOut.Message);
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                return Errors.IERR_UNKNOWN;
            }

            Common.Entities.Database.BpTx bptx = null;
            try
            {
                iret = DatabaseHelper.SaveTx(iret, 1, oXmlIn, false, start, end, out bptx);

                DynamicData adata = new DynamicData();
                adata.AddValue("ISOCompactTemplate", _isoConverted);
                oXmlOut.Additionaldata = adata;
                xmlparamout = XmlParamOut.GetXML(oXmlIn.Actionid, bptx.Trackid, oXmlIn.Clientid, oXmlIn.Ipenduser, oXmlIn.Ipenduser,
                    oXmlIn.Companyid, oXmlIn.Userid, end, bptx.BpOrigin.Id, adata);


                //II.- Grabo Save Data si aplica (Si es 0 => toma el default
                int isave = oXmlIn.SaveVerified != 0
                                ? oXmlIn.SaveVerified
                                : Global.CONFIG.SaveVerified;
                string msgSaveAudit = null;
                LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact -- HAbilita Auditoria [1-Si | 0-no] => isave = " + isave.ToString());
                //Si es = 1 => Se graba
                if (isave == 1)
                {
                    LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact -- DatabaseHelper.SaveDataVerified in...");
                    //Si da error, lo informo pero no bloqueo
                    int ierrsave = DatabaseHelper.SaveDataVerified(oXmlIn, bptx);
                    if (ierrsave != Errors.IERR_OK)
                    {
                        msgSaveAudit = "Problemas para grabar SaveVerified [" + ierrsave.ToString() + "]";
                        LOG.Warn(msgSaveAudit);
                    }
                    else
                    {
                        LOG.Debug("ServicesPluginManager.ConvertISOToISOCompact -- DatabaseHelper.SaveDataVerified OK");
                    }
                }
            }
            catch (Exception ex)
            {
                oXmlOut.Message = "ServicesPluginManager.ConvertISOToISOCompact Error [" + ex.Message + "]";
                LOG.Error(oXmlOut.Message);
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                return Errors.IERR_SAVING_TX;
            }

            LOG.Info("ServicesPluginManager.ConvertISOToISOCompact OUT!");
            return Errors.IERR_OK;
        }



        #endregion Action == 1000


        #region Action == 1100

        private static int Extract(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /*
               1. Tomar Minutiaetype del paramin para tener el matcher correspondiente
               2. Tomar sample y extraer en template del tipo
               3. Si 
                    3.1. Extract negativo => Retorno error
                    3.2. Extract positivo => Retorno en Base64 + Grabo Tx
            */
            int iret = -1;
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            string msg = null;
            DateTime start, end;
            int iErr = 0;
            BpIdentity identity = null;
            int operationOrder = Global.CONFIG.OperationOrder;
            string xmlparamverifyout = null;
            DynamicData ddataFromConnector = null;
            PersonalData pdout = null;
            MatcherInstance matcherintance = null;
            IExtractor extractor = null;
            ITemplate templateCurrent = null;
            ITemplate templateCurrentExtracted = null;
            BpTx txcreated;
            Parameters oparamOutVerify;
            DynamicData templateGenerated;

            try
            {
                LOG.Info("ServicesPluginManager.Extract IN...");
                start = DateTime.Now;

                //Added 29-04-2016
                //Si debe chequear serial de device, se fija en tabla bp_devices para esa compañia si esá y está habilitado.
                if (Global.CONFIG.CheckClientConnected == 1)
                {
                    //Added 29-04-2016
                    //Para control de cliente via IP y no x Serial ID del lector
                    if (Global.CONFIG.AutoRegisterClientConnected == 2)
                    {
                        oXmlIn.Clientid = oXmlIn.Ipenduser;
                    }
                    LOG.Debug("ServicesPluginManager.Extract - checking ClientId Autorized [" + oXmlIn.Clientid + "]");
                    if (!ClientAuthorized(oXmlIn, out msg))
                    {
                        msg = "Device no habilitado en la plataforma! [Serial ID = " + oXmlIn.Clientid + " - " +
                                                                       "(Msg=" + msg + ")]";
                        LOG.Warn(msg);
                        oXmlOut.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_CLIENT_NOT_AUTHORIZED;
                    }
                }

                //1.Tomar Minutiaetype del paramin para tener el matcher correspondiente
                matcherintance =
                        Global.MATCHER_MANAGER.GetMatcherInstance(oXmlIn.Authenticationfactor,
                                                                  oXmlIn.Minutiaetype);
                //    1.1.- Si no existe retorno con error.
                if (matcherintance == null)
                {
                    msg = "No existe Matcher configurado para esta tecnología y minucia [" +
                                       "AF=" + oXmlIn.Authenticationfactor + "-MT=" +
                                       oXmlIn.Minutiaetype + "]";
                    LOG.Warn(msg);
                    oXmlOut.Message = msg;
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(Errors.IERR_MATCHER_NOT_AVAILABLE,
                                          OperationOrder.OPERATIONORDER_DEFAULT, oXmlIn, null,
                                          start, end, out oparamOutVerify, out txcreated, out identity);
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Debug("ServicesPluginManager.Extract -- GetMatcherInstance NO OK! (matcherintance == null)");
                    return Errors.IERR_MATCHER_NOT_AVAILABLE;
                }
                LOG.Debug("ServicesPluginManager.Extract -- GetMatcherInstance OK!");

                //2.Tomar sample y extraer en template del tipo
                LOG.Debug("ServicesPluginManager.Extract -- MatcherInstance.Extractor Sample IN...");
                extractor = matcherintance.Extractor;
                templateCurrent = matcherintance.Template;
                templateCurrent.AuthenticationFactor = oXmlIn.Authenticationfactor;
                templateCurrent.MinutiaeType = oXmlIn.SampleCollection[0].Minutiaetype;
                templateCurrent.SetData = oXmlIn.SampleCollection[0].Data;
                templateCurrent.AdditionalData = oXmlIn.Additionaldata;
                LOG.Debug("ServicesPluginManager.Extract -- templateCurrent.AuthenticationFactor = " + templateCurrent.AuthenticationFactor);
                LOG.Debug("ServicesPluginManager.Extract -- templateCurrent.MinutiaeType = " + templateCurrent.MinutiaeType);
                LOG.Debug("ServicesPluginManager.Extract -- templateCurrent.Data = " + oXmlIn.SampleCollection[0].Data);
                LOG.Debug("ServicesPluginManager.Extract -- templateCurrent.AdditionalData = " + templateCurrent.AdditionalData);

                //extractor.Threshold = extractor.Threshold != oparamin. No viene el THExtractor, ver si lo agrego
                iErr = extractor.Extract(templateCurrent, TemplateDestination.TEMPLATEDESTINATION_TOVERIFY,
                                         out templateCurrentExtracted);
                //3.1.- Si hay error retorno
                if (iErr != 0)
                {
                    msg = "Error extrayendo minucias sample [" + iErr + "]";
                    LOG.Error(msg);
                    oXmlOut.Message = msg;
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(Errors.IERR_EXTRACTING, OperationOrder.OPERATIONORDER_LOCALONLY,
                                          oXmlIn, null, start, end, out oparamOutVerify, out txcreated, out identity);
                    oXmlOut.PersonalData = new PersonalData
                    {
                        Typeid = oXmlIn.PersonalData.Typeid,
                        Valueid = oXmlIn.PersonalData.Valueid
                    };//FillPersonalData(oparamin, identity);
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_EXTRACTING;
                } else  //Extrajo bien armo salida
                {
                    templateGenerated = new DynamicData();
                    templateGenerated.AddValue("TemplateGenerated", templateCurrentExtracted.GetData);
                }

                end = DateTime.Now;
                LOG.Debug("ServicesPluginManager.Extract -- DatabaseHelper.SaveTx in...");
                //I.- Grabo transaccion en log para auditorias
                iErr = DatabaseHelper.SaveTx(Errors.IERR_OK, OperationOrder.OPERATIONORDER_LOCALONLY, oXmlIn, false, start, end,  out txcreated);
                //Si no graba, devuelve error
                if (iErr != Errors.IERR_OK)
                {
                    msg = "No pudo grabar la BpTx";
                    LOG.Warn(msg);
                    oXmlOut.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return iErr;
                } //Sino, sigo
                LOG.Debug("ServicesPluginManager.Extract -- DatabaseHelper.SaveTx out >>    txcreated != null : " + (txcreated != null).ToString());
                if (txcreated != null)
                {
                    LOG.Debug("ServicesPluginManager.Extract -- DatabaseHelper.SaveTx out >>    txcreated.BpOrigin != null : " + (txcreated.BpOrigin != null).ToString());
                    LOG.Debug("ServicesPluginManager.Extract -- DatabaseHelper.SaveTx out >>    txcreated.trackid = " + txcreated.Trackid);
                } else
                {
                    LOG.Debug("ServicesPluginManager.Extract -- DatabaseHelper.SaveTx out >>    txcreated = null! (Warn)");
                }


                //II.- Grabo Save Data si aplica (Si es 0 => toma el default
                int isave = oXmlIn.SaveVerified != 0
                                ? oXmlIn.SaveVerified
                                : Global.CONFIG.SaveVerified;
                string msgSaveAudit = null;
                LOG.Debug("ServicesPluginManager.Extract -- HAbilita Auditoria [1-Si | 0-no] => isave = " + isave.ToString());
                //Si es = 1 => Se graba
                if (isave == 1)
                {
                    LOG.Debug("ServicesPluginManager.Extract -- DatabaseHelper.SaveDataVerified in...");
                    //Si da error, lo informo pero no bloqueo
                    int ierrsave = DatabaseHelper.SaveDataVerified(oXmlIn, txcreated);
                    if (ierrsave != Errors.IERR_OK)
                    {
                        msgSaveAudit = "Problemas para grabar SaveVerified [" + ierrsave.ToString() + "]";
                        LOG.Warn(msgSaveAudit);
                    }
                    else
                    {
                        LOG.Debug("ServicesPluginManager.Extract -- DatabaseHelper.SaveDataVerified OK");
                    }
                }

                //5.Si es ActionId = 1101 => es Exrtract y Enroll
                XmlParamIn oXmlAux = null;
                if (oXmlIn.Actionid == 1101 && DatabaseHelper.CumpleFiltroToEnroll(oXmlIn, out oXmlAux))
                {
                    LOG.Debug("ServicesPluginManager.Extract -- EnrollAfterRemoteVerification in...");
                    //Si da error, lo informo pero no bloqueo
                    int ierrenroll = EnrollAfterVerification(oXmlAux, txcreated);
                    if (ierrenroll != Errors.IERR_OK)
                    {
                        msg = "Problemas para Enrolar luego de verificación =" + " [" + ierrenroll.ToString() + "]";
                        LOG.Warn(msg);
                        oXmlOut.Message = oXmlOut.Message + " - " + msg;
                    }
                    LOG.Debug("ServicesPluginManager.Extract -- ServicesManager.EnrollAfterRemoteVerification OK");
                    LOG.Debug("ServicesPluginManager.Extract -- ServicesManager.EnrollAfterRemoteVerification out >>    txcreated != null : " + (txcreated != null).ToString());
                    LOG.Debug("ServicesPluginManager.Extract -- ServicesManager.EnrollAfterRemoteVerification out >>    txcreated.TrackId = " + txcreated.Trackid);

                    //IV.- Armo salida y retorno
                    //Copio Personaldata enviado al retorno
                    LOG.Debug("ServicesPluginManager.Extract -- copy Personal data in...");
                    pdout = oXmlIn.PersonalData; // ServicesManager.FillPersonalData(oXmlIn, identity);
                    LOG.Debug("ServicesPluginManager.Extract -- FillPersonalData out >>    txcreated != null : " + (txcreated != null).ToString());
                }

                //6. Formateo salida
                xmlparamout = XmlParamOut.GetXML(oXmlIn.Actionid, txcreated.Trackid,
                                  oXmlIn.Authenticationfactor, oXmlIn.Minutiaetype,
                                  oXmlIn.Bodypart,0,0,0,
                                  oXmlIn.Matchingtype, oXmlIn.Clientid, oXmlIn.Ipenduser,
                                  oXmlIn.Enduser, oXmlIn.Companyid, oXmlIn.Userid,
                                  oXmlIn.Companyid,
                                  oXmlIn.Userid,
                                  pdout, end, txcreated.BpOrigin.Id, ddataFromConnector,
                                  oXmlIn.Verifybyconnectorid, templateGenerated,
                                  null);
                LOG.Debug("ServiceManager.Extract -- End OK!");

            }
            catch (Exception ex)
            {
                msg = "ServicesPluginManager.Extract Exception [" + ex.Message + "]";
                LOG.Warn(msg, ex);
                oXmlOut.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                return Errors.IERR_UNKNOWN;
            }
            return Errors.IERR_OK;
        }


        #endregion Action == 1100

        #region Action == 1200

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlparamin"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        internal static int Matcher(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /*
               1. Tomar Minutiaetype del paramin para tener el matcher correspondiente
               2. Tomar primer sample y extraer en 1er template del tipo
               3. Tomar segundo sample y extraer en 2do template. Crear lista d eun template como si fuera lista de irs desde BD
               4. Matching con IMatcher.Verify
               5. Si 
                    5.1. matching positivo => Enroll normal
                    5.2. matching negativo => Enroll temporal
            */
            int iret = -1;
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            string msg = null;
            DateTime start, end;
            int iErr = 0;
            BpIdentity identity = null;
            int operationOrder = Global.CONFIG.OperationOrder;
            string xmlparamverifyout = null;
            bool toRemote = false;
            DynamicData ddataFromConnector = null;
            IMatcher matcher = null;
            string xmlparamverify = null;
            PersonalData pdout = null;
            MatcherInstance matcherintance = null;
            IExtractor extractor = null;
            ITemplate templateCurrent = null;
            ITemplate templateCurrentExtracted = null;
            ITemplate templateCurrent2 = null;
            ITemplate templateCurrentExtracted2 = null;
            BpTx txcreated;
            Parameters oparamOutVerify;

            try
            {
                LOG.Info("ServicesPluginManager.Matcher IN...");
                start = DateTime.Now;

                //Added 29-04-2016
                //Si debe chequear serial de device, se fija en tabla bp_devices para esa compañia si esá y está habilitado.
                if (Global.CONFIG.CheckClientConnected == 1)
                {
                    //Added 29-04-2016
                    //Para control de cliente via IP y no x Serial ID del lector
                    if (Global.CONFIG.AutoRegisterClientConnected == 2)
                    {
                        oXmlIn.Clientid = oXmlIn.Ipenduser;
                    }
                    LOG.Debug("ServicesPluginManager.Matcher - checking ClientId Autorized [" + oXmlIn.Clientid + "]");
                    if (!ClientAuthorized(oXmlIn, out msg))
                    {
                        msg = "Device no habilitado en la plataforma! [Serial ID = " + oXmlIn.Clientid + " - " +
                                                                       "(Msg=" + msg + ")]";
                        LOG.Warn(msg);
                        oXmlOut.Message = msg;
                        xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IERR_CLIENT_NOT_AUTHORIZED;
                    }
                }

                //1.Tomar Minutiaetype del paramin para tener el matcher correspondiente
                matcherintance =
                        Global.MATCHER_MANAGER.GetMatcherInstance(oXmlIn.Authenticationfactor,
                                                                  oXmlIn.Minutiaetype);
                //    1.1.- Si no existe retorno con error.
                if (matcherintance == null)
                {
                    msg = "No existe Matcher configurado para esta tecnología y minucia [" +
                                       "AF=" + oXmlIn.Authenticationfactor + "-MT=" +
                                       oXmlIn.Minutiaetype + "]";
                    LOG.Warn(msg);
                    oXmlOut.Message = msg;
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(Errors.IERR_MATCHER_NOT_AVAILABLE,
                                          OperationOrder.OPERATIONORDER_DEFAULT, oXmlIn, null,
                                          start, end, out oparamOutVerify, out txcreated, out identity);
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Debug("ServicesPluginManager.Matcher -- GetMatcherInstance NO OK! (matcherintance == null)");
                    return Errors.IERR_MATCHER_NOT_AVAILABLE;
                }
                LOG.Debug("ServicesPluginManager.Matcher -- GetMatcherInstance OK!");

                //2.Tomar primer sample y extraer en 1er template del tipo
                LOG.Debug("ServicesPluginManager.Matcher -- MatcherInstance.Extractor 1er Sample IN...");
                extractor = matcherintance.Extractor;
                templateCurrent = matcherintance.Template;
                templateCurrent.AuthenticationFactor = oXmlIn.Authenticationfactor;
                templateCurrent.MinutiaeType = oXmlIn.SampleCollection[0].Minutiaetype;
                templateCurrent.SetData = oXmlIn.SampleCollection[0].Data;
                templateCurrent.AdditionalData = oXmlIn.Additionaldata;
                LOG.Debug("ServicesPluginManager.Matcher -- templateCurrent.AuthenticationFactor = " + templateCurrent.AuthenticationFactor);
                LOG.Debug("ServicesPluginManager.Matcher -- templateCurrent.MinutiaeType = " + templateCurrent.MinutiaeType);
                LOG.Debug("ServicesPluginManager.Matcher -- templateCurrent.Data = " + oXmlIn.SampleCollection[0].Data);
                LOG.Debug("ServicesPluginManager.Matcher -- templateCurrent.AdditionalData = " + templateCurrent.AdditionalData);

                //extractor.Threshold = extractor.Threshold != oparamin. No viene el THExtractor, ver si lo agrego
                iErr = extractor.Extract(templateCurrent, TemplateDestination.TEMPLATEDESTINATION_TOVERIFY,
                                         out templateCurrentExtracted);
                //Si hay error retorno
                if (iErr != 0)
                {
                    msg = "Error extrayendo minucias primer sample [" + iErr + "]";
                    LOG.Error(msg);
                    oXmlOut.Message = msg;
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(Errors.IERR_EXTRACTING, OperationOrder.OPERATIONORDER_LOCALONLY,
                                          oXmlIn, null, start, end, out oparamOutVerify, out txcreated, out identity);
                    oXmlOut.PersonalData = new PersonalData
                                            {
                                                Typeid = oXmlIn.PersonalData.Typeid,
                                                Valueid = oXmlIn.PersonalData.Valueid
                                            };//FillPersonalData(oparamin, identity);
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_EXTRACTING;
                }

                //3.Tomar segundo sample y extraer en 2do template. Crear lista d eun template como si fuera lista de irs desde BD
                LOG.Debug("ServicesPluginManager.Matcher -- MatcherInstance.Extractor 2do Sample IN...");
                extractor = matcherintance.Extractor;
                templateCurrent2 = matcherintance.Template;
                templateCurrent2.AuthenticationFactor = oXmlIn.Authenticationfactor;
                templateCurrent2.MinutiaeType = oXmlIn.SampleCollection[1].Minutiaetype;
                templateCurrent2.SetData = oXmlIn.SampleCollection[1].Data;
                templateCurrent2.AdditionalData = oXmlIn.Additionaldata;
                LOG.Debug("ServicesPluginManager.Matcher -- templateCurrent.AuthenticationFactor = " + templateCurrent2.AuthenticationFactor);
                LOG.Debug("ServicesPluginManager.Matcher -- templateCurrent.MinutiaeType = " + templateCurrent2.MinutiaeType);
                LOG.Debug("ServicesPluginManager.Matcher -- templateCurrent.Data = " + oXmlIn.SampleCollection[1].Data);
                LOG.Debug("ServicesPluginManager.Matcher -- templateCurrent.AdditionalData = " + templateCurrent2.AdditionalData);

                //extractor.Threshold = extractor.Threshold != oparamin. No viene el THExtractor, ver si lo agrego
                iErr = extractor.Extract(templateCurrent2, TemplateDestination.TEMPLATEDESTINATION_TOVERIFY,
                                         out templateCurrentExtracted2);
                //Si hay error retorno
                if (iErr != 0)
                {
                    msg = "Error extrayendo minucias segundo sample [" + iErr + "]";
                    LOG.Error(msg);
                    oXmlOut.Message = msg;
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(Errors.IERR_EXTRACTING, OperationOrder.OPERATIONORDER_LOCALONLY,
                                          oXmlIn, null, start, end, out oparamOutVerify, out txcreated, out identity);
                    oXmlOut.PersonalData = new PersonalData
                                            {
                                                Typeid = oXmlIn.PersonalData.Typeid,
                                                Valueid = oXmlIn.PersonalData.Valueid
                                            };//FillPersonalData(oparamin, identity);
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_EXTRACTING;
                }

                LOG.Debug("ServicesPluginManager.Matcher -- Extractor.Extract = OK");

                //4.Matching con IMatcher.Verify
                   //Instancio Matcher
                LOG.Debug("ServicesPluginManager.Matcher - Matcherintance.Matcher IN...");
                matcher = matcherintance.Matcher;
                //Seteo los parametros de umbrales diferentes si hay diferencias
                matcher.Threshold = oXmlIn.Threshold != 0
                                        ? oXmlIn.Threshold
                                        : matcher.Threshold;
                LOG.Debug("ServicesPluginManager.Matcher - matcher.Threshold = " + matcher.Threshold);
                matcher.MatchingType = oXmlIn.Matchingtype != 0
                                           ? oXmlIn.Matchingtype
                                           : matcher.MatchingType;
                if (matcher.MatchingType != 1 && matcher.MatchingType != 2)
                {
                    matcher.MatchingType = 1;
                }
                LOG.Debug("ServicesPluginManager.Matcher - matcher.MatchingType = " + matcher.MatchingType);

                //Preparo xml de parametros para llamar al Matcher
                Parameters parameters = new Parameters();
                parameters.AddValue("currentTemplate", templateCurrentExtracted.GetData);
                parameters.AddValue("currentTemplateAF", templateCurrentExtracted.AuthenticationFactor);
                parameters.AddValue("currentTemplateMT", templateCurrentExtracted.MinutiaeType);
                parameters.AddValue("MTcurrentTemplateBP", templateCurrentExtracted.BodyPart);
                parameters.AddValue("additionalData", oXmlIn.Additionaldata);

                string xmltemplates = null;
                LOG.Debug("ServicesPluginManager.Matcher -- Convert2doTemplateToListTemplate in...");
                iErr = Convert2doTemplateToListTemplate(templateCurrentExtracted2, out xmltemplates);
                if (iErr == 0)
                {
                    parameters.AddValue("listBaseTemplates", xmltemplates);
                    LOG.Debug("ServicesPluginManager.Matcher -- Convert2doTemplateToListTemplate OK");
                }
                else
                {
                    msg = "Error serializando lista templates para verificar [iErr=" + iErr + "]";
                    LOG.Warn(msg);
                    oXmlOut.Message = msg;
                    end = DateTime.Now;
                    DatabaseHelper.SaveTx(Errors.IERR_EXTRACTING, OperationOrder.OPERATIONORDER_LOCALONLY,
                                           oXmlIn, null, start, end, out oparamOutVerify, out txcreated, out identity);
                    oXmlOut.PersonalData = new PersonalData
                                            {
                                                Typeid = oXmlIn.PersonalData.Typeid,
                                                Valueid = oXmlIn.PersonalData.Valueid
                                            };//FillPersonalData(oparamin, identity);
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_SERIALIZING_DATA;
                }

                //Llamo a Matcher Verify
                LOG.Debug("ServicesPluginManager.Matcher -- Parameters.SerializeToXml(parameters) in...");
                xmlparamverify = Parameters.SerializeToXml(parameters);
                LOG.Debug("ServicesPluginManager.Matcher -- Parameters.SerializeToXml(parameters) out");
                LOG.Debug("ServicesPluginManager.Matcher -- xmlparamverify = " + xmlparamverify);

                start = DateTime.Now;

                LOG.Debug("ServicesPluginManager.Matcher -- matcher.Verify in...");
                iErr = matcher.Verify(xmlparamverify, out xmlparamverifyout);
                LOG.Debug("ServiceManager.Matcher -- matcher.Verify iErr = " + iErr.ToString());
                LOG.Debug("ServiceManager.Matcher -- out xmlparamverifyout = " + xmlparamverifyout);
                end = DateTime.Now;

                //                            end = DateTime.Now;
                LOG.Debug("ServicesPluginManager.Matcher -- DatabaseHelper.SaveTx in...");
                //I.- Grabo transaccion en log para auditorias
                iErr = DatabaseHelper.SaveTxMatcher(Errors.IERR_OK, oXmlIn, xmlparamverifyout, start, end,
                                             out oparamOutVerify, out txcreated);
                //Si no graba, devuelve error
                if (iErr != Errors.IERR_OK)
                {
                    msg = "No pudo grabar la BpTx";
                    LOG.Warn(msg);
                    oXmlOut.Message = msg;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return iErr;
                } //Sino, sigo
                LOG.Debug("ServicesPluginManager.Matcher -- DatabaseHelper.SaveTx out >>    txcreated != null : " + (txcreated != null).ToString());
                if (txcreated != null)
                {
                    LOG.Debug("ServicesPluginManager.Matcher -- DatabaseHelper.SaveTx out >>    txcreated.BpOrigin != null : " + (txcreated.BpOrigin != null).ToString());
                }
                LOG.Debug("ServicesPluginManager.Matcher -- DatabaseHelper.SaveTx out >>    oparamOutVerify != null : " + (oparamOutVerify != null).ToString());

                //II.- Grabo Save Data si aplica (Si es 0 => toma el default
                int isave = oXmlIn.SaveVerified != 0
                                ? oXmlIn.SaveVerified
                                : Global.CONFIG.SaveVerified;
                string msgSaveAudit = null;
                LOG.Debug("ServicesPluginManager.Matcher -- HAbilita Auditoria [1-Si | 0-no] => isave = " + isave.ToString());
                //Si es = 1 => Se graba
                if (isave == 1)
                {
                    LOG.Debug("ServicesPluginManager.Verify -- DatabaseHelper.SaveDataVerified in...");
                    //Si da error, lo informo pero no bloqueo
                    int ierrsave = DatabaseHelper.SaveDataVerified(oXmlIn, txcreated);
                    if (ierrsave != Errors.IERR_OK)
                    {
                        msgSaveAudit = "Problemas para grabar SaveVerified [" + ierrsave.ToString() + "]";
                        LOG.Warn(msgSaveAudit);
                    } else
                    {
                        LOG.Debug("ServicesPluginManager.Matcher -- DatabaseHelper.SaveDataVerified OK");
                    }
                }

                //5.Si es ActionId = 1201 => es Match y Enroll
                //     5.1.matching positivo => Enroll normal
                //     5.2.matching negativo => Enroll temporal (No implementado)
                //     Solo si es Result=1 es decir matching positivo 
                //        y (10/2019) cumple con filtro de enrolamiento, que chequea que tenga ya BIRs del mismo 
                //          tipo que envairon ahora, y con un tiempo configurable, para no agrander demasiado la BD 
                XmlParamIn oXmlAux = null;
                if (oXmlIn.Actionid == 1201 && 
                    (txcreated != null && txcreated.Result == 1) &&
                    DatabaseHelper.CumpleFiltroToEnroll(oXmlIn, out oXmlAux))  
                {
                    LOG.Debug("ServicesPluginManager.Matcher -- EnrollAfterRemoteVerification in...");
                    //Si da error, lo informo pero no bloqueo
                    int ierrenroll = EnrollAfterVerification(oXmlAux, txcreated);
                    if (ierrenroll != Errors.IERR_OK)
                    {
                        msg = "Problemas para Enrolar luego de verificación =" + " [" + ierrenroll.ToString() + "]";
                        LOG.Warn(msg);
                        oXmlOut.Message = oXmlOut.Message + " - " + msg;
                    }
                    LOG.Debug("ServicesPluginManager.Matcher -- ServicesManager.EnrollAfterRemoteVerification OK");
                    LOG.Debug("ServicesPluginManager.Matcher -- ServicesManager.EnrollAfterRemoteVerification out >>    txcreated != null : " + (txcreated != null).ToString());
                    LOG.Debug("ServicesPluginManager.Matcher -- ServicesManager.EnrollAfterRemoteVerification out >>    oparamOutVerify != null : " + (oparamOutVerify != null).ToString());

                    //IV.- Armo salida y retorno
                    //Copio Personaldata enviado al retorno
                    LOG.Debug("ServicesPluginManager.Matcher -- copy Personal data in...");
                    pdout = oXmlIn.PersonalData; // ServicesManager.FillPersonalData(oXmlIn, identity);
                    LOG.Debug("ServicesPluginManager.Matcher -- FillPersonalData out >>    txcreated != null : " + (txcreated != null).ToString());
                    LOG.Debug("ServicesPluginManager.Matcher -- FillPersonalData out >>    oparamOutVerify != null : " + (oparamOutVerify != null).ToString());


                    LOG.Debug("ServicesPluginManager.Matcher -- XmlParamOut.GetXML in...");
                    LOG.Debug("ServicesPluginManager.Matcher -- XmlParamOut.GetXML   Param...");
                    LOG.Debug("ServicesPluginManager.Matcher -- XmlParamOut.GetXML      oparamin != null : " + (oXmlIn != null).ToString());
                    LOG.Debug("ServicesPluginManager.Matcher -- XmlParamOut.GetXML      txcreated != null : " + (txcreated != null).ToString());
                    if (txcreated != null)
                    {
                        LOG.Debug("ServicesPluginManager.Matcher -- XmlParamOut.GetXML      txcreated.BpOrigin != null : " + (txcreated.BpOrigin != null).ToString());
                    }
                    LOG.Debug("ServicesPluginManager.Matcher -- XmlParamOut.GetXML      oparamOutVerify != null : " + (oparamOutVerify != null).ToString());
                    LOG.Debug("ServicesPluginManager.Matcher -- XmlParamOut.GetXML      ddataFromConnector != null : " + (ddataFromConnector != null).ToString());
                    LOG.Debug("ServicesPluginManager.Matcher -- XmlParamOut.GetXML      pdout != null : " + (pdout != null).ToString());
                    LOG.Debug("ServicesPluginManager.Matcher -- XmlParamOut.GetXML      end : " + end.ToString());
                   
                }

                //6. Formateo salida
                xmlparamout = XmlParamOut.GetXML(oXmlIn.Actionid, txcreated.Trackid,
                                  oXmlIn.Authenticationfactor, oXmlIn.Minutiaetype,
                                  Convert.ToInt32(oparamOutVerify.GetValue("bodypart")),
                                  Convert.ToInt32(oparamOutVerify.GetValue("result")),
                                  Convert.ToDouble(oparamOutVerify.GetValue("score")),
                                  Convert.ToDouble(oparamOutVerify.GetValue("threshold")),
                                  oXmlIn.Matchingtype, oXmlIn.Clientid, oXmlIn.Ipenduser,
                                  oXmlIn.Enduser, oXmlIn.Companyid, oXmlIn.Userid,
                                  oXmlIn.Companyid,
                                  oXmlIn.Userid,
                                  pdout, end, txcreated.BpOrigin.Id, ddataFromConnector,
                                  oXmlIn.Verifybyconnectorid,
                                  (string)oparamOutVerify.GetValue("BioSignature"));
                LOG.Debug("ServiceManager.Verify -- End OK!");
                
            }
            catch (Exception ex)
            {
                msg = "ServicesPluginManager.Matcher Exception [" + ex.Message + "]";
                LOG.Warn(msg, ex);
                oXmlOut.Message = msg;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                return Errors.IERR_UNKNOWN;
            }
            return Errors.IERR_OK;
        }

        /// <summary>
        /// Enrola o Modifica una identidad que fue previamente verificado via algun conector externo.
        /// </summary>
        /// <param name="oparamin">Parametros iniciales </param>
        /// <param name="txcreated"></param>
        /// <returns></returns>
        private static int EnrollAfterVerification(XmlParamIn oparamin, BpTx txcreated)
        {
            int ierr = Errors.IERR_OK;
            string oput;
            try
            {
                if (txcreated == null)
                {
                    LOG.Error("ServicesManager.EnrollAfterRemoteVerification txcreated = null");
                    return Errors.IERR_BAD_PARAMETER;
                }

                //if (txcreated.Operationsource != Source.SOURCE_REMOTE)
                //{
                //    LOG.Error("ServicesManager.EnrollAfterRemoteVerification Source incorrecto apra Enrolar");
                //    return Errors.IERR_BAD_PARAMETER;
                //}

                XmlParamIn oin = (XmlParamIn)oparamin.Clone();
                oin.Actionid = Action.ACTION_ENROLL;
                oin.OperationOrder = OperationOrder.OPERATIONORDER_LOCALONLY;
                oin.SaveVerified = 2;
                oin.InsertOption = 2;
                DynamicData ddata = new DynamicData();
                DynamicDataItem ddataitem = new DynamicDataItem("source", oin.Verifybyconnectorid);
                ddata.DynamicDataItems = new List<DynamicDataItem> { ddataitem };
                ddataitem = new DynamicDataItem("threshold", txcreated.Threshold.ToString());
                ddata.DynamicDataItems.Add(ddataitem);
                ddataitem = new DynamicDataItem("score", txcreated.Score.ToString());
                ddata.DynamicDataItems.Add(ddataitem);
                ddataitem = new DynamicDataItem("timestamp", txcreated.Timestampend.ToString("dd/MM/yyyy HH:mm:ss"));
                ddata.DynamicDataItems.Add(ddataitem);
                oin.Additionaldata = DynamicData.SerializeToXml(ddata);

                oin.PersonalData.Companyidenroll = oin.Companyid;
                oin.PersonalData.Typeid = oparamin.PersonalData.Typeid;
                oin.PersonalData.Valueid = oparamin.PersonalData.Valueid;
                oin.PersonalData.Name = !String.IsNullOrEmpty(oparamin.PersonalData.Name) ? oparamin.PersonalData.Name : oin.PersonalData.Name;
                oin.PersonalData.Patherlastname = !String.IsNullOrEmpty(oparamin.PersonalData.Patherlastname) ? oparamin.PersonalData.Patherlastname : oin.PersonalData.Patherlastname;
                oin.PersonalData.Motherlastname = !String.IsNullOrEmpty(oparamin.PersonalData.Motherlastname) ? oparamin.PersonalData.Motherlastname : oin.PersonalData.Motherlastname;
                oin.PersonalData.Birthdate = !(oparamin.PersonalData.Birthdate == null) ? oparamin.PersonalData.Birthdate : oin.PersonalData.Birthdate;
                oin.PersonalData.Documentexpirationdate = !(oparamin.PersonalData.Documentexpirationdate == null) ? oparamin.PersonalData.Documentexpirationdate : oin.PersonalData.Documentexpirationdate;
                oin.PersonalData.Birthplace = !String.IsNullOrEmpty(oparamin.PersonalData.Birthplace) ? oparamin.PersonalData.Birthplace : oin.PersonalData.Birthplace;
                oin.PersonalData.Nationality = !String.IsNullOrEmpty(oparamin.PersonalData.Nationality) ? oparamin.PersonalData.Nationality : oin.PersonalData.Nationality;
                oin.PersonalData.Photography = !String.IsNullOrEmpty(oparamin.PersonalData.Photography) ? oparamin.PersonalData.Photography : oin.PersonalData.Photography;
                oin.PersonalData.Signatureimage = !String.IsNullOrEmpty(oparamin.PersonalData.Signatureimage) ? oparamin.PersonalData.Signatureimage : oin.PersonalData.Signatureimage;
                oin.PersonalData.DocImageFront = !String.IsNullOrEmpty(oparamin.PersonalData.DocImageFront) ? oparamin.PersonalData.DocImageFront : oin.PersonalData.DocImageFront;
                oin.PersonalData.DocImageBack = !String.IsNullOrEmpty(oparamin.PersonalData.DocImageBack) ? oparamin.PersonalData.DocImageBack : oin.PersonalData.DocImageBack;
                oin.PersonalData.Visatype = !String.IsNullOrEmpty(oparamin.PersonalData.Visatype) ? oparamin.PersonalData.Visatype : oin.PersonalData.Visatype;

                oin.PersonalData.Verificationsource = oparamin.PersonalData.Verificationsource;
                //if (oparamin.Actionid == Action.ACTION_VERIFY ||
                //    oparamin.Actionid == Action.ACTION_VERIFYANDGET ||
                //    oparamin.Actionid == Action.ACTION_IDENTIFY)
                //{
                //Si es positivo, pongo verificationsource con el valor que viene
                //if (txcreated.Result == 1)
                //{
                //    //oin.PersonalData.Verificationsource = GetVerificationSource(oparamin);
                //    if (String.IsNullOrEmpty(oparamin.PersonalData.Verificationsource))
                //    {
                //        oparamin.PersonalData.Verificationsource = DatabaseHelper.SetVerificationSource(null, oparamin.Authenticationfactor, 0, "A", null);
                //    }
                //    else //Si viene indicado => Update a verificado con esa fuente
                //    {
                //        //Add 03-12 => Tomo identity si existe para mandar el VF correcto
                //        BpIdentity oIdentity = null;
                //        int iret = DatabaseHelper.GetIdentity(false, 7, oin.Companyid, oparamin.PersonalData, out oIdentity, false);
                //        string vf = null;
                //        if (iret == 0 && oIdentity != null)
                //        {
                //            vf = oIdentity.Verificationsource;
                //        }
                //        oin.PersonalData.Verificationsource = DatabaseHelper.SetVerificationSource(vf, oparamin.Authenticationfactor, 1, "A", oparamin.PersonalData.Verificationsource);
                //    }
                //}
                //else  //Si es negativo, pongo verificationsource en nulo para poder revisarlo manualmente. 
                //{
                //    oin.PersonalData.Verificationsource = null;
                //}
                //}

                oin.Verifybyconnectorid = null;

                //Added 17-06-2016 - Chequeo que typeid/valueid/company no exista => Enrolo 
                //                   Sino AddBir
                if (!DatabaseHelper.ExistIdentity(oparamin.Companyid, oparamin.PersonalData.Typeid, oparamin.PersonalData.Valueid))
                {
                    LOG.Debug("ServicesManager.EnrollAfterRemoteVerification - !ExistIdentity => Enroll after verify en : " + oparamin.Verifybyconnectorid);
                    ierr = ServicesManager.Enroll(oin, out oput);
                }
                else
                {
                    oin.Actionid = Action.ACTION_MODIFY; //Action.ACTION_ADDBIR;
                    LOG.Debug("ServicesManager.EnrollAfterRemoteVerification - ExistIdentity => AddBir after verify en : " + oparamin.Verifybyconnectorid);
                    ierr = ServicesManager.Enroll(oin, out oput);
                }

            }
            catch (Exception ex)
            {
                ierr = Errors.IERR_UNKNOWN;
                LOG.Error("ServicesManager.EnrollAfterRemoteVerification Error", ex);
            }
            return ierr;
            
        }

        private static string GetVerificationSource(XmlParamIn oin)
        {
            string ret = null;
            try
            {
                if (oin.Additionaldata != null)
                {
                    DynamicData dd = DynamicData.DeserializeFromXml(oin.Additionaldata);
                    foreach (DynamicDataItem item in dd.DynamicDataItems)
                    {
                        if (item.key.Equals("VerificationSource"))
                        {
                            ret = item.value;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("ServicesManager.GetVerificationSource Error", ex);
            }
            return ret; 
        }

        /// <summary>
        /// Dado un template lo toma como si fuera un bir, y lo serializa como una lista, para pasarlo al Matcher como 2do parametro
        /// </summary>
        /// <param name="templateCurrentExtracted2"></param>
        /// <param name="xmltemplates"></param>
        /// <returns></returns>
        private static int Convert2doTemplateToListTemplate(ITemplate templateCurrentExtracted2, out string xmltemplates)
        {
            int ret;
            List<Template> Templates = new List<Template>();
            xmltemplates = null;
            try
            {
                Template temp;
                temp = new Template();
                temp.Id = 0;
                temp.AuthenticationFactor = templateCurrentExtracted2.AuthenticationFactor;
                temp.MinutiaeType = templateCurrentExtracted2.MinutiaeType;
                temp.BodyPart = templateCurrentExtracted2.BodyPart;
                temp.SetData = Convert.ToBase64String(templateCurrentExtracted2.Data);
                temp.Companyidenroll = 0;
                temp.Useridenroll = 0;
                temp.Verificationsource = null;
                Templates.Add(temp);

                xmltemplates = Bio.Core.Utils.SerializeHelper.SerializeToXml(Templates);
                temp = null;

                ret = Errors.IERR_OK;
            }
            catch (Exception ex)
            {
                LOG.Error("ServicesPluginManager.Convert2doTemplateToListTemplate", ex);
                xmltemplates = null;
                ret = Errors.IERR_UNKNOWN;
            }
            return ret;
        }

#endregion Action == 1200


        #region Helpers

        /// <summary>
        /// Chequea que los parámetros seteados de acuerdo a la operación estén correctos.
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="action"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static bool IsCorrectParamInForAction(XmlParamIn oXmlIn, int action, out string msgerr)
        {
            msgerr = null;
            string msgret = "";
            if (oXmlIn == null)
            {
                msgerr = "Parametros nulos";
                return false;
            }

            LOG.Debug("ServicesPluginManager.IsCorrectParamInForAction IN...");
            LOG.Debug("ServiceManager.IsCorrectParamInForAction - oXmlIn.Actionid = " + oXmlIn.Actionid + " - Action = " + action);
            msgret = ServicesManager.CheckStatusCompanyAndUser(oXmlIn, msgret);
            LOG.Debug("ServiceManager.IsCorrectParamInForAction - CheckStatusCompanyAndUser msgret = " + msgret);
            msgret = ServicesManager.CheckOrigin(oXmlIn, msgret);
            LOG.Debug("ServiceManager.IsCorrectParamInForAction - CheckOrigin msgret = " + msgret);
            switch (action)
            {
                case 100: //Register
                case 101: //Register + Enroll
                    msgerr = msgret;
                    //Minimo debe venir TypeId/ValueId
                    if (oXmlIn.PersonalData == null ||
                        oXmlIn.PersonalData.Typeid == null || oXmlIn.PersonalData.Typeid.Trim().Length == 0 ||
                        oXmlIn.PersonalData.Valueid == null || oXmlIn.PersonalData.Valueid.Trim().Length == 0)
                    {
                        msgret += "|typeid/valueid nulos";
                    }
                    //Chequeo que venga AdditionalData sino no puedo grabar datos de la transaccion por ejemplo result o score
                    if (string.IsNullOrEmpty(oXmlIn.Additionaldata))
                    {
                        msgret += "|Additionaldata nulo";
                    }
                    //Si es con enroll chuequeo que vengan samples apra enrolar sino informo
                    if (action == 101)
                    {
                        if (oXmlIn.SampleCollection == null || oXmlIn.SampleCollection.Count == 0)
                        {
                            msgret = "Samples collection vacio";
                        }
                    }
                    LOG.Debug("ServicesPluginManager.IsCorrectParamInForAction - Case=" + action + "-  msgret = " + msgret);
                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia
                case 1000: //ConvertISOtoISOCompact

                    //Chequeo que venga template ISO
                    if (oXmlIn.Actionid != Bio.Core.Matcher.Constant.Action.ACTION_PLUGIN_ISOTOISOCOMPACT)
                    {
                        msgret = "Acción indicada no corresponse (Debe ser = " + Bio.Core.Matcher.Constant.Action.ACTION_PLUGIN_ISOTOISOCOMPACT +
                            " Indicado = " + oXmlIn.Actionid + ")";
                        //oXmlOut.Message = "Acción indicada no corresponse (Debe ser = " + Bio.Core.Matcher.Constant.Action.ACTION_PLUGIN_ISOTOISOCOMPACT +
                        //    " Indicado = " + oXmlIn.Actionid + ")";
                        //LOG.Info("ServicesPluginManager.ConvertISOToISOCompact Error " + oXmlOut.Message);
                        //xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        //return Errors.IERR_NULL_TEMPLATE;
                    }

                    //Chequeo que venga template ISO
                    if (oXmlIn.SampleCollection == null || oXmlIn.SampleCollection.Count == 0 ||
                        oXmlIn.SampleCollection[0].Data == null || oXmlIn.SampleCollection[0].Data.Length == 0)
                    {
                        msgret = "Sample collection vacio o template informado (Data) nulo";
                    }

                    msgerr = msgret;
                    LOG.Debug("ServicesPluginManager.IsCorrectParamInForAction - Case 1000 msgret = " + msgret);
                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia
                case 1100: //Extract
                    msgerr = msgret;
                    if (oXmlIn.SampleCollection == null || oXmlIn.SampleCollection.Count != 1 ||
                        oXmlIn.SampleCollection[0].Data == null || oXmlIn.SampleCollection[0].Data.Length == 0)
                    {
                        msgret = "Samples collection vacio o template informado (Data) nulo";
                    }
                    LOG.Debug("ServicesPluginManager.IsCorrectParamInForAction - Case 1100 msgret = " + msgret);

                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia
                case 1101: //Extract + Enroll
                    msgerr = msgret;
                    if (oXmlIn.SampleCollection == null || oXmlIn.SampleCollection.Count != 1 ||
                        oXmlIn.SampleCollection[0].Data == null || oXmlIn.SampleCollection[0].Data.Length == 0)
                    {
                        msgret = "Samples collection vacio o template informado (Data) nulo";
                    }
                    if (oXmlIn.PersonalData == null ||
                        oXmlIn.PersonalData.Typeid == null || oXmlIn.PersonalData.Typeid.Trim().Length == 0 ||
                        oXmlIn.PersonalData.Valueid == null || oXmlIn.PersonalData.Valueid.Trim().Length == 0)
                    {
                        msgret += "|typeid/valueid nulos";
                    }
                    LOG.Debug("ServicesPluginManager.IsCorrectParamInForAction - Case 1100 msgret = " + msgret);

                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia
                case 1200: //Matcher
                    msgerr = msgret;
                    if (oXmlIn.SampleCollection == null || oXmlIn.SampleCollection.Count != 2 ||
                        oXmlIn.SampleCollection[0].Data == null || oXmlIn.SampleCollection[0].Data.Length == 0 ||
                        oXmlIn.SampleCollection[1].Data == null || oXmlIn.SampleCollection[1].Data.Length == 0)
                    {
                        msgret = "Samples collection vacio o template informado (Data) nulo";
                    }
                    LOG.Debug("ServicesPluginManager.IsCorrectParamInForAction - Case 1200 msgret = " + msgret);

                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia
                case 1201: //Matcher + Enroll
                    msgerr = msgret;
                    LOG.Debug("ServicesPluginManager.IsCorrectParamInForAction - Case 1201 msgret = " + msgret);
                    if (oXmlIn.SampleCollection == null || oXmlIn.SampleCollection.Count != 2 ||
                        oXmlIn.SampleCollection[0].Data == null || oXmlIn.SampleCollection[0].Data.Length == 0 ||
                        oXmlIn.SampleCollection[1].Data == null || oXmlIn.SampleCollection[1].Data.Length == 0)
                    {
                        msgret = "Samples collection vacio o template informado (Data) nulo";
                    }
                    if (oXmlIn.PersonalData == null ||
                        oXmlIn.PersonalData.Typeid == null || oXmlIn.PersonalData.Typeid.Trim().Length == 0 ||
                        oXmlIn.PersonalData.Valueid == null || oXmlIn.PersonalData.Valueid.Trim().Length == 0)
                    {
                        msgret += "|typeid/valueid nulos";
                    }
                    return msgret.Trim().Length == 0; //Si no hubo error esta vacia

                default:
                    msgerr = "Accion no documentada";
                    LOG.Debug("ServicesPluginManager.IsCorrectParamInForAction - default msgret = " + msgret);

                    return false;
            }
            //LOG.Debug("ServiceManager.IsCorrectParamInForAction OUT!"); 

        }

        /// <summary>
        /// Added 29-04-2016 para chequeo de verificación de clientes que se conectan
        /// </summary>
        /// <param name="oparamin"></param>
        /// <returns></returns>
        internal static bool ClientAuthorized(XmlParamIn oparamin, out string msg)
        {
            bool _ret = false;
            msg = "";
            try
            {
                LOG.Debug("ServicesPluginManager.ClientAuthorized - TipoCheck = " + Global.CONFIG.AutoRegisterClientConnected.ToString()
                           + " - oparamin.ClientId = " + oparamin.Clientid);

                _ret = DatabaseHelper.ClientAuthorized(oparamin.Companyid, oparamin.Clientid, out msg);
            }
            catch (Exception ex)
            {
                LOG.Error("ServicesPluginManager.ClientAuthorized Error", ex);
            }
            LOG.Debug("ServicesPluginManager.ClientAuthorized out => " + _ret.ToString());
            return _ret;
        }

        #endregion Helpers


    }
}