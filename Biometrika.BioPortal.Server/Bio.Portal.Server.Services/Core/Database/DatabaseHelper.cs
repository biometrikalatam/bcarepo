﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bio.Core.Api;
using Bio.Core.Constant;
using Bio.Core.Matcher;
using Bio.Core.Matcher.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Core.Serialize;
using Bio.Portal.Server.Common.Common;
using Bio.Portal.Server.Common.Entities.Database;
using Bio.Portal.Server.Common.Entities;
using BioPortal.Server.Api;
using log4net;
using Action = Bio.Core.Matcher.Constant.Action;
using Newtonsoft.Json;

namespace Bio.Portal.Server.Services.Core.Database
{
    /// <summary>
    /// Clase destinada a resumir todas las rutinas referidas a accesos a BD.
    /// </summary>
    public class DatabaseHelper
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(DatabaseHelper));

        /// <summary>
        /// Chequea que un Origen exista
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        static public bool ExistOrigin(int origin, out string msgerr)
        {
            bool ret = false;
            try
            {
                BpOrigin oOrigin = AdminBpOrigin.Retrieve(origin, out msgerr);
                if (oOrigin != null)
                {
                    ret = true;
                    //oOrigin.Dispose(); 
                }
            }
            catch (Exception ex)
            {
                msgerr = ex.Message;
                LOG.Error("DatabaseHelper.ExistOrigin", ex);
            }
            return ret;
        }

        ///<summary>
        /// Dado los parametros de entrada, recupera desde la BD los birs para comparar.
        ///</summary>
        ///<param name="paramin">Parametros de búsqueda</param>
        ///<param name="list">Lista de Birs para comparaciones</param>
        ///<returns>Codigo de errror</returns>
        static public int GetBirs(XmlParamIn paramin, out List<BpBir> list)
        {
            int res = Errors.IERR_OK;
            list = null;

            try
            {
                LOG.Debug("DatabaseHelper.GetBirs paramin != null = " + (paramin != null).ToString());
                if (paramin != null)
                    res = AdminBpBir.RetrieveWithFilter(0, paramin.Companyid,
                                                        paramin.PersonalData != null ? paramin.PersonalData.Typeid : null,
                                                        paramin.PersonalData != null ? paramin.PersonalData.Valueid : null,
                                                        paramin.Authenticationfactor, paramin.Minutiaetype, paramin.Bodypart,
                                                        Properties.Settings.Default.CHECK_VF, out list);
                if (list != null) LOG.Debug("DatabaseHelper.GetBirs out - list.count = " + list.Count.ToString());
            } catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.GetBirs", ex);
            }
            return res;
        }

        /// <summary>
        /// Dada una lista de Birs, los traduce a un xml de templates para enviar a los Matcher genericamente.
        /// </summary>
        /// <param name="listBirs">Lista de elementos BpBirs</param>
        /// <param name="xmlisttemplates">Xml con lista de templates genericos serializados</param>
        /// <returns></returns>
        static public int ConvertListBirsToListTemplate(List<BpBir> listBirs, out string xmlisttemplates)
        {
            int res;
            xmlisttemplates = null;

            try
            {
                res = AdminBpBir.SerializeListBirsToITemplate(listBirs, out xmlisttemplates);
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.ConvertListBirsToListTemplate", ex);
            }
            return res;
        }

        /// <summary>
        /// Graba Transaccion en la BD
        /// </summary>
        /// <param name="operationcode"></param>
        /// <param name="operationsource"></param>
        /// <param name="oparamin"></param>
        /// <param name="xmlparamoutverify"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="oparamoutverify"></param>
        /// <param name="txcreated"></param>
        /// <param name="identity"></param>
        /// <returns></returns>
        static public int SaveTx(int operationcode, int operationsource, XmlParamIn oparamin,
                                 string xmlparamoutverify, DateTime start, DateTime end,
                                 out Parameters oparamoutverify, out BpTx txcreated,
                                 out BpIdentity identity)
        {
            int res = Errors.IERR_OK;
            int result = 0;
            double score = 0;
            double threshold = 0;
            int bodypart = 0;
            txcreated = null;
            oparamoutverify = null;
            int id = 0;
            string msg, msgaux;
            string typeid;
            string valueid;
            identity = null;
            string abs = null;
            bool isResultOK = false;

            try
            {

                LOG.Error("DatabaseHelper.SaveTx in - xmlparamoutverify = " + xmlparamoutverify);
                //Si es verificacion o identificacion, sino deja to.do en default
                if (xmlparamoutverify != null)
                {
                    oparamoutverify = Parameters.DeserializeFromXml(xmlparamoutverify);
                    LOG.Debug("DatabaseHelper.SaveTx >>    Parameters.DeserializeFromXml(xmlparamoutverify) : oparamoutverify != null : " + (oparamoutverify != null).ToString());
                    if (oparamoutverify == null)
                    {
                        LOG.Error("Error deserializando parametros de salida del verify/identify");
                        return Errors.IERR_DESERIALIZING_DATA;
                    }
                    try
                    {
                        result = Convert.ToInt32(oparamoutverify.GetValue("result"));
                        isResultOK = result == 1; //Seteo para que si hay que hacer ABS => Sepa si debo hacerlo o no
                        score = Convert.ToDouble(oparamoutverify.GetValue("score"));
                        threshold = Convert.ToDouble(oparamoutverify.GetValue("threshold"));
                        bodypart = Convert.ToInt32(oparamoutverify.GetValue("bodypart"));

                        //Add 20-05-2016 - POr Reniec que devuelve datos en la verificacion
                        //if (oparamin.Actionid == Action.ACTION_VERIFY)
                        //{
                        //    string spersonalda = (string)oparamoutverify.GetValue("personaldata");
                        //    if (String.IsNullOrEmpty(spersonalda))
                        //    {
                        //        try
                        //        {
                        //            PersonalData pd = XmlUtils.DeserializeObject<PersonalData>(spersonalda);
                        //            oparaminout.PersonalData.Name = !String.IsNullOrEmpty(pd.Name) ? pd.Name : oparaminout.PersonalData.Name;
                        //            oparaminout.PersonalData.Patherlastname = !String.IsNullOrEmpty(pd.Patherlastname) ? pd.Name : oparaminout.PersonalData.Patherlastname;
                        //            oparaminout.PersonalData.Motherlastname = !String.IsNullOrEmpty(pd.Motherlastname) ? pd.Name : oparaminout.PersonalData.Motherlastname;
                        //            oparaminout.PersonalData.Birthdate = !(pd.Birthdate == null) ? pd.Birthdate : oparaminout.PersonalData.Birthdate;
                        //            oparaminout.PersonalData.Documentexpirationdate = !(pd.Documentexpirationdate == null) ? pd.Documentexpirationdate : oparaminout.PersonalData.Documentexpirationdate;
                        //            oparaminout.PersonalData.Birthplace = !String.IsNullOrEmpty(pd.Birthplace) ? pd.Birthplace : oparaminout.PersonalData.Birthplace;
                        //            oparaminout.PersonalData.Nationality = !String.IsNullOrEmpty(pd.Nationality) ? pd.Name : oparaminout.PersonalData.Nationality;
                        //            oparaminout.PersonalData.Photography = !String.IsNullOrEmpty(pd.Photography) ? pd.Photography : oparaminout.PersonalData.Photography;
                        //            oparaminout.PersonalData.Signatureimage = !String.IsNullOrEmpty(pd.Signatureimage) ? pd.Signatureimage : oparaminout.PersonalData.Signatureimage;
                        //            oparaminout.PersonalData.Visatype = !String.IsNullOrEmpty(pd.Visatype) ? pd.Visatype : oparaminout.PersonalData.Visatype;
                        //        }
                        //        catch (Exception ex)
                        //        {
                        //            LOG.Error("ServiceManager.FormatDynamicDataToParameter Error " + ex.Message);
                        //        }


                        //    }
                        //    LOG.Debug("DatabaseHelper.SaveTx >> " +
                        //          "score = " + score.ToString() +
                        //          "threshold = " + threshold.ToString() +
                        //          "bodypart = " + bodypart.ToString() +
                        //          "id = " + id.ToString());

                        //}


                        if (oparamin.Actionid == Action.ACTION_IDENTIFY)
                        {
                            id = Convert.ToInt32(oparamoutverify.GetValue("id"));
                        }
                        LOG.Debug("DatabaseHelper.SaveTx >> " +
                                  "score = " + score.ToString() +
                                  " - threshold = " + threshold.ToString() +
                                  " - bodypart = " + bodypart.ToString() +
                                  " - id = " + id.ToString());
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("Error tomando resultados de Xml Param Out Verify", ex);
                        return Errors.IERR_DESERIALIZING_DATA;
                    }
                } else //Tomo bodypart desde paramin
                {
                    bodypart = oparamin.Bodypart;
                }

                try
                {
                    if (oparamin.Actionid == Action.ACTION_IDENTIFY)
                    {
                        LOG.Debug("DatabaseHelper.SaveTx >> ACTION_IDENTIFY AdminBpBir.Retrieve - id = " + id.ToString());
                        if (id > 0) //Identifico bien, sino viene 0
                        {
                            BpBir bir = AdminBpBir.Retrieve(id, out msg);
                            if (bir == null || bir.BpIdentity == null)
                            {
                                throw new Exception("Error obteniendo bir/identity id=" + id +
                                                    " [" + msg + "]");
                            }
                            identity = bir.BpIdentity;
                            typeid = bir.BpIdentity.Typeid;
                            valueid = bir.BpIdentity.Valueid;
                        } else
                        {
                            identity = null;
                            typeid = "NA";
                            valueid = "NA";
                        }
                        LOG.Debug("DatabaseHelper.SaveTx >> ACTION_IDENTIFY [typeid = " + typeid +
                                    "/valueid = " + valueid + "]");
                    } else
                    {
                        if (oparamin.Actionid == Action.ACTION_VERIFYANDGET)
                        {
                            LOG.Debug("DatabaseHelper.SaveTx >> ACTION_VERIFYANDGET AdminBpIdentity.Retrieve - " +
                                        "oparamin.Companyid = " + oparamin.Companyid.ToString() +
                                        "oparamin.PersonalData.Typeid = " + oparamin.PersonalData.Typeid +
                                        "oparamin.PersonalData.Valueid = " + oparamin.PersonalData.Valueid);
                            AdminBpIdentity.Retrieve(oparamin.Companyid,
                                                    oparamin.PersonalData.Typeid,
                                                    oparamin.PersonalData.Valueid,
                                                    out identity, out msg);
                            LOG.Debug("DatabaseHelper.SaveTx >> ACTION_VERIFYANDGET AdminBpIdentity.Retrieve " +
                                      " - identity != null : " + (identity != null).ToString());
                        }
                        if (oparamin.PersonalData != null)
                        {
                            typeid = oparamin.PersonalData.Typeid;
                            valueid = oparamin.PersonalData.Valueid;
                        } else
                        {
                            typeid = "FIX";
                            valueid = "GetOp";
                        }
                        LOG.Debug("DatabaseHelper.SaveTx >> not ACTION_IDENTIFY [typeid = " + typeid +
                                    "/valueid = " + valueid + "]");
                    }
                    LOG.Debug("DatabaseHelper.SaveTx >>  Retrieves Out - oparamoutverify != null : " + (oparamoutverify != null).ToString());

                }
                catch (Exception ex)
                {
                    LOG.Error("En Identify, error obteniendo datos de Identidad verificada", ex);
                    return Errors.IERR_DATABASE;
                }

                string trackid = Guid.NewGuid().ToString("N");

                //Added 02-2015 para agregar todas las BpTxConx cuando son varias
                /*
                     Consultationtype > string > nombre metodo
                     Connectorid > string
                     Trackid > string > Trackid externa
                     Status > int > Retorno de servicio 
                     Result > int > 1-Verify Positivo | 2-Verify Negativo (Si es verify si es get 0)
                     Score > double > Score obtenido (Si es verify si es get 0)
                     Threshold > double > Umbral utilizado (Si es verify si es get 0)
                     Timestamp > string > fecha y hora tx externa
                    Devuelve un Dynamicdata de la forma:
                        <DynamicData>
                            <DynamicDataItem>
                                <key>tx1</key>
                                <value>
                                  <DynamicData>
                                    <DynamicDataItem>
                                         ... Cada valor d elos de arriba...                                          
                                    </DynamicDataItem>
                                  </DynamicData>
                                </value>
                            </DynamicDataItem>
                          .........  
                            <DynamicDataItem>
                                <key>txN</key>
                                <value>
                                  <DynamicData>
                                    <DynamicDataItem>
                                         ... Cada valor d elos de arriba...                                          
                                    </DynamicDataItem>
                                  </DynamicData>
                                </value>
                            </DynamicDataItem>
                       </DynamicData>
                 
                   Asi viene= oXmlout.AddValue("externaltxs", "");  //Tipo DynamicData
               */
                //Debo chequear que si hubo transaccion remota, y armar BpTxConx
                List<BpTxConx> listConx = null;
                //Si es source remote, genero BpTxConx
                if (operationsource == Source.SOURCE_REMOTE && oparamoutverify != null && (oparamoutverify.GetValue("externaltxs")!=null))
                {
                    listConx = new List<BpTxConx>();
                    DynamicData ddETXs = DynamicData.DeserializeFromXml((string)oparamoutverify.GetValue("externaltxs"));

                    if (ddETXs != null && ddETXs.DynamicDataItems != null)
                    {
                        foreach (DynamicDataItem item in ddETXs.DynamicDataItems)
                        {
                            DynamicData ddTxEx = DynamicData.DeserializeFromXml((string)item.value);


                            // TODO VEr porque en SRCeI vienen tres Tx y aca viene una sola, y eso rompe esta logica
                            //Chequeo con OR por si es un verify and get o en futuro mas de una verificación. 
                            isResultOK = isResultOK || (Convert.ToInt32(ddTxEx.GetValue("result"))) == 1; //Seteo para que si hay que hacer ABS => Sepa si debo hacerlo o no
                            BpTxConx bpconx = new BpTxConx(null, (string)ddTxEx.GetValue("Consultationtype"),
                                                           (string)ddTxEx.GetValue("Connectorid"),
                                                           (string)ddTxEx.GetValue("Trackid"),
                                                           Convert.ToInt32(ddTxEx.GetValue("Status")),
                                                           Convert.ToInt32(ddTxEx.GetValue("Result")),
                                                           Convert.ToDouble(ddTxEx.GetValue("Score")),
                                                           Convert.ToDouble(ddTxEx.GetValue("Threshold")),
                                                           (string)ddTxEx.GetValue("Timestamp"));
                            listConx.Add(bpconx);
                        }
                    }
                }

                //Added 01-2015 - Advanced Biometric Signature
                if ((oparamin.Actionid == Action.ACTION_VERIFY ||
                    oparamin.Actionid == Action.ACTION_VERIFYANDGET ||
                    oparamin.Actionid == Action.ACTION_IDENTIFY)
                    && Global.CONFIG.BioSignatureType != 0
                    && isResultOK) //Si es != 0 => Hay que generar firma
                {
                    LOG.Debug("Entrando a generar BioSignature...");
                    string strsample;
                    int sampletype;
                    int retget = GetImageFromWSQorTemplate(oparamin.SampleCollection, out strsample, out sampletype);
                    if (retget == 0) {
                        string errdesc;
                        string ddata = "";
                        if (identity != null && oparamin.Actionid == Action.ACTION_VERIFYANDGET)
                        {
                            ddata = "<DynamicDataItem>" +
                                                "<key>TypeId</key>" +
                                                "<value>" + (!String.IsNullOrEmpty(identity.Typeid) ? identity.Typeid : "") + "</value>" +
                                           "</DynamicDataItem>" +
                                           "<DynamicDataItem>" +
                                                "<key>ValueId</key>" +
                                                "<value>" + (!String.IsNullOrEmpty(identity.Valueid) ? identity.Valueid : "") + "</value>" +
                                           "</DynamicDataItem>" +
                                           "<DynamicDataItem>" +
                                                "<key>Name</key>" +
                                                "<value>" + (!String.IsNullOrEmpty(identity.Name) ? identity.Name : "") + "</value>" +
                                           "</DynamicDataItem>" +
                                           "<DynamicDataItem>" +
                                                "<key>Patherlastname</key>" +
                                                "<value>" + (!String.IsNullOrEmpty(identity.Patherlastname) ? identity.Patherlastname : "") + "</value>" +
                                           "</DynamicDataItem>" +
                                           "<DynamicDataItem>" +
                                                "<key>Motherlastname</key>" +
                                                "<value>" + (!String.IsNullOrEmpty(identity.Motherlastname) ? identity.Motherlastname : "") + "</value>" +
                                           "</DynamicDataItem>";
                        }
                        else
                        {
                            ddata = "<DynamicDataItem>" +
                                                "<key>TypeId</key>" +
                                                "<value>" + typeid + "</value>" +
                                           "</DynamicDataItem>" +
                                           "<DynamicDataItem>" +
                                                "<key>ValueId</key>" +
                                                "<value>" + valueid + "</value>" +
                                           "</DynamicDataItem>";
                        }
                        retget = Global.BIOSIGNATURE_FACTORY.GetSignature(Global.CONFIG.BioSignatureType,
                            trackid, strsample, sampletype,
                            operationsource == Source.SOURCE_LOCAL ? "LOCAL" : oparamin.Verifybyconnectorid,
                            identity != null && !String.IsNullOrEmpty(identity.Photography) ? identity.Photography : "",
                            score.ToString(), ddata, out abs, out errdesc);
                        if (retget != Errors.IERR_OK)
                        {
                            LOG.Warn("Error generando BioSignature [" + errdesc + "]. Se almacena BioSignature = null");
                            abs = null;
                        }
                        else  //Agrego XML BioSignature en la salida
                        {
                            string xmlenvelope = null;
                            //Si está habilitado el Notario Virtual => Notarizo
                            if (Properties.Settings.Default.NVEnabled)
                            {
                                retget = Bio.Portal.Server.Services.Core.NV.NVHelper.Notarize(trackid, typeid, valueid, abs, out xmlenvelope);
                                abs = xmlenvelope;
                            }

                            if (retget != Errors.IERR_OK)
                            {
                                LOG.Warn("Error generando BioSignature [" + errdesc + "]. Se almacena BioSignature = null");
                                abs = null;
                            }
                            else {
                                if (oparamoutverify == null) oparamoutverify = new Parameters();
                                oparamoutverify.AddValue("BioSignature", abs);
                                LOG.Debug("BioSignature Generada! [" + abs + "]");
                            }
                        }
                    } else {
                        abs = null;
                    }
                }
                //Grabo primero la Tx sola
                txcreated = AdminBpTx.Create(trackid, operationcode, typeid, valueid,
                                result, score, threshold, start, end,
                                oparamin.Authenticationfactor, oparamin.Minutiaetype, bodypart,
                                oparamin.Actionid, AdminBpOrigin.Retrieve(oparamin.Origin, out msgaux),
                                DateTime.Now, oparamin.Clientid, GetIP(oparamin.Ipenduser), oparamin.Enduser, oparamin.Additionaldata,
                                oparamin.Companyid, oparamin.Userid, operationsource, abs, listConx, out msg);
                LOG.Debug("DatabaseHelper.SaveTx >>   AdminBpTx.Create Out : oparamoutverify != null : " + (oparamoutverify != null).ToString());

                if (txcreated == null)
                {
                    LOG.Error("DatabaseHelper.SaveTx - AdminBpTx.Create [" + msg + "]");
                    res = Errors.IERR_SAVING_TX;
                }
                LOG.Debug("DatabaseHelper.SaveTx >>   AdminBpTx.Create Out : txcreated != null : " + (txcreated != null).ToString());

                //BpTx txcreated2;
                //if (!AdminBpTx.Write(txcreated, out txcreated2, out msg))
                //{
                //    LOG.Error("DatabaseHelper.SaveTx - AdminBpTx.Write [" + msg + "]");
                //    res = Errors.IERR_SAVING_TX;
                //}
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.SaveTx", ex);
            }
            return res;
        }


        /// <summary>
        /// Graba Transaccion en la BD
        /// </summary>
        /// <param name="operationcode"></param>
        /// <param name="oparamin"></param>
        /// <param name="xmlparamoutverify"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="oparamoutverify"></param>
        /// <param name="txcreated"></param>
        /// <returns></returns>
        static public int SaveTxMatcher(int operationcode, XmlParamIn oparamin,
                                 string xmlparamoutverify, DateTime start, DateTime end,
                                 out Parameters oparamoutverify, out BpTx txcreated)
        {
            int res = Errors.IERR_OK;
            int result = 0;
            double score = 0;
            double threshold = 0;
            int bodypart = 0;
            txcreated = null;
            oparamoutverify = null;
            int id = 0;
            string msg, msgaux;
            string typeid;
            string valueid;
            string abs = null;
            bool isResultOK = false;

            try
            {

                LOG.Error("DatabaseHelper.SaveTxMatcher in - xmlparamoutverify = " + xmlparamoutverify);
                //Si es verificacion o identificacion, sino deja to.do en default
                if (xmlparamoutverify != null)
                {
                    oparamoutverify = Parameters.DeserializeFromXml(xmlparamoutverify);
                    LOG.Debug("DatabaseHelper.SaveTxMatcher >>    Parameters.DeserializeFromXml(xmlparamoutverify) : oparamoutverify != null : " + (oparamoutverify != null).ToString());
                    if (oparamoutverify == null)
                    {
                        LOG.Error("Error deserializando parametros de salida del verify/identify");
                        return Errors.IERR_DESERIALIZING_DATA;
                    }
                    try
                    {
                        result = Convert.ToInt32(oparamoutverify.GetValue("result"));
                        isResultOK = result == 1; //Seteo para que si hay que hacer ABS => Sepa si debo hacerlo o no
                        score = Convert.ToDouble(oparamoutverify.GetValue("score"));
                        threshold = Convert.ToDouble(oparamoutverify.GetValue("threshold"));
                        bodypart = oparamin.Bodypart; // Convert.ToInt32(oparamoutverify.GetValue("bodypart"));

                        LOG.Debug("DatabaseHelper.SaveTxMatcher >> TypeId/Valueid = " + oparamin.PersonalData.Typeid + "/" + oparamin.PersonalData.Valueid +
                                  " - score = " + score.ToString() + " - threshold = " + threshold.ToString() +
                                  " - bodypart = " + bodypart.ToString() + " - id = " + id.ToString());
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("Error tomando resultados de Xml Param Out Verify", ex);
                        return Errors.IERR_DESERIALIZING_DATA;
                    }
                }

                string trackid = Guid.NewGuid().ToString("N");

                //Advanced Biometric Signature
                if (Global.CONFIG.BioSignatureType != 0 && isResultOK) //Si es != 0 => Hay que generar firma
                {
                    LOG.Debug("Entrando a generar BioSignature...");
                    string strsample;
                    int sampletype;
                    int retget = GetImageFromWSQorTemplate(oparamin.SampleCollection, out strsample, out sampletype);
                    if (retget == 0)
                    {
                        string errdesc;
                        string ddata = "";
                        if (oparamin.PersonalData != null)
                        {
                            ddata = "<DynamicDataItem>" +
                                                "<key>TypeId</key>" +
                                                "<value>" + (!String.IsNullOrEmpty(oparamin.PersonalData.Typeid) ? oparamin.PersonalData.Typeid : "") + "</value>" +
                                           "</DynamicDataItem>" +
                                           "<DynamicDataItem>" +
                                                "<key>ValueId</key>" +
                                                "<value>" + (!String.IsNullOrEmpty(oparamin.PersonalData.Valueid) ? oparamin.PersonalData.Valueid : "") + "</value>" +
                                           "</DynamicDataItem>" +
                                           "<DynamicDataItem>" +
                                                "<key>Name</key>" +
                                                "<value>" + (!String.IsNullOrEmpty(oparamin.PersonalData.Name) ? oparamin.PersonalData.Name : "") + "</value>" +
                                           "</DynamicDataItem>" +
                                           "<DynamicDataItem>" +
                                                "<key>Patherlastname</key>" +
                                                "<value>" + (!String.IsNullOrEmpty(oparamin.PersonalData.Patherlastname) ? oparamin.PersonalData.Patherlastname : "") + "</value>" +
                                           "</DynamicDataItem>" +
                                           "<DynamicDataItem>" +
                                                "<key>Motherlastname</key>" +
                                                "<value>" + (!String.IsNullOrEmpty(oparamin.PersonalData.Motherlastname) ? oparamin.PersonalData.Motherlastname : "") + "</value>" +
                                           "</DynamicDataItem>";
                        }
                        else
                        {
                            ddata = "<DynamicDataItem>" +
                                                "<key>TypeId</key>" +
                                                "<value>UNKNOW</value>" +
                                           "</DynamicDataItem>" +
                                           "<DynamicDataItem>" +
                                                "<key>ValueId</key>" +
                                                "<value>UNKNOW</value>" +
                                           "</DynamicDataItem>";
                        }
                        retget = Global.BIOSIGNATURE_FACTORY.GetSignature(Global.CONFIG.BioSignatureType,
                            trackid, strsample, sampletype, oparamin.Verifybyconnectorid,
                            oparamin.PersonalData != null && !String.IsNullOrEmpty(oparamin.PersonalData.Photography) ? oparamin.PersonalData.Photography : "",
                            score.ToString(), ddata, out abs, out errdesc);
                        if (retget != Errors.IERR_OK)
                        {
                            LOG.Warn("Error generando BioSignature [" + errdesc + "]. Se almacena BioSignature = null");
                            abs = null;
                        }
                        else  //Agrego XML BioSignature en la salida
                        {
                            string xmlenvelope = null;
                            //Si está habilitado el Notario Virtual => Notarizo
                            if (Properties.Settings.Default.NVEnabled)
                            {
                                retget = Bio.Portal.Server.Services.Core.NV.NVHelper.Notarize(trackid, "UNKNOW", "UNKNOW", abs, out xmlenvelope);
                                abs = xmlenvelope;
                            }

                            if (retget != Errors.IERR_OK)
                            {
                                LOG.Warn("Error generando BioSignature [" + errdesc + "]. Se almacena BioSignature = null");
                                abs = null;
                            }
                            else
                            {
                                if (oparamoutverify == null) oparamoutverify = new Parameters();
                                oparamoutverify.AddValue("BioSignature", abs);
                                LOG.Debug("BioSignature Generada! [" + abs + "]");
                            }
                        }
                    }
                    else
                    {
                        abs = null;
                    }
                }
                //Grabo primero la Tx sola
                txcreated = AdminBpTx.Create(trackid, operationcode,
                                (!String.IsNullOrEmpty(oparamin.PersonalData.Typeid) ? oparamin.PersonalData.Typeid : ""),
                                (!String.IsNullOrEmpty(oparamin.PersonalData.Valueid) ? oparamin.PersonalData.Valueid : ""),
                                result, score, threshold, start, end,
                                oparamin.Authenticationfactor, oparamin.Minutiaetype, bodypart,
                                oparamin.Actionid, AdminBpOrigin.Retrieve(oparamin.Origin, out msgaux),
                                DateTime.Now, oparamin.Clientid, GetIP(oparamin.Ipenduser), oparamin.Enduser, oparamin.Additionaldata,
                                oparamin.Companyid, oparamin.Userid, OperationOrder.OPERATIONORDER_LOCALONLY, abs, null, out msg);
                LOG.Debug("DatabaseHelper.SaveTxMatcher >>   AdminBpTx.Create Out : oparamoutverify != null : " + (oparamoutverify != null).ToString());

                if (txcreated == null)
                {
                    LOG.Error("DatabaseHelper.SaveTxMatcher - AdminBpTx.Create [" + msg + "]");
                    res = Errors.IERR_SAVING_TX;
                }
                LOG.Debug("DatabaseHelper.SaveTxMatcher >>   AdminBpTx.Create Out : txcreated != null : " + (txcreated != null).ToString());

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.SaveTxMatcher", ex);
            }
            return res;
        }


        /// <summary>
        /// Graba trackid para luego usar con el Agente de Notarizacion Asyncronico
        /// </summary>
        /// <param name="idtx"></param>
        /// <returns></returns>
        static public int SaveIdToNotarizeAsync(string idtx)
        {
            int res = Errors.IERR_OK;
            BpTxToNotarize bres;
            string msg;

            try
            {
                if (String.IsNullOrEmpty(idtx))
                    return Errors.IERR_BAD_PARAMETER;

                bres = AdminBpTxToNotarize.Create(idtx, out msg);

                //Si la creo bien
                if (bres != null)
                {
                    res = Errors.IERR_OK;
                }
                else
                {
                    res = Errors.IERR_DATABASE;
                }
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.SaveIdToNotarizeAsync", ex);
            }
            return res;
        }

        /// <summary>
        /// verifica que la IP sea menor de 15 sino es una concatenacion de IPs separadas por ~
        /// por lo que hace split y toma la primera.
        /// </summary>
        /// <param name="ipenduser"></param>
        /// <returns></returns>
        private static string GetIP(string ipenduser)
        {
            string res = "1.1.1.0";
            try
            {
                if (String.IsNullOrEmpty(ipenduser)) return res;

                if (String.IsNullOrEmpty(ipenduser) || ipenduser.Trim().Length > 15)
                {
                    string[] aIPs = ipenduser.Split('~');
                    res = aIPs[0].Trim().Length <= 15 ? aIPs[0].Trim() : "1.1.1.0";
                }
                else
                {
                    res = ipenduser.Trim();
                }
            }
            catch (Exception ex)
            {
                res = "1.1.1.0";
                LOG.Error("DatabaseHelper.GetIP", ex);
            }
            return res;

        }

        ///<summary>
        /// Graba en BD la muestra utilizada para el proceso corriente.
        ///</summary>
        ///<param name="oparamin">Parametros de donde sacar la data</param>
        ///<param name="txcreated">Tx crada para relaiconarla con la data grabada</param>
        ///<returns></returns>
        static public int SaveDataVerified(XmlParamIn oparamin, BpTx txcreated)
        {
            int res = Errors.IERR_OK;

            try
            {
                string msg;

                //Added 12-08-2016 - POr SML, guardar todas las muestras que vienen, para evitar perder info 
                int iCountSaved = 0;
                string msgConcat = "";
                for (int i = 0; i < oparamin.SampleCollection.Count; i++)
                {
                    BpVerified oBpV = AdminBpVerified.Create(oparamin.SampleCollection[i].Minutiaetype,
                                                             oparamin.SampleCollection[i].Data, oparamin.Additionaldata,
                                                             DateTime.Now, txcreated, oparamin.InsertOption,
                                                             oparamin.Matchingtype, out msg);
                    if (oBpV != null) iCountSaved++; //Si grabo la cuento, para despues ver si hubo algun error
                    else msgConcat = "-" + msg;
                }

                //
                if (iCountSaved != oparamin.SampleCollection.Count)
                {
                    LOG.Error("DatabaseHelper.SaveDataVerified - AdminBpVerified.Create [" + msgConcat + "] - Grabadas " +
                               iCountSaved + " de " + oparamin.SampleCollection.Count);
                    res = Errors.IERR_SAVING_VERFIED;
                }
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.SaveTx", ex);
            }
            return res;
        }


        /// <summary>
        /// Obtiene un BpIdentity, de acuerdo a la operacion que se informa. Si es Enroll, se chequea 
        /// primero si existe. Si exsite devuelve error, sino 
        /// </summary>
        /// <param name="checkexist"></param>
        /// <param name="actionid"></param>
        /// <param name="companyid"></param>
        /// <param name="pData"></param>
        /// <param name="oBpIdentity"></param>
        /// <param name="withBirs"></param>
        /// <returns></returns>
        public static int GetIdentity(bool checkexist, int actionid, int companyid,
                                      PersonalData pData, out BpIdentity oBpIdentity, bool withBirs = false)
        {
            int res = Errors.IERR_OK;
            string msg;
            oBpIdentity = null;
            try
            {
                LOG.Debug("DatabaseHelper.GetIdentity IN...");
                res = AdminBpIdentity.Retrieve(companyid, pData.Typeid, pData.Valueid,
                                               out oBpIdentity, out msg, withBirs);
                LOG.Debug("DatabaseHelper.GetIdentity - AdminBpIdentity.Retrieve ret = " + res.ToString());
                //Si es 0 (encontro en el retrieve) y si es Enroll => Error
                if (res == Errors.IERR_OK)
                {
                    //Si no encontro y es Modify o AddBir, saldra con error desde Retrieve
                    if (actionid == Action.ACTION_MODIFY) //|| actionid == Action.ACTION_ADDBIR)
                    {   //Si es modificacion => Hago merge entre lo enviado y lo existente
                        LOG.Debug("DatabaseHelper.GetIdentity - Ingreso a MergeIdentity xq es ActionId = Modify y la Identity Existe...");
                        res = AdminBpIdentity.MergeIdentity(pData, oBpIdentity, out oBpIdentity, out msg);
                        LOG.Debug("DatabaseHelper.GetIdentity - MergeIdentity res = " + res.ToString());
                    }
                    if (actionid == Action.ACTION_ENROLL)
                    {
                        LOG.Debug("DatabaseHelper.GetIdentity - Sale con IERR_IDENTITY_EXIST xs es ActionId = Enroll pero la Identity ya existe");
                        res = Errors.IERR_IDENTITY_EXIST;
                    } //Sino devuelve lo que recupero 
                } else
                {
                    //Si no encontro y es Modify o AddBir, saldra con error desde Retrieve
                    //if (actionid == Action.ACTION_MODIFY) //|| actionid == Action.ACTION_ADDBIR)
                    //{   //Si es modificacion => Hago merge entre lo enviado y lo existente
                    //    res = AdminBpIdentity.MergeIdentity(pData, oBpIdentity, out oBpIdentity, out msg);
                    //}
                    //else 
                    if (actionid == Action.ACTION_ENROLL || actionid == Action.ACTION_ENROLL_FORCED)
                    {  //Si no existe, y es Enroll la creo desde 0
                        LOG.Debug("DatabaseHelper.GetIdentity - Entra a AdminBpIdentity.FillIdentity xw es Enroll y la IDentity no existe...");
                        res = AdminBpIdentity.FillIdentity(companyid, pData, out oBpIdentity, out msg);
                        LOG.Debug("DatabaseHelper.GetIdentity - FillIdentity res = " + res.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.GetIdentity", ex);
            }
            LOG.Debug("DatabaseHelper.GetIdentity OUT!");
            return res;
        }

        public static bool ExistIdentity(int companyid, string typeid, string valueid)
        {
            int res = Errors.IERR_OK;
            bool ret = false;
            string msg;
            BpIdentity oBpIdentity = null;
            try
            {
                LOG.Debug("DatabaseHelper.ExistIdentity In...");
                res = AdminBpIdentity.Retrieve(companyid, typeid, valueid, out oBpIdentity, out msg);
                LOG.Debug("DatabaseHelper.ExistIdentity res = " + res.ToString());
                //Si es 0 (encontro en el retrieve) y si es Enroll => Error
                if (res == Errors.IERR_OK)
                {
                    ret = (oBpIdentity != null);
                    LOG.Debug("DatabaseHelper.ExistIdentity ret = " + ret.ToString());
                }
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                ret = false;
                LOG.Error("DatabaseHelper.ExistIdentity", ex);
            }
            LOG.Debug("DatabaseHelper.ExistIdentity Out!");
            return ret;
        }

        /// <summary>
        /// Recibe un paramin con una lista de samples a enrolar, y verifica de cada uno si
        /// cumple con el filtro. Luego devuelve el mismo paramin depurado. Si la lista de sample es > 0 
        /// => devuelve true y enrola con este nuevo Paramin, sino devuelve false porque todos los birs
        /// ya estan enrolados
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="oXmlOut"></param>
        /// <returns></returns>
        internal static bool CumpleFiltroToEnroll(XmlParamIn oXmlIn, out XmlParamIn oXmlOut)
        {
            oXmlOut = null;
            bool ret = false;
            LOG.Debug("DatabaseHelper.CumpleFiltroToEnroll IN...");
            LOG.Debug("DatabaseHelper.CumpleFiltroToEnroll Verificando TypeId/ValueId = " +
                oXmlIn.PersonalData.Typeid + "/" + oXmlIn.PersonalData.Valueid);
            try
            {
                oXmlOut = (XmlParamIn)oXmlIn.Clone();
                oXmlOut.SampleCollection = new List<Sample>();

                foreach (Sample sampleItem in oXmlIn.SampleCollection)
                {
                    BpBir lastBir = AdminBpBir.RetrieveLastBir(oXmlIn.Companyid, oXmlIn.PersonalData.Typeid,
                                                        oXmlIn.PersonalData.Valueid, oXmlIn.Authenticationfactor, 
                                                        sampleItem.Minutiaetype, sampleItem.BodyPart);

                    //Si es nula lastBir => Se enrola porque no hay muestras anteriroes enroladas
                    //Si no es nula, verifico que sea mayor a Global.CONFIG.DaysFromLastEnroll configurado x defulat 180 dias
                    if ((lastBir == null) ||
                        (lastBir != null && lastBir.Creation.AddDays(Global.CONFIG.DaysFromLastEnroll) < DateTime.Now))
                    {
                        LOG.Debug("DatabaseHelper.CumpleFiltroToEnroll OK - Para enrolar AF = " + oXmlIn.Authenticationfactor +
                            " - MT = " + sampleItem.Minutiaetype + " - BP = " + sampleItem.BodyPart);
                        oXmlOut.SampleCollection.Add(sampleItem);
                    } else 
                    {
                        //oXmlOut.SampleCollection = oXmlIn.SampleCollection;
                        LOG.Debug("DatabaseHelper.CumpleFiltroToEnroll NOOK - No Enrola AF = " + oXmlIn.Authenticationfactor +
                            " - MT = " + sampleItem.Minutiaetype + " - BP = " + sampleItem.BodyPart + " porque no supera los " +
                            Global.CONFIG.DaysFromLastEnroll.ToString() + " dias desde ultimo enroll!");
                    }
                }

                ret = (oXmlOut.SampleCollection != null && oXmlOut.SampleCollection.Count > 0);
             }
            catch (Exception ex)
            {
                LOG.Debug("DatabaseHelper.CumpleFiltroToEnroll Error Inesperado", ex);
                ret = false;
            }
            return ret;
        }

        /// <summary>
        /// Elimina una 
        /// </summary>
        /// <param name="companyid"></param>
        /// <param name="pData"></param>
        /// <returns></returns>
        public static int DeleteIdentity(int companyid, PersonalData pData)
        {
            int res = Errors.IERR_OK;
            string msg;

            try
            {
                res = AdminBpIdentity.Delete(companyid, pData.Typeid, pData.Valueid, out msg);
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.DeleteIdentity", ex);
            }
            return res;
        }


        /// <summary>
        /// Dado un template extraido, se genera un BIR para enrolar
        /// </summary>
        /// <param name="templateCurrentExtracted"></param>
        /// <param name="oBpIdentity"></param>
        /// <param name="companyidenroll"></param>
        /// <param name="useridenroll"></param>
        /// <returns></returns>
        public static int AddBirToIdentity(BpIdentity oBpIdentity, ITemplate templateCurrentExtracted,
                                           int companyidenroll, int useridenroll)
        {
            int res = Errors.IERR_OK;
            string msg;
            BpBir birgenerated;

            try
            {
                LOG.Debug("DatabaseHelper.AddBirToIDentity IN...");
                if (oBpIdentity == null || templateCurrentExtracted == null)
                    return Errors.IERR_BAD_PARAMETER;

                LOG.Debug("DatabaseHelper.AddBirToIDentity Ingresando a AdminBpBir.ConvertTemplateToBir...");
                res = AdminBpBir.ConvertTemplateToBir(null, templateCurrentExtracted,
                                                      companyidenroll, useridenroll, out birgenerated);
                LOG.Debug("DatabaseHelper.AddBirToIDentity AdminBpBir.ConvertTemplateToBir Ret=" + res.ToString());
                //Si la creo bien
                if (res == Errors.IERR_OK)
                {   //Si la lista actual esta en null creo lista
                    if (oBpIdentity.BpBir == null)
                    {
                        oBpIdentity.BpBir = new List<BpBir>();
                        LOG.Debug("DatabaseHelper.AddBirToIDentity - oBpIdentity.BpBir = new List<BpBir>() OK");
                    }

                    //Seteo la Identidad en la Bir
                    birgenerated.BpIdentity = oBpIdentity;
                    LOG.Debug("DatabaseHelper.AddBirToIDentity oBpIdentity seteado en birgenerated...");

                    //Si es AF=Password, elimino cualqueir ocurrencia que haya de ese AF, para que solo haya
                    //un BIR de ese AF
                    if (birgenerated.Authenticationfactor == AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
                    {
                        oBpIdentity.BpBir = DepureBpBirOfPsw(oBpIdentity.BpBir);
                        //oBpIdentity.BpBir = AddOrReplacePassword(oBpIdentity.BpBir, birgenerated);
                    }
                    //else
                    //{
                    //Agrego a la lista
                    oBpIdentity.BpBir.Add(birgenerated);
                    LOG.Debug("DatabaseHelper.AddBirToIDentity BIR Adedd en oBpIdentity.TypeId/ValueId = " + 
                        oBpIdentity.Typeid + "/" + oBpIdentity.Valueid);
                    //}
                }
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.AddBirToIDentity", ex);
            }
            return res;
        }

        private static IList<BpBir> AddOrReplacePassword(IList<BpBir> bpBir, BpBir birgenerated)
        {
            string msg = null;
            IList<BpBir> auxBir = new List<BpBir>();

            try
            {
                if (bpBir == null) return null;

                foreach (BpBir bir in bpBir)
                {
                    if (bir.Authenticationfactor == AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
                    {
                        bir.Data = birgenerated.Data;
                        bir.Timestamp = DateTime.Now;
                        auxBir.Add(bir);
                    } else
                    {
                        auxBir.Add(birgenerated);
                    }

                }

                return auxBir;
            }
            catch (Exception ex)
            {
                LOG.Error("DatabaseHelper.DepureBpBirOfPsw", ex);
                return null;
            }
        }

        private static IList<BpBir> DepureBpBirOfPsw(IList<BpBir> bpBir) //, out IList<BpBir> bpBirToDelete)
        {
            string msg = null;
            IList<BpBir> auxBir = new List<BpBir>();
            //bpBirToDelete = new List<BpBir>(); 
            try
            {
                if (bpBir == null) return null;

                foreach (BpBir bir in bpBir)
                {
                    if (bir.Authenticationfactor == AuthenticationFactor.AUTHENTICATIONFACTOR_PASSWORD)
                    {
                        auxBir.Add(bir);
                    }
                    //else
                    //{     //Si es password, elimino el bir porque debo reemplazarlo por el actual. Solo
                    //      //se mantiene un bir PASSWORD.
                    //    //bool delete = AdminBpBir.Delete(bir.Id, out msg);
                    //    //if (!delete) LOG.Error("DatabaseHelper.DepureBpBirOfPsw - AdminBpBir.Delete[" + 
                    //    //                        bir.Id.ToString() + "]");
                    //    //bpBirToDelete.Add((bir.Id);
                    //}
                }

                foreach (BpBir bir in auxBir)
                {
                    bpBir.Remove(bir);
                }

                return bpBir;
            }
            catch (Exception ex)
            {
                LOG.Error("DatabaseHelper.DepureBpBirOfPsw", ex);
                return null;
            }
        }

        /// <summary>
        /// Graba la Identidad
        /// </summary>
        /// <param name="oBpIdentity">Identidad</param>
        /// <returns>Cod Error si existe o 0</returns>
        public static int SaveIdentity(BpIdentity oBpIdentity)
        {
            int res = Errors.IERR_OK;
            bool bres;
            string msg;
            BpBir birgenerated;

            try
            {
                LOG.Debug("DatabaseHelper.SaveIdentity IN...");

                if (oBpIdentity == null)
                {
                    LOG.Debug("DatabaseHelper.SaveIdentity - Retorna porque oBpIDentitiy = null");
                    return Errors.IERR_BAD_PARAMETER;
                }

                LOG.Debug("DatabaseHelper.SaveIdentity - Grabando Identity => Company/TypeId/ValueId => " +
                          ((oBpIdentity.Companyidenroll!=null)? oBpIdentity.Companyidenroll.Id.ToString():"Null") 
                          + "/" + oBpIdentity.Typeid + "/" + oBpIdentity.Valueid +
                          " - Verificationsource = " +
                          (string.IsNullOrEmpty(oBpIdentity.Verificationsource) ? "NULL" : oBpIdentity.Verificationsource));
                LOG.Debug("DatabaseHelper.SaveIdentity - Ingresando a AdminBpIdentity.Write..."); 
                bres = AdminBpIdentity.Write(oBpIdentity, out msg);
                LOG.Debug("DatabaseHelper.SaveIdentity - AdminBpIdentity.Write Retorno = " + bres.ToString());
                //Si la creo bien
                if (bres)
                {
                    res = Errors.IERR_OK;
                } else
                {
                    res = Errors.IERR_DATABASE;
                }
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.SaveIdentity error ", ex);
            }
            LOG.Debug("DatabaseHelper.SaveIdentity Out [res=" + res.ToString() + "]");
            return res;
        }

        /// <summary>
        /// Dada una identidad, se llena PersonalData para retornar
        /// </summary>
        /// <param name="identity">Identidad recuperada desde BD</param>
        /// <returns>Objeto PersonalData para retornar</returns>
        public static PersonalData FillPersonalDataFromBpIdentity(BpIdentity identity)
        {
            PersonalData pdret = new PersonalData();

            try
            {
                if (identity == null) return null;

                pdret.Id = identity.Id;
                pdret.Nick = identity.Nick;
                pdret.Typeid = identity.Typeid;
                pdret.Valueid = identity.Valueid;
                pdret.Name = identity.Name;
                pdret.Patherlastname = identity.Patherlastname;
                pdret.Motherlastname = identity.Motherlastname;
                pdret.Sex = identity.Sex;
                pdret.Documentseriesnumber = identity.Documentseriesnumber;
                pdret.Documentexpirationdate = identity.Documentexpirationdate.HasValue ?
                    identity.Documentexpirationdate.Value : new DateTime(9999, 01, 01);
                pdret.Visatype = identity.Visatype;
                pdret.Birthdate = identity.Birthdate.HasValue ?
                    identity.Birthdate.Value : new DateTime(1900, 01, 01);
                pdret.Birthplace = identity.Birthplace;
                pdret.Nationality = identity.Nationality;
                pdret.Photography = identity.Photography;
                pdret.DocImageFront = identity.DocImageFront;
                pdret.DocImageBack = identity.DocImageBack;
                pdret.Signatureimage = identity.Signatureimage;
                pdret.Selfie = identity.Selfie;
                pdret.Profession = identity.Profession;
                try
                {
                    LOG.Debug("DatabaseHelper.FillPersonalDataFromBpIdentity identity.Dynamicdata = " + identity.Dynamicdata);
                    if (identity.Dynamicdata != null)
                    {
                        identity.Dynamicdata = identity.Dynamicdata.Replace("?<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
                        LOG.Debug("DatabaseHelper.FillPersonalDataFromBpIdentity identity.Dynamicdata Replaced = " + identity.Dynamicdata);
                    }
                    pdret.Dynamicdata = identity.Dynamicdata != null ?
                            XmlUtils.DeserializeObject<DynamicDataItem[]>(identity.Dynamicdata) : null;
                }
                catch (Exception ex)
                {
                    LOG.Error("DatabaseHelper.FillPersonalDataFromBpIdentity Error XmlUtils.DeserializeObject<DynamicDataItem[]>", ex);
                }

                pdret.Enrollinfo = identity.Enrollinfo;
                pdret.Creation = identity.Creation;
                //pdret.Verificationsource = identity.Verificationsource ?? "NV";
                pdret.Verificationsource = identity.Verificationsource ?? "0A|0A";   //Added 08-2018 para enroll temporal
                pdret.Companyidenroll = identity.Companyidenroll.Id;
                pdret.Useridenroll = identity.Useridenroll;
            }
            catch (Exception ex)
            {
                pdret = null;
                LOG.Error("DatabaseHelper.FillPersonalDataFromBpIdentity", ex);
            }
            return pdret;
        }

        /// <summary>
        /// Dependiendo de la accion pedida, se elimina la información que sobra.
        /// </summary>
        /// <param name="actionid">Una action id de las GET</param>
        /// <param name="personalData">Datos de una identidad</param>
        /// <param name="pdret">Retorno filtrado segun actionid</param>
        /// <returns>Codigo de error</returns>
        public static int SelectDataToReturn(int actionid, PersonalData personalData, out PersonalData pdret)
        {
            pdret = personalData;
            int ret = Errors.IERR_OK;
            try
            {

                switch (actionid)
                {
                    case 10: //SACTION_GETDATA;
                        pdret.Photography = null;
                        pdret.Signatureimage = null;
                        pdret.DocImageFront = null;
                        pdret.DocImageBack = null;
                        pdret.Selfie = null;
                        break;
                    case 11: //SACTION_GETPHOTO;
                        pdret = new PersonalData();
                        pdret.Typeid = personalData.Typeid;
                        pdret.Valueid = personalData.Valueid;
                        pdret.Photography = personalData.Photography;
                        pdret.DocImageFront = null;
                        pdret.DocImageBack = null;
                        pdret.Signatureimage = null;
                        pdret.Selfie = null;
                        break;
                    case 12: //SACTION_GETSIGNATURE;
                        pdret = new PersonalData();
                        pdret.Typeid = personalData.Typeid;
                        pdret.Valueid = personalData.Valueid;
                        pdret.Photography = null;
                        pdret.Signatureimage = personalData.Signatureimage;
                        pdret.DocImageFront = null;
                        pdret.DocImageBack = null;
                        pdret.Selfie = null;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                pdret = null;
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.SelectDataToReturn", ex);
            }
            return ret;
        }

        /// <summary>
        /// Dada la accion correspondoiente genera una salida de paraletros key/value 
        /// </summary>
        /// <param name="opramin">XmlPAramIn original</param>
        /// <param name="oparamOut">Objeto parametro con pares key/value</param>
        /// <param name="msg">Msg error si existe</param>
        /// <returns>Codigo de operacion</returns>
        public static int GetDataOp(XmlParamIn opramin, out DynamicData oparamOut, out string msg)
        {
            int ret = Errors.IERR_OK;
            oparamOut = null;
            msg = null;
            IList list;
            DynamicDataItem item;
            Hashtable ht;

            try
            {
                //static public int ACTION_GETACTIONS = 20;
                //static public int ACTION_GETORIGINS = 21;
                //static public int ACTION_GETMATCHESRAVAILABLES = 22;
                //static public int ACTION_GETCONNECTORSAVAILABLES = 23;
                //static public int ACTION_GETAUTHENTICATIONFACTORSAVAILABLES = 24;
                //static public int ACTION_GETMINUTIAETYPESAVAILABLES = 25;
                switch (opramin.Actionid)
                {
                    case 20: // ACTION_GETACTIONS
                        oparamOut = new DynamicData
                        {
                            DynamicDataItems = new List<DynamicDataItem>()
                        };
                        ht = Action.GetActions();
                        foreach (DictionaryEntry de in ht)
                        {
                            item = new DynamicDataItem
                            {
                                key = de.Key.ToString(),
                                value = de.Value.ToString()
                            };
                            oparamOut.DynamicDataItems.Add(item);
                        }
                        break;
                    case 21: // ACTION_GETORIGINS
                        oparamOut = new DynamicData
                        {
                            DynamicDataItems = new List<DynamicDataItem>()
                        };
                        list = AdminBpOrigin.ListAll;
                        foreach (BpOrigin obj in list)
                        {
                            item = new DynamicDataItem
                            {
                                key = obj.Id.ToString(),
                                value = obj.Description
                            };
                            oparamOut.DynamicDataItems.Add(item);
                        }
                        break;
                    case 22: // ACTION_GETMATCHESRAVAILABLES
                        oparamOut = new DynamicData
                        {
                            DynamicDataItems = new List<DynamicDataItem>()
                        };
                        ht = Global.MATCHER_MANAGER.GetMatchersAvailables();
                        foreach (DictionaryEntry de in ht)
                        {
                            item = new DynamicDataItem
                            {
                                key = de.Key.ToString(),
                                value = ((MatcherInstance)de.Value).Name
                            };
                            oparamOut.DynamicDataItems.Add(item);
                        }
                        break;
                    case 24: // ACTION_GETAUTHENTICATIONFACTORSAVAILABLES

                        oparamOut = new DynamicData
                        {
                            DynamicDataItems = new List<DynamicDataItem>()
                        };
                        Hashtable htAux = Global.MATCHER_MANAGER.GetMatchersAvailables();
                        ht = new Hashtable();

                        foreach (DictionaryEntry de in htAux)
                        {
                            if (!ht.ContainsKey(((MatcherInstance)de.Value).Authenticationfactor.ToString()))
                            {
                                ht.Add(((MatcherInstance)de.Value).Authenticationfactor.ToString(),
                                         AuthenticationFactor.GetName(
                                             ((MatcherInstance)de.Value).Authenticationfactor));
                            }
                        }

                        foreach (DictionaryEntry de in ht)
                        {
                            item = new DynamicDataItem
                            {
                                key = de.Key.ToString(),
                                value = de.Value.ToString()
                            };
                            oparamOut.DynamicDataItems.Add(item);
                        }
                        break;
                    case 25: // ACTION_GETMINUTIAETYPESAVAILABLES
                        oparamOut = new DynamicData
                        {
                            DynamicDataItems = new List<DynamicDataItem>()
                        };
                        ht = Global.MATCHER_MANAGER.GetMatchersAvailables();
                        foreach (DictionaryEntry de in ht)
                        {
                            item = new DynamicDataItem
                            {
                                key = ((MatcherInstance)de.Value).Minutiaetype.ToString(),
                                value = MinutiaeType.GetDescription(((MatcherInstance)de.Value).Minutiaetype)
                            };
                            oparamOut.DynamicDataItems.Add(item);
                        }
                        break;
                    case 26: // ACTION_GETBODYPARTENROLLED
                        oparamOut = new DynamicData
                        {
                            DynamicDataItems = new List<DynamicDataItem>()
                        };
                        string arrListBirsEnrolled = DatabaseHelper.GetBirsEnrolled(opramin, out msg);

                        item = new DynamicDataItem
                        {
                            key = "EnroledList",
                            value = arrListBirsEnrolled
                        };
                        oparamOut.DynamicDataItems.Add(item);
                        
                        break;
                    default:
                        ret = Errors.IERR_INCONSISTENT_ACTION;
                        break;
                }

            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.GetDataOp", ex);
            }
            return ret;
        }

        private static string GetBirsEnrolled(XmlParamIn opramin, out string msg)
        {
            string sRet = null;
            msg = null;
            List<Bio.Core.Api.BpBirApi> arrListBirs = null;
            try
            {
                List<BpBir> listBirs;
                int ret = GetBirs(opramin, out listBirs);

                if (ret == 0)
                {
                    if (listBirs != null && listBirs.Count > 0)
                    {
                        arrListBirs = new List<BpBirApi>(); //List<BioPortal.Server.Sync.Api.BpBir>();
                        foreach (BpBir bpBir in listBirs)
                        {
                            if (opramin.Authenticationfactor == bpBir.Authenticationfactor || opramin.Authenticationfactor == AuthenticationFactor.AUTHENTICATIONFACTOR_All)
                            {
                                if (bpBir.Minutiaetype == opramin.Minutiaetype || opramin.Minutiaetype == MinutiaeType.MINUTIAETYPE_ALL)
                                {
                                    //if (bpBir.Bodypart == opramin.Bodypart || opramin.Bodypart == BodyPart.BODYPART_ALL)
                                    //{
                                        arrListBirs.Add(new BpBirApi(0,
                                                                    bpBir.Authenticationfactor,
                                                                    bpBir.Minutiaetype,
                                                                    bpBir.Bodypart,
                                                                    null,
                                                                    bpBir.Additionaldata,
                                                                    DateTime.Now,
                                                                    bpBir.Creation,
                                                                    bpBir.Companyidenroll));
                                    //}
                                }
                            }
                        }
                    }

                    //Serializo
                    sRet = Bio.Core.Utils.SerializeHelper.SerializeToXml(arrListBirs);
                } else
                {
                    msg = "DatabaseHelper.GetBirsEnrolled [Error GetBirs = " + ret + "]";
                }

            }
            catch (Exception ex)
            {
                sRet = null;
                LOG.Error("DatabaseHelper.GetBirsEnrolled", ex);
                msg = "DatabaseHelper.GetBirsEnrolled [" + ex.Message + "]";
            }
            return sRet;
        }

        /// <summary>
        /// Obtiene las compañias con sus status, para control de accesos
        /// </summary>
        /// <param name="arrcompanias"></param>
        /// <returns></returns>
        public static int GetCompanys(out Companys arrcompanias)
        {
            string msgErr = "";
            arrcompanias = null;
            int res = Errors.IERR_OK;
            //xmlparamout = "";

            try
            {

                IList<BpCompany> companias = AdminBpCompany.GetCompanys(0, out msgErr);
                arrcompanias = new Companys();
                foreach (BpCompany company in companias)
                {
                    if (company.Laststatusdate != null)
                        arrcompanias.AddItem(new Company(company.Id, company.Rut, company.Status, (DateTime)company.Laststatusdate));
                    else
                        arrcompanias.AddItem(new Company(company.Id, company.Rut, company.Status, DateTime.MinValue));
                }

                //Serializamos el arreglo de compañías
                //xmlparamout = Bio.Core.Utils.SerializeHelper.SerializeToXml(arrcompanias);
                //res = 1;
            }
            catch (Exception ex)
            {
                LOG.Error("DatabaseHelper.GetCompanys", ex);
                res = Errors.IERR_UNKNOWN;
            }
            return res;
        }

        public static int GetCompanyIdByLegalId(string companyLegalId, out int outCompanyId, out string msgError)
        {
            int ret = Errors.IERR_OK;
            outCompanyId = -1;
            msgError = null;

            try
            {
                outCompanyId = AdminBpCompany.GetCompanyIdByIdLegal(companyLegalId, out msgError);
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msgError = ex.Message;
                LOG.Error("DatabaseHelper.GetCompanyIdByLegalId", ex);
            }
            return ret;
        }


        public static int GetBirsFroSync(int af, int mt, int bp, BpIdentity identity,
                                         out string xmlIdentityOut, out string msg)
        {
            int ret = Errors.IERR_OK;
            xmlIdentityOut = null;
            msg = null;
            IList list;
            BioPortal.Server.Sync.Api.BpIdentity identityout;
            try
            {
                if (identity == null) return Errors.IERR_IDENTITY_NOT_FOUND;

                if (identity.BpBir == null) return Errors.IERR_BIR_NOT_FOUND;

                //Copy data base
                identityout = new BioPortal.Server.Sync.Api.BpIdentity();
                identityout.Nick = identity.Nick;
                identityout.Typeid = identity.Typeid;
                identityout.Valueid = identity.Valueid;
                identityout.Name = identity.Name;
                identityout.Patherlastname = identity.Patherlastname;
                identityout.Motherlastname = identity.Motherlastname;
                identityout.Photography = identity.Photography;
                identityout.Signatureimage = identity.Signatureimage;
                identityout.DocImageFront = identity.DocImageFront;
                identityout.DocImageBack = identity.DocImageBack;
                identityout.Selfie = identity.Selfie;
                identityout.Sex = identity.Sex;
                identityout.Companyidenroll = identity.Companyidenroll.Id;
                identityout.Verificationsource = identity.Verificationsource;

                //Copio Birs filtrando por param de entrada
                if (identity.BpBir != null && identity.BpBir.Count > 0)
                {
                    identityout.BpBir = new ArrayList(); //List<BioPortal.Server.Sync.Api.BpBir>();
                    foreach (BpBir bpBir in identity.BpBir)
                    {
                        if (bpBir.Authenticationfactor == af || af == AuthenticationFactor.AUTHENTICATIONFACTOR_All)
                        {
                            if (bpBir.Minutiaetype == mt || mt == MinutiaeType.MINUTIAETYPE_ALL)
                            {
                                if (bpBir.Bodypart == bp || bp == BodyPart.BODYPART_ALL)
                                {
                                    identityout.BpBir.Add(new BioPortal.Server.Sync.Api.BpBir(0,
                                                                                              bpBir.Authenticationfactor,
                                                                                              bpBir.Minutiaetype,
                                                                                              bpBir.Bodypart,
                                                                                              bpBir.Data,
                                                                                              bpBir.Additionaldata,
                                                                                              DateTime.Now,
                                                                                              bpBir.Creation,
                                                                                              bpBir.Companyidenroll));
                                }
                            }
                        }
                    }
                }

                //Serializo
                xmlIdentityOut = Bio.Core.Utils.SerializeHelper.SerializeToXml(identityout);
                ret = Errors.IERR_OK;
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                xmlIdentityOut = null;
                LOG.Error("DatabaseHelper.GetBirsFroSync", ex);
            }
            return ret;
        }

        internal static int QClientCurrent()
        {
            int ret = 0;

            try
            {
                ret = AdminBpTx.GetQCurrentClient();
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.QClientCurrent", ex);
            }
            return ret;
        }

        private static int GetImageFromWSQorTemplate(List<Sample> list, out string strsample, out int strsampletype)
        {
            int ret = 0;
            strsample = "";
            strsampletype = 0;
            try
            {
                if (list == null || list.Count == 0)
                {
                    ret = Errors.IERR_BAD_PARAMETER;
                    LOG.Error("DatabaseHelper.GetImageFromWSQorTemplate Intentando tomar sample para BioSignature pero lista de sample = NULL");
                }
                else
                {
                    strsample = list[0].Data;
                    strsampletype = list[0].Minutiaetype;

                    //Si hay WSQ o RAW lo tomo en lugar de la minucia
                    //Si la primera tomada no es RAW o WSQ, busco en el resto de la lsita si hay mas samples, y tomo la imagen de la huella.
                    if (strsampletype != MinutiaeType.MINUTIAETYPE_WSQ && strsampletype != MinutiaeType.MINUTIAETYPE_RAW &&
                        list.Count > 1)
                    {
                        for (int i = 1; i < list.Count; i++)
                        {
                            if (list[i].Minutiaetype == MinutiaeType.MINUTIAETYPE_WSQ || list[i].Minutiaetype == MinutiaeType.MINUTIAETYPE_RAW)
                            {
                                strsample = list[i].Data;
                                strsampletype = list[i].Minutiaetype;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.GetImageFromWSQorTemplate", ex);
            }
            return ret;
        }

        internal static int GetBioSignature(string idtrack, out string xmlbiosignature)
        {
            int ret = Errors.IERR_OK;
            xmlbiosignature = null;
            string msgError;

            try
            {
                BpTx bptx = AdminBpTx.Retrieve(idtrack, out msgError);
                if (bptx == null) ret = Errors.IERR_NO_LOCAL_DATA;
                else xmlbiosignature = bptx.Abs;
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msgError = ex.Message;
                LOG.Error("DatabaseHelper.GetCompanyIdByLegalId", ex);
            }
            return ret;
        }

        internal static int SaveTx(int coderr, int operationsource, XmlParamIn oparamin,
                                    bool isBioSignatureOK, DateTime start, DateTime end, out BpTx txcreated)
        {
            int res = Errors.IERR_OK;
            int result = 0;
            double score = 0;
            double threshold = 0;
            int bodypart = oparamin.Bodypart;
            txcreated = null;
            int id = 0;
            string msg, msgaux;
            string typeid;
            string valueid;
            bool isResultOK = false;

            try
            {

                LOG.Error("DatabaseHelper.SaveTx in - isBioSignatureOK = " + isBioSignatureOK);
                //Si es verificacion de BioSignature 
                result = isBioSignatureOK ? 1 : 2;
                isResultOK = isBioSignatureOK;
                score = 0;
                threshold = 0;
                bodypart = 0;
                id = 0;
                if (oparamin != null && oparamin.PersonalData != null &&
                    !String.IsNullOrEmpty(oparamin.PersonalData.Typeid) &&
                    !String.IsNullOrEmpty(oparamin.PersonalData.Valueid))
                {
                    typeid = oparamin.PersonalData.Typeid;
                    valueid = oparamin.PersonalData.Valueid;
                }
                else
                {
                    typeid = "NA";
                    valueid = "NA";
                }

                string trackid = Guid.NewGuid().ToString("N");

                //Grabo primero la Tx sola
                BpOrigin origin = AdminBpOrigin.Retrieve(oparamin.Origin, out msgaux);
                txcreated = AdminBpTx.Create(trackid, coderr, typeid, valueid,
                                result, score, threshold, start, end,
                                oparamin.Authenticationfactor, oparamin.Minutiaetype, bodypart,
                                oparamin.Actionid, origin,
                                DateTime.Now, oparamin.Clientid, GetIP(oparamin.Ipenduser), oparamin.Enduser, null,
                                oparamin.Companyid, oparamin.Userid, operationsource, null, null, out msg);
                LOG.Debug("DatabaseHelper.SaveTx >>   AdminBpTx.Create Out : txcreated != null : " + (txcreated != null).ToString());

                if (txcreated == null)
                {
                    LOG.Error("DatabaseHelper.SaveTx - AdminBpTx.Create [" + msg + "]");
                    res = Errors.IERR_SAVING_TX;
                }
                LOG.Debug("DatabaseHelper.SaveTx >>   AdminBpTx.Create Out : txcreated != null : " + (txcreated != null).ToString());

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.SaveTx", ex);
            }
            return res;
        }


        //Added 29-04-2016
        internal static bool ClientAuthorized(int companyid, string clientid, out string msg)
        {
            bool bRet = false;
            BpClient client, clientSaved;
            msg = "";                           ///=> CARGAR MENSAJE PARA RETORNO Si no esta o esta deshabilitado!!
            int status = 0;
            try
            {
                client = AdminBpClient.Retrieve(companyid, clientid, out msg);

                //Si es 0 (encontro en el retrieve) y si es Enroll => Error
                if (client == null)
                {
                    //Si no encontro =>
                    //      1.- Informo
                    //      2.- Si flag es Autoregister => Save con Status = Disable [0]
                    ///         AutoRegisterClientConnected puede ser
                    ///         0 - No se autoregitra
                    ///         1 - Si se Con Serial Id Sensor
                    ///         2 - Si se Con IP
                    if (Global.CONFIG.AutoRegisterClientConnected > 0)
                    {   //Si es modificacion => Hago merge entre lo enviado y lo existente
                        client = AdminBpClient.Create(companyid, clientid, status, out msg);
                        if (client == null)
                        {
                            msg = "Intento de crear clientId = " + client.Clientid + " con Error [" + msg + "]";
                            LOG.Warn(msg);
                        }
                        else
                        {
                            bool bSaved = AdminBpClient.Write(client, out clientSaved, out msg);
                            if (!bSaved)
                            {
                                msg = "Intento de grabar en BD clientId = " + client.Clientid
                                         + " con Error [" + msg + "]";
                                LOG.Warn(msg);
                                bRet = false; //Se devuelve false, porque como recen lo agrego está deshabilitado
                            }
                        }
                    }
                }
                else
                {
                    bRet = (client.Status == 1); //=>Enabled = 1 | Disabled = 0
                    if (!bRet)
                    {
                        msg = "Intento de acceso de clientId = " + client.Clientid
                                  + " - Deshabilitado desde " + client.Lastmodify.Value.ToString("dd/MM/yyyy HH:mm:ss");
                        LOG.Warn(msg);
                    }
                }
            }
            catch (Exception ex)
            {
                bRet = false;
                msg = "Error desconocido [" + ex.Message + "]";
                LOG.Error("DatabaseHelper.ClientAuthorized", ex);
            }
            return bRet;
        }

        internal static BpTx GetTx(XmlParamIn oparamin, out string msg)
        {
            BpTx tx;
            msg = "";
            int status = 0;
            try
            {
                tx = null;
                IList listTx = AdminBpTx.Retrieve(oparamin.Companyid, oparamin.PersonalData.Typeid, oparamin.PersonalData.Valueid,
                                        Global.CONFIG.TimeProxyWindowTx, out msg);

                if (listTx == null || listTx.Count == 0) //Si es null => No hay Tx disponible => Retorna no hay
                {
                    msg = "No se pudo recuperar transacción para (Company,TypeI,valueId)=(" +
                                                                  oparamin.Companyid + "," +
                                                                  oparamin.PersonalData.Typeid + "," +
                                                                  oparamin.PersonalData.Valueid + ") [msg=" + msg + "]";
                    LOG.Debug("DatabaseHelper.GetTx " + msg);
                }
                else
                {
                    tx = (BpTx)listTx[0];
                }
            }
            catch (Exception ex)
            {
                tx = null;
                msg = "Error desconocido [" + ex.Message + "]";
                LOG.Error("DatabaseHelper.GetTx", ex);
            }
            return tx;
        }

        internal static string ArmaXMLTx(BpTx tx)
        {
            string ret = null;
            try
            {
                ret = "<BioPortalTx>" +
                            "<Trackid>" + tx.Trackid + "</Trackid>" +
                            "<Companyidtx>" + tx.Companyidtx + "</Companyidtx>" +
                            "<Typeid>" + tx.Typeid + "</Typeid>" +
                            "<Valueid>" + tx.Valueid + "</Valueid>" +
                            "<Actiontype>" + tx.Actiontype + "</Actiontype>" +
                            "<Authenticationfactor>" + tx.Authenticationfactor + "</Authenticationfactor>" +
                            "<Minutiaetype>" + tx.Minutiaetype + "</Minutiaetype>" +
                            "<Bodypart>" + tx.Bodypart + "</Bodypart>" +
                            "<Clientid>" + tx.Clientid + "</Clientid>" +
                            "<Ipenduser>" + tx.Ipenduser + "</Ipenduser>" +
                            "<Operationsource>" + tx.Operationsource + "</Operationsource>" +
                            "<Result>" + tx.Result + "</Result>" +
                            "<Score>" + tx.Score + "</Score>" +
                            "<Threshold>" + tx.Threshold + "</Threshold>" +
                            "<Timestampstart>" + tx.Timestampstart + "</Timestampstart>" +
                            "<Timestampend>" + tx.Timestampend + "</Timestampend>" +
                            "<Abs>" + (tx.Abs != null ? tx.Abs : "") + "</Abs>" +
                      "</BioPortalTx>";

            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("DatabaseHelper.ArmaXMLTx", ex);
            }
            return ret;
        }

        internal static int ConsumeTx(XmlParamIn oparamin, out string msg)
        {
            BpTx tx;
            msg = "";
            int status = 0;
            int iErr = Errors.IERR_OK;
            try
            {
                tx = null;

                string _trackid = oparamin.Additionaldata;
                tx = (BpTx)(AdminBpTx.RetrieveTxByTrackid(_trackid, out msg)[0]);

                if (tx == null)
                {
                    msg = "Error Recuperando Tx con Trackid = " + _trackid + "[Error=" + msg + "]";
                    iErr = Errors.IERR_DATABASE;
                }
                else
                {
                    //BpTx txClon = tx;
                    tx.Consumed = DateTime.Now;
                    BpTx txSaved;
                    //if (!AdminBpTx.Write(txClon, out txSaved, out msg))
                    if (!AdminBpTx.Update(tx, out msg))
                    {
                        msg = "Error Consumiendo Tx con Trackid = " + _trackid + "[Error=" + msg + "]";
                        iErr = Errors.IERR_DATABASE;
                    }
                }


            }
            catch (Exception ex)
            {
                tx = null;
                msg = "Error desconocido [" + ex.Message + "]";
                LOG.Error("DatabaseHelper.GetTx", ex);
                iErr = Errors.IERR_UNKNOWN;
            }
            return iErr;
        }


        /// <summary>
        /// Setea (add/update) verification source. 
        /// </summary>
        /// <param name="verificationsource"></param>
        /// <param name="authenticationfactor"></param>
        /// <param name="status"></param>
        /// <param name="type"></param>
        /// <param name="verificationsourcename"></param>
        /// <returns></returns>
        internal static string SetVerificationSource(string verificationsource, int authenticationfactor, int status, string type, string verificationsourcename)
        {
            string ret = null;
            int iErr = Errors.IERR_OK;
            try
            {
                VerificationSource vs;
                if (!String.IsNullOrEmpty(verificationsource))
                {
                    LOG.Debug("DatabaseHelper.SetVerificationSource In...verificationsource=" + verificationsource);
                    try
                    {
                        vs = XmlUtils.DeserializeObject<VerificationSource>(verificationsource);
                    }
                    catch (Exception ex)
                    {
                        LOG.Debug("DatabaseHelper.SetVerificationSource Error deserializando valor actual..." + ex.Message);
                        LOG.Debug("DatabaseHelper.SetVerificationSource Crea objeto VerificationSource");
                        vs = new VerificationSource();
                    }
                    if (vs == null)
                    {
                        LOG.Debug("DatabaseHelper.SetVerificationSource Sin error en deserializacion pero vs nulo. Crea VS...");
                        vs = new VerificationSource();
                    }

                    LOG.Debug("DatabaseHelper.SetVerificationSource Deserializado...");
                } else
                {
                    LOG.Debug("DatabaseHelper.SetVerificationSource Crea objeto VerificationSource xq está nulo");
                    vs = new VerificationSource();
                }
                int res = vs.SetVerificationSource(authenticationfactor, status, type, verificationsourcename);
                LOG.Debug("DatabaseHelper.SetVerificationSource vs.SetVerificationSource res=" + res);
                ret = vs.GetXML();
                LOG.Debug("DatabaseHelper.SetVerificationSource XMLRet = " + ret);
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("DatabaseHelper.SetVerificationSource", ex);
                iErr = Errors.IERR_UNKNOWN;
            }
            LOG.Debug("DatabaseHelper.SetVerificationSource Out!");
            return ret;
        }

        /// <summary>
        /// Dado parametros de busqueda, retorna el verificationsource de una identity en la BD si existe
        /// </summary>
        /// <param name="companyid"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <returns></returns>
        internal static string GetVerificationSource(int companyid, string typeid, string valueid)
        {
            int res = Errors.IERR_OK;
            string ret = null;
            string msg;
            BpIdentity oBpIdentity = null;
            try
            {
                LOG.Debug("DatabaseHelper.GetVerificationSource In...");
                res = AdminBpIdentity.Retrieve(companyid, typeid, valueid, out oBpIdentity, out msg);
                LOG.Debug("DatabaseHelper.ExistIdentity Retrieve res = " + res.ToString());
                //Si es 0 (encontro en el retrieve) y si es Enroll => Error
                if (res == Errors.IERR_OK)
                {
                    ret = oBpIdentity.Verificationsource;
                    LOG.Debug("DatabaseHelper.GetVerificationSource ret = " + (String.IsNullOrEmpty(ret)?"NULL":ret.ToString()));
                    if (String.IsNullOrEmpty(ret) || ret.Substring(0,1) != "<")
                    {
                        //<SourceVerified></SourceVerified>
                        ret = "<VerificationSource xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><VerificationSourceItems><VerificationSourceItem><AuthenticationFactor>2</AuthenticationFactor><StatusVerified>0</StatusVerified><TypeVerified>A</TypeVerified></VerificationSourceItem></VerificationSourceItems></VerificationSource>";
                        LOG.Debug("DatabaseHelper.GetVerificationSource VF Asignado => ret = " + ret);
                    }
                }
            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                ret = null;
                LOG.Error("DatabaseHelper.GetVerificationSource", ex);
            }
            LOG.Debug("DatabaseHelper.GetVerificationSource Out!");
            return ret;
        }

        internal static int SaveTxRegister(int coderr, int operationsource, XmlParamIn oparamin,
                                       DateTime start, DateTime end, out BpTx txcreated)
        {
            int res = Errors.IERR_OK;
            int result = 0;
            double score = 0;
            double threshold = 0;
            int bodypart = oparamin.Bodypart;
            txcreated = null;
            int id = 0;
            string msg, msgaux;
            string typeid;
            string valueid;
            bool isResultOK = false;

            try
            {

                LOG.Error("DatabaseHelper.SaveTxRegister in - coderr = " + coderr);

                LOG.Error("DatabaseHelper.SaveTxRegister - Parseo AdditionalData...");
                Dictionary<string, string> DictAditionalData = 
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(oparamin.Additionaldata);
                LOG.Error("DatabaseHelper.SaveTxRegister - DictAditionalData.len = " + DictAditionalData.Count);

                //Si es verificacion de BioSignature 
                result = DictAditionalData.ContainsKey("result") ? Convert.ToInt32(DictAditionalData["result"]) : 0;
                isResultOK = (result==1);
                score = DictAditionalData.ContainsKey("score") ? Convert.ToDouble(DictAditionalData["score"]) : 0;
                threshold = DictAditionalData.ContainsKey("threshold") ? Convert.ToDouble(DictAditionalData["threshold"]) : 0;                
                bodypart = DictAditionalData.ContainsKey("bodypart") ? Convert.ToInt32(DictAditionalData["bodypart"]) : 0;
                
                id = 0;
                if (oparamin != null && oparamin.PersonalData != null &&
                    !String.IsNullOrEmpty(oparamin.PersonalData.Typeid) &&
                    !String.IsNullOrEmpty(oparamin.PersonalData.Valueid))
                {
                    typeid = oparamin.PersonalData.Typeid;
                    valueid = oparamin.PersonalData.Valueid;
                }
                else
                {
                    typeid = "NA";
                    valueid = "NA";
                }

                string trackid = Guid.NewGuid().ToString("N");

                //Grabo primero la Tx sola
                BpOrigin origin = AdminBpOrigin.Retrieve(oparamin.Origin, out msgaux);
                LOG.Debug("DatabaseHelper.SaveTxRegister >> tid=" + typeid + "|valueid=" + valueid + "|reult=" +
                                    result.ToString() + "|score=" + score.ToString() + 
                                    "|TH=" + threshold.ToString() + "|bodypart=" + bodypart.ToString());
                txcreated = AdminBpTx.Create(trackid, coderr, typeid, valueid,
                                result, score, threshold, start, end,
                                oparamin.Authenticationfactor, oparamin.Minutiaetype, bodypart,
                                oparamin.Actionid, origin,
                                DateTime.Now, oparamin.Clientid, GetIP(oparamin.Ipenduser), oparamin.Enduser, null,
                                oparamin.Companyid, oparamin.Userid, operationsource, null, null, out msg);
                LOG.Debug("DatabaseHelper.SaveTxRegister >>   AdminBpTx.Create Out : txcreated != null : " + (txcreated != null).ToString());

                if (txcreated == null)
                {
                    LOG.Error("DatabaseHelper.SaveTxRegister - AdminBpTx.Create [" + msg + "]");
                    res = Errors.IERR_SAVING_TX;
                }
                LOG.Debug("DatabaseHelper.SaveTxRegister >>   AdminBpTx.Create Out : txcreated != null : " + (txcreated != null).ToString());

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                LOG.Error("DatabaseHelper.SaveTxRegister", ex);
            }
            return res;
        }
    }
}