﻿using System;
using System.Text;
using System.Web.Security;
using System.Web.Services;
using Bio.Core.Constant;
using Bio.Portal.Server.Services.Core.Services;
using BioPortal.Server.Api;
using Bio.Core.Matcher.Constant;
using log4net;
using Action = Bio.Core.Matcher.Constant.Action;

namespace Bio.Portal.Server.Services
{
    /// <summary>
    /// Summary description for BioPortal_Server_PROXY
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BioPortal_Server_PROXY : System.Web.Services.WebService
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(BioPortal_Server_PROXY));

#region Admin
        
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public int PingService(out string xmlparamout)
        {
            xmlparamout = null;
            return 0;
        }

#endregion Admin


#region GET

        /// <summary>
        /// Recibe los parámetros para hacer la operacion de Enroll/Modify/Get, y devuelve resultado
        /// de la misma, o mensaje de error si existe.
        /// Los pasos son:
        ///         0.- Chequeo licencia valida
        ///         1.- Deserializo objeto xmlparamin
        ///         2.- Chequeo que esté toda la información necesaria sino retorno con ERR
        ///         3.- Llamo a ServicesManager.Enroll con parametros deserializados.
        ///         4.- Retorno resultado recibido desde ServicesManager.Enroll.
        /// </summary>
        /// <param name="xmlparamin">XML con parametros</param>
        /// <param name="xmlparamout">XML con resultado</param>
        /// <returns>Codigo de ejecución</returns>
        [WebMethod]
        public int Get(string xmlparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;

            try
            {
                //0.- Chequeo licencia valida
                if (!Global.LicenseValid(Global.SERVICE_WS))
                {
                    oXmlOut.Message = Errors.SERR_LICENSE;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_LICENSE;
                }

                //1.- Deserializo objeto xmlparamin
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                if (!ServicesManager.IsCorrectParamInForAction(oXmlIn, oXmlIn.Actionid, out msgerr))
                {
                    oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                        msgerr + "]";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //3.- Llamo a ServicesManager.Get con parametros deserializados.
                res = ServicesManager.Get(oXmlIn, out xmlparamout);

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                oXmlOut.Message = Errors.SERR_UNKNOWN;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                LOG.Error("BioPortal_Server_PROXY.Get", ex);
            }

            return res;
        }


        /// <summary>
        /// Recibe los parámetros para hacer la operacion de Enroll/Modify/Get, y devuelve resultado
        /// de la misma, o mensaje de error si existe.
        /// Los pasos son:
        ///         0.- Chequeo licencia valida
        ///         1.- Deserializo objeto xmlparamin
        ///         2.- Chequeo que esté toda la información necesaria sino retorno con ERR
        ///         3.- Llamo a ServicesManager.Enroll con parametros deserializados.
        ///         4.- Retorno resultado recibido desde ServicesManager.Enroll.
        /// </summary>
        /// <param name="xmlparamin">XML con parametros</param>
        /// <param name="xmlparamout">XML con resultado</param>
        /// <returns>Codigo de ejecución</returns>
        [WebMethod]
        public int Consume(string xmlparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;

            try
            {
                //0.- Chequeo licencia valida
                if (!Global.LicenseValid(Global.SERVICE_WS))
                {
                    oXmlOut.Message = Errors.SERR_LICENSE;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_LICENSE;
                }

                //1.- Deserializo objeto xmlparamin
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                if (!ServicesManager.IsCorrectParamInForAction(oXmlIn, oXmlIn.Actionid, out msgerr))
                {
                    oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                        msgerr + "]";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //3.- Llamo a ServicesManager.Get con parametros deserializados.
                res = ServicesManager.Get(oXmlIn, out xmlparamout);

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                oXmlOut.Message = Errors.SERR_UNKNOWN;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                LOG.Error("BioPortal_Server_PROXY.Get", ex);
            }

            return res;
        }

#endregion GET

    }
}
