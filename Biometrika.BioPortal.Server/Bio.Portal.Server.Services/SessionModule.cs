﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using NHibernate;
using Bio.Portal.Server.Common;
using System.Configuration;

namespace Bio.Portal.Server.Services
{
    //See http://www.codeproject.com/KB/architecture/NHibernateBestPractices.aspx#WEB
    public class SessionModule : IHttpModule
    {
        private ISession _session;

        public void Init(HttpApplication context)
        {
            context.BeginRequest += new EventHandler(BeginTransaction);
            context.EndRequest += new EventHandler(CloseSession);
        }

        private void CloseSession(object sender, EventArgs e)
        {

            PersistentManager.CloseSession();
        }

        //Create our session
        private void BeginTransaction(object sender, EventArgs e)
        {

            _session=PersistentManager.session();
        }

        public void Dispose()
        {
            _session = null;
        }
    }
}