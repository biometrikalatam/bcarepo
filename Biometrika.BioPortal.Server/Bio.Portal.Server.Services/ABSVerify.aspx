﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ABSVerify.aspx.cs" Inherits="Bio.Portal.Server.Services.ABSVerify" %>

<!DOCTYPE html>
<script type="text/javascript">

    (function (a, b) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) window.location = b })(navigator.userAgent || navigator.vendor || window.opera, 'http://detectmobilebrowser.com/mobile');

</script> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .newStyle1 {
            font-family: "Arial Black";
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <TABLE width="100%" align="center">
        <TR height="100">
    	    <TD></TD>
    	    <TD align="center"><IMG SRC="images/logo_bps_gral1.gif" WIDTH="311" HEIGHT="47" BORDER="0" ALT="Default"></TD>
    	    <TD></TD>
        </TR>
         <TR height="100">
    	    <TD>&nbsp;</TD>
    	    <TD align="center">
                <h1><FONT SIZE="6" COLOR="blue"><span class="newStyle1">
                    <asp:Image ID="imgSello" runat="server" Height="70px" ImageAlign="AbsMiddle" ImageUrl="~/images/sello.png" Width="71px" />
&nbsp;&nbsp; Biometrika Signature Advanced - Verify</span></FONT</TD>&nbsp;&nbsp;&nbsp;&nbsp; <span class="newStyle1">
                    <asp:Image ID="imgOK" runat="server" Height="70px" ImageAlign="AbsMiddle" ImageUrl="~/images/Good-Shield.png" Width="71px" />
                    <asp:Image ID="imgNOOK" runat="server" Height="70px" ImageAlign="AbsMiddle" ImageUrl="~/images/Error-Shield.png" Width="71px" />
                    <asp:Image ID="imgQuestion" runat="server" Height="70px" ImageAlign="AbsMiddle" ImageUrl="~/images/Question-Shield.png" Width="71px" />
                    </span>&nbsp;
    	        </h1>
    	    <TD>&nbsp;</TD>
        </TR>
         <TR height="100">
    	    <TD>&nbsp;</TD>
    	    <TD align="center">&nbsp;<asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial Black" Font-Size="Small" Text="Track Id:"></asp:Label>
&nbsp;
                <asp:Label ID="labTrackId" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="Small" Text="4ak34kg2kg34kg2jkgk234gj2jkj4hj"></asp:Label>
                <br />
                <br />
&nbsp;&nbsp;&nbsp;
                <asp:Image ID="imgHuella" runat="server" Height="132px" ImageAlign="AbsMiddle" ImageUrl="~/images/FondoBPC_Boton_plano.jpg" Width="120px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Image ID="imgFoto" runat="server" Height="132px" ImageAlign="AbsMiddle" ImageUrl="~/images/User.png" Width="118px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Image ID="imgQR" runat="server" Height="109px" ImageAlign="AbsMiddle" ImageUrl="~/images/qr.png" Width="100px" />
                <br />
                <br />
                <br />
                <TABLE width="40%" align="center">
                    <TR>
    	                <TD align="right" width="40%"><asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Arial Black" Font-Size="Small" Text="Sample Type:"></asp:Label></TD>
    	                <TD align="left">&nbsp;<asp:Label ID="labSampleType" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="Small" Text="WSQ"></asp:Label></TD>
                    </TR>
                    <TR>
    	                <TD align="right" width="40%"><asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Names="Arial Black" Font-Size="Small" Text="Timestamp:"></asp:Label></TD>
    	                <TD align="left">&nbsp;<asp:Label ID="labTS" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="Small" Text="10/10/2015 10:20:30.567"></asp:Label></TD>
                    </TR>
                    <TR>
    	                <TD align="right" width="40%"><asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Arial Black" Font-Size="Small" Text="Source Verification:"></asp:Label></TD>
    	                <TD align="left">&nbsp;<asp:Label ID="labSource" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="Small" Text="LOCAL"></asp:Label></TD>
                    </TR>
                    <TR>
    	                <TD align="right" width="40%"><asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Names="Arial Black" Font-Size="Small" Text="Score:"></asp:Label></TD>
    	                <TD align="left">&nbsp;<asp:Label ID="labScore" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="Small" Text="13550"></asp:Label></TD>
                    </TR>
                </TABLE>
             </TD>
    	    <TD>&nbsp;</TD>
        </TR>
        <TR>
    	    <TD>&nbsp;</TD>
    	    <TD align="center">
                <br />
                <asp:Label ID="labMsg" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Medium" ForeColor="#339933" Text="Biometrika Signature Advanced - OK!"></asp:Label>
                <br />
                <asp:Image ID="imgHuella0" runat="server" Height="64px" ImageAlign="AbsMiddle" ImageUrl="~/images/biometrika.png" Width="420px" />
            </TD>
    	    <TD>&nbsp;</TD>
        </TR>
    </TABLE>
    </div>
    </form>
</body>
</html>
