﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using log4net;

namespace Bio.Portal.Server.Services
{
    public partial class ABSVerify : System.Web.UI.Page
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(ABSVerify));

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ClearPage();
                imgOK.Visible = false;
                imgNOOK.Visible = false;
                imgQuestion.Visible = true;
                string id = Request["id"];
                if (String.IsNullOrEmpty(id))
                {
                    SetMsg(System.Drawing.Color.Red, "El Id de BioSignature está vacío. Reintente...");
                    imgOK.Visible = false;
                    imgNOOK.Visible = false;
                    imgQuestion.Visible = true;
                    ClearPage();
                } else {
                    string xmlbiosignature = null;
                    int ierr = Bio.Portal.Server.Services.Core.Database.DatabaseHelper.GetBioSignature(id, out xmlbiosignature);
                    if (ierr == Bio.Core.Constant.Errors.IERR_OK  && !String.IsNullOrEmpty(xmlbiosignature))
                    {
                        ShowBioSignature(id, xmlbiosignature);
                    }
                    else
                    {
                        SetMsg(System.Drawing.Color.Red, "Error recuperando BioSignature! [" + ierr.ToString() + "]");
                    }
                }
            }

        }

        private void ShowBioSignature(string idtrack, string xmlbiosignature)
        {
            try
            {
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.PreserveWhitespace = false;
                //xmlDoc.LoadXml(xmlbiosignature);
                imgOK.Visible = true;
                imgNOOK.Visible = false;
                imgQuestion.Visible = false;
                string biosignature = xmlbiosignature;
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = false;
                xmlDoc.LoadXml(biosignature);
                XmlNode node = xmlDoc.GetElementsByTagName("ExtensionItem")[4];
                string abs = node.ChildNodes[1].InnerText;
                XmlDocument xmlDocABS = new XmlDocument();
                xmlDocABS.PreserveWhitespace = false;
                xmlDocABS.LoadXml(abs);


                // Check arguments.
                if (xmlDocABS != null)
                {

                    this.labTrackId.Text = idtrack;
                    string sample = xmlDocABS.GetElementsByTagName("Sample")[0].InnerText;
                    int sampletype = Convert.ToInt32(xmlDocABS.GetElementsByTagName("SampleType")[0].InnerText);
                    this.labSampleType.Text = Bio.Portal.Server.Api.Constant.MinutiaeType.GetDescription(sampletype);
                    //Imagen Smple si aplica
                    Bitmap bmpSample;
                    switch (sampletype)
                    {
                        case 21: //Bio.Portal.Server.Api.Constant.MinutiaeType.MINUTIAETYPE_WSQ:
                            Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                            byte[] wsq = Convert.FromBase64String(sample);
                            short w, h;
                            byte[] raw;
                            bool bOK = dec.DecodeMemory(wsq, out w, out h, out raw);
                            bmpSample = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, w, h);
                            break;
                        case 22: //Bio.Portal.Server.Api.Constant.MinutiaeType.MINUTIAETYPE_RAW:
                            bmpSample = Bio.Core.Imaging.ImageProcessor.RawToBitmap(Convert.FromBase64String(sample), 512, 512);
                            break;
                        case 41: //Bio.Portal.Server.Api.Constant.MinutiaeType.MINUTIAETYPE_JPEG:
                            byte[] byImg = Convert.FromBase64String(sample);
                            System.IO.MemoryStream m = new System.IO.MemoryStream(byImg);
                            bmpSample = new Bitmap(System.Drawing.Image.FromStream(m));
                            m.Close();
                            break;
                        default:
                            bmpSample = null;
                            break;
                    }
                    byte[] byImgFoto = null;
                    if (bmpSample != null)
                    {
                        try
                        {
                            using (System.IO.MemoryStream m = new System.IO.MemoryStream())
                            {
                                bmpSample.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg);
                                byImgFoto = m.ToArray();
                            }
                            if (byImgFoto != null && byImgFoto.Length > 0) {
                                Global.Fotos[idtrack + "_huella"] = Convert.ToBase64String(byImgFoto);
                                this.imgHuella.ImageUrl = "MostrarImageABS.ashx?id=" + idtrack + "&type=huella";
                            }
                        }
                        catch (Exception ex)
                        {
                            
                        } 
                    }
                    

                    ////Barcode
                    Bitmap barcodeBMP = null;
                    string barcode = xmlDocABS.GetElementsByTagName("Barcode")[0].InnerText;
                    if (!String.IsNullOrEmpty(barcode))
                    {
                        Global.Fotos[idtrack + "_barcode"] = barcode;
                        this.imgQR.ImageUrl = "MostrarImageABS.ashx?id=" + idtrack + "&type=barcode";
                    }                  

                    ////Foto
                    Bitmap fotoBMP = null;
                    string foto = xmlDocABS.GetElementsByTagName("Photografy")[0].InnerText;
                    if (!String.IsNullOrEmpty(foto))
                    {
                        Global.Fotos[idtrack + "_foto"] = foto;
                        this.imgFoto.ImageUrl = "MostrarImageABS.ashx?id=" + idtrack + "&type=foto";
                    }                   

                    //ID + TS
                    this.labTS.Text = xmlDocABS.GetElementsByTagName("Timestamp")[0].InnerText;
                    this.labScore.Text = xmlDocABS.GetElementsByTagName("Score")[0].InnerText;
                    this.labSource.Text = xmlDocABS.GetElementsByTagName("Source")[0].InnerText;

                    //string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                    //                "<DynamicData xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
                    //                "<DynamicDataItems>" +
                    //                    xmlDoc.GetElementsByTagName("DynamicData")[0].InnerXml +
                    //                "</DynamicDataItems>" +
                    //                "</DynamicData>";

                    //DynamicData ddata = Bio.Core.Api.DynamicData.DeserializeFromXml(xml);

                    //if (ddata != null)
                    //{
                    //    grpBioSignature.Text = "BioSignature >> " +
                    //                            (!String.IsNullOrEmpty((string)ddata.GetValue("TypeId")) ? (string)ddata.GetValue("TypeId") : "") + " " +
                    //                            (!String.IsNullOrEmpty((string)ddata.GetValue("ValueId")) ? (string)ddata.GetValue("ValueId") : "");
                    //    labName.Text = (!String.IsNullOrEmpty((string)ddata.GetValue("Name")) ? (string)ddata.GetValue("Name") : "") + " " +
                    //                    (!String.IsNullOrEmpty((string)ddata.GetValue("Patherlastname")) ? (string)ddata.GetValue("Patherlastname") : "") + " " +
                    //                    (!String.IsNullOrEmpty((string)ddata.GetValue("Motherlastname")) ? (string)ddata.GetValue("Motherlastname") : "");
                    //}
                }
                else
                {
                    SetMsg(System.Drawing.Color.Red, "Error interpretando BioSignature! [XML Psrse]");
                    imgOK.Visible = false;
                    imgNOOK.Visible = true;
                    imgQuestion.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ABSVerify.aspx Error", ex);
            }
        }

        private void ClearPage()
        {
            try
            {
                labTrackId.Text = "??";
                labSampleType.Text = "??";
                labTS.Text = "??";
                labSource.Text = "??";
                labScore.Text = "??";
            }
            catch (Exception ex)
            {
                LOG.Error("ABSVerify.aspx Error", ex);
            }
        }

        private void SetMsg(System.Drawing.Color color, string msg)
        {
            labMsg.ForeColor = color;
            labMsg.Text = msg;
        }

        public static void ExtractDataFromBioSignature(string absin, out string sFotoInABS, out string sSampleInABS, out string sQRInABS,
                                                                     out string sTrackid, out string sTimestamp)
        {
            sFotoInABS = null;
            sSampleInABS = null;
            sQRInABS = null;
            sTimestamp = null;
            sTrackid = null;
            string abs;
            try
            {
                string biosignature = absin;
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = false;
                xmlDoc.LoadXml(biosignature);
                XmlNode node = xmlDoc.GetElementsByTagName("ExtensionItem")[4];
                abs = node.ChildNodes[1].InnerText;
                XmlDocument xmlDocABS = new XmlDocument();
                xmlDocABS.PreserveWhitespace = false;
                xmlDocABS.LoadXml(abs);

                if (xmlDoc != null)
                {
                    string sample = xmlDocABS.GetElementsByTagName("Sample")[0].InnerText;
                    int sampletype = Convert.ToInt32(xmlDocABS.GetElementsByTagName("SampleType")[0].InnerText);
                    Bitmap bmpSample;
                    byte[] byBMP = new byte[262144];
                    System.IO.MemoryStream ms;
                    switch (sampletype)
                    {
                        case 21: //Bio.Portal.Server.Api.Constant.MinutiaeType.MINUTIAETYPE_WSQ:
                            Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                            byte[] wsq = Convert.FromBase64String(sample);
                            short w, h;
                            byte[] raw;
                            bool bOK = dec.DecodeMemory(wsq, out w, out h, out raw);
                            bmpSample = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, w, h);
                            ms = new System.IO.MemoryStream(byBMP);
                            bmpSample.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            ms.Close();
                            sSampleInABS = Convert.ToBase64String(byBMP);
                            break;
                        case 22: //Bio.Portal.Server.Api.Constant.MinutiaeType.MINUTIAETYPE_RAW:
                            w = 512;
                            h = 512;
                            bmpSample = Bio.Core.Imaging.ImageProcessor.RawToBitmap(Convert.FromBase64String(sample), w, h);
                            ms = new System.IO.MemoryStream(byBMP);
                            bmpSample.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            ms.Close();
                            sSampleInABS = Convert.ToBase64String(byBMP);
                            break;
                        case 41: //Bio.Portal.Server.Api.Constant.MinutiaeType.MINUTIAETYPE_JPEG:
                            sSampleInABS = sample;
                            break;
                        default:
                            bmpSample = null;
                            break;
                    }

                    //Barcode
                    Bitmap barcodeBMP = null;
                    sQRInABS = xmlDocABS.GetElementsByTagName("Barcode")[0].InnerText;
                    
                    //Foto
                    sFotoInABS = xmlDocABS.GetElementsByTagName("Photografy")[0].InnerText;
                    
                    //ID + TS
                    sTrackid = xmlDocABS.GetElementsByTagName("TrackID")[0].InnerText;
                    sTimestamp = xmlDocABS.GetElementsByTagName("Timestamp")[0].InnerText;
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }
    }
}