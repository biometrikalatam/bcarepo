﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bio.Core.Matcher;
using Bio.Core.Matcher.Constant;
using Bio.Core.Matcher.Interface;
using Bio.Portal.Server.Common.Entities;
using Bio.Portal.Server.Common.Entities.Database;
using Bio.Portal.Server.Services.Core.Database;
using log4net;

namespace Bio.Portal.Server.Services.Tools
{
    public partial class ResultGenBirs : System.Web.UI.Page
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ResultGenBirs));

        private string msgerr;
        private int iErr = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LOG.Debug("ResultGenBirs - Entrando al proceso de generación de BIRs...");
                string toPrint = "";
                string sCod = Request.Form["txtCodigo"];
                string sValueid = Request.Form["txtRut"];
                string[] sArrValueId = sValueid.Split(new string[] { "|" }, StringSplitOptions.None);
                string sTypeid = Request.Form["txtId"];
                int iCompany = Convert.ToInt32(Request.Form["txtCompany"]);

                LOG.Debug("ResultGenBirs - >>> [" + sCod + "] Procesando...");
                LOG.Debug("ResultGenBirs - >>> Cantidad de Valeids = " + sArrValueId.Length.ToString());
                if (string.IsNullOrEmpty(sCod) ||
                    (!sCod.Equals("saceo") && !sCod.Equals("saoperador") && !sCod.Equals("samp")))
                {
                    Label1.Text = "Codigo Incorrecto. No puede seguir...";
                }
                else
                {
                    //Se fija si existe la identidad, sino dice que sale...
                    Bio.Portal.Server.Common.Entities.Database.BpIdentity ident = null;
                    for (int i = 0; i < sArrValueId.Length; i++)
                    {
                        ident = null;
                        LOG.Debug("ResultGenBirs - >>> Company/TypeId/ValueId = " + iCompany.ToString() + "/"
                                 + sTypeid + "/" + sArrValueId[i] + "...");

                        iErr = AdminBpIdentity.Retrieve(iCompany, sTypeid, sArrValueId[i], out ident, out msgerr);
                        if (iErr != 0)
                        {
                            Label1.Text = iErr.ToString() + "[" + msgerr + "]";
                        }
                        else
                        {

                            LOG.Debug("ResultGenBirs - >>> Armando tabla ANTES...");
                            toPrint = "<table border=1>";
                            toPrint = toPrint + "<tr><td colspan=3><b>"
                                      + ident.Typeid + " - " + ident.Valueid + " - " + ident.Name + " " +
                                      ident.Patherlastname + " " +
                                      ident.Motherlastname
                                      + "</b></td></tr>";
                            toPrint = toPrint + "<tr><td colspan=3>----------------------------------</td></tr>";
                            toPrint = toPrint + "<tr><td colspan=3>ANTES</td></tr>";
                            toPrint = toPrint + "<tr><td colspan=3>----------------------------------</td></tr>";
                            foreach (BpBir bir in ident.BpBir)
                            {
                                toPrint = toPrint + "<tr><td>" + bir.Id + "</td>"
                                          + "<td>af=" + bir.Authenticationfactor.ToString()
                                          + "-mt=" + bir.Minutiaetype.ToString()
                                          + "-bp=" + bir.Bodypart.ToString()
                                          + "<td>" + bir.Creation.ToString("dd/MM/yyyy HH:mm:ss")
                                          + "</td></tr>";
                            }
                            toPrint = toPrint + "<tr><td colspan=3>----------------------------------</td></tr>";
                            toPrint = toPrint + "<tr><td colspan=3>DESPUES</td></tr>";
                            toPrint = toPrint + "<tr><td colspan=3>----------------------------------</td></tr>";

                            //Genero Matcher, Extractor y Template correspondioente de acuerdo a AF y MT. 
                            //Esto lo hago clonando desde el MatcherManager global. Esto lo hago si hay 
                            //samples para agregar
                            Hashtable htMa = Global.MATCHER_MANAGER.GetMatchersAvailables();

                            if (htMa == null || htMa.Count == 0)
                            {
                                Label1.Text = "No existen Matchers disponibles para enrolamiento]";
                            }
                            else
                            {
                                //2.1.1.- Genero BIR si es factible
                                MatcherInstance mi;
                                IExtractor extractor;
                                Bio.Core.Matcher.Interface.ITemplate templateCurrent;
                                Bio.Core.Matcher.Interface.ITemplate templateCurrentExtracted;

                                mi = (MatcherInstance) htMa[7];
                                extractor = mi.Extractor;
                                IList<BpBir> birsgen = new List<BpBir>();

                                LOG.Debug("ResultGenBirs - >>> Recorriendo BIRs para generar Minucias...");

                                foreach (BpBir bir in ident.BpBir)
                                {
                                    if (bir.Minutiaetype == 21)
                                    {
                                        templateCurrent = mi.Template;
                                        templateCurrent.AuthenticationFactor = 2;
                                        templateCurrent.MinutiaeType = 21;
                                        templateCurrent.SetData = bir.Data;
                                        templateCurrent.AdditionalData = null;
                                        templateCurrent.BodyPart = bir.Bodypart;
                                        templateCurrentExtracted = null;
                                        LOG.Debug("ResultGenBirs - >>>>>> Procesando BIRr Id = " + bir.Id +
                                                 " - bp = " + bir.Bodypart.ToString() + "...");
                                        iErr = extractor.Extract(templateCurrent,
                                                                 TemplateDestination.TEMPLATEDESTINATION_TOENROLL,
                                                                 out templateCurrentExtracted);
                                        if (iErr == 0)
                                        {
                                            //iErr = DatabaseHelper.AddBirToIdentity(ident, templateCurrentExtracted,
                                            //                                   7, 0);
                                            LOG.Debug("ResultGenBirs - >>>>>> Agregada BIRr Id = " + bir.Id +
                                                     " - bp = " + bir.Bodypart.ToString() + "!");
                                            BpBir birgenerated;
                                            iErr = AdminBpBir.ConvertTemplateToBir(null, templateCurrentExtracted,
                                                                                   iCompany, 0, out birgenerated);
                                            birsgen.Add(birgenerated);
                                        }
                                        else
                                        {
                                            LOG.Debug("ResultGenBirs - >>>>>> Descartada BIRr Id = " + bir.Id +
                                                     " - bp = " + bir.Bodypart.ToString() + "[iErr=" + iErr.ToString() +
                                                     "]");
                                        }
                                    }
                                }

                                //Agrego los birs generados en la coleccion de Birs
                                LOG.Debug("ResultGenBirs - >>> Actualizando lista de BIRs con generadas. Agregando " +
                                         birsgen.Count.ToString() + " birs...");
                                foreach (BpBir bpBir in birsgen)
                                {
                                    bpBir.BpIdentity = ident;
                                    ident.BpBir.Add(bpBir);
                                }

                                LOG.Debug("ResultGenBirs - >>> Salvando Identity...");
                                //Grabo Identity
                                iErr = DatabaseHelper.SaveIdentity(ident);

                                if (iErr != 0)
                                {
                                    Label1.Text = "Error grabando identity [" + iErr.ToString() + "]";
                                }
                                else
                                {

                                    LOG.Debug("ResultGenBirs - >>> Obteniendo Identity de nuevo y armando DESPUES...");
                                    //Leo de nuevo desde la BD para asegurarme que grabo bien
                                    ident = null;
                                    iErr = AdminBpIdentity.Retrieve(iCompany, sTypeid, sArrValueId[i], out ident, out msgerr);
                                    toPrint = toPrint + "<tr><td colspan=3><b>"
                                              + ident.Typeid + " - " + ident.Valueid + " - " + ident.Name + " " +
                                              ident.Patherlastname + " " +
                                              ident.Motherlastname
                                              + "</b></td></tr>";
                                    foreach (BpBir bir in ident.BpBir)
                                    {
                                        toPrint = toPrint + "<tr><td>" + bir.Id + "</td>"
                                                  + "<td>af=" + bir.Authenticationfactor.ToString()
                                                  + "-mt=" + bir.Minutiaetype.ToString()
                                                  + "-bp=" + bir.Bodypart.ToString()
                                                  + "<td>" + bir.Creation.ToString("dd/MM/yyyy HH:mm:ss")
                                                  + "</td></tr>";
                                    }
                                    toPrint = toPrint + "</table>";
                                    Label1.Text = Label1.Text + "<hr><br />" + toPrint;
                                    LOG.Debug("ResultGenBirs - >>> Imprimiendo Resultado...");
                                    LOG.Debug("ResultGenBirs - >>> " + toPrint);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Label1.Text = "Error generando birs... <br>" + ex.Message + "<br>" + ex.StackTrace;
                LOG.Error("ResultGenBirs - Error", ex);
            }
            LOG.Debug("ResultGenBirs - Fin del proceso de generación de BIRs!");
        } 
    }
}