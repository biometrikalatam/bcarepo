﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenBirs.aspx.cs" Inherits="Bio.Portal.Server.Services.Tools.GenBirs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            font-family: "Arial Black";
            color: #003300;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <span class="style1"><strong>Tool GenBirs...<br />
        </strong></span><br />
        Codigo Empresa 
        <asp:TextBox ID="txtCompany" runat="server" Width="200px"></asp:TextBox>
        <br />
        <br />
        Tipo Id&nbsp;
        <asp:TextBox ID="txtId" runat="server" Width="200px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label1" runat="server" Text="ValueId"></asp:Label>
&nbsp;
        <asp:TextBox ID="txtRut" runat="server" Width="200px"></asp:TextBox>
&nbsp;(Separados con pipe | )<br />
        <br />
        Codigo Seguridad:
        <asp:TextBox ID="txtCodigo" runat="server" TextMode="Password" Width="143px"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="btnGenerar" runat="server" onclick="btnGenerar_Click" 
            Text="Generar" Width="91px" EnableViewState="False" 
            PostBackUrl="~/Tools/ResultGenBirs.aspx" />
&nbsp;
        <asp:Label ID="labMsg" runat="server" Text="Msg"></asp:Label>
    
    </div>
    </form>
</body>
</html>
