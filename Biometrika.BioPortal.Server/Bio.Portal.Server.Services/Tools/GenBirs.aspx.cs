﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bio.Portal.Server.Services.Tools
{
    public partial class GenBirs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGenerar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCodigo.Text))
            {
                labMsg.Text = "Debe ingresar codigo de seguridad! Operación no permitida...";
                return;
            } else
            {
                Response.Redirect("ResultGenBirs.aspx?info=" + HttpUtility.UrlEncode(this.txtCodigo.Text, System.Text.Encoding.Unicode), true);
            }

        }
    }
}