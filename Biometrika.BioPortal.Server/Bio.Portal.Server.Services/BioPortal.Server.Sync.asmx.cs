﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;
using Bio.Core.Constant;
using Bio.Core.Matcher.Constant;
using Bio.Portal.Server.Services.Core.Services;
using BioPortal.Server.Api;
using log4net;
using Action = Bio.Core.Matcher.Constant.Action;

namespace Bio.Portal.Server.Services
{
    /// <summary>
    /// Summary description for BioPortal.Server.Sync
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class BioPortalServerSync : System.Web.Services.WebService
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BioPortalServerSync));

        /// <summary>
        /// Obtener los valores data bioemtrica para procesos de sincronizacion con productos
        /// Biometrika de WA y demás.
        /// </summary>
        /// <param name="accessKey">Clave que se debe indicar para poder usar esta acción</param>
        /// <param name="xmlparamin">Parámetros de entrada</param>
        /// <param name="xmlparamout">Objeto conteniendo codigo de retorno, msg erro si existe y respuesta
        /// de los BIRs en formado DynamicData
        /// </param>
        /// <returns></returns>
        [WebMethod]
        [XmlInclude(typeof(BioPortal.Server.Sync.Api.BpBir)), XmlInclude(typeof(BioPortal.Server.Sync.Api.BpIdentity))]
        public int Sync(string accessKey, string xmlparamin, out string xmlparamout)
        {

            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;

            try
            {
                //0.- Chequeo licencia valida
                if (!Global.LicenseValid(Global.SERVICE_WS))
                {
                    oXmlOut.Message = Errors.SERR_LICENSE;
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_LICENSE;
                }

                //1.- Deserializo objeto xmlparamin
                XmlParamIn oXmlIn = XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                if (!ServicesManager.IsCorrectParamInForAction(oXmlIn, Action.ACTION_SYNC_GETBIRS, out msgerr))
                {
                    oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                        msgerr + "]";
                    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IERR_BAD_PARAMETER;
                }

                //3.- Llamo a ServicesManager.Verify con parametros deserializados.
                res = ServicesManager.Sync(oXmlIn, out xmlparamout);

            }
            catch (Exception ex)
            {
                res = Errors.IERR_UNKNOWN;
                oXmlOut.Message = Errors.SERR_UNKNOWN;
                xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                LOG.Error("BioPortalServerSync.Sync", ex);
            }

            return res;

        }

        
    }
}
