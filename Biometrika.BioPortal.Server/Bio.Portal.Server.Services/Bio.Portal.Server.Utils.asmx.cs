﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Bio.Portal.Server.Services.Core.Services;
using log4net;
using Bio.Core.Constant;
using Innovatrics.AnsiIso;
using Innovatrics.AnsiIso.Enums;
using Innovatrics.Sdk.Commons;
using Innovatrics.Sdk.Commons.Enums;

namespace Bio.Portal.Server.Services
{
    /// <summary>
    /// WS para agregar aqui cualquier función que permita ayudar en procesos internos
    /// de productos Biometrika. 
    /// El primero es apra la conversión de minucias ISO para MoC
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Bio_Portal_Server_Utils : System.Web.Services.WebService
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Bio_Portal_Server_Utils));

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public int ISOToISOC(string _iso, out string _isoConverted) //Recibe una huella iso en base64
        {
            int iret = 0;
            _isoConverted = null;
            try
            {
                LOG.Info("Bio_Portal_Server_Utils.ISOToISOC IN...");
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Param ISO IN = " + _iso);
                Byte[] _isoTemplate = Convert.FromBase64String(_iso);

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializing...");
                //IEngine.Init(); //Inicialización Innovatrics
                IEngine iEngine = IEngine.Instance;
                Iso iso = Iso.Instance;
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializated!");
                
                Byte[] _temp = new Byte[4000];
                Int32 _isoLength = 0, _isocLength = 0;

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN...");
                // iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

                //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2...");
                byte[] _isocTemplateWrapped = iso.ConvertToISOCardCC(_isoTemplate, 63, IEngineSortOrder.SortNone, IEngineSortOrder.SortXAsc);
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2 OUT!");
 
                iEngine.Terminate(); //Finalización Innovatrics
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine Terminated!");

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Starting Reorder Minutiae...");
                //Re-ordenamiento de minucias
                for (Int32 n = 0; n < _isocTemplateWrapped.Length; n++)
                {
                    _temp[n] = _isocTemplateWrapped[n + 28];

                    if (n > 0 && _temp[n - 1] == 0x00)
                    {
                        _isocLength = n;

                        break;
                    }
                }

                _isocLength = _isocLength - 1;

                Byte[] _isocTemplate = new Byte[_isocLength];

                for (Int32 n = 0; n < _isocLength; n++)
                {
                    _isocTemplate[n] = _temp[n];
                }
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Reorder Minutiae OUT!");

                _isoConverted = BitConverter.ToString(_isocTemplate);
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC _isoConverted = " + _isoConverted);

                //String[] _dataArray = _data.Split('-');

                //return _dataArray;
            }
            catch (Exception ex)
            {
                iret = -1;
                LOG.Error("Bio_Portal_Server_Utils.ISOToISOC Error [" + ex.Message + "]");

            }
            return iret;
        }

        [WebMethod]
        public int BVIISOToISOC(int companyid, string serial, string _iso, out string _isoConverted) //Recibe una huella iso en base64
        {
            int iret = 0;
            _isoConverted = null;
            string msg;
            try
            {
                LOG.Info("Bio_Portal_Server_Utils.ISOToISOC IN...");

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC - checking ClientId Autorized [" + serial + "]");
                //Added 29-04-2016
                //Si debe chequear serial de device, se fija en tabla bp_devices para esa compañia si esá y está habilitado.
                if (Global.CONFIG.CheckClientConnected == 1)
                {
                    //Added 29-04-2016
                    
                    if (!ServicesManager.ClientAuthorized(companyid, serial, out msg))
                    {
                        _isoConverted = null;
                        return Errors.IERR_CLIENT_NOT_AUTHORIZED;
                    }
                }

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Param ISO IN = " + _iso);
                Byte[] _isoTemplate = Convert.FromBase64String(_iso);

                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializing...");
                //IEngine.Init(); //Inicialización Innovatrics
                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializated!");

                //Byte[] _temp = new Byte[4000];
                //Int32 _isoLength = 0, _isocLength = 0;

                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN...");
                //Iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

                //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2...");
                //Iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, _isocTemplateWrapped);
                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2 OUT!");

                //IEngine.Terminate(); //Finalización Innovatrics
                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine Terminated!");
                //IEngine.Init(); //Inicialización Innovatrics

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializing...");
                IEngine iEngine = IEngine.Instance;
                Iso iso = Iso.Instance;
                iEngine.Init();
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializated!");

                Byte[] _temp = new Byte[4000];
                Int32 _isoLength = 0, _isocLength = 0;

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN...");
                // iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

                //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2...");
                byte[] _isocTemplateWrapped = iso.ConvertToISOCardCC(_isoTemplate, 63, IEngineSortOrder.SortNone, IEngineSortOrder.SortXAsc);
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2 OUT!");

                iEngine.Terminate(); //Finalización Innovatrics
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine Terminated!");

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Starting Reorder Minutiae...");
                //Re-ordenamiento de minucias
                for (Int32 n = 0; n < _isocTemplateWrapped.Length; n++)
                {
                    _temp[n] = _isocTemplateWrapped[n + 28];

                    if (n > 0 && _temp[n - 1] == 0x00)
                    {
                        _isocLength = n;

                        break;
                    }
                }

                _isocLength = _isocLength - 1;

                Byte[] _isocTemplate = new Byte[_isocLength];

                for (Int32 n = 0; n < _isocLength; n++)
                {
                    _isocTemplate[n] = _temp[n];
                }
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Reorder Minutiae OUT!");

                _isoConverted = BitConverter.ToString(_isocTemplate);
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC _isoConverted = " + _isoConverted);

                //String[] _dataArray = _data.Split('-');

                //return _dataArray;
            }
            catch (Exception ex)
            {
                iret = -1;
                LOG.Error("Bio_Portal_Server_Utils.ISOToISOC Error [" + ex.Message + "]");

            }
            return iret;
        }

        [WebMethod]
        public int BVIISOToISOCTx(int companyid, string typeid, string valueid, string serial, string _iso, out string _isoConverted) //Recibe una huella iso en base64
        {
            int iret = 0;
            _isoConverted = null;
            string msg;
            try
            {
                LOG.Info("Bio_Portal_Server_Utils.ISOToISOC IN...");

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC - checking ClientId Autorized [" + serial + "]");
                //Added 29-04-2016
                //Si debe chequear serial de device, se fija en tabla bp_devices para esa compañia si esá y está habilitado.
                if (Global.CONFIG.CheckClientConnected == 1)
                {
                    //Added 29-04-2016

                    if (!ServicesManager.ClientAuthorized(companyid, serial, out msg))
                    {
                        _isoConverted = null;
                        return Errors.IERR_CLIENT_NOT_AUTHORIZED;
                    }
                }

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Param ISO IN = " + _iso);
                Byte[] _isoTemplate = Convert.FromBase64String(_iso);

                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializing...");
                //IEngine.Init(); //Inicialización Innovatrics
                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializated!");

                //Byte[] _temp = new Byte[4000];
                //Int32 _isoLength = 0, _isocLength = 0;

                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN...");
                //Iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

                //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2...");
                //Iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, _isocTemplateWrapped);
                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2 OUT!");

                //IEngine.Terminate(); //Finalización Innovatrics
                //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine Terminated!");
                //IEngine.Init(); //Inicialización Innovatrics

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializing...");
                IEngine iEngine = IEngine.Instance;
                Iso iso = Iso.Instance;
                iEngine.Init();
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializated!");

                Byte[] _temp = new Byte[4000];
                Int32 _isoLength = 0, _isocLength = 0;

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN...");
                // iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

                //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2...");
                byte[] _isocTemplateWrapped = iso.ConvertToISOCardCC(_isoTemplate, 63, IEngineSortOrder.SortNone, IEngineSortOrder.SortXAsc);
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2 OUT!");

                iEngine.Terminate(); //Finalización Innovatrics
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine Terminated!");

                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Starting Reorder Minutiae...");
                //Re-ordenamiento de minucias
                for (Int32 n = 0; n < _isocTemplateWrapped.Length; n++)
                {
                    _temp[n] = _isocTemplateWrapped[n + 28];

                    if (n > 0 && _temp[n - 1] == 0x00)
                    {
                        _isocLength = n;

                        break;
                    }
                }

                _isocLength = _isocLength - 1;

                Byte[] _isocTemplate = new Byte[_isocLength];

                for (Int32 n = 0; n < _isocLength; n++)
                {
                    _isocTemplate[n] = _temp[n];
                }
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Reorder Minutiae OUT!");

                _isoConverted = BitConverter.ToString(_isocTemplate);
                LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC _isoConverted = " + _isoConverted);

                //String[] _dataArray = _data.Split('-');

                //return _dataArray;
            }
            catch (Exception ex)
            {
                iret = -1;
                LOG.Error("Bio_Portal_Server_Utils.ISOToISOC Error [" + ex.Message + "]");

            }

            try
            {
                /*
                public static BpTx Create(string trackid, int operationcode, string typeid, string valueid, int result, double score,
                            double threshold, DateTime timestampstart, DateTime timestampend, int authenticationfactor,
                            int minutiaetype, int bodypart, int actiontype, BpOrigin bporigin, DateTime? timestampclient,
                            string clientid, string ipenduser, string enduser, string dynamicdata, int companyidtx,
                            int useridtx, int operationsource, string abs, IList conxList, out string msgErr)
                */
                //Core.Database.DatabaseHelper.SaveTx()
            }
            catch (Exception ex)
            {
                LOG.Error("Bio_Portal_Server_Utils.ISOToISOC Error [" + ex.Message + "]");
            }

            LOG.Info("Bio_Portal_Server_Utils.ISOToISOC OUT!");
            return iret;
        }

        /// <summary>
        /// Graba una Tx de verificacion para la verificacion contra cédula nueva. 
        /// Contra cedula vieja ya lo hace el Matcher NEC
        /// </summary>
        /// <param name="companyid"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="serial"></param>
        /// <param name="_isoConverted"></param>
        /// <param name="msgret"></param>
        /// <returns></returns>
        [WebMethod]
        public int SaveTx(int companyid, string typeid, string valueid, string serial, string _iso, string _isoConverted, out string msgret) //Recibe una huella iso en base64
        {
            int iret = 0;
            msgret = null;
            _isoConverted = null;
            string msg;
            try
            {
                
            }
            catch (Exception ex)
            {
                iret = -1;
                LOG.Error("Bio_Portal_Server_Utils.ISOToISOC Error [" + ex.Message + "]");

            }
            return iret;
        }

        /*BACKUP Antes de v2 de SDk ANSI/ISIO Innovatrics
         *  

                       [WebMethod]
               public int ISOToISOC(string _iso, out string _isoConverted) //Recibe una huella iso en base64
               {
                   int iret = 0;
                   _isoConverted = null;
                   try
                   {
                       LOG.Info("Bio_Portal_Server_Utils.ISOToISOC IN...");
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Param ISO IN = " + _iso);
                       Byte[] _isoTemplate = Convert.FromBase64String(_iso);

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializing...");
                       //IEngine.Init(); //Inicialización Innovatrics
                       IEngine iEngine = IEngine.Instance;
                       Iso iso = Iso.Instance;
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializated!");

                       Byte[] _temp = new Byte[4000];
                       Int32 _isoLength = 0, _isocLength = 0;

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN...");
                       // iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

                       //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2...");
                       byte[] _isocTemplateWrapped = iso.ConvertToISOCardCC(_isoTemplate, 63, IEngineSortOrder.SortNone, IEngineSortOrder.SortXAsc);
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2 OUT!");

                       iEngine.Terminate(); //Finalización Innovatrics
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine Terminated!");

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Starting Reorder Minutiae...");
                       //Re-ordenamiento de minucias
                       for (Int32 n = 0; n < _isocTemplateWrapped.Length; n++)
                       {
                           _temp[n] = _isocTemplateWrapped[n + 28];

                           if (n > 0 && _temp[n - 1] == 0x00)
                           {
                               _isocLength = n;

                               break;
                           }
                       }

                       _isocLength = _isocLength - 1;

                       Byte[] _isocTemplate = new Byte[_isocLength];

                       for (Int32 n = 0; n < _isocLength; n++)
                       {
                           _isocTemplate[n] = _temp[n];
                       }
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Reorder Minutiae OUT!");

                       _isoConverted = BitConverter.ToString(_isocTemplate);
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC _isoConverted = " + _isoConverted);

                       //String[] _dataArray = _data.Split('-');

                       //return _dataArray;
                   }
                   catch (Exception ex)
                   {
                       iret = -1;
                       LOG.Error("Bio_Portal_Server_Utils.ISOToISOC Error [" + ex.Message + "]");

                   }
                   return iret;
               }

               [WebMethod]
               public int BVIISOToISOC(int companyid, string serial, string _iso, out string _isoConverted) //Recibe una huella iso en base64
               {
                   int iret = 0;
                   _isoConverted = null;
                   string msg;
                   try
                   {
                       LOG.Info("Bio_Portal_Server_Utils.ISOToISOC IN...");

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC - checking ClientId Autorized [" + serial + "]");
                       //Added 29-04-2016
                       //Si debe chequear serial de device, se fija en tabla bp_devices para esa compañia si esá y está habilitado.
                       if (Global.CONFIG.CheckClientConnected == 1)
                       {
                           //Added 29-04-2016

                           if (!ServicesManager.ClientAuthorized(companyid, serial, out msg))
                           {
                               _isoConverted = null;
                               return Errors.IERR_CLIENT_NOT_AUTHORIZED;
                           }
                       }

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Param ISO IN = " + _iso);
                       Byte[] _isoTemplate = Convert.FromBase64String(_iso);

                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializing...");
                       //IEngine.Init(); //Inicialización Innovatrics
                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializated!");

                       //Byte[] _temp = new Byte[4000];
                       //Int32 _isoLength = 0, _isocLength = 0;

                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN...");
                       //Iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

                       //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2...");
                       //Iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, _isocTemplateWrapped);
                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2 OUT!");

                       //IEngine.Terminate(); //Finalización Innovatrics
                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine Terminated!");
                       //IEngine.Init(); //Inicialización Innovatrics

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializing...");
                       IEngine iEngine = IEngine.Instance;
                       Iso iso = Iso.Instance;
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializated!");

                       Byte[] _temp = new Byte[4000];
                       Int32 _isoLength = 0, _isocLength = 0;

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN...");
                       // iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

                       //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2...");
                       byte[] _isocTemplateWrapped = iso.ConvertToISOCardCC(_isoTemplate, 63, IEngineSortOrder.SortNone, IEngineSortOrder.SortXAsc);
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2 OUT!");

                       iEngine.Terminate(); //Finalización Innovatrics
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine Terminated!");

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Starting Reorder Minutiae...");
                       //Re-ordenamiento de minucias
                       for (Int32 n = 0; n < _isocTemplateWrapped.Length; n++)
                       {
                           _temp[n] = _isocTemplateWrapped[n + 28];

                           if (n > 0 && _temp[n - 1] == 0x00)
                           {
                               _isocLength = n;

                               break;
                           }
                       }

                       _isocLength = _isocLength - 1;

                       Byte[] _isocTemplate = new Byte[_isocLength];

                       for (Int32 n = 0; n < _isocLength; n++)
                       {
                           _isocTemplate[n] = _temp[n];
                       }
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Reorder Minutiae OUT!");

                       _isoConverted = BitConverter.ToString(_isocTemplate);
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC _isoConverted = " + _isoConverted);

                       //String[] _dataArray = _data.Split('-');

                       //return _dataArray;
                   }
                   catch (Exception ex)
                   {
                       iret = -1;
                       LOG.Error("Bio_Portal_Server_Utils.ISOToISOC Error [" + ex.Message + "]");

                   }
                   return iret;
               }

               [WebMethod]
               public int BVIISOToISOCTx(int companyid, string typeid, string valueid, string serial, string _iso, out string _isoConverted) //Recibe una huella iso en base64
               {
                   int iret = 0;
                   _isoConverted = null;
                   string msg;
                   try
                   {
                       LOG.Info("Bio_Portal_Server_Utils.ISOToISOC IN...");

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC - checking ClientId Autorized [" + serial + "]");
                       //Added 29-04-2016
                       //Si debe chequear serial de device, se fija en tabla bp_devices para esa compañia si esá y está habilitado.
                       if (Global.CONFIG.CheckClientConnected == 1)
                       {
                           //Added 29-04-2016

                           if (!ServicesManager.ClientAuthorized(companyid, serial, out msg))
                           {
                               _isoConverted = null;
                               return Errors.IERR_CLIENT_NOT_AUTHORIZED;
                           }
                       }

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Param ISO IN = " + _iso);
                       Byte[] _isoTemplate = Convert.FromBase64String(_iso);

                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializing...");
                       //IEngine.Init(); //Inicialización Innovatrics
                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializated!");

                       //Byte[] _temp = new Byte[4000];
                       //Int32 _isoLength = 0, _isocLength = 0;

                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN...");
                       //Iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

                       //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2...");
                       //Iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, _isocTemplateWrapped);
                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2 OUT!");

                       //IEngine.Terminate(); //Finalización Innovatrics
                       //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine Terminated!");
                       //IEngine.Init(); //Inicialización Innovatrics

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializing...");
                       IEngine iEngine = IEngine.Instance;
                       Iso iso = Iso.Instance;
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializated!");

                       Byte[] _temp = new Byte[4000];
                       Int32 _isoLength = 0, _isocLength = 0;

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN...");
                       // iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

                       //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2...");
                       byte[] _isocTemplateWrapped = iso.ConvertToISOCardCC(_isoTemplate, 63, IEngineSortOrder.SortNone, IEngineSortOrder.SortXAsc);
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2 OUT!");

                       iEngine.Terminate(); //Finalización Innovatrics
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine Terminated!");

                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Starting Reorder Minutiae...");
                       //Re-ordenamiento de minucias
                       for (Int32 n = 0; n < _isocTemplateWrapped.Length; n++)
                       {
                           _temp[n] = _isocTemplateWrapped[n + 28];

                           if (n > 0 && _temp[n - 1] == 0x00)
                           {
                               _isocLength = n;

                               break;
                           }
                       }

                       _isocLength = _isocLength - 1;

                       Byte[] _isocTemplate = new Byte[_isocLength];

                       for (Int32 n = 0; n < _isocLength; n++)
                       {
                           _isocTemplate[n] = _temp[n];
                       }
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Reorder Minutiae OUT!");

                       _isoConverted = BitConverter.ToString(_isocTemplate);
                       LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC _isoConverted = " + _isoConverted);

                       //String[] _dataArray = _data.Split('-');

                       //return _dataArray;
                   }
                   catch (Exception ex)
                   {
                       iret = -1;
                       LOG.Error("Bio_Portal_Server_Utils.ISOToISOC Error [" + ex.Message + "]");

                   }

                   try
                   {
                       
                       //public static BpTx Create(string trackid, int operationcode, string typeid, string valueid, int result, double score,
                       //            double threshold, DateTime timestampstart, DateTime timestampend, int authenticationfactor,
                       //            int minutiaetype, int bodypart, int actiontype, BpOrigin bporigin, DateTime? timestampclient,
                       //            string clientid, string ipenduser, string enduser, string dynamicdata, int companyidtx,
                       //            int useridtx, int operationsource, string abs, IList conxList, out string msgErr)
                       
        //Core.Database.DatabaseHelper.SaveTx()
    }
            catch (Exception ex)
            {
                LOG.Error("Bio_Portal_Server_Utils.ISOToISOC Error [" + ex.Message + "]");
            }

LOG.Info("Bio_Portal_Server_Utils.ISOToISOC OUT!");
            return iret;
        }

        /// <summary>
        /// Graba una Tx de verificacion para la verificacion contra cédula nueva. 
        /// Contra cedula vieja ya lo hace el Matcher NEC
        /// </summary>
        /// <param name="companyid"></param>
        /// <param name="typeid"></param>
        /// <param name="valueid"></param>
        /// <param name="serial"></param>
        /// <param name="_isoConverted"></param>
        /// <param name="msgret"></param>
        /// <returns></returns>
        [WebMethod]
public int SaveTx(int companyid, string typeid, string valueid, string serial, string _iso, string _isoConverted, out string msgret) //Recibe una huella iso en base64
{
    int iret = 0;
    msgret = null;
    _isoConverted = null;
    string msg;
    try
    {
        //LOG.Info("Bio_Portal_Server_Utils.SaveTx IN...");

        //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Param ISO IN = " + _iso);
        //Byte[] _isoTemplate = Convert.FromBase64String(_iso);

        //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializing...");
        //IEngine.Init(); //Inicialización Innovatrics
        //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine initializated!");

        //Byte[] _temp = new Byte[4000];
        //Int32 _isoLength = 0, _isocLength = 0;

        //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN...");
        //Iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, null);
        //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC OUT - _isoLength = " + _isoLength.ToString());

        //Byte[] _isocTemplateWrapped = new Byte[_isoLength];

        //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2...");
        //Iso.ConvertToISOCardCC(_isoTemplate, 63, 0, 0, ref _isoLength, _isocTemplateWrapped);
        //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC ConvertToISOCardCC IN 2 OUT!");

        //IEngine.Terminate(); //Finalización Innovatrics
        //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC IEngine Terminated!");

        //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Starting Reorder Minutiae...");
        ////Re-ordenamiento de minucias
        //for (Int32 n = 0; n < _isocTemplateWrapped.Length; n++)
        //{
        //    _temp[n] = _isocTemplateWrapped[n + 28];

        //    if (n > 0 && _temp[n - 1] == 0x00)
        //    {
        //        _isocLength = n;

        //        break;
        //    }
        //}

        //_isocLength = _isocLength - 1;

        //Byte[] _isocTemplate = new Byte[_isocLength];

        //for (Int32 n = 0; n < _isocLength; n++)
        //{
        //    _isocTemplate[n] = _temp[n];
        //}
        //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC Reorder Minutiae OUT!");

        //_isoConverted = BitConverter.ToString(_isocTemplate);
        //LOG.Debug("Bio_Portal_Server_Utils.ISOToISOC _isoConverted = " + _isoConverted);

        //String[] _dataArray = _data.Split('-');

        //return _dataArray;
    }
    catch (Exception ex)
    {
        iret = -1;
        LOG.Error("Bio_Portal_Server_Utils.ISOToISOC Error [" + ex.Message + "]");

    }
    return iret;
}

    */
    }
}
