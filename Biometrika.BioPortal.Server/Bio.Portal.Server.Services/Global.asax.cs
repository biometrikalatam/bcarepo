﻿using Bio.Core.Constant;
using Bio.Core.Matcher;
using Bio.Core.Utils;
using Bio.Portal.Server.Common;
using Bio.Portal.Server.Common.Common;
using Bio.Portal.Server.Common.Config;
using Bio.Portal.Server.Common.Entities;
using Bio.Portal.Server.Common.Entities.Database;
using Bio.Portal.Server.Services.APIRest.Models;
using Bio.Portal.Server.Services.Core.Database;
using Bio.Portal.Server.Services.Core.Jumio;
using Bio.Portal.Server.Services.WebApi;
using Biometrika.BioSignature.Advanced;
using Biometrika.License.Check;
using Biometrika.License.Common;
using Biometrika.Regula.WebAPI.Api._2021;
//using BiometrikaLCheck;

//using BioLicenseCheck;
using log4net;
using log4net.Config;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Bio.Portal.Server.Services
{
    ///<summary>
    /// Seteos iniciales y finales del BioPortal Server Services.
    ///</summary>
    public class Global : System.Web.HttpApplication
    {
        public static Hashtable Fotos = new Hashtable();

        /// <summary>
        /// Added 04/07/2020 para manejo de Jumio/Facetec y BPWeb
        /// </summary>
        internal static JumioHelper JUMIO_HELPER;
        internal static FacetecHelper FACETEC_HELPER;
        internal static RegulaHandler REGULA_HANDLER;
        internal static string _DISCLAIMER_AUTORIZATION_BPWEB;
        /// <summary>
        /// Added Tipo d eNotarizacion - Added 04-2018
        /// Valores posibles:
        ///            0 - Online (Intenta solo en linea)  
        ///            1 - Off Line (Graba XML en NVAsyncPath para ser procesado por agente Asincronico)
        ///            2 - Primero intenta online, luego off line (Si falla OnLine => graba xml en NVAsyncPath para ser procesado por agente Asincronico)
        /// </summary>
        internal static int NV_TYPE = 0;

        /// <summary>
        /// Added Biometrika Signature Advnced - Added 01-2015
        /// </summary>
        internal static BioSignatureFactory BIOSIGNATURE_FACTORY;

        /// <summary>
        /// Manejo de Tokens - Added 10-2014
        /// </summary>
        internal static TokenManager TOKEN_MANAGER;

        /// <summary>
        /// Arreglo de compañias con sus status. Pemrite controlar accesos
        /// </summary>
        public static Companys ArrCompanys;

        //[System.Xml.Serialization.XmlArray("Templates")]
        //public List<Template> Templates;

        /// <summary>
        /// Manejo de matchers, ectraxtors y templates.
        /// </summary>
        internal static MatcherManager MATCHER_MANAGER;

        /// <summary>
        /// Manejo de conectores para consultas externas.
        /// </summary>
        internal static ConnectorManager CONNECTOR_MANAGER;

        /// <summary>
        /// Configuraciones del BioPortal Server
        /// </summary>
        internal static BpConfig CONFIG;

        //Variables globales para funcionamiento general

        /// <summary>
        /// IP del servidor donde corre el BPServer, y al que está asociado la licencia.
        /// </summary>
        internal static string IP_SERVER;

        /// <summary>
        /// Determina si está habilitado por licencia el uso del plug-in web.
        /// </summary>
        internal static bool WS_WEB_PLUGIN_ENABLED;

        /// <summary>
        /// Determina si está habilitado por licencia el uso del plug-in Process (Converts, extractor, etc).
        /// </summary>
        internal static int PLUGIN_PROCESS;
        
        /// <summary>
        /// Determina si está habilitado por licencia el uso del conector al SRCeI.
        /// </summary>
        internal static bool WS_CONNECTOR_SRCeI_ENABLED;

        /// <summary>
        /// Determina cantidad de puntos remotos de verificacion
        /// </summary>
        internal static int Q_CONCURRENT;

        /// <summary>
        /// LOG del modulo
        /// </summary>
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Global));


        

        /// <summary>
        /// Genera los siguientes pasos.
        ///     0.- Inicializo LOG
        ///     1.- Levanto configuración BioPortal
        ///     2.- Chequeo de licencia.
        ///     3.- Genero e inicializo MatcherManager
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_Start(object sender, EventArgs e)
        {
            try
            {
                //test();

                AreaRegistration.RegisterAllAreas();
                GlobalConfiguration.Configure(WebApiConfig.Register);
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);


                //test();

                string root = HttpContext.Current.Server.MapPath(".");
                //string urlbaseservice = HttpContext.Current.Request.Url.AbsoluteUri;

                ConfigurationManager.AppSettings["PathHibernate"]= root + ConfigurationManager.AppSettings.Get("PathHibernate").ToString();

                 //0.- Inicializo LOG
                XmlConfigurator.Configure(
                    new FileInfo(root + Bio.Portal.Server.Services.Properties.Settings.Default.PathConfigLog));
                LOG.Info("BioPortal Server Starting...");

                //1.- Levanto configuración BioPortal
                LOG.Info("Leyendo Config en = " + root + Bio.Portal.Server.Services.Properties.Settings.Default.PathConfigFile + "...");
                CONFIG =
                    SerializeHelper.DeserializeFromFile<BpConfig>(root +
                        Bio.Portal.Server.Services.Properties.Settings.Default.PathConfigFile);
                LOG.Info(CONFIG != null
                             ? "   BioPortal Server reading BpConfig = OK"
                             : "   BioPortal Server reading BpConfig = ERROR");
                LOG.Info(CONFIG.ToString());

                //1.- Chequeo de licencia.
                bool bResultOKCheckLic = CheckLicense(root + Portal.Server.Services.Properties.Settings.Default.PathLicense);

                //Si chequeo es ok, sigue carga sino manda msg error y corta.
                if (bResultOKCheckLic)
                {
                    //Added 01-2015 - Creo BioSignature Factory si está habilitado
                    //Si chequeo es ok, sigue carga sino manda msg error y corta.
                    if (CONFIG.BioSignatureType != 0)
                    {
                        BIOSIGNATURE_FACTORY = new BioSignatureFactory();
                        BIOSIGNATURE_FACTORY.InitFactory(Properties.Settings.Default.PathPFX,
                                                         Properties.Settings.Default.PFXPSW,
                                                           Properties.Settings.Default.PathCER,
                                                         Properties.Settings.Default.BarcodeTypeABS,
                                                         Properties.Settings.Default.URLQR);
                        LOG.Info("   CONFIG.BioSignatureType = " + Global.CONFIG.BioSignatureType);
                    }

                    //2.- Genero e inicializo MatcherManager
                    MATCHER_MANAGER = SerializeHelper.DeserializeFromFile<MatcherManager>(root +
                              Bio.Portal.Server.Services.Properties.Settings.Default.PathConfigMatchers);
                    if (MATCHER_MANAGER == null || MATCHER_MANAGER.MatchersConfigured.Count == 0)
                    {
                        LOG.Info("   BioPortal Server Reading Matchers = ERROR - No Matchers Configured!");
                    }
                    else
                    {
                        LOG.Info("   BioPortal Server Reading Matchers = OK - Matchers Configured = " +
                        MATCHER_MANAGER.MatchersConfigured.Count.ToString());

                        //Si levanto matchers, inicializo creando las instancias
                        MATCHER_MANAGER.Initialization();

                        if (MATCHER_MANAGER.QuantityMatchersAvailables() == 0)
                        {
                            LOG.Info("   BioPortal Server Initializing Matchers = WARNING - No Matchers Availables!");
                        }
                        else
                        {
                            LOG.Info("   BioPortal Server Initializing Matchers = OK  - Matchers Availables = " +
                                           MATCHER_MANAGER.QuantityMatchersAvailables().ToString());
                            LOG.Info("   Matchers Availables = " + MATCHER_MANAGER.MatchersAvailablesToString());
                        }
                    }

                    //3.- Genero e inicializo ConnectorManager
                    CONNECTOR_MANAGER = SerializeHelper.DeserializeFromFile<ConnectorManager>(root +
                            Bio.Portal.Server.Services.Properties.Settings.Default.PathConfigConnectors);
                    if (CONNECTOR_MANAGER == null || CONNECTOR_MANAGER.ConnectorsConfigured.Count == 0)
                    {
                        LOG.Info("   BioPortal Server Reading Connectors - No Connectors Configured!");
                    }
                    else
                    {
                        LOG.Info("   BioPortal Server Reading Connectors = OK - Connectors Configured = " +
                        CONNECTOR_MANAGER.ConnectorsConfigured.Count.ToString());

                        //Si levanto connectors, inicializo creando las instancias
                        CONNECTOR_MANAGER.Initialization();

                        if (CONNECTOR_MANAGER.QuantityConnectorsAvailables() == 0)
                        {
                            LOG.Info("   BioPortal Server Initializing Connectors = WARNING - No Connectors Availables!");
                        }
                        else
                        {
                            LOG.Info("   BioPortal Server Initializing Connectors = OK  - Connectors Availables = " +
                                           CONNECTOR_MANAGER.QuantityConnectorsAvailables().ToString());
                            LOG.Info("   Connectors Availables = " + CONNECTOR_MANAGER.ConnecotorsAvailablesToString());
                        }
                    }

                    //3.- Genero e inicializo TOKEN_MANAGER
                    TOKEN_MANAGER = SerializeHelper.DeserializeFromFile<TokenManager>(root +
                             Bio.Portal.Server.Services.Properties.Settings.Default.PathConfigToken);
                    if (TOKEN_MANAGER == null || TOKEN_MANAGER.TokensConfigured.Count == 0)
                    {
                        LOG.Info("   BioPortal Server Reading Tokens - No Token Configured!");
                    }
                    else
                    {
                        LOG.Info("   BioPortal Server Reading Tokens = OK - Tokens Configured = " +
                        TOKEN_MANAGER.TokensConfigured.Count.ToString());

                        //Si levanto connectors, inicializo creando las instancias
                        TOKEN_MANAGER.Initialization();

                        if (TOKEN_MANAGER.QuantityTokensAvailables() == 0)
                        {
                            LOG.Info("   BioPortal Server Initializing Tokens = WARNING - No Tokens Availables!");
                        }
                        else
                        {
                            LOG.Info("   BioPortal Server Initializing Tokens = OK  - Tokens Availables = " +
                                           TOKEN_MANAGER.QuantityTokensAvailables().ToString());
                            LOG.Info("   Tokens Availables = " + TOKEN_MANAGER.TokensAvailablesToString());
                        }
                    }

                    //4. Traer las compañía
                    LOG.Debug("Global.Application_Start - Cargando Compañias...");
                    LoadCompanies();

                    //BpClient[] clientsArray;
                    //string msgErr;
                    //bool retorno = Bio.Portal.Server.Common.Entities.AdminBpClient.RetrieveAll(out clientsArray, out msgErr);

                    //Added 04-2018 - Tupo Notarizacion
                    LOG.Debug("Global.Application_Start - Configurando tipo notarizacion...");
                    try
                    {
                        NV_TYPE = Properties.Settings.Default.NVType;
                        if (NV_TYPE < 0 || NV_TYPE > 2)
                        {
                            LOG.Error("Ajuste parteando Properties.Settings.Default.NVType [valor en Web.Config = " +
                                Properties.Settings.Default.NVType +
                                " no valido. dee ser 0 - OnLine | 1 - OffLine | 2 - First OnLine => Second OffLine...Frozando Default = 0]");
                            NV_TYPE = 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("Global.Application_Start - Error parseando Properties.Settings.Default.NVType [" + ex.Message + "]");
                        NV_TYPE = 0;
                    }
                    LOG.Info("Global.Application_Start - NVType Configured = " + NV_TYPE.ToString());

                    //Added 04-07-2020 - Para manejo de 3eros en BPWeb
                    LOG.Debug("Global.Application_Start - Configurando Jumio Helper...");
                    bool bInitJumio = false;
                    try
                    {
                        JUMIO_HELPER = new JumioHelper();

                        bInitJumio = 
                            JUMIO_HELPER.Initialization(Properties.Settings.Default.Jumio_UrlBaseOnBoarding,
                                                        Properties.Settings.Default.Jumio_UrlBaseAuthentication,
                                                        Properties.Settings.Default.Jumio_UrlBaseGet,
                                                        Properties.Settings.Default.Jumio_UrlBaseGetAuth,
                                                        Properties.Settings.Default.Jumio_Token,
                                                        Properties.Settings.Default.Jumio_Secret,
                                                        Properties.Settings.Default.Jumio_Timeout);
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("Global.Application_Start - Error Inicializando JumioHelper [" + ex.Message + "]");
                        JUMIO_HELPER = null;
                    }
                    LOG.Info("Global.Application_Start - Jumio Helper Inicializado OK => " + bInitJumio.ToString());

                    
                    LOG.Debug("Global.Application_Start - Configurando Facetec Helper...");
                    bool bInitfacetec = false;
                    try
                    {
                        FACETEC_HELPER = new FacetecHelper(); 

                        bInitfacetec =
                            FACETEC_HELPER.Initialization(Properties.Settings.Default.Facetec_UrlBase,
                                                        Properties.Settings.Default.Facetec_LicenseKey,
                                                        Properties.Settings.Default.Facetec_LicenseFacemapEncriptionKey,
                                                        Properties.Settings.Default.Facetec_Timeout);
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("Global.Application_Start - Error Inicializando FacetecHelper [" + ex.Message + "]");
                        JUMIO_HELPER = null;
                    }
                    LOG.Info("Global.Application_Start - Facetec Helper Inicializado OK => " + bInitfacetec.ToString());

                    LOG.Debug("Global.Application_Start - Configurando Regula Handler...");
                    bool bInitRegula = false;
                    try
                    {
                        REGULA_HANDLER = new Biometrika.Regula.WebAPI.Api._2021.RegulaHandler();
                        int ret = REGULA_HANDLER.Init(Properties.Settings.Default.RegulaURL,
                                                      Properties.Settings.Default.LicenseFile);
                        if (ret == 0)
                        {
                            bInitRegula = REGULA_HANDLER.IsInitialized(); // NamkuUtils.GetOrRefreshToken();
                            LOG.Debug("Global.Application_Start - Regula.Initialize Configured OK!");
                        }
                        else
                        {
                            bInitRegula = false;
                            LOG.Warn("Global.Application_Start - Regula.Initialize Error - NOT Configured!");
                        }
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("Global.Application_Start - Error Inicializando FacetecHelper [" + ex.Message + "]");
                        JUMIO_HELPER = null;
                    }
                    LOG.Info("Global.Application_Start - Regula Handler Inicializado OK => " + bInitRegula.ToString());

                    try
                    {
                        LOG.Info("Global.Application_Start - Read Disclaimer Autorization dafault BPWeb..."); 
                       _DISCLAIMER_AUTORIZATION_BPWEB =
                            File.ReadAllText(root + Properties.Settings.Default.Autorization_Disclaimer_Default_BPWEB);
                        if (string.IsNullOrEmpty(_DISCLAIMER_AUTORIZATION_BPWEB))
                        {
                            _DISCLAIMER_AUTORIZATION_BPWEB = @"<p><b>Autorización</b></p>" +
						        @"<p>Al utilizar el servicio autorizo expresamente a Biometrika y Notario Virtual para encriptar y almacenar en sus registros mis datos personales:" +
                                @"Nombre, RUT, fecha de nacimiento, género y patrón facial. A cotejar luego esos datos contra aquellos que se capturen en el futuro, con la finalidad de verificar mi identidad." +
                                @"Dicho datos no serán comunicados a terceros y podrá pedir que sean eliminados enviando mail a soporte@biometrikalatam.com" +
                                @"<p></p>" +
                                @"<i>*El patrón biométrico facial almacenado es una representación matemática de los puntos característicos de una imágen de la persona, que se almacena encriptada, y NO puede ser utulizado en un sistema ajeno al de Notario Virtual.</i></p>";
                        }
                    }
                    catch (Exception ex)
                    {
                        _DISCLAIMER_AUTORIZATION_BPWEB = @"<p><b>Autorización</b></p>" +
                            @"<p>Al utilizar el servicio autorizo expresamente a Biometrika y Notario Virtual para encriptar y almacenar en sus registros mis datos personales:" +
                            @"Nombre, RUT, fecha de nacimiento, género y patrón facial. A cotejar luego esos datos contra aquellos que se capturen en el futuro, con la finalidad de verificar mi identidad." +
                            @"Dicho datos no serán comunicados a terceros y podrá pedir que sean eliminados enviando mail a soporte@biometrikalatam.com" +
                            @"<p></p>" +
                            @"<i>*El patrón biométrico facial almacenado es una representación matemática de los puntos característicos de una imágen de la persona, que se almacena encriptada, y NO puede ser utulizado en un sistema ajeno al de Notario Virtual.</i></p>";
                        LOG.Error(" Error: " + ex.Message);
                    }
                    LOG.Info("Global.Application_Start - Disclaimer => ");
                    LOG.Info("Global.Application_Start - " + _DISCLAIMER_AUTORIZATION_BPWEB);
                }
                else
                {
                    LOG.Fatal("Global.Application_Start - Chequeo de Licencia NO OK. Revise archivo de licencia y reinice el servidor!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Global.Application_Start ERROR", ex);
            }

            //5. Fin Inicio
            LOG.Info("BioPortal Server Started!");
            //bool resp = ArrCompanys.ExistCompanyId(1);
            //Company com = ArrCompanys.getCompany(1);

            //UpdateTest();
            //CreateTest();
        }

        /// <summary>
        /// Carga las compañias con sus status.
        /// </summary>
        internal static string LoadCompanies()
        {

            //ManagerServices.BioPortalManagerServices mansrv = new ManagerServices.BioPortalManagerServices();
            //string xmloutput = "";
            //int res = mansrv.GetCompanys(out xmloutput);
            //ArrCompanys = Bio.Core.Utils.SerializeHelper.DeserializeFromXml<Companys>(xmloutput);
            int err = Errors.IERR_OK;
            if (ArrCompanys == null) ArrCompanys = new Companys();
            lock (ArrCompanys)
            {
                err = DatabaseHelper.GetCompanys(out ArrCompanys);
            }
            if (err != Errors.IERR_OK)
            {
                LOG.Fatal("Global.LoadCompanies ERROR - Imposible obtener Companias Habilitadas [" +
                           err.ToString() + "]");
                return "Global.LoadCompanies ERROR - Imposible obtener Companias Habilitadas [" +
                       err.ToString() + "]";
            }
            else if (ArrCompanys == null)
            {
                LOG.Warn("Global.LoadCompanies Warning - Lista de Companias Nula!!");
                return "Global.LoadCompanies Warning - Lista de Companias Nula!!";
            }
            else
            {
                LOG.Info("Global.LoadCompanies - Companias OK");
                LOG.Debug("Compañias leidas > " + ArrCompanys.CompanysToString());
            }
            return null;
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (MATCHER_MANAGER != null && MATCHER_MANAGER.QuantityMatchersAvailables() > 0)
            {
                MATCHER_MANAGER.Release();
            }

            if (CONNECTOR_MANAGER != null && CONNECTOR_MANAGER.QuantityConnectorsAvailables() > 0)
            {
                CONNECTOR_MANAGER.Release();
            }
        }

        #region Global Helper

        internal static int SERVICE_WS = 0;
        internal static int SERVICE_WSWEB = 1;
        internal static int SERVICE_CONNECTOR_SRCeI = 2;
        internal static int SERVICE_PLUGIN_PROCESS = 3;


        /// <summary>
        /// Chequea si la licencia del producto está ok para el servicio utilizado.
        /// </summary>
        /// <param name="flag">0-WS | 1-WS Web Plugin</param>
        /// <returns>true si está ok, false sino</returns>
        internal static bool LicenseValid(int flag)
        {
            bool ret = false;
            try
            {
                ////Si es Web Service Local solo => OK
                if (flag == SERVICE_WS)
                {
                    ret = true;
                }

                //Si es Web Service Web Plug-In
                if (flag == SERVICE_WSWEB)
                {
                    ret = WS_WEB_PLUGIN_ENABLED;
                }

                //Chequeo si tiene ok para Connector SRCeI
                if (flag == SERVICE_CONNECTOR_SRCeI)
                {
                    ret = WS_CONNECTOR_SRCeI_ENABLED;
                }

                //Chequeo si esta habilitado el plugin process
                if (flag == SERVICE_PLUGIN_PROCESS)
                {
                    ret = (PLUGIN_PROCESS == 1);
                }

                return ret;
            }
            catch (Exception ex)
            {
                LOG.Error("Global.LicenseValid Error", ex);
                return false;
            }
            return ret;
        }

        //Objeto de licencia para chequeo al comienzo y
        //durante la operacion
        //internal static BioLicenseCheck.BioLicense oBKLic;
//        internal static BiometrikaLCheck.BioLicense oBKLic;
        internal static BKLicense oBKLic;


        /// <summary>
        /// Chequea licnecia dado el path, y lanza un thread para que se chequee cada 1 hora.
        /// </summary>
        /// <param name="pathLicense">Path del archivo de licencia</param>
        internal bool CheckLicense(string pathLicense)
        {
            bool result = false;
            try
            {
                LOG.Info("Global.Application_Start - CheckLicense In...");
                //BiometrikaLCheck.LCheck oCheck = new LCheck(); //BioLicenciaCheckClass();
                LicenseCheck  oCheck = new LicenseCheck(); //BioLicenciaCheckClass();
                string sPathLicencia = pathLicense;
                string msgErr = null;
                //Collection oVal = oCheck.CheckLicencia(ref sPathLicencia, ref msgErr);
                //string sCodigo = "BPS4";
                string sCodigo = "BPS5";
                string AttrAux;
                //oBKLic = oCheck.CheckLicencia(ref sPathLicencia, ref sCodigo);
                int iret = oCheck.CheckLicense(sPathLicencia, sCodigo, 3600000, out oBKLic, out msgErr);
                if (iret != 0 || oBKLic == null || !oBKLic.Status)
                {
                    if (oBKLic == null)
                    {
                        LOG.Fatal("Global.Application_Start - Objeto de licencia nulo");
                    }
                    else if (!oBKLic.Status)
                    {
                        LOG.Fatal("Global.Application_Start - Licencia Inválida! [" + msgErr + "]");
                    }
                    result = false;
                }
                else
                {
                    if (!ParseLicenses(oBKLic))
                    {
                        LOG.Warn("Global.Application_Start - Warning Licencias Inválidas - " +
                                    "Parseo de licencia inválida. Ver log...");
                        result = false;
                    }
                    else
                    {
                        LOG.Warn("Global.Application_Start - BioPortal Check License OK!");
                        result = true;
                    }
                }


                //if (oBKLic == null || !oBKLic.StatusLicense)
                //{
                //    if (oBKLic == null)
                //    {
                //        LOG.Fatal("Global.Application_Start - Objeto de licencia nulo");
                //    }
                //    else if (!oBKLic.StatusLicense)
                //    {
                //        LOG.Fatal("Global.Application_Start - Licencia Inválida! [" +
                //            oBKLic.Msg + "]");
                //    }
                //    result = false;
                //}
                //else
                //{
                //    if (!ParseLicenses(oBKLic))
                //    {
                //        LOG.Warn("Global.Application_Start - Warning Licencias Inválidas - " +
                //                    "Parseo de licencia inválida. Ver log...");
                //        result = false;
                //    }
                //    else
                //    {
                //        LOG.Warn("Global.Application_Start - BioPortal Check License OK!");
                //        result = true;
                //    }
                //}

                //Hago refresh, que la primera vez chequea desde path
                //Global.RefreshStatusLicense();

                ////Lanzo thread para chequeao cada 60 minutos =>>>> esto lo elimino porque la nueva lib de licencia lo hace solo
                Thread tLicenseBP = new Thread(new ThreadStart(ThreadCheckLicenseBP));
                tLicenseBP.Start();
                LOG.Info("Global.Application_Start - CheckLicense Out!");
            }
            catch (Exception ex)
            {
                LOG.Error("Global.CheckLicense", ex);
            }
            return result;
        }

        /// <summary>
        /// Hace refresh del estado de la licencia.
        /// </summary>
        /// <returns></returns>
        //internal static bool RefreshStatusLicense()
        //{
        //    bool ret;
        //    bool refresco;
        //    string root = HttpContext.Current.Server.MapPath(".");

        //    try
        //    {
        //        //Si es nulo, la chequeo desde path (primera vez)
        //        if (oBKLic == null)
        //        {
        //            oBKLic = new BioLicenseClass();

        //            BiometrikaLCheck.LCheck oCheckLic = new LCheck(); //BioLicenciaCheck();
        //            string sPathLicencia =   Bio.Portal.Server.Services.Properties.Settings.Default.PathLicense;
        //            string sCodigo = "BPS4";
        //            oBKLic = oCheckLic.CheckLicencia(ref sPathLicencia, ref sCodigo);
        //            refresco = true;
        //        }
        //        else	//Sino solo la valido
        //        {
        //            DateTime dtv = oBKLic.Vencimiento;
        //            oBKLic.IsValid(ref oBKLic);
        //            refresco = !(dtv.Equals(oBKLic.Vencimiento));
        //        }

        //        if (oBKLic == null || !oBKLic.StatusLicense)
        //        {
        //            if (oBKLic != null)
        //            {
        //                LOG.Fatal("Global.Application_Start - " +
        //                    "BioPortal Server - LICENCIA ANÓMALA! [" +
        //                    oBKLic.Msg + "]");
        //            }
        //            else
        //            {
        //                LOG.Fatal("Global.Application_Start - " +
        //                    "BioPortal Server - LICENCIA ANÓMALA! [" +
        //                    "Imposible chequear licencia]");
        //            }
        //            ret = false;
        //        }
        //        else
        //        {
        //            //Si refresco la licencia, reparseo,
        //            //sino dejo como está para ser mas rápido
        //            if (refresco)
        //            {
        //                if (!ParseLicenses(oBKLic))
        //                {
        //                    LOG.Warn("Global.Application_Start - " +
        //                                "Parseo de licencias locales y RC inválidas!");
        //                }
        //            }
        //            LOG.Info("Global.Application_Start - Chequeo de Licencia BP OK! " +
        //                oBKLic.Msg);
        //            ret = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Global.RefreshStatusLicense - " + ex.Message);
        //        ret = false;
        //    }
        //    return ret;
        //}

        ///// <summary>
        ///// Thread para chequear la licencia. Se despierta cada 1 hora.
        ///// </summary>
        internal static void ThreadCheckLicenseBP()
        {
            bool bRunning = true;
            try
            {
                while (bRunning)
                {
                    //Chequeo licencia, y si da falso, entonces sale del loop
                    bRunning = oBKLic.Status;

                    LOG.Info("Global.ThreadCheckLicenseBP - " +
                                "Chequeó licencia en thread [" +
                                bRunning.ToString() + "]");
                    //Duerme una hora y rechequea
                    //(1000 miliseg x 60 seg x 60 minutos =
                    //				 3600000 milisegundos = 1 hora)
                    Thread.Sleep(3600000);

                    //Para test uso 1 minuto
                    //Thread.Sleep(60000);
                }

                //Added 08-2018 con cambio de Check License
                //   Si llega aca es que salio del thread de chequeo de la licencia,m y es que la licencia está inválida. 
                //      => Deshabilita todos los servicios del BioPortal Server
                WS_WEB_PLUGIN_ENABLED = false;
                PLUGIN_PROCESS = -1;
                WS_CONNECTOR_SRCeI_ENABLED = false;

    }
            catch (Exception ex)
            {
                LOG.Error("Global.ThreadCheckLicense - " + ex.Message);
            }
        }

        /// <summary>
        /// Toma del objeto de licencia, ciertos datos incluidos en el, que afectan
        /// el funcionamiento del BioPortal Server.
        /// </summary>
        /// <param name="oBkLic">Objeto de licencia</param>
        /// <returns>true o false si no hubo o hubo error respectivamente</returns>
        //internal static bool ParseLicenses(BioLicense oBkLic)
        internal static bool ParseLicenses(BKLicense oBkLic)
        {
            string nattr = "";

            //Added para chequeo de fechas por si no anda en el chequeo de licencia lib

            //IP del servidor
            try
            {
                nattr = "IP";
                //IP_SERVER = oBKLic.GetAttrAdicionalByName(ref nattr);
                IP_SERVER = oBKLic.GetValueAtrAdic(nattr);
                LOG.Debug(" >> Global.ParseLicenses - IP_SERVER=" + IP_SERVER.ToString());
            }
            catch (Exception lex)
            {
                LOG.Warn(" >> Global.ParseLicenses [IP_SERVER] - " + lex.Message);
                IP_SERVER = "";
            }

            //Web Plugin Habilitado o no
            try
            {
                nattr = "WS_WEB_PLUGIN_ENABLED";
                //WS_WEB_PLUGIN_ENABLED = Boolean.Parse(oBKLic.GetAttrAdicionalByName(ref nattr));
                WS_WEB_PLUGIN_ENABLED = Boolean.Parse(oBKLic.GetValueAtrAdic(nattr));
                LOG.Debug(" >> Global.ParseLicenses - WS_WEB_PLUGIN_ENABLED=" + WS_WEB_PLUGIN_ENABLED.ToString());
            }
            catch (Exception lex)
            {
                LOG.Warn(" >> Global.ParseLicenses [WS_WEB_PLUGIN_ENABLED] - " + lex.Message);
                WS_WEB_PLUGIN_ENABLED = false;
            }

            //Parseo Licencia a Conector Servicios SRCeI
            try
            {
                nattr = "WS_CONNECTOR_SRCeI_ENABLED";
                //WS_CONNECTOR_SRCeI_ENABLED = Boolean.Parse(oBKLic.GetAttrAdicionalByName(ref nattr));
                WS_CONNECTOR_SRCeI_ENABLED = Boolean.Parse(oBKLic.GetValueAtrAdic(nattr));
                LOG.Debug(" >> Global.ParseLicenses - WS_CONNECTOR_SRCeI_ENABLED = " + WS_CONNECTOR_SRCeI_ENABLED.ToString());
            }
            catch (Exception rcex)
            {
                LOG.Warn(" >> Global.ParseLicenses [WS_CONNECTOR_SRCeI_ENABLED] - " + rcex.Message);
                WS_CONNECTOR_SRCeI_ENABLED = false;
            }

            //Plugin Process Habilitado o no //Added 16-02-2018
            try
            {
                nattr = "PLUGIN_PROCESS";
                //PLUGIN_PROCESS = Convert.ToInt32(oBKLic.GetAttrAdicionalByName(ref nattr));
                PLUGIN_PROCESS = Convert.ToInt32(oBKLic.GetValueAtrAdic(nattr));
                LOG.Debug(" >> Global.ParseLicenses - PLUGIN_PROCESS=" + PLUGIN_PROCESS.ToString());
            }
            catch (Exception lex)
            {
                LOG.Warn(" >> Global.ParseLicenses [PLUGIN_PROCESS] - " + lex.Message);
                PLUGIN_PROCESS = -1;
            }

            //Parseo Licencia a Conector Servicios SRCeI
            try
            {
                nattr = "QCONCURRENT";
                //Q_CONCURRENT = Int32.Parse(oBKLic.GetAttrAdicionalByName(ref nattr));
                Q_CONCURRENT = Int32.Parse(oBKLic.GetValueAtrAdic(nattr));
                LOG.Debug(" >> Global.ParseLicenses - QCONCURRENT = " + Q_CONCURRENT.ToString());
            }
            catch (Exception rcex)
            {
                LOG.Warn(" >> Global.ParseLicenses [Q_CONCURRENT] - " + rcex.Message);
                Q_CONCURRENT = 0;
            }

            //Parseo Licencia de producto si tiene el codigo correcto
            bool licenciaBP = false;
            try
            {
                //licenciaBP = oBKLic.codigo.Equals("BPS4");
                licenciaBP = oBKLic.Codigo.Equals("BPS5");
                LOG.Debug("BP.ParseLicenses - [licenciaBP == BPS5 > " + licenciaBP.ToString() + "]");
                if (!licenciaBP)
                {
                    LOG.Fatal("Global.ParseLicenses - " +
                        "La licencia configurada no corresponde a este producto. " +
                        "Debe ser CODIGO = BPS5");
                }
                else
                { //Si es Q_CONCURRENT > 0 agrego chequeo)
                    if (Q_CONCURRENT > 0)
                    {
                        LOG.Debug("BP.ParseLicenses - Chequeando Clientes Concurrentes...");
                        licenciaBP = licenciaBP && IsValidQClientesConcurrentes(Q_CONCURRENT);
                        if (!licenciaBP)
                        {
                            LOG.Fatal("Global.ParseLicenses - " +
                                "Ha excedido la cantidad de clientes concurrentes definidos en la licencia. Comúníquese con el proveedor...");
                            WS_WEB_PLUGIN_ENABLED = false;
                            WS_CONNECTOR_SRCeI_ENABLED = false;
                        }
                    }
                    else
                    {
                        LOG.Info("Global.ParseLicenses - Q_CONCURRENT= 0 => No control clientes concurrentes!");
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Warn("Global.ParseLicenses " +
                                   "[Checking Licencia BP] - " + ex.Message);
                licenciaBP = false;
            }

            return licenciaBP;
        }

        private static bool IsValidQClientesConcurrentes(int qconcurrent)
        {
            bool isValid = false;
            try
            {
                //Si esta configurado para no chequear => Da error
                if (Global.CONFIG.CheckClientConnected == 0)
                {
                    isValid = false;
                    LOG.Info("Global.IsValidQClientesConcurrentes - Global.CONFIG.CheckClientConnected Disabled! Debe habilitarlo apra proseguir...");
                }
                else
                {
                    if (qconcurrent > 0)
                    {
                        int qCurrent = DatabaseHelper.QClientCurrent();

                        isValid = (qconcurrent >= (qCurrent * 1.15));   //Verifica que haya una cantidad de concurrentes de no mayor de 15%
                        //de lo almacenado en SerialID diferentes
                        LOG.Info("Global.IsValidQClientesConcurrentes " +
                                           "Q_CONCURRENT=" + qconcurrent + " - Q_Current=" + (qCurrent * 1.15));
                    }
                    else
                    {
                        LOG.Info("Global.IsValidQClientesConcurrentes - Q_CONCURRENT= 0 => No control clientes concurrentes!");
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Warn("Global.IsValidQClientesConcurrentes " +
                                   "[Error chequeando Q_CONCURRENT] - " + ex.Message);
                isValid = false;
            }

            return isValid;
        }

        #endregion Global Helper

        private ImageSample ConvertStringToImageSample(string _code, string _sSample)
        {
            ImageSample ret = new ImageSample();
            try
            {
                if (string.IsNullOrEmpty(_sSample))
                    return null;

                string[] arrAux1 = _sSample.Split(',');
                if (arrAux1.Length != 2)
                    return null;

                ret.code = _code;
                ret.data = arrAux1[1];

                string[] arrAux2 = _sSample.Split(';')[0].Split(':');

                if (arrAux2[1].Contains("pdf"))
                {
                    ret.typeimage = TypeImage.pdf;
                    ret.typeimageformat = TypeImageFormat.pdf;
                } else if (arrAux2[1].Contains("jpg") || arrAux2[1].Contains("jpeg"))
                {
                    ret.typeimage = TypeImage.image;
                    ret.typeimageformat = TypeImageFormat.jpg;
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }

        public void test()
        {

            Bio.Portal.Server.Services.APIRest.Models.WA.EnrollModel em = new APIRest.Models.WA.EnrollModel();
            em.type = 0;
            em.deviceid = "serial-de-android";
            em.companykey = "clave-de-empresa-fija-ahora-no-interesa";
            em.geolocalization = "coordenadas";
            em.typeid = "RUT";
            em.valueid = "11111111-1";
            em.samples = new List<APIRest.Models.WA.WASample>();
            APIRest.Models.WA.WASample s = new APIRest.Models.WA.WASample();
            s.additionaldata = null;
            s.authenticatorfactor = 4;
            s.minutiaetype = 41;
            s.bodypart = 16;
            s.data = "JPG-en-base64-de-cara-en-800x800";
            em.samples.Add(s);
            s = new APIRest.Models.WA.WASample();
            s.additionaldata = null;
            s.authenticatorfactor = 4;
            s.minutiaetype = 41;
            s.bodypart = 16;
            s.data = "Otra-JPG-en-base64-de-cara-en-800x800";
            em.samples.Add(s);
            string json = JsonConvert.SerializeObject(em);

            int i = 0;

            //string sSamples = "[\"data:application/pdf;base64,JVBERi0xLjUKJeLjz9MKMyAwIG9iago8PC9UeXBlL1hPYmplY3QvQ29sb3JTcGFjZS9EZXZpY2VSR0IvU3VidHlwZS9JbWFnZS9CaXRzUGVyQ29tcG9uZW50IDgvV2lkdGggMjYwL0xlbmd0aCAzMzQ3MS9IZWlnaHQgMjYyL0ZpbHRlci9EQ1REZWNvZGU+PnN0cmVhbQr/2P/gABBKRklGAAEBAQEsASwAAP/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAQYBBAMBIQACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/APn5v25v21yzH/hr79qEDJwB8f8A4sgdT2/4S7jt/iaT/hub9tb/AKO//ai/8SA+LP8A811f60rw18O2l/xgfBuy/wCaXyPXRa/7h9/mf5JPxH8RG7/6/wDGutn7vFWeJba2/wBu2D/hub9tb/o7/wDai/8AEgPiz/8ANdR/w3N+2t/0d/8AtRf+JAfFn/5rqf8AxDXw7/6IPg3/AMRfI/8A5gF/xEbxE/6L/jb/AMSrPP8A5uD/AIbm/bW/6O//AGov/EgPiz/811H/AA3N+2t/0d/+1F/4kB8Wf/muo/4hr4d/9EHwb/4i+R//ADAH/ERvET/ov+Nv/Eqzz/5uD/hub9tb/o7/APai/wDEgPiz/wDNdR/w3N+2t/0d/wDtRf8AiQHxZ/8Amuo/4hr4d/8ARB8G/wDiL5H/APMAf8RG8RP+i/42/wDEqzz/AObg/wCG5v21v+jv/wBqL/xID4s//NdR/wANzftrf9Hf/tRf+JAfFn/5rqP+Ia+Hf/RB8G/+Ivkf/wAwB/xEbxE/6L/jb/xKs8/+bg/4bm/bW/6O/wD2ov8AxID4s/8AzXUf8Nzftrf9Hf8A7UX/AIkB8Wf/AJrqP+Ia+Hf/AEQfBv8A4i+R/wDzAH/ERvET/ov+Nv8AxKs8/wDm4P8Ahub9tb/o7/8Aai/8SA+LP/zXV9T+HP2zf2uZdA0OWb9p79oOeaXRdHllmn+NXxMkmlll0y1kkllkfxSWkkkkdnkdiWdiWYliSf4V+nfkeT8IcDcE4rhLK8v4WxeL4rxGHxWK4cwWFyTE4nDxyjFVY0K9fLKOGqVqKqRjUVKpKUFOMZqPMkz/AGY/Yt4HDeK3jR4u5V4n04+IuV5Z4XYXMcty7jqEOLsDgMfLizJ8NLG4LCcQwzHD4XFvD1atCWJoU6dd0ak6Tm6c5Re1/wANlfta/wDRzPx9/wDDz/En/wCamj/hsr9rX/o5n4+/+Hn+JP8A81Nf5ff6xcR/9FHn/wD4eMf/APLz/o2/4l98DP8Aoz3hf/4r7g//AOcof8Nlfta/9HM/H3/w8/xJ/wDmpo/4bK/a1/6OZ+Pv/h5/iT/81NH+sXEf/RR5/wD+HjH/APy8P+JffAz/AKM94X/+K+4P/wDnKH/DZX7Wv/RzPx9/8PP8Sf8A5qaP+Gyv2tf+jmfj7/4ef4k//NTR/rFxH/0Uef8A/h4x/wD8vD/iX3wM/wCjPeF//ivuD/8A5yh/w2V+1r/0cz8ff/Dz/En/AOamj/hsr9rX/o5n4+/+Hn+JP/zU0f6xcR/9FHn/AP4eMf8A/Lw/4l98DP8Aoz3hf/4r7g//AOcof8Nlfta/9HM/H3/w8/xJ/wDmpo/4bK/a1/6OZ+Pv/h5/iT/81NH+sXEf/RR5/wD+HjH/APy8P+JffAz/AKM94X/+K+4P/wDnKH/DZX7Wv/RzPx9/8PP8Sf8A5qaP+Gyv2tf+jmfj7/4ef4k//NTR/rFxH/0Uef8A/h4x/wD8vD/iX3wM/wCjPeF//ivuD/8A5yh/w2V+1r/0cz8ff/Dz/En/AOamj/hsr9rX/o5n4+/+Hn+JP/zU0f6xcR/9FHn/AP4eMf8A/Lw/4l98DP8Aoz3hf/4r7g//AOcof8Nlfta/9HM/H3/w8/xJ/wDmpo/4bK/a1/6OZ+Pv/h5/iT/81NH+sXEf/RR5/wD+HjH/APy8P+JffAz/AKM94X/+K+4P/wDnKH/DZX7Wv/RzPx9/8PP8Sf8A5qaP+Gyv2tf+jmfj7/4ef4k//NTR/rFxH/0Uef8A/h4x/wD8vD/iX3wM/wCjPeF//ivuD/8A5ynU6F+2D+1ZNaSvN+0l8eZWFy6hn+MXxHchRFCQAW8Tk4yScA4ySe9Fc8+J+JVJpcRZ7Zaf8jfMOi/7CD5nGeA3gnDE1ox8I/DOMYzskuAuEkkrJ2SWTpL5I/HBup+p/nSV/wBNEdl6L8j/AIJI7L0X5BRTGFJklgqqWLMFAHLFmOFAA5JJOAOpPHplN2V3tp+e/wAioxcnZb6WVm222lZJddT3T4f/ALMP7R/xXs9Yv/hr8CPiz45stAhebV7vwz4E8R6rb2SxwxXDxNNbaeyTXYgnhnFjbma9aGaGUQeXKjN4neWl5p17d6dqFndWF/YXNxZX1je281peWV7aSvBdWd5a3CRz213bTxvDcW00aTQyo8cqI6Mo8nA59k2Z4zG5fl+aZfjcdlvsP7QwmFxlGvXwaxMeag69OnKUqaqJS5XJJNxlG900vbzPhnP8mwWAzHNcozHL8FmftvqOIxmEq4eniZYeUYVowdSMfeg5J2klKUGqkU4NMgor1zwgooAK+tfDX/Iu6B/2AtE/9NNlX+en7RH/AJN/wD/2WOK/9UmLP91f2Dn/ACfjxr/7NFhP/WzyM9R8L/DX4heNtE8XeJfCHgvxL4m8PeANOj1fxxrWiaTdahpvhLS5oryaLUPEF3AjRabaSxadfyRz3LIjLZ3BBxG1eh+Ff2Wv2lPHXhjTvGvgr4D/ABX8W+EdXt57rSvEfhzwRrms6TqVvbXE1pcS2F1YWk6XaRXdtcWz+RvIngkjxuUgf5PYfLswxcoRwuAx2JlUpTr044bBYrEOdCnV9hUrQVGlNzo0637mdWKdOFX91KSn7p/0pZ/4neHfCv13/WbjfhjIFluaYLJMwlm+cYTAQwWb5jlkc6wOW4qeIqQhQxmLyicczoUakoyngn9YVqd5Ly638EeNbrxUPAtv4O8Vz+NzfHS/+EMh8OazL4tXUgu9tPfw2lkdZS9jT55bVrITRRgySIqAkepfEb9ln9pH4Q6Ivib4m/A/4leC/DZMKyeINY8MX39iWjXMiQ28ep6pZrd2OkS3E8kVvBDqs9lLNPKkEaNMyxsqWX4+vRxOIo4LFVaGE/3qtSw9apSwytd+3qQg4UbJNv2ko2SvsaZj4k+H+UZxw3w/mfGnDOBzvjCnTrcLZZiM4wccTn1Gu4Rw1XLUqrhiKWNnUhTy6opqGZVn7DL5YmunTXgtFch9qFFABRQAUUAFFABRQAUUAdl4d/48pf8Ar6f/ANEwUVxVPjl6nyWP/wB7r/4//bUfn83U/U/zpK/6hI7L0X5H/nlx2XovyCkyPUc9OetMqzeyb+R7T8AfgB8Sv2l/iPZfC/4V6ZDqPiGfSda8RX9xePdR6Zofhvw3YSajruvao1hZ6jqclpY2qqsVlpOmaprOqX89lpOi6ZqGq39nZzf0Df8ABJv/AIJ76P8AD/8Aa+ufH/jHxP4S+KVt8K/CeoJaaPHoOqWOu/D74qa2dMufCOu+ItA1eC6s4dLvvDNv45Xwj4jtNRvFtfFehappWpW/h7xTotpaS/g/jV4gUMl4d4o4cwkMRPMq3DNWpicXRqUqdPLKObVZZdhZVL1I1pSxMvbUIyormpVa+EaU41Ks8P8A0P4G+G2JzrOeHOLsXXwlHLcJxLSp4PBYilUq1c0rZXS/tDEum1H2NP6s40qtONWUvaxo4tyVJ0KMcX/Txofw68G+HNa8SeJ9P0GwPiTxbqlzq+v+Ibm0t7jW7+Wa0ttOhs5dSeM3Y02y0yytNOsNOST7Lb2tuqrH5jzSS/zaftx/8EdvFfxi+IPxA/ai8Gapq2geF73w1BfXnwq0nwppev8AxY1J/BHg/wANeHLHxGLe88W+FND1DVvHEGi6v4v8U2kl+vjC1vpra0tvD3ibxVrV9pum/wAg+DviQ+CuKcTmuYtYujmeXYbII0quIqUKeGjKrh5YPEzqRo4mVPAZesHGliI0qNSrClXjOjCcoOEv678WvDqHH3DFDJ6FaWCqYHM1nar0cNDEVKzpUa9PE4enTnXwsJ4rGQxdWVGdWtGm6sLVZRjLmh/OD8cfhBq3wR8dP4P1K9fUre50y113RdTms49Nub7SLu6vrDdfabFfapBYX9nqmlarpN7BaanqmnvdafLc6Vqmo6ZPaX1x5Dkeo/Ov9I8ozGnmuWYLMYL2ccZh6df2bnz+ylKKVSnzuMHNU6ilBT5IKdudQipJL/NbiHJ6mQ51mWT1J+2ll2Lq4ZVlD2ftqcGvZVpUlKoqUqtOUJypqpUVOcnDnm4uUlor0zxQr618Nf8AIu6B/wBgLRP/AE02Vf56ftEf+Tf8A/8AZY4r/wBUmLP91f2Dn/J+PGv/ALNFhP8A1s8jP2M/4J6f8mk/8FO/+yHeHv10P4pZ/kK+prFv2wD/AME4/wBhOD9jw/EdfF1zrfjeHxPJ8PVtCsWkv4j8VrpkniWXVQ2iQaOuqtG3mazt05ZA32k/ZxLj/NvKP7R/s3C/2S8Usw/1Txyw/wBS9p9ZbfG+EVRU/Y/vLexdT2jWipc7m1BSa/1p8W/9Qf8AiKPFD8UFlD4CX0i+BZ8SLPXU/st4WH0X8a6CxPsv3zk8asKsPGh+/linRjQ/fOB7bq3xp+Cfwo/4KdeE774r+IPDPhX4ja3+yLofw8+JHj0tZL4c8PfHbVtR0u+tZdQvQG0zTLubwrYNpLanftb2UGn3egafdzQ2c4K/nr+3B8M/+Chnwr+HGuL8VfjnrXx5/Zq8Xa3pJk8ZaJ4lg8R+HLxotVi1LwvL4g027tv7W8J+dqdraPBHo89x4TfU1s7CPVrqaWytn686nj8Tl2cLKcc6c8uzbOqnEeX4bEeyeKp42GDp1sdOhCahjMNCtSxNGpKXtPZN88IckpzPlvBnDcF5Bx94Q/8AEXOEKmP/ANevDzwufgZxpmmCr18LkGa8K5nn+JyXh2OLjVhUy7NMdg8Zw7jKMXCSq1o4LD42m8PjYyX5C0V+Vn+pYUUAFFABRQAUUAFFABRQB2Xh3/jyl/6+n/8ARMFFcVT45ep8lj/97r/4/wD21H5/N1P1P86Sv+oSOy9F+R/55cdl6L8gPQ1+637OX/BMXwB46/Yt8X/F/wCKHivVPA3i3x1J4b0H4TzeJ/APjjRNWsPiBdxR6vaW40u28NeK/EHiHwFdaZd2V3JrvhDw3dJrfh6LxTqs15b2dhp2o+F/zHxT4yxfBuVZRicvowxONzDP8swdPDvFYahVq4aOKpV8co08RXwyqUXhadSnXqfWKFHDqpCWJrU6E5SX7D4OcD4HjbN87w+ZTqUsJl/D+ZVlVWDr4ilSxeKoTwmEqurSpVoUsTSrVVXwdKdGtVrzozlhqUqtC68g/YV+JLf8Evf2055v2pfDN7Z+EPGvws8WeA9Y1bw0lr4ribwt4i8R2T6f410JNPu459U0V/Evw/n0bU7aEQ+IbC2Gr282jLrWnyaS/wC5X7PH7af7Hdz8bvBfgXwd+0M3jr4j/FS28R/DXwjrmgeHrzSNN+Gnwk0y11DxX4L0HX/FGvfD/wCHOkX+vad4k02a88P6PdeF7jTfC+neIda8KC3vdPisr/WPwfxb4Tz/AIozDHcZcNZfHNuHs44Sw+Lq4udVUXgHkWHzDEYvD4rCyxFLETliqMcPh6OHhhcTKeL9pL2uEnhF9Y/fvBzirIOEcso8CcTZhDK+Jsp4urYCngVTliFjqueYjAYfBVsFiaVCpRdOnWqVXVrPEUY0cOl7aFSniIyp/THxL/b18WfDf4v6D8GJ/hfrV54rPj3wp4Y8SapLbaXL4SufAut+MPDllF8R/C8ll4oS4S71Lw3r1u914a1HUJZtE1PXfCv2P/hI72HVvD82vrn7fX7Lv7NXxl+JH7NPxX8d+OoPGNhqEnjvSBL4G+JPxHl1HQPHtlF4kOiQah4X0DxLqt1caDfXeo2NjbXFsbS38OHQLG11G7uoL22svwKh4aZlmNDCYTh2eBzPGZpktbiaMJ4vD4NYbAYDFYClTg8XmMcHTo1Hh81lUxlOdenTnVoK0YRo0pVv3/GcdZblUq+KzxY3AYXCZnh8ijOngsTj5YnHY6nXly0sHlkcbiaq9rglChOOGm1CtfnbnVjR/nT8dN8E/wDgqt/wUah0iPxb4f8A2fPgvoOk+GPhh4Qtby40rwr49+I9ppOu6hFb2PhLSvElpZWNt438T6zrmoJpuk6paXN3pdhDpFhNpd7qLTxp9N/8FMP2Vvh7cfCj4WXXgv8AZk1X4d/GH4g/E/4R/s4fBcx3cOn2t6J9Hsk0Gz1+71G+0u4vPP8ACdnoHh7RrrWNMsrfQdZXxt4Z1LSdMh8G+Hdd1f8ApSpneecIcUeGHBuIzGeX0eH8jwE80xWIjVqU+JPr9KGK4gWGrx9vHFUMto5dOjhlSjKTxdadSrWh7PD3/m2lkfDvFvCfirxnTwlHMsTxLnGYU8twkXSjW4dqZbWngeHZ4mlKNCvgMZj8RjYYzFxqSX+xewpwo1FKs5/gP8aPgN47+BWp6NZ+LBpeqaT4ksru88N+LfDTavceF9d/s26+xavZWdzrWjaDqlvqmkztbS3umalpFhenStU0DxHZw3fhjxN4b1rVvGK/p3J80w2dZdhcywamsPiYScY1eRVaVSlUnRr0KvsqlWl7ShXp1KM3RrVqTnCTp1akHGcv5Mz/ACTGcOZvjclx7pvF4GpGFSVJVfZVIVaVOvQrUfbU6Nb2VehVp1qXtaNGp7OpFzpwk3FFfWvhr/kXdA/7AWif+mmyr+B/2iP/ACb/AIB/7LHFf+qTFn+3v7Bz/k/HjX/2aLCf+tnkZ+p/7D/xR+HfgT9mf/goR4X8Y+M/D3hvxF8RfhBomjeBdF1jUrey1HxZqtvo/wARYptP0K2mZZNQuopdSsI3igDMr3cCkZlQH0T4g/tYal4C/wCCe37G3gj4F/HfUvCPxM0LV/HNt8SfD3w+8a3Wi+KNM0m61LxPfaXF4kttIvIL23tZZrm1uLM3G0CSaNoyruQf8yMJm9PB5VTWFx9TC46nw1i8JCeHq1qGIp4mtxbhcZ7KFWi4TjOeAVWr7s1enzRk1do/2v4u8Lcw4t8XMzXEHBNXPuC8y8e+Ds8xsczyn6/keOyDL/o7Y3IquZYinXpzw9bL8JxQsLlk61SMqVPM1Sov94kl8Q/s5+Dfgz8aPijrelftLfHPWPhLZeING1XUNK+It7ZT+JDqHxEvNV06S3/4Sy/vxcCPTb6zm1y71LUtSvtONxerbJJrVlJMZJf0j+J/if4BfsgfsLfFv9lDwp+0j4e/aY8dfGjxPpuu6NZ+BkS68GeA9Hj1jwxcahcyXem614l0fSrmaHw7Lf8A2NdYh1C/1u7gli0pLW1vNSmyyX+zMLlmZ5rVzeMMfPL81yyWUSpS9viZ5lhZYXD16dR1EqtCHtqlfEe63RnQipNOvST9nxiqeJPE3iPwD4SZV4VynwHhuNPDbjrAeJeEqcuVcP5fwRm9HNuIMHiKEaEsHgMyhSwsMsybDvE4GvicPjqlGhhsVTrxcPxC/wA/5/r+gHSivjj+wwooAKKACigAooAKKACigDsvDv8Ax5S/9fT/APomCiuKp8cvU+Sx/wDvdf8Ax/8AtqPz+bqfqf50lf8AUJHZei/I/wDPLjsvRfkFfqv/AMEkf2vW/Z2/aZ8G+CfHmtNN8D/ilrkWiaxo2vPLqvhTwb8SNTfToPAnxYtNEuHaysvEGka7p2j6JqfiC1axv4PC1/fzS323TLRY/h/ErIVxJwNxLlUaaniqmVYvEZc0kqsMywdKWJwMqNRtOjUliKVOn7WLvGFSaalGUoy/QPC7P58Nce8L5n7R08Ms2wmDzCzfLLLcfWhg8cqsIxm61OFCtKt7FxanUpU3FwnGNSH6Oftsf8Ejf2h/jh+0nYfENNN8L6To/jnx7bW3xL+I2k/EzVtV8I2ngU211qmqeP18MfFHVdR8b+B9c0PTtOv7S88BaX4i8eeErm5utCHha+0HTIdWs7f488Kf8E9v2f8A4jaNF41+B3xN/a7+GerXMN54i+Emq/F74Caf4js9aHhu4jvbbxZfS/AvXfFfjrwb4U1QQg+FvGHiT4f6Jo2qXoU6Lda7Ja3lrB+E8NeMqlwvk1DIsNlGa5dkmXZfl+fYfO8QsgzGrHEfWYYXBZbOnQjkVTFYbLMrxn12tXngMtxNaeA9hPDV8dTy5f0bxD4KYfEcW53i83xWe5bmOe5jjcy4fxeTwjneX4SWGWAdfHZrGs3m/wBXr5tmmEeDw2HlWxuHoxx0KqqYLL55s/2U1X9ovwF4yb4Uan8XfBfg+b43+CPhtoVhrPjGwHjm7+Gl34l0n7L4gh06PxtYeJvCvwmtPAt34sstKvr+bxd8SrDUbKKOfT/E2jC1h1Xw4fV/2Z9J/Zh+H/hYXPxegvvjB8dPj9ff8LB+PHxM8ffCvUE0fxLr9yLVZYNKm8T6LY+HfEPwx+H/AJ8eneFLDwIfFdjofhmzvfHU8Nnoqa9r9r+G5plvEOU5NWy3JcTWSxdWlDA46tjcLg8XhMhwmJxea/2bGFCNLERxlbGU8oxOLo1Ks8wwM8uoV8RhMuwkpYmr++4KtleaZpgsdmlKnOpl9OtPFYSnhMVWw1bO8VhMLlv9o06mIXJLC0cFXzbA4WtTh9TxdPMK1KliMViYKjT/ACr/AGE/+Cd/ij4l/tWftBfF39o3wDrdiuj/ABTvNX+GXijw3rN9d+Frf4ia9471PxY/jTTLj4X+KdM8QEaNpsNlq3gXXm1K08F21zqMF/ri3xh0vTLr9KP+CkX7M/xD+O3jj9mOz8LeIPFvxB+JPw5+KVn430rwlf33h7wB8LYPDen6TqmoT6tdqt/pXiLxB4hm8XaJ4P07UL/w9qurajoHhe61yWFPDbXiXt59lxR4h5fX8UMkxFOphMXknDGQTw2CxtSjOVbAYfFcOU45k/qSw0cXXzCOIq1IU3i/bYbDxiq0KdCNPFYh/BcJ+HOLwnh1nGDr4fE4LPOJeIlj8zoU8XCMMS8LxN7XL1DEwrzwtLBywFGlUqU6HLWrznUpVnWc6eHX5w/8Fp/g7Y+Fvh/8J9RvNR1Wy8c3/wAHvh74t8Y+C7mysNQi0/xP8Kx4O+EOseIrrxLpVxJpl34l8V6N8StG0bXV06N7bUdJ+CXh+5tLptP0qCMfzWr0H+f55P5/kOlf0Z4E4+eZeHmW4iUacIQxONw2HjTlNxlhcLVWGw9ZQqJTpLEUqMazpyScZTd1dn8y/SFwMMB4iYmnGdWpOtl2CxFeVaEIuOIrutWq0YzglCrGgqsKaqRcrqPK5Scbi19a+Gv+Rd0D/sBaJ/6abKv5X/aI/wDJv+Af+yxxX/qkxZ/rH+wc/wCT8eNf/ZosJ/62eRm3/wDXP+P9M/h6UZ75+h/lX+SR/wBRYe/+eMH9Dgj0OCOcUUAFFABRQAUdelABR/n/AD+YoAKMj19vx9KACigAooA7Lw7/AMeUv/X0/wD6JgoriqfHL1Pksf8A73X/AMf/ALaj8/m6n6n+dJX/AFCR2XovyP8Azy47L0X5E1paXmo3lrp2nWlzf6hf3MFlY2NlBLdXt7e3UiQWtpaWsCST3NzczyRw28EKPLNM6RxozsoP9hf/AASo/wCCPth8BD4e/aK/ac0i21T45Qyw6v4B+H8txBqGifClWignsda1hYlMOpfEmCQyGIJPc6V4SIRrFrvXY49R03+f/pEcf/6ocGyyjA1vZ53xWsTluGcdZ4fLYRprNsUrO8JujXp4OjLScauKdam74eR/RP0c+Af9a+L1neOo8+TcKuhj6inH93ic2nOUsqw+sffjRqUqmNq2bivq9KlUXLiVf9Df+CgXxO/Z7i+A/wAS/hZ8UvjN4f8Ahz4h17SLaz02K0+JngvwB8RtHur5rZrLxJ4ch8V6lZ3V5Z20Nw41L+zLO/Op6Q+raPFmWW4EH5Z/ED4M+ErTVfDK/tD6H4Y8WePfFXhrwzPF4t0P4C+DfjFa/FG8FhceHdO8aWF/d+ISbLUPGvgfw54P17WdA0rVvFGlW/ie78VaR4Y1m+8L6HppT+YfDqpneS5Dg5QweLy7E5ljcyxWR5lOvmGQ08bl31PKZ5pSjnVPDSp1sPTnRwOIwtOjKcIT/tFVFzzi6H9e8WUMmzvOcXh62JwGaYXA4LAUM6yepSwOdyweOeKxcsoxEsnqzlOhWqwq4+lWq14QnVjDAujeNKfPxHg/wL4B8RfEv40+EYfA2rw6R8KfCHwO1GO1H7KnhmPxSbz4g33xVg1F08BjV11I6LHZ6D4Wjt9bj1h4FkW6tjo6W4kvlseDfCHwI0P4zfDHw34B0Lwx4b8W2fiuDxDNPr37O/gz4RWXh3UvCGq+Hrfwld6trema5fa1DHpXxS8Z/DjVdY0e1i0ue98NnWoX1OxivlS8/QsRmGdylmODoYx1sOspwuKxdLC8X47FVKjxHD2EzSMoYL6u4ZjR9tjHCjUqThRq88o8sVzRfy1DB5HVp4HMMVl2FhiaOb47AYLEYrhnCUpYdU8/rZRKEMXKXtMDOtSwlN1oU17RKFOTcoqm1/Qb8D/GPgHXNK8R+Dfh94o0HxVpvwo1rT/A11faNrNlrM7XcXhbQNWvZ9bmsZ5YY9ZuNW1PVVvk2QH7Tbzh4UkRwPbGkQSJEXUSSK7om4B2SMoruqE5ZYzLHvYKVUvGGxvXP8l5phsVh8fiKeNoVsPi5ypYnEUKtKVKpSniqNHGSi6clGUYx9rdJpe4k23ds/ecFVoVsNTnh6sK1FOVKnVhOM4TVCpKhdTV4yfNTadm/fTjo1p+Qf8AwWK8NeMvFXwJ0ewtLOzk+GenaH8dtb8cX09zbm8/4S2P4G+NtH+EekWFg0guZ1vPEmvX+s3V8kZtdPj8NRpPIt3eadFN/C8vQfp9O3XPav76+i9Ww8+BatKhKTnRxVR42LTUY42rjcwl7l9JReAjl7co3XPzRbvFn8GfSmoYmHFuV16/I6dbCVY4JxXvfVaWHy1TjVe/OsbPFtJqPuSi7O9xa+tfDX/Iu6B/2AtE/wDTTZV+F/tEf+Tf8A/9ljiv/VJiz/TP9g5/yfjxr/7NFhP/AFs8jP2o/ZE/Zw+DvxY/YV+NOreJvBWkXnxf1TUPjg/w08dSR3L67o958LPhl4B8bafo1hsuEgNvqE91rSTCSCVxFc3W0ENhfoDxn+x98DtB8BfsX6BongXwPpnxCHx8+EPwm+LPiTxX4e1TxTpnjPxZ4u+BOm/E+TQfG2jWGsaXcajoWq+IfEOlaPewWd7pr2dk7XUlxstJA3+auFyTLK+XUMRDDpYiWXYGi6lSeInRjmFXGU8XLFThTrwlNPBSVCVGDjDkV4RjVamv9YeMvG3xDyvxJ4q4ZpcSZz/ZuX+IPG2ZUcNgMZhsDjf9VMt4Rr5Fg8gwOMrYWvSwVGlxRTWaYXFVqNdUMwlTrVI1aVOVKfyRpn/BOZ/iH4r0+JfjB4K8Da/8YvGn7T2jfCLwDp3gDxI+lX/iX4CeO/F2l6/4ajvI9VnsPCXhNdF0S1vdCvL++vr+3hvINGi0/VX06e/uPOvDv7LGjfFbw/8AstaX4e8S6Hp9h4s+D3xz+JHirxL4V+FPifU/iVqMvw18TRJ4o8MT+FLPXbnUPir4u0PUp5vDPguPw8mhx3vhzTJ76GG4RFDeRLhupCVFPFUJPGSpUnCnQxE50J1Xk2JcaMeeDq1Vh83wjhG9SNRyrYdS9soVF+xYf6SGJxcZTXCuc4TDcIYnNaix+M4wyfDYbi2jl/D3jFlFCpn9WeAawGVYniPwv4irYzEYmVFZZXy/Ks+xEamDj9Wr15f2Yf2e7P8AZd+Kvxil+Nvi6TxX4O+NrfC7w8l98KfFWiWl9PF4T8S6/pHhbVvCN07axoviDxdJp9lNda7qOoSaX4NSKfSdQ0+a9aW7rv8AXv8Agl3440FfD9hP8TNOk8S2vxB+B/w/+K2lSeAfFmn6T4Guvj3qWm6X4W1Dwr4p1F7bR/ifFoV9qltY+LYNFbS20q+dbV5d5OJpcNRxdKnUwWLc74WlVqqrQlDlxFV46oqTceb2dJYPLcVV9rNyTrU4UItvERdPoxX0nM54YxeIw/GnAmFwrxfFua5RlKyvi/JMXThlOWYTgvButCpGMpZvi58RcccPU3TwUVKWU4/Mc3p0/quQ4ilW851T9mfwv4U8J/tW+EPC/jvwh8V/Hvwu1T4O+DdUef4a+J9H1Tw/4u1b4ut4G1vTvh74n1jVrW1tX/t4r4f1vUJ9Jv7XxFo2Tposl/0uuY+OX7GjfCfwR418ZeFvi34b+KJ+Dnj/AEv4UfHTStM8Oa74cfwJ8QNWgd7Wz0K+1V57Xxt4eS/hu9Cutetf7Kkh1m1eAaZLCzzQclXJn7KtKOJw8p4WjVcIQp14yxTo4jMFWalO8U4YbL6+IUnGleDoUfZOrKtUX0eQeOmNln2XYDN+D87wf+tnFGU0MZGvxFlGZ4bgvBZ1wl4VUuHY0aeGoUliKGdZ/wAcZbRr5fhq1argsbW4gzeeInh1Swa+iv2M/wBk34XXWo+B/Enxl8TeFfEviP4o/BP4y/EjwX8C9T8F6rrcb+D/AA7onijStG8eat4rLnQdI1ePxJoVxqGg6LPatLc6fZzXqagl3GljJ4N4o/YQ8W+EvgZefEfV/GLRePdO+D+h/HvUPh7H4C8XN4etfhbr9/ZWNrOPi+Yo/Bc3j6ztNQtNf1TwFEpvbTSJJHS/nmhdK61kMKmWUq9PEwdWnSxOLrtUqzfPPA5bi8PgEn9uNKrUlKuoKjGrN05SaUJv5rDfSHxeA8UM9yzM8jxqyjNs94P4b4awGL4kyrD0KeS4XinjnhniPjzCYeva9fHZhgsqjS4YpKWdZlgMLQxUGoU1B5P7Q8/gP4B/tm/Ek6X8JfAXivwL4P1G2isvhX4gs9QtvA9zFqXw60j93PaaDf6VfRra6lqUmtwC1vIN2pRRyTCSJpYn+u/2rvgZ8KvG/wC0fqH7Pvw38D/Cf9nPw98N/hAPjl4h+I+l6J4v1W91HRI/hzoHjDxHp2u2Frquq3FzZaNBeahd6NFoumHUJWt47WRLuWfzUmOGwtShmeEjhcJCtLPsHgsPj6s8d7XCUMS8ZKpHlpYiVCVKP1CmrywdesoVsTrUk6Hsc8dxFxflOM8FeOqvGHF+Z5dV8B804sz/AIFwmMyuhl/FuecOZdwRGGIxmKx2EnXWPzetx5Wq4qtUxlGg8TlGVTlKlS+ue2+VvgN+yH4L/aH8S+IPCXgP43azLq8nivW/DHw5vX+APxOvPCviWGytpL3Q9d8c+LNPFxo/wr0/xJaqiQWWuy3+r6fcs8N1ZODA830N40/Y/wDht4t+Dv7KuhaL4m8J/DT4++Kfg38Y9atPDMPgvWb4/Gvxd8OfEfiC91e21/xtpzW+l6BfWegeH5rDw7d6lFqUmqXDSW5trGKFJ7kwOS4evhqlZ4uLjisLioYacqFePs8XhszymhCKs+SccRTx0Iuq5NYfnxHNTk6NKde+NvHzinJeNcnyWfB9bCYrhbNKedcTZLlPHXD2ZQzbg/F+HviBmmc1cyeEdsBj+F6/C+OzvC5BjqTxmdRwGS4jCPDrM6c8P8s6P+yho3if9m/Wvj7o3xS1gnwbp3hDxB8QtB1H4OeO9D0fRvC/i3xRZ+FZ9Q8F/ETUzD4a+I+s+GbnULe61bSPD4jVoxIltfNKqrJ7X47/AGPvDWr/ABc+N2o+JviT4C+C/wALPh5rfwQ8EWGueFfht4muNJ17xr8WvAeg6j4V07Q/Alvrl7qOkQXMS3XiDxrql/r066dLNe6jDaTLcfY7TCGTQcMO54yjTo4mhh8ZLETw+I56FJ0c0niIxipctVQll1aEbJrESnhpRq0F7eEfezLx3zXDZ5nVOHBuMxOZ8M43izhLCcPZXxvkGNyviPOZ8ReDuCyWvjMZhsNWw+X46lguOsNWrrEVnV4Zf+suU47DYvGTpSpfC3xi+FPij4HfFHxx8I/Gjae/ijwFrs+hatNpM8tzpl1IsMN3a32nz3EFtcPZahYXVrfW32i2gnSG4RJ4Y5VdB5rXiYihUw1ethqqSq4erUo1Em2lOlNwmk2k2uaLs2k7bpPQ/o3h3PMFxPw/kXEuWqqsu4iybK89wCxEYwrrBZvgaGYYT20ITnCFX6viKftIxnOMZ3UZSSTfZeHf+PKX/r6f/wBEwUV5lT45ep5WP/3uv/j/APbUfn83U/U/zpp6H/P+fw59Oa/6hFpFPtH9D/zzIfZ+X6eT/J+jP6QP+CG37P3wAuvjN4J+L+veOPhx4r+I8nwg8T3/AIW+Guo+L9I1bxt4U+KGk/ErXtO1/wAS2fgc6XZahodtovwrTwTNomoahd6rPc6h4o8WavpU5t9Ptjo/9G/7UH7VGh/stfDr4jfEnxF4D8X+MNC+GvhLQvFeuSeF73wfbpBF4m1zUvDXh+zul8SeJtG1BG1HXLD7GJtJ0zXHhjnWZrZihiP+dXjNSznjXxfjkuLdTKeaWX5Hk0szw2Lo0ZYWpm0sBh8TSpUI4qrUoYzH4iq6eKpxdPEtqrejh2nS/wBIvBmOTcHeEdLNMJVoZpFYfGZ/nMssxOExE1jFltHGYrDVKlR4OnTxGDwdGlSqYavKNXCuHsKjlUi+b+DH9sn9rL4hftrfHjX/AI0ePY4tGXUIrPQfB/hC31Ca80XwL4R07cumaDZXc6QNPm5mu9X1nUjbWv8AaWtajqN+lraQTQWVt/Rp4D8IRa9o3wx/Z6/aU+Gvh74hN4PvPBf7NE9v4g8QeML290/wFYPa3+lx2t14Z8V6VbR+IYNG8XWGk6i8K3lpb6n4ctJNGWCwitIE/oDxKynDcJ8McC5Dk+MrYLG8N4HF1sszClQwzrv+y8Jl2GxeIviKOJoU6mMr4ulXxCcZc0qs3HWKlH8U8HM0r8ZcU+IvEObUKeJwvEGJwFHH5fVrVJUIUsyxGY1sLhX7N0KlWlhMNhJ4WhJ8j5IXfxST8Y8OxTRfEzRvC+u3n7Q/hD9jTXdBvfhN4i+KXgz42eNZ/izpuv8Aw5s7L4geLbK51iTV9Y8Tyfsl/D278Y+JvhzpOiMmpQQeD31T4knVL+487xDJ0n/Cv/hJrevfDD4V6d8J/Bnwq8Oa7ZePfhj4gufDi+I9bubTw1438Z+C/hHJaX+nfErxV4r0C7sdG8QeL9B+I2tyanp1xql9qHgqKKPUbKS/upW/Pq+LxuGrUKuXYzBUqdTBPMcwhleXYHLamK4lhhMzwuPwWLnQwkMW8vyWeHw0Mnngp4b2dPGY1UasptOl+s4LLcLXo4pZpgsTUnQrwyvLnmGYY3NIQ4beLy7FYPG0oYzFV6H9pZxKpVqZzPFwnWxNbBYR4ui404Op+gX/AASZ/Z4+GHwc8A+O/E3wp8XaP4kk8ZfEvxUvjG68P3+pnw5qOkLZaHqvg7SLbw/cajfW3hrWvh0mqXulraFri9uNM8SXd5falrelaj4O1evon/gol8Q/H/7P3wE8V/tP/CyCTUfGHwm8Nazo17o8slu2mXXhL4hXmi6Df65c28tncXEl34B8QJ4Y+IFvHYXelvqNj4b1TQ768jsdVkuLT8VzfFy4r8Xo08+pVMJLPM8weR5h7aCjVoUseqWUUas/ZKip1cFhq1CLq7V5Yb2tV1FVmpfouX4Wlwh4ZVP7CUMXDIckxma4ClTn+6xNbBQq5nUowc3JUqeMxUK1qV1GhGu6VP2caceXzL9uDxjoXxQ/YN0vxt4XvNQ1Xw74r8Mr4v0y+1e3+y6tqGg23wk8beMHvdWsmbzbC+utD06a/v7C5SG9spHlt761tLuKa2j/AIJ16D/P+R6f0r+ofov4epg+H+J8FWjGFfC546VWnBuVOE6UsVQnGEtpRU6MlGXWKTP5Y+lRXpYrM+DMTRcpU6+VY6tCU4uE5U6qyupBzhJKcJOM+aUZJSjKUk0pcwtfWvhr/kXdA/7AWif+mmyr8U/aI/8AJv8AgH/sscV/6pMWf6T/ALBz/k/HjX/2aLCf+tnkZ+gf7Pf7dHij9nrwt8O/CekfDzw94o03wJ8RPiN4+vV1fXNQsk8WQ/En4cH4can4a1KC10+cWdjY2aw6rbXcUlxLPeQJBLbRxHzR18n/AAUa8eXdzbXupfDvw9qV1Z/th6f+15aySeINTiW21PStCtfDelfDeFF0yQJ4esNGsbOzt9YB+3CK2QfYBzj/AC2w/EuJw2Fp4WOHw8o05qam1U5244bBYWKk1VScYwwakkkrTq1Gt0o/7y599Fjh/PuLM94uq8WZrhcbnscZCph6eV4SpQwscdn+fZ7iHSlPFxlUqVHnVLAylJQvRyvDVLc9apCB4f8A+Ci3i7QPGnwQ8aRfC7w3cXHwQ8e/tL+PNNsJPEuppB4huf2ldX1zV9Z02/nXSTJY2/hR9ckt9MubeOd9TS3SS5hs2keNPC/CP7SVlodl8CtN8R/DX/hJ7H4DaX8Rbbw4+j/Evxd8OddutZ+IHjm38cxeIrbxR4Stl1fw/f8Ahu4juNLtYrJryDUrK7na5W2l8sxxW4gq4hUo1qMHShOEpKnOrTqNQw2R4V8s41E6cuTI6EozhrGdWq7Ncqj6+WfR2wmTRxc8t4yxdLGYhY32WIxnDeV5phaMsdjfGvGTWIyrHYqpgcyw3/G7M0oVsDi4+wxeHyPCUqvKsyxLw3pHiH9t6fxzov7Q/h/4i/Bfwd4w0z47/EV/ivpcX/CU+LdAk+GPj6HwlqPg3SfEGlT6VOJ/Fb6TpN9HMtn4gljt9Q1aCTUdTF0LqS1i7HxZ/wAFENT8Z+J/BXxE1r4N6c/xP8MeKPhN4q1rxZb/ABb+KEWgeK7v4U6roeqW8f8AwrGbUJ/A+gXPiUaDZW2t6pDY6pOrr/aFjb219vmkr/WKcoNVMDhpylOFaU1UxkObFUMVi8RhazVPFQXLQhmGJoxpuLp1bwnXVWcHfxZ/Rfw8K2XvB+IWbUMHllCWBwmBxfCvDGawoZXjeHuDsjz7BUcTjqMsTh8XnWI4IybNf7Zw0qWYZU1isvyyVCliZYlfPcv7TniRdU/aT1nT/Dun6fqf7RnjPw/44urqLUbiY+BtV8N/FiT4uaeukpJZ41hE1do9O8y8NkyW0S3flPIxt69K+OP7bE/xe8C/EDwT4d+C/gr4Uj4w/EfSPir8Ytc8P+IvFHiC78deMdGtp1gns7HW5ksfCOkzarcT69d6RYR37S6tK8y30aPPHcc8c6qRo4qm8NQlUxFOcKdaTrc+HdaeP9tKmo1Y05OrQzHEUX7WFRJKjKKTjN1fra3gPgqvEfDWfQ4tzahQyPNMnzTH5TRy7APDcQLh7J/DrA5HhsXiK0q1fBRwGbeG+V56q+CUa1b6/j8rn+4dPEx3/hV+3i3w08NfD20vPgT4P8YePvhn8NPHfwf8M/E+88ZeLNF1WP4d+OLjXrs6Jd+HLCOfw/eXug3PiG+TSdZvEnZdOlubBrFLq6XVbbj/AB5+2RqXxO+Duk/Dnxv8PF1Pxnofw00f4Uab8UtN+KPxF0C3bwx4eljj0abV/hTpeoReAtd8RWuko2j3GuajGyX9tL5t3pck8NvJFpPPOfB/UpYHD+yeFVGTjPFRnPFRw+Ew9PHTf1nWrGGEg50tMNUc53w/vR5PDw30b6OF4vfGVPjzMK2OocU0c4y3C4/hXhvMsNlnDjzniTPMdwbSWNhVVShisVxHKOF4hlTjnuVQwGHlhK0+f2dLxP8AaF+Lw+P3xL1r4nTeD9P8Eap4l0vQrbxDp2k6xqOtWOoa5pGiWmiXniKB9Tjhm04axDY20z6ND5tppzxlIJ7ku8r+2+Nv20fEPjX4zfEL4zXXgHRrDUPiD8CNX+BN3oUGu31xZaZper/D+08ASeIra8fTYri71CCzs11JNOltobd7l2t2uhEqyHkeaVPaYiUKVOKxGZUMycfftCdBYtRpRvUb9m1jJ3c3OXuQfNdPm+0q+D2GxOTcGZPiOJMbWfBvAGdeH9DGPLcLTqZhgs4fBiWYV6NPEQpYfE4SlwZhKcaNP2lKu8ZWqTnB0oRqdP8As5/t56/+z34L8K+EF+GGleNP+EH+Kz/FnwxqR8e+MfBFumrXPhHSvBV5pvibQPDJ/svxpZPpOlJc6a+ubk0rWZDqL2eqQo1hLsj/AIKBS/8ACPeCYJfgH4Ku/iB8LvB/xP8ACfwt+Jt14y8WjUfBUnxYuvEMviTXf+EYtUt/D/iDULW11+S00GPUUgGkyQz3sk1/9uW2svQo8QypYfDYd4LD1I4bC1cOpTqYvmdR1ctrYetFRxMYQ9jPKsLzUlBUqyliPaKftIKl+c519FzC5rxRnvEuG8Qs4yh57mlXFV8Hg+G8hqThlOY5dxblWf5HXzGs/rWYPOsv42z7CRzjFxePyig8tpYFOnlsY18mz/bDn8a/CWD9nTVfDHhTwLp/i74S/DP9n7xF8WtY8WfEbVNF8P8AhL4deI9M1bw94pt/hxo0Go6Lo88KWTJ4t/sLQtU1jxExjvrW4sXgfTb3vPiN+2f4E0f4w/HnTbH4feHP2hfgv458U/BDxJoY1rV/GHw/I8YfBHwPofhnT/GGj3NnZw63Fp2p3VtqtpeaNq2nQHVdGFrFIbJZpC1f25TqRp16+Hw1WdKNHCPBSeMccTQeHzWOKr1KiqKFJupmUlQp0a1NU/Z0/wBw4wlOtxS+j7mNHO8yybLeIs6wGDzKjn3FuH49pZTw1F5DxJiOKvCXF5FkWFySFeM85q4fLPDitVzfNM1oWzZ4+viK+LjjMSqFD4W+NXxY8R/HT4r+PPi94th0+18RePvEFxruo2elRzxaZYh4obSy06wS5nurkWem6da2dhbtcXE87w2yPNLJKzsfMK+fxFepia9bE1be0xFWpWqcqajz1ZucrJuTS5pOybbtu29T+neG8jwnDHDuQcNYCdWpgeHckyrIsFOu4yrzweUYDD5fhpVpQjCDquhh4Oo4QjFzu4xirJdl4d/48pf+vp//AETBRXmVPjl6nmY//e6/+P8A9tR+fzdT9T/OkPPFf9Qi2XovyP8Azy46KL8l/Wh+lH/BMCP4ceBfjT4q/ao+KvjPxV4a8Hfsg+ELT4v3Wh+BYoJfGHjzUdV8TaN8OtD8LWLXl5Y2Y0e+1vxhp1l4jt7m6hj1LT70aRJNa22oXV9afR/xF/4Kg6B+118ZP2hPDXx6i8TfCz9nP9or4U+EPgx4cOhXd14qk+DFx8P/ABynxA+H3xS8U+GrCG1/4Tm4tfF8+rXvjHTtCjstYHhvWLjQ9EuNU/s23GofgvEvBmY8VceZ1xNQoYJ1eDMoybK+HcPWS9pmfEGEx+WcZKriK817PDYWEJ4fLKEnzunWxmJxMmo0VTq/0ZwtxnlXCPh/w9w5i8RinS43zbPMz4kr0nKVPK8gxeCzHgtqlh6NSNXE1nUw8szqwduengoUIL2lSlOh8Q+Kf+CfP7S2n+KNM8O/DzwpY/Hix8TaVceJ/A/iP4I6zp3juz8X+B1ay+zeNbLStOn/AOEh0rRp4dU0v7Wdd0nT5tEvdRtNM1mOy1CeCCb90rX9pTSdB1P4XfEP9sHwv8Qf2P8A4s2GpeA9Z8b6T8YfhP8AFW6+Hvxj17wBonhbw3feNfAXjrwZ4O8Tz+HbrXdK0fw/b+IfCOt+F7q00XUEha11e7uL28ub3xPEHNMp4+oZAuHMdh6vFNDDZph3wfjcVhMrz+vg80hho42GHoZlVwtGeOy3GZRRqSoSrU6eJw9DGqjiYSUG/q/DHJ848MMZxJLibBVI8J4upldePGGApVs0yiliMtlWqYGrXeWxxWJo4DH4LNq6eKnh7YWvUwrr03SnOUOn0D4kfDiw8EeAtbtPiX8MtU+H9pq3x41qT4sr4mutK+FcXhzW/BXw28GvfX2va5oOn6xYavo/ivXtN8N6x4Ak8MyfEGLUNS0aaz8KX2keIdB1TUfNvBHxv+Fh8Vad4z+Bs3iz9tn4nfDyx17VfDXwf+A/wp+MU+k3Hia98ceFPF2h+KviH8SvF3grwlb+HfC/hPxJoPh27lt9H0TXtQ1q7lt0lns7WcxzfmVDI84r0sbUq4KrlGXwnisHnWf5xXy7L8t4ZlVx2aUMWsdVxGNcsXmOAjjFN4DLoYuFTExw9GeJp08RGsv2TG8UZJhPqVChjKecZnjKGHx+SZDlFPFZjmPEVKGFwGIwksJHDYeUMJl+OlhZQjmmYTwtCnSdeq4uVCcF5B+w/wDtF/Gv9kDx58YPCv8AwUE+FfjLwx8HPi1D8UfjjaaZ8U/hjDpHg5/jZqmmHxPdagnibX/D900M/jO08Jt4E0bw/Ff30X/CR61plvFpa3dxdNP/AEXftCftGfsbaBp1/wDAb9oT4peA/C2n/Fn4T61qb+GPFk9zpFj4i+GOs6Xr2naheWWoS2SaYBJZaVq8dpaxXiass9tB9itPtE1ks3yXirwpjcbxtleceHLlnuV5vh3nGSvJsV9foRx2SUMD/a9Wg/bSgsXTdHDYnEYajGNSEJUX7JOajHp8L+JKWE4KxmUeIfs8kzbJ8S8rzmjnNGOBX1LOcZi1k8XKcVGrgMRCdXBYTEyqVKNapRr0o1ZunKUvxy8c+MdI+GH/AARx8VaBoniDQPEtj8EfGvxz+AMOteH/ABPaeL4NZ/t22+J/wXttfm1jT91ms3iW5+KOn+N9L061luE0bwxrmh2puHa2aWv5Mx0H+GP8/kPoOlf014HUKnsePMdUpVaM8XxznEZ061B4apGftf7RaeHn+8or/hV9ylO0oRspLmUkv5h+kBWpLE+H+ApVI1VhOCMtqKUK8MVB05pYGEliaf7rESbyyfPVptwm0pqylG619a+Gv+Rd0D/sBaJ/6abKv5q/aI/8m/4B/wCyxxX/AKpMWf6ofsHP+T8eNf8A2aLCf+tnkZ7/AKL+z/8AF7xH4Z+G/jDQ/B8+p+Hvi38Qz8KPh/fWuqaI7658QBNHAPDj2R1IX+lXBaVJRdaxbWFg1oTepdNao8q+p/s2/CH4WeJtc/aAuPj7/wAJ8nhj4DfCbxJ491bT/hfrPhO08Q3mseHPHXhPwjcafbap4istX8OXVlt129lSdZYba4aG3uYNR+znbN/lNhMB/tOHWPp16eEqwnXk8POiq9SjTwqxjVCU414KcqM6Ws6UowdRRmozTiv+i7i3xJpy4c4nXAGYZVjOMMoxlDKMPQzvL84llODzWrxhT4MqSzSlh44PF18HQzWGOp3wNfnxUMLOvgpYii6cqna/Ej9iHxo/jDUpPgPa6h4v+F//AArb4VfFxNb8fa14K8D634L8NfGLwzrHibwroHjqTWNb0nSm8QwpoGrac02jh7bUbn+yBb20M+tafbzecxfsVftMS+GvDvis/DYW+leJ7jwJDYRXni/wNZ6zYwfFDVIdF+Heq+JfD1z4lj1zwloXjPV54tO0LW/Eun6XptzdFkkuIQjNW9bIcd7evHC0nWoReJqUJzrYWnVqYbD1qlKVSVKdeMozj7Kq5wt/zD4uVN1KeExE6XzGR/SO8PnlGUR4szqllPFFSjw9gs9y7Lso4jzHLsJxBnmW4fHQwuAx9DK8RSxGBqvFYH2GI9tOND+2sgwmOqUsdm2Eo1t74X/sffFvVviHo+meNfhjrV74Y0744XvwN8XaPpXjz4a+EPE2rfEHQ9Jvte13wD4Q1Txp4gs9Eu/Eo0nT55dPvEN7o93O9paWdzdXd/ZW9x5jq/7PPxJt/CGo/EpNL8P+H/BMmu67pWgJ4u+Ivw70bxNqcOi+LF8G391p3hm/8SWfiDXtM8P+ILmDR/EGv6Ppk+i2VzHd3jXSafaXN3Bz1MpxlKkqlSh7Jxji6lR1cXg6cfY4ZYVS5ac6sZqop4hRpxblLGTqU6WDp1KkZqXuYPxr4KxnEX1TCcQSzLLcVDhbLMvwOV8K8W43NKufcQYjiCtCtDEYfKKuCxOVvLcoqVcTVoSlHInleZYnOq2EoVMO39TeEP2S9Ku/jl8TvDmv/Br4rzeCPAFt4D0iHw4fjR8C/Bd43j3xx4b0HU9B8N6n8U9bu7/wTrV14zkur7U/CXh7wONW8Q3unXenW8nkTwXDv4H4E/Y0/aS+J0Ulx4L+Gr3UI8WeL/AkUWs+LfA/hi7m8c+CI4p/EHgm2s/E3iLR7y98U2cMrPHpVrayT3SWt/NAGhsL2SDqeR4yrKjRw2DqqtUxGLpKVXH4GpCcaNelhqdNxpqn7GcK/tKTxFWr7HHTlbCU4KhWdT5TKfH3hfLKOZ5rxnxzkVbJsDwzwDi6csj4O43p1Y5lnnDGI4jzfHTrYjL8RPG4fHYWeFxmGyrB4RYzhjAQoQ4kqUcfmEaNHsv2PP2adF+OXxL+JXhjx9Z/EC7X4UfDnxb44v8A4a/DOPQx8VPHWseGdX0nQpPBfhZfEQk0u21OO91Vpb55oLqZY7N7S3hE9ws0POX/AMAB8XPiH41sv2YfBHxI0zwL4Js9EPi5/wBoHxB8PfBOo+ANa1G4bSX0fxh4qvb/AMNeD9Pn1DxAn9k+HLG6uLfWtUui0C2LPHIYphlUauXYKrSp4uePxuJxShKUsPRy2nhcHT5sTCrUrez9niaCtisRXq4mnhsPg5qdaEIqVY9PG+LeJyvxB4zp5rm3CeB8OeCuGOGsVjcJDDZ9mHiDmeb8X1sKsgzPJsBl1LEUMwyPMsTiHw3gsDhMFiM1xXEEI0aLar06SxNA/ZC/aG8Q3/jrTo/AcGgS/DbxIPBfjG88b+L/AAT4C0XTfGkieZa+ELXxD4x8Q6LoWteJL6ArdWOl6LqGoS3VlJBfRE2d3Zz3Fu5/Zz8U6roHwEtPDXhmOx8T/E/w58RvEt34k8R/FT4TWvw/1/TPCuuQKl9o2rv4pgtfB/8AwimkTjTfF+leNL+x1iTXhImn2RS3nhi5/wCysXGK9tSjTlUUvZRqYzA0JQcMZQwcqmJp1qyqUaHtqvs1VrLD0pSvVVWVGlWa9rEeNnA2IxuHp5Rn9TGYTLsQsTnlbC8LcW5ph8wy6vwFxDxjhcBw/mOXZPXy/G579VwGDzargKNfEYmOBw+Ky14eGbYzB4eXXaZ+yN408F6l8XtF+OfhvVPCuoeGf2XPiB8evh9LpPiDw3rGk+JX8Oa54f0jQdXtda0K51/StZ8LXxv9TfZZ3ltc3kIsb2KaG1nge5+Mv8/5/wA+3SssbgamBVKFeMqeIk6yrU5ThOMJU58seSVNyhKMo2kpKc4yvzRlytH0fAfHeXce1M4zHIsZRzDhynHJ6uS4ynhcXhK1aljMC6mLWJo46FHE0q1DGU62HlRrYbD1aE6c6dSDlG4UVwn6GFFAHZeHf+PKX/r6f/0TBRXFU+OXqfJY/wD3uv8A4/8A21H5/N1P1P8AOvWfgX8Jz8bfiZo/w/l8beE/hvpl1Y69rfiHx745u57Pwt4R8N+GNEv9e1vXNWe0invJILezsGiggtYHee9uLaORoLdprqD/AKc81x/9lZXjsx+rYjGPA4HE4uODwkVLE4uWHoyqQwuHjK0XXxE1GjSUnGPtJx5pJXZ/58OQZZ/bWb5XlP1ijhFmOOweDnjMS+XD4SniK9OnUxdd80bUcNTlOtVtJScISUU5WR9o/s0/Bu30n9pT9pr9lSy11PjRb+MPgZ8bfhZpHin4LaJqnxG0LxL4g0CDRviB4F8RaVa6Lb3M8+k2fjfwR4cmlvjIlla6jCtqdVjiePUa+8/HX7DHwL8ea1dfCrxT8KPEX7Gmg/B74s+OdCtPjJfeG9Tab45/Db4ffCfQ5/E2qaMviHXtT1nxj4x8Ya5oJ+IlpqMuieEvhd8I/Bt5fWNzqmv+LPEd7az/AIHxVxtmmUcQ4bG5biaeEr1+H8JxW+EcXiqNHMs3xWLwGHy+OX43BxhUxFKWBoUalWsnLDqm8NUxqqVP7PeHr/0vwbwDk+dcNV8rzHCPMKVHiLMuEv8AW3B4V1cHlGEy7H18zlmWAx1StSpSp5lWnRw+Hq06eKjet9SqQSx85U/0f+HGv+JP2dv2WvBvwY/Y18C/DPwN8R9F0q61zx74r8Q21lrXjmTw5axaPB4v+KFp4b1zV9C8DeNtZ8Y+Kr/wj4b8Oy3fx0ufCbyXNrHpM13oOgW1jp2Lq6fEjXP2Cv2ibvx58Lv2qfjj8WfFevJd6p8Kf2lNHX4l6Rr0mi6pJ4h06f4X6F8IfDfiHwZ4e8E6h4k1aeyXS/Ber6N4u0EaJNHYeLNEtvCPh/WLX8DVPLp4z+2cZm2Bo8UZvx5lmYZjnecVp4XMcuy6WcwxlCpwvXqzoZbg6WCcIV8dia+Ko46pg8RTw8MswOGw1T6x/QlWjjMLg3k+DynFYjhjK+DMfgMvybLqNDFZbjcVTypUK2H4joKdXMsbUx0ZvD4DBYPA18JGtRxOIxWZY7E42hSwny437Jv7N3jnwPP+1CPj/wDHT4K+JZ/iZ4Y+Nr/sl/8ACsbLUj4LufhzaaXo+ifCTTvghN4btm8WeONBsbHVvCPgn4k6npt1Y32g6vpOqeIPD+qwpr1/rX0Z8HLnWJv2AvHGoaD8A/2mP2W/E1x8Z/EnjjwL8Nf2ZNJ1z4UeOli13xNc+GNCttc8Q+O/CGn+GNZ0KLwnc6RqXiLVPH+veIbbW5bN18Lz6EdJ8N+E9A+hzvOszzjLsHhs7/1VyynknF+RZFDD43MFisn4zwOGVTD1uI80y94z63mOFwVClGpTx2CeJqY2GZezownjcHhK+G+ayPIcJlGZ4zFZXDivMqnEHC+d8RLFLLaWHznhLHYqrhK9Hh3KMwrYOjl2V4nHTxEksozBUKOFrZXKpipQwVfEUanpvgn4v+O/iL8KfFGi/tZ+CvBHiDxRr2l6h4a+HvxSS88P+FPjPNo+raHptxqWj6loHwhi8d+FdG8SeAIviDpetm/0H4leH9K8W6Fr1rq8PhzQmt7/AO0fnF4j/Zv+Hek+PvGnxjvfA/jj/gox4r+MDfC/X7zwPe6BdnU/hpaa98bZtM+Mek6tZaJ4wt73RbO/1zTtQ0H4afEz4d6p490HwhqWgeIvB3jjTJfDniSTxBqefCuIfDOOzjBZDmKyzJszxGFwWW8UZhOpCXCOFnisFiMfDBVsbQq4vFZdWng8XluDq5hhcFKrXjh8Ri8HRwtXHYmj2cV5Y+IssybGZ9ls82zLKYYnH43hTBqhUo8U4mGDxWHwKx2GpYmGFpY7DfWsLmeKoYLFY2NFQxGGweIxtWGCp1vjb4/+IvHPwb+DXjv/AIJ4+Lbm7+F2gR/tW2j/AAu0Lxn4F8ReB7PVfgxL40+JVzdfHTxh4s1vT3m8SaVq+sj4d6ZoN7aTm20rwv4J1OaCwu57i6vpfhj9p39nqL9nDx5pvhTTviz8OPjX4e1zRbnWtB+IPwu1KbUvDGp/2Z4k1/wfr2nB50V0vtF8R+GtUsbhoJbzT7mEWl9Y39zFcER/0twTmGHwdfDUcJgMVXwfHGMzfiunneGrUMblixlfB5VWxGHrYujKUI1cZVhmFXBUY29hhcEsO03FSX8o8f5ZVzLC4nF18wweHxfh9gMk4RnkNXD4nA5l9Qo47MqNDErD4ucqtSODp18soYnFJTpYzE4upiYypJxpS+c6+tfDX/Iu6B/2AtE/9NNlX8l/tEf+Tf8AAP8A2WOK/wDVJiz/AFs/YOf8n48a/wDs0WE/9bPIz9uv2DfGms/Cn9mv4hePvi14M1+1+E3w417XPj/8APiJfWunt4Rm+NNj4H8afBeTwjDdz3q351rXPEWv+Gf7A07T7G5lbVNFv9SuVtrG2N63wZ+yz+0VL+ztc/HDxHZ3mrWnjXxz8FdV8C+BdYstJ0fXYtP8YX3jLwZ4gi1HXbTXhNYnTTYaDqaTSvYao4u5rZfsLB2lh/y8rY+eCw3D0p0q1PFYbC4+vGVSjBOcMXBUcvrU41fcqYdU6NJp1E+ZRqSUJ05U3P8A3NyrgLC8c5/9JTB5XjMuzbhviLingfhpqnisdh6GGxvDePln3iLkeIxFGnTr4XM6eaZ1mcassDNx58RgpRxdOsqrw8dz+0TfeMPg5+0l4b+KOteJ/GHxO+N3jX4J+JbTxHfx2t3ZNa/DebxiNVt9SuDPb/2XHBZ65pVp4c03TNLfTra3tWtYI9Pt7WCNvrL9oH9vHw38WvhJqdp4P1/xH4C8a+KPh58MvAnifwHa/BH4VtHd6p8PdS8PXp8QS/H6HUo/H8vhhX0KDVPDPhwaVPqWmazDbxSSWmk74V56OdWweMw9SdaNfE4SrCpiJYPBY6dWvVrZlKqubF1FPDLFUczqQq5hh2sdQkqipQnCpJv1uIfAXEYrivhPMMsyzJ63CvDnHuFx+H4djxJn3DmHwXC1PhjwlyzD4nkynD1IZzXyPMPDRvDcL5k5ZVmka+X4nGYunPDNUdn4i/t9/CvxR8V/2WfFmhaD40sPC/w9+M6/tDfHGK50jSItT1r4mavY+ENO18+FbK31ua11S1sLXw9qTaZf6hdaRPenWningthEzH5/vv2hfhfqP7LfxZ+FHiS61vx14k8V+Jde1v4T+F9a+GHhizh+DWt638R/+En1LxToPxdXxLc+JDo/iDwzCLXV/BMGhx28viXVtYd5P7NlkvLvbE51hcRXxVTkrxp1sPjaapVMNhq8Z1MRgMBQoc6liF7FUsZhfrvtqSqVKNSnSdKnOUpqn89wr4B8b8MZXwNh41slxObZFn3BeYYzNsLxDmmXTyqnkXH3GueZrjMHCnlUpZ08VwZxBh+Fp5RjPqmEzGnmWa08VWpUcNRniPbvEf7YnwD+KsXjDwT44f4j+C/BT/Gv4GfHvwp4o8OeDtG8QeI9S1b4YfBvwn8LvFHgbW9Hk8V6MmnR6y2h3V54V8SRarqFvpsrC41HToxKYWq2/wC3d8OdW8dfCPx74i8PeKtNuvCn7cnxM/aY8T6XpGl6Zcw23gXxnY+H7PStJ0iZtZtRqXii3i0mVNWhnTT7G4mY3Ed/KLiSOLWWf4OT9p7LFe1qVaDqx9nQjCFLB5l9bw/s5fWW51KuHqVVXUlThCvCmoSqwnKpT8XCfRy4/o4GjllTE8OewyrIMZgctxSzbGyqY7G8T+G9PhvP6WMoPKUsPQyjiHLcCsrxEatd43JcdXqTo4SvhHhcR8leFPGnwh1r46fFLxz438bfGn4X6R4q8S+NvEnw/wDG/wAJbHTrjxb4W1TX/Gcmt6fc6/pjeIdDvJdPfQJ77Tr218Pa9DfwaldW08VzNa2sqT/anxb/AG3PgJ+0FafHf4W+NbX4peDPAHxZ8NfAPSbX4s23hvwx4l+IGo698Db3Urm+8S+OfB1nr+k6ff3fj63vhbQy2XiKSTQ57eynuraaIywWXPgM1wOFw9enN45yxmIryqwjSoSo4aNXAZlgYYqiniabrYiMcfDmw01h6bp0qkFjf9oj9W+z468J/EjP+KuFOIcpwHAUf+Ic5RwxjOHcVVx+ZYPOuKs0ybijgHiXE8I8Q4mnldalgsjp4ngvH1cuzVSxiw+KzvBVY5ZGWFzGdXlfip+1p+z9+0h4Q+JHw1+I4+KHww8MTfH+y+Nvw88SeGfCnh/x1rOraTpvwt0X4UHwl4u8PXHijw1ZWHiLUdK0SHXLbX4NX1LTLPUbm5srtWgijuLvzD4R/tDfBXw7ov7P3h3xno9/cP8ACn4f/tP+HU17Xvht4W+JWi+FPHPxb8Z23iP4beOovAOu6vFpHjiDw5YQXUGtaPqP2RLa/wBQS4083sljDdJpVznAVsZDGVKWIl7SnGjXw9TD4WvRpxo5hQxFGpFVsR7LGRnQg3XwtfD0aUq0fZTlWoVqlSlwZT4NeJuScDPgXDV8hnQyPMa+c5JneXcS5tkubZtWzjwr4p4UzvLpV6GVuvw5UoZ9m2Fw2VZ3hcTicTDK8TPMlSwmLy2nhcR9AfFP9t74E/GDxP4es9Wk8f8Ahzwyf2K/Gf7L3iPxZYfDXwlY3dr4n1u78MalY+LdE+G3hzxXZ6FZ+HLifw7LD/wjWm6zYQ6Gt7a2dgTp1qZYvyCOM8EkdiQAT7kDIBPoCcdMnrXDneZUM0rxxFKNWEueqpwnRoUYcjjR9nOEaFWUYynJVVKjGEKdGMKXs51OeUaP6J4C+HHE/hjkeJ4fz+GV1KH1TAVsLjMvznHZtUljZ5lxDUx2BrzzHA4TF1oYLCVspq0s0rznWzKvjMZGrQoTwftMSUV4h+9hRQB2Xh3/AI8pf+vp/wD0TBRXFU+OXqfJY/8A3uv/AI//AG1H5/N1P1P86+wP2Zf2zvHf7NWkeJfA1r4O+HXxH+EfxE1B/wDha3w78b+FdKu5fHPh2+0W58Oav4cg8apZyeK/CsV3pV28lleaFexNp2sQ22oi2uYjfWV//wBMnE3DuG4pyXEZRisRi8Gqro18Pi8DWnQxOExuFqQxGCxUJQlH2n1bFU6VZ0KjdGvyezrRlTk0f+fnwbxTi+D87w2dYTDYTGunSq4bEYPHUo1cNisFi4exxmGmpJuHt8PKpRVWC56XO5Ru1Z/Ynhj45/8ABPP4Q+BNQ+PHwL+Gn7RHhf4r63qnij4Ta18E3/afg8PQ2XgvxT4RivJ9ZHiHSfh5J408R+ANWuRqPhzUFsNX03XtMvtKsJ7vUNHvNX0G+P2J/wAE/f28rv8Aak8cePvh3+0xf/BvRtT8I/CO/wBV/Zou9cg0T4d+Gvh/P8NrbTvFlv4STx8fA3iT4gWfg/To/BOm+J/FGp+Kfihc2t1o+gahba7oHjsX5tLT+feL+BuKcfw7xFxVxPiqOYcV5BCNPJnw7k9XLp4jL8FiKTxuPr0o5lCWcVcdlM8RGlgqlWOX4aM8Xh3gMbWqckP6W4P8Q+Est4l4W4S4Swcsu4Vz5zxWbf2/m7x6w+Y4yhVeBy+jKpRxMMpjhczp0ZV8TGLx2Kq/UqssZhKFByre7Xv7O+sa58Btej/Zy+P2q6N+0B8KtJ1bxP4r+EXwy17w58cfH+oXdr4Z+E3gf9nD4f8AjDw/4QvfE3hHVvB3iPwh8Nrnxv4o1m81efRfDfxO8bP4ok1DWv7HvBL5d8Nvhv8A8FANR8Y/Dz4RfGz4pfAL9nn4y+LpPil4I8Pad49uPDsHiXxZ4H0prHxhoVp8PvBvwN17QbiayvvFOtfF3WZfEl1c6HpOTY2Gk3ct9/ZejzfDYTP+E8RgMxp8ScPZjU4kyvE5ljsNgMppYvB5rn3DuHy2rmeFzXFZZiqEsNhMLDK62ArQxub4SFeWDeKo0aM5YXkqfpOKy3i/C5pgavDXEOWLhfMKGCwOIxWeVsJmGW5JxDVzbD5XXy3C47DYzD5hjcXPHQxuC/s/BY32VPMo4dVK0JYiTp9tN45+Pvhz/gn1+0B+1gnjf4U6h468CfFbxp8MpdF8Pz6rB8OPEHhzw38W/BHh3UdW0q8l+L1jfxXN5f8Aht9X8KaCUWbUtFuv7I0bwncaR491k+I6Hxc039vfT/GK6H4C+N37NHxo+OWt/Bfwb8QfB3w2+F1zY2Hi2zjvvGPwgn06/Xwh8W/E2raH4/8AD1z4a8BSeKP7ctJ9R1FNT8Kab4l1jTtPg1i3uNbnA4fgSjjsVUzXJeJfq2H4o4hyjEY+nFToZTkuR4fhmtjczzj6rlWHxynKnmPsZ1sDVm8CqX1uPLRcqoY7M+PMRg6EMqzThWOMq8JcPZtRwVaFSlXzfPs9nxDSwmXZTLFZ5PCYalCeV0p0aePw844+VWeHnWjKE1Do/BP7LPxW0n4faL8UP2w/j/d+EfjVrmu61beEPg58QPEHg/4S+Fr/AOPnwZ8SPq/wf8B/DqxtNN034bah8OvGXhfw/JH4l8UaTfyaLr+max4Y8O2NjpjaCLiTy79sL9rSP9hb4X/suWH7M+sfDGP4+634Zu7rxp4lsr3wH8aNQ0vwB4ZOkfD7T/DWq+JL3wP4ktBN4kT4e2+i+PbT4ffE7StOg8deE/Ft80Hiy/1UePbrXKqeD8QOKMt4fy7J8VgeCcVm+ZYT6zl1DMaeBzThzLsuzKhSrrPFjKE5VcdVwOGp4qnPETxlTFVcXLFYWtRjVw0ubO8djuAeC8fn+ZZvSzHjPBZPlladHMsVgp1sHxFmWPy7EVaMsmo4aph4wwTx9SWDdPDRw8MDQwlKji6EnHGn5/zftA/sNfHPwR/wsD9pb4YfHhPHfw3svDng/wAPfD/wJ+0bFeeFNch8Saz4p8Q6vD8P/Dnj/wAL+IfGHgrwTo182q6rqFpP4u1220afWtI0K01WRbjTLeH59/aA/be1f4rfDi1/Z7+Gvwv8BfBj9mXw1caNL4G+G+n6Tp/irxlpU2kXmp6pc6zrvxZ1jTYvF+u+IPEmsavfan4iuEl0/T7hmhtYNPiUX0+pfuuRcDcQzzXDUeI85w9fhzhLNVjuGMBlOVzyOrjcwUaOKpZtm9WhXlQxSorG4/CVcLhaOHwWKxVXF4iVClGNKkv584k8ReG4ZRiMVw1ktalxTxrkssDxbj80zKpnNPA5dONTAVsoyyOInKrQq1pYHBY2liK8qmJw+Chgqcp1atSc6Pw1X1r4a/5F3QP+wFon/ppsq/lj9oj/AMm/4B/7LHFf+qTFn+rf7Bz/AJPx41/9miwn/rZ5GdQdR1A2A0o398dMFx9rGnfa5/sAu9pT7SLPf9nE+wlDMI/MKEqX2kiqVf5IpWvZJXd9FbV7t92f9Q9KjRo+09jSpUva1Z1qnsqcKftK1S3tKtTlS56s2k51JXlKyu2FFM0CigAooAKKACigAooAKKACigDsvDv/AB5S/wDX0/8A6JgoriqfHL1Pksf/AL3X/wAf/tqPz+bqfqf50lf9Qkdl6L8j/wA8uOy9F+QmB6CjA9B+Xtj+XH0p2XZFXfd/ezR0nWNX8P3sep6BqupaHqUP+p1HR7+60y+hwyuDFd2MsNxGQ6I42SD5lVuqgiTW9d1zxLq+oeIfEms6v4h8Qatdy3+q67rmpXmrazqd9M7STXuoapfz3F9e3c0jM8lzczyTSMxZnJOa5/qmF+s/W/q2H+tui8P9a9jT+sewcoTdD23L7T2LnCE3S5+RyhGXK3FNdbzDHPBrL3jcU8BHE/W44J4iq8IsUoOmsSsM5+xVf2c5Q9r7PnUJSipWlJPZtviF47s/AepfC618W+IYPhxrHiXTfGWqeB49Uul8M33irSbG70rT9fuNJEhs21W2069ntBeCNZJYBbCfzfsVkbbmLa6urO4tr2zubi0vLOWC4tbu1mlt7q1nt2V4Jra4iZJoJoWVWhkidJI2VSjKQCM6GBwmHeK9jhqNL67XnicWoU4JYnEVKVGhUrVklyzqTpUaVKcmr1IwjzXtd3XzLHYj6j7bGYiq8toU8LgXKrLmwuHpVqmIp0aM3LnpwpV69apTSaUJVJciikorY8S+KvFHjPVLjXPGPiXxB4t1u8kkmu9Z8TazqOvardzTP5k0tzqOq3N3eTySyHfI8szs7fMxJ5rA2qOgA7dB054/U/ma2w+HoYWjSw2GoUcPh6EI06NChShRo0oRSUYU6VOMYU4RSSjGMUkkrLQwxWMxeNxFbF4zE4jF4rETdSviMTWqV69apLedWrVlOpUm7K8pybdlroGB6D8qXA9K2slskjBtvdt+rfa35aegV9a+Gv8AkXdA/wCwFon/AKabKv8APT9oj/yb/gH/ALLHFf8AqkxZ/up+wc/5Px41/wDZosJ/62eRm3RX+SR/1FhRQAUUAFFABRQAUUAFFABRQAUUAdl4d/48pf8Ar6f/ANEwUVxVPjl6nyWP/wB7r/4//bUfn83U/U/zpK/6hI7L0X5H/nlx2XovyCimMKKACigAooAKKACvrXw1/wAi7oH/AGAtE/8ATTZV/np+0R/5N/wD/wBljiv/AFSYs/3V/YOf8n48a/8As0WE/wDWzyM26K/ySP8AqLCigAooAKKACigAooAKKACigAooA7Lw7/x5S/8AX0//AKJgoriqfHL1Pksf/vdf/H/7aj8/m6n6n+dJX/UJHZei/I/88uOy9F+QUUxhRQAUUAFFABRQAV9a+Gv+Rd0D/sBaJ/6abKv89P2iP/Jv+Af+yxxX/qkxZ/ur+wc/5Px41/8AZosJ/wCtnkZt0V/kkf8AUWFFABRQAUUAFFABRQAUUAFFABRQB2Xh3/jyl/6+n/8ARMFFcVT45ep8lj/97r/4/wD21H5/N1P1P86Sv+oSOy9F+R/55cdl6L8gopjCigAooAKKACigAr618Nf8i7oH/YC0T/002Vf56ftEf+Tf8A/9ljiv/VJiz/dX9g5/yfjxr/7NFhP/AFs8jNuiv8kj/qLCigAooAKKACigAooAKKACigAooA7Lw7/x5S/9fT/+iYKK4qnxy9T5LH/73X/x/wDtqPz+bqfqf50lf9Qkdl6L8j/zy47L0X5BRTGFFABRQAUUAFFABX1r4a/5F3QP+wFon/ppsq/z0/aI/wDJv+Af+yxxX/qkxZ/ur+wc/wCT8eNf/ZosJ/62eRm3RX+SR/1FhRQAUUAFFABRQAUUAFFABRQAUUAdl4d/48pf+vp//RMFFcVT45ep8lj/APe6/wDj/wDbUfn83U/U/wA6Sv8AqEjsvRfkf+eXHZei/IKKYwooAKKACigAooAK+tfDX/Iu6B/2AtE/9NNlX+en7RH/AJN/wD/2WOK/9UmLP91f2Dn/ACfjxr/7NFhP/WzyM26K/wAkj/qLCigAooAKKACigAooAKKACigAooA7Lw7/AMeUv/X0/wD6JgoriqfHL1Pksf8A73X/AMf/ALaj8/m6n6n+dJX/AFCR2XovyP8Azy47L0X5BRTGFFABRQAUUAFFABX1r4a/5F3QP+wFon/ppsq/z0/aI/8AJv8AgH/sscV/6pMWf7q/sHP+T8eNf/ZosJ/62eRm3RX+SR/1FhRQAUUAFFABRQAUUAFFABRQAUUAdl4d/wCPKX/r6f8A9EwUVxVPjl6nyWP/AN7r/wCP/wBtR+fzdT9T/Okr/qEjsvRfkf8Anlx2XovyCimMKKACigAooAKKACvrXw1/yLugf9gLRP8A002Vf56ftEf+Tf8AAP8A2WOK/wDVJiz/AHV/YOf8n48a/wDs0WE/9bPIzbor/JI/6iwooAKKACigAooAKKACigAooAKKAOy8O/8AHlL/ANfT/wDomCiuKp8cvU+Sx/8Avdf/AB/+2o/P5up+p/nSV/1CR2XovyP/ADy47L0X5BRTGFFABRQAUUAFFABX1r4a/wCRd0D/ALAWif8Appsq/wA9P2iP/Jv+Af8AsscV/wCqTFn+6v7Bz/k/HjX/ANmiwn/rZ5GbdFf5JH/UWFFABRQAUUAFFABRQAUUAFFABRQB2Xh3/jyl/wCvp/8A0TBRXFU+OXqfJY//AHuv/j/9tR+fzdT9T/Okr/qEjsvRfkf+eXHZei/IKKYwooAKKACigAooAK+tfDX/ACLugf8AYC0T/wBNNlX+en7RH/k3/AP/AGWOK/8AVJiz/dX9g5/yfjxr/wCzRYT/ANbPIzbor/JI/wCosKKACigAooAKKACigAooAKKACigDsvDv/HlL/wBfT/8AomCiuKp8cvU+Sx/+91/8f/tqPz+bqfqf50lf9Qkdl6L8j/zy47L0X5BRTGFFABRQAUUAFFABX1r4a/5F3QP+wFon/ppsq/z0/aI/8m/4B/7LHFf+qTFn+6v7Bz/k/HjX/wBmiwn/AK2eRm3RX+SR/wBRYUUAFFABRQAUUAFFABRQAUUAFFAHZeHf+PKX/r6f/wBEwUVxVPjl6nyWP/3uv/j/APbUfn83U/U/zpK/6hI7L0X5H/nlx2XovyCimMKKACigAooAKKACvrXw1/yLugf9gLRP/TTZV/np+0R/5N/wD/2WOK/9UmLP91f2Dn/J+PGv/s0WE/8AWzyM26K/ySP+osKKACigAooAKKACigAooAKKACigDsvDv/HlL/19P/6JgoriqfHL1Pksf/vdf/H/AO2o/P5up+p/nSV/1CR2XovyP/PLjsvRfkFFMYUUAFFABRQAUUAFfWvhr/kXdA/7AWif+mmyr/PT9oj/AMm/4B/7LHFf+qTFn+6v7Bz/AJPx41/9miwn/rZ5GbdFf5JH/UWFFABRQAUUAFFABRQAUUAFFABRQB2Xh3/jyl/6+n/9EwUVxVPjl6nyWP8A97r/AOP/ANtR+fzdT9T/ADpK/wCoSOy9F+R/55cdl6L8gopjCigAooAKKACigAr618Nf8i7oH/YC0T/002Vf56ftEf8Ak3/AP/ZY4r/1SYs/3V/YOf8AJ+PGv/s0WE/9bPIzbor/ACSP+osKKACigAooAKKACigAooAKKACigDsvDv8Ax5S/9fT/APomCiuKp8cvU+Sx/wDvdf8Ax/8AtqPz+bqfqf50lf8AUJHZei/I/wDPLjsvRfkFFMYUUAFFABRQAUUAFfWvhr/kXdA/7AWif+mmyr/PT9oj/wAm/wCAf+yxxX/qkxZ/ur+wc/5Px41/9miwn/rZ5GbdFf5JH/UWFFABRQAUUAFFABRQAUUAFFABRQB2Xh3/AI8pf+vp/wD0TBRXFU+OXqfJY/8A3uv/AI//AG1H5/N1P1P86Sv+oSOy9F+R/wCeXHZei/IKKYwooAKKACigAooAK+tfDX/Iu6B/2AtE/wDTTZV/np+0R/5N/wAA/wDZY4r/ANUmLP8AdX9g5/yfjxr/AOzRYT/1s8jNuiv8kj/qLCigAooAKKACigAooAKKACigAooA7Lw7/wAeUv8A19P/AOiYKK4qnxy9T5LH/wC91/8AH/7aj8/m6n6n+dJX/UJHZei/I/8APLjsvRfkFFMYUUAFFABRQAUUAFfWvhr/AJF3QP8AsBaJ/wCmmyr/AD0/aI/8m/4B/wCyxxX/AKpMWf7q/sHP+T8eNf8A2aLCf+tnkZt0V/kkf9RYUUAFFABRQAUUAFFABRQAUUAFFAHZeHf+PKX/AK+n/wDRMFFcVT45ep8lj/8Ae6/+P/21H5/N1P1P86Sv+oSOy9F+R/55cdl6L8gopjCigAooAKKACigAr618Nf8AIu6B/wBgLRP/AE02Vf56ftEf+Tf8A/8AZY4r/wBUmLP91f2Dn/J+PGv/ALNFhP8A1s8jNuiv8kj/AKiwooAKKACigAooAKKACigAooAKKAOy8O/8eUv/AF9P/wCiYKK4qnxy9T5LH/73X/x/+2o/P5up+p/nSV/1CR2XovyP/PLjsvRfkFFMYUUAFFABRQAUUAFfWvhr/kXdA/7AWif+mmyr/PT9oj/yb/gH/sscV/6pMWf7q/sHP+T8eNf/AGaLCf8ArZ5GbdFf5JH/AFFhRQAUUAFFABRQAUUAFFABRQAUUAdl4d/48pf+vp//AETBRXFU+OXqfJY//e6/+P8A9tR+fzdT9T/Okr/qEjsvRfkf+eXHZei/IKKYwooAKKACigAooAK+tfDX/Iu6B/2AtE/9NNlX+en7RH/k3/AP/ZY4r/1SYs/3V/YOf8n48a/+zRYT/wBbPIzqdN0vU9av7PSdG07UNY1XUZ1trDS9KsbrUtSvrlwSlvZ2NlDPdXU7AErFBFI5AJ24BI2fFngnxn4C1QaJ458I+J/BmstAl0mleLNA1bw7qL2rsVS5jstYs7O5e3ZgUEyRtGXBTduBFf5KqnUdOVVQn7KMlCVTlbpxnJNwg5pOKnOMZSjFtNqMnb3Xb/p5qZzlFHNMNkdbNctpZ3jMLXx+Dyepj8LDNcVgcNONPEYzDZdKqsZXwtCclCriKVGVKnK8ZzTTS5+2tri9ubeztIJrq7u5ora1treJ5ri4uJ3EUEEEMYaSWaaVkjijjVnd2VVUkgVt+K/CHivwJ4g1Dwp428Na94Q8UaS0Car4c8TaRqGha7pr3NrDe2632lalb219atPZ3NvdwrNAhltriGZQY5Y2ZKE3CVRQk6cJwpzqKL5I1KkakqcJTtyxlUjSqyhFtOapVXFNU58us8yy6nmNDKJ47BwzXFYLFZlhstliaMcfiMvwNfB4XG46jhHNV6uEwmJzHAYfE4iFN0qNbG4WlUnGeIpRk/W/Bni/wzpfhnW/EXhfxDoOj+NNOm1fwhqusaPqGm6d4p0q3kSGfUvD15eW8NvrNhDNIkUt3p8lxBHIyo7qxAPNUThOnLlqQnTlywny1IyhLkqQjUpy5ZJPlnTlGcJWtKEoyi3Fpt4DMcBmuGWMyzG4XMMI62Lw0cVgsRSxWHeIwGLr4DG0FWoznTdXB47DYnB4mnzc9DFYetQqxjUpziiipOwKlggmup4LW2ikuLm5mit7eCFGkmnuJ5FihghjQF5JZpXWOONAXd2VFBYgUEylGEZTnJRhCLlKUmlGMYpuUpN6JJJtt6JK7NjxP4X8SeCte1Lwr4w8P614V8TaNNHb6v4e8RaZeaNrelzy28N3FDqGl6hDb3tnLJa3NvcpHcQxu8E8MqgxyIzYVVOE6c5U6kJ06kJOE6dSMoThOLalCcJJShKLTUoySlFpppNHPgsbg8ywWDzLLsVh8dl+YYXD47AY7CVqeIwmNwWLowxGFxeFxFKU6VfDYmhUp1qFalOVOrSnCpCUoyTfYeE/h58QfHsWpTeBfAfjTxpDoyGTV5vCXhbXPEcOlJjdu1GbRrC9jsgVywFy0bFQWC7Rmq0fgjxnL4a1nxnH4T8SN4S8O63B4a8QeJl0TUzoOh+IroKbbQdX1Y2osdN1mcOpi0y8nhvJARthNX7Cu4xkqFaUZxqThKNKclKFH+NOLUWnCjq6sk+WnZ87jY8ypxTwzSxuJy2pxDkcMywWNyzLsZl8s2wCx2Ex+dzp08nwWKwn1j6xh8VmtSrThl2HrU4VcbOpCOGhVcopw+HfCXinxdJrEXhXw3rviWTw9oGpeKtej0HSb/V5NG8MaMIm1fxDqiWEE5sNE0tZom1DVLvybKzEsZnnQOuZ9f8ABPjLwrp3hnWPE/hTxH4e0nxppI17wfqet6LqOl6f4q0RhCV1jw7eXttDb6zpjC5tyL7T5Li1ImiIlPmJk9hXdN1lQrOjFXdVUp+yS5owu6nLyJKc4wu5fHKMfikk9Z8Q5DTzWnkU85yuGdVqipUsolj8LHMqtWWCr5kqdPBOqsTOby7C4rHKEabk8Hhq+JSdGlOceXorI9g7Lw7/AMeUv/X0/wD6JgoriqfHL1Pksf8A73X/AMf/ALaj8/m6n6n+dJX/AFCR2XovyP8Azy47L0X5BRTGFFABRQAUUAFFABX1r4a/5F3QP+wFon/ppsq/z0/aI/8AJv8AgH/sscV/6pMWf7q/sHP+T8eNf/ZosJ/62eRn6w/se+LtV/Z8/Y9/az/ak+HjaZD8Y9K8WfC34PeFNfv9PtNVl8EaD4v1TT5PEGsadaXkM0Cajqy6iltBNPHLbmfS7MTQTxRywyb2tfAf9uj9rG5/ZYs/2gPiTba98OfiV4xPh74eeOpvEvgrxdfaJJ408Iv4/wBXk1TRPC9zZajJqln4Q8JXV1DYeIWgu9LuFOktPZC/mjf/ADHo0s3xuWZXkuX1XHAYnCQxeY0o1aEKdSpic9nh44qrRqVadXEyoSoYGklRVWpSVDT2UKtbm/2tzrOPC/gzxF8SvGXjnJ8TmXHuS8a53w1wBifYZpXpYHA8G+CuRZvVyWnXwyrZdlNPOKWf8RY2picwouFfFYyhXor6zg6TVTx9+zf+zfF4V1f4q/s66j8U7K9/Z1/am8HfBP4paT8Tr3w9qcHii21PxZZaBoXj3wtLoGm2L6ZBqHiG3khbTL5ppDaTs0lvYPaxte/Sn7cHwb+AXxt+P37bel6PL8UtP/ad+FPw2s/jf/aN7N4cX4W+JfDvg3wV4Fk1TwjpOk28c+vRahbeFLvTsarqF9Z/adcuLq4jhFhZx2M/VHKsq+oZjQoVsweDxlfLK2WyqSwzr/XqWE4nw6WLnGhCnPCvF4SvGUaNKlVVKpTaqOVJzrfGw8TvFiXG3BnEeb0OCv8AWXgnJeNco8QlhqGa08rzHgvHcW+AedY3/VulKtVrYXPaOQ8Y5LOFbG16mDVXLcwk6M54nD0I8XbfsgaB+0hd/wDBP/wfpnxC+LMHgPxV+z/45+IWu6Z4ov8ARfFWp+EfDngnUfDFvr3hr4ZWOnaPpdvpo8S61efZ9Nj1J9cez+12d3NFeSWctnd17v8A4J0/DjW/HX7Lmu2ngf4/fB74a/GD4ra98H/iB8Ofi/Poln8TdG1XTvC/iTxb4e8TeH9VtNIfTv7G8SabodxG8V1p16bS7sZY0kfzZEtilwrSxtGGJ5s4qxqTy2EMQ3TqUMPhpYbIXWpYrEvCunGu6eZ1YZfBOCpQwCpypYiNTmpckfpLcR8GShw5h4eHeWSyrKPE3FY/JMPRzGjmGd8USz3xlxGXZxkGW1MdKSyTLs04Gy6rxUq+JdfGYjjCdWhPCexjGn414c/Zk/ZQ8X+LP2orTTNR+Otn4M/ZN+HNnq3i+/mfwW/inxv4v8E/EDW9E+JEnhrTnZ9J07QvFOjaXFY+EBql/FeeHr2eTU9Wg1SK3Wzu+0h/YR+BXxT+NP7JafCXxR8Q/C3wM/aH+EPjH4ueJo/GU2jaj468HaZ8MJ4l8Zada31lp/8AZclzezahpmjWU7WWp29jdC+1ZTqdmbaxHLh+HspxjpUqGKzGFaviaHJKtHDOlDASzillGIq1I0qfPKvF1qVeEYyUYxjUi/bOceT7PM/H3xg4UxWY4niLLuBcflWUcN5t9YwuUwziGOxHGEvBr/iK+T4elUxNSlS/sSlPL8blNer7uKrvGRk4U1hqdev87/tFfs/fBa2/Z++Hf7Vf7Od18Q9P+HPiz4ieIPhHr/gr4nz6HqviPRvFGjWusappuu2mtaBDaWk2leING0iW5fTZrR59PuZbcLeEPNbW3S/A34J/s5+E/wBn/wCHv7Q/7QF38VNY1v4rfGub4d/CLw18Lbvw5YDR/wDhELuKPV/GPiRvEtnLb6pbWfiBYrVtLgvrJ5LZIkhS4kvJptO4HleVwx9adWpj1k9PK6OaR5Z4f+0HDFU8PTo4eVR0ZYZ1IY7ERo1Kiw/JVpU5SjTouqvZ/dYrxM8UMb4YYHB4WjwcvFXFeJmceE2Y46rSzR8GLG8PVuIMfm2d0sHGtDNKeFx3C/DuKr0MN7adbD43FxcfaRpRoy+1vi/+yTo/xj/ap/bo+NXjnwN8X/ij4X+HnxD8AeDdB+GXwKXSx4/8YeL9f8DeB7m+v3uNYtLuy0/w14U0OeO/1JsJdXcl9CIJlNl9lv8A8xf26v2cdN/ZV+Nl94C8Oalrl/4Y1jwV4e+I3hQeLrW3tPE+j6N4nj1AJoPidLSOK1l1fQ9R0rUbG7uILS0WeKGF3tIZzKg9LiHIlSoYzN/Z45Va2cYjmnOCWXzo1sfmeHjTw9T2KbxNKWCp1KkViKl6eKjzU6LpxlX+R+jx4047OeKeCPCx43hivkOB8JuE8FhcrwtTE/655fm2QeH/AIa5vic4zWE6ssJ/YOd/61ZnlWVxp06deOK4Zr1pyVPEcs/0+/4KD/Hz4q/saL+zh8Dv2ZvE6fCXwdF8F/CXjPWrvwlonh6HUPF+vwGfwjb3mu6jeaTez6gy6X4YtZb1ZGA1WW7EuqfazBaeT4p8IfiD4N+IP7EP7VfxP/ajl8e+KtF8U/tf+APE/jtfhrF4Z0rxV4g1rUvDelPBHYy6obLQtCsEv5Ybi5NrZzCHTbM6XptlA1xDc2fdjMfOWe4vKcXWrRybLMBjKNDCYVUaao0qeVSjN4aMoOnTq11Kcp1JRmpVKntZ06vJTgfnPBnAODpeCHAXijwpgMsp+MfiDx9wfUx3FfEGKzbF4bH5rmPixhK+WrPaNOvWlPLcqxODy6Cw+Cw9Ks8LhJ0IVIzrSqnYx/su/B74PfHD9qz4Y+BfGfxqsI739ifxJ8bPhl4h0bxD4c0yG6+H174Sa58V/D/4lR3Phya912x8Qa2dGmtk0q30V00azurO/uPtrxXTZvxa+DbftDWv/BID4LjVJdEh8f8A7Pttp2o6vbwC5n0zRdP03Qtf126tYJD5b3iaNpV+unrNugN61sJw0JYGP7HoQpY7LcHiMZUhWxLwEZYqpTnCMVxFgaOHq0adOjRVKVTDzw7xEW6nPiI1JQcKbhTh0YHxTz/G5/wj4l8TZTw5TzDLeGMRx3UpZNh8Xha+Lnhvo6cd5lmWAzTEYjF4mNaNPO8qzVZVLDRhHC5VjcLh6tTE4mnVrS8k/bD/AGJfBHwu+CVv8ePhn4U+OPwy0fRPitefCbX/AAR+0BYafD4l8TWs1i+o+Gvid4Ym0yx05bTQNWEE9hPpl7BJcC7ICmzexuoLn8r6+Vz3LaeV472FGnjqNGrh6GJpUcygoY+jGrDWnioxpUIqopxk1y0aa9nKDcbtt/1b4AeI2Z+JnAMs5zrHcOZtnOV5/nHD2Z5zwhUq1OF83r4CdDE0Mdkjr1K1aOFlgcdhKNWNStVl9co4mcZRpyhCHZeHf+PKX/r6f/0TBRXzNT45ep+gY/8A3uv/AI//AG1H5/N1P1P86Sv+oSOy9F+R/wCeXHZei/IKKYwooAKKACigAooAK+tfDX/Iu6B/2AtE/wDTTZV/np+0R/5N/wAA/wDZY4r/ANUmLP8AdX9g5/yfjxr/AOzRYT/1s8jPtT9lT9prSPgMfib4O+Inw6tvjB8EvjT4ZtvDnxH+Hd1q0uizTXGkTT3Xh3xNoOqLBdrpuu6JPd3ZguIoIbljLbXFvf2F7pthdw/QV3+314A+GWkfBbwl+yv8Cb74eeEvhB8a5fjo6fEL4gan441bxP4kvvDWoeDdW0aeb7OjaVpWqeG9TvLOaeDULyWGQQz2tvCySi6/y5wPEVLA4GhSWXKpmWETo4TMXipRp0sJPG08e6NTBqjetOFdYh06n1qnFLFT5qU3Tipf70cefR8zzjXjrO8cuPI5V4acXVMPnPFvBtPI8PicxxXFWD4MxvAlDM8rzipUisBQxWSVsv8A7QpeynLEYjLKbmqjqxnhsr4t/tq/CTWvBGteBfgl8DNc+Glj8TPj7on7QHxkv/EfjgeLLzxBrmiaquv2vhrw0q6ZZw6PoFvrf+n2TygvbtEIo7JBPK69T8XP+CgPww8dR/tAePvCHwC1bwX+0J+0R4LufhV4v8fXHxCudc8M6Z8Op30vSrs6D4al02CWy8R614Q0HR9I1SSK4itIdStn1O0keJ5rG72lxLh39bp0cpdLDyo0f7PpvHuc8FiqVLMqccRUqLCQjiqfts3xWI9j7PDv2kKEPbckZqp8vgPo0ccQlwzjM08TMuxmaPNcavESvS4YhRw3FPDWPzXw+xVXKMqpTxcnk2JeW+GPC+VvGt1+aFXM5yhJVacHy/gn/goTe/Dm7/ZIvvDPw/S7H7PPw08a/Cjxtp2s6x/oPxG8LeOtS0671q306axs47nw3dRQ2MT2NxMNVWDUILO4mt57dZrWavpH7Wv7PXw7+M3wS+J/wi+C/wAXdMtvhn421Lxd4js/Hvxy1TxtceIra78N6poWneHtHstSsLjS9Ei0ubUpbyLXHNzqd1C8lndJIrLKmMeIMO4YN4jLpVcRl+IwNfA1Y45U40vq+HynD4mFWnLCVHVjiY5XCVJxnSeHniKrn9ZUYI7q/wBHDimjic7WS8bZDhMt4w4b47yLi/C5jwpRzTFYmefZz4j5xwxisizGtiFislll0vEKVHO5Yeop5lDJsN7LkjWlCl5f4F/ar0vwfP8AtqyzeDdQvh+1d4e8Z6FpQj1e2g/4Q1/FPi7WPEsVzqJayk/tdbSPVUtHithZGV4XlDxqypXp3w3/AG+5Phjq/wCxrq2kfD59TT9mX4ffEz4b+MdL1LWoEs/iP4e+JupWl3qSWEiaZMdBks4LZXg+1warHLe29s8kbWxnibPBcQzwdbD1o4ONR4aNNKMq/uzcM7wuc++vYP3JPD/VZRu7xm6t7r2L9/in6POI4pjneHxPEdDDYbOlXjKVLA1Z4jC+28DMz8IoVqbeJhGdXD4/H0+IIXcFKhQeCvGrNYiPAftEftR/D7x78HPh/wDs7fAv4V6v8LPhB4K8aa/8Sbu38UeLm8Y+J9f8aa7Df2ayy3yWVnb2ej6PYarqFlY24e7nuIpLZpnt2s9txpfCX9qv4U6H+z5o3wM+NvwS1T4op8NvibqXxX+D2saH45n8Dto2uarbJJfaF4lks9Pvb288N32sLJql7/Z8kN5cidbdVgltIb0S86wssbWqzy6c8uqZdSyyOAeNaqRoYenQ9hOWMWGXNUWJw8MTUaw8YylKVOCprlkt34JcZLw4yvJKPHuBw/iPhvELH+JOP4zjw/CplNfPc9xmdUs7p0OH54nlWCfD+f5hlWCo1cRZ1KdCtW9nCU6UPU5f+CgPhHxX8SP2mrj4mfDDxfrfwU/aP8QeDfGtx4M8J/EKfwh428E+N/BOj+H9I0/XtD8XadZwefDf22kTW2raZMkFrfWgsY5g8UF5baj8N/HLxl8O/HnjmTWPhh4N8U+B/B0ej6bpdrofjTx9qnxJ1+W5tftDX2p3niLV4YZo11KW4Mg0i2Q2GnlXFqSkxVcs0zbDZpSUp4GdLHRxGIlHErGKdOOFrYvE41Yf6v8AVYSlUhWxcoLEfWIx9nSglh4ucpHoeFfg7xN4acQ0alHinIsZwf8A6rcO4TF5NT4Ww1PPqnF2T8I8K8IYrNaHEtSrUx2GyTGYbhiGYxyeMpRWPx+JlPSMZz+5p/26vgf8ZvAHw98Mfti/s13vxe8U/CPTLHw74E8beB/iJrPw91G88M2ejaNp8mkeLjp7Rz6g2oahpMurX9wl1PbG4vD9g0uxeO5lvfBvFH7T/g7VPgR+0J8EvCnwoj8B6T8Y/jr4f+LXhex0jXBe6D4C0HQLGwsLbwikVzYQ3uqSNFY+Z/aQeyhV5SEs44gkcfVi+IMNi1Cv/ZUKWZ1MJVwmNxkcbUcMSpYNYKnUhhPYOGHlGny1aq9pVdapG/PSc5yl81wr4A8a8MYrD5K/EynmHhpw5xjkXFHBHCFfIqFLF5PSwfHOX8b5rh8fnFGVPEZhVdfDYvLsmdZ1aGBw2YVXGnSilQl6/wCJP29dB174x+Mfiknw41e2tfE/7F+q/sqRaRJ4hsnmtNT1HSoNNXxnLdjTUjk0+Mwmc6QsKXLKSn21SNxzR+3dp2m6J+xBrPh/4c3lr8Wf2O1g8OHXb3xCJvCvjf4fDTbrRtS0M6RFaxXOk6rrNg9vFPqbte/YD9qltxOJIoY7jxPKMsRUWCSnVrfWKMlim1RxCzHBZhTk/wDZ17SMFhZUZQ9x1PbKopQdNRn59L6MmYLB5Bga3FuD+r5fkcOGc29nldTmx+T4rw7414EzeOFdXETjh8VXXFdHGYWc1NUoYSrTlJzqwlHzX9o/45/s7/EzQLiz+EPwY+Jnw98Rax4vTxPrGqeMfjd4g8b6BYWTQ6s134V8NeD7q2XS4NNkv9Qt7y11S5lTUtPj02PT7WNbGVoo/jCvCx1bBV8TOpgMHPA4aSi44eeK+uSjK15N1/q+G5ld8sV7JOMYpSlOfNOX7x4XcNcWcJ8J4fKONeIMm4mz2nia1SrmeQ8OYThfL5Yfko0MNSWXYSUoVMSqdD22LxtRqrXxFaorKnTpnZeHf+PKX/r6f/0TBRXjVPjl6nr4/wD3uv8A4/8A21H5/N1P1P8AOkr/AKhI7L0X5H/nlx2XovyCimMKKACigAooAKKACvrXw1/yLugf9gLRP/TTZV/np+0R/wCTf8A/9ljiv/VJiz/dX9g5/wAn48a/+zRYT/1s8jNuiv8AJI/6iwwPSigAooAKKACigAooAMD09/x9aMD0oAKKACigDsvDv/HlL/19P/6JgoriqfHL1Pksf/vdf/H/AO2o/9kKZW5kc3RyZWFtCmVuZG9iago0IDAgb2JqCjw8L0RlY29kZVBhcm1zPDwvQ29sb3JzIDMvUHJlZGljdG9yIDE1L0JpdHNQZXJDb21wb25lbnQgOC9Db2x1bW5zIDEwNz4+L1R5cGUvWE9iamVjdC9Db2xvclNwYWNlL0RldmljZVJHQi9TdWJ0eXBlL0ltYWdlL0JpdHNQZXJDb21wb25lbnQgOC9XaWR0aCAxMDcvTGVuZ3RoIDUyMjkvSGVpZ2h0IDEwNy9GaWx0ZXIvRmxhdGVEZWNvZGU+PnN0cmVhbQp42u3dZ5PcxBYGYJloMpicjO21MVRR8P//B1AEY2NssskmB9/n6r3uEhqNpiXN7KxvbX/YmtFIrdPviX36dO+JW7duNcdtQTtxjODCdozg0naM4NJ2jODSdozg0naM4NJ2jODSdozg0naM4NK2TwT//vvvf/755+67777rrrv++uuv77//3t8TJ074CVX33XffY4895sNvv/3mhgceeCA/HbW2BwQB98MPP/z8889//vmnz5B68skn4Xj16lVg3WobyMD30ksv/fLLL5999tnJkydfeeUVF3/88cdff/31kUceefDBB/cN3eEiCKmffvrpnnvuefjhh8H08ccfQxBGZNAHcJw+fRolPn/11VfQfOaZZ1wE7jfffPPll18+8cQTL7zwgn4+/fRTV1588cWnnnoqPWPDvffe+3+LIODgBQvQfPLJJxA5f/68MV+5coVwvfrqq8Tq+vXrN2/e9NltLroNgiTOzWj7/PPPQfbcc889/fTT4Aa9m8+ePUsM9f/HH3/k/lOnTj366KN7UfMdIqjna9euwc6Aicn7778PO0ixaBBk9VwnlcQKEAcHBwHaIxAhkvfffz/IAERzz5w5AyDM8NXNOoEv9nj2xo0b7te/e/ai2rtFEFIZ/+OPP27wRksZCRTTRlth5J7ff/+dzroIiB6CfvIVcCSXKdQVgQU6uAnv119/TUIJI0yh6aJ7vJc7Aj2I7zwEdUXFjJOUUStXvvjiC4N8/vnnAeT6pUuXiJKhfvfdd0EKHDzDOgTzlW6SVoh8++23hE4PfvUTz+Nx3sYHj+QeL4UsT6VDNvfOQ5BkgYxOMfbkLqhxteyaX999911/DRUoH330kQE/++yzUKbd7ucuoAkO8kVsIUj6IEgSX375ZVzRM90nxUB0HbdACaZ33nmHU/IKOHrk8uXLxLDYytXmV3duy2huWYuNlutk+40/w3777bcN0ngQTQdBTGpg+t5777mfPAICNO50P+2j4FQSNFQSbQRKh0brcdrqNhJNxFx38YG26ZNrhqAOIesnQg1oj6zy2LM0AA8wePWG/SPYtP4xDpQIUCWfIxGGCqwPPviArAELmpAyTgJLsjxIjiJEcHRzhkc8/SoSimkzbL2BCb7657uJrUfAijHQYXn9hAGrXiXw4QcyMBjKiUOPHIJNK4kAosKGwVSxWXGmBm+ErtDZTEiM1hhCwzq18qs7c4P2Y9v0Fh/CMpIpLCGGLIN3gVLos9oPToDPDXrTT2yIpxiNI4egRjSMzVDhRY4giOHeRWr8CtnZZiiAFtlhN3kYCCZ4IqQxiL2nSB9t8PbueIFISxC2BMQdRjMFxNhEUrOLt1Dw8AmOAGIuEgZ0EQcff9WDL40SsAAMy2yO7nZOAkS6A7to647ewiB4ERFj4LyLCj/00EN5Xdf2DY//xAkKAURW+JAQZOPiE7G95n4at9DQ1LSkcIghvLwxugmdQeVdBRHufNq8Kc00BN1MHVhuYs8G73dKP0gejU4YgDyfwcd1bHwQj5O8mDGTmYwg+IBIDJEoICivdKVpbfO+YfxvAESsAFcJXxqV4pqNaKrGTNZiOpKwAGQ0xVtJIjOUrBT1qdTu3bVx1zHSmCZh1lSDWIugUK5pGRUSeUAkAo4kMiIUB9FMOEB35zEq4WMN0cYsTn2W5CatK3jYMoLEzVSJ9JmfYRRVJeq8m2AFiGSQNxSI8Wh7t4zgMy/k7uY9HjUiB/XBTRWCWEpJwZT1CjhyWz7gcySROnvx3uFDiWkJqpZkDYxLTC4e2hqCgEtmmIOL6DHVeOVNXuMKcjmyo+BDiB7vQQyXBLlGRCDocqVL2YxgMcxQY+n0y42wg4DzmfNycS/eo1DelTjcpS43btyI4Z7XSINJFLu0HQRDK18BRMpLhTGZwpI7PCeYh5PI7BIDpj/aliu0AT2Fi+w1SQQi7ZktjEZnLl8jhmMIdufwyZ4CEVmUl9ffFnBeAYuIjHfh/4g3dxuFwM6kWHIRdnyoMZfFpuQZ4/3mUWUaWjmXH0NQTE9bGYWy/gBBvIWg3nNxYQOEED22NVgQc1gQ7VVvkPQtDSgL86UZhWe7blSHSXzNA1EnrBNB2SiGaxFEAQeC6NOnT2MF0UO3vySRFAgDxS4LJ7wic5LCgaKhlCpotBIWp06d6vaPHjfjX7m5PNIddlay8jVre9gzT5fxklPeOFleiyBWX7t2DR8gZZCoF+ILNdlXH/TLhywJGnSSVWMw0VwSrTdjzsIbEGFR8lQxxOAoeQqUkFNyh8cwik1MrUh3hQSTjKJYzEnNi9Yla6sQBBkQwYfW69evGyprTQxhtxUjSMCB0rTJfR3GJiQcieq5zpYnxnT96tWrrEoCgDySmUOytn7F5qa1gMZMdopBZHno/jxdDg3jkcZaBGkrEMlCHB+yaK6LuHru3Ll1y2CVTYeXLl0ycv0jsRu+AousMRR+IgKZpTLHly9fTsrAzaSsN/FyP5ZkpTg3eLzbIfRn0Am7CxcujCvyWgSz1gEvnjFJC9Rn9QN99SH7YNNJpEa37GzX3qXSg9S4mPqYJITcT7sR4wpkewYEcCB2WxZAsixTfiXsKJ8RIcawjk/yxnxxql6MhIoh8au2JY9WH0KnKqFpw9SCVEEQcRDs0pcUpNZFEATMGXq8l2EZNMEQdJvr7kmVRHcgbC45nYEgbUuJxDQEsUuc4TECnOgsIglH8FUKIKX7pm0lUqGSIAOl3qLFKUAoGpfRApc0uY2sYVjSLTQxMsieuL7KbDNiGA3KYNOKYQp0pno/hvjixYsj2ZoBBFOtEo7hQBbOYcflwbTShySSYL/0jwh0xyF6nJnXIZHJMjGHK/6IJ4G1eEUk7OaAm+te7f6M3xVmpGebEjmEVVng743Zs/FFk+AL45GBkRMQZIZoa5Z3SSJ1QG7ijFNt2/hWDxJYbM/kL24HCsaZTtCUAg/cwhVCbdjQIZWJrj1I2EskgQxqGGsQzSLLibqxNosh8cWGQ8dxfTXkZlsZh9VofLxl7s+e1CKYGj1j4HD9xViU0ZHw1qhqvDBW0yk9d81wUUafyQjGEDcjL6FcuQ3bYYfuEsqICghsiUiiqvhqeGiDu3tcTDyI8kFFoRDh2TQhbBrCxCPXIggvCPpAs9CEaW4wsUtFafRx4yuJMKRgXeqp0oq1wlLgJutDZ8ldQYeCg4/kxlmR/USIq7rSy80E+hTlDBJZootJ8OnW2CG4LvvZRzAFZ2FUMguZqCalWuOCoy9ojbZ2H0lFJdHjYSlafJQhubm8EQRFbANfV/pW4SuN4RsvQEh0HZmYhCDLS67XRYV9BIkJEI0H9VkaznQYEJlm1cggBLGB8Kd6tTuGOBARid7SFZVPmWXIBTr9bVptMNpB+JLCQVh0PxqNzTGmI4QVn16PYNM6E95vXbpwbTzoOhIDor/e+mjbal5JNyFlMASqlIw3tz2mblNGk4s9BNFKi8GHDYOZFSxxA0oQFgRJHzGpSXMwhbHskxDEMGZn3QT5Xwimmr7MwxOdJnmZuqnKZExcELwMDFJxPuw9k4f6VEsW+7iKoPvXKS9inmnbvLQQGlgMOE56yrviEjcjmNruEjSlSiwxIKK7ce/GxjlwoNkBYdiRaE3gQjC7vrKLoOZFyBiUvmQACXUMqBtCfNZqaqhKtRzWTkLQW8DHcG9GEE1IT4Fa04Z1BkOgaDELSJLr14KTMokmlvJAndCFXqjRQxDcyFg1VWT26bahAT10hTiXSsAsw27E0YhS/j8JQW/B19TIbkCwuZ3jLJ+b1iGS/NQ/T1qKblqDmMAwX994443VHnoIDjYYpcgk25pAQMYLypm5Y8/GckpYQFCwtUMEV1tWO1NFOTWzT4s//PDD8oq33nprNR7aiGBX+tzMOBBAfWYbCcKizj4nyz9OEgurh10hmJqYnqtCIrqpSS+4m4Hgm2++uRqXjiPojXxR6sRQkuVg2ooe4QXNzdoTW4HTGHxwcDBur3eLYM+TpCWk1MWMbVc1CHYj6kEEOZ88BSzKW9yRv6Ente+xbok019GDEvAJM3eFYPYJ9sQhlR7zNqduRHBk1vE/+v6dOmza6AxMPW2F7JUrV/SGzRcvXlxHDxHBrd16kh65TavF1IQx2roMdldFRjrpzX/1YDC9GjVEiuHRSbtfe+21df4kS6BZn6lvCMjaSxWCNIIYUpAS8Ron9qK+lwpdiOAIfCN8Svqgm/hKy8ofyrE5W0YHH09cMTVZjZ7n21aFYHa8Aas7DYz9TonMVhAcgc8N5GjQZZGg2MEkX0tgRDdLyiALL+vo8bjRlYC3smW9IbP1zQiScBEcU9JVE3aK8CeZPEmRBxEcqbDKpK231l5aEoUkiCnE4DjoxA/mi/CF+7lz50Ym715KBpPKrm+6NSFZl1oekEHqEMgDFp4LQSGbCekSBMWDqWhZB18SqyMzn2zvTD7V/cnQsDyp74HpeJ2GOZLHp5YwEPbM1qsQTIaVXhiMJxNAYDJaZ9QY9xB8/fXXCQtmrI4Bn2sK6SDlcc40mlgy21lOYKpGHs/aw9TqwmRY2dZ1PQ/M6rKd0vs8k91vHAj2ztg91UUwVcp0cBC+bHFbR+Wt2y2pfPDpGbOjuZkXZ2PjCDHZklzmC9nPV1PLQPoguO7XgWgmp0ok854pZ1L8M/bk9mRwsEX6RpSXzpazQVI7ywiiKhUKScFtTHmggeQyICl8SNoNJ6jXOHkQYAHXhTLNeDwYFmUpB+sQykhT7XocNyKYgghtnXRjJCdrmpROIoZA9Ehlwjwty9AJ3b00+fbUEEQMkyJbzQm5uZcnrkUQh0VPSOdbsiaZWn7d1e+eGkcw0jcCH7alziTHrBAcEpS8FkZmPb6SklRJxAXRShFPzglJ2TqWEA7G1Fc0d6eYLl64cGEkEB5GEGosbhSH/uaEHP26nrL9ykRhTgkYfMVG6SvlH8YQictLjZA/RVt2etdsINBVNtGGkgggW8/iu8KAGlFxtb2Ix+RCbDe5bkYXeo/F0XV2ZoZp7q8sYEVEfN/grxgDghFOUCgMIBTZ8t8FmjKaw3n24OCgpoaCGU36J0CcPXvWU6nL8EGMQT7imkgiwgwzZG9cbl+LoJcZP5i6HE5cnc3CG1OtG7PBqxWoqy1zuNVVHuQRbRylXxsrAHACIt0V59RVxhpk526qatBMw4REviYDhknjIXpTk2EFJYphkUAkZnXchJeK3WZZW4dgdt54y/nz5zciGHS685DIXYQ3hWHmSLF9qW7OlaaNY3oLthMQdB2hXCHukfDkuGqiQpxMHf1gn1NB9C6WpGfvdM6u0QMCMl5IlmOoyg7PZLabdpqRXUSZLwRfryAfJI6JyJA3Fg8247445iBVaxqmuQhQ74Pm4D6mSN/grCOB26SIkvfP2TBPto218hVJ2VmaUsxxS5oCqERmOZ4r9UowdT0xRpnVpPw9E0fPQvnMmTMbZXwtgqhPqRWe5OyS1J3EoHgxzeqVYo4oLyrRVx98FBr0lnReqnZcYVIML6sOI3FVamFJa0obypbDKFDKoLrazRqmLLH4nATS83dDrDaCnfPbuCeSnwKiUnoQ+JA1mO/LwVEzsty6zVS6VCjAguaOF4JmbpqjbnIFdgggeijMyh9ouvzOqqkHU3BQipg2UrgZQdaQ9NHiJNdSIJ60KwuVkuasAWWzR+9xhD7XtiXHoyS2j0Znc+l4QBpnWoovkk8s56CV7YARw8Eeaqr4axFMZX1OuvM5yeqmLXEj+ZhZltBWpQ+tYf4hHFZR4M6hj6WMJKsCOQQsobghZF6A5vjcXotLqak1rUKwnLiGCKjBKzt9khchFyPwdU3P4cBnDpNsa7lCWtnfVK1klk2r2B9fVxcmm9tHf1QKYBWC8WgMX86bpUFlTpJlxkH4QgoJnVT4v6RRUgzOrrtcYSjRSYGymxIvU9rszpzwU9IKXR5wwZSsPhNa5UmyJRADU/uTgAB8+ImOkXI8/E/V0uBOw201KPByobC7sJclvZLuTEZL4zFyLl9q/7tdkZKNie45CDatJFIN/ZbC/JySV1PNiKvoNpitn/KZsB8XE6X2xkJ5Uy0VL5e8FjL4kJxYClxa3B1Ctq1O2m8059SoGunrtUxpUJ9Sja3Al/woCHxIqrU/tttmpJyIG52lFkDMQXxdOUhQ7adJ6jIZwUnStzokzKfR7FEq8aeqNmpTp5ws+sYTZRK4sIApokY21LKblKsFIuHNRu4crzDDak9DcAl8paXmimrnzAEf7mrbiduti1c3Ve7tN9vmQ0q2ahCHCEkkXHoI8XkQGVlKS9iIr5OSx3MQjPJi2pJDIMrAMrvIYbYkwmCy3JFlzKadjUTi4j1Ty51WlujqeZZ11OwOXJWAeacdTUOw7NBfIn2DUDatdnc1upc6jAyOn5NZD2JsYqrl8lPOcdyYsluKYDaLZ51hiwiuojlA4jbeGHVOQUQiwSCYhdaUd87reYIMLj8MZ78tM7zsOc+cr3KZf7xNsIMjW2TuxJYik+XHDU3zxQEx86R9I7CobQu+ZkY8SP4FUN3p5x3XwJedAVuZsM+ZkyTrCcTZh9TtpZUtZERP8LytfMfMs4BpMQThmP9qs29w6obanhMCvo01StO6nT1+/sScFIjmWEffLIIsBy1v3P85tS09j5pvEeILUOedK3QILQsgJa+1/f6X62DqcFMVedSEkbHLycq7S1Bu7Ux0BhGOjOMRsYzJpyUvudPzObf8H16S70wVwL4Cb9ilJGGLuciRtv1z+QEnysn5yjnI7dD+hVewA5zJ76RK0SVtt/8bgjDevHkzBc9TU1KVLQUU97eNsTv8M3V3/h//UgaYUjMtNRjb6jxnVGQNXju0ZdVuO7z/Okmjc3BSNj+m/KX591k9Y4Telt/klh9s28mTJ33+f/6fiYMtKftykEgEs2ys6d2cJYFsvkluKidn5P+l7hG4fSK42kqd/jCJnVNXj2A7Egje0e0YwaXtPwCUFlcKZW5kc3RyZWFtCmVuZG9iago1IDAgb2JqCjw8L0xlbmd0aCA5NjYvRmlsdGVyL0ZsYXRlRGVjb2RlPj5zdHJlYW0KeJylVs1y2zYQvvMp9qjMODAI8AfsTbGVjDpt4lhsp9OmB4iEFXhIQgZIp9P36JP1CXLMMZMX6IK2/iyZVWxqbIIkFvvt7vctcBO8ygOegKAJ5GUwyYP3AYMf/dsQKP78fxExyOvg9HUIIYX8Khi9yK/93M0UCkW9bcTCiKQM0lSQKNo1xrVsQAn3F3wKaG9x+eZ+YBfBaPp2lk/zX/J3cD6Bi8vJr9PZ9N+3MHt3Nh3/5F1TWOCfXwbeoMtPB7D88Sfeyz6WGxCM8LT/IjgwCinPcA6c6npB4dzA+x3od98R88oME/NyazwYeEQhFiFJxW7Q6+BCEqaQ+7DzIhhNGrDKLTvlWgkSXAfOVLrQbVdCqaBQttVXupCl8Y+6XhqHX02jHBTGelPTlFo1rQJZgWxavegMOO1aVUtYWnWrHU6X1Qk49VVWsjZuK4H3MLaxp5REice+RoyBxxvENx26KtpOVnXvtkNXJTQGig6fJcJqQDdXxtay0N8ajGduFQbmdtGrBirpYC4dPmBopWyNH1QwbVyL8bc+4mGocUbCaADqxV34CGJmCi0rMrxclBJkx/Zym3mjS1UYDLjsg/pheCGekDjcWSgkYrfohXR9RTFbrbR7SdsqnJ8lq0XXSFB/od217LO0KjF+tWopbWtOAGnkM6mtXmjr0Ay81f9lkcUkzAbQNp6cFmtlkIi6wcHZR12pcYu0O7T2oBQf142gEKUDuqFY6yzequ4/0F9nX8qu6vOgSySgLmV5oM4I6oGvZMXzg76oSBK+5+vClMoi17FkupeUBixFaz/XGpWARLZKVvpvr9alsZh9kEtvgi+OwhStCH0YU5pQtofp3BSdV6IBr0wk6dJ2aq4QlZzLa9SU2RXehk6TezoZP5oR/zsGJF/J5BGQabiHcYvwHkvjG8ed2JHSTtlbDw4Hi87irdfqCVQaG0crH/a9wxifwjgkfxLRxxk3mmAjqn0xP4zkhxdr0T/RHWOc0AzSMCXZd++IZ76uZi59x8Vm4boKM9OqStWfW9wevn9PHExLJMSAEDflHVdQmzvylb7Hg29aCOxK3iL9ZdFhL/JwD/TLpyaRIvtiSLJkD+ARSRzYTVECl18WGoN5di43nkmMV4Lmvs9mGcliwEPFelwFs7WDB7ykQ7zEfc15CT2XkHhEYwx4NFDs445iT2Uah4RFA85/Hv8Gr8aT3wHCmMeCC/5SPNdnKCBJsgGnr1XxUaJHfkrFKaOMHuXxBnev/jXeWMqBJ2x1wgz3T5j3ExBBtDpfRpvD5X8bF9tYCmVuZHN0cmVhbQplbmRvYmoKMSAwIG9iago8PC9Hcm91cDw8L1R5cGUvR3JvdXAvQ1MvRGV2aWNlUkdCL1MvVHJhbnNwYXJlbmN5Pj4vUGFyZW50IDYgMCBSL0NvbnRlbnRzIDUgMCBSL1R5cGUvUGFnZS9UYWJzL1MvUmVzb3VyY2VzPDwvWE9iamVjdDw8L2ltZzEgNCAwIFIvaW1nMCAzIDAgUj4+L1Byb2NTZXQgWy9QREYgL1RleHQgL0ltYWdlQiAvSW1hZ2VDIC9JbWFnZUldL0NvbG9yU3BhY2U8PC9DUy9EZXZpY2VSR0I+Pi9Gb250PDwvRjEgMiAwIFI+Pj4+L01lZGlhQm94WzAgMCA1OTUgODQyXT4+CmVuZG9iago3IDAgb2JqClsxIDAgUi9YWVogMCA4NTIgMF0KZW5kb2JqCjggMCBvYmoKPDwvTGVuZ3RoMSA1NTgyMC9MZW5ndGggMjQwNDEvRmlsdGVyL0ZsYXRlRGVjb2RlPj5zdHJlYW0KeJy0vAl8U8UWMD7b3e/NTdekLW0TQikQodBKAbfmyVYRFRGh8Cgte0VQEJRFERFrobhQZRNRUZEiIhZUKAiKgIBK3VBRUVxQNqkisj1ok/+ZSVIKqO99v+/7Z3oyc+fOeubsE0AYIaSjBxBFnqFjBo8dss23DGpWIhT/8NB7Jngm7r7vYYQSLkeIzBkxduQYOiF9GEKubQgps0aOnjxi9zV7PkIovRdC7dSS4YOHHV7+zDMI9SqAMXJLoMIxM/5HeJ4Dz81KxkyYhD/TOsHzGwgFvhp959DB1PE1jFXxCDx/O2bwpLEJS6SfEfqsBNp7xt41fGys8vsOeJ4J8z0IdRjWyVdsIhY/BR6bwoOCmqL7cRX+D+lPXqUT6TRaTh+hz9OP6SlmsZtYEXtYypL6SkVSmVQuPS7tkI5IJ+QmckiZpsxQ31BD2p/6H8Z/HFc4/u18z7nDWeMMufamjkq9PXVr6gepobRpac+l/ZGekJ6a3jX9hvR+6f3T/51emD41/Y30bem70/em/55+Ij3oGeYZ53nMs9jztme/55hX8sZ5Xd6m3ubeNt4c743eQd4Z3tneed7lTUlTuandNLZpQtPkpulNWzb1N81vOrjpcB/xOX3eDJRBMsyM5zNeyfggY1fGxxkHmt952bTWGa0zW7duHWrjWvZcpbeyrPLVynWVoVeKXhl+Tj7nOpd77qpz15z717ku5949F6obWneiPq/+eP2J+vqgLzghWBcMhepDIY5PtASvxmfJALKKTqEPAY4eoy/ST+hp5mC9WDErk9pJ/aXB0izpUalC+kQ6JiM5TekFOFqi7teOCxxhR55zicDRJ4AjBDganbotNZiG0h5IW5J2PN2d7knPT+8VwVFR+gPpa9O3p3+Z/l368fRTHuQZ4ZngmeNZ0oCjxAYc3eDt5S3yPuSd04CjGMBRUtO0CI6Kmw4TOPI04GhFIxwVt0YCRz8AjgKVrsqmlTMrqypPv9L2leJz6FzcudRzHQFHgXOdz3U7t7tuUN3x+qsEjs4GPcGxwXsAR3WhUGg/QlIZwBAkPkEX/2YBhOq/QojuQUizNUszNYPX1z1cNz3cTnqef9NM4sfNTr4ArYP19fVn6k+dDBz9HaHahbVHEDqYg9DPyQgd2HHgvQNbD7x7YPOBdw68fWDTgbcObDiw/sC6A2sPvHHgddTw+fWJX9kPHX/wI/T9qO8n/1Dy669HRiH0WzpCR5ccfWzf/H0v7JuN0D7Oq+iovW/cviJ4avtrh19b/Zpy+OrDVx3O2fvs3vnf/Lb/1N7xewu/g3X8eOrHkz8e+/GD7266rRyhETG85/BtQ+byPK4U2Ki3cjOc9k3h+e1vgcnmOkYgZLzoGArlvYL7EPbATq8BOCs9Is2HvS+RtiMkNwHoBjAkun5lhbKK52qieqPuBBEzWp8bfadvQ40++jHxfTQMRgv0jx+jl+k225hDzCnRGnPV+bcm4MPc3Oh5Gwdze+Rpyz+NbC6/4OkbgKNWLi9bAasHfPeJvrPud0xw7ELI8Tl/cnwpvoFC7J+gQNGL6CFUijai+egAehg9hmajZ9DLaClyonJA4Az0JDqG/kCPogVoJsboW/Q7ehatQH+i4+gEegEk7060Hb2KhqChaA4ahj5Aw9EO9D76CH2IdqEadBCNQJ+ij9EnaBUaiX5DFehz9BnajUrQYfQrmoVGodvQ7WgMGo3uQEvQnWgcGovuQuPR3WgCugdNRIfQJDQFTUb3oqnoPrQOPY+moftB/k9HR9BRtB53wV0xwRQzLKFzqA53w91xPr4O1aMglrGCVRTCPfD1uCe+Ad+Ib8Ia1rGBTdwL34xOodO4N74F98G34r64Hy7A/fEA/G88EBfiQbgIF+PB6Az6AnfEnfAQPBQPw8PxCGxhBx6JS7CNnTgGx6If0I84Dsfj2/AonIAT8RX4djwaj8F34DvxWOzCbvQaqsJJOBmPw3fhFNwEp+I0PB5PQP9BZ9FPaD9Oxx7sBd1wN74HT8ST8GQ8Bd+L78M+3Axn4OZ4Kr4fT8MP4On4QbQBZ+IWuCVuhX5Gv+AZiqU4FFtxKjFKrBKnxCsJSqLiUtxKkpKspLAOShMlFS1T0pR0xaN4laaKT2mmZCjNlUylhdJSaUVeJEsVv3KZ0lppo2QpbZV2SraSo1yutFdylQ7kJbIMP4RL8cO4DM/Es5TpyoPKDOUhpVR5WClTZiqzlHJltvKI8qjymPK4MkepUJ5QnlTmKvOU+coCZSEJkH8xN0tSnlIWKU8ri5VntA+VZ5XnlCXK88oLyosgwbOVpcpLyjKlUlmuvAxc+IqyUnlV26XVaB9pO/Vn9Gf15/Ql+vP6C/qL+lL9JX2ZUWE8YTxpzDXmGfONBcZC4yljkfG0sdh4xnhWeV15Q3lTWausU6qV9coG5S3WUftY+0T9Uf1J3a/+rP6iHlAPqofUw+oR9Vf1qFqr/qb+rh5T/1CPq3+qJ9ST6in0JfpePY2+Us+o/1HPqufUOrVeDYLWRRrWiEY1pknoa/QN2ov2oT3oO03WFE3VNE3XDJC1luYAqevUYrRYLU6L1xK0RO1T7TNtt/a59oX2pbZH+0r7WvtG26t9q32n7dO+137QfiQvkxXyGrlWfl1+Q35TXiuvI5Vytbxe3iC/JW+UN8lvy+/Im+V35S3yVnmb/J68Xd4h75Tflz+QP5R3yTXyR/LH8idkufyp/Jm8W/5c/kL+Ut4jfyV/LX8j75W/lb+T98nfyz/IP8o/yfvln+Vf5APyQfmQfFg+Iv8qHxXz18q/yb/Lx+Q/5OPyn/IJ+aR8Sj4tn5H/o72o/aTt137WftEOaAe1Q9phvVJfrr+sr9Bf0Y5ov2pH5bPyOblO/VT9TN2tfq5+oX6p7lG/Ur9Wv1H3qt+q34EeJg7qYOo+9XuH5JDVH4znjCXG8yjNMvAz+Fn8HC7HS/Dz+AW8FL+El+FKvBz78ct4BX4Fr8SX4VfxKvwa2Eyr8Rr8On4Dv4nX4nW4GrfG6/EG/BbeiDfht/E7eDN+F2/BbfBWvA1n4bb4Pbwd78A78fv4A/wh3oVr8EfkCvwx/gS3w9n4U5yDP8O78ef4C/wl3oO/wl87FK1W+02fq8/T5+sr9Vf1VfprLBl/g/fib/F3eB9Lxz8wD/4J78c/419YE5ZCWpCWpBXxk8uYl7RhTUlb0o5kkxyWSi4n7Y3f1EFqkVqsDlaHqEPVYepwkks6MB9rpo5QR7IM1lwtUW9jmawFa8laqaPU29XR6hjmZ5epd6h3qmPVcepd6nh1gnq3eo86kbVmbVgWa8vasWx1kjpZnaLey3LY5aw9y1XvU6eq96vT1AfU6eqD6gz1IbVUfVgtU2eqs9Rydbb6iPqo+pjxgvq4OketUJ9Qn1TnqvPU+awD66guUBeqT6mL1KfVxeoz6rPqc2BDPa++oL6oLmWd2BXqS+oytVJdrr6srlBfUVeqr6qr1NfUKr1KX62uVtcYLxpLjZeMZUalsVx9HWzUN9W16jq1Wl2vblDfUjeqm9S31XfUzeq76hbjZWOF8Yqx0qE6NIfuMBymwzJ+N44ZfxjHHQ6H7XA6YhyxjjhHvCPBkehwWaZlWQ7LtpxWjBVrxTnmOxZYGdpS7SVtmVapLdde1lZor2grtVe1VdprWpW2Wlujva69oa/R3tTWauu0am29tkF7S9uobdLe1t7RNmvvks6kC+lKupHuJJ9cR3qQ60lP41VyA7mR3ER6kZtJb3IL6UNuJS/ob+hv6q/rA/R/6wP1QrPUrDNjzFgzzow3E/S1tItepA/Si/XB+hB9qD5MH67fZhaZxeZg0PhD6c3mOX0d7W0Oo7fQPvRW2pf2owW0Px1A/00H0kI6iBbRYjqYDqFD6TA6nI6gI2kJvY2OorfT0eZI8zbzdvMOcxwdQ++gd9KxdBy9i46nE+jd9B7wGybRyWAX30vvo1Pp/eBFzGDj2F36CH2kXqJX6+v1Dfpb+kZ9kzmB+mgzmkGb00zagrakraifXsZ605vM99kttJzdyf7NxrO72UToP5ndy+5jU1lnNo1NZzNYKQ2xMjaLzWaPsQo2ly1gmC1iXdkz7Fn2HFvCnmcvsBfZUtaNvcSWsUq2nL3MVjDGXmEr2SpWxdawN9hato69xTayd1gftoW9x3ay95nEPmC72EfsE/YZU9jn7Ev2FfuGfcv2sR/YT+xndoB1Z4fYYfYrvZrVgleUz65nPdkx9gc7zv5kJ9hJdoqdZho7w86yehaUELsBPKebjUNsoEQkygolJkmSLCmSKmmSLhmSKVmSQ7IlpxQjxUpxUryUYO6UEvVR+mjwJ2zmZDEslsWxeJbAEplLcklJUoqULnn0sfrb+mZ9i75N367v1D/Qd+kf65/pu/XP9S/0Pfpe/Xt9v35AP6wf1X/Xj+sn9FP6Gf2sXmcgAxvEbG22NXPM9mYH8wrzKvMa819mF7O7mW9eZ15v3mD2MnubfcwyM2jOMh8xHzXnmE+aC8ynzMXmc+bz5gsWsrBFLGbJlmqn2U3tZuZS8yVzmVlpLjdfNleYr5grzVfNVeZrZpW52lxjvm6+Yb5prjXXmdXmenOD+Za50dxkvm3pdB/9nv5Af6Q/0f30Z/oLPUDn0fl0AV1In6JHaS39jf5Oj9E/6HH6Jz1BT9JT9DQ9Q/9Dz+qLjTeNtXg2fgQ/ih/Dj+M50uvSG9Kb0lppnVQtrZc2SG9JG6VN0tvSO9Jm6V1pi7RV2ia9J20Hz3Wn1cryG39al+EK/AQ1qUUd1KZOGmO1ttpYWVZbq52VLb0vfSB9KO2SaqSPpI/Bk/tU+kzaLX0ufSF9Ke2RvpK+lr6R9krfSt9J+/SZ+iy9XJ+tP6I/qj+mP67P0Sv0J/QnybPkObKEPG83tzP1+9FqtIZ0VPLx5ehNtBZtxU+i19EbaJs+DT2I3kVldgu7pd0KvWL77cvs1g63I8mR7EhxNDG+Qu/ZbfCV+Cr9ATvLbmu3s7PRJvS2nYPbO1LxXPtyu72da3ewO6K37E72FfaV9lX21fY1dp4dsP9lX2t3trvYXe1udnc7377O7mFfb/e0b7BvtG+ye9k3GwONr+3e9i12H/tWu6/dzy6w+9sDHGmOdIfH4XU0dfgczZytHBmO5o5MRwtHS0cr+9/2QLvQHmQX4Vy72B5sD7GH2sPs4Q6/4zJ7hD3SLrFvs0fZt9ujjUJjkFFkFBuD7TH2Hfad9lh7nH2XPd6ewIrAVx6MTuJ5bAgbijuwYWw4uYfGkmvZCDaSlWjvax8YQ4yhxjBjuNXcyjQdpm1OsVpYLc3/OJ4hnRyt7bvte+yJjjaOLHuSPdmeYt9r32dPxQvwQvwUXmSst1+0l9ov2cvsSnu5/TJ6CtWiLegl9ISjA3pcn25sMN4yNhqbjLeNd4zNxrvGFmOrsc14z9hunLROWaetM8Yp47Rxxthh7DTeNz6w/mOdtc5ZdVa9FbRCDmR8aOwyaoyPjP8YZ41zRp1Rb3xsfGIEjZCJTGwS6ylrkdNvPW0ttp6xnrWes5ZYz1svWC9aS62XrGVWpbXcetlaYb1irbRetVZZr1lV1mprjfW69Yb1prXWWmdVW+utDc7LnK2dbZxZzrbOds5sZ47zcmd764R10opH76DNVoKVaLkst/qhukv9iNyn36WP1yfod+v36BP1SfpkfQqZSu4n08gDZDp5kMwgD5FS8jApIzPJLDKXzCPzyQKykDxFFpGnyWLyDMjbCSBfp7MH2YM0BDIWQZoFqRzSNEgPQCqDNBNSZ0hdIPWBdCvrCxL0Pbad7QBpPI/Nh1TBnmBPQsKMMAppEXuaLYb0PKQlkF6A1BUSM75j3Yx9IHslJkNSIKmQvoG0F9KXkPZA6i5k7yExp8VMNgCk73WsB72aXsMBJO2NIGt7mT+Z+82fzV/MA+ZB85B52Dxi/moeNWvN38zfzWPmHyDbP2GfQvoT0nFIJyDdB1L7NEjxP0CCn2RBFhIYKKNl5r2Q7oM0FdL9kKZBegDSdEgPmjMgPQTyXmcGM/RF+tP6An2h/pQpmZ+an5m7zBrz32ahXmYOMr8yvza/Mfea35rfmfvM780fzB+F9k0A/RsPGpjr4ETQwjfSrpD60zjQxgOoC9K/6bWglQdSt3EEdHMhTTJ+BQ2dQpMhFdFU2gRSMU2naZAGC60NetvYS4cZ34LuHk49kEZA8kIaCakppBJIrSHdFkmjIul22pa2oVl0NM2h7Wg26Pc76OW0PWj5O2kH2hF0/Vjaicl0HEujV9Ag6P276JWQxkO6CtIEmifO425hC3QGa+BfNGAcpTfQXvR62pM+QKfTbrQ7u9KoRUjagJKi35d+WEa4PnTw/Df/BEtCRy5sSU6GfpNqIP8u9Bs59k+xiYs/agT+6+cz8K0vw3GivBNraOdF72ehfJQSmhiqDv2ClqIipIcGhp4PnYq81RtK/NM/VIneh3wrwBY0Q+SbBEyG8mjIK8Az5/u6E61Cy9AiKD6N5qLZaJd4Dsd/Qgjh/f+45hpIJfg3VIweueTdcpHWQ4vlqDceha5DUyHNQOXwbhzMVoEmwfh1aFzocdIKjYE5l5BFVEfVsILZoHMr8Ke0AsYdT0rIVPB/viNNaA747LeT8bg36Ym8qKd0kB1HC0MlaCV6D5VCGoj6wFrewBtC6UEel56BNsK4c0OloVWoN2opx8PY3VC3YIFUjB5XislxlIGuQd1RL1SAxqPluBh6z7ukDqHW8FSMpqMn0QashnaHVoUq5N5QMxBVga/mZxvwNbhUKUaJSEfXs6nyYoWPVIx6og/xt+x1aQvo3hEAdwT+1af3DT2vy7+yU8cOue0vz8lu1zarTevL/K1atshsntHM19TrSU9LbZKSnOR2JSbEx8XGOG2HZRq6piqyxCjB6DJc5e5c0HVUVVLn4qpuvi4+p6eq243HbsiqQrEpXl9MTv/W4SZVkr8KxV1fFd+rYDUKdOxfJfsveH9jFc1wHvdCtxtSPF2rWAb8+XoMHlbVoneB1+f8LKXhfX/oUpXcucDrTakiGfB3HbyCvx6DPcOqnL2g3psSrrmuCvUq4FAd+qkjVDo7evunVKHeBVVp8FgdOgbP/fv/xQrXA7ltvmCNN+Jy5+puSZ27VKH41ajbT1UogTc61hFVoauqWvhhGU4owVDuKpRVheOPV+G4KpxwAyz4gvF5rx86Xrz7rsNG+boOuw2wOKz4PB6PhbHo9ZR7ynsXxORAUax2taF39nUerre+DK3WDSgaUIJeY1fjbtdgUSDdul6xmiDVAlzF8tV15TCqKjC7GAq+LoAkeBN3/k11aPMjjV8h6BYtxYVLuEruXKWIeT23VQUGV6HZntWXbS5/pNqJhhT7zWG+YYMHFlTRwbDG1YhmdC3pU9Xk+l4DoAomASgu8fBT7SK++Bl5upZ4yuGZty2Gb18XfrYX1A8rGV7MqQEX+7rAO61zQZl3c0pVLORdq2L8Vd2hWfcpP6fQ8q7u2zz8sby8zFO15OaCxm+9/BuO2g1LL+/qg9lgsK6jruXnlRU9H0Fz1w3jBzHYU/XAkFFh8hr8SJS4veXOqm6nvHAMcBDRXhEEDisexZc7ajDfYtdRnvLZw8U2HxHbApL0dB3VhQPvCASOboXeAwq6lvi6Ai5nhyeETUOBZlzc1+utSvLzjuXlXfn6Bg+DlYfXCy/OL56TfYofw3o6VwX6iAz1EfiHGQODu/SPVEUaDODd+JviLv37e8NnfH3vgs58P77BXVLCu2yoKY7UQEXX6Eu+Wt91MEKVZ6iHs5YPmnbkX8M7ovKhHQWuvP0x9Op1vleVlOH0ecpPIn6stUcvrBkcqZEznCcRL3bzdSsuL+/m83QrLy4fXB16YIjP4/SVr77++vKxXYs9gskx1G+YnVLV7ZH+Vc7iEnwFHDSnuW69+Ql185QMDouEPJ8X1hTTP/q619+9RkDpQO/AWeVOWEOVCQInxdONb5ELDi5MOC/C7LcWAP0PFbQqvoAvboFRUziH0P4ZXW+7JbJxoMIIsXCxdnOkFgbxejnvzK4OoCHwUPXAzQXhZw8akrIGBbL8cG7F/M3m6JuEW/mbB6JvGroX++AMr7/ln2i5MR2Xx/hiPZ2yxBK8EZHQuYCmkP7hEkmhvKT7QRxdVeXyQ5lk8O2DxCuH8/rEV+X0V0mdCzanXNXf44wBccVP+hbf9TcPKOgYPWwQjZ/43sdcIKJ4ZxW+qgon8noEAlJIaerqCC8bOnq6lkdJrYp07lPQeLwo7104x/V9oiXRoSrfH20Xfr5OPIdJd2rKFC6fCLp2tQ/PvHl1AM+8ZUDBeidCnpl9CtYQTDoXX9t/dTN4V7Deg1BA1JKGWv7k4U/oes4aa4gqXqWsDyD0gHjLRIV4HlqNkahTo3UYDa0m4TqnqOMKSBg/KBu+msv8JkhBKCfGG5PhjfFm4xPBSbhJ8GcZnUWjpRbiFrvh047frd0b7E2Kpd1IQ9cEmssMNZHUBDVDpVT2gc1AQAuU0oANBjPRqE6HE4+BC/24aFBhstt5Q+22osLsokKUl9euLfahGCfOgS9SPD24GA+bHvwi2Bt/hI/jP/FHQW8wLZgO8/WA+bIi82VoTGqiK8lKK4VqVGsijZYIsSWsS5JSSgIGmEvh6cRcn8OkRYWb4VvMF8enE3N+iIcFF0/H/uAX0u5gdtAKOoLZ+Hv8I/4Jfw8zrg1+j2eA7aahXgEHaatZuex1ugapVQoFvRVopjlz5dVUe00N2GqeepNapDKD6ob6Wtu2AeMBo8o4ZrDzW67PqfWjrPqcU7Xt2vq98bKvfXZ7X/scPKNk+PDhNTOeLX63Bua8FlfD4sfAabQLeB7HGGeRm0gRoYQbX2QsPGOMJDxWwh4JF47Lcm4vLCxEWYU1hYBIb3svKan/hjTH1W/y9cMx8fVTlBFIJFUYvYYDTMJ8edIDUpV0TGJRFKEsWJVXLKempiZ82qQT+AwU9Q5cTjHCbxAaTwjNQJgiGgB6JT3AjAWrFZYzguCu4PGSNgRT8quEsyRMigoLswAKnZv5+ooK84pghWVSG3/Z1G0Y+zDpFCyowMulDWe7wTRAhwNDB5kLzldBcWhQINbx9JUwtNOw8xFTn74c3FuQAIGW8MzijFmkGcEWSSUkZpYc8MuYyLEycbAEch+Jl+8lSxLg8AXmw9QGy6hFef7CWg6AKE+M0wuAs8G2JL6mJMYZSzrhfcGmQR/+bvacObPL58yRdter9SY5Qc6cXY5NcUloB/8IvhJcGTzB6X8/ZxlYr4q6BPxEYU9LAdOZKykMPwSLDTicuUzBsy9HXdAEaIh0bayOyRIdFxaNi4ntlFVYm73ND5jJ+9JfG9OJIz8GSMITl0PRxrt+x/1zKtj0nnOxDjPlgZemw0xuND6QZSdiOyE9IS/hpgRmGCmG36CWrmDb0p6WndjpZK6n8BwXtl3pLhJLXdWhnYEkw5nrwm55Vhw2Z1mB5Lj7SJIFSEoWSAqzo7N2e2wnP0cYkELeoMLamnZtBxXCY6HXe3kuAju9ua+prPhyc3OywSiXkeJlel37ZfitpY9Mu2OxvtVZ/9He36rqT0pk28Mn8PAvyibPfmTmuwd/Xb3kZLAnF0AD4HxTYBfN0UuBUZ50bDVJbUJiXdiKSY0hDlsFGsNOhuKfTgh45YSn3W0DaTgt5Wkvu0nDSMOWJlkJFnFYT8txqFmCe1ZWM2w1S212dTPqpM1SZ6UlpDWjmnMW1WdpgRb0PpKpwRZbnN+iszbW1SmrNovvsTYrthPKqx1UWAMPebXbCv1ItIFz4K/5tgtxIuyT+Zo2y8yJT8zJFhgABvEITJxHBGUPVwSPfPRxsE7f+sTMh555cXG+H8dOGvND8paE46u3HRoOlsoHMz8M/rRh9OIVZeXznqy7857DG7Z+sXxQJaeivoAVLzBAEzQvoLV24dS41nEkpjr0SaAXkHqs1dQiV+u4CUKPupPi3Qi7k1zInbAIcQZUmzyVRG8yMDKwRQ37aTUuOQm5kpowd7LbiJ0lmbOMQJp0H0k1ABNpF2CiU6csV05WFuCgpkigQFBkTSdAQJnaxi9NdW5DhYV+wAJsEyXEI4GHxCgeYP9x3gQv7jHmxXXYGfx9/4bgmYStzqdun/nyylnPjJl7DY2tf5zeXRCs/+qn4J6PdpVO/2jhojWTqbyKMw3fcypQQhxKRS3R+oDTRukgAHSnTrxu3Zkbzzl9JBBtig9bPpwCtJKOnamYpWJnAmYJWM5MzCSO5klYS8J2oo0lGzvBa/Yuig/Y8ThJtqX4Re62SU/bLPNpKS5xVmyTWSbSzVimpoHkmKUCv95HWqmAFf+l9AGCojYPRHVUU3XqFCEJQRXi4/eDDAHcsDCBtL88o3n7y8P0IEU4JSEeX948U4ac44wcxe2CJ7/+KngStzvz6zubjwT7Hrp3zOh7iVq+YH75sw8++Bwljx5b/8sv6489+vie53btem7Pv0qqht911/Cq+pqiWZ07zyq677HHuJQUMp1lCG19WcAtVckg2CXGRbuqcdHeXTutEVQUFeydOiG+p7Bwj4kK+JoaMrsGkVAQIakAqA+Qj+oDbZZr1RrpB0IcSYyBTFUUjFkTTNh0qsdTqmOlCUbKdFWLV1VNrQ4dCUzQknJpInwt1JfrhEzRMe2gd9f76SN1hhXdpWfqlCzkw9KJGr5C66EN0EZpzK210GZqlOga1qn8ZEAhLkIMBJrbkK8ELaNqVDXpRGKo95AO5kjztEkLzEkmUc1qk29uHBwBcmflcTrmQiv5Bi7VhZovBIgmv7+sjV+NWF/ObaB93I0eFcl51VUKAIZeGFPso17qwzl0UTB4iq7du6J+0Qs1JOdTUkCW1xfUL8dvB68VGnEpyOOjULJQIkpDWwKdO1r5FlENrKg4hhhpFY6AxBwV7raJFRJ1VbBMhNVkdzKxiMFip5laE43YVHNblnylZvIdaxoot7Nv8Aoo1K3jdSx5KqoOnRCVUDiyllciBwj0ukASr3RNYwEPupOks9tJpQfP9BzycNQ00LM/2+n318b6/YAijiNg8ah4r71Q3BXieOblRh/Qs5KYiL1hJocaIey87GjwUPBk/V6iH8Hm7scerZuCuz2/cO664C48GA98/vNgNV7x0bfShp2rPh/R7F2cNXLS6DHnXqibMRnkW3HooDQIeD0JvRu4PNndyn2Fm0oxWLdxgoppdw0D1WrWEzJTXGUuIAFX3BMoTnG6nIAtXUuc6oriwMWFQhJHgktyJbiI7QqAhHBOpSZ/CwbZlwGBSxpQKEgDqPgpkKoZuSmuO0kyHU2qU3C/FJyZ0iGlX8rElMoUKUJGUfvM7weUDSrkOBMcHxGN8F0Ihgunq0LJA0Yj8mYjl6858Dh/AN7ukCMjqgffDz6Kx+MOv+NeCTvNr1/6PliHyXcrP3fsTAhuSMJP4UJITwWDu2cvDdYE9wd/Du5avvhISFjgQE9SMdCTjGw0KeAGzpEI1uwKRJUKWTXcBqecqbCjujciez0iSIQq0+Tq0ClRCYUzgSa8Vg7E0DuJU76d9IvBlTHYHZMfMzOGctIYF6YNvlW+T75LTg1c0xfGcYPI6+3g9XCbyOtdij8lv9YnBLO2YAPnHToY3Bw8KW2o99enrMItsQMr2MP1F+eFw7B2Aw0JpGsVMliGMlC+VEGoXgEM4VKJjlSGZH0ajtI4FIKBWL5WHLCAgk18OzlkYS60GtaX7dy2Lbq+CLWCDAOSTIgAO3zuLeare462rNtDZ0gbVgUDq4LOyIqmw4o0dFMgBtZBFFyhUoUxRORpCp+6CV+EEkABIzHXgPl1BeY3cKbRz4hwUHgZsApQi2Cice3IBUx4ah9MPr3uF7K6/qYt9DvJDF6xqr6E27AYbYSvaUJGDA2YBMzmaZhFdlwXsMI7Bgt8mretxD2IVr4WuYrkkjpII6WJUpkkd5f6QbFMWiBVSuukHdJpSVso4UkSFsTa6PhgUcJkzMHTtm4Nz833nQVFCV0ZsAllFRgFbHcuUtg0MX1MeHoZkF2p4JkKFluNbNS53c9RLVAMPiDLqrt7C3kfjPMT4ZFlGUY2ccZ6REMnAjrfk5EP/o9WHaoOzIfCI8Zig8zQntQIKWEY57MCNolRMpJi3J32oxMpJW4dY0lP0DNAJczUucIAU1f/Vid0J7xJk9pIoCQiG98jSbhUmicRlAZeBdSTSrKO7CB7CNSTeYRIRKdsMsMqc7MWrCNjGjJUiplikGlIjwjNUwFNSE1N4gdfF/DwJyXgQHcQC858ogP3cyxwENXhdnR0HHKwxjzCSRB4JBtl5eTk5XAxCeZBu7Zcr4wrOv8tcnxe5XDigXMBB4fTCcZeWa4ODloVHFQNVY/ix7GPfX/Oy0HacO4Ktj1ycpJPUOy4gFsBy266rMTLsiJR0KHTkCOym7OR3chkGmiJM1F1EWF7FjBgWzoog4kGnmTgfOOQccagESl3nq3Ce4KTLhOumFcsNAfH5Ei+HfWoejtF61npuXthdfeyUi6duATfCxIcZDPatFrmkY2AHvsEYuYTcpwNwui3qFT6IeAUYokmuRM0IZaBPL5cF9ZwSW7u9fj9NZFMWFnt2qYEesQ+oaNk1AqYBoaU5AQ5Q6YaDD2VJkzVAiUUTIRkegXtQdlIDUtagtZe66oxnSaBmHNro8mhJNwvCWcm4ehehU4rjBh1iDszedzr64/D8lt842z+HRbiNB1fjmcGJwV3BT8M3oPLcc63OCl46Ovvgr/gZGl3sDi4AlIxfl78TnTJ2Q24I3ZD6hjcCYrxcHBHWIazOiEHE9DUgGVWaJqRABLccE6VoiiCwjdhFQYcj4kkJUjEolIA2MfUpxnRMzU4qbp4MyPgku4kiQaIJxee6Kp0NVLw4jT9wuThZ3peSsV5s4XnApwMRrpQ5aDDlxLfG38+FjwQPBl8fMsWPP6t5wrKpgUrpA2dvy/fd6B+LTnzyEMFM5tFtNEo2Ek8+CR/BFzdzX5ggFHSkw6khBLcBE9H8fGI0122IEcUXx6PL4u/Kp6o8e54ohHJkeDIcLR3MJo0VXG4HJmODg7mEHLQAdo8kMy7OTBxxMSCH6mCWzsJUQslKNPUKAp4IZDF26mBNJDQqertpCwGk9gYLaFigYxlWW3yhEaRmp+GJ6bhyrR1aWGzELg14suAaVhU2LYRrgBVMeDz+P2FtcConESAA/yOBnOwHeL86/VRji7OvAKNspKQ6ApbRF6vNOqz8oyVY4NPbSGzVnw1s+qpp3rVPY5Xv/licHuwHS4reaj+UWnDtZ+XLtyVztp9hZ+465GznL95ZOMocJAPB9ajVLBn+MaacFTcw0sDmmJako5Jv3QwrtMxbp/WNY3QSamYxKX6Um9JpbRjcn4yIS1cHV35LoomxWNyazy+1QndHJMcZJKFsWphfYCJCwxMMoA/CHVJmUBkFI0EcdIDLHrdTvEyVJKAE9wVsZzAEjieY5lCuXgeScuoZIC9wY0sj1CPFSgucVqCMKkSUqbZomAn2FSf5gVOF1vwitPkrb1sGg1keO8kzbillXEog1Rm4MbGqDBFubFeC3/A+n6u3PP84HkLqdpIfgobvqixFd/YngcT7Lwb2gy8rmY52czlI17ucSUyQfikJvhBsEZ9y/5z5Ys7Pgpqe3Hq0TO4rbYpNbj6hTWP47IDS4O7k3DiN0ufeXj85Dtuf/vZNT8f+AzrU4YPHNi3z+p5m87CmbUGfl4LXKCg6wLtsFQhU4qWEGkJmUYFJVNwt+i0tm0Pazhf4w7OGY0pmkubqO3RhB5pUK0HnAc4c2YL+wHIqr2XrQ2mbA2mM/DQz56Q+D8IAB0AUpbrgDjUP+CZGIupomNDRdgkDgksKRTnmGYLRWDL0ySu2cT5SYEE+w4SL4FKT8D5CY1QLkw84cpy3TVImLBS0+akPah3J8dfDC/IjOwP1XfesuWtt7ZsnRZCSduxuZr2qns9+P3atdhLb6irCp6qmwrrM0Eq5AsbdTO3AY4ECsHpQ9zzm4QX4p2Ykva4K8jH4ZhRBbtwJh6J2USyAHQ2JVcACQ4gowijKnGTFqSEMND6lKhoLFHoGNJBHameVmmBOkklqlqtNnbxon5dhCT8/rAKSgkkS3ITicnTEY4HBEqsiSSDn0rATyVlbW74e7evP48+5oApkl+/fWf99o8xpHAU8pyLHeZnocNeewpZfiowWjU6GiXGTGOhISnCUFsgMZBxTOLCUAb3RTV0A8gE+MxABjjJkiypigoGiQKvpyMFVqfwCPh0psXzWjlTHilPlJkKdksJGEkMy5TpgbjEXJnbhB1jE3N1vZUOZnMrRCRQhjBZBiMG1WVdRswCnJlsDDls4QILL7eqrTMW7S6s57D2A/3ayZ2VFVaCgMOsvKLCcOj1Bq7wywAZPHOqm9XNl34L5QlGAeCIIykHSz1/C94Y7PkbyQlKtfhePLU2SMmY+icpI2vrfyJp9T0AYxnALSuFBXN7oCmF7U+PRIsV3AoQo1DgH6JWBLBKiaYwvs8UMzGXEVXBYPAA4hQww8loUmacFjZ4RIu7wS51bo/lQQxw8bMFF8FuImFkr7c9D3knYC9beW4yfbzuPZZcN44uX8v6rlp7bgXoshtDB+lBdjWsy4ObBrLTW1kxuUqqKzUztUMqIwuSK5PXJe9I3pMs4SnxmK5z7HDscRx0nHZIquW2CHim3KgxoBNtazlz8YJYvvKAkJyBWDPenavGumOJhVLM2AXutmqSO6lFUknSpKSdSXJS4vwUpzLfTPR4vDCAuzT8luooiYtN4f0nCamalGTHYk9purCY0qtDh97ktenCtLOEkTfDjhesXx36OeAWgjjQNL2EeO0RpF9TXNkU5zfFF/l3fmHoZcdGnX9OB7XhSCc8+sMSol1b8TIiXbEft/d5wgErl+xr2ry905vDjYemii8WtGFCjI/6pwdHTBpRuCp7zv5nd5HpA913TKpTNybgN3f9vBG7fv3+5n59hjz8ShmecNfILm/g7i/U1vfmNuRs0IB+oA8HSkKbAt2XJ1UnEZIPFltlwroEQjLiMC2wsa4nA+FfoQ/QR+lTdIVMlMvkBTLFlWwdI8txNSau+cRpoZj5aqIZP0OKhgag8FkgQdhVquSWWoCgYhaxkVFqRptA4YNAIm9iBlKk20iyOZzkpxxKOZNCL3Q7uZkQtafyuIFaeF72CCTl5mTHJvBLg8wEsJtjzkdEcf79qz/5ObRl5T2bNk1+ZsqcmVOemU6XLQh+fS54Lvjp7b1Bvlw7/dOPt33IQ3goHyjzF6DMJLQg4G3h7ugmmWoHlRDNmt8D7BunK25+V37VkqhriTNcXONGgiCfB1JEEKS7aySYg5IT1mJR5wwaGzHCvw2kRsMgHYRuZzEkxTWKJNMR5EwKkMm4C0IeDQYk8BgIitqaPBH6hx1LkQBHYjjaEYlkdsiRZdLpp+Af2P5p1eGELaysqGz5cvhiWxIOJ4HUULGGm1YtKxpW8+EHNcMGLavi5893exDOH4gfrVyP4oGrrgSGWm5X20Q13Sap1NfpZLlSrYCZI2HcD2PSHWFM58c5XUibbyVZiQkoqdQtdukGHgk4+C7d8gynk1c5OaNm8ipngEePMp3dnROdZc4FTsUiKe4SkuwcceGBn0dBmFNQXpYzfOINpgjX4TENW+8ALEHaO2Nzsl1w7sAKU56ZXDFr8rPTxbG/u3Lixo24HT4jjpgtxC3OYYazRuGDZ7tFMMD8cN7xKAV9FdCSY7DqiXPlmnzdXQ1HrhqHnSPxRFAi81MCegqOITJKme9u28M9wE3c88tlPFzGVHYqsa5YoqFYx/wMxC+HbCCRmBmxCTO0qJcBhX0BWzhdKcYM6ozQxb5A80voIlW7jTQBuuiXOjGV5KceSj2TKhzGC9AjXMbPwZQQFyciJga4qS08HyEHJhnEUdpAMigmwZsYpZY4QT0KufFI8Gcct28/tut+NF+fMfQ5tjWxvti1ZMK4FxKTcAYoS4Yzg/uCp1tXrxleuHQNeeKphQsWcV5xAgKHgP0cjyrWIzX0ZeBmzcrNdIIHMU/DsYjOl21SGb8unnQwuhuVxjrjIGgR2aDxsTIqnUSxQl00k66jO+geKrMWFMNfP4oX0ErwaKLhw7fDESk6KRF3T6xMXJd4OlHYcdxhGFc4rqawsL5TpxrnNh5BzQNuEZQyzs+9Z1/7HBCV2a4EQSsJOQm4ZOPixbjlv7M7XtG18/u0ou5OWjF3EO5lLtVvKBo4V9BDsLfQTG7wCtLWJgupDo7fIWFYm9FCEiePK3mp0rfOR1qkAVuA1FzuqfYQcmvC8ARClzurnaTSWmcR0l3HKBJg6YrxFcA+CeB/vw6Mxnh41A2WRZP58U633XS+IxE8UCTlWU4hPjcH4vSYXEmdT5okJZqlyBkJORwKy0wkz5ACMIyUPiNGvIppYLeYgBLjismM6R4zMaYsZkEMsFsGKiHNYoDdwA84k3EJu33ub+C42m2gfwojgpYXLxK2hcgPnwynF3RRTjyXtO3PM2H7HHqeOenBYPbOg1uPPDTnPDdG+LN3EK16clXF5+0beLJ3vRnhU05dsxGiPJrvRHcEWj+pvqiCtxRDx9EK+jyVNIrjQKiB+abPQw7itJHskEslFlE7B8IhGQlZpQ6uYYRQcgRipRIS4xhOLgwlcd80L7ZTrQi6DxJXa54YIB+hPUQQJv/RKjxyU+GoOXM2baLPPhecXN+DrHrwrhX1qrijgZXifBFdVFDOGmmhsBdFyEVGxMZaicRQKa4OfSLuCvBw1XnKD0TL3Q9wtcLxQvGTg/xN8GEZ7+8CSgxVB/uIUUGvosfCxvUae6GTjy0O34lMW51HmzhAobJIfOIzEdQxjLjYSDTnExHPEbOkBNqRVpozF8aI9IwrkQJJKblSAAhQ0FEL6YxEJNuBSs/w28vhsc5TReNgpdt49OwAihJEfykejjuzvdCx0aXPmX5Ddm7ONZfBDjr9/Li+xNHvITaA7wQ3wk+LdQRcghKBjbpozDmKFgZo8dcfOB9CBXRAL9FfzgCeTEVvr0dNQkde14xcF7/lagOFvk0ebkLQza7pLuJKwq4ETFIka547PibRGQu4j8GlaI7qVuOdpTHcvRYCOGaOWqqxaCAs0EEIZX5PZ6C0dK20bdu+6VhNz09fnv5V+qF0qaPI6Mj0HekkM71f+oL0dVAMX1SMG9eYh04A/8TkOE/Uiugkvwar7dQpfEvBuc17nqgaiCsBDBdXLic5OWNTz/6zHtrYc8CsGZs2Pfzi4cCoud1xN7LqobFAaZGMZZQ9t3lq5+DkKIcAXpzo7v+BQ+yLOMRu4BASCQH9HZ/8v2AUltFY2/KY5dMBPXY+jzD2AMLgIu0yIdyc5nwea+wqU4vKwhqXE1Xqpi0o5X58pgYOjD0DFMOxaHjzk/CyKehaZ+RMfwgbY1ogid5G3BpYWEk4HzyAQ0lnklhEk1544VKI8oQ9HvkhSqMoZFx2w9VxIum0n//YZH/YzuIGFgC7mmvH4Jngd1Ezq+6amg8+rOGGFpcOwRKxZ25jbw/0TBKC4b9Y2uxCG5tIOAE0MaXCzraFnZ3y/7ehnZd3kdkVLSB/I2M75v/I2A4mSpvq9kStba5tS4S25bjZth7ZEfVqcK7szg1RuVomlWAiEE4CoGMrk9Yl0eWJ1Ylgj8aDPQrGRncbcmOkQRw20JPTPZ8l4hTbxI21pSAQlDBDEaEahVvjmeE7B0VxKZlKd2WiUqYsULhRCloyWfmvRil32vLC1w5RoxQsLz/mvxb6G0248RIVGCwhrr/XfmQNYMZCPQJubjwQk5B5hlOxZU0qlVnkbvGTQHz4btFGdxOHPJxk2utscuGJcjUT5ll8gdAha1pcP2fkpk0Tn2nRhk42F9THsoy5K1U+ex74BVUwezrGPKp0IHA3n2Vw+J6WUPGNJiZzq6df0sikiUmMNpTQ+ThziYnLDExaGB0NgibpmID7nspQbMJ8p5P/tGAkmUjKxO3RQXIarGcizUeJPA5ANBJXGuvmDrk4RHepFWsRUSQgu4SWI2ppalQKpPITFaH5VFpKAt7U24iHjCCZ3krveWREop1+f6dOWbX+rNrwZfI2buhsaxTtvCCyCc635KE5id6GwCbIvDhf5FQZD0QTOXhD/CaC5bvvnRSc9eqeN/Dz2kZ9T9HUYjz9rYNJuO+WwdflXzlowhMPvRrcMz+/6zUdCu9bWPEOYNkXOk4ypdYoEU0LdKVcN69gmLYwJhnEcFrKfN2On08mJmK7MhEnWrqzdJKCw/RK1RYKhr9KZZ2yQ9mjMMWMEPaWsLmsTHLj7u5K9zr3aXeDuRy1lr8s3B6+wAXDWQQg/RIPKzRvzy1mYOcc8KCFZ0Uyr7luwujkGTM2rlzZoW271sS85cGxZMBcrAbPzK1fPOZKFXZRJmI5GaCF7g+ku+G4F6srVeKgafR++ixdBZrIxE3wZVwTcfs5HChBxJJspz6PJDpBAUXvIA6Jg+Vqqjq0K8y0YJw4QBlJjYy28FVZXm1WXk4snJo/rIyKCjNkn4dHT8XSw67go1XBpzYNHPXsjI0by5bgmWRtfW/QRuTMub2cylNAf/4CK5fR55H7sxiJEtmmNsFEQrJEGSzkB7EoSVIVYVN9V+OEv5oazlUxfP4UkKci4lnWOOJZFo54ip87dOYh2TT4GkSwTpJJD0JvxpjL9K6YGhSmU9EoolBgB/W0KqKuE9UytVI9CI+KCtYLiUSRzgdeRQzxf4qn0l+Cdy4P3rkLJ+EklnFuL8uo28L/6x0MUhfR47B/A4+P7D+ZSIbKZKbZso2ZrhHwDxEW8eHojSEhlikQsTmMCfGHsgQ2XAIdT36lYsJ/YTBRFXHlSYTiDxD+QMfvMfyezCkc5xsFQOYLDUb47yiqDYozJcxv2SfyOC7hl+/rJKrh/zWeW3Y+nlsWjufKNkPpYB/q/Ask5Y/cagR/68dALK+NxGzvJiabQCwwql+3YnJ5HrBdKblnLDzSmmiRTBG/BRvPfz6Ay8vhi9r/PXILYxSFv/ivgoWM6R8N5sbF5WB6/AjYD2eO4JnBBUcOHz4SXICfDQ4hmTjIf70SZPy0WsJpfQGnpaGJgdaj6BRaTulw9W61VKWKyn9ZA9goC4d3FToPXG91XiSuy7WpCD4pjBjKbUTnohFc8gXglAuXPCwi3c7tTv9FQd3oD4OLwr84jcZ16Rd1fci8+ifosPrbydol5JclS+pTQqFwXFfaTXJRF1iyCz2KDiCE4gIOgny9fMU+OtLntdMz/IXtEbQW1qBonX9x67ReacVpND+tCdWjrcP+4EZofbNovUe01tdilBxjSxn+9heM2LvRiDEBk6DkXskkP9mtUTEe4LMEf0yG0Q/BFi0M/Ev+REUtwCBRwMAk9JOIM2Ba2idt256w8K0W/sXCay1cZuEpFm5pXWdNtqhqdbRKrOXWGUsKR/kLG7kDN9Tyn38LW5JrCR62vTwXjyiZP78EgH4oMgD+i2W2gfSRdoMZfE2g1SSKJ2I8ic6kCymlD6jAPoTMRAvRckT7oZGwxonAApnipyf85+V8lmTxK3NQVtQXR/p8N7dU2h08iJM41sSvBJS2gJHsMEbw+xgsOMCxM9mTTLoLjJBkzgCmlcvzNzU9N1lqzl389jiCK5l9QZZKG8ETDaxHBjBJc8OZS/gPvWJk8CVVCo4HvzBugTrCWUpI1VrE5cdxqZVV83khiIfC7d99W7OdBxOwwAWPXYtfZnIts3RCv8vXpuVpMyK5tPH2KX36chD/VxiST259ZIG6s8i+6iRKCf971ddOXiX+jejuAZ6ccwPrjxvvqwo8atA+/C8i+H/oVxwEKjD2nRt4dorx/oX/UgIhWH8Bxwn/PT2AR+RryR7Uh41HLoAblVQ0XeqLCkC4DCQr0L0APWgqupatRONE2xXoWpFDf2g/EGA/QB7AAIB0gL6R+iGRct9I+7V8jAYYjwap6Wi81DcUhPmWSjtQMcBSXmb70VK5U/gZ+m0ESdBB1I+H+hWibfh93/PtoDwQ+rWOlE3lUZQEuc4B6jNgnNliz52Ql76LerBRoROwF76+GwFmwxz5kAuANs5IeTbezSFUDe9FGeafLdpH3ov2K0T/POjng+cyKKfAOnTIHbwM0JKsRJ1IPNoIeTvYf0F43wA7wvto2NMOsc8b/xIarTEKsKbJsL6jACsBvmi0touh7CLoRXNQKeT8LNqJc6pBJaxnKAj4mi39gpCASSgb9rcfII+fG6dEWGcvKA+86DwJn5ueQDnSG+heeT7sYxjgu11oDTmF/OQoai1noHFAXzdG1jOkgSaGoXh4vhbyHI4TKD8q4GH07yieOG5g7mI413xSEDrBqsT59eGAd6CpHPhYMH8bgX84d9w3yOm6J7QZADAN6p0CYO9RmqRN0RwYN6mBDhtyQXsrSafQ95CrAOl8DVEQdBYBThcwvgtABkgFaA1wBuAFgFI6BT0j+GaF4DEV2puCXoFmOG0K+gDagLE4DvMFzYb3sFScZxjHy6D/KIAkAK+8Ek2LQHsYg59JcYR2N0bH5rTFaSaaC7wjwBvsO4LnGxtyvpc40ZZE5s2P5pzvYM3deU5uFfS1lPyJro3Q6uxoHl2D4NsV4XrI/Y322lrwCM8/Qb5Iv7JoHsVFQ74qzBPyZBh3cXT9AKtE7pXahffD7oKzPIV6aofR49B+I10Gc5cgL8z9C+dxLtPIfGSzL9AT5Es0gwOUV0M+ttH+L8DVxTiI5tE9Xpw3wgWnZZAzoY+AH5+G/BfI10B+EvItsH/Os/FcpnGeFeMiIT9mR/A8++/wegl+L8Lrxfi8OBcycUdEJgJ9EcTHCO+f8zXnTcHbYTpaG21/cR7tL3iO4z8sP/Ij9JgbgTGwxjpO/wC/0Xwuw4GPu6EceR3KoS0hXwR02w4AcpBFLRt4727BH1wPZER1QJQ3o/JfykNjGsn+pewXwA2X/xGZDzQzCN6P4Lwr1rs4Qjtcph9FkwW9TQec90Yp9McGWT4Q3s2OnEdKRC9E3+ezCnjOj8jS1sghlQJuSwFXvZEakaniHa8TumkEas1mIW5DcBk/kO+Dr4efvbIK8Lkq3EbsLR3WuSMif66O7An6yjlwrlBW2oNrFo8GNMgd3mcp7HPFhbojqksFLmBM+UMxty7VQf1kNFH+EcZyQfur4bydUHe1WN8g1RT9Bgo9Mx8NYnkgy52w3xVCz+dLKxCDdfvYZKibHMFRBYy79hJ9KHQpXSF4OEVeKPDuYAuhvgucySig58ehfAp0y0bIs8I4lk5A3hdkWXgcJOZGom9eRAd/wfWwfBj2vFDMgy44J+AdWg056HWlDj3O64DufFxWAnBa9AreivLM+H/8fzQafULrIoX+UP4M5DRYEaF8/DGqAIMrF6Fge6HHakCmPI6upANBxrcE/q5BC8njoHtqwmWAK/kzvMsDGACQD3W9RH0N6gFwZeS5NUAZG4HuZFdD3QiAe6E/gvPsJcbpA9AhCsDDxXQcnPEIMJHPjw8QGiGgJtyvMYi1RuHSNV8fWTNf7/UXrffKRusMr/Ev1ifWAeOKfrxNDffjQt8CZITzIHgzocqLZGcZ4PhjgDMAYAQLmwN0asiM6FYW0atR4M8fN6r3R4DrR9cl+u2CPHT8n9//L/lf64dG+USY54+/ew80sxDocSGXCZwu/06vhPPQ8X9+/9/z/6Yb/jY/b4tckPMx8fdoDcDdAH6RnwiVRoEVcAD7+uJ8D+QA8lb+jNsB5EH9AICrAe4GyI9AawA/wMBw21Ap9QEsCY8VLZPeoVIyCMrDgYei5UxYw4ZwG74WuijS3gFr5mvbiNJ5medkBKoRdQORybai9o05P1rHBoZOibYZaCU8iz7cfqU90SzYdybAOAq2Ls9B5qZDvkqqBhvzXnQ1zzmEcRAGUo10DqBXenMAecZlmq5Wh4EthfUsFWvqzYHvRe4NYyxHmcBLOSwe7MVq8LWuRjl8Dj43n5fDXwiwlEZlvdE++f/Hw/dyGGAIQF+OhygO+Zrp9+hJhM4NvBCC0K3+SoTqekKuA7QGXnZBDpqifjm8LwWYL21Fg6D991AH8587C3UgquqPRfpN4/3YINAz0+CcVwJMA9wASANRa9iPDHP3gv30ir7n43GAMvc9xNqkceBTeFFfub0YR/QXYy6CNgw9CTqmV7Q9vJvOQeoA43ZoqM8CWp7Ox+L9+JyRsXuJtcRC3g5gb6QPL/PxEbRZhHrAXoIgO9PpwOAQKI+/EII/hnEVPEzTQT8E0SKSD/oJdB/0cdKBofdhHsT7sxmoF+DDCXhMgvaDInh8l+MRYAj//69g7iFAg8UAA2EsB8A0gGIBG0F+B4FOw889pEzUS/6Un/H5NXAa4mca5s0oLQJPAP2xd8EeA56J5pyOOL0L3gE+4TT2F7zKoQfA+415tcEWjco7Lke4vcBpNuLL46hPPQtdAXq+rfIw2DYfIwY2xlLWXehoYRNzmwXOcvYlMuglyK8A+wvsGW5TQP6XdnI4aiL0TmIk95zPgWci+IjyIucnFkDDxL7rAR9Q5iB/jeIaA96AWrAnw3ERzpdRnokymXx3GGcwz3eAe9BrwXpYz23gkwyMxDmK5bB/hLQysMcehbWAxuO4gzW35EsDaAU8eYTzJedPeG7D+Z8HcSK8WwJyRopAq8ZAHkCrANZwuSBkw/3h84zu93/NG9PJ/005Ku8igMJ5qC4MSI3Kwb8CLheFvrgo53KS64VoHpWXjfUDP5vG5/O/lIWMBVBDKE3ZHZrcwBs1f5O/+z/lIzj9cx782xxoT+Cc02Cj8iX8GjnTvwPpTyRJcwAa83BEPwgaiORAk92APu8A4Hbt8/C8Va1D2eqrPD4YmgnwGkARwBP8HeQTcAnKJrv4++AL8DwAcMDbFkTgiYvP5uLz4PqS661IjKQx5EWAXQQfRGJPrgiI9udtqQtssdmX5Od9+qWNbKC/z/+7TReOh+wXPqnzL3z/i3MeF7sl+nw+JoAUyHc3yLmL8mgcIJyHVl+Ur+Q+H5cfF+f/sI6/lI+X2IiX4q9xHCeaj4jkqQ1xpv+SXxD3achDtZFn1/+BLWpG9IDI/yYu4bgoT7kkrhTNxZkgKxKr5TbtjTwmImIF/wANMUmwfjhIP0VgcRgifuslIP0K7wGUthdCg9/8NyCD1cVBef4i6B8GHrMIQ2hvGBANQ+hpDoT/7ASAnrsIXg8D++t4MacLHldEyjcRuCcM7NJY8gUgg4WnTAL4AOA62DOPh/wD/N08Sm4YGnAfxWUEL7C3X2GfkxqtWcwfHff/+hz/L8/l/9m+/2HtjYGswBT48j+RnMei5b9cN1iY8g0A4G3Lb8M6w/zcDeAO3gfwugVgDcD9EQjTUafQKYADlN/KuoGmGvW5hA4qRHxodsOZRONyy2DOg2E+4DFBDjxG+1f4UcAYksEDieYinif0AaZhnRTcCbbyCci/Av2zFPIjkAP1BZ+N6Kx3AK6OPH8VabfqPCDQjKHuPJ4D0CUM3F4TduOy83WiHuYKtQ6/C/4ankeM0z6iJ9+J2HpvRda29fycwdWRtfI2BRc98z77w/PxsphrA+Q+gEWR/a0OzxWcG9lnWbie7020X30eQjSy5luiAP7rb+DzJZ73Z//HvABNBrgKzk+X6uHMro7ohauRBTkFurmtkY4l8h2izc1RfoTzHiPFo1Fsf+jTyH2dqTQL36OB3I2P6jscjoengA4rlXaEPo3qKsUBfZbAOPVhXozcy4zi/heP99K+4j7Px06j+Khe4XdMIh562UWyfzTYPaOhz3aUD5DDwneWWfRl5GE3Qt/RsCZ+PxePmkufoXd5TE//N1qqTUJL1Z7gSxTDmD3QUjoUzeZ1yjXwrJyPn/N7ItDNL/+fxlsutGMuicVfqp/RpXqc7w325Oe2QeM1N/hBUMf312AP/JdYFuxlBIx1DeRnId/5N/cPciS/LzLuqIY9EXQtndBwHzFb+GfL0bWsG+oUPY/o3P+DTRK+qzhvK7qitiK8f6RBJkZi9QxdqM+kvQ16wyf1hnPuBvsb0nC/y++Srhf9fkCpkfMI65bdEb3KaRWkB7kRnrlN9rCwt0ojAG3Bj+V9wvdi+dJ7Ed0j7oVDZ86DiEfPDt9Dh85wP1cAvzfmd6SwJsg/pk8Jmo6N3CUjBuOxKy+4U06hHwkboyW7A7Xk5w5wWWTv+dFc4C8Pxu8W2SO3dUKivwr+QDheH2mrPAbgBqiDta9FPvok8skmQBJygs86mx1Ds+lPKI09iGaKOxNOvzVgd40M1fNc7JvveQGKb0Qj4TtHsJVpHfQJwZqvQtmyBe2fAVkewaHw4fl4nEZuFDpilODxH5DEQeL3OvcAz+5A8fQm8LthDnE/gfg9Wqiaz0fXwvu1KEUrAnlxCOQIj3NthvUtBHmxBeivB7JoEOrPwPMUNFtpj1RpBZLU08DHnC/i0RgYrwe/M+J3Og00yEAOLoV+kVzbDTLpT8BTDCoCvOSri2C+RSg/Om90z/QMige5ezaiRyK6ps7PY1/ReAjISZtlibup8B3vLMibi7UMELTaF7n5WcnPwF4+hb3Ewto7o9lqd5A726HdLpgr+wI8t+Z3LkoXwOFQgEhsRDGgfCX0j+Q8BiPukoB/GvyOpTAHv8u52G+IjCHumNZeKjeicuriu9GG8SN7YZ3D8qlh75H8El7fKM41hd9b/WUe3ZNT3FnNjuoa5VN0M+iVfOUnNEY+hnpI/197Zx4fRZH28ae6e3pykYmQCwaSQAJiBpiQCBJECUHYLHFJVkQhXBEDcijMkiAi0WTFGNigXAsSdoGs4gVKRuLL4XK5RkVXEFDwAJFXWQ6XNREUlRyzv6quDsNA9LPvZ/97O/LNU11dXV1VXf3UU/VUm8fwPKfQsKB+qPetFML1nz1e3g/vgh5MPW3TqWuAPulkzDl9i/icncP9GHz8VprRd7m/ZazvXe1N37d4nh2AHRyQsruxdidAuPlpSFhyvkdBOE+DvP8CXhD22Q+Ux2EeCmGHIQ8LGaIcozz1JORaIH3X5t4GDo4bOGo80gG//ReTObCLIuV8+S4Z7mSEESf3O5h5gEo/kM53Vg3HfAz3ZmnIX0UZvRQi7hVBKwPBNRMlsSCZ78PQ7qaVAQwOBNdy6Q4E8Vx2DUTGdwgE8VxmBoL4zGuUo7V0rZWjtfhugSC+23+hHK3lmxgI4hN/pnzZgSA++z8oR2vtnBQI4pN+phzDA0H88MBywIaNl/Y3Zok+j+FrFbY0LDsfX0P/GGCGxX0JIo77BXMhMVtrHgk5GHAbfjZ4QKYJBX+Q4cEy//uNNGKd/XMexvs92d9G5ogxn69LJ2HcuYs66zbo8kmUA31RoZai3eugIzPEu1kueVfC9/ncpT4sWCPmXsa75v+OVaq9KI6DcH+UH7Mg33NyTsJt/d8ZmPGXy9UyRsCqtzUaOhdje5zQ+1PpSRCnXhC2RCK3UdRHxdheoa6Ajb1F+uvN8f4UpEEs30ulLvOb87ulDx4I/z23h/ZDL5+Ejt7A+vvZIDeo85CeAdhN0ldv+OGRBx9D+Xii8r9xeg/mecuoAMSoJyCXCnK1LMpV/04FLJsKlA8QfgLx7ahAKyC+P1Wg/QbxsWAtjk+Bl4xrYI8QbJkBgOupAdrHNEDVjDDyEChfGPA4rs+U56Aviwy9K85/dTktv5YdQtxFnMtFfjwdepDaEYQjvAeSp1uKNNvRR9KEzs7j6UW6a6Sx19DQ0Gk0VKulW0JW0tDgUXSLsgJ5jxW2QZR85m3xvLeADw1fgY+vyRaDgYE+whYfppS2SzTfNhLP9ydaBZkHshDOtfXA2PYTdDHGhSt8mYWMGfvlfPv5Ob7m7E/Qb2HH7IRNScZfRzGlspF/soQ6jxR2tdg3yvhfJ9lo5Ct9GcI+uIbvsbt9BJXDdsnimPsdNfJ9ALv5Pdgs0WJ9kNuyW4Wf8kfUHXZS807IfNhLZ9CPyJxncsntJN7fjPda2FcvKPtolXoIZZvM9w34+F6Du+X7v9X0f0qfxoP/LR+I//Mx170D17t/6bhVH4T0Hfj7EQLDfB2d52HKq3wGpoxE20ofg/CJB/hV8Bwq5Zxvp77BV4Vjzp/QT9Yibi2e1VzIrXhGSXy+pb5PHi0OdvzLvu+U6dSNrxMhPlj7FLwFm93cE2vuoTXm97P99s3yedBsc94j5yDhcj12uOY3dzPnbZqxz1XoGHEN9JKY9+C9VTZRCiClQcgY9giFskoKFccNeE95fx1G49izFMlRFLIjbhwYrgylcdocwWilwXcA6ccpp4FxLc8vmT3kex95xih8vTTVyFM5Iu7bFjgFP4Jy2FANdCv6nMmf0Y8/F+NAA3SCySYK5lITe6SFHR3Nw2J/6GTfUZtO0S3Pgbc5sWZ+DV97+qX1TDyLXQHsB/tMedUaXQAB1+2/Ko25LmSswUfxvY7mnj35vG4SczLWMgeMQr2izDm8Wg/9WI8xB1I8z02G/5f/vzhgr19et48UazxZ5rxEvsedQJyf5LYA3nPygjkc/WXYi9/Lca4Q9x6GueJeMaeMMvdVw75NFH7lGoxTL0LmQ/4GY10++l8cxtRboF9ciOuNco6hveoImq3mgePos+9KiG0AfwF9ruB5NoxFoL+9Y2imn/NrtobYzyH3qbTspTmFcj1Mj7fMr0x/RwxVwZ7pJtp7MTmD+lN7/XHoJcxLOcxLvzcR8xy+33kGred7+zm2d5RFGBeO+unrrhJul12AzAP9ObhvT1DKfdvav2gNR0/E+34B8615NEzRaBp4FsQBO3CDWNAZpIBQ0F2e5/FjQTLoGnocY+VxSsI9yyBvlu9PsbSJnm5NB5s6zGw7/3PsC99uyXrwmr8/tLWw6UMN9Pdf654t+tP0A4/1zcXccCVkHuQc1GOY/SilBr0PXQkZerzhJ7kmzccmbntyO3ay//1/bh9Ga2Fj/blhLu73QegG3xr7Pozb4ymV9391OW22KXiP+mL880PvhTHjLdRBypCTaP93qd01x7z1V/vHW8IBY4s5FrVWVi0KzwiIdt5OSzko/03S7oFsvgi+kXFHQIVhB/iHm89Bxks5Uo73JlUB3GLk6RsHWta/+f4tjorZeyBIt4Dv81Lt0NU83QYf92/QFfaSsecrlr1OuoyrV46i7cSxGUdJUpppfpDH2aKtjH1X5t6riXKfhz9m/FtyHwjnFYXv85zM9Z3vBf99m3zfpZw/8Wefwu25y3/lzne7+WyZh2Ypcp+Z8NGbNkUBHVULWIz5LuGauVyfG+vxvm2hj1JiUA36ThrG48PoR1I3a9kUoccaaCMg28Ceex526AaxD36nfSsNCJoNTgt7chTY1UIe5gGrke5zoVvuh96P059Fufqj7WE/mDYEdA7f0x+pVdMEsTfwCO67zPeJUom5wwhQSfdyWvzc4bCBh6GcF2ma+hX6XBzmDZdgix/A/f5AQ9X3jHXX4K98zfZhNDx4PmQupB0ymobbF/ma9bkBco74RmAqH7vMtTk/v71xPILWK9uEjRPLfRrQ1w6EQ+03wHZtT1kh/6J8+3Kxps39JyH6ToyfmFOijUKgZ0h/WOr3UqRfIOZ2Werd0B+bcWx8k3CF39vWmbprT4nxjbd1iwQ71D9i/teOhiqXYGt0QP0bkbaaxmszYJs1UQrKlqWupoogvg53K9K+g3qch+S+kMcpD+fjeFnQx9bDvqnAvCxfECdss9v4+K2Hof7HxH42kcb2Gc6PlP6BRYZf0VhP9J2DHXnO9N2Kej/N1yx9B8Uz/gHXjcF1MyEnIJ4fz6SxKAOvT6SoF0HybyvmGBLvZEu8fTrCfG98I66rFe2gCV8Q2hPzxUibC8+Mz4O/ook4l8fnK7jvX4MTcP2fcf1wGq7OxbwS99L7UqTKjH6n3oPwPTQe87ksf/tV3YJ54haxtukUa+cS/ayBsIVmws61Ie1rgPddL0Dd0L+zlF3C91Qu7if9CLa1ON8TdrDAV6o9w21in6o9A6h5hbZErDf0FGuHObBlz6Fsb4vyZ3EfgZiDcV82g+T3OGjYzeoFuoOvM6oTxZrGWOF/eRI2zmMYu9FG6BvimzntuOFfsz+B9juB/piFfN6giuB2NNaeSn3Mvh50Pc41U0hwH1qvn6RFV/h4CsW8zNzfHW2C+PH+x/6gTr399un3N/fqg37mMf8OTX5b0h56IwflqAyQfP/5KvthNs22h9ZCP62xHUJ5xrID0PlHbBt9Z20blUcFp1CvGtBID9mO0wzbDoQPg3DaxOP1IoTPI76cNmnptMnenTbp2ZTKz2lfUnfb+7RcXPcirdYjkfYI2uA4+voeWiry3UpLoSsK9C64LlLey7jPY7ZHED5BD2l2nDtEBbZlhr9Ev5cealnnnY35yxrSGCwFxn0mHSlTf4UWaHNpKfRenvYeLRJ9pwzhSISLcP8CSH4OadhhWqS/irQPiv5s+LOGQ+8hneahYmHnf0O3Qk5Rz0In8PnYTuqCe5Rr/aAjn5f5830cL0AWieMsjA+LxHGF8PVn2eqhl1T0ST6/mUKZQQrdLzhF5YLThgztSuW2ZpqnEy20fYQ6vwn5HJhIC+3htBBtRGChdh/a8Cj63wTEERUjzXr9Q7QNztv+7qsVbcjbrwxt97b8JgzWgGjfR5F+Da0W1+RCx+rQl2ONb0LFNzH1dJPdiXvwds9FOlyLvBbqv6WklndChc30NNpwPuXa50Amoi060FCuu+xpOPbgvXvdlxEEqa0VlEN356kP4xl9RH1xjzyUp1xPhh7agWuG4FyS73XbGcQPA8uFLIdeKBbhfpSqNuCey1H21RSnzsGz+Bs9oq2iQvUbXFvs+1jrQXdhLMzD3GOAuHY++vojeJa7+F9eadqsTUQ+SK9V0hicq4BuL9dmybW+mSjLywi/hfZ9EfPlp1DOGTKuVurhl5F+L+KBeLazjOt5PP9uBzrE2AMzS54z94+sk/rwXnGuQtvtO6N7oeduQrgn8p4l+w/SibLzuX3gWoi0m6EDBoB5ivF9ZKayjzzie5pZhr/C/Gu9IbvEdx0vS1/FfuU43slltEUfg3cIKCWUpv9a5mF865sQxP1534CLyI9/j8y/bb0ddQJ87y7Pn1+rOilTfY622CYYeaG8E9WDmCut9+3HmLSFI76Z5OG+Rpjo8l8Sbg2UJceEPUDdOFLftXzH6af7TJ139TUNxvolS5VrmDiWax/JrJiizW9Bg/7W8k1oL/kNXj/zmzz1n1Qi9l/Poh36RtphXyy+qQuDDAveS2Ghx0SYx4VAhiCO2xEt32Xz8UWsLT8vdH2FTa7HQDrtz5MzeK4IV4j5+4cUjrhwuWZj7CVDfwl+nO7jeUlp7B0x9pGIOJRzKI6nglHo389gHAkFryCMt6wJaqHpPI47QG43vkFo+hLsAN8CaOEmPsfqaOyraapGOISokX9zsAnH6M2NoyHfBvvkdwzfgydBd3A9wEjTiHlv43iuE9Tv6QbbDTTY2K9g7GVURspvNOfTY7BFKvRM2Bg11BFzLuEL1qPoNlsfGhYUgXdqPtiDNtyFcTMM57+DnQAbETpN+FLVY7A3pS/afocYSypsO9E/R4p1fhfe7Ri7hxKhFwk2ZAjGtBDM8dfrjX6+03XI92bKCi6CTrwo9spgLtX0aVBvbsM3wapsjFUG8n26TV+DOtSb1xfWeBM0btNdOG4v6/8Dbxvl13SjMoluFHb+BrEvXdQ7BLa6/UtK1LfSQ7AXx+q/wvmPUL/+sB/6QH9+RU51AzHbjxgnVwlbS/ij9f9FHv2AtA/Et4eL0Rd4eS+g/DfL75ZX+HxKLEEXNL6EcnRkaZhz7kN/TaI4uXdsPd97EITrbP8jfBLCbrV3Rv/LpXJzvdmPfr/AixhhO/gBDck+w1sLu0RZAA2BNCr6oDZF7KAX6LMuY98kaWydYLR7CGyf0MeIwm4FmGG2ud2PvZcJ70LkQLqIBKLr8AzbRhi0m0gUhTJFPUEU3Y0opq1B7G2SY63TATNt54NEHSE7TSaKQ33icdwZ1e+Mp4tbUpf3DBLxriQhIglPtSvK0+0k0fV4SboXo/vjPUtGe7lQlh6SXhGS1/5Dvmwdd8QvcCPI/z/y9bVJGWohWPTz9A6ysLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLD4fw8jiuirLKNYGkd2UiiC3MT/f7wnlDfJRuzVOzO2sT41HbukuweFsT40EOSAxWAdsFEG61XTMT49ZVBb1h25ZeB3KVgCqoAX2KiOdRPnutY449MzBoWwJBy6GW6A3DRxSs3YENwm3T3JPdl9n3uKe6p7mrvIPds9z13sPug+FJbQmNKY0XigUfv2fPuS82zx+erzJ86rBw9Fx3ScVxztnFfcPqU4ozi3OL/YU2x7cA6iH/Dg1/0z8Wv6jGinY3rJ9MUz1s2onmGbPqNkVoei2ZFRHe+bhl8vTds+7ey0n6Zpk6fi6O2pbNKUSKdjUvwkpXrK7il1k3yTtElTyn7XoX1h9MOD23eeC9yDfsUyUeZMKgEq5bKbRX36ozb9aSYoAbuBDaEbca6EpaFZ0tAsaUg/k6UiJhWtmIojD+ssru6M1u2MHDrTBKCh+eLQfHG0B6iUwHpSCsgHHlAKlgAd5XC8FhqfHuweFM/CcW04nkw4VYPdwIZURugD8AWoE7EJ+D0TeMEBcIKFb7YddAxKRHig3xUaOWSuPHVVQF52kTYHlMh78tx9yEtfVyLyqjby5nHIK17mXn1FWfgd+Bm3zGcx2HM5xeagB9YNcsq71rXk5ZBHigi5Zc45Adfa+3t2ykMFTRVe40pLnyBy88g7rfOrj95SFp5XqaxtnTjDLw52pnv+yvrieSWwoJo216Xj9VA2h0amb/ftYfYaJ39PYlkYShGG27nxm+EeoeL52hFrR9nsaG87ys+PeM+njILQiPSM5tDr0k9cZPgX+V3Sd0rRdyzji/wvlIRPUj7J+ESd/MaDbyiVu1jC5pTNGZvVfK/Hq/xpOblWLyNX5VJy/RHhZWDpct21fIXqWlWmuZ5eqblWIly54qUVysxBDtYbrdsb5elNKkth0ZSKmraHjIKM3lymurb7TjB3DarjGdSGxYhSx6AlYlDWGPSBGNJYOxa5OTm+ageLRNeJRHUiM6JVT0N9g+JumNlQ0qBmXMq9lH+p/pK2zbcnw3fpuqj03HOec4r4lXGO7f4n83zNUr5mnrNswlnP2fqzahVCZzxnlLozrOTM7jN1Z9T60wz/3KdLT1edrj+t5Z6qP6Xknso/5Tm155SW+w/m+QcrPXniZP1JNeNY1TElZUnuEiWhKqdqQtW6quqquir9xPcM/y6UxcW7v2QZn7N1h6sP7z6sOj7M+FA5cYA5aj21irt2YG1O7YTadbXVtXW1vtqgyp2Vu5XPPtVcR8oU1/v7dNc+yHf36q69kE9WaK5FFe3jK+bvjV/4SEH870uCXI+AkjK7qxqNvaCMucrLyPU4wvMh3U05TUpJxsY61rP+5vqc+qn1JfU2501RsX2jovpEtb0xypEWFZYaFdw7Sk+JUt1R1Cuq2/WO7tc7ulFyvCPZnTwwWXUn7U5Skl2OHi5Hl8TwpERHXHx4QrwjwcYcEdeFhbUJDwsOCQ3T7UFhqmZDV1PCSI2Nz3V4HEqJo9Sx26E61HjVreaoJeputU61a6xHvJN1ahNr79AmKiKmTVstso3HyXKdHme9U+UPLNTZLjbd4yx1KrlpzNs2m7LvzPS2Y5AjMr1pruxtasId3lRXtjc4d8yoVxl7ajRivcqCbYzu9GoLtikQbQfnjRm1jbXnp8uc29FpyJudX/bkaFcnb0H2iFHe0k6jvak8sKTTaCp0FRW5+E9hoSERkqLQ9Wr3bkO8yUPu8fYYkn+by++n0Fs3xFs/5J4p+JXvrUu8rbCQua76QZaF/MdVZISK+H8iXMSD+O3iQZdx50Iq9L9U/BTxU0YeLpdIOHt8YdGVuES+4gdl8MZ6e6OxXDxtoahKoVknHlPIcxN1Re2CeUvm3pGZ7R1wR7bXkTvG2yERB3tx0BcHYYmZLqJ/A+/wEwIKZW5kc3RyZWFtCmVuZG9iago5IDAgb2JqCjw8L0ZvbnRCQm94Wy01ODAgLTI1NiAxNDczIDEwMDNdL0NhcEhlaWdodCA3MTUvVHlwZS9Gb250RGVzY3JpcHRvci9Gb250RmlsZTIgOCAwIFIvU3RlbVYgODAvRGVzY2VudCAtMjEwL0ZsYWdzIDMyL0ZvbnROYW1lL0RHTENPSStNaWNyb3NvZnRTYW5zU2VyaWYvQXNjZW50IDcyOC9JdGFsaWNBbmdsZSAwPj4KZW5kb2JqCjIgMCBvYmoKPDwvTGFzdENoYXIgMjQzL0Jhc2VGb250L0RHTENPSStNaWNyb3NvZnRTYW5zU2VyaWYvVHlwZS9Gb250L0VuY29kaW5nL1dpbkFuc2lFbmNvZGluZy9TdWJ0eXBlL1RydWVUeXBlL0ZvbnREZXNjcmlwdG9yIDkgMCBSL1dpZHRoc1syNjUgMCAwIDAgMCAwIDAgMCAzMzMgMzMzIDAgMCAyNzcgMzMzIDI3NyAyNzcgNTU2IDU1NiA1NTYgNTU2IDAgNTU2IDAgMCA1NTYgMCAyNzcgMCAwIDAgMCAwIDAgNjY2IDY2NiA3MjIgNzIyIDY2NiA2MTAgMCAwIDI3NyAwIDAgNTU2IDgzMyA3MjIgNzc3IDY2NiAwIDcyMiA2NjYgNjEwIDcyMiA2NjYgMCA2NjYgMCA2MTAgMCAwIDAgMCAwIDAgNTU2IDU1NiA1MDAgNTU2IDU1NiAyNzcgNTU2IDU1NiAyMjggMjI4IDAgMjI4IDgzMyA1NTYgNTU2IDU1NiA1NTYgMzMzIDUwMCAyNzcgNTU2IDUwMCAwIDUwMCAwIDUwMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMzUwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgNzc3IDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgNTU2IDAgMCAwIDAgMCAwIDAgNTU2IDAgMCAwIDAgMCAwIDAgNTU2IDAgNTU2XS9GaXJzdENoYXIgMzI+PgplbmRvYmoKNiAwIG9iago8PC9JVFhUKDIuMS43KS9UeXBlL1BhZ2VzL0NvdW50IDEvS2lkc1sxIDAgUl0+PgplbmRvYmoKMTAgMCBvYmoKPDwvTmFtZXNbKEpSX1BBR0VfQU5DSE9SXzBfMSkgNyAwIFJdPj4KZW5kb2JqCjExIDAgb2JqCjw8L0Rlc3RzIDEwIDAgUj4+CmVuZG9iagoxMiAwIG9iago8PC9OYW1lcyAxMSAwIFIvVHlwZS9DYXRhbG9nL1ZpZXdlclByZWZlcmVuY2VzPDwvUHJpbnRTY2FsaW5nL0FwcERlZmF1bHQ+Pi9QYWdlcyA2IDAgUj4+CmVuZG9iagoxMyAwIG9iago8PC9DcmVhdG9yKEphc3BlclJlcG9ydHMgTGlicmFyeSB2ZXJzaW9uIDYuNC4wKS9Qcm9kdWNlcihpVGV4dCAyLjEuNyBieSAxVDNYVCkvTW9kRGF0ZShEOjIwMjAwODEzMjE0MDE2LTAzJzAwJykvQ3JlYXRpb25EYXRlKEQ6MjAyMDA4MTMyMTQwMTYtMDMnMDAnKT4+CmVuZG9iagp4cmVmCjAgMTQKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDQwMTI4IDAwMDAwIG4gCjAwMDAwNjQ3NTMgMDAwMDAgbiAKMDAwMDAwMDAxNSAwMDAwMCBuIAowMDAwMDMzNjQxIDAwMDAwIG4gCjAwMDAwMzkwOTUgMDAwMDAgbiAKMDAwMDA2NTQ2MyAwMDAwMCBuIAowMDAwMDQwNDAzIDAwMDAwIG4gCjAwMDAwNDA0MzggMDAwMDAgbiAKMDAwMDA2NDU2MiAwMDAwMCBuIAowMDAwMDY1NTI2IDAwMDAwIG4gCjAwMDAwNjU1ODEgMDAwMDAgbiAKMDAwMDA2NTYxNSAwMDAwMCBuIAowMDAwMDY1NzIwIDAwMDAwIG4gCnRyYWlsZXIKPDwvUm9vdCAxMiAwIFIvSUQgWzxlMDQ1OTc5NGNjYTc1ZTAwODBkZjViOWI2MDljYWI0NT48NDlkM2I1YjkzMTBmZDAxZmQzZWYxOWMzMDFlN2Y1Y2E+XS9JbmZvIDEzIDAgUi9TaXplIDE0Pj4Kc3RhcnR4cmVmCjY1ODg4CiUlRU9GCg==\",\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCALKAoADASIAAhEBAxEB/8QAHQABAAAHAQEAAAAAAAAAAAAAAAECBAUGBwgDCf/EAFIQAAIBAwIDBAUGCQoEBAUFAAABAgMEEQUGEiExBxNBURQiYXGBCDKRkqHRFRY0QlJUVXSxFxgjU2Jmk5TS4zOCwfA2cqLhJENEc7IlNTdjg//EABsBAQACAwEBAAAAAAAAAAAAAAABAgMEBQYH/8QANhEBAAEDAgUBBgUEAgIDAAAAAAECAxEEMQUSEyFBURQiUmFxkQYVMoHwFqGx0ULhIyRDwfH/2gAMAwEAAhEDEQA/AOqQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlnNQTcuSSzkCYGNPfe14ycZa7p6knhp1lyLjpGv6VrCk9Lv7e6UXh91UUv++pebddMZmJRExOy6Al417RxoolMCVTTHeLOOYEwJYzTeMNEZSUeoEQWG+3ht6wupW17q9lQuI/OpzqpNF1sr+1vreNeyr069GSUlOnJSTTLTRVTGZhGYlUghxIhxrBVKYEqmn5jjQEwJVNfbgca59eQEwJJVIxWW+SWW/IpLbVrC6nUja3VGtKn85U5qWOnl70TETOwrgWzSde0vWHWWl31C7dF4qKlNS4feXHjWSJjHaRMCXvFkcaAmATysoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACnvVm3q/+R/wZUHjcxc6dSEfnSg0vfhkxuidnJ+2NSemXGsXEtt2+t0Y5lUlXpd5Ggk36zyngy3snsq1lPVd38NGNtQtqso21KrF5ainwvDzH3NGZdmmwdQ0SrrsNdhbVLXUId2o05uWYtvKfJeDKLaXZ3q+g69fW86tKrty8pzo1IKq+Phkks4xyeV1PRajW2bkXKYmPHr3j/bXooqjE+cMbs7nd2+Fq2u2WsXWl29tDip21GvUUJJcXJJSxn1eb8cl4p9omv6r2eXV5Z2aeoUbn0atKlGT4YOnJ95yeU845nn/J7vTRKmo2W2NSt6WlXbaUJzWeHnyeYvDw306mSaTsbWtu7QdptvVKVvqdSr31apOnGcKnq44MSi8c/Ew3bum7Y5Z7xj5R5z/JTFNWGFbN1uC1bS6633du6cmqtjqXfypvqscTfD0w8mRdo2o67e7kt9Mt9XtdE0jgjUjfRvY05zfA2/VVRTa8EuH2lqr9mm49Zr21PVbPQLOlGop1rqzg41Zpc+fhnw6FTujs33HLdj1bb9ezcVSpwh6TiUouMFHmpRafj9JWfZ5u80VxnE+mP5+y3vTHeFN2Ubs1KlvG50S81Ktq1vVm1CvWnNyTXLKUnlJ+XxNn9pN1Vtdi6vXoNqrG3aWHhrPL/qYNtjs/17S990tavrihdwm3O4queJOTzzSSx5G0dX0+jqmm3NhdZdC4punLDw8Pyfma2rrs9emujvHbOFozNGPLTvYltXSdV2/d6hqdvSvK9w3TarwjPuseKb5pvP2F2tdv6ftHZW5Ljb2tVbmpK2qS4oVIpU3Hiw0o9H4N+wtlvsPfG3by+o7S1ShS0ytOThGtJNqLfJYlB4ePFF1272a3ekbQ1qhOtTrazqdCUJPifBFtS5Zx5y5vxNm/coqrmvq5iZjt/v0UxM09oYxsa53FebeluW81y8lY6P3rdvKtNyupRjxpTbeGuKSXToilVXeO49Avt2UNZurWlCsoRsqFWcYNLhT4Vx4xzNlbC2bdaZsK80HW1Sc7mVXi7qba4ZrHXlhmEfyd750/TbjRNM1O1ei1anFwOUYuWeH+w2ua8GWovWKrlUxMR37Zjty/7VmmY+391Pujfup612ZWuo21arp97Sv429eVrVcHP+j4uq8Mvp7C37h1zd+kalt/Uq2pTlK+o06tO1hUnGk8YSUlxc88fPzMv13swulsKx0XRpUp3UbqNzc1KtRpTfA4tp493I9N47C1fVrfaMbT0dy0u3hSuO8qYy48GXFY5/NZai/pYmIpiMZq39PC1VNUz9ln0/cO5dL7UNOp6tdOtR1anS47ZSn3VHj4eUI8TxJY6+OX5nvvW83Bq267q0ra9Q29p9sp9xKjepVKq5YzCE+Jvr4eHtL3q+yNWu9+6DrFL0f0OyjRVXNR8WY4zhYLDfdne7bbet/quiXdjTp3NxKarVEpThGTT6Si1lY6oxU12KqoqiYieX085TPNspuz7WtY3BtjdOi1dUr1K1Gk6tG9qSm6iT68284wuSXTJQdhOmX9xe1NQoajVpWVGc1UteOeKr4VhtZx4rr5GadnWw9S29qutvUZ0qtvfUeCM1PMm3nOVj+BRdnmx907W11w9KtXo03JzhGacpPCw+cc+C6PwMly/ZiLtFFUd8TH/wB4RiZxMx4Y1sHeV9om19zancVat9Vo1KEKUK9RtRcpVFyz4dPoKjbul791XSvxjtNZuqld1VVpadO4lwVFlPk3NRUcN8m+iL3tPsyu6Wha9puvOileunOhKjUbalBzab5eckUOm7M7R9OslpVnrVvR09T6xqLijHlzT4eJdOmRVc0883TmmKsxv6YWnMTGfRed/azuqG0bKHo9tpd9cRUbut6bTpdy+LGIyc11XPq8dDBrPc+sbX3Fp8Y7jq69Gu1CvCrKrKnBNx+a2+GTw/nRyuRl2+ezjW9Y07R/RNQjeXlnSUas7ubfeyb4nLmn4+DRbdT7Nt26g9HuL2+tbqvbSjxUsqEKUVjlHhil0SKaedPFERXVHfOYx9v52R73NlvaHzVjoRJaTzTj7iY4jMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEGRAEMDHsIgjAhhJckMEQSIP3ECYEYEPaHjyIgYEMchgiBgQQwvIiCRAYIgjAlxz6EfgRAwJcc+hHHsIgCGPYMEQMCGA8ciIJwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKbUb2306yrXd5U7u3oxc6k8N8KXV8jFH2n7P/bNP/Cn/pK1VU07yzWtNevRm3RNX0iZZoC3aHrena5ZRutKu6dzQf50P+q6ouDkl1fImJie8MdVM0Ty1RiUQWfXNzaRoUqMdWvYWzrPFPijJ8X0IuyqRxnJOfBNNURmY7JgQ4l5hNNZTCqIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGBjPaR/wCBtaw8f/DT5/A5x0m4trXbFd3e34XXeylTp38qvD3csLkljwydN7u06rq+2tR0+3cI1bmjKnFzbUcteLWWYPtns9ubTYV/oWrVLV3FxKU4TotyjF4WHzin1Xkat+3VXXGPm9JwniFnSaaqm5Peao7d4nHr2Y7sCVxszs9vtwV6quqN0odzSpyzh8TXFz5eK+gtk7/flHQKO5/wtm0qN1VQU1yjzfzeHGML7UZbtTs/1WhoGo6Fr9ezraXWxKk6M5ylCaec8Lil9pZanZlu+ra0tJra5bPRKdTMYcUuLhy+fDw9efTiaMVNFVNuIxP/AG3Y1elrvXLlddMzNXeZjenG0b4lQ783VU1vR9A1G2iqDrOUJxlCMnxKXD1a9jL7vTcuu6jvKG2Nt3Hoc4xi51nJRy+HifPDwuaMa7UbOw0aeiaBps3OrbvMopZacpKXNeGc5M43nsHUdS1yGu7d1GFlqShGOKnKPJYzlJ+HsJnqTMxE98QrnSW4tVTERExXy5jtvGMrZsfX9e0netTb+6Lt3nexbpVM8WHz55wm08G4o/NRrHY2wtWsNw1dd3Nf0by/cXGCptySz4ttL6EjZ0ehnsxVGc7OFxWuzXeibONozjbPyTAAzuaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH0AAlIKC8UTgCXCz0QwkvAmJeJeaA8p21GcuKVGnJ+co5Z6KKXQmTREGcpeFZ6L6CPQiAgAASAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYErml5miO1bftanuy1tNLr1oU9MrcVbglhVJcuXtSy1hmz+0LctPa+g1LuXBKvNqnRhJ/Ok/u6nKVepKtVnUqS4pTblKXi285Zqam7j3Yer/DXCadVVN67Hux2j5y7D27q1vrel29/aSzSqrKXk/FfSXTJz72D7qVhqT0G5cY0LmTnSk3jFTC9X48/idARkpdPsM9mvnpy4fE9DVodRVZnbx9EwCBkaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAedecaUHOclGEVlt+CJ306mtO2ndcdI0CWmUW/TNQpyhFwlju45SbfvTf0MrXVFMZln0unr1N2mzRvLVXa3umruDclahSruenWk3CjFLCcsYlL6cmDDk8tePPOc5ByKqprnml9f0Wlo0lmmzRtD2sbqrY3dC6t593VozVSEvJrnk6s7PNwR3Jtq3v213/8Aw6ySxwzXVY+KOTUsvrgzzsh3O9A3LGld13DTriLjUzPEIy6qTXwwZ9Nc5J5fVxfxJwz2ux1aI96n+8OnkCSlJSgnF5T5p56k50nzMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+gbHgBS6hdUbOzrXNzUjToUoupOUuiiubZyXvTcE9za/W1KcHBTjGEIttuMUuS/j9JtTt23coUIaDp9xGUqqbu4x54hnCi/Jtp/QaPzy59fPzOfqbuZ5Ie9/CvDOnT7XcjvO30AAar2YFjPPmvFAA+rpLsY3Wtc2+7K4cVe2EYwfNtzhjlL6cr4Gx08rJyPsTcdXbG4aN7CX9A2oV446wb5/R1+B1hZ3FO5taNei1KnVgpwa6NNZX8Tp2LkV0/N8s4/w6dFqZmmPdq7wqQAZ3CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABDzLJu3cFvtzQ6+o3eXSp4iox6ybeFgvb6nOfbfuueq67PRqHA7KykuKS5uVTHP4LKXvTMV6vkpy6XCtBVrtRTajbefo13ql/V1PUrm9uJOVavNyk316t4+0piBE5UzM95fW7dum3RFFMYiAABcAADljDSZvPsI3V31pV0S+rznXhNO3431hw/NXu4ftRowrdD1GppOsWmo0Ip1raopxT8fNGS1c6dWXK4vw+Ndpqrcfq3j6uzQWPaGu09xbfs9SpJR76HrwTzwTXKUfg0XxHVicxl8mromiqaKt4AASqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABByS6tEWUep3lOws611XbVKjTdST9iTYTETM4hh3axvKltbRZRtqkPwtc4jbwfPHPDm15L+ODmatVlcVZ1arzUnJyk/NvmXfem56u79xVtVnGpTtsKnbUKjz3ceSl083xP4ryLIc3UXOevEeH1HgHC40OniuqPfq7z8vkAA13eAAAAAAAAbC7Jd8VNu6pSsLyWdLupPixH/AIc8cpL3tLP2HSdCpGpCM4PMZLiT80zil9OTcWujXVHQ/YhuytrWn1NPvJRnWs6cYp/nY6c/Pp1NnT6iYqiiradnhPxPwmKP/ctfv/ttTKBKunMmOi8UAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIkAa97ZO0OXZztqnqsNPjqEp3EaHdSr91jKbznhl5eRqr+crqtClZXep7DurPS7qpGMbydxNwa8XHNJKTS54TJnsiO+zpfJDJiVPtA2vLSrW/qbg0u3truLlQndXUKPeJScW1xNeKa95Ytr7z1havfU98XG39FoVas1pdrKvGFzXpKTxOWarT5JdF+cufgRntlPnDZYMde8Nuq4u6D3BpMa1plXFKV3TUqLXJ8azmOPaS2+99r3NWNK33NotarKDqRhTvqUpOKTbkkpdEk237CfOBkmQWfR9xaRrSr/gfVtP1B0Md76JcQqqm+fKXC3jo+vkSadujQ9Thd1NN1vTLynaLiuJW91TqKiufz2m+Hkn18gL2CwUt47draXX1Olr2k1NNoNRrXULynKlTbaSUpp4XN+LJrPdmg31/b2Vjrml3N3cU+9pUKN3TnUqQxnijFPLWE3lIC+gxepv7aVK7la1N06DC5jU7p0XqFFTUs44XHiznPLBk1OXFzXNYTTAmAAAAAAAAAAAAAAAAAAAAASzeEaC+UPuCdxqmn6LaVpKFKDq3MYyaT4uST6ZxjJuTeW4LLbWh1tQ1CtTpQjmNOM3jvJ4bUF45ePJnINavXvLu4vb2Sld3NSVWq0ksSk8tcvJtowX7nLTiN3qPwxw/r6jr1fpo/vP/AElSwknjksciI6g5j6QAAAAAAAAAACBftj61PQt06fdOtKjbzr04V5J4Sg5rOfYWIh4PllNYfsXmRuwamxTqLdVuraXalpWp16EKtGcalOazGUXlM910NT9gGv8Apu2quk15x9IsqknDHWdKTyn7cS4o/BeZtam8xyzr2quemKpfH9ZpqtLeqs1eJTAAyNYAAAAAAAAAAAAAAAAJK1WFGHHUlGMV1cnhE5qn5Tzx2N6y/bT/APzRMRlEziG0KVzTqw4qc4yj5xeSFG6pVs9zUp1MdeCSeDjH5Pm+qmzI19H3IpUNG1ql3tpXqrCVTPByf6Lw858YrzZlnySbypp21d6XtC3qXdShONSNGjFzlUahL1Y4TeW8Lln3FcrTGHVCeRk5d3B23dqOj2dTVr/Z2m6VpDrOEIahTqU6+MpYSlUjKT9qhjxMk7QO2zV9H7Ndp7n0OysHV1iTjVpXMJzUMJp8PDKL5ST5vJPfOEeMt/5GTRekdo3aPW2xq+vahtKjG3hSpy020t6FSpWunNP1vVk2oxwnjh556rqYRfdvnaJtuvaz3dtvQ7ahVw3bZnQunF5w+CVSUoLk+coEROe8J5Zzh1Xkg3hNs0T2t9smr7S1vbVLQrKxvbLV7WNwlXhNVJOTXCotSSWcrqngtu1O2beP8pthtTfG3rHTnfz4aao8UalNNS4W8ykpZaSysId1cugoXdGdV04VacqiyuFTTax7D1jLOeWGjmns81e1p/KF3lGGi2sKttG7qTuLZ15162JLK4XUcW3/AGYr2HjuTtm7T9LnqN5HaenabotCq40aur0alvOa54S7ypT45PD5RWfYTiczCfT5unsgwDsc33U7QdnQ1iraRs7iNWVCrRi3KKlHHNNpPmnnHgZ+RE5hMxjsAAlAAAAAABgMDQvyxv8A+M7T9/hn6JGotH2D2k7/ANkaHYJWH4uUZ97QlOpCDhlY4nhcT5N8jqLtS2BZ9omg09K1K6urWhTrKup2/DxNpNY5p+ZdtmbaobU25aaPaVqtajbrEalXHE/fjkRVnMY+aacYlyR8obb8NoUto6Kqyqq0tpJ1FlcXFVqSb+0yjtxvKFx2p7Fp0ailOhbpT4fzcpNP6MG9u07s30jtE0mFnq/eUKtOcZwuaCj3kMZ5LKaxzZhdr8nfbVnW0evaXV9SuNP5yqqUW7mWcpzyms45cschX3pxHphFuqYqiavXLSOk7fW6vlH67o93Uqx0y61G7jd06dSUHUgnUlhtNPrEr907W0u0+Ufo+17adWx0eVCjaONOpJSdOVJ5hxLD9bOG/ab90Hsg0zRe0W73jQ1C+qX1zXq15UZ8Hdp1OLKWFn85+JqXtD2dPc/ynaVrqGm3VbR7q2jSqV1Rk6cH3EsPi6JqSTXPqi1U81VU/TCZx7sQouyu3/FH5Smsbf0OtWlpco1YToym2uUVJcWXzcW3z6835lD8nStSobb7Vo1ZJR9DUksdY4rLP2/ab07MOx3Q+z7Ury/0+rcXl3Xj3cKt1wt0Y88qGEuvLPuMfvPk57WutyXuqzrXnd3U5zdmnFUoOXlyzjx5tkRPeZ+iN4x8mkNoxx8lrfPL/wCvt/D/APtpGa9nW2NI0DsWt99X2pXGna1VoOjHUlGVx6PSdfu4xhS4ksuK4cprHEzZmmdh2j2HZ7rG0YalqErLU60K9WtJQ7yLjKMklhY/NMn0zs70m17PKGzruEr/AEqlDh/p8cUnxuabwkuTYp7TGfUnvEuQtS25pdHs3vNfttP1S+unc5p61eVlaOOeBrhoxnUdR5beW49fYdZdhOp3ur9mWk3WpXE7i5cXF1JvLaTwsvx6dTC9M+TXtOxsdSoVK17dzvKfd06tbgcrZ5+dDCSz789DaOwNq0tm7Zt9FtbqvdUKEpOFSvw8WG28eqkuRSqJmY+S0VRiYZGngZyWncuo3Gl6Rc3lnZu8r0Y8SocfDx+5pP8Agael2+uEnCW28Si8NK96Y/8A8zLFMz3he3ZruRmmG98jJof+cD/dx/57/bH84H+7j/z3+2Ryyt7Ld9G+MjJof+cD/dx/57/bH84H+7j/AM9/tjlk9lu+jfGRk0P/ADgf7uP/AD3+2P5wP93H/nv9scsnst30b4yMmh/5wP8Adx/57/bH84H+7j/z3+2OWT2W76N8ZGTQ/wDOB/u4/wDPf7Y/nA/3cf8Anv8AbHLJ7Ld9G+MjJof+cD/dx/57/bH84H+7j/z3+2OWU+y3fRvjJTaleUrDTrm8ry4aNvSlVnLGcRist/YaQ/nA/wB3H/nv9s8qfanqm9r+OgafpNvbW9/TdCpKdSVWUVJNOSwo9FkmKJlanSXM+9GIYR2h7xu93a1c04XVaejUqqnRoVEo4aWE8efOX0mNPHhz9pvOh2FaeoxVTWb9yxmTxDnLxxy6fE9v5CtM/bGofVp/cc+5ZuV1Ze30f4g4dpLMWbcTiPk0MDfP8hWmftjUPq0/uH8hWmftjUPq0/uMfstxtf1Vofn9mhgb5/kK0z9sah9Wn9w/kK0z9sah9Wn9w9luH9VaH5/ZoYeXt5G+P5C9L/bGofVp/cY3unsrsNIutOp2uo39bvqyjX+YuGn4tYjzeeifLOROmuH9VaH5/ZqsG94dhulTipQ1m/afR8NP7ib+QrTP2xqH1af3CNLcP6q0Pz+zQwN8/wAhWmftjUPq0/uH8hWmftjUPq0/uHstw/qrQ/P7NDA3z/IVpn7Y1D6tP7h/IVpn7Y1D6tP7h7NcP6q0Pz+zUGztfudt6/a3ttVnCj3kY3EILPeUuJcUfozj24OstI1G11TTqN5YVo1betHihKPRo1Y+wvS8ctY1H4Kn9x62m1q/ZzXV3pt3O6tXTdNxrRSay28csLC8PeJuV6Sma64915zjOq0fE64rsTMV/TdtpdXkia1/Hq/XKNvbYXTk/vI/j5qH6vbf+r7zB+eaX1n7OR+Van4WyQa2/HzUP1e2/wDV94/HzUP1e2/9X3k/nml9Z+x+Van4WyQa2/HzUP1e2/8AV95N+Pt9+q2/2/ePzzS+s/Y/KtT8LY4Ncfj7ffqtv9v3j8fb79Vt/t+8fnml9Z+x+Van4Wxwa4/H2+/Vbf7fvH4+336rb/b94/PNL6z9j8q1Pwtjg1x+Pt9+q2/2/ePx9vv1W3+37x+eaX1n7H5VqfhbHBrj8fb79Vt/t+8h+Pt9+q2/2/ePzzS+s/Y/KtT8LZGcmuu3/RNS3D2Yappui2s7u+quDhRhjMsSTfX2F42tr9/rFyuOhRjbrPHKKllcuS6mWnS02opvURco2aF21VbqmireHNE+x6+1/sS0myvLF2W5tLjUnQVRevh5fd5T8Xwvx+1nl2J7J3rt3s63ravT7nS9buoJ2Mpyim5KL6c+v3nTmBgyRGIiPREzmZn1cc3eye0DXNjX2k6ttHUrvV4z9Jjqd9qjny4l6lOi8pNrllS6F23psDdeodiWxtFtNBu6mpWFetK5oYipQTlPDfPHRrozrBAtO+TLU3ahpO6qnZHbafs5V6Gp06FGnOlbz4KnCkk1GXLD5f8AuaN3h2d713DtWyrR2Xc0L63mqdWvc6h6Vd10uLn6y5R9bpnwOyyJEdoiEfNzF2ibG3Lq2t9m1ex0W5q0dOsbalePC/oZRcMxks+GH59C6b62buG/+UXtzcFppVero9pGj310muCDjKbfXD8V4HRDXMtm5tIhr23tR0mrWqUKd7QlQlVpNKUFJYbWfERHifMYROcRHpn+7lnaUNRue3vtHpaBVVPVKlC+ha1G8KNXijwvLXLmSWmy+0PV9I1nS9x7XvdSvq8Zyp6hf6o3RpNcXOnS5rieVhrHQ3r2V9k2jdnXpc9OqV7u5rvncXPC5qP6KwlyNjRi0sEzM5mfmtO0RHphp75MW29Y2xsK6sdwWFayu5X06ihVSTcXCHNYb8mbjIeJEiIwiZyAAkAAAAAAAAAAAAAAgo4ZEAAAAAAAAASzjxP/AKHLXbVs78Xded5bcTs72UpRXDypyyvV/idTmMb/ANtW+59v3NpXhxVowlKhJtrhnjkZLdUROJ2bOlvdKvvtLjUHvfWtSzuqlvWWKlN4aPAvMYnDs4AAQgAAAAAAAAAATCKTefA6G7A9m07OyhuC8jUV7W46dNSWMQ5c/PwZqTs22xV3NuS3t+FStKc4zuPZDLz8eR19aWlG0t6dG2pqnSprEYx5JImqZppx6tPW3uSnkjeXtFYIgGByQAAAAwPC8uKdrRlWrTUIRWcsxPVLNwnpNzcZ9Ju7+nUqxfNR9VpRS9iwvbjJeLzg1TU/QZxlK3tsVK2ViM5fmJeeHzftSPHdX5Rof79T/gwKjSnHTrh6ZOo5RUe9oOWcuP5yz44f2P2F5TyW/V7eUrbvrdtXFCSqw9rXWPxWV8T20y7p39nSuqDbpVY8UG1h49qAqgAAAAAo9Uso31rOjOTinzzFLK+krCDXMrXRFdM01bSmJmmcw0hqFnOyvK1vUzmnJx6dV5/RgpjPu0LSYulC/pPhalw1ct49jRgJ8/12lq0t6bc7ePo9lw/UxqLUT5gABpt4AAAAAAAAAAA9LalKvXp04ptykly5/nJeHvPMzXYWkRqT9NuKanGOHS4vCWeq+g2dHpqtVei3GzT12ojT2pq8+GW7d0qnpNhGjDLbbnJvrxMu3iMIjjmfQbdum3TFFO0PGVVTXM1SAAyKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBrqRAHP3ygdnyp1vxhtFGFJqFKtCK8eaT6e7xNINp9Op29uLTKWs6He6fcLNK4pOD/6P35ON91aLX0DXrvTbiLVSjLx8U0sYfiuRnpq56fo7Gku9SjlneFoABDZAAAAAAAACenCVSSjBZfgSGzOw3a0db3JO6vaM5WVrTbbXSUm8JfxfwJp3zOyJqiimaqtobi7H9nx2xt/va0lO6vIxqT9XHCsco5+LNhksPmrlgmMVVU1TmXCuXJu1TXPkABVQAAApdTuZW1txU0pVZNQhF+LbwuhUz6dccyzW9GGo6n6ZLnC2zToOL5PiXrS9vgl5cL8wK7S7aVC0iqzjOvJupUlHo5vm8Z5454WfDBa91/lOifv9P8Agy/x6Fg3X+U6J+/0/wCDAv8APnFp9C0UnUsNScFwuyupcVN5acJ4Xq+WHhv3svBR6vZUtQsZ21eU4wm1zg8SWHnk/ACqg8rOckxb9Gu1cWrhUlF3VGXd10vCa6/T1LguaAAAAQZEAeNxRp3FCdOpFShNNNNZNP7g02Wl6hKhLp1i/Nefs93gbmfQxneejwv7CVwlLvqMXjGOafX6Dj8Y0XtFrmpj3ob/AA7VTp7sZ2ndqwEX7f4kDxMy9lmJ7wAAAAAAAAAEYpyeF1fTyIyKnS7OrfXtKhS6ylz5deTePsz8Gbl0+0p2lnQoUklGnBRXIxfYWkyt7P0yvH+kqpqKST5eefHoZiuXI9nwXRTZtdSuO9X+HkOJ6rr3cRtCIAO45oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAg0zTvb9tCle6Q9dtKEfTqLjGrJS+dDov4m4zyuaUK1JwqRUovlhrKLUVcs5ZLVybdUVQ4TfJteCBmPahtWptbcdWlw4ta7lVoYXJR4ny+HIw5rDaMs9ndiqKoiYAAQAAAABcnzAqdNsa+pX9CytI8dzXkoU4ecn0Ov9gbZobb27aWtKlCFw6cfSJLrKeMv7WzU3yfdoqvXe4rqKaoylTpRlF/OaXPn5czoCCxEi5OI5XO1t6Znpx+6MVhIiAYnPAAAAJJz4YOTaS88gUGr3EuCna0ONV7h8MZR/Mjy4pfBPl7cFVYWlGxtoW9tBQpQXqxXgUWkRq161W9rtf0i4aMcfMp5b+l8n9BdUAMf3X+U6J+/wBP+DMgMf3X+U6J+/0/4MDICEllEQBZ9RdLTbyneNKNKs1SrYXm/Vk/i2s+T9hd4PME+fxPK5owr05U60FOlOLjKLXJp8mvtKHR61enTlZXku8uLdRj3jfOpHHKb9rxz9oF0AAAAACSpFSpyjJZi1hp+KJyDWVhkSNT7z02FhqzdKMYUqq4o46e3HlzyY+bf3Rpi1HSqlJKPernBtLOfL4mpK9N0qtSDxmMnF48zw/F9F7Pe5o/TU9VwnV9W3yVT3h5gMHKdcAAAAAC8bZ0meqanTj3fHQhJd6/BL/tFphHjnGOUsvGWuRtTZOmSsNJTrQcLiq+KfEua8kdPhWj9qvxn9Md5cvimq6Nrlp3lfbSlGjbwpU4qFOCUYxXRJLkl7j2IJYRE91EREYh5IABIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBrPUiGBgPbBtN7o25w0OVzaN1qeFly5PMfjyOT3BwbVSLUlyafVHdcln3HLPbjtmht7dFOdnBwtr2m6kYvLzNP1kn08U/iZrfvRiN/Dp6G7zRNud/DW4ADfkAAQF52lotXXdwWWn0VJurUSk0spR6v7CzYOkuwfZv4L06eraja8N7XlxUJS6xhw/9csmPd96VL1yLVE1SzjTtux0ZQei1O5iqag7eS/oX0zLhWMSfiyvsNYU/6LUKfoV2m06dR+q+fLhl0eVh8vMu+PYUuo2NvqFDuL23p16LeeGays+fvMMznu4MzNU5lVReYpkSwu31Owq5tak7yzx/warXeQx0UZcsrH6XP2lZZara3Nx6P3ndXkY8Urar6tSK8+HOce1ZRAuQGV5oZAFl1HgvrmOnSnJx/wCJWcOWI8+FZ884/wC2XG9uYWtGdWeWo49WKy5NvCSXi22keGlUJU6Lq1YYrV5urU5Y5vovgkl8AK2nFRSS6JYROAAMf3X+U6J+/wBP+DMgMf3X+U6J+/0/4MDIAAALPrVDguKGow4+8tFLMU+U4vCeV44WWvLBeCWS4sprkB5WtxTuaNOrRmp05xUoyXLKZ7lns3Kx1KdpNRjb1pSqUHnrJ85R9+ctezPkXgAA2l1Z4XVxRtaUq91Wp0aMFlznJRive2B7lLeX1vaRTuKsYZXJN85exLq2W2tfX99Tf4HoqMW/VubmL7uS84pNNryfR+B7WWk0IXVO6uYzub2GVGvX9aUcrD4eiivcln2gUdWV9rNGtSxW0+zm+GNVcUK8o+cejg/es+8xXeG342EKda1lOVKXJqWOWF197eX72bM+BR6jaU7yhOjVhxJrlnwfg/eaWv0kaqzNHnw2NNfmxciuGkunIFVqFnVsLqdGrFZjNx5Z6r2/R4lKfPqqZoqmmreHtrVym5RFVOwAAuAE1KLqVY04xlObfKMKbqSb64UVzfQmImZiIVrri3E1VbQybZGjO+vVcVU1SotSw45y88lnp08jZ8IpZwW3b2m09L0+nQpwSlhOpLxlLxbLoe84dpI0tmI8zu8Vq9TOouzXP7AAOi1QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADCu1bb9PcG0tRpwt41b2jSdS3lw5kpLnhY588Yx45M1JHFtt5LU1ctUVQtTXNFUVR4cK16c6FWdKvCVKrCThKFRcMoyTw00/FNP6DzNg9t2lfgztAv6tKM1QvIxrpxhwxUn872PnHLfm2a+Nq9bxMVRtU79NUXKYrjyAE9OnKpKMYJtt46GGIzOFoZh2VbWlurc1O3lwejW+Ktbii2nHPT3vDOubelGjThCnFRhCKjFLokYT2TbTp7a23bylGSvLmnGVZyWGurSx7OIzsrcqz2jaHG1d7q14jaAAGNqhQXmmULqp3rpqFdLCqwfDNf8yw8f94K8AY9K4vdGt3PUYTvbeDaVaguKoovpxQx4cueXnryLxaXlvc28K9tWp1KU/myhJNN+WV4nvOCmsPw5pmP65aK2uqdzY1qlvfV/wCiXdwUlP2zj445c+vhnmBWVVUu9YUeJO1tsOUGubqNZX0J59+C7GMabd/gKlCy1GgqVGKc3eQlmi3J5fE3zjLLfXljx8DI6dWFSEJU5RlCSypReU17APQAADH91/lOifv9P+DMgMf3X+U6J+/0/wCDAyAAAACDlh9AKHVrWde1cqDirmlJVKTl0Ul5+xptfEm067jc2dGu+TlH1o+MZLqn7c+B46lrFCzUoRjO4usZjbUMSqy+Hgva8GO3Gn1HdyrancytLO/kv/h6U2u7qY6Of9rMm0klnzAv13q8ZXatLCk7u4w+LgfqU/Ljkvm5+k86GkVa+amrVlczlJyVLn3UFjlHh5J482vgXG0tqVvRVK2pwo049FCPCvoKoCFOKhCMIpKMUkklhIiAAAAGF7+0eVxSje0FFOkn3ix85eH25+k115c0/cb0r0lWpzpzw4STi1jwNR7n0t6bqNSCcnTeJRk44T9x5Pjui5Ko1FG07/V6Dg+r/wDhq/ZZwAecy9CGZ9nmmqrdSvKtNuFNLu3JfnPOWn48uRi2lW1W71CjQoJSqTklHiWUn5teXj9JuDRtPWnadRtlNS4FzaWMt839p2OC6Sb2o6kx7tP+XA4xq8R0af3/ANK5EQkD27zoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADEO0zbFPcu0by0jRda7hHvLVcfDiounP7zkS8tq1ndVrW5hKFxQk6dWEusZrk0/imd0yXqs5g7dNpT0XcNTVqMM2eozlUaj+ZUwnLLby8vifufsNzTVdSmbNX1h0NDf5Z6dW0tXrHxNodh+z5a3rTv7+3c9NoqUVJ9JTwuXwya70ewq6pqtpY28XKrcVY01jl1Z2Lsrb9DbW3LTTqEYZppuc4r5031Zin3I7tnVXulTjzK/KK5EQlhJAwOMAAAAG8JsCEnhFrsZq/uqt2uJ0oxdGi2sZTacpL2PEefsGsVatTurK0qKncVnlyf5lNY4n7+aS95caFOFKlGFKKjTikopLCSAmUE+f8GWKvpt3p9KpLQJ04zcnP0e4k+6bb5vKTkn7i/jC8kBabXWqU7iFtcxnbXc/m0aq4XL/yvLUl7Uy6QbeW8Y8MHhf2lvd0eC4owqrw4lzXtT8H7S19zqOnVM2c3eWKjnuKk26yfsnJ8/8AmfxAvpj+6/ynRP3+n/Blx0jU6Oo0JTp8UKkG41KVTClBrqnhtfFNryZbt1/lOifv9P8AgwMgBJWkoUpSlJRilltvGEWKeo3mp05x0OPDHicfTK6/o1JPDSjlSf0Je0C66lqFtp1HvrytCjSyo8UvFvol7S2zranqFeKt4eh2LT46lRYrN+HCueF7+ZU2ml06dVXF1Kd1d8OHUqty4fPgXSOfYl0Rc0ljoBSadpttp9KpC3i81ZupUlJ5c5vm2/e/LkT3tvTrWlWjPpUi4vHXp4FSQl06ZAt2hXFWtbzpXcYwu6EuCpGPl1i8+2LT9+S5Fn1XvLO5o6hRajRj6tysc5Qzy+hv6Gy7QxwrnleeQJgAAAAEDHd46XG+0mrKKfe0lxqSxlpeBkZLUScGmsp8umTDfsxftzbq2le3XNuqK6d4aJmuGcly5PGU8ogubx4mT740lWN87inDhp15tp58fFY8P/cpNq6LLVr3m+GjSac+T5ryTPAVaO7Tf6GO+3/b19GuonT9f+ZZH2f6Vw0vwhXS4vWjSw+eOXE/i4pfAzpdDyoUoUqUYQilFckj1R7nQ6WnSWYtR+/1eQuXKrlc11byAA3FAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB9C3a7p1DVdMr2dzThUp1YuLU4qSWU1nD95cX0LXuK+jp+h3925913NGdTjayo4i3nHiTGeaMbomrk970aR+T3o1hW1PWbudSNW50+49GhHk2nzXHhdMnQEPmrByJsHcFfbO6LO/p1ErerVUbyKjynCTxJ8vFJ8XvOtLK6pXVrSr2841KNSKlGcXlNPozo8TsTauRPiWK1xGNfE1+YVIAOazAAAHlc3FO2t6taq8Qpxcpcs8l/E9S0XNRXmpU7KMJuFKSq1ppeqvFRb888Lx5MCbRaHFKrfzjLvLlLh4vzYJvhWH0ysPz58+hdSCWGRAAAAQlnqg5JFBd6ta29SNFydS5lHijQprinJdM4XhyfPpyfkB5atpdtqEJZcqFfli4otRqR90sPBhG8NeubC90e37laj3d5ByqW2WqfX/AInXhfj1Mudne6xSmtRn6Jat4VtS5TkvDjnn7I4ftZTbjtqdtU0KnQgqcI31NKMenRgetrp8dUuaV/d3sLqEY8MaFCebd8/nNZfFL28i/UUoxUUlFJYSSwWy90WnO79Otakre9jHCnDnGf8A5o9H7+vtPK31apZxa1ujGzxLhVdf8GS8G5Z9X4/SBfASQqwmk4SUotZ4k8onAAADzr01Vg4SWYtNNe8tOjyp2Up6TVqTlUoR4qbllynSfRt+eW18C9Fq16lcOiriwpQq3tBqdOEpcKn1WG/c2BdVzQKezuqVzbU6tF5pySa/914dMFQAAAAPowQl0YFp3HZ0b7SqsK8lGKXGpSXRr3ll7OeB6fXlBx4W44UfBY6P2k+4r6lqN0tMhJTp08Va+Ovg4R+PX/l9patPu1t7UvUpYsryalUfN8M/P48keR1nFNLb4tbt1bxHefrsvFVzk7fpz/dsJYInnSkp04yXRrJ6HrYmJ7woAAkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgwITfqvPTBovt73S5VqOh2NzjgbldRpzafOOFCWPBpt/FGyu0XdD2rtyrf040qtw5xpUqVSWFKTfPp5RTftwcp3Nepc16lavOU6lSTnJuTfNv2/A7HCdJ1K+rXtDh8Z1nTo6NO8vHrjLxzN79gm7O/0+G3LyVSV1b8dShJ84ullepnzWX8DRPiVek6neaPqVC/02tKldUXJxa8cxxhnc1umjUWpo8+HE4bq5092Jnad3aS6IFq21q1LXNDstRt8KFenGfCnnhfjH4PkXU8XVTNMzTO720TmMwAEsngJU99dq1t6lTHFKLUIwXWUm0or4to8NHtKtrax9LqKpdSfHVnFYUpdP4JL4FJRnDVNWlVabo2b4I9cOo28v24SX1i+JJdAABJVqRpwcpyUYpZbfRICcotS1O202znc31WNGjDCcnl4b9xQT1etdXSt9JpKqnHid1NPuY+xY5yfs5L2nvp+lwo1FWvKs7u7Tb72r0jnwjHOI+XL45Apq1fUdThGNhGVjRzzr1cSlJf2YrP0vBXWOmW1vOVRU+Ou+Uq1R8VR+xyfMr8IilgCEUorkWDdf5Ton7/AE/4MyAx/df5Ton7/T/gwMgaTWGeNza0Lqk6VzShWpPk4VIqUX8GewAsdxYXNhXp1dKnGVFJqdrVm+GXk4yw+HHljDz1WD2tdboVLunZ1ozt72UeJUaq9ZpdWscmi6tJ9Txu7O3u6PdXNKNSGeJZ6p+afVP2rmB68XMmMeav9Ho1KlPvdQtE+LuEm61Nfowb+fjyeHjqy6abf0L6GaNVSmknOm2lOGfCS8GBWkHHPiyIAstv3tlrEqElH0W4k5UceE/nSi/fzkvdIvRQ6rZK+tZU+JwqRfHTnFv1ZLo+RJpF/wCnUIzfqVYtwq0/0JrqvpAuIAAg2W/WtRjp+n1a8ubWIxXnJ8kvpLh7zDtWuql7q8qMXF2drya6uVTHL4JN/FI5nFuIU8P0tV+rfx9SKZrqimPKmtozzOtXjGNzVblVx5+Cz446Euo2/pdlVoZxKS9V+UlzT+DKhJLoQlz69D4hc1Ndy7N6Z7zOXaizTFvp+Fw2jq8rqz9GuuKF5btxqRmubXVP3YaMmXQ1vWqS0jWaGrQnig3GhcU1Fc03hSz7ORsG0uIXNvTrU3mE4qS+J9e/DfFI12liKp96HHrpmiqaZ8fzL3AB6SFQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPG7uaNrb1a1xUhSpUoOpOcnhRiubbfkejmlnPgsmqu2/eNDTNLq6JaTqfhK8pJzcM4p0c88tNY4lxJYz0fmZbNmq9XFFMbsV69TZomurw072gblutzbmv69WrTnZ0K9SjZuk8wdKMmoyT8eJZefajGCZv7OSJT21q3TaoiinaHg9Rem9cm5V5AAZGFt3sI3c7PUJaFqNzCNnUjxWvGlHhqcXOOfHOX1/RR0DGSbwmjiWEpQq0qtKcqdWlONSE4PDjJPKfwaOrezXddDdugUr2nxK4pf0FwnHhXepLix7PL2HmuL6Tkqi/TtO/1eu4Pq+tb6dW9P+GXt46lr166uaNtGnpyhO8qvhpxm8Rwucm34Yin8WvMuNR8lzwWfSo17y7rX9aUXQliNrHHOMPFt+PE1n3HFdldbOhG2taVGHzYRSy/H2nrJrDyyg1HVbXTqdL0mUnUqyUKcIRcpTk+iXl73he0ofRtTv7p+kTVnp0o47im/6Zv2zTaS9kcP2gVN3qtOLnRs4VLy5j1pW+G1/wCZtpL4so1o9bU4Up6/wVJU5qpCjRbUYyXjnk2/DyLzY2dGyoRo21ONOnHwj5+Lb8X7SoAlhFRiklhe4mAAAAAY/uv8p0T9/p/wZkBj+6/ynRP3+n/BgZAAAAAAPoy16lpNK+q0a7cqdzSy4VIPmvg+XVJ9PAugAsVO7vLCtUjqsHO2/wDl3NCDaS8eOK6e9LHmXa0uaFzRU7arTqwfjCWUes48WPLxXmWi60edOdS40mt6LdyecOUnRm/7UM4z7Vh+0C8NrD5lkuu/sNUp3UKcXZ13wXUm+cGuUZJeKy8P2cyajqzta1G11en3FzNNQqLDp1eFes085j7pY97Lje04XVpVoyTlGa4Xjw9oHvB5ZMWvRak6dOpa3E5VK9vLgc31nF84Sfm8dX5plydSKWX0ImcCzbm1H0KycKEl6ZWfBQTTfrPxa8kWC0t4WttTo0uJwgseu8vrn/qSzu62palXu60XGhHNK2TWHw5zKXxxH6vtKg+Sfizi3tmp6NE+5R/eW/orXbqz5A3gEGsnknReV1b07q2rUaqbhVg6cknjk1g9tjXTo1rnSaspp22O6UklxQbfNPx5NZGC1a5OvYwp6lYxlK4tm/VjLHFGSSa6Pxw/gd/8O8R9i1dM1fploa21mOpTvH+GyE+fUiU1jXVxQpVYvMJxU1JdGmipPtMTExlzgAEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQm8IC0bn1q22/o9zqN7LFKjBy4crMmk2orzbx0ORdU1O61jULi/vp8VevN1Hzb4U3lRTz4dDZXb1uunqmrWmjWFSFW2spOpcSX9cnhR+C5/83sNUJtJLy5HqOE6TpW+pVHer/Dy3GtZNVXRp2jcIESB13BAABP4Gb9ku66m2dzUqMkpWF/VhSrJyw4N5UZLw6tZb8jBssY4uTMd61TeomiraWzpdRVYuxXDr/Xr+jOo9OrTVCEqffV68pcMadJP1vW6Jvp16PPgQsrq71C0jHTKStLb5kateDcsJ4WKeVjplNvph88mvOxqvS3DbV7rUNSr1NbozcZQap4jTWOCUYuLw+a59ebNqRsqvCl6fdYXT1aX+g8RdtTarmirw91auRdoiunaUum6Tb2UFKLlVuH864qc5yfm2XHh5lH6JX/aF19Wl/oHotf9oXX1aX+gxsitBRei1/2hdfVpf6B6LX/aF19Wl/oArQUXotf9oXX1aX+gei1/2hdfVpf6AK0FF6LX/aF19Wl/oHotf9oXX1aX+gCtMf3X+U6J+/0/4Mufotf9oXX1aX+gsG6bWurnReLULl5v4Y9Sn5P+yBlgKL0Wv+0Lr6tL/QPRa/7Quvq0v9AFaCi9Fr/tC6+rS/0D0Wv+0Lr6tL/QBWgovRa/7Quvq0v9A9Fr/tC6+rS/0AVoKL0Wv+0Lr6tL/QPRa/7Quvq0v9AHtXtaVeMo1oRqQl1jOKaZa4abdafOpPTq6qW8nl21dtqPshLPq/FP4Fd6LX/aF19Wl/oJXa12v/3G6+rS/wBAFhutUoemu9lGpQvbSDhXtZvEp0uspRX5yXVPpywTbm1GpWsqVtp+O8u4SUpS604OPOWPPmsIqdwafQqaVXeqahcq1hHilLFNOPtTUcp+4xTbunKzt5TdWtUqVMKPfSUnGC+b0SXTD+3qed/EnFo4fpZ5Z9+rtH+1rdvq1xQu8IKMVGKSjHkkvImC6A+M1TNU5nd3IpxGIAAQsBrkA1lYYiZjZExmMKjat5K1uammVsOmk6ltL+xnnF+1N/Q/YZXF5MB1Ci5RhcUYcd3bN1KPtl5e59DMdJvaWoWULi3cuCXhKPC170fX/wALcX9u03TuT79LiXbM2a8ePCuQCB6pjAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADEe07dC2ptipfUlGd3KcadCD5qUm+ef+XL+gyqpOMKcpSaSXNt+C8zlvtb3PPcm7K8bes5abZPuaEVU4oTkutTC5ZecfA3eH6WdTdiJ2jdp67UxprU1efDDa9SdevVr1m51q0nOpN9ZSfVs8n1I9ORB9T2MRjZ4Wqqap5pQABKAAACJAjHqBf8AY+4K22NyWepQlV7inLgrwgk+Kk+Ulhte9e1I600m8o3+m213bz46NeCqU5PlmLWU/oOLspv/ANjd/YBum4uKVfb99V4o28OO0lKfrOOW5Rx7Mr4I4nF9Lz09enxu9FwTV9+hVP0bu5AlXREx5t6UAAAAAAAAMe3Wl6TonL/6+H8GZCY/uv8AKdE/f6f8GBkAAAAAAAAAAAEksKLzyJ30LfrF7Gy02vX+dKMfVXnJ8l9rRWuqKKZqq2gnZj+47qV9qP4NXDK0pxVSvz+dLrGD9mOb93tPDCSSisJcsYx9h5WVKcI1KleXFcVpurVaeVxN9F7EsRXsRUHxLjvE6uI6uqvPux2j6OrpLPJTzTvIugAOK2wAAAAAfNE+i3voGpzoVI1FQu+FwlFZjCp0afllcOPiSFPfUXXtpQptRqdYyf5rXNP6cHV4LxGrh2qpuxttP0a2qtdSjtvDOqbzHP8AEmLPtm+neaXT9KcfS6WadZJ9ZLx6LGevxLuuZ9wtXabtEV0bS48TlEAGRIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfQFLqN9RsLK4urmShQoQlUqSb+bFLLYiJmcQNedtu66Wj7Zr6ZRqx/COoR7uME3xQpPKlPl7se9nNsVGEVGEVGKWEl4F93tuGruzclXVa9NQTgqVGHDzhBe32vn8SxNHseH6X2ezETvO7xnFdV172KdoG8kADecsAAAAAAABEqtL1C70rUKF/p1R07ug3KDzjPLDT9/QpCKeF0K1UxVExLJauTbriuneHZG2tcttd0W1v7ScJQqwi5KMs8Emk3F+1ZRdzm/sL3XDRdYq6NfVJRtdSqxdumm+Gt0fweI/E6QPGazTTprs0Tt4e60mojUWorgABqtkAAAAADH91/lOifv9P8AgzIDH91/lOifv9P+DAyAAAAAAAAAAgwEpYT9xg2p16WsauqkKjlRsJOEVFvhlU5qWfPGFj2ovm6NVqWNrGjapSvLhuFKPkvGXwTyWG1tqVrRjSt4RjTj5Lx8zxP4w4vGns+yW596rf6MumtzcufKN3vEiQj1Inyx2wAAAAAAABkEmuj5kQBSU6y0jVIahmToTj3Nxzfqr82Xv4sLPt9hnkZKUYyjzTWcmF1YRnGSn8xrhftRdNqajOpbOwuvym1Si54xGcfzZJ+PLCftyfTPwbxbqW50dye8d4+jj6qz068xtLIwAe9a4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMgAMoZAAZGUAAGQIM0j8oDc9SEbfQtOuXBycp3ihJc4YSVOS68+JP3YNob23DQ2xt681W4ipxoxXDBy4eOTeEs+ByHqWsLVNQudQu69F3VzUdWbUlhOTzhexdF7Dr8I0nUudWraP8ALl8U1NVm1y0by8lySS5JETx9Iof11L66HpFD+upfXR6fMPIdOv0l6g8vSKP9dT+uh6RR/rqf10Mo6dfpL1B5ekUf66n9dD0ij/XU/roZOnX6S9QeXpFH+up/XQ9Io/11P66GTp1+kvUHj6Vb/rFH66Hpdv8ArFH66JOlX6S9iJ4el2/6xR+uh6Xb/rFH66CelX6SqaVSdGrTq0pyhUpyU4yi8NSTyn9KOq+y7c1TdG1be8uJP0qEpUa+UlmcfFY8Gmmcmq7tsflFH66Mx7JN1UNA3lazdxS9FvGratma5Jvk/g1H6Tm8T0vXtc1P6qXX4TfrsXOSuPdl1mDwo16dRKUKkZQaypReU/ievHH9JHksTD1XNHqmBLxx/SQ44/pInEnNHqmBLxx/SQ44/pIYk5o9Uxj+6/ynRP3+n/Bl+44/pIx/dUlK50TDTxfwzjw5MYk5qfVkQJeOP6SHHH9JDEnNHqmBLxx/SQ44/pIYk5o9UwJeOP6SHHH9JDBzR6piSbUW23hIj3keXrLmYhvfVrRxWl+m06FWslKrLvFGUKfPmm/FtYNbVainTWqrte0E1xHlRyk77U6+oVJOVNy4beMlyhBcsr39foKkoY6npsUoxvrNRSwkq0cfxI/hXTv1+0/xo/efEOIXb+t1FV+ume/y8OtYqtWqIp5oVqWAUX4V079ftP8AGj94/Cunfr9p/jR+80uhc+Gfsze0WvihWgovwrpyWXf2mP8A70fvI/hTT2s+nWuP/ux+8dC58M/Y69r4oVgKP8Kaf+vWv+LH7x+FNP8A161/xY/eOhc+Gfsnr2/ihWAo/wAKaf8Ar1r/AIsfvH4U0/8AXrX/ABY/eOhc+Gfsde38UKwFH+FNP/XrX/Fj94/Cmn/r1r/ix+8dC58M/Y69v4oVb6lNdTr2tSle2j9ei81FjLnS6yiva8ciX8Kaf+vWv+LH7x+FNPaw761/xY/ebOjr1GkvU3rcTEx8mK9VbuUzTNUM6t60a1KFSnJuEkpJ+zB6rqYVszU7ShevSKFanVpyTrUHCalwr86PL+1l+6S8jNUfcNFqo1dmm9TG7kROUQMrIyjbSAAABkZQADKGQAGRkABkZAAAAAAAAAAAAAAAAAFDrGpUNK0u7v7rKoW1OVWeOuEsvHtKzqas7eNeVhoEdKpylGte4bx04IyWUVrqimmapbWh006rUU2Y8yyHZ3aFpO6tSr2NjTuqValDj/p4xSks45YkzL+NdHyOUdF1a321relahpV46mIx9KjGEvV6OfXrlZ+g2h2x09Bp0be9v61/LUK9Fwt6NtUSTx4v2ZZr2781ROd4drXcFot6mi3azyV7dszmN/Rt1VYt4yvpJu8j58kaL2VtG803aeq61qVWrC5qWlSFKiqmcR5es+eM+qW7SLiX8i+u8VaTq+mpR9ZtpcdP/wBy9N7MRMxu1quD0zVNNu5zRFUU7ev7+G5t4bsstrWFO7vqderTnLhSoKLefi0XqxuoXllQuqakoVoRqRUuqTWeZzRXnKfZfDjnKTV616zzy7uH3nRe2/8Aw9pn7vT/APxRNF3nmPplj4hw6nSWonOZ5pj7LlJZ8hwRfgiZIYMrj4iUvBHyHBHyJ8DBJiEnBHyQ4I+SJ8DAyYhJwR8kOCPkifAwMmIScEfJDgj5InwMDJiEnBH9GP0DgX6MfoJwMyYhJwL9GP0DgX6MfoJwMyYhJwL9GP0Fo3JoFlr+nu0v4yVJzjPNNqLynlc8F6IOOSYqmJzCJpiqMTDmndtlHb+uV9LtZVYwoYcIzkpPu2lwtPr55z7Czu7rJ/8AEb+j7jenavtuprG341rONL0m0n30nKPrTpqL4oJpdXyfvSNBcusWnHwa6MX6q8RXTLgV6Wi1cmiqM57w93d1/wCsf2D0qv8A1j+z7jwBqxdufFJ0aPhe/pVf+sf2fcPSq/8AWP7PuPADrXPik6NHwvf0qv8A1j+z7hK6rSxmo/V5r3ngCetc+KToUfC9/Sq/9Y/s+4elV/6x/Z9x4AjrXPik6NHwvf0qv/WP7PuHpVf+sf2fceAJ61z4pOjR8L39Kr/1j+z7gruv/WP7DwIr2kda58Uo6NHoqqVzdTqRhTqy4pPBtXs30GnX7ype23HS4ecm3hT5ck17H9phOxtHnqWpU4OLSlNJJ+XVt+zBv/SdNt9Os40LaLUU8uTfOT82zFVzX6+Sqc0xv9W1w/T0zVN6IxG0KNbY0hr8jX15fePxX0f9TX15feXpLC6kcGb2Wz8EfZ2Fk/FfR/1NfXl94/FfR/1NfXl95e8DA9ksfBH2MysU9q6POEoys4uElhrjlz+0utva0qFONOlCMYR6JIqMDBaixbo/TTEIxGcpO7h+gvoHdw/QX0E+BgvyU+iUndw/QX0Du4foL6CfAwOSn0Endw/QX0Du4foL6CfAwOSn0Endw/QX0Du4for6CfAwOSn0HnCmovKSJpPCJksElXCi3Lmi0Rgx6MV3vvnTNnu1jqNK5qzueJxjQjFtJYy3lrlzRftI1O31XTbe+tG5Ua0I1I56pNZ5nOW99Xtdxb6vvwted1ZWneUKD4ZNZT6eqn4pvPsMr7Gdc49ta1pderJyowlKiv7Li1hP4GpF/N3kz2el1PBItaKm9GeftM+mJ/15bsdWOM5X0kFUj1b5dTlzR7KtqlluadW8uYU7GnK5hCNRrinnx+joe1Oyvrvs/wDwjcandyjbVFChRdWWIL1s/HK5ZLRqu+Mev9ifw7icdXvmI28zt5dP94sMhGcX7fZk0HqGu3V72XaStR1mvb1bipUhLu6bnVrKM1FJPKSxnx8zHL+tV2/uWwuNFV/aRqQjUgrqrCU6ibkukW8L2Nk16iKY5ohgtcCquVVUTXiYzG3acfP/APXT/GsvoWfUNednq1CyWl6lcd60nc0aUXRhl49aXFlY8eRpW+s6+u9r91pyuq9GlcLFSVOo4vhVOLwvo+0qd16dS0PtP2xYWEqkaFP0fk5fOfH1fm3gmq9jOI2RTweiJppmvMzTNWMbNl0+0DTJ6vqmnK2u++06E51JOMeGSisvh9bn8cFZs7eVhuqNy7Cjc0u4aUu+jFZy2uWJPyZqS2/8eb1/d7n/APEs+3dbvND2Zrtzp1Tuq860KXGllwTdTLXtKxdqzHpmYbVfBrVVuen+r3Md/i3dK8cX44ZMpc8czmm40OVjsq13HQ1eutUdduUu9eH979pvDs41S51naGn39/JSuKsXxSSwniTRa3f55iMb93N13DJ0tvq01c0Zmme2O8MpQIRImw5QAAAAAAAAAAAAAh5mA7r7PqO5N02mrXl9LuqChH0bu8xlGMuJpvPjnyM/GEVqpiqMSzWNRc09XPanEtfbo7MND1exp0LGhQ0yrCak61CjHMks+q/Ncyy6n2QrU1a+ka5WboUo0k3RzlLp+cbbwvJDBSqzRPhtWeK6yzERRcnt++/1a02x2ZLRdU9JuNZur6lwSg6FSD4ZKSxz5vJa/wCRq1764cdXue5qNyjS7tcMX4Z8Hy9xt/CA6FGMYTHF9ZTVNdNeJn5Q1hPssovbK0f8J1FFVnW73ulnPCo4xn2I2HplqrKwt7VSclRpxgm/HCx/0KwFqbdNOMNa/q72o7XKs98/vIugALtcAAAAAAAAAAAAAAAAAAElVJwxJcjTfazs+Nrw6tpdBxpyfDXp0oZUeUm58lyWUuvmbnayUt/bxubWrQmm4VYOEse1ETGYww3rUXKe7lEF23Lol1oWqVLS7pTgs5pycfVmuT5Po8Zxy8S0mrVGJw5IACqQAAAAAAAA9aFKdWrGNOLlmSWEea9hnmwNtXOo15ShF9xiLlUeMJPrj4dPMiuZppzHdSYquVRap8s47MNL7ijXrtZgsQjJ4eX1l9HL6TYUPmnjaUIW9CNKnFKMUkuWD3M+ntzbo5Z7z5du3bpt0xRTtAADOuAAAAAAAAAAAAAAAAFNqFKda1nTp1O7k18/GcFSHz6hMTicw1ttbss0nSHdz1LutWlWaadxRXqYznGcvLzzPCh2VWtnuOep2GpVLek5TatlTXClJSXDya5JS5e42hhDC8kY+lR6N78z1czNU1z37S1npHZfS0211qjHU6lT8J0XRk3SS7vLbz159T0odm1OhtKpoa1KbhUnxd73SyuUuWM+cjZGF5DBHRoznH8knieqmczX5if3jZq+67K7S423YaVLUayq2kpyp3Cpr8+Sbyvh5lJLsftp29KH4WuJXNOfFOvUpZclhrC59OfmzbeF5DC8h0aJ3haOL6yImIr9Z8ed2CWexadtvmW5FfTlNpruO6wsuKj1z7PLxI7i2PT1nd9hrtS+qUpWnBilGllS4XnqZ1heQwvJFpt07MHtt/mivm745f29GvqXZ7Tp63rGpen1G9ShUg4d1yp8ax1zzJts9ndnpGm6jY3Vx6bRvVh8dNLh+dzXXn6xn+F5DC8kOnTHeIWnX6iqnlmrt2/ts09Q7F7SFxB1NauZ2cJ8aoumv45x9htmztaNpQhQtqcKdKHzYwWEvgVGARRaponNMI1Wu1GrxF6rONkERAMjUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABkCIAwPtY2/LV9E9JtaUJXlq+NSfV00nxRXv5GhJRcZOL6rqdaVIKaaaTT6pnNW+tHq6PuO9pTT7qVTjhLCSafP/v4mK5RmMw52sp5aoq8Sx8AGu1QAAAAAAJqUJVa0KcesnheZMRnsrM4VuhWT1DVbW0UoqpWqKnBN9W+f/Q6V29pVLSNOoW1GMY8EEpNLq/Mx3sx0COkaHSrVI5ubhcTk236vh9/xM1XU2KbUUzmd29o9PyZu1bz/hEAGRvgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIMwTtT25PV9Gdeypd5e0nHHm4Z5r4ZbM8ZLJLhkGO5bi5TNMuTKkHTnKEliUeUl5MkM37UNurRtUjcUUu4upTmmn05rl8PZ5owg1a6eWcOP3iZireAAFEgAAGcdlu25atq0LytSzZ21RSlPix6y5pY8TDrO1qXlzToUVmpNpLLx/34HSe1dCt9C0ina0IxUnidVrnmeFkz2qf+Usli11a++0L5GKj06eBNgAzOuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH0YAFh3folLXNFr29WK41FuDxnDOa763qWd3UoVk1KDxzTR1i1lYNN9se3nC6Wr0Kcu7lFKo1HKzlLm/ArXTzUudrbeP/JT+7VgI/8AfUgajUyEcZ8M+wgXPbuk19a1OFpbQlKT5vEksLzLUxmcImWwexzb3e1aup3UIuMcKlFrPPGc8/f/AANxQzwLPUpNIs4WOn0bal8ynFRWepWo28Y7OrprXSox5AAGwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADWS3a/p1LVNMr2deMZQqxaxJZSfgy4kGs9QiqIqjEuW9zabU0nXLqyqJRdOXJN+D6FsN39ru2431hHULSnFXNJ/wBI8fPj7fcaQNe7Ricxs4tVHSqmj0RSbaS5t8jePZJtqFjpUNQuoR9Kq5UWmn6hrXs+0J61r1sp8PcUpqU8pvOFxfcjoqhThSpRhSjGMIrCUVhJGS1TiMyz6S3Fyrn8Q9YrCIhAyOoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgAeVWCqwnCUcxaxhnOm9du19I3FUoRhUdGtUzSk4v1stdPP5x0gWjWNCs9WuLWreQcpW1RVI8+TaeeZE0xVGJamqsTdiJp3haezrQPwBolOjVhmvUk5znwrL8sv3GWEIrDJi0ti3RFFMUwAAhcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACEvmvx5AS955rBHj9hytquqbg2J8onTbbUNX1CvoWrXXHTp17ibpRhUm4YSbx6rw+Xg0VvbBrmra32/7e2rpOqajY2lONONwrS4nDi4m5yclF88RSXMVdpn5Ed8Y8unYyUllDwNL737YbfY2p2+1dC0W/wBy6ta0oQqU6dX1oxUVzbUZycumfV+JVdnPbVb7wjq9rcaNX0nWdMozuKtnXrJ5jHPJS4U084TTjy4l1I7+Ub7Nve4g5Y6nPW1vlF3O5bq0tNO2VdVLircxo1HC8c6dGDaXeTkqPJc34Jcup7XnyirSpua+07T9uX2oaVat8eoWlXvJY/S7tRwk5ck+P7icTG5M4b/41jPgRTyjl35M+9qtntjeOq7q1m7uLW0nR4J3lzKo1mNT1Y8T5N4WEXSh8pa64FeVdganDRFPE76Nw5KMW+v/AAlHPs4viR6pdHZIlk2luGz3Pt/T9Y02blaXlKNSKljii31jLHinlP2ovYicmMbgAJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPowH0A56+V1tmvf7Z0vcFhDNbR6s5VZZxwwlw88eL4oxLL8mezut7bx17tB1yMFdxrRo0u75R4u7amuHyw4HTVWkqkHGcIzi+qksohbUIUIuNKnCnF88RWCK/eiIKJ5cy417aNt2+ids+o65vjS9Qr7U1CpmFWyklKU+6WFFtpZyuaZe+wzb9rc3m5NwaRoNzYaBGwuadndV67lUrLjTimsuOUqeW0+TR1dXt6ddrvqUKkVzSmspfSSwt4048FOlGEP0YpJfR8SZ7zkjMRhzL8j+0V1s3d8Icq1WqqKy+WHTf3mK9j+t6t2a7p1nad7odzea1Wi+4o2yUpSm+F825JcHDHOVzOxadrTpc6VKEG3zUFjP0dSV2lN1e97mn3n6fCuL6ev2jxMfPJvP7YcTdmO1tT3R2U750rSKCqX9O8tqqpcWG1BVOJL2+wxnRaO1a2kQ0ee3tZut8O47iNFVVG3k+LDTxLiUlh+HU+gFK2p0W+5pQp568KSySqyoKSl6PSUk88XAs588jPbAxDsb0K4252e6Rp13ZKxrwhKc7ZVHU7pym5cOX1azz9pnXiQ5kSIjEEzkABIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJZNrLfgWe73No1ncdxd6vp9Cty/o6leMZc+nJvJae0/VrnR9oX1zZJyryxTWM5ipcsr3GsOy/Yej7j2vdX2rd66zqThGaqtcGF1fgzFcrmmYppjvLq6TQW69POqv1TFMTjtGZy3zQrRrQU6c4zg1ylF5TPXJq7SadHs77PL69stSjq9KMs05Rx3fE5KKS4W+WevPqYpV3pv600inuKsrZ6TVnmNOVOOMN8s/nJe0rVeiin3o7ptcJr1EzNmqOXOIz2zPyhuvUtYsNMlCOo39tauo8Q76rGHF7slwTeDQXaxqsNctNo38PVdZOUkuib4MoyPeu89ejumjtva9OlG7cISdWouJttN4w+SWMcys6inM/st+UXKqKJpnvPNnO0crbWRk1Hsveuv0921tu7qVB1lBuNWCw1JLKXLljGSwbW3dvrcl5Wt9OuLfgoVFKrUdOKcINtYWeXh5eBPtFPpOdsI/Jb8c0zVERTETnPaYnbDfeeRFt45HPu4O1TWrnWZ0dHvLays6aUU6sVmcsc220/HPJIvW0+0vWL7RtShVtqd9qVpGNSEqUWlVjxqLzFeXPp5ExfpmrlWucB1Vu31KsePPeM7Nz5fiRTOfrLtC125tqFxHcVlG7lVjxWNW2UIcOV0n5eGTd+i3da80yhcXEaUak45lGjU7yKfskupai9FycQ1NXw+7pMdTC5ZGTTe5d5bntu0ato2jd3cQccUreUEstwznPXlzfUjsveuv1Na1jSNyVafpdvRnOm1BR4Zx8OXJ5ysFZ1FMVcv7M9XBtRFrqxMbRVjPfE/JuGc1CLlJpJc22+hb9O1zTdRr1KNhqFpdVqazKnRrRnKK5dUny6mmNn7t3xuWpKNtXoTt6NVO4qunCLVPxST5dE30KfaO69S73cMqfolKdGhGVOpTtKcJZdSMW5YSz1Ji/TP3Z54Fep54mYzTjafX1dARb8WRyc6fyib2raA9Tp1reNpRrdzUrd1FuUmk0seCX/Urb/tH3hb0dO1StRtaGn1eXdxgpd7jGW23lcmuhj9qo9JWn8OaqJxzU+m/n0+rfybwQyaq3fvDX/wAC6Jc6JG2s1fUoValzXnBRjxRT4Vx4XjzLFsntH1ypumOma1dW19QqyVNToRjiMnJLlKPJrn5F6tRTR2lrWuD6i7aqu0zHbPbPfs3i5NPqW+Os6fU1Kenw1C1d9FetbqrF1F/y9TTGmbx3vreuahpuj1repOnUfDKdOK7uKljr0+nJctC1u9j2sXOnXPo1SNOM1KorenGo3wp/PUckzejx64Xq4Ndt80VzGYp5sRPhuZPIUnk0VR3hvnctfUrzbno9HTbZvhjKEW+Hnzy+bfq9OXUvOl9o2o6h2d6rqsLel+FLFxi4xi+HEnH1sexNv4CL9Mzj+dlK+DaiiInMT3iJiJ2ztlt3ORk0Nou/tdr19MqQ16wu6lV/09lXpxt1Hry4zeVnOVS3hOXDmUU/VeV08H4k27sXNmtrNDc0cxTcwqF0AXQGVpgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC1bh0i31zSrixvIOVKrFp4eGnzwzT1htXtC29Z3em6R6NUsazbzGpHxznHFhp9PA3qQxzMdduK8Zb2l4hc0tE24iJpnviYzGfVq7Qezy6p7AvtG1S4dS7uHxRSqt06bTTillcua58jFK+yd+3GlUtuzVqtIpTxGo6sfmptp8syfuwjfmBj2lOhTiI9Ge3xnUUVVVdpzOe8bT6w0/vbYGpV7DbdpolBVoWCxVcqkY/o+eM9GVe+Nla1+MtDcW1nD8IRjFThOS5vDXLPLpjPM2rgYJ6NPf+bK0cX1FPLtOM7+ebfLU2ydk65U3bW3Du1UI3Dg1ThTmm1LplpLCWPJs9+x/aGrbcutXqavbwpRuFBQaqRk3jiz0fLqbSS5dQ0RTYpic+UXuK37tNdNWMVREY9IjbDRetdnOuaXuG4udt2dnfWVVZUbtU58LeOJNT9vkZNtLZutaVoN3cTdjR164XDGKp0+6pxznGYw8s+DRs7HtHCTFmmKpqha9xe/foiiuI7Y747zj1c+X+wN0agp0qu3tIo1Kk/WuqM4wcefzlGLx7emTdW1NKloug2thUqyrToxac5PLfNsvPCHFvx5E02qaaubyxariN3VURbqiIiPRz/uOlf1u22pT0ipGjeuOadR+DVJvHTnlZMg2PsnXqesa1rG46VNXtzRnTppVIy4pS6vlyS5JI2Mtq6StfWtK2/wD1LGO945eWOmcdPYXrhMcaeOeqqrzLcv8AGa6rVNq1ER7sUzPns1j2RbU1XbumatT1a2jRq3El3ajOMsrD8n7TGtvbB3DZ/h/0izhH0ugoUv6WL4n3kX58uSZvTAwZIs0xERHjuwxxjURXXX2zXjP7NFWuwNw0+ze+0mdnBX1W+jXjDvY4cFFLOc48GVG4ti6/ebN0awt7SErq3i1Uj3sVjPB45/ss3bgYKxp6I7/LC/55qeeK+2c823nGGjdz7D3TdWGg+iRpXMbW1pUqlpVrLghOMEm8Pk+ftJNN7PNwWu6dM1StaW7/AKWFS4VGUIRp4ceiWM8l7TeuPYMETp6Z3/mE08d1NFHTiIx3jb1at7Mdp6voe6tZvdSto07e4z3UlUjJv1s9EyTTtn6tS7WbvWq9vBaZVU8VFUi3zjjpnJtXBBrmX6NP98teril+quuucZqjln6fyGjYbP3vtu41Oy2vGjU0y4eIOdWGeHnhetzTWTItA2Jqe3tj6hZ2MrStqty1KUaqUqUumY815Z6+JtDBHBWmxTTVmF7vF792mKZiPGe2+NsufZdne5dUvLaN1o2nad/SKdS7oVEpJe5NpeXJG99Ltnaafb28pObpQUXJvLeFgquEii1FqKZmYYdZxC7q4im5iIj0RXQAGVogAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC36zqlpo2n3F/qdxTt7ShBznUn0SSbfv6dDnhfKI3LqVO5vtt7DuL3R7d4qXOak1DCy+KUY4XLmbF+Udpmoar2V6pQ0qNSdeMo1ZRhzbhF5l9hrP5OO99p7f7J7+31nU9Pt7mnWrVK1rWrRhUrRwsKMZPMsrlyLTGKYmPKsd5nPhuPsu7Q7Pf21pavQouzlTm6dWjUlxcDXjnGGi6Ud97Xraj6BS3Ho071ScHQje03U4l1XDxZNI7g3pp2tfJ63FdbC0evotrQlCjUp06UYRip1Icbi49cxbz5ZfM1LuDRth0exDSdV0+8tp70qVo+kRV43W5zlxJ0uLEUuXPh6YfiViOy28y6V7Ze1O82BqWgW1np1vew1OcoOU6jjwJOKysdfnGwdZ3Fpeh0Y1ta1Ky0+i8Ljuq8aay84WW/Yzk3tjub+82z2SV9W4/TJ0k5uaxKX/AAub9pcu1Nabq/ylbfTN93SpbchSgoxr1nQpcPdtrM01hOTfPKI37+MGJxj5uotF3FpOuUZVtG1Oxv6Uc5nbV41EufmmWyHaHs+cOOO6NEcOPus+m016/wCj16+w5u7LY2Gj/KQ1DTth14Vdu1KNRSVCu61JwUE88bbylLo8vmWn5MOx9F3bqW4LrXrH0x6dKlO1jKtOChUbk+LEZLPzY8nyJxhMx6OsNZ3ltzRKsKOs67pdhWnHjjTubqFOTXniTXIrtP1iy1KwV9p95bXVnJZjVo1Yyi/jnHkcJUHebk31q1Tc8tBrajhp/jFd1rOCjHhUcSpzhz4UsJvmueGZv2Y6bdz2jvHTYbq0ux0WsoShVpV60La3r99BvFSrFJ5ilHlJ55dS004qxKP+OXTsO0TaFS6ja0t0aFO5lPu1SV/TcnLOMYznJk8Jtx54966HDUtC1Lbm3qVxrG0dq69ty2uIupq1ndcVWceSajUo1ot8uazF83k7D7PtYtNe2fpupaXCvTs7iDlTjXk5VIrLSy2238Skz3xCcdlTre7tv6FWjS1vXNM0+pL5sbq6hTcl7m8lVpWt2GsWiu9JvrW8tHzVahVjUg+eOTT9hzB2kbT1e87Z9W1Xb0drbpuqi4PwLe3VOrWpYhFS4qLnFrGOqfLJ69m2vU6GwN+2GhaB+A9x2NtVlexs6tWrS4l3kVwRnUnhrCXJ8/DkTjvEIz7sy6Frb/2pRvnZVNy6LC8UlDuZXlNTy+ixnqXqvqdrQs/S7i6t6Vsll1pzSh9bofPnRNJtLzZ11d3FfaNCoqrXpF5fXMb6DyscNGEmpR9vdvk3z5ZWw+0C51i3+TltS0q3sbuwndyhKvRjPhqU058CzKKeFjly8CcZlE9odXaVvfbWr3btdJ3DpF7c4z3VvdwqSx7otsqbndGi2usUtJudWsaWqVWu7tJ14xqzz0xBvL+COcae3OxLTtP29fW+56umaoowm62l3sritOpiLxUhw1ODnnlwx+JT9pl7bab8qTbt7qFxC2tacLeVSvcNU4xis85N4S/76Fd4mU/8oh0vDdGiz1uejR1axerwWZWSrxdaK4eLnDPEuXPoUEu0DaUYV5Pc2i8NCXBVfplP+jlz5PnyfJ8jnfaeo2Ws/K+vb7TLqld2k4VFCvRkpwnw2qg2msp80+aMZ7Htl6LvLc++obhtJXMLOFSrQSrThwzcpLi9VrPTx5cy2O8/sj/TsHTdwaVqmnT1DTNTsruxhniuKNaM6ccdcyTxy95r/tI7YdI2/tC51fbV/pGt3VCrCnK3pXkZYUnjL4W2c8dm17f0Pk+dosbKUlCNaknw/mxlhSx70WbUtI2PS7CbLUbK6tnvGpXUa1NXjdZx43lSo8WFFJLD4V72Ij1POHYm1N7W2pbC0vcutVbTSqV5SVSSrVlGEG88uKWPIrtL3ztfV7qFrpW4tHvLqo8RpULynOT8eSTyzmreF9tiPYp2eafuqesTjUpOrRtdNq06cZyWE5TlUi8Y4+XNdWYRqdtpWkdq215baoaRaWruaM4R0/UJXko5qf8AzKjnJceOTUWl7CsRPJlMd6sOzb7eu3NPv7ixvte0q3vbaPHWoVbqEZ044Usyi3lLDTMB1DtXvnv2w0nRaO3r/RLuvRpwvY6xQ72UZuCk40uPibTlJJJc8I1HuPbtluz5Xl/o+sUu/wBOrRh6RS7yUO8jGyhJLMWmvWjHoy2bt0qy0P5SmhaZpVH0eytdQ0+nRpKTlwxXcYWXzfxZaY7VfJEfqpifLq/Wd47f0OpCnrWuaXp9WabjC6uYUm8deUmit0/W7DUNOV/Y31rc2PDxekUaqnTx/wCZcjkHY9ltPc3aXvOXaveUYSp1KncK8u3bLPePOGpLLSxy5+48OySd9e6DvTQLPVla7YqWFWVK6vJqjQhXcqSTlN44cptY9omMd/GcJjv2nfGXVr7RNoK6dr+NGh+k8XB3Xp1Pi4s4xjiyZTSlxxUuWHzWDhe70TVNrbRpXGr7Q2jrm3Le5jnWrW5dSpVbl81VaNaEnjLXzcZxk7P2RqNrqu1dKvbCnVpWta1o1KUKrk5Ri6cWk2222k0m8vLKzOJwRsvgAJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHnXpRrUZ0qkFOE4uMoyWU0/Bmr9R7Buz2+vJXFTQFTnJ5caNxUpx+rGSRtQAY5p+z9C03QK2iWWk2tLSqyxUtlD1Z5xniz16GH0uwfs+pai72O34urxOXBOvUlTy/7HFjHsNpgjxg85YZuvs525uqelPWdPlUWmcrVU60qcaa5eEWv0UN6dnG2N5uMtxaRSuaySiq8ZSp1MLw4otPHPoZmCY7EsM2X2b7X2bKpPb+k07atUi4zrOcp1HF4eOKTbxyJtjdnm3tjyvHtyxnbO7cXW4q86nG45w/Wbx1fQzEEYGu91djmyd06pLUNX0WMruaUZzo1p0uLHJZUWk3hLmXXTOz7bOm7ZnoFtots9KqY7yjUXH3jTTzJvm3lLr5GXgn6kdmsbbsU2XRpwpR0+89GjUVT0aWoV5UW0886blw+HTBsLT7C306zp2lhQp29tTWIUqcVGMV7EisBGBgeu9le1da16prVzYV6OqVPn3FpdVLec+nVwkvIr9sbC29tm0urbSdLpU4XacbidRupUrp5b45Sbcur6mWgme6MNVT7Bez2eoq8/ACjVU1PhjcVFDl/YzjHwMy1fZmg6tt6nod9pVrU0qnjgt+HEYY6OOMY+BkYHnKWsdF7DdhaNfwvLPQoyrweYuvXqVVF5zlRk2sr3F13t2XbU3rqNC+3DpfpF1RhwRqQqzp5jnKT4WsmcgDX+3eyXaG3Nfo61oulO11ClB04yhXnw8LjwvMW2s48ce0rNrdm229r3Wp3OjWM6FbUouFy3XnPjTbbxxN45t9DNARgYPtrsy2xtvRdU0nStNUdP1JL0mlVqzqqpyax6z5dX0wWGj2Cdn1KjVpQ0FqNXHE3dVXJc8+q+LK6eBtYEjANY7KNpaxt/TNG1HSnVsdOi42se+mp00+q401Jr3soKHYhsS3q2FW30TuqtlU72lOFxUUlLKeZNSzLGPHJs4AYhT7Pdv0t8VN3ws5/h+ceB3HfTw/U4Pm54fmrHQpdU7MNsapu6lue9salTWaVanXhWVecUpU+Hh9XOOXBHw8DOQRg+bXe6ex3ZO6L932raJB3Tbc6lCpOi5tvLcuBrLLjpPZ1tnS9t19BstHoQ0u4TValL1+8zjnKT5t8vMzMEjWL7EtldwrdafeeiKfeK2eoV3RznOe7c+H7DYun2lKxtKdrbU40relFQpwisKMUsJJeSWF8CpBGAABIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/2Q==\"]";
            //List<string> arrSamples = JsonConvert.DeserializeObject<List<string>>(sSamples);

            //APIRest.Models.ImageSample isample = ConvertStringToImageSample("code1", arrSamples[0]);
            //isample = ConvertStringToImageSample("code1", arrSamples[1]);

            ////string json = "{\"nombrefirmante\":\"Gonzalo Mansilla\",\"rutfirmante\":\"15988779-0\",\"telefonofirmante\":\"995744080\",\"mailfirmante\":\"gonzalo.mansilla@biometrika.cl\",\"calle\":\"Avenida Salesianos 1000\",\"numero\":\"111\",\"departamento\":\"15B\",\"tipovivienda\":\"Depa\",\"comuna\":\"San miguel\",\"ciudad\":\"Santiago\",\"marcavehiculo\":\"Hyundai\",\"modelovehiculo\":\"Yaris\",\"patentevehiculo\":\"ABCD55-7\",\"aniovehiculo\":\"2016\",\"colorvehiculo\":\"Beige\"}";
            ////Dictionary<string, string> clavevalor = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            //int i = 0;


            //string s = System.IO.File.ReadAllText(@"c:\tmp\testb64.txt");
            //string ret = Bio.Portal.Server.Services.Core.Services.BPUnitOfWork.UploadVideo(7, "trackid", s);
            ////string saux = APIRest.Models.StepName.autorization.ToString();

            //int i = 0;

            /*
                0-JPG
                1-Jumio
                2-FacetecOnboarding
                3-FacetecAuthentication
                4-String
                5-URL
            */
            //List<APIRest.Models.Step> wf = new List<APIRest.Models.Step>();
            //APIRest.Models.Step step = new APIRest.Models.Step(0, "geolocatization", 3);
            //wf.Add(step);
            //step = new APIRest.Models.Step(1, "signaturemanual", 0);
            //wf.Add(step);
            //step = new APIRest.Models.Step(2, "jumio", 1);
            //step.metadata = new Dictionary<string, string>();
            //step.metadata.Add("urlservicio3ro", "https://biometrika.netverify.com/web/v4/app?authorizationToken=eyJhbGciOiJIUzUxMiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAB3KsQ7CIBAA0H-5uZdw5TigGzEmstQFPwA4mDs4aIz_buOb3wfGKz1hIxfFkfVEjmWBmhU2oNZsjU6wTu-Ru1QMOhh1BJ1CNHxTWKD_Mwez-jgFjdCKbO3AZrjiVGuZOked88xHeR_nTo9yu-4lX1LJ9x2-P-wO-MWIAAAA.jQg42moj3N9x7fVQQyhGh4qM1muAM5A8pK4dz0KCI-7b_BlbaVYUU7z7yP5vmN8mNmqtpW8Dd3YYwlmtjjdSvA&locale=es");
            //wf.Add(step);
            //step = new APIRest.Models.Step(3, "faceteconboarding", 2);
            //step.metadata = new Dictionary<string, string>();
            //step.metadata.Add("licensefacetec", "lkalfkhalkhflkahsflkahsdl");
            //step.metadata.Add("licenseforexncrypt", "-----BEGIN LICENSE------lkalfkhalkhflkahsflkahsdl...----END LICENSE----");
            //step.metadata.Add("session", "lkalfkhalkhflkahsflkahsdl");
            //wf.Add(step);

            //APIRest.Models.InitializationModel im = new APIRest.Models.InitializationModel(0, "RUT", "21284415-2", "http://www.dominio.com/redirect",
            //                                            null, "titulo", "Mesage", wf);

            //string json = JsonConvert.SerializeObject(im);

            //int i = 0;

            //APIRest.Models.UpdateModel um = new APIRest.Models.UpdateModel();
            //um.trackid = "afhashfahflashfalf";
            //um.metadata = new Dictionary<string, string>();
            //um.metadata.Add("xx", "yy");
            //um.metadata.Add("zz", "aa");
            //um.samplelist = new List<APIRest.Models.Sample>();
            //APIRest.Models.Sample s = new APIRest.Models.Sample("s1", 0, "value1");
            //um.samplelist.Add(s);

            //string json = JsonConvert.SerializeObject(um);

            //APIRest.Models.UpdateModel umD;

            //umD = JsonConvert.DeserializeObject<APIRest.Models.UpdateModel>(json);


            //int i = 0;

            //try
            //{
            //    string s = "{\"timestamp\":\"2020-07-24T14:35:24.356Z\",\"scanReference\":\"974f5b8b-c742-4b9d-b98e-5e85113389dd\",\"document\":{\"type\":\"ID_CARD\",\"dob\":\"1969-09-28\",\"expiry\":\"2022-08-02\",\"firstName\":\"GUSTAVO GERARDO\",\"gender\":\"M\",\"issuingCountry\":\"CHL\",\"issuingDate\":\"2017-08-21\",\"lastName\":\"SUHIT\",\"nationality\":\"ARG\",\"number\":\"601014310\",\"status\":\"DENIED_FRAUD\"},\"transaction\":{\"clientIp\":\"200.83.215.110\",\"customerId\":\"BiometrikaLatam\",\"date\":\"2020-07-24T12:47:43.377Z\",\"merchantScanReference\":\"fc308d9726a44a258a73c34e60b97704\",\"source\":\"WEB_CAM\",\"status\":\"DONE\",\"updatedAt\":\"2020-07-24T13:00:39.573Z\"},\"verification\":{\"mrzCheck\":\"NOT_AVAILABLE\",\"rejectReason\":{\"rejectReasonCode\":\"100\",\"rejectReasonDescription\":\"MANIPULATED_DOCUMENT\",\"rejectReasonDetails\":{\"detailsCode\":\"1002\",\"detailsDescription\":\"DOCUMENT_NUMBER\"}}}}";
            //    Biometrika.Jumio.Api.DataResponse dr = JsonConvert.DeserializeObject<Biometrika.Jumio.Api.DataResponse>(s);
            //    if (dr != null && dr.verification.rejectReason != null && dr.verification.rejectReason.rejectReasonDetails != null)
            //    {
            //        //if (dr.verification.rejectReason.rejectReasonDetails.GetType().Equals(typeof(Biometrika.Jumio.Api.RejectReasonDetail))) {
            //        //try
            //        //{
            //        //    Biometrika.Jumio.Api.RejectReasonDetail drd = 
            //        //        JsonConvert.DeserializeObject<Biometrika.Jumio.Api.RejectReasonDetail>((string)dr.verification.rejectReason.rejectReasonDetails);
            //        //    //(Biometrika.Jumio.Api.RejectReasonDetail)dr.verification.rejectReason.rejectReasonDetails;
            //        //}
            //        //catch (Exception ex)
            //        //{
            //        //    LOG.Error(" Error: " + ex.Message);
            //        //}

            //        //try
            //        //{
            //        //    List<Biometrika.Jumio.Api.RejectReasonDetail> l =
            //        //        JsonConvert.DeserializeObject<List<Biometrika.Jumio.Api.RejectReasonDetail>>((string)dr.verification.rejectReason.rejectReasonDetails);
            //        //    //(List<Biometrika.Jumio.Api.RejectReasonDetail>)dr.verification.rejectReason.rejectReasonDetails;
            //        //}
            //        //catch (Exception ex)
            //        //{

            //        //    LOG.Error(" Error: " + ex.Message);
            //        //}
            //        //} else
            //        //{
            //        //    List<Biometrika.Jumio.Api.RejectReasonDetail> l = 
            //        //        (List<Biometrika.Jumio.Api.RejectReasonDetail>)dr.verification.rejectReason.rejectReasonDetails;
            //        //}
            //    }

            //    s = "{\"timestamp\":\"2020-07-24T15:31:19.132Z\",\"scanReference\":\"f5f325e4-241b-473f-9419-487477cf4d09\",\"document\":{\"type\":\"ID_CARD\",\"dob\":\"1979-06-13\",\"expiry\":\"2021-06-13\",\"firstName\":\"MIRTHA ANDREA\",\"gender\":\"M\",\"issuingCountry\":\"CHL\",\"lastName\":\"RAMIREZ RAMIREZ\",\"nationality\":\"ARG\",\"number\":\"107792585\",\"status\":\"DENIED_FRAUD\"},\"transaction\":{\"clientIp\":\"200.83.215.110\",\"customerId\":\"biometrika\",\"date\":\"2020-07-15T21:21:50.313Z\",\"merchantScanReference\":\"654321\",\"source\":\"WEB_UPLOAD\",\"status\":\"DONE\",\"updatedAt\":\"2020-07-15T21:32:58.570Z\"},\"verification\":{\"mrzCheck\":\"NOT_AVAILABLE\",\"rejectReason\":{\"rejectReasonCode\":\"100\",\"rejectReasonDescription\":\"MANIPULATED_DOCUMENT\",\"rejectReasonDetails\":[{\"detailsCode\":\"1007\",\"detailsDescription\":\"SECURITY_CHECKS\"},{\"detailsCode\":\"1001\",\"detailsDescription\":\"PHOTO\"}]}}}";
            //    dr = JsonConvert.DeserializeObject<Biometrika.Jumio.Api.DataResponse>(s);
            //    if (dr != null && dr.verification.rejectReason != null && dr.verification.rejectReason.rejectReasonDetails != null)
            //    {
            //        //try
            //        //{
            //        //    List<Biometrika.Jumio.Api.RejectReasonDetail> l =
            //        //              (List<Biometrika.Jumio.Api.RejectReasonDetail>)dr.verification.rejectReason.rejectReasonDetails;
            //        //}
            //        //catch (Exception ex)
            //        //{

            //        //    LOG.Error(" Error: " + ex.Message);
            //        //}
            //    }
            //}
            //catch (Exception ex)
            //{

            //    LOG.Error(" Error: " + ex.Message);
            //}

            //string json = "{    \"logoServiceCompany\": \"/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABAExAAIAAAARAAAAPlEQAAEAAAABAQAAAFERAAQAAAABAAAAAFESAAQAAAABAAAAAAAAAABBZG9iZSBJbWFnZVJlYWR5AAD / 2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz / 2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz / wAARCAAxAMgDASIAAhEBAxEB / 8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL / 8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4 + Tl5ufo6erx8vP09fb3 + Pn6 / 8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL / 8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3 + Pn6 / 9oADAMBAAIRAxEAPwD9 / Kr6tq9roOmXF7fXVvZWdqhlmnnkEcUKDkszNgKB6nivGP2xf25PD / 7IsWk2N5azap4k8RxzPpViJBBFKIigdnlOcAeYuFUMx9AMsPz++M37R / j79qrxJb2usXl1fLcTqtjoemxOtsJCcIEhXLSSZJwzl35IBA4r7Ph3grF5nFYibVOj / M93bey / V2Xqfm / F / iVl + STeEpp1cRp7i0SvquZ + d9km / JXufX / xj / 4Kw + FfBniWGw8J6TceL7eGXbe33n / ZLfb3EBZS0jDnkqqHAwzA5HtnwD / ap8F / tIaaZPDeqD7dEm + 40y6AhvrUcctHk7l5A3oWTJxuzXwVp3 / BOLxxBo9veeI9a8D + CzdD91bazq + y4P1CIyfk5I7gVzvxW / ZD + In7OFla + KW + z3Wl2rrLDr / h2 / aWO0cnCtvASWPnjftC5IG7JGfrq3CnDmIpxwuDrqNXZS5r8z7dIvy5bfM / P8Nx5xjg608bmOFcqG7hy8rgu63kvPnTXofq5RX5 +/ s//wDBWPUPAFpHY/E6NtX0mEbf7ZtkVbyAD/nrGMLN25Ta+AeJGNfeXibxVpngvw5eaxrGoWWk6Tp8Rnury8mWCC2jHJd3YhVUdySBX51nXDuNyusqWJj8XwtaqVrbdeq0aTP2Hhvi7Lc8w8sRgpfDbmUtHG99+nR6ptaPXRmhRXP/AA4+K/hf4w6FJqnhLxJoXijTYZ2tpLrSb+K8hjlUKxjLxswDBWU7Sc4YHoRS+B/ir4Y+JsupR+G/EWh6++i3Js9QXTr6K6NjOM5il2MdjjB+VsHivHlRqRvzRfu76bevY+hjiaUlFxknzbarX07/ACN+iijNZmwUUUUAFFFcD8Nf2oPAfxf+Jnivwd4b8RW+qeJPBMog1qzSCWM2b7mQgO6BJMMrK3lswUjBwSK1p0ak4ynCLajq2lole1321016mNTE0ac406kkpSdoptJtpXsl1dtdOmp31FFNnnS2heSRlSONSzMTgKBySayNh1Fc/wDDf4s+FvjJoMmqeEfEmg+KNMhmNtJd6TfxXkKShVYxl42YBgrodpOcMp7iuY8dftffDD4caX4ou9W8deHI18FIr67BbXa3l1pYaRYlEsEO+VWMjqgXbkswGM10Rwlec/ZRg3LtZ310Wnm9DlqY7DU6ftp1IqNm7tpKyV273tZJXfkej0Vi/Dr4haN8WfAmk+JvDt9HqWh65ape2N0isgmicZU7WAZT6qwDA5BAIIrarGcZQk4yVmtGn0OinUjOKnB3T1TWqaezTCiiipKCiiodQv4tKsJ7q4by4LeNpZGwW2qoyTgcngdqN9EDaSuyaiuG/Z9/aR8F/tTeBZPE3gPWG1zRI7p7Jrk2VxaYmRVZl2Toj8B15xjnrwaK0rUalKbp1YuMlumrNeqZjh8RSr01WoSUoy1TTTTXk1oz87v+DicZ8c/Cn/rw1P8A9GWtcv8A8E7PiHJ+zt+xV8U/jzqirr2reHbyLwx4ciuAGks55RAJJdx+8P8AS4Tyc7IZFDAOa6j/AIOJj/xXXwp/68NT/wDRlrXnv/BOuHT/ANqH9i74ufs7LqFlpfi/WrtPFPhs3L7V1CaIW5eEHoMG0jDdWCTuwVhG2P2/Lf8AklMPz/w+Zc/+D2r5vO1t7dL9D+Yc4/5L/F+zt7Xkfsr/APP32K5LX0vf4b/at1se+eCf2ZvAPxh+G3w9+JfxC+IXjA33xTeK2VWijluLi9mbESh/Lfy4lAYEMpQZUBkAAMX7Npuv2af+CiV78Elu5Nd8M6959lqFtPGPs1xE+nNeRu0X3TIECxuQACpftgD528c/tQeKvh/4b+B/wv8AFnwt1vwrqnwZ1uHUTPfXTf8AE5NvvJSNPJChXPSRJJFI5BYEGvbPjB+3z8NfCfxc8SfETwP4I8XD4t6/aR2YvPE0ccFroI8iOLfHbq7kymNI+CMEE4cKzK2k8LmdSnOhNOpCrGooJODjF8y9lJNbJRd73b0SSukjbB4jJadWliqbVKrQnSc21UU5rkbrRalfmcpq1rJatt2bb+R/j9oS+Evid410OO4kurXw/q2o6ZbSO27dFBPJEp9OVQHPev1+/wCCi3/JhnxW/wCxZu//AEXX4va5cS3lnezTyyTzzLJJLLI255XOSzMe5JJJPcmv3I/au+E+pfHb9mPxr4N0eaxt9U8SaPNYWsl7I0dukjrgF2VWYL6kKT7VzeIUlRr5dKrL4ZSu/R07s9Xwwp+3wma08PH4oRUYrzVWy/Q/Ob9gLxzff8E9Piv8NJtVupj8Kv2hdBspjc3DKI9L1ZVVGYn+ECV8E/KPKuYyxYwGu9/Yx+Pa/sqfBX9rbx1NZ/bJvDnjm7aG1clVluXmaGFHPUKZZEDEcgZxzXvPjb/gnlN8Vf8Agmtofwb8QXGk/wDCVeG9Jg/s7UIJJHtLbUoFYI4Zow/lMGaNz5e7ZK+BnFcX+yZ/wS88TeDf2W/i38O/ihrmi6hcfEy7F2NQ0m6nvHhlCBlnkM0URaRZ1WTHIbHJ5rwcVnGWYqlWrYiS5pThGSX24QqJqS8+TR+ifU9nA8O53gK2Hw2Eg+SNOpOEna1OpOk06b8vae8uiUmuh5fr/wC1B+1T8GP2bfDf7QmueLvCfiHwlrU1vdXPg8aRDbrbWdy2ISs6IJctuTGZGKF1LeZhhVb/AIKEeMvHnxm/bQ/Z1n8M+JtJ0vR/E6waz4JMmnF30yWZIC1xeKxZZpCSNoUBVQYxkszdHqf/AATo/aS+JXwR8P8AwS8UeNfhxb/C3QLqLOqWCXMmr3VpExMMRRkEZ2fLtXK4Krl328+qfts/sFeMvG3jH4OeLPhBeeGbHVvhAotrTTteeUWs0Efk+SA0asTt8sqwJUsrZDKy86U8wyyji4STpKT9qk4xSiouP7vm0te+jdm7NqW5liMpzrE5fVpuNdwXsJOM5tzc4zvV5Pevy2s0rpcyTgtDQ/af8d/EX4D/ALNfgu11749fD7wL4ykvJV1rxFqOkxMdSgHmNmytCpDSJugVl2YwSxZTgN5//wAE0v27PGPxk/ar8ZfDLXvHWj/FbQdM0htZ0nxXaaSulvcBJLZGjMKKi7c3JByuQ0RwzKwIm/aO/YY+Ofxo8ZfCX4nSX/wr1L4leB0mTVNKu1uv+EflP2qSW3aFShkbajqH3bW3RqysSBXY/su/sTfE74eft06/8ZvH/iLwbrN14v8AC7afqMGjR3FutleGa1CQwRyKd9ukFqgEruJGYnKdWPl82WRyypGrKnKpKMmrKKanz6KNoc1rbPmUbaKPb3FHO55zRnQhVhRjKCfNKTUqfs9XK8+Xm5mlJcjndNuffxn9hT9o/wDam/bj0uyvNO8UeFdF8N+E/EccWv6tc2MK3mtwlo5JLOGNYHjXy4SfmxGxMyfPwcdb+zx+1ZfeDP2of2utU1ix0WbSfh7DLqax2Gk2ljeX62xuSqTXEcYkmbamxWlZsbvc16t/wS2/Y28VfsT/AAX8ReHfF174fvr7V/EEmqwvpFxNNCsTW8EQDGWKMht0TcAEYI57DD+C3/BPTWtC+PX7ROreMLzRLnwh8aop7OCHTbqU30NvM04fzA8Koj7JeCrPhh7VeMzDK518XTjGCp2iocsUnL34uVnu3ZO3ZaKyMstyfPKeEy+rOdSVe8nU55NqL9nOMbrZK7jfe71d3dngNn+1H+1Zq/7Js37SUPjfwZH4Whu3nXwb/YkZie0S7Nq/70p5/EgY7fODFBuD7iEr9Av2evjFa/tBfA3wp42s7drOHxNpkN+bYyeYbV3UF4t2Bu2NuXdgZ25wM18Or/wTi/aS0z9n2f4C2fjf4ZyfCia+Mi6u8FzHrCWjXX2hovKClOZMyGPecklPOCHA+7Pgr8KdO+Bfwj8N+DdJaaTTvDOnQ6dBJLjzJhGgUyPgAbmILHAAyxwBXBxPUy+VJfVuRy55cvs1a1Ky5VLRXlfvd73Z6nBNDN6eIf1z2qh7KHP7WXNetd87hq7Qt2tHayPhv9jrxpY/8E//ANrz9pH4c6tvt/Cen2M3xB0aKNdsUVpFH5skcSnlm8maKPj/AJ829K8/+AOna58O/wDglH8bvjffwaXceLvipq39pSm+sYb61uLcagsJElvMrRsGmmvThgQVdD2Br3z/AIKg/wDBODxl+154+8N+Kfh5rHh7RNbtNIu9B1d9Wu54Eu7OQkxIvlQy5x5tyGDAZEi45FepftG/seXXjT/gn3efBjwXcaba3EOkWGk6fPqTvDAVtpYGLytGjtuZYmJIU5ZuepNer/bWClGhWclz1p0va36Km7O/lJ2l52PD/wBW8zhPFYaMGqeGhX+rtfalWV4qPW8E5QXa+jtofOfxy/bg+IvwB/ZG/Zd17woNNmvfFsFpHqWlQ6fb29vqiCCAraxqqBbdW3FR5QULkYGBiqv7cGi/tJeAv+CefxE1D4k+PPB2of2lfWy3llpNjt+z6dPNDAbOGQRREZkl+cyeaWjXaHyST3nxr/4Jx+PPiP8AAX9nXwvYal4Rj1H4Ry2smsvcXlwsNwIlgDfZysBZ8+W2N6p1HTt77+3V+znfftY/sp+LfAOm6ha6XqWtxQPaXF0rGBZYLmK4RX2gsFYxBCwDFQ2QrY2nCGaZfQrYV04w/iyc5cqbUVUvHW2nu9tbW7I6p5Fm2Jw+OVadXSjGNKKm1FzdG0tL6vnSWrte76tny54T8Y/Gj9lz/glPqfji68d6PqSw+FPDk/gyG30aFH0G3cwo8Uu5MTMYZYk3NnBjJ6mtz9qf9s34i/DP/glP8PfidouuQ2vjTXoNJe+vmsIJFlM8DPLiJkMa5YA8KMdsVveA/wBj/wCMvxK/Yo8VfB34p6p4BsdPj8P6fofha40D7RLLE1pkrLeGRVDAmK1H7vGQsnCkjPkHjn/gmv8AtMfGn9lTR/hT4k8XfCuz0HwP5R0SO1N40uqvGfKjF1P5PyRxQSS7CkRZmVA65/eL1UJ5ZVxHPip0rxrc0nZWlTcVZK0bPX4l3bb6s4sTRzuhhPZYGnXtLDckVzO8aqnJtu8rxfK1yv8AlSitkj3f9tP47+IvBXws+Hd5YfG/wT8HpdXthcatd6tpsWoX18phjYNa2zBgwVywcbQP3i/MuNrec/8ABPT9ubxZ8fPFXxa+H/ibxXo/xGtPDelSaho/iux00acdQgOUYPCqoo+/GQAgKkSAtICrDa/aZ/YH+J+s/tMfC/4rfD65+H2p614J8PQaJPpfij7Q1issSzhZ08tdzqPtLsB8jI8SMN2SF1v2XP2EfiV8LP2mvid8QvGniLwjrlx8RtB+zyNpqz2/2a9fy2eIRMhAt49pjRy7OyorMoYkDjhLKoZXKDlCU3G60ipKXPtpHm0j1c7NaKPU76kc+nncKkYVI01Lll70nCUPZ/FZz5FeXRU+ZPVy1s8X/ggP/wAmNXn/AGM95/6ItqK9Q/4JgfskeJv2Lf2brjwf4svNBvtUk1mfUFk0meWa38t44VALSRRtuzG2RtxjHPoV83xNiKdfNK9ai+aMpNprqj7LgvB1sJkeFw2Ii4zjBJp7pnZftY/sT/D/APbP8KW+m+NNMme608P/AGfqllL5F/pxfG7y3wQVOBlHVkJAJUkAj8nv2t/+CW3xU/Yh1b/hK9DnvPEnhnSZhd23iLRN8F7pJT5xLNEhMkBTaT5qMyLgEuhIWv27oru4f4vx2Vfu4PnpdYS287dvy7pnl8XeHmV59++qL2ddbVI6S025v5kvPVdGj8SPhL/wWB+P2mT2ulzeMrbV7fYUSW/0q2lmUKhx+8VFLnjOX3E9yayzD8QP2zvjZeXMNnfeMPGmuGOS5NpaxQjaiJCjybAkUUaqqLvfavAySTk/o/8AH3/gj58Jvjb8SbPxVY2974K1LzzJqSaEI4bfU1ZSGzEylI5D18yMDJLFg7EMPoD4N/Azwl+z/wCEU0PwfodlomnqdziFS0ty/TfLIxLyvjjc7E4wM4AFfcS48yfCUfb5ZhVGtJWfuqKXq1q1fWyte2tmfneD8M+IcXVeEzvGudCDunzSk5aaWjL4WldXd7X05kfJv7J3/BHPQvBYtta+KU1t4o1VcSJokGf7Ltj1xKSA1yemQQsfLAq4w1fW/wAY20VfhrqS+ItGs/EGiyrHDdaddW8dxDcq8iKFaOQFGAJBwR2rp6yPHng6H4geE7zSLi4urSK8C5mttnmxFWDgrvVlyCo+8pHtX5zjc8xWYYuGIx1RuzW2iirq/Klt8tX1bZ+vZfw7g8qwM8NllNJuL31cnZ25m99ej0V9EkeZ6n+yt8B9G8QWOmXXwn+GsN1qJ225bwda+TI2GOzzfI8sOQrEKWDHBwDVDU/2fv2dtG1qbT7r4a/CyG4t3SKTd4StPLV2KAJ5nk7N482NiucqrhiAvNdld/s+2mreONL8QajrutalqGkmJomuI7TkxvMy42wAx584q3llN4jjzyCWLv8AZu8O3/iHX76eITL4jinjuYmtbfennxCKUpP5fnruXdx5hALtjAwBtHMEmufE1HprZve/S/S3f71sctTLqrT9nhKS97S6j8Nt3brfs/k9zibr4Efs5WeppZv8N/hV5zTm2bb4StHSF/OMAEjiErGDMrRBnIDSKyAllIFfwr8Dv2f9e0KO6uvhT8K9NnZ4Ee3fwxYu0ZuLuS0t8kQ/8tJo2Ue/X1ruV/Zs0uKKOOPVtcVJ7O3sdV+eAnXEinluN058rh5JZ7hnaLy93nv0whVh/Zn0sX9tLHrOvRwxPYyTW4a3KXbWd/JfwbyYiwxNK4OwruXAPTJ1/tCny2WIqXuteaW3X5vpvstVdpYf2bieZSeFpWs9OWO7217J2vtvKydot8T/AMKS/Z3dYriP4X/C3+zHWeRtQl8K2UNr5cMZkeVJHhCyxhQTvjLKAM5qrD8Kf2c7XR/tWqfDD4XaXMrXBltZfCNsbi0SEoztNH9nDRbYpYZX3ABElDbinzn0Ifs1aZJ4P0/Qp9Y16407RrV7HTVZ4FeyhMQiRQwiBYxgKVZ9xyi7i3zbpv8AhnfTrm61W6vNW1u+vtasr6yvLiRoVaRbuK0idgqxBVZI7KEJgY+8WDls01mVK9nXqW12k79beWq300skr6sTyvFWvHDUr6bxjbpfZ30d7a6ptu1kjgfi78DPgF8IvDd9eXfwl+GNxeW2n3GoQ2cfhSzLTrEufmZYGEasxVA74Xc6jJJApnin4JfADQP3dv8ACb4X310NStNPMQ8LWcauZr6CykeNzBtl8mSdRIEJ2NhG2sRXpPj34FWfju3uIf7X1nS49Q0htDvxZmD/AE21O7areZG+1l3yYZNvEr5z8u2jB+zF4ft9WvrxHmSS81RNW3pbWqzRyjUItQZPOEPmtG88S5VnPy8AjCFVRzKn7OLqV6nNfX3parTTy67PXunoPEZXiPa1FSw1LltaPux0dnr59N0rNWSavI85174S/Abw94t13Rp/gr8PvtWjRwyo3/CK6f5d8HMIkEZ8vrF9ogLBscSrjPOLHiD4Ufs3+Hbe6kf4X/Di6a0nS3aO28F20ryk3KWrNEBB++WOaRUkMe7YxCnDEKfTvGPwH0fxvLeS3VxqMc93qdrqySwyIr2s0EaxAR5U/K8Ssjhs5WVwCMjGbcfs0abcRbG1rXdtnE8OljdBjSEa7hu8RjyvnxLbQAed5mFiC93LOnmdKSg51qienN70uyvbfrf8X0Sc1spxUZVFTw9JrXlbjHTV2vt0t32SdrykuR1n4Dfs6+HDeDUfhj8M9PNj5W/7T4Mt4fM8yRIk8vdAPNzJJGh8vdhnVTgkA11+Cv7O9vDG178L/hbZtNcXEADeFLOQRrDcNbmSUrCRCnmLt3SbQGO0nIIrsX/ZZ0W48UzavPqeszXU16t8Wb7PuLLqFtqCo0nleY6LJaxxqHY7Icou3gizqH7N2lXsWpQpqmtW9rrq3MGrQo0JXUree7numgctGSqq11cIDGVfZMwLFgjqLMKSik8RUvfX3ntb/P16LTVjeW4pybWEpJW0XLFu9+vyt1XV6u0Sp/wxJ8GP+iRfDD/wlrH/AONUf8MSfBj/AKJF8MP/AAlrH/41Xp9FeN/amN/5/T/8Cf8AmfR/2PgP+fEP/AY/5HmH/DEnwY/6JF8MP/CWsf8A41R/wxJ8GP8AokXww/8ACWsf/jVen0Uf2pjf+f0//An/AJh/Y+A/58Q/8Bj/AJHmH/DEnwY/6JF8MP8AwlrH/wCNUf8ADEnwY/6JF8MP/CWsf/jVen0Uf2pjf+f0/wDwJ/5h/Y+A/wCfEP8AwGP+Rg/D34W+GfhHosmm+FPDmg+GNOmnNzJa6Tp8VlDJKVVTIUjVVLFVUFiM4UDsKK3qK46lSU5Oc2231erO2nThTioU0klslogoooqDQKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//Z\",    \"logoCustomer\": null,    \"background\": \"#F7F8FB\",    \"primary\": \"#97BE0D\",    \"secondary\": \"#424242\",    \"accent\": \"#82B1FF\",    \"error\": \"#FF5252\",    \"info\": \"#2196F3\",    \"success\": \"#4CAF50\",    \"warning\": \"#FFC107\"}";
            //string json = "{\r\n    \"logoServiceCompany\": \"/9j/4AAQSkZJRgABAQEAYABgAAD/4QBYRXhpZgAATU0AKgAAAAgABAExAAIAAAARAAAAPlEQAAEAAAABAQAAAFERAAQAAAABAAAAAFESAAQAAAABAAAAAAAAAABBZG9iZSBJbWFnZVJlYWR5AAD / 2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz / 2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz / wAARCAAxAMgDASIAAhEBAxEB / 8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL / 8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4 + Tl5ufo6erx8vP09fb3 + Pn6 / 8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL / 8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3 + Pn6 / 9oADAMBAAIRAxEAPwD9 / Kr6tq9roOmXF7fXVvZWdqhlmnnkEcUKDkszNgKB6nivGP2xf25PD / 7IsWk2N5azap4k8RxzPpViJBBFKIigdnlOcAeYuFUMx9AMsPz++M37R / j79qrxJb2usXl1fLcTqtjoemxOtsJCcIEhXLSSZJwzl35IBA4r7Ph3grF5nFYibVOj / M93bey / V2Xqfm / F / iVl + STeEpp1cRp7i0SvquZ + d9km / JXufX / xj / 4Kw + FfBniWGw8J6TceL7eGXbe33n / ZLfb3EBZS0jDnkqqHAwzA5HtnwD / ap8F / tIaaZPDeqD7dEm + 40y6AhvrUcctHk7l5A3oWTJxuzXwVp3 / BOLxxBo9veeI9a8D + CzdD91bazq + y4P1CIyfk5I7gVzvxW / ZD + In7OFla + KW + z3Wl2rrLDr / h2 / aWO0cnCtvASWPnjftC5IG7JGfrq3CnDmIpxwuDrqNXZS5r8z7dIvy5bfM / P8Nx5xjg608bmOFcqG7hy8rgu63kvPnTXofq5RX5 +/ s//wDBWPUPAFpHY/E6NtX0mEbf7ZtkVbyAD/nrGMLN25Ta+AeJGNfeXibxVpngvw5eaxrGoWWk6Tp8Rnury8mWCC2jHJd3YhVUdySBX51nXDuNyusqWJj8XwtaqVrbdeq0aTP2Hhvi7Lc8w8sRgpfDbmUtHG99+nR6ptaPXRmhRXP/AA4+K/hf4w6FJqnhLxJoXijTYZ2tpLrSb+K8hjlUKxjLxswDBWU7Sc4YHoRS+B/ir4Y+JsupR+G/EWh6++i3Js9QXTr6K6NjOM5il2MdjjB+VsHivHlRqRvzRfu76bevY+hjiaUlFxknzbarX07/ACN+iijNZmwUUUUAFFFcD8Nf2oPAfxf+Jnivwd4b8RW+qeJPBMog1qzSCWM2b7mQgO6BJMMrK3lswUjBwSK1p0ak4ynCLajq2lole1321016mNTE0ac406kkpSdoptJtpXsl1dtdOmp31FFNnnS2heSRlSONSzMTgKBySayNh1Fc/wDDf4s+FvjJoMmqeEfEmg+KNMhmNtJd6TfxXkKShVYxl42YBgrodpOcMp7iuY8dftffDD4caX4ou9W8deHI18FIr67BbXa3l1pYaRYlEsEO+VWMjqgXbkswGM10Rwlec/ZRg3LtZ310Wnm9DlqY7DU6ftp1IqNm7tpKyV273tZJXfkej0Vi/Dr4haN8WfAmk+JvDt9HqWh65ape2N0isgmicZU7WAZT6qwDA5BAIIrarGcZQk4yVmtGn0OinUjOKnB3T1TWqaezTCiiipKCiiodQv4tKsJ7q4by4LeNpZGwW2qoyTgcngdqN9EDaSuyaiuG/Z9/aR8F/tTeBZPE3gPWG1zRI7p7Jrk2VxaYmRVZl2Toj8B15xjnrwaK0rUalKbp1YuMlumrNeqZjh8RSr01WoSUoy1TTTTXk1oz87v+DicZ8c/Cn/rw1P8A9GWtcv8A8E7PiHJ+zt+xV8U/jzqirr2reHbyLwx4ciuAGks55RAJJdx+8P8AS4Tyc7IZFDAOa6j/AIOJj/xXXwp/68NT/wDRlrXnv/BOuHT/ANqH9i74ufs7LqFlpfi/WrtPFPhs3L7V1CaIW5eEHoMG0jDdWCTuwVhG2P2/Lf8AklMPz/w+Zc/+D2r5vO1t7dL9D+Yc4/5L/F+zt7Xkfsr/APP32K5LX0vf4b/at1se+eCf2ZvAPxh+G3w9+JfxC+IXjA33xTeK2VWijluLi9mbESh/Lfy4lAYEMpQZUBkAAMX7Npuv2af+CiV78Elu5Nd8M6959lqFtPGPs1xE+nNeRu0X3TIECxuQACpftgD528c/tQeKvh/4b+B/wv8AFnwt1vwrqnwZ1uHUTPfXTf8AE5NvvJSNPJChXPSRJJFI5BYEGvbPjB+3z8NfCfxc8SfETwP4I8XD4t6/aR2YvPE0ccFroI8iOLfHbq7kymNI+CMEE4cKzK2k8LmdSnOhNOpCrGooJODjF8y9lJNbJRd73b0SSukjbB4jJadWliqbVKrQnSc21UU5rkbrRalfmcpq1rJatt2bb+R/j9oS+Evid410OO4kurXw/q2o6ZbSO27dFBPJEp9OVQHPev1+/wCCi3/JhnxW/wCxZu//AEXX4va5cS3lnezTyyTzzLJJLLI255XOSzMe5JJJPcmv3I/au+E+pfHb9mPxr4N0eaxt9U8SaPNYWsl7I0dukjrgF2VWYL6kKT7VzeIUlRr5dKrL4ZSu/R07s9Xwwp+3wma08PH4oRUYrzVWy/Q/Ob9gLxzff8E9Piv8NJtVupj8Kv2hdBspjc3DKI9L1ZVVGYn+ECV8E/KPKuYyxYwGu9/Yx+Pa/sqfBX9rbx1NZ/bJvDnjm7aG1clVluXmaGFHPUKZZEDEcgZxzXvPjb/gnlN8Vf8Agmtofwb8QXGk/wDCVeG9Jg/s7UIJJHtLbUoFYI4Zow/lMGaNz5e7ZK+BnFcX+yZ/wS88TeDf2W/i38O/ihrmi6hcfEy7F2NQ0m6nvHhlCBlnkM0URaRZ1WTHIbHJ5rwcVnGWYqlWrYiS5pThGSX24QqJqS8+TR+ifU9nA8O53gK2Hw2Eg+SNOpOEna1OpOk06b8vae8uiUmuh5fr/wC1B+1T8GP2bfDf7QmueLvCfiHwlrU1vdXPg8aRDbrbWdy2ISs6IJctuTGZGKF1LeZhhVb/AIKEeMvHnxm/bQ/Z1n8M+JtJ0vR/E6waz4JMmnF30yWZIC1xeKxZZpCSNoUBVQYxkszdHqf/AATo/aS+JXwR8P8AwS8UeNfhxb/C3QLqLOqWCXMmr3VpExMMRRkEZ2fLtXK4Krl328+qfts/sFeMvG3jH4OeLPhBeeGbHVvhAotrTTteeUWs0Efk+SA0asTt8sqwJUsrZDKy86U8wyyji4STpKT9qk4xSiouP7vm0te+jdm7NqW5liMpzrE5fVpuNdwXsJOM5tzc4zvV5Pevy2s0rpcyTgtDQ/af8d/EX4D/ALNfgu11749fD7wL4ykvJV1rxFqOkxMdSgHmNmytCpDSJugVl2YwSxZTgN5//wAE0v27PGPxk/ar8ZfDLXvHWj/FbQdM0htZ0nxXaaSulvcBJLZGjMKKi7c3JByuQ0RwzKwIm/aO/YY+Ofxo8ZfCX4nSX/wr1L4leB0mTVNKu1uv+EflP2qSW3aFShkbajqH3bW3RqysSBXY/su/sTfE74eft06/8ZvH/iLwbrN14v8AC7afqMGjR3FutleGa1CQwRyKd9ukFqgEruJGYnKdWPl82WRyypGrKnKpKMmrKKanz6KNoc1rbPmUbaKPb3FHO55zRnQhVhRjKCfNKTUqfs9XK8+Xm5mlJcjndNuffxn9hT9o/wDam/bj0uyvNO8UeFdF8N+E/EccWv6tc2MK3mtwlo5JLOGNYHjXy4SfmxGxMyfPwcdb+zx+1ZfeDP2of2utU1ix0WbSfh7DLqax2Gk2ljeX62xuSqTXEcYkmbamxWlZsbvc16t/wS2/Y28VfsT/AAX8ReHfF174fvr7V/EEmqwvpFxNNCsTW8EQDGWKMht0TcAEYI57DD+C3/BPTWtC+PX7ROreMLzRLnwh8aop7OCHTbqU30NvM04fzA8Koj7JeCrPhh7VeMzDK518XTjGCp2iocsUnL34uVnu3ZO3ZaKyMstyfPKeEy+rOdSVe8nU55NqL9nOMbrZK7jfe71d3dngNn+1H+1Zq/7Js37SUPjfwZH4Whu3nXwb/YkZie0S7Nq/70p5/EgY7fODFBuD7iEr9Av2evjFa/tBfA3wp42s7drOHxNpkN+bYyeYbV3UF4t2Bu2NuXdgZ25wM18Or/wTi/aS0z9n2f4C2fjf4ZyfCia+Mi6u8FzHrCWjXX2hovKClOZMyGPecklPOCHA+7Pgr8KdO+Bfwj8N+DdJaaTTvDOnQ6dBJLjzJhGgUyPgAbmILHAAyxwBXBxPUy+VJfVuRy55cvs1a1Ky5VLRXlfvd73Z6nBNDN6eIf1z2qh7KHP7WXNetd87hq7Qt2tHayPhv9jrxpY/8E//ANrz9pH4c6tvt/Cen2M3xB0aKNdsUVpFH5skcSnlm8maKPj/AJ829K8/+AOna58O/wDglH8bvjffwaXceLvipq39pSm+sYb61uLcagsJElvMrRsGmmvThgQVdD2Br3z/AIKg/wDBODxl+154+8N+Kfh5rHh7RNbtNIu9B1d9Wu54Eu7OQkxIvlQy5x5tyGDAZEi45FepftG/seXXjT/gn3efBjwXcaba3EOkWGk6fPqTvDAVtpYGLytGjtuZYmJIU5ZuepNer/bWClGhWclz1p0va36Km7O/lJ2l52PD/wBW8zhPFYaMGqeGhX+rtfalWV4qPW8E5QXa+jtofOfxy/bg+IvwB/ZG/Zd17woNNmvfFsFpHqWlQ6fb29vqiCCAraxqqBbdW3FR5QULkYGBiqv7cGi/tJeAv+CefxE1D4k+PPB2of2lfWy3llpNjt+z6dPNDAbOGQRREZkl+cyeaWjXaHyST3nxr/4Jx+PPiP8AAX9nXwvYal4Rj1H4Ry2smsvcXlwsNwIlgDfZysBZ8+W2N6p1HTt77+3V+znfftY/sp+LfAOm6ha6XqWtxQPaXF0rGBZYLmK4RX2gsFYxBCwDFQ2QrY2nCGaZfQrYV04w/iyc5cqbUVUvHW2nu9tbW7I6p5Fm2Jw+OVadXSjGNKKm1FzdG0tL6vnSWrte76tny54T8Y/Gj9lz/glPqfji68d6PqSw+FPDk/gyG30aFH0G3cwo8Uu5MTMYZYk3NnBjJ6mtz9qf9s34i/DP/glP8PfidouuQ2vjTXoNJe+vmsIJFlM8DPLiJkMa5YA8KMdsVveA/wBj/wCMvxK/Yo8VfB34p6p4BsdPj8P6fofha40D7RLLE1pkrLeGRVDAmK1H7vGQsnCkjPkHjn/gmv8AtMfGn9lTR/hT4k8XfCuz0HwP5R0SO1N40uqvGfKjF1P5PyRxQSS7CkRZmVA65/eL1UJ5ZVxHPip0rxrc0nZWlTcVZK0bPX4l3bb6s4sTRzuhhPZYGnXtLDckVzO8aqnJtu8rxfK1yv8AlSitkj3f9tP47+IvBXws+Hd5YfG/wT8HpdXthcatd6tpsWoX18phjYNa2zBgwVywcbQP3i/MuNrec/8ABPT9ubxZ8fPFXxa+H/ibxXo/xGtPDelSaho/iux00acdQgOUYPCqoo+/GQAgKkSAtICrDa/aZ/YH+J+s/tMfC/4rfD65+H2p614J8PQaJPpfij7Q1issSzhZ08tdzqPtLsB8jI8SMN2SF1v2XP2EfiV8LP2mvid8QvGniLwjrlx8RtB+zyNpqz2/2a9fy2eIRMhAt49pjRy7OyorMoYkDjhLKoZXKDlCU3G60ipKXPtpHm0j1c7NaKPU76kc+nncKkYVI01Lll70nCUPZ/FZz5FeXRU+ZPVy1s8X/ggP/wAmNXn/AGM95/6ItqK9Q/4JgfskeJv2Lf2brjwf4svNBvtUk1mfUFk0meWa38t44VALSRRtuzG2RtxjHPoV83xNiKdfNK9ai+aMpNprqj7LgvB1sJkeFw2Ii4zjBJp7pnZftY/sT/D/APbP8KW+m+NNMme608P/AGfqllL5F/pxfG7y3wQVOBlHVkJAJUkAj8nv2t/+CW3xU/Yh1b/hK9DnvPEnhnSZhd23iLRN8F7pJT5xLNEhMkBTaT5qMyLgEuhIWv27oru4f4vx2Vfu4PnpdYS287dvy7pnl8XeHmV59++qL2ddbVI6S025v5kvPVdGj8SPhL/wWB+P2mT2ulzeMrbV7fYUSW/0q2lmUKhx+8VFLnjOX3E9yayzD8QP2zvjZeXMNnfeMPGmuGOS5NpaxQjaiJCjybAkUUaqqLvfavAySTk/o/8AH3/gj58Jvjb8SbPxVY2974K1LzzJqSaEI4bfU1ZSGzEylI5D18yMDJLFg7EMPoD4N/Azwl+z/wCEU0PwfodlomnqdziFS0ty/TfLIxLyvjjc7E4wM4AFfcS48yfCUfb5ZhVGtJWfuqKXq1q1fWyte2tmfneD8M+IcXVeEzvGudCDunzSk5aaWjL4WldXd7X05kfJv7J3/BHPQvBYtta+KU1t4o1VcSJokGf7Ltj1xKSA1yemQQsfLAq4w1fW/wAY20VfhrqS+ItGs/EGiyrHDdaddW8dxDcq8iKFaOQFGAJBwR2rp6yPHng6H4geE7zSLi4urSK8C5mttnmxFWDgrvVlyCo+8pHtX5zjc8xWYYuGIx1RuzW2iirq/Klt8tX1bZ+vZfw7g8qwM8NllNJuL31cnZ25m99ej0V9EkeZ6n+yt8B9G8QWOmXXwn+GsN1qJ225bwda+TI2GOzzfI8sOQrEKWDHBwDVDU/2fv2dtG1qbT7r4a/CyG4t3SKTd4StPLV2KAJ5nk7N482NiucqrhiAvNdld/s+2mreONL8QajrutalqGkmJomuI7TkxvMy42wAx584q3llN4jjzyCWLv8AZu8O3/iHX76eITL4jinjuYmtbfennxCKUpP5fnruXdx5hALtjAwBtHMEmufE1HprZve/S/S3f71sctTLqrT9nhKS97S6j8Nt3brfs/k9zibr4Efs5WeppZv8N/hV5zTm2bb4StHSF/OMAEjiErGDMrRBnIDSKyAllIFfwr8Dv2f9e0KO6uvhT8K9NnZ4Ee3fwxYu0ZuLuS0t8kQ/8tJo2Ue/X1ruV/Zs0uKKOOPVtcVJ7O3sdV+eAnXEinluN058rh5JZ7hnaLy93nv0whVh/Zn0sX9tLHrOvRwxPYyTW4a3KXbWd/JfwbyYiwxNK4OwruXAPTJ1/tCny2WIqXuteaW3X5vpvstVdpYf2bieZSeFpWs9OWO7217J2vtvKydot8T/AMKS/Z3dYriP4X/C3+zHWeRtQl8K2UNr5cMZkeVJHhCyxhQTvjLKAM5qrD8Kf2c7XR/tWqfDD4XaXMrXBltZfCNsbi0SEoztNH9nDRbYpYZX3ABElDbinzn0Ifs1aZJ4P0/Qp9Y16407RrV7HTVZ4FeyhMQiRQwiBYxgKVZ9xyi7i3zbpv8AhnfTrm61W6vNW1u+vtasr6yvLiRoVaRbuK0idgqxBVZI7KEJgY+8WDls01mVK9nXqW12k79beWq300skr6sTyvFWvHDUr6bxjbpfZ30d7a6ptu1kjgfi78DPgF8IvDd9eXfwl+GNxeW2n3GoQ2cfhSzLTrEufmZYGEasxVA74Xc6jJJApnin4JfADQP3dv8ACb4X310NStNPMQ8LWcauZr6CykeNzBtl8mSdRIEJ2NhG2sRXpPj34FWfju3uIf7X1nS49Q0htDvxZmD/AE21O7areZG+1l3yYZNvEr5z8u2jB+zF4ft9WvrxHmSS81RNW3pbWqzRyjUItQZPOEPmtG88S5VnPy8AjCFVRzKn7OLqV6nNfX3parTTy67PXunoPEZXiPa1FSw1LltaPux0dnr59N0rNWSavI85174S/Abw94t13Rp/gr8PvtWjRwyo3/CK6f5d8HMIkEZ8vrF9ogLBscSrjPOLHiD4Ufs3+Hbe6kf4X/Di6a0nS3aO28F20ryk3KWrNEBB++WOaRUkMe7YxCnDEKfTvGPwH0fxvLeS3VxqMc93qdrqySwyIr2s0EaxAR5U/K8Ssjhs5WVwCMjGbcfs0abcRbG1rXdtnE8OljdBjSEa7hu8RjyvnxLbQAed5mFiC93LOnmdKSg51qienN70uyvbfrf8X0Sc1spxUZVFTw9JrXlbjHTV2vt0t32SdrykuR1n4Dfs6+HDeDUfhj8M9PNj5W/7T4Mt4fM8yRIk8vdAPNzJJGh8vdhnVTgkA11+Cv7O9vDG178L/hbZtNcXEADeFLOQRrDcNbmSUrCRCnmLt3SbQGO0nIIrsX/ZZ0W48UzavPqeszXU16t8Wb7PuLLqFtqCo0nleY6LJaxxqHY7Icou3gizqH7N2lXsWpQpqmtW9rrq3MGrQo0JXUree7numgctGSqq11cIDGVfZMwLFgjqLMKSik8RUvfX3ntb/P16LTVjeW4pybWEpJW0XLFu9+vyt1XV6u0Sp/wxJ8GP+iRfDD/wlrH/AONUf8MSfBj/AKJF8MP/AAlrH/41Xp9FeN/amN/5/T/8Cf8AmfR/2PgP+fEP/AY/5HmH/DEnwY/6JF8MP/CWsf8A41R/wxJ8GP8AokXww/8ACWsf/jVen0Uf2pjf+f0//An/AJh/Y+A/58Q/8Bj/AJHmH/DEnwY/6JF8MP8AwlrH/wCNUf8ADEnwY/6JF8MP/CWsf/jVen0Uf2pjf+f0/wDwJ/5h/Y+A/wCfEP8AwGP+Rg/D34W+GfhHosmm+FPDmg+GNOmnNzJa6Tp8VlDJKVVTIUjVVLFVUFiM4UDsKK3qK46lSU5Oc2231erO2nThTioU0klslogoooqDQKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//Z\",\r\n    \"logoCustomer\": null,\r\n    \"background\": \"#F7F8FB\",\r\n    \"primary\": \"#97BE0D\",\r\n    \"secondary\": \"#424242\",\r\n    \"accent\": \"#82B1FF\",\r\n    \"error\": \"#FF5252\",\r\n    \"info\": \"#2196F3\",\r\n    \"success\": \"#4CAF50\",\r\n    \"warning\": \"#FFC107\"\r\n}";
            //Root r = JsonConvert.DeserializeObject<Root>(json);

            //int i = 0;
            //APIRest.Models.UpdateModel um = new APIRest.Models.UpdateModel();
            //um.trackid = "423kg4k23g4k23g4k2j3g4kj32";
            //um.metadatalist = new List<APIRest.Models.Metadata>();
            //APIRest.Models.Metadata m = new APIRest.Models.Metadata("name1", 0, "Data1");
            //um.metadatalist.Add(m);
            //m = new APIRest.Models.Metadata("name2", 1, "Data2");
            //um.metadatalist.Add(m);
            //string s = Newtonsoft.Json.JsonConvert.SerializeObject(um);
            //System.IO.File.WriteAllText(@"c:\tmp\updatemodel.json",s);
            //int i = 0;
            //Bio.Core.Api.BpBirApi obj = new Bio.Core.Api.BpBirApi(0, 2, 7,1,null,"data",DateTime.Now, DateTime.Now,7);
            //string s = Bio.Core.Utils.XmlUtils.SerializeObject<Bio.Core.Api.BpBirApi>(obj);

            //List<Bio.Core.Api.BpBirApi> arr = new List<Bio.Core.Api.BpBirApi>();
            //arr.Add(obj); arr.Add(obj);
            //string s1 = Bio.Core.Utils.XmlUtils.SerializeObject<List<Bio.Core.Api.BpBirApi>>(arr);
            //int a = 0;

            //VerificationSource vs = new VerificationSource();

            //VerificationSourceItem vsi = new VerificationSourceItem();
            //vsi.AuthenticationFactor = 2;
            //vsi.StatusVerified = 1;
            //vsi.TypeVerified = "A";
            //vsi.SourceVerified = "SRCeI";
            //vs.VerificationSourceItems.Add(vsi);

            //string sxml = SerializeHelper.SerializeToXml(vs);

            //bool b = vs.IsIdentityValidated(2);

            //int i = vs.SetVerificationSource(4, 1, "M", "gsuhit@biometrika");

            //i = vs.SetVerificationSource(4, 1, "M", "gsuhit@biometrika1");

            //i = vs.UpdateVerificationSource(4, 0, "M", "gsuhit@biometrika");

            //b = vs.IsAFInVerificationSourceItems(4);
            //b = vs.IsIdentityValidated(4);
            //string xml = vs.GetXML();

            //int h = 0;

            //MatcherManager mm = new MatcherManager();
            ////mm.MatchersConfigured = new List<MatcherIntance>();
            ////Hashtable ht = new Hashtable();
            //List<MatcherDefinition> arr = new List<MatcherDefinition>();
            //MatcherDefinition mi = new MatcherDefinition();
            //mi.Authenticationfactor = 2;
            //mi.Minutiaetype = 2;
            //mi.Thresholdextract = 0f;
            //mi.Thresholdmatching = 0.0001f;
            //mi.Assembly = "Bio.Core.Matcher.DigitalPersonaPlatinum";
            ////ht.Add(2,mi);
            //arr.Add(mi);

            //mi = new MatcherDefinition();
            //mi.Authenticationfactor = 2;
            //mi.Minutiaetype = 3;
            //mi.Thresholdextract = 0f;
            //mi.Thresholdmatching = 0.0001f;
            //mi.Assembly = "Bio.Core.Matcher.DigitalPersonaGold";
            ////ht.Add(3, mi);
            //arr.Add(mi);

            //mi = new MatcherDefinition();
            //mi.Authenticationfactor = 2;
            //mi.Minutiaetype = 7;
            //mi.Thresholdextract = 10;
            //mi.Thresholdmatching = 63;
            //mi.Assembly = "Bio.Core.Matcher.Verifinger5";
            ////ht.Add(7, mi);
            //arr.Add(mi);

            ////mm.MatchersConfigured = ht;
            //mm.MatchersConfigured = arr;

            //bool b = SerializeHelper.SerializeToFile(mm,
            //    "D:\\Biometrika\\Desarrollo\\Bio.Portal.Server\\Bio.Portal.Server.v4\\Bio.Portal.Server.Services\\bin\\Matchers.cfg");
            //ITemplate temp = new Bio.Core.Matcher.DigitalPersonaPlatinum.Template();
            //temp.AuthenticationFactor = 2;
            //temp.AdditionalData = "AddData";
            //temp.MinutiaeType = 2;
            //temp.BodyPart = 7;
            //temp.Data = System.Text.Encoding.ASCII.GetBytes("Algo");

            //bool b = SerializeHelper.SerializeToFile(temp,
            //    "D:\\Biometrika\\Desarrollo\\Bio.Portal.Server\\Bio.Portal.Server.v4\\Bio.Portal.Server.Services\\bin\\template.xml");

            //string xml = SerializeHelper.SerializeToXml(temp);

            //int i = 0;
            //Templates = new List<Template>();
            //Template temp = new Template();

            //temp.AuthenticationFactor = 2;
            //temp.MinutiaeType = 2;
            //temp.BodyPart = 1;
            //temp.SetData = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("Algo"));
            //Templates.Add(temp);

            //temp = new Template();
            //temp.AuthenticationFactor = 2;
            //temp.MinutiaeType = 2;
            //temp.BodyPart = 2;
            //temp.SetData = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("AlgoAlgo2"));
            //Templates.Add(temp);

            //string xmllistBirs = Bio.Core.Utils.SerializeHelper.SerializeToXml(Templates);

            //List<Template> TemplatesD = Bio.Core.Utils.SerializeHelper.DeserializeFromXml<List<Template>>(xmllistBirs);
            //int i = 0;

            //ConnectorManager cm = new ConnectorManager();

            //List<ConnectorDefinition> arr = new List<ConnectorDefinition>();

            //ConnectorDefinition cd = new ConnectorDefinition();
            //cd.ConnectorId = "ConnectorBP4";
            //cd.Name = "BioPortal Conector para BioPortal 4";
            //cd.Assembly = "Bio.Core.Matcher.ConnectorBioPortal4";
            //cd.Config = new DynamicData();
            //cd.Config.DynamicDataItems = new List<DynamicDataItem>();
            //DynamicDataItem dditem = new DynamicDataItem("UrlWS", "http://test.biometrika.cl/BioPortal.Server.v4/Services/BioPortal.Server.WS.asmx");
            //cd.Config.DynamicDataItems.Add(dditem);
            //dditem = new DynamicDataItem("UrlWSWEB", "http://test.biometrika.cl/BioPortal.Server.v4/Services/BioPortal.Server.WS.Web.asmx");
            //cd.Config.DynamicDataItems.Add(dditem);
            //dditem = new DynamicDataItem("Timeout", "60000");
            //cd.Config.DynamicDataItems.Add(dditem);
            //arr.Add(cd);
            //cm.ConnectorsConfigured = arr;

            //bool b = SerializeHelper.SerializeToFile(cm,
            //    "D:\\Biometrika\\Desarrollo\\Bio.Portal.Server\\Bio.Portal.Server.v4\\Bio.Portal.Server.Services\\bin\\Connectors.cfg");
        }

       

        internal static void LOGExtended(string msg)
        {
            try
            {
                if (Properties.Settings.Default.BP_Debug_Extended && !string.IsNullOrEmpty(msg))
                {
                    LOG.Debug(msg);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("Global.LOGExtended Excp: " + ex.Message);
            }
        }

        public void CreateTest()
        {
            //string msg;
            //int operationcode = 1;
            //int operationsource = 2;
            //string trackid = "1a2b3c4d5e";
            //string typeid = "UID";
            //string valueid = "CreateTest";
            //int result = 1;
            //int score = 100;
            //int threshold = 1;
            //DateTime timestampstart = DateTime.Now;
            //DateTime timestampend = DateTime.Now;
            //int authenticatorfactor = 2;
            //int minutiaetype = 7;
            //int bodypart = 7;
            //int actiontype = 1;
            //BpOrigin bporigin = AdminBpOrigin.Retrieve(1, out msg);
            //Nullable<DateTime> timestampclient = new Nullable<DateTime>(DateTime.Now);
            //string clientid = "CreateTestClientId";
            //string ipenduser = "127.0.0.1";
            //string enduser = "CreateTestEndUser";
            //string dynamicdata = null;
            //int companyidtx = 7;
            //int useridtx = 1;
            //string msgErr = string.Empty;
            //string msgErrExpected = string.Empty;
            //BpTx expected = null;
            //BpTx actual = null;
            ////BpTx actual = new BpTx(trackid, operationcode, typeid, valueid, result, score, threshold, timestampstart,
            ////    timestampend, authenticatorfactor, minutiaetype, bodypart, actiontype, bporigin, timestampclient,
            ////    clientid, ipenduser, enduser, dynamicdata, companyidtx, useridtx, operationsource);
            //List<BpTxConx> listBpTxConx = new List<BpTxConx>();
            //BpTxConx conx = new BpTxConx(null, "1", "ConnectorBP4", "1a2b3c4d5e", 0, 1, 100, 10, "10/10/2010");
            //listBpTxConx.Add(conx);
            //actual = AdminBpTx.Create(trackid, operationcode, typeid, valueid, result, score, threshold, timestampstart,
            //    timestampend, authenticatorfactor, minutiaetype, bodypart, actiontype, bporigin, timestampclient,
            //    clientid, ipenduser, enduser, dynamicdata, companyidtx, useridtx, operationsource, listBpTxConx, out msgErr);

            //int i = 0;
            //int j = 0;
        }

        public void UpdateTest()
        {
            try
            {
                string msgErr = string.Empty; // TODO: Initialize to an appropriate value
                BpIdentity bpi = null;
                int i = AdminBpIdentity.Retrieve(7, "RUT", "21284415-2", out bpi, out msgErr);
                // TODO: Initialize to an appropriate value

                string msgErrExpected = string.Empty; // TODO: Initialize to an appropriate value
                BpIdentity expected = null; // TODO: Initialize to an appropriate value
                bool actual;
                //actual = AdminBpIdentity.Update(bpi, out msgErr);

                IList<BpBir> bpBirToDelete = new List<BpBir>();
                foreach (BpBir bir in bpi.BpBir)
                {
                    if (bir.Authenticationfactor == 1)
                    {
                        bpBirToDelete.Add(bir);
                        //bir.BpIdentity = null;
                    }
                }
                foreach (BpBir bir in bpBirToDelete)
                {
                    bpi.BpBir.Remove(bir);
                }
                actual = AdminBpIdentity.Write(bpi, out msgErr);
                LOG.Info("Res Write=" + actual.ToString());
            }
            catch (Exception ex)
            {
                LOG.Error("Error En UpdateTest", ex);
            }
        }
    }

    public class Root
    {
        public string logoServiceCompany { get; set; }
        public object logoCustomer { get; set; }
        public string background { get; set; }
        public string primary { get; set; }
        public string secondary { get; set; }
        public string accent { get; set; }
        public string error { get; set; }
        public string info { get; set; }
        public string success { get; set; }
        public string warning { get; set; }

    }
}