﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Portal.Server.Services.APIRest.Models
{

    public class UpdateModelR
    {
        public UpdateModelR() { }

        public UpdateModelR(int _code, string _msg, string _trackid)
        {
            code = _code;
            msgerr = _msg;
            trackid = _trackid;
        }


        public int code;
        public string msgerr;
        public string trackid;
    }
}