﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Portal.Server.Services.APIRest.Models.WA
{
    public class WAModels
    {
    }

    public class EnrollModel
    {
        public int type; //0-Solo imagen para disco | ... otros
        public string deviceid;
        public string geolocalization;
        public string companykey;  //Key unica generada para una copañia, para atar la persona a esa compañia
        public string typeid;
        public string valueid;
        public List<WASample> samples; 
    }

    public class WASample
    {
        public int authenticatorfactor;
        public int minutiaetype;
        public int bodypart;
        public string data;
        public string additionaldata;
    }

    public class EnrollModelR
    {
        public int iduser;
    }

    public class MarkModel
    {
    }

    public class MarkModelR
    {
    }

    public class BIRModel
    {
    }

    public class BIRModelR
    {
    }

}