﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Portal.Server.Services.APIRest.Models
{
    public class CreateModelR
    {
        public CreateModelR() { }

        public CreateModelR(int _code, string _msg, string _trackid)
        {
            code = _code;
            msgerr = _msg;
            trackid = _trackid;
        }

        public CreateModelR(int _code, string _msg, string _trackid, string _urlservice) { //, string _tokensession) {
            code = _code;
            msgerr = _msg;
            trackid = _trackid;
            urlservice = _urlservice;
            //tokensession = _tokensession;
        }

        public int code;
        public string msgerr;
        public string trackid;
        public string urlservice;
        //public string tokensession;

    }
}