﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Portal.Server.Services.APIRest.Models
{
    public class StatusModelR
    {
        public StatusModelR() { }

        public StatusModelR(int _code, string _msg)
        {
            code = _code;
            msgerr = _msg;
            trackid = null;
            status = 0;
        }

        public StatusModelR(int _code, string _msg, string _trackid, int _status, string _statusdescription)
        {
            code = _code;
            msgerr = _msg;
            trackid = _trackid;
            status = _status;
            statusdescription = _statusdescription;
        }

        public int code;
        public string msgerr;
        public string trackid;
        public int status;
        public string statusdescription;
    }
}