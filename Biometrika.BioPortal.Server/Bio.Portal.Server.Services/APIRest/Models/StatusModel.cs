﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Portal.Server.Services.APIRest.Models
{
    public class StatusModel
    {

    }

    public enum StatusEnum
    {
        PENDING = 0,
        DONE = 1,
        FAILED = 2
    }
}