﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Portal.Server.Services.APIRest.Models
{
    public class UpdateModel
    {
        /// <summary>
        /// TrackId unico para actualizar
        /// </summary>
        public string trackid;
        /// <summary>
        /// Datos a actualizar (Por definir)
        /// </summary>
        public IList<Sample> samplelist;

        public IDictionary<string,string> metadata;
    }

    public class Sample
    {
        public Sample() { }

        public Sample(string _name, int _type, string _value) {
            name = _name;
            /*
                1-JPG
                2-Facemap
                3-string
            */
            type = _type;
            value = _value;
        }


        public string name;
        public int type;
        public string value;
    }

    public class Metadata
    {
        public Metadata() { }

        public Metadata(string _key, string _value)
        {
            key = _key;
            value = _value;
        }


        public string key;
        public string value;
    }


}