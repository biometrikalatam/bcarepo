﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Portal.Server.Services.APIRest.Models
{
    public class Error
    {
        public Error() { }

        public Error(int _code, string _msgerr, string _trackid)
        {
            trackid = _trackid;
            code = _code;
            msgerr = _msgerr;

        }

        public string trackid;
        public int code;
        public string msgerr;
    }
}