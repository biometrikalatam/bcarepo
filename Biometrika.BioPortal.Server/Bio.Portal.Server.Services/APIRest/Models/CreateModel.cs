﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Portal.Server.Services.APIRest.Models
{
    public class CreateModel
    {
        public CreateModel()
        {
        }

        //public CreateModel(string _msgerr)
        //{
        //    msgerr = _msgerr;
        //}

        public CreateModel(int _company, string _typeid, string _valueid, string _taxidcompany , string _theme, string _customertrackid,
                            string _callbackurl, string _redirecturl, string _successurl, TypeVerify _typeverifyonboarding, TypeVerify _typeverifiauth,
                            int _onboardingmandatory, OnboardingType _onboardingtype, float _threshold,
                            string _titlebpweb, string _messagebpweb, int _autorizationinclude, string _autorizationmessage, 
                            int _signerinclude, int _checklocksrcei, int _videoinclude, string _videomessage, int _idcardinclude,
                            int _checkadult, int _checkexpired, int _georefmandatory,
                            int _carregisterinclude, int _writinginclude, string _formid, int _selectpersontype)
        {
            company = _company;
            typeid = _typeid;
            valueid = _valueid;
            taxidcompany = _taxidcompany;
            theme = _theme;
            customertrackid = _customertrackid;
            callbackurl = _callbackurl;
            redirecturl = _redirecturl;
            successurl = _successurl;
            typeverifyonboarding = _typeverifyonboarding;
            typeverifyauth = _typeverifiauth;
            onboardingmandatory = _onboardingmandatory;
            onboardingtype = _onboardingtype;
            signerinclude = _signerinclude;
            checklocksrcei = _checklocksrcei;
            threshold = _threshold;
            titlebpweb = _titlebpweb;
            messagebpweb = _messagebpweb;
            videoinclude = _videoinclude;
            videomessage = _videomessage;
            autorizationinclude = _autorizationinclude;
            autorizationmessage = _autorizationmessage;
            idcardinclude = _idcardinclude;
            checkadult = _checkadult; // 0-No o 1-Si
            checkexpired = _checkexpired; // 0-No o 1-Si
            georefmandatory = _georefmandatory; // 0-No o 1-Si

            carregisterinclude = _carregisterinclude; // 0-No o 1-Si
            writinginclude = _writinginclude; // 0-No o 1-Si
            formid = _formid; // Null=No incluye form o "stringid"=Nombre el form (para futuro que será definible, ahora solo usa el form fijo)
            //selectpersontype = _selectpersontype;

    }

        public int company;
        public string typeid;
        public string valueid;
        public string taxidcompany;
        public string theme;
        public string customertrackid;
        public string callbackurl;
        public string redirecturl;
        public string successurl;
        public TypeVerify typeverifyonboarding;
        public TypeVerify typeverifyauth;
        public int onboardingmandatory; // 0-No o 1-Si
        public OnboardingType onboardingtype; //0-single: Solo el type indicado | 1-Combined: Verifica con Jumio + Liveness Facetec para enrolar en ambos
        public float threshold = 60; //0 a 100 => Default 60
        public string titlebpweb;
        public string messagebpweb;
        public int autorizationinclude;  //0-No o 1-Si
        public string autorizationmessage;  //Si pide autorizacion y va null => msg default en BPWeb sino este msg en HTML format
        public int signerinclude; // 0-No o 1-Si
        public int checklocksrcei; // 0-No o 1-Si => Solo si se lee cedula porque si es Auth lo ignora
        public int videoinclude; // 0-No o 1-Si
        public string videomessage; //Mensaje para ller grabado por video. Si es null Msg default "Yo, <Nombre>, doy fe de los datos aqui ingresados."
        public int idcardinclude; // 0-No o 1-Si - Captura Cedula frento/back. Si es  onboardingmandatory = 1 y es tv2d esto no se carga dos veces
        public int checkadult; // 0-No o 1-Si
        public int checkexpired; // 0-No o 1-Si
        public int georefmandatory; // 0-No o 1-Si   => Si es mandatory => Debe bloquear el proceso si no permite tomarlo.

        public int carregisterinclude; // 0-No o 1-Si
        public int writinginclude; // 0-No o 1-Si
        public string formid; // Null=No incluhye form o "stringid"=Nombre el form (para futuro que será definible, ahora solo usa el form fijo)
        //public int selectpersontype; // 0-No (pregunta) o 1-Persona Natural 0 2-Persona Juridica


    }

    /// <summary>
    /// Tipos de Verificacion
    /// </summary>
    public enum TypeVerify
    {
        tv2d = 1,           //No soportada ahora
        tv3d = 2,           //Facetec
        tv3dplus = 3,       //Jumio Automatico
        tv3dstrong = 4      //Jumio Hibrido
    }

    public enum OnboardingType
    {
        single = 0,          //Solo del type indicado
        combined = 1         //Verifica con Jumio + Liveness Facetec para enrolar en ambos
    }                        //   Si Jumio da positivo => Se verifica facemap de facetec con Face devuelta por Jumio
                             //                           si da positivo => Enrola tambien en Facetec    
}