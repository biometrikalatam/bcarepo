﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Portal.Server.Services.APIRest.Models
{
    public class InitializationModel
    {
        public InitializationModel() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_code"></param>
        /// <param name="_msgerr"></param>
        public InitializationModel(int _code, string _msgerr) {
            code = _code;
            msgerr = _msgerr;
        }

        //string _themeHeaderColor, string _themeHeaderLogo, string _licensefacetec, string _licenseforexncrypt, 
        //string _session, int _signerinclude, string _urlservicio3rom, TypeVerify _type, int _onboarding)
        //
        public InitializationModel(int _code, string _typeid, string _valueid, string _redirecturl, string _successurl,
                                     string _jsontheme, string _titlebpweb, string _messagebpweb, 
                                     List<Step> _workflow)
        {
            code = _code;
            typeid = _typeid;
            valueid = _valueid;
            redirecturl = _redirecturl;
            successurl = _successurl;
            //type = (TypeVerify)_type; //1-Jumio o 2-Facetec
            //onboarding = _onboarding; // 0-No o 1-Si
            //Tema grafico
            jsontheme = _jsontheme;
            titlebpweb = _titlebpweb;
            messagebpweb = _messagebpweb;
            //themeHeaderColor = _themeHeaderColor;
            //themeHeaderLogo = _themeHeaderLogo;
            //urlservicio3ro = _urlservicio3ro; //Si es jumio debo enviar la url
            //signerinclude = _signerinclude; // 0-No o 1-Si
            //licenseforexncrypt = _licenseforexncrypt;
            //session = _session;
            //licensefacetec = _licensefacetec;
            workflow = _workflow;
        }

        public int code;
        public string msgerr;
        public string typeid;
        public string valueid;
        //public string trackidext;
        //public string callbackurl;
        public string redirecturl;
        public string successurl;
        //public TypeVerify type; //1-Jumio o 2-Facetec
        //public int onboarding; // 0-No o 1-Si
        //Tema grafico
        public string jsontheme;
        public string titlebpweb;
        public string messagebpweb;
        //public string themeHeaderColor
        //public string themeHeaderLogo;
        //Seran incluidas en el Step de Factec---
        /*  
            public string urlservicio3ro; //Si es jumio debo enviar la url
            public string licensefacetec;
            public string licenseforexncrypt;
            public string   ;
            public int signerinclude; // 0-No o 1-Si
        */
        //---------------------------------------
        public List<Step> workflow;
    }

    //public class Workflow
    //{
    //    public Workflow() { }

    //    public string name;
    //}

    public class Step
    {
        public Step() { }

        public Step(string _name) //int _order, int _type) //, string _value)
        {
            //order = _order;
            name = _name;
            optional = 0;
            /*
                1-JPG
                2-Facemap
                3-string
            */
            //type = _type;
            //value = _value;
        }

        public Step(string _name, int _optional)
        {
            name = _name;
            optional = _optional;
        }

        public Step(string _name, int _optional, string _sampler)
        {
            name = _name;
            optional = _optional;
            sampler = _sampler;
        }

        //public int order;
        public string name;
        public int optional;
        public string sampler;
        //public int type;
        //public string value;

        /*
            Los metadatas por cada tipo son:
            * autorization = 0
                - messagetodisplay (Formateado HTML)
            * signaturemanual 
                x No metadata
            * geolocation
                x No metadata
            * jumio 
                - urlservicio3ro
            facetec
                - type [onboarding: es proceso cedula vs selfie | authentication: solo liveness para hacer authentication]  
                - licensefacetec
                - licenseforexncrypt
                - session
            video = 5
                - messagetodisplay (Formateado HTML)
        */
        public IDictionary<string, string> metadata;
    }

    /// <summary>
    /// 
    /// </summary>
    public enum StepName
    {
        autorization = 0,
        signaturemanual = 1,
        geolocation = 2,
        jumio = 3,
        facetec = 4,
        video = 5,
        selfie = 6,
        idcard = 7, 
        carregister = 8,
        writing = 9,
        form = 10
    }
}