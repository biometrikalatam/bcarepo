﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Portal.Server.Services.APIRest.Models
{
    public class VerifyModel
    {

    }

    public class VerifyModelR
    {
        public VerifyModelR() { }

        public VerifyModelR(string _trackid, string _status, int _result, double _score, int _rejectCode, string _rejectDescription)
        {
            trackid = _trackid;
            status = _status;
            result = _result;
            score = _score;
            rejectCode = _rejectCode;
            rejectDescription = _rejectDescription;
        }

        public string trackid;
        public string status;
        public int result;
        public double score;
        public int rejectCode;
        public string rejectDescription;
    }
}