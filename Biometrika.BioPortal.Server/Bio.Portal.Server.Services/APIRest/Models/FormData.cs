﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Portal.Server.Services.APIRest.Models
{
    public class FormData
    {
        public string name { get; set; }
//        public Dictionary<string, string> value { get; set; }
        public string value { get; set; }

        public FormData() { }

        public FormData(string _name) {
            name = _name;
        }


    }
}