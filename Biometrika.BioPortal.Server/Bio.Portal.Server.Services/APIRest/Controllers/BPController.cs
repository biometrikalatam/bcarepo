﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
using Bio.Core.Constant;
using Bio.Portal.Server.Common.Entities;
using Bio.Portal.Server.Services.APIRest.Models;
using Bio.Portal.Server.Services.Core.Services;
using log4net;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;

namespace Bio.Portal.Server.Services.APIRest
{
    /// <summary>
    /// Controller para el manejo generico de controlers. Al igual que los matchers y connector, 
    /// la idea es tener un unico punto de ingreso y pedir un controller en particular, como por ejemplo
    /// el POSControllers. 
    /// TODO - Hay que revisar si es buena idea porque debe implementar una inteface IBPController, pero
    /// debe acceder a los mismos recursos, clases, tablas etc de BioPortal Server, por lo que quiza no 
    /// valga la pena
    /// </summary>
    /// <remarks>Hola</remarks>
    public class BPController : ApiController 
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BPController));

        //#region SDK for 3ra Parte

        ///// <summary>
        ///// Crea una transacción que será procesada por BPWeb. 
        ///// </summary>
        ///// <remarks>
        ///// Utiliza Basic Authentication [ej.: request.AddHeader("Authorization", "Basic dXNlcm5hbWU3OnNlY3JldGtleTc=")] - 
        ///// Se envían los parámetros para procesar. Se pide si se incluye firma manual, tipo de
        ///// verificación dentro de las opciones:
        /////     * tv2d = 1 - No soportada ahora
        /////     * tv3d = 2 - Facetec
        /////     * tv3dplus = 3 - Jumio Automatico
        /////     * tv3dstrong = 4 - Jumio Hibrido
        ///// Se puede pedir un OnBoarding combinado, esto es Jumio y Facetec en la misma tx. Luego, 
        ///// si se recibió la autorización para enrolar (1-Autorizado | -1-No autorizado), se enrolan ambos 
        ///// proveedores, si es que dio positivo la comparacion del Facemap contra la Face devuelta desde Jumio 
        ///// en el proceso de Onboarding. 
        ///// Ademas se puede pedir para generar el workflow en el BPWeb, lo siguiente:
        /////     * autorizationinclude: 0-No o 1-Si - Esto muestra un disclaimer para aceptar enrolamiento 
        /////     * autorizationmessage: Mensaje de autorizacion personalizado si se desea. Sino se muestra por default el nuestro
        /////     * signerinclude: 0-No o 1-Si - Pide la firma olográfica 
        /////     * checklocksrcei: 0-No o 1-Si .- chequea si cedula está bloqueada en procesos de onboarding. [Aun sin integrar]
        /////     * videoinclude: 0-No o 1-Si - Permite grabar mensaje como evidencia adicional, leyendo un mensaje [Aun sin integrar]
        /////     * videomessage: Mensaje a leer si videoinclude=1, para dejar grabado con datos del negocio [Aun sin integrar]
        ///// </remarks>
        ///// <param name="param">Json con parámetros</param>
        ///// <response code="200">Tx Creada ok</response>
        ///// <response code="404">Parámetros erroneos</response>
        ///// <response code="4xx">Errores a Definir</response>
        ///// <response code="500">Error Desconocido Interno</response>
        ///// <returns>URL donde conectarse para iniciar la verificación.</returns>
        //[Route("api/txCreate")]
        //[HttpPost]
        //[SwaggerResponse(HttpStatusCode.OK, "CreateModelR", typeof(CreateModelR))]
        //[SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(Error))]
        //public object TxCreate([FromBody] CreateModel param)
        //{
        //    /*
        //        Los pasos a realizar son:
        //        1) Leo parametros, parseo y chequeo consistencia de datos (login/token/?)
        //        2) Genero nuevo trackid
        //        3) Chequeo si esta o no enrolado el valueid y chequeo tipo de tx (onboarding o authentication)
        //        4) Si es un pedido para generacion de verify/auth en 3ro => Genero trackid en 3ro
        //        5) Grabo datos de bp_tx con datos adicionales
        //        6) Retorno
        //    */
        //    CreateModel paramIn;
        //    string msg = "";
        //    string new_trackid;
        //    string new_trackid3ro = null;
        //    string trackid3roEnrollORFacemap = null; //Si es un Auth se usa para enviar al servicio de 3ro para verificar o toma el facemap
        //                                             //de FaceTec desde bpidentity3ro para grabar en columna Target 
        //    try
        //    {
        //        LOG.Debug("BPWebController.TxCreate - IN...");
             
        //        //0) Check Basic Authentication
        //        if (!BPUnitOfWork.Authorized(Request, out msg))
        //        {
        //            LOG.Warn("BPWebController.TxCreate - No Autorizado!");
        //            return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //                (new Error(Errors.IERR_CONX_WS, "Acceso NO Autorizado [" + 
        //                                (string.IsNullOrEmpty(msg)?"":msg) + "]", null)));
        //        }


        //        //1) Leo parametros, parseo y chequeo consistencia de datos(login / token /?)
        //        if (param == null)
        //        {
        //            LOG.Warn("BPWebController.TxCreate - parametro nulo");
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                (new Error(Errors.IERR_BAD_PARAMETER,"Parametro de entrada nulo. Reintente...", null)));
        //        }
        //        paramIn = param; // JsonConvert.DeserializeObject<CreateModel>(param);

        //        //TODO - Check Company Access
        //        msg = BPUnitOfWork.CheckStatusCompanyAndUser(paramIn.company, msg);
        //        if (paramIn.company <= 0 || msg.Length > 0)
        //        {
        //            LOG.Warn("BPWebController.TxCreate - Acceso No Permitido [" + msg + "]");
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                (new Error(Errors.IERR_BAD_PARAMETER, "Acceso No Permitido [" + msg + "]", null)));
        //        }

        //        //Lo hago asi porque luego puedo agregar otros tipos como veridium x ej que es Huella
        //        if ((paramIn.typeverifyonboarding != TypeVerify.tv3d && paramIn.typeverifyonboarding != TypeVerify.tv3dplus && 
        //            paramIn.typeverifyonboarding != TypeVerify.tv3dstrong && paramIn.typeverifyonboarding != TypeVerify.tv2d) || (
        //            paramIn.typeverifyauth != TypeVerify.tv3d && paramIn.typeverifyauth != TypeVerify.tv3dplus &&
        //            paramIn.typeverifyauth != TypeVerify.tv3dstrong && paramIn.typeverifyauth != TypeVerify.tv2d)
        //            )
        //        {
        //            LOG.Warn("BPWebController.TxCreate - Opcion Onboarding = " + paramIn.typeverifyonboarding.ToString() + " o " +
        //                      "Opcion Auth = " + paramIn.typeverifyauth.ToString() + " no soportada aun");
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                (new Error(Errors.IERR_BAD_PARAMETER, "Opcion Onboarding = " + paramIn.typeverifyonboarding.ToString() + " o " +
        //                      "Opcion Auth = " + paramIn.typeverifyauth.ToString() + " no soportada aun", null)));
        //        }

        //        //2) Genero nuevo trackid
        //        new_trackid = BPUnitOfWork.GetTrackIdUnique();
        //        if (string.IsNullOrEmpty(new_trackid))
        //        {
        //            LOG.Warn("BPWebController.TxCreate - Error generando rtrackid unico. Reintente...");
        //            return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                (new Error(Errors.IERR_UNKNOWN, "Error generando rtrackid unico. Reintente...", null)));
        //        }

        //        //3) Chequeo si esta o no enrolado el valueid y chequeo tipo de tx(onboarding o authentication)
        //        //1 - OnBoarding Forzado | 0 - Authentication default y solo OnBoarding si no está enrolado
        //        // Pasos:
        //        //    1.- Si viene 1 no viene fijo onboarding => Chequeo si esta enrolado. 
        //        //        1.1.- Si no esta enrolado => Seteo OnBoarding
        //        //        1.2.- Si esta => Seteo Authentication
        //        LOG.Debug("BPWebController.TxCreate - paramIn.onboardingmandatory = " + paramIn.onboardingmandatory);
        //        int typeverify = paramIn.onboardingmandatory;
        //        APIRest.Models.OnboardingType obtype = paramIn.onboardingtype;
        //        if (typeverify == 0)  //Esto indica que no es mandatorio OnBoarding => Debo revisar que este enrolado 
        //        {
        //            LOG.Debug("BPWebController.TxCreate - BPUnitOfWork.IsIdentitiyEnrolled in con  paramIn.typeonboarding = " +
        //                 paramIn.typeverifyonboarding + " y paramIn.typeauth = " + paramIn.typeverifyauth + "...");
        //            //TODO - Armar la logica de type/onboarding de acuerdo a excel
        //            typeverify = 
        //                BPUnitOfWork.CheckLogicalOnboardingAuth(paramIn.company, paramIn.typeid, paramIn.valueid, 
        //                                                 paramIn.typeverifyonboarding, paramIn.typeverifyauth, 
        //                                                 param.onboardingtype, out obtype, out trackid3roEnrollORFacemap);
        //            LOG.Debug("BPWebController.TxCreate - typeverify = " + typeverify);
        //            if (typeverify != 0 && typeverify != 1) //Si no es 0 o 1 => Aviso de error y no sigo
        //            {
        //                return Request.CreateResponse(HttpStatusCode.Conflict,
        //                              (new Error(Errors.IERR_NO_ENROLLTYPE_ENABLED, 
        //                              "BPWebController.TxCreate Error creando workflow de verificacion [" + 
        //                              typeverify.ToString() + "]", null)));
        //            }
        //        } else //Si es onboarding seteado desde param => Verifico que se pueda hacer o sale por incompatibilida
        //        {
        //            //Chequeo que los parametros sean compatibles
        //            typeverify =
        //                BPUnitOfWork.SetOBandOBType(paramIn.typeverifyonboarding, paramIn.typeverifyauth,
        //                                                 param.onboardingtype, out obtype);
        //            LOG.Debug("BPWebController.TxCreate - typeverify = " + typeverify);
        //            if (typeverify != 0 && typeverify != 1) //Si no es 0 o 1 => Aviso de error y no sigo
        //            {
        //                return Request.CreateResponse(HttpStatusCode.Conflict,
        //                              (new Error(Errors.IERR_NO_ENROLLTYPE_ENABLED,
        //                              "BPWebController.TxCreate Error creando workflow de verificacion [" +
        //                              typeverify.ToString() + "]", null)));
        //            }
        //        }
        //        LOG.Debug("BPWebController.TxCreate - Sigue con typeverify = " + typeverify);
        //        //4) Si es un pedido para generacion de verify/ auth en 3ro => Genero trackid en 3ro
        //        CreateModelR resp = null;
        //        CreateModelR resp2 = null;
        //        string _url3ro = null;
        //        string _session = null;
        //        TypeVerify tv = (typeverify == 0) ? paramIn.typeverifyauth : paramIn.typeverifyonboarding;
        //        LOG.Debug("BPWebController.TxCreate - tv => " + tv.ToString());
        //        switch (tv)
        //        {
        //            case TypeVerify.tv2d:
        //                break;
        //            case TypeVerify.tv3d: //Facetec
        //                LOG.Debug("BPWebController.TxCreate - Entro por Facetec...");
        //                resp = Global.FACETEC_HELPER.CrateTx(out _session);
        //                _url3ro = trackid3roEnrollORFacemap;
        //                break;
        //            case TypeVerify.tv3dplus: //Jumio Automatico
        //                resp = Global.JUMIO_HELPER.CrateTx(typeverify, new_trackid, trackid3roEnrollORFacemap, out _url3ro);
        //                if (typeverify == 1 && obtype == OnboardingType.combined) //Si es onboardion obligatorio y es combinado => Facetec
        //                {
        //                    resp2 = Global.FACETEC_HELPER.CrateTx(out _session);
        //                }
        //                break;
        //            case TypeVerify.tv3dstrong: //Jumio Hibrido
        //                resp = Global.JUMIO_HELPER.CrateTx(typeverify, new_trackid, trackid3roEnrollORFacemap, out _url3ro);
        //                if (typeverify == 1 && obtype == OnboardingType.combined) //Si es onboardion obligatorio y es combinado => Facetec
        //                {
        //                    resp2 = Global.FACETEC_HELPER.CrateTx(out _session);
        //                }
        //                break;
        //            default:
        //                break;
        //        }
        //        LOG.Debug("BPWebController.TxCreate - _url3ro = " + (string.IsNullOrEmpty(_url3ro)?"NULL": _url3ro));

        //        //5) Creo el workflow que debe realizar el BPWeb de acuerdo a los parámetros 
        //        string msgerr = null;
        //        string jsonWorkflow = null;
        //        LOG.Debug("BPWebController.TxCreate - Ingreso a CreaWorkflow...");
        //        int ret  = BPUnitOfWork.CreaWorkflow(paramIn, tv, typeverify, _session, _url3ro, obtype, out jsonWorkflow, out msgerr);
        //        LOG.Debug("BPWebController.TxCreate - CreaWorkflow Ret = " + ret);
        //        if (ret < 0 || jsonWorkflow == null)
        //        {
        //            return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                (new Error(Errors.IERR_UNKNOWN, "BPWebController.TxCreate Error generando el Workflow de wozard para BPWeb [" 
        //                + (string.IsNullOrEmpty(msgerr)?"Desconocido":msgerr) + "]", null)));
        //        }

        //        //6) Grabo datos de bp_tx con datos adicionales
        //        int operationcode = (resp != null || tv==TypeVerify.tv2d) ? 0 : -1;
        //        string auxMessageAut = (paramIn.autorizationinclude == 1) ? 
        //                                ((string.IsNullOrEmpty(paramIn.autorizationmessage)?
        //                                Global._DISCLAIMER_AUTORIZATION_BPWEB:
        //                                paramIn.autorizationmessage)): null;
        //        LOG.Debug("BPWebController.TxCreate - Ingresa a AdminBpTx.CreateTx...");
        //        ret = AdminBpTx.CreateTx(9, operationcode, paramIn.company, paramIn.typeid, paramIn.valueid, param.taxidcompany, new_trackid,
        //                                     (int)tv, paramIn.customertrackid, paramIn.callbackurl, paramIn.redirecturl, paramIn.successurl,
        //                                     paramIn.signerinclude, paramIn.checklocksrcei, typeverify, paramIn.theme, paramIn.threshold, 
        //                                     (resp!=null)?resp.trackid:"NA", 4, 41, 16, _url3ro, _session, paramIn.titlebpweb, paramIn.messagebpweb,
        //                                     paramIn.autorizationinclude, auxMessageAut, jsonWorkflow, 
        //                                     paramIn.videoinclude, paramIn.videomessage, (int)obtype,
        //                                     paramIn.carregisterinclude, paramIn.writinginclude, param.formid,
        //                                     out msgerr);
        //        LOG.Debug("BPWebController.TxCreate - Sale de AdminBpTx.CreateTx => ret = " + ret);
        //        //7) Retorno
        //        return Request.CreateResponse(HttpStatusCode.OK, 
        //                                      (new CreateModelR(Errors.IERR_OK, null, 
        //                                       new_trackid, Properties.Settings.Default.BPWeb_URL + "?trackid=" + new_trackid)));
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("BPWebController.TxCreate Error: " + ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                (new Error(Errors.IERR_UNKNOWN, "BPWebController.TxCreate Error [" + ex.Message + "]", null)));
        //    }
        //}



        ///// <summary>
        ///// Recupera información de enrolameinto de una identidad
        ///// </summary>
        ///// <remarks>
        ///// Utiliza Basic Authentication [ej.: request.AddHeader("Authorization", "Basic dXNlcm5hbWU3OnNlY3JldGtleTc=")] - 
        ///// Dada la comañia + typeid + valueid, retorna status de ennrolameinto:
        /////     * code: 0 si ejecuto ok o menor a 0 si dio error o no encontro informacion
        /////     * exist: 0/1 si existe o no
        /////     * isenrolledtv2d: 0-No o 1-Si
        /////     * isenrolledtv3d: 0-No o 1-Si
        /////     * isenrolledtv3dplus: 0-No o 1-Si
        ///// </remarks>
        ///// <param name="param"></param>
        ///// <returns></returns>
        //[Route("api/userEnrolled")]
        //[HttpGet]
        //[SwaggerResponse(HttpStatusCode.OK, "UserEnrolledModelR", typeof(UserEnrolledModelR))]
        //public object UserEnrolled(int companyid, string typeid, string valueid) //[FromBody] UserEnrolledModel param)
        //{
        //    //return null;
        //    /*
        //        Los pasos a realizar son:
        //        1) Leo param y verifico existencia (login/token/?)
        //        2) Recupero Identity => Si no existe retorna error
        //        3) Si existe veo si estan enrolados servicioseros y cargo variables de salida
        //        4) Retorno
        //    */
        //    string msg = "";
        //    UserEnrolledModelR ueResponse = null;
        //    try
        //    {
        //        LOG.Debug("BPWebController.UserEnrolled - IN...");

        //        //0) Check Basic Authentication
        //        if (!BPUnitOfWork.Authorized(Request, out msg))
        //        {
        //            LOG.Warn("BPWebController.TxCreate - No Autorizado!");
        //            return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //                (new Error(Errors.IERR_CONX_WS, "Acceso NO Autorizado [" +
        //                                (string.IsNullOrEmpty(msg) ? "" : msg) + "]", null)));
        //        }

        //        //1) Leo parametros, parseo y chequeo consistencia de datos(login / token /?)
        //        if (companyid <= 0 || string.IsNullOrEmpty(typeid) || string.IsNullOrEmpty(valueid))
        //        {
        //            LOG.Warn("BPWebController.UserEnrolled - parametro nulo");
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                new Error(Errors.IERR_BAD_PARAMETER, "Parametro de entrada no puede ser nulo", null));
        //        }

        //        msg = BPUnitOfWork.CheckStatusCompanyAndUser(companyid, msg);
        //        if (companyid <= 0 || msg.Length > 0)
        //        {
        //            LOG.Warn("BPWebController.UserEnrolled - Acceso No Permitido [" + msg + "]");
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                (new CreateModelR(Errors.IERR_BAD_PARAMETER, "Acceso No Permitido [" + msg + "]", null)));
        //        }

        //        //2) Recupero Identity => Si no existe retorna error
        //        string msgerr = null;
        //        UserEnrolledModel param = new UserEnrolledModel(companyid, typeid, valueid);
        //        HttpStatusCode ret = BPUnitOfWork.CheckUserEnrolled(param, out ueResponse, out msgerr);
        //        if (ret != HttpStatusCode.OK)
        //        {
        //            LOG.Error("BPWebController.UserEnrolled - Error chequeando identity, o no existe...[ret=" +
        //                ret.ToString() + "]");
        //            return Request.CreateResponse(ret,
        //                (new Error(Errors.IERR_DATABASE, "BPWebController.UserEnrolled - Error chequeando identity, o no existe...[ret=" +
        //                ret.ToString() + " - " + (string.IsNullOrEmpty(msgerr)?"":msgerr) +"]",null)));
        //        }
        //        //3 - Retorno
        //        return Request.CreateResponse(ret, ueResponse);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("BPWebController.UserEnrolled Excp: " + ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                (new Error(Errors.IERR_UNKNOWN, "BPWebController.UserEnrolled Excp [" + ex.Message + "]", null)));
        //    }
        //}

        ///// <summary>
        ///// Retorna status de la transaccion
        ///// </summary>
        ///// <remarks>
        ///// Utiliza Basic Authentication [ej.: request.AddHeader("Authorization", "Basic dXNlcm5hbWU3OnNlY3JldGtleTc=")] - 
        ///// Dado un trackid unico, consulta en BioPortal, para saber estado de la misma. 
        ///// Los estados posibles son:
        /////     * PENDING = 0,
        /////     * DONE = 1,
        /////     * FAILED = 2
        ///// </remarks>
        ///// <param name="trackid">Track id único que debe haberse generado antes</param>
        ///// <response code="200">Tx Creada ok</response>
        ///// <response code="404">Parámetros erroneos</response>
        ///// <response code="4xx">Errores a Definir</response>
        ///// <response code="500">Error Desconocido Interno</response>
        ///// <returns>Estado de la transaccion</returns>
        //[Route("api/txGetStatus")]
        //[HttpGet]
        //[SwaggerResponse(HttpStatusCode.OK, "StatusModelR", typeof(StatusModelR))]
        //public object TxStatus(string trackid)
        //{
        //    /*
        //       Los pasos a realizar son:
        //       1) Leo parametros, chequeo consistencia de datos (login/token/?)
        //       2) Leo info de trackid desde BD =>
        //           2.1) Si error => Retorono error 500
        //           2.2) Si no existe => Retorno Error Not Found
        //           2.3) Si lee => Armo salida 
        //       3) Retorno
        //   */
        //    string msg = "";
        //    StatusModelR smResponse = null;
        //    try
        //    {
        //        LOG.Debug("BPWebController.TxStatus - IN...");

        //        //0) Check Basic Authentication
        //        if (!BPUnitOfWork.Authorized(Request, out msg))
        //        {
        //            LOG.Warn("BPWebController.TxCreate - No Autorizado!");
        //            return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //                (new Error(Errors.IERR_CONX_WS, "Acceso NO Autorizado [" +
        //                                (string.IsNullOrEmpty(msg) ? "" : msg) + "]", null)));
        //        }

        //        //1) Leo parametros, parseo y chequeo consistencia de datos(login / token /?)
        //        if (string.IsNullOrEmpty(trackid))
        //        {
        //            LOG.Warn("BPWebController.TxStatus - parametro nulo");
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                new Error(Errors.IERR_BAD_PARAMETER, "Parametro de entrada [trackid] no puede ser nulo", null));
        //        }

        //        //TODO - Check Company Access
        //        //msg = BPUnitOfWork.CheckStatusCompanyAndUser(paramIn.company, msg);
        //        //if (paramIn.company <= 0 || msg.Length > 0)
        //        //{
        //        //    LOG.Warn("BPWebController.TxCreate - Acceso No Permitido [" + msg + "]");
        //        //    return Request.CreateResponse(HttpStatusCode.BadRequest,
        //        //        (new InitializationModel(Errors.IERR_BAD_PARAMETER, "Acceso No Permitido[" + msg + "]")));
        //        //}


        //        //2) Leo info de trackid desde BD =>
        //        HttpStatusCode ret = BPUnitOfWork.GetStatusTxFromTrackId(trackid, out smResponse);
        //        if (ret != HttpStatusCode.OK)
        //        {
        //            LOG.Error("BPWebController.TxStatus - Error recuperando TrackId = " + trackid + ", o no existe...[ret=" +
        //                ret.ToString() + "]");
        //            if (ret == HttpStatusCode.NotFound)
        //            {
        //                return Request.CreateResponse(HttpStatusCode.NotFound,
        //                   (new Error(Errors.IERR_NO_LOCAL_DATA, "BPWebController.txGetData Trackid no encontrado [" + ret.ToString() + " - " +
        //                   ((smResponse != null && !string.IsNullOrEmpty(smResponse.msgerr)) ? "Desconocido" : smResponse.msgerr) + "]",
        //                    trackid)));
        //            }
        //            else
        //            {
        //                return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                       (new Error(Errors.IERR_UNKNOWN, "BPWebController.txGetData Error Desconocido [" + ret.ToString() + " - " +
        //                       ((smResponse != null && !string.IsNullOrEmpty(smResponse.msgerr)) ? "Desconocido" : smResponse.msgerr) + "]",
        //                        trackid)));
        //            }
        //        }
        //        //3 - Retorno
        //        return Request.CreateResponse(ret, smResponse);

        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("BPWebController.TxStatus Error: " + ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                (new Error(Errors.IERR_UNKNOWN, "Error desconocido. Reintente...", trackid)));
        //    }
        //}

        ///// <summary>
        ///// Recupera datos relacionados con una transaccion
        ///// </summary>
        ///// <remarks>
        ///// Utiliza Basic Authentication [ej.: request.AddHeader("Authorization", "Basic dXNlcm5hbWU3OnNlY3JldGtleTc=")] - 
        ///// Dado un trackid unico, y tipo de data pedida, se recupera toda la data relacionada
        ///// con la transaccion del tipo pedido:
        /////     * 0 -> Basico
        /////     * 1 -> 0 + Datos (sin imagenes)
        /////     * 2 -> 1 + Imagenes
        ///// </remarks>
        ///// <param name="trackid">Track id único que debe haberse generado antes</param>
        ///// <param name="flag">0-Basico | 1-0+Texto | 2 - 1+Images</param>
        ///// <response code="200">Tx Creada ok</response>
        ///// <response code="404">Parámetros erroneos</response>
        ///// <response code="4xx">Errores a Definir</response>
        ///// <response code="500">Error Desconocido Interno</response>
        ///// <returns>Estado de la transaccion</returns>
        //[Route("api/txGetData")]
        //[HttpGet]
        //[SwaggerResponse(HttpStatusCode.OK, "TxDataModel", typeof(TxDataModel))]
        //public object TxGetData(string trackid, int flag)
        //{
        //    /*
        //       Los pasos a realizar son:
        //       1) Leo parametros, chequeo consistencia de datos (login/token/?)
        //       2) Leo info de trackid desde BD =>
        //           2.1) Si error => Retorono error 500
        //           2.2) Si no existe => Retorno Error Not Found
        //           2.3) Si lee => Armo salida 
        //       3) Retorno
        //   */
        //    string msg = "";
        //    TxDataModel txResponse = null;
        //    try
        //    {
        //        LOG.Debug("BPWebController.txGetData - IN...");

        //        //0) Check Basic Authentication
        //        if (!BPUnitOfWork.Authorized(Request, out msg))
        //        {
        //            LOG.Warn("BPWebController.TxCreate - No Autorizado!");
        //            return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //                (new Error(Errors.IERR_CONX_WS, "Acceso NO Autorizado [" +
        //                                (string.IsNullOrEmpty(msg) ? "" : msg) + "]", null)));
        //        }

        //        //1) Leo parametros, parseo y chequeo consistencia de datos(login / token /?)
        //        if (string.IsNullOrEmpty(trackid))
        //        {
        //            LOG.Warn("BPWebController.txGetData - parametro nulo");
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                new Error(Errors.IERR_BAD_PARAMETER, "Parametro de entrada [trackid] no puede ser nulo", null));
        //        }

        //        //TODO - Check Company Access
        //        //msg = BPUnitOfWork.CheckStatusCompanyAndUser(paramIn.company, msg);
        //        //if (paramIn.company <= 0 || msg.Length > 0)
        //        //{
        //        //    LOG.Warn("BPWebController.TxCreate - Acceso No Permitido [" + msg + "]");
        //        //    return Request.CreateResponse(HttpStatusCode.BadRequest,
        //        //        (new InitializationModel(Errors.IERR_BAD_PARAMETER, "Acceso No Permitido[" + msg + "]")));
        //        //}


        //        //2) Leo info de trackid desde BD =>
        //        HttpStatusCode ret = BPUnitOfWork.GetDataTxFromTrackId(trackid, flag, out txResponse);
        //        if (ret != HttpStatusCode.OK)
        //        {
        //            LOG.Error("BPWebController.txGetData - Error recuperando TrackId = " + trackid + ", o no existe...[ret=" +
        //                ret.ToString() + "]");
        //            if (ret == HttpStatusCode.InternalServerError)
        //            {
        //                return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                    (new Error(Errors.IERR_UNKNOWN, "BPWebController.txGetData Error [" + ret.ToString() + " - " +
        //                    ((txResponse != null && !string.IsNullOrEmpty(txResponse.msgerr)) ? "Desconocido" : txResponse.msgerr) + "]",
        //                     trackid)));
        //            }
        //            else if (ret == HttpStatusCode.NotFound)
        //            {
        //                return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
        //                    (new Error(Errors.IERR_IDENTITY_NOT_FOUND, "BPWebController.txGetData Transaccion con error [" + ret.ToString() + " - " +
        //                    ((txResponse != null && !string.IsNullOrEmpty(txResponse.msgerr)) ? "Desconocido" : txResponse.msgerr) + "]",
        //                     trackid)));
        //            }
        //            else if (ret == HttpStatusCode.NotAcceptable)
        //            {
        //                return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                    (new Error(Errors.IERR_VERIFY, "BPWebController.txGetData Verificacion Negativa [" + ret.ToString() + " - " +
        //                    ((txResponse != null && !string.IsNullOrEmpty(txResponse.msgerr)) ? "Desconocido" : txResponse.msgerr) + "]",
        //                     trackid)));
        //            }
        //            else if (ret == HttpStatusCode.ExpectationFailed)
        //            {
        //                //Retorna OK, pero lo que hace es indicar el error obtenido
        //                return Request.CreateResponse(HttpStatusCode.OK, txResponse);

        //                //(new Error(Errors.IERR_VERIFY, "BPWebController.txGetData Verificacion Negativa [" + ret.ToString() + " - " +
        //                //((txResponse != null && !string.IsNullOrEmpty(txResponse.msgerr)) ? "Desconocido" : txResponse.msgerr) + "]",
        //                // trackid)));
        //            }
        //            else if (ret == HttpStatusCode.NotAcceptable)
        //            {
        //                //Retorna OK, pero lo que hace es indicar el error obtenido
        //                return Request.CreateResponse(HttpStatusCode.OK, txResponse);
        //            }
        //        }
        //        //3 - Retorno
        //        return Request.CreateResponse(ret, txResponse);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("BPWebController.txGetData Error: " + ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                (new Error(Errors.IERR_UNKNOWN, "Parametro de entrada nulo. Reintente...", trackid)));
        //    }
        //}


        //#endregion SDK for 3ra Parte


        //#region BPWeb 

        ///// <summary>
        ///// Inicia el proceso de verificacion. 
        ///// </summary>
        ///// <remarks>
        ///// Revisa que todos los elementos necesarios para procesar la verificacion de esta transacción estén cargados, 
        ///// lo realiza, y retorna el resultado si es un proceso sincronico (facetec). 
        ///// En caso que sea asincronico el status devuelto sería **VERIFYING**.
        ///// Los status posibles son: 
        /////     * NOTEXIST - trackid no existente
        /////     * CREATED - creado pero aun no inicio
        /////     * PROCESSING - cuando se empezaron a cargar los samples para proceso
        /////     * VERIFYING - cuando se dio inicio al proceso de verificacion con esta api
        /////     * DONE - Termino el proceso de verificacion (con resultado positivo o negativo pero sin errores
        /////     * FAILED - Se produjo un error con la transacción. Se puede reprocesar
        ///// </remarks>
        ///// <param name="trackid"></param>
        ///// <returns></returns>
        //[Route("api/txVerify")]
        //[HttpGet]
        //[SwaggerResponse(HttpStatusCode.OK, "VerifyModelR", typeof(VerifyModelR))]
        //public object TxVerify(string trackid)
        //{
        //    //return null;
        //    /*
        //        Los pasos a realizar son:
        //        1) Leo TRackid y verifico existencia (login/token/?)
        //        2) Recupero Tx con el trackid => Si no existe retorna error
        //        3) Reviso que tenga todo lo necesario de acuerdo al proceso de verificacion
        //           3.1) Si falta algo retorno el/los faltantes
        //           3.2) Si esta todo hago verify contra Facetec Server
        //        4) Grabo datos de bp_tx con datos de respuesta
        //        5) Si es OnBoarding => Hago update de datos bp_identity
        //        6) Retorno
        //    */
        //    string msg = "";
        //    VerifyModelR vfResponse = null;
        //    try
        //    {
        //        LOG.Debug("BPWebController.TxVerify - IN...");

        //        //0) Check Basic Authentication
        //        //if (!BPUnitOfWork.Authorized(Request, out msg))
        //        //{
        //        //    LOG.Warn("BPWebController.TxCreate - No Autorizado!");
        //        //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //        //        (new Error(Errors.IERR_CONX_WS, "Acceso NO Autorizado [" +
        //        //                        (string.IsNullOrEmpty(msg) ? "" : msg) + "]", null)));
        //        //}

        //        //1) Leo parametros, parseo y chequeo consistencia de datos(login / token /?)
        //        if (string.IsNullOrEmpty(trackid))
        //        {
        //            LOG.Warn("BPWebController.TxVerify - parametro nulo");
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                new Error(Errors.IERR_BAD_PARAMETER, "Parametro de entrada [trackid] no puede ser nulo", null));
        //        }

        //        //TODO - Check Company Access
        //        //msg = BPUnitOfWork.CheckStatusCompanyAndUser(paramIn.company, msg);
        //        //if (paramIn.company <= 0 || msg.Length > 0)
        //        //{
        //        //    LOG.Warn("BPWebController.TxCreate - Acceso No Permitido [" + msg + "]");
        //        //    return Request.CreateResponse(HttpStatusCode.BadRequest,
        //        //        (new CreateModelR(Errors.IERR_BAD_PARAMETER, "Acceso No Permitido[" + msg + "]", null)));
        //        //}


        //        //2) Leo info de trackid desde BD y hago verificacion o retorno error =>
        //        string msgerr = null;
        //        HttpStatusCode ret = BPUnitOfWork.Verify(trackid, out vfResponse, out msgerr);
        //        if (ret != HttpStatusCode.OK)
        //        {
        //            LOG.Error("BPWebController.txUpdate - Error en Verify TrackId = " + trackid + ", o no existe...[ret=" +
        //                ret.ToString() + "]");
        //            return Request.CreateResponse(HttpStatusCode.NotModified,
        //                (new Error(Errors.IERR_DATABASE, "BPWebController.txUpdate Error [" + ret.ToString() + " - " +
        //                ((vfResponse != null && !string.IsNullOrEmpty(msgerr)) ? "Desconocido" : msgerr) + "]",
        //                  trackid)));
        //        }
        //        //3 - Retorno
        //        return Request.CreateResponse(ret, vfResponse);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("BPWebController.txUpdate Error: " + ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                (new Error(Errors.IERR_UNKNOWN, "BPWebController.txUpdate Error [" + ex.Message + "]", trackid)));
        //    }
        //    //VerifyModelR ret = new VerifyModelR("234hj234hjg23h4g2jh42jh3", "NOTEXIST", 0, 0, 0, "TrackId no Existe");
        //    //return ret;
        //}

        //[Route("api/WebhookJumio")]
        //[HttpPost]
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public object WebhookJumio(Data param)
        //{
        //    int flag = 0;
        //    try
        //    {
        //        LOG.Debug("BPWebController.WebhookJumio IN..."); 
        //        //string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
        //        string jumioIdScanReference = System.Web.HttpContext.Current.Request.Params["jumioIdScanReference"];
        //        LOG.Debug("BPWebController.WebhookJumio - Recibido jumioIdScanReference = " + jumioIdScanReference);
        //        if (!string.IsNullOrEmpty(jumioIdScanReference))
        //        {
        //            LOG.Debug("BPWebController.WebhookJumio - Iniciando UpdateStatus...");
        //            int ret = BPUnitOfWork.UpdateStatus(null, jumioIdScanReference, 1); //0 = OnBoarding
        //            if (ret < 0) LOG.Warn("BPUnitOfWork.GetStatusTxFromTrackId - Error updateando resultado [" + ret + "]");
        //            else LOG.Debug("BPWebController.WebhookJumio -  Update OK!");
        //        } else
        //        {
        //            LOG.Warn("BPWebController.WebhookJumio - Se recibio POST pero jumioIdScanReference es NULL");
        //            return Request.CreateResponse(HttpStatusCode.NotFound);
        //        } 
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = -1;
        //        LOG.Error("BPWebController.WebhookJumio Error: " + ex.Message);
        //    }
        //    LOG.Debug("BPWebController.WebhookJumio - OUT [" + (flag == 0 ? "OK" : "Error") + "]");
        //    return (flag == 0? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateResponse(HttpStatusCode.InternalServerError));
        //}

        //[Route("api/WebhookJumioAuth")]
        //[HttpPost]
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public object WebhookJumioAuth(Data param)
        //{
        //    int flag = 0;
        //    try
        //    {
        //        LOG.Debug("BPWebController.WebhookJumio IN...");
        //        //string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
        //        string transactionReference = System.Web.HttpContext.Current.Request.Params["transactionReference"];
        //        //string livenessImages = System.Web.HttpContext.Current.Request.Params["livenessImages"];
        //        //string idScanImageFace = System.Web.HttpContext.Current.Request.Params["idScanImageFace"];


        //        LOG.Debug("BPWebController.WebhookJumio - Recibido jumioIdScanReference = " + transactionReference);
        //        if (!string.IsNullOrEmpty(transactionReference))
        //        {
        //            LOG.Debug("BPWebController.WebhookJumio - Iniciando UpdateStatus...");
        //            int ret = BPUnitOfWork.UpdateStatus(null, transactionReference, 0); //0 = Authentication
        //            if (ret < 0) LOG.Warn("BPUnitOfWork.GetStatusTxFromTrackId - Error updateando resultado [" + ret + "]");
        //            else LOG.Debug("BPWebController.WebhookJumio -  Update OK!");
        //        }
        //        else
        //        {
        //            LOG.Warn("BPWebController.WebhookJumio - Se recibio POST pero jumioIdScanReference es NULL");
        //            return Request.CreateResponse(HttpStatusCode.NotFound);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = -1;
        //        LOG.Error("BPWebController.WebhookJumio Error: " + ex.Message);
        //    }
        //    LOG.Debug("BPWebController.WebhookJumio - OUT [" + (flag == 0 ? "OK" : "Error") + "]");
        //    return (flag == 0 ? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateResponse(HttpStatusCode.InternalServerError));
        //}

        //[Route("api/WebhookJumioAuthSuccess")]
        //[HttpGet]
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public object WebhookJumioAuthResult(string transactionStatus, string transactionReference, string errorCode) //[FromUri] object param)
        //{
        //    string trackId = null;
        //    string msgerr = null;
        //    try
        //    {
        //        LOG.Debug("BPWebController.WebhookJumioAuthResult - IN...");

        //        //1)Chequeo parametroos
        //        if (string.IsNullOrEmpty(transactionStatus) || string.IsNullOrEmpty(transactionReference))
        //        {
        //            LOG.Debug("BPWebController.WebhookJumioAuthResult - Error parametros de entrada [Null]");

        //            return CreateResponseHtml(1, "Error procesando el resultado de la autenticación. Reintente...");
        //            //return "<p>Problemas para verificar registro. Complete y reintente...</p>"; 
        //            //(new ResponseModel(Errors.IRET_API_BAD_REQUEST, "Problemas para verificar registro. Complete y reintente..."));
        //        }

        //        //2) Reviso que exista transaccion
        //        LOG.Debug("BPWebController.WebhookJumioAuthResult - Procesando transactionReference = " + transactionReference + "...");
        //        string txtrackid = AdminBpTxConx.RetrieveTrackIdFromTrackid3ro(transactionReference, out msgerr);
                
        //        if (string.IsNullOrEmpty(txtrackid))
        //        {
        //            LOG.Error("BPWebController.WebhookJumioAuthResult - Tx TrackId no existe!");
        //            return CreateResponseHtml(1, "Transaccion de Autentitcación NO existe. Reinicie el proceso...");
        //        }

        //        //4) Update datos en BPTx y Conx
        //        int ret = 0;
        //        //ret = BPUnitOfWork.UpdateUthenticationResult(transactionStatus, transactionReference, errorCode);

        //        if (ret < 0)
        //        {
        //            LOG.Error("BPWebController.WebhookJumioAuthResult - Error actualizando status autenticacion  [" + ret + "]");
        //            return CreateResponseHtml(1, "Error actualizando status autenticacion  [" + ret + "]. Reintente...");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("BPWebController.WebhookJumioAuthResult Error Desconcido procesando actualizacion de autenticacion : " + ex.Message);
        //        return CreateResponseHtml(1, "Error Desconcido procesando actualizacion de autenticacion ["
        //                                          + ex.Message + "]");
        //    }
        //    LOG.Error("BPWebController.WebhookJumioAuthResult");
        //    return CreateResponseHtml(0, "<b>Autenticacion</b> actualizada con exito!"); //"<h2>Litole!</h2>"; //

        //}

        ///// <summary>
        ///// Inicia proceso desde BPWeb
        ///// </summary>
        ///// <remarks>
        ///// Dado un trackid unico, consulta en BioPortal, los valores para poder guiar al usuario
        ///// en BPWeb con el wizard. Se controlan aqui varios factores como que la transacción existe, 
        ///// no esté vencida, etc.
        ///// Devuelve un workflow de las evidencias pedidas al bpweb, ordenadas. Actualmente los pasos que se pueden pedir son:
        /////     * autorization: si se pidio pedir autorizacion al usuario verificado. Se envia en metadata el messagetodisplay (Formateado HTML)
        /////     * signaturemanual: si se pidio firma olográfica. Sin metadata adicional
        /////     * geolocation: Puede ser opcional. 
        /////     * jumio: puede ser onboarding o auth Jumio. En metadata se envía urlservicio3ro para conectarse.
        /////     * facetec: puede ser onboarding o auth facetec. se envian en metadata:
        /////         ** type [onboarding: es proceso cedula vs selfie | authentication: solo liveness para hacer authentication]  
        /////         ** licensefacetec: licencia facetec (string)
        /////         ** licenseforexncrypt: licencia facetec para encrypt (string)
        /////         ** session: session generada (string)
        /////     * video: video grabado. Se envia por metadata messagetodisplay (Formateado HTML). [Aun no implementado]
        ///// </remarks>
        ///// <param name="trackid">Track id único que debe haberse generado antes</param>
        ///// <response code="200">Tx Creada ok</response>
        ///// <response code="404">Parámetros erroneos</response>
        ///// <response code="4xx">Errores a Definir</response>
        ///// <response code="500">Error Desconocido Interno</response>
        ///// <returns>Jason conteniendo los parámetros de ejecución en BPWeb</returns>
        //[Route("api/txInitialization")]
        //[HttpGet] 
        //[SwaggerResponse(HttpStatusCode.OK, "InitializationModel", typeof(InitializationModel))]
        //public object TxInitialization(string trackid)
        //{
        //    /*
        //        Los pasos a realizar son:
        //        1) Leo parametros, chequeo consistencia de datos (login/token/?)
        //        2) Leo info de trackid desde BD =>
        //            2.1) Si error => Retorono error 500
        //            2.2) Si no existe => Retorno Error Not Found
        //            2.3) Si lee => Armo salida 
        //        3) Retorno
        //    */
        //    string msg = "";
        //    InitializationModel imResponse = null;
        //    try
        //    {
        //        LOG.Debug("BPWebController.TxInitialization - IN...");
        //        //0) Check Basic Authentication
        //        //if (!BPUnitOfWork.Authorized(Request, out msg))
        //        //{
        //        //    LOG.Warn("BPWebController.TxCreate - No Autorizado!");
        //        //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //        //        (new Error(Errors.IERR_CONX_WS, "Acceso NO Autorizado [" +
        //        //                        (string.IsNullOrEmpty(msg) ? "" : msg) + "]", null)));
        //        //}

        //        //1) Leo parametros, parseo y chequeo consistencia de datos(login / token /?)
        //        if (string.IsNullOrEmpty(trackid))
        //        {
        //            LOG.Warn("BPWebController.TxInitialization - parametro nulo");
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                (new Error(Errors.IERR_BAD_PARAMETER, "Parametro de entrada nulo. Reintente...", trackid)));
        //        }

        //        //TODO - Check Company Access
        //        //msg = BPUnitOfWork.CheckStatusCompanyAndUser(paramIn.company, msg);
        //        //if (paramIn.company <= 0 || msg.Length > 0)
        //        //{
        //        //    LOG.Warn("BPWebController.TxCreate - Acceso No Permitido [" + msg + "]");
        //        //    return Request.CreateResponse(HttpStatusCode.BadRequest,
        //        //        (new InitializationModel(Errors.IERR_BAD_PARAMETER, "Acceso No Permitido[" + msg + "]")));
        //        //}


        //        //2) Leo info de trackid desde BD =>
        //        HttpStatusCode ret = BPUnitOfWork.GetTxFromTrackId(trackid, out imResponse);
        //        if (ret != HttpStatusCode.OK)
        //        {
        //            LOG.Error("BPWebController.TxInitialization - Error recuperando TrackId = " + trackid + ", o no existe...[ret=" +
        //                ret.ToString() + "]");
        //            return Request.CreateResponse(HttpStatusCode.Conflict,
        //                (new Error(Errors.IERR_DATABASE, "BPWebController.TxInitialization Error [" + ret.ToString() + " - " +
        //                ((imResponse != null && !string.IsNullOrEmpty(imResponse.msgerr)) ? "Desconocido" : imResponse.msgerr) + "]",
        //                 trackid)));
        //        }
        //        //3 - Retorno
        //        return Request.CreateResponse(ret, imResponse);

        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("BPWebController.TxInitialization Error: " + ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                (new Error(Errors.IERR_UNKNOWN, "Parametro de entrada nulo. Reintente...", trackid)));
        //    }
        //}


        ///// <summary>
        ///// Actualiza Transaccion ya creada
        ///// </summary>
        ///// <remarks>
        ///// Dado un trackid unico, y datos para actualizar en la Tx, se actualizan los valores 
        ///// que vienen de BPWeb. Puede ser la firma manual o samples para facetec por ejemplo.
        ///// Por ahora una única API TxUpdate recibe todos los parámetros. 
        ///// Esta se reempalzará por las siguientes APIs, en las próximas versiones:
        /////     * api/txUpdate/autorization
        /////     * api/txUpdate/geolocation
        /////     * api/txUpdate/signaturemanual
        /////     * api/txUpdate/video
        /////     * api/txUpdate/facetec
        /////     * api/txUpdate/jumio
        /////</remarks>
        ///// <param name="param">Parametros de entrada
        ///// </param>
        ///// <response code="200">Tx Creada ok</response>
        ///// <response code="404">Parámetros erroneos</response>
        ///// <response code="4xx">Errores a Definir</response>
        ///// <response code="500">Error Desconocido Interno</response>
        ///// <returns>Jason con mensaje de error si el code es distinto a 200</returns>
        //[Route("api/txUpdate")]
        //[HttpPost]
        //[SwaggerResponse(HttpStatusCode.OK, "UpdateModelR", typeof(UpdateModelR))]
        //public object TxUpdate(UpdateModel param)
        //{
        //    //return null;
        //    /*
        //        Los pasos a realizar son:
        //        1) Leo parametros, parseo y chequeo consistencia de datos (login/token/?)
        //        2) Recupero Tx con el trackid => Si no existe retorna 
        //        3) Recolecto datos de la lista para updatear, actualizando en la tx lo que se vaya a grabar en BD
        //        5) Grabo datos de bp_tx con datos updateados
        //        6) Retorno
        //    */
        //    UpdateModel paramIn;
        //    string msg = "";
        //    UpdateModelR umResponse = null;
        //    try
        //    {
        //        LOG.Debug("BPWebController.txUpdate - IN...");
        //        //0) Check Basic Authentication
        //        //if (!BPUnitOfWork.Authorized(Request, out msg))
        //        //{
        //        //    LOG.Warn("BPWebController.TxCreate - No Autorizado!");
        //        //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //        //        (new Error(Errors.IERR_CONX_WS, "Acceso NO Autorizado [" +
        //        //                        (string.IsNullOrEmpty(msg) ? "" : msg) + "]", null)));
        //        //}

        //        //1) Leo parametros, parseo y chequeo consistencia de datos(login / token /?)
        //        if (param == null || string.IsNullOrEmpty(param.trackid))
        //        {
        //            LOG.Warn("BPWebController.txUpdate - parametro nulo");
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                (new Error(Errors.IERR_BAD_PARAMETER, "Parametro de entrada nulo o trackid nulo. Reintente...", null)));
        //        } 
        //        paramIn = param; // JsonConvert.DeserializeObject<CreateModel>(param);

        //        //Recolecto info desde request para llenar IP y EndUSer, para colocar en lugares como CI en IdDispositivo
        //        string browser;
        //        string so;
        //        int retG = Bio.Core.Utils.RequestHelper.GetBrowserAndOS(System.Web.HttpContext.Current, out browser, out so);
               
        //            //TODO - Check Company Access
        //            //msg = BPUnitOfWork.CheckStatusCompanyAndUser(paramIn.company, msg);
        //            //if (paramIn.company <= 0 || msg.Length > 0)
        //            //{
        //            //    LOG.Warn("BPWebController.TxCreate - Acceso No Permitido [" + msg + "]");
        //            //    return Request.CreateResponse(HttpStatusCode.BadRequest,
        //            //        (new CreateModelR(Errors.IERR_BAD_PARAMETER, "Acceso No Permitido[" + msg + "]", null)));
        //            //}


        //            //2) Leo info de trackid desde BD =>
        //        HttpStatusCode ret = BPUnitOfWork.UpdateTxFromTrackId(paramIn, browser, so, out umResponse);
        //        if (ret != HttpStatusCode.OK)
        //        {
        //            LOG.Error("BPWebController.txUpdate - Error actualizando TrackId = " + paramIn.trackid + 
        //                ", no existe o no coincide documento reconocido con pedido...[ret=" + ret.ToString() + "]");
        //            string msgR = "BPWebController.txUpdate Error [" + ((int)ret).ToString() + " - " +
        //                ((umResponse != null && !string.IsNullOrEmpty(umResponse.msgerr)) ? umResponse.msgerr : "Desconocido") + "]";
        //            return Request.CreateResponse(HttpStatusCode.Conflict, 
        //                                          (new Error(Errors.IERR_DATABASE, msgR, param.trackid)));
        //            //return Request.CreateResponse(HttpStatusCode.NotModified,
        //            //   (new Error(Errors.IERR_DATABASE, "BPWebController.txUpdate Error [" + ((int)ret).ToString() + " - " +
        //            //   ((umResponse != null && !string.IsNullOrEmpty(umResponse.msgerr)) ? umResponse.msgerr : "Desconocido") + "]",
        //            //   param.trackid)));
        //        }
        //        //3 - Retorno
        //        return Request.CreateResponse(ret, umResponse);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("BPWebController.txUpdate Error: " + ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                (new Error(Errors.IERR_UNKNOWN, "BPWebController.txUpdate Error [" + ex.Message + "]", param.trackid)));
        //    }
        //}

        ///// <summary>
        ///// Obtiene datos para pintar un form si es que se necesita
        ///// </summary>
        ///// <remarks>
        ///// Dado un trackid unico, se revisa para esa tx si tiene form y si es asi, si ese form tiene una rutina asociada
        ///// para conseguir datos para pintar el form. Puede ser interno o llamados tambien a servicios externos como desde 
        ///// la patente obtener marca, modelo, etc.
        /////</remarks>
        ///// <param name="trackid"></param>
        ///// <returns>json con los datos a pintar</returns>
        //[Route("api/txUpdate/form")]
        //[HttpGet]
        //[SwaggerResponse(HttpStatusCode.OK, "FormData", typeof(FormData))]
        //public object TxGetFormForUpdate(string trackid)
        //{
        //    //[SwaggerResponse(HttpStatusCode.OK, "FormData", typeof(FormData))]//
        //    //return null;
        //    /*
        //        {
        //            "rutempresa": "76102607-0",
        //            "razonsocialempresa": "Biometrika S.A.",
        //            "nombrefirmante": "Gonzalo Mansilla",
        //            "rutfirmante": "15988779-0",
        //            "telefonofirmante": "995744080",
        //            "mailfirmante": "gonzalo.mansilla@biometrika.cl",
        //            "calle": "Avenida Salesianos 1000",
        //            "numero": "111",
        //            "departamento": "15B",
        //            "tipovivienda": "Depa",
        //            "comuna": "San miguel",
        //            "ciudad": "Santiago",
        //            "marcavehiculo": "Hyundai",
        //            "modelovehiculo": "Yaris",
        //            "patentevehiculo": "ABCD55-7",
        //            "aniovehiculo": "2016",
        //            "colorvehiculo": "Beige"
        //        }
        //        Los pasos a realizar son:
        //        1) Si trackid = null => Retorno
        //        2) Recupero Tx con el trackid => Si no existe retorna 
        //        3) Recupero el nombre del dll a ejecutar para llenar el json con los valores que se deben pintar
        //        4) Si dll de 3 es vacio => Lleno con datos base a llenar
        //        6) Retorno
        //    */
        //    string msg = "";
        //    string strJsonReturn = null;
        //    FormData formDataToReturn = new FormData();
        //    try
        //    {
        //        LOG.Debug("BPWebController.TxGetFormForUpdate - IN...");
        //        //0) Check Basic Authentication
        //        //if (!BPUnitOfWork.Authorized(Request, out msg))
        //        //{
        //        //    LOG.Warn("BPWebController.TxCreate - No Autorizado!");
        //        //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //        //        (new Error(Errors.IERR_CONX_WS, "Acceso NO Autorizado [" +
        //        //                        (string.IsNullOrEmpty(msg) ? "" : msg) + "]", null)));
        //        //}

        //        //1) Leo parametros, parseo y chequeo consistencia de datos(login / token /?)
        //        if (string.IsNullOrEmpty(trackid))
        //        {
        //            LOG.Warn("BPWebController.TxGetFormForUpdate - parametro nulo");
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                (new Error(Errors.IERR_BAD_PARAMETER, "Parametro de entrada nulo o trackid nulo. Reintente...", null)));
        //        }

        //        //2) Leo info de trackid desde BD =>
        //        HttpStatusCode ret = BPUnitOfWork.GetDataForFormTxFromTrackId(trackid, out formDataToReturn); // strJsonReturn);
        //        if (ret != HttpStatusCode.OK)
        //        {
        //            LOG.Error("BPWebController.TxGetFormForUpdate - Error recuperando datos para pintar form de TrackId = " + trackid +
        //                ", no existe o no coincide documento reconocido con pedido...[ret=" + ret.ToString() + "]");
        //            string msgR = "BPWebController.TxGetFormForUpdate - Error recuperando datos para pintar form de TrackId = " + trackid +
        //                ", no existe o no coincide documento reconocido con pedido...[ret=" + ret.ToString() + "]";
        //            return Request.CreateResponse(HttpStatusCode.Conflict,
        //                                          (new Error(Errors.IERR_DATABASE, msgR, trackid)));
        //        }
        //        //3 - Retorno
        //        return Request.CreateResponse(ret, formDataToReturn); //strJsonReturn);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("BPWebController.txUpdate Error: " + ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                (new Error(Errors.IERR_UNKNOWN, "BPWebController.txUpdate Error [" + ex.Message + "]", trackid)));
        //    }
        //}


        //#endregion BPWeb 





        //#region BP Manager 





        //#endregion BP Manager  


        //#region Utils

        //protected string GetIPAddress()
        //{
        //    System.Web.HttpContext context = System.Web.HttpContext.Current;
        //    string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //    System.Web.HttpBrowserCapabilities cap = context.Request.Browser;
        //    if (!string.IsNullOrEmpty(ipAddress))
        //    {
        //        string[] addresses = ipAddress.Split(',');
        //        if (addresses.Length != 0)
        //        {
        //            return addresses[0];
        //        }
        //    }

        //    return context.Request.ServerVariables["REMOTE_ADDR"];
        //}

        ///// <summary>
        ///// API Ping para revisar que servicio esta vivo.
        ///// </summary>
        ///// <param name="id">Entero de prueba</param>
        ///// <returns></returns>
        //[Route("api/Ping")]
        //[HttpGet]
        //public string Ping(int id)
        //{
        //    return "BPWebController Live! = " + id.ToString();
        //}

        ///// <summary>
        ///// Crea respuesta para retornar HTML cuando se pide algo a la API 
        ///// desde el mail  (Ej: validar mail)
        ///// </summary>
        ///// <param name="msg"></param>
        ///// <returns></returns>
        //private HttpResponseMessage CreateResponseHtml(int code, string msg)
        //{
        //    HttpResponseMessage response;
        //    try
        //    {
        //        LOG.Error("BPWebController.CreateResponseHtml IN...");
        //        string htmlpage = "<table border=\"0\" width=\"60%\" align=\"center\">" +
        //                           "<tr><td align=\"center\"><a href=\"http://www.notariovirtual.cl\" target=\"_blank\">" +
        //                           "<img src=\"" + Properties.Settings.Default.SITE_ROOT_URL + "/images/biometrika.png\" /></a></td></tr></table>" +
        //                    "<table border=\"0\" width=\"60%\" align=\"center\">" +
        //                      "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
        //                       "<tr><td><p style=\"font-family:'Arial'\"><b>" +
        //                       ((code == 0) ? "<font color=\"green\">Funcionamiento Correcto!</font>" :
        //                                   "<font color =\"red\">ERROR</font>")
        //                         + "</b></p></td></tr>" +
        //                        "<tr><td><p style=\"font-family:'Arial'\">" + msg + "</td></tr>" +
        //                        "<tr><td>&nbsp;</td></tr>" +
        //                        "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
        //                        "<a href=\"http://www.biometrikalatam.com\" target=\"_blank\">Biometrika Latam<a>.</p></td></tr>" +
        //                        "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
        //                    "</table>";
        //        response = new HttpResponseMessage();
        //        response.Content = new StringContent(htmlpage);
        //        response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
        //    }
        //    catch (Exception ex)
        //    {
        //        response = new HttpResponseMessage();
        //        response.Content = new StringContent("Error generando página de resultado.");
        //        response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
        //        LOG.Error("BPWebController.CreateResponseHtml Error: " + ex.Message);
        //    }
        //    LOG.Error("BPWebController.CreateResponseHtml OUT!");
        //    return response;
        //}

        //#endregion Utils

        ////// POST api/<controller>
        ////public void Post([FromBody]string value)
        ////{
        ////}

        ////// PUT api/<controller>/5
        ////public void Put(int id, [FromBody]string value)
        ////{
        ////}

        ////// DELETE api/<controller>/5
        ////public void Delete(int id)
        ////{
        ////}

        ////// GET api/<controller>
        ////public IEnumerable<string> Get()
        ////{
        ////    return new string[] { "value1", "value2" };
        ////}

        ////[Route("api/VerifiyInit")]
        ////[HttpGet]
        ////public object VerifiyInit([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.VerifiyInit(param);
        ////}

        ////[Route("api/VerifiyGetStatus")]
        ////[HttpGet]
        ////public object VerifiyGetStatus([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.VerifiyGetStatus(param);
        ////}

        ////[Route("api/WebhookJumio")]
        ////[HttpPost]
        ////public object WebhookJumio([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.WebhookJumio(param);

        ////}

        ////[Route("api/txCreate")]
        ////[HttpPost]
        ////public object TxCreate([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.VerifiyInit(param);
        ////}

        ////[Route("api/txStatus")]
        ////[HttpGet]
        ////public object TxStatus([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.VerifiyGetStatus(param);
        ////}

        ////[Route("api/txVerifyInit")]
        ////[HttpPost]
        ////public object TxVerifyInit([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.WebhookJumio(param);

        ////}

        ////[Route("api/txVerifyGet")]
        ////[HttpGet]
        ////public object TxVerifyGet([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.WebhookJumio(param);

        ////}

        ////#region Sampler

        ////[Route("api/sampler/idcard")]
        ////[HttpPost]
        ////public object SamplerIdCard([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.WebhookJumio(param);

        ////}

        ////[Route("api/sampler/jumio")]
        ////[HttpPost]
        ////public object SamplerJumio([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.WebhookJumio(param);

        ////}

        ////[Route("api/sampler/facetec")]
        ////[HttpPost]
        ////public object SamplerFacetec([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.WebhookJumio(param);

        ////}

        ////[Route("api/sampler/signer")]
        ////[HttpPost]
        ////public object SamplerSigner([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.WebhookJumio(param);

        ////}

        ////[Route("api/sampler/finger")]
        ////[HttpPost]
        ////public object SamplerFinger([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.WebhookJumio(param);

        ////}

        ////[Route("api/sampler/selfie")]
        ////[HttpPost]
        ////public object SamplerSelfie([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.WebhookJumio(param);

        ////}

        ////[Route("api/txVerifyGet")]
        ////[HttpGet]
        ////public object txVerifyGet([FromBody] object param)
        ////{
        ////    return BPUnitOfWork.WebhookJumio(param);
        ////}

        ////#endregion Sampler
    }

    //public class Data
    //{
    //    public Data() { }

    //    public string idAddress;
    //}
}