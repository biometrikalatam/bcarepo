﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bio.Portal.Server.Services.APIRest.Models.WA;
using log4net;

namespace Bio.Portal.Server.Services.APIRest.Controllers
{
    public static class WAControllerUnitOfWork
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(WAControllerUnitOfWork));

        internal static int SaveDataInHDD(EnrollModel param)
        {
            string root;
            int ret = 0; 
            try
            {
                root = HttpContext.Current.Server.MapPath(".");

                if (!System.IO.Directory.Exists(root + "\\" + param.companykey))
                {
                    System.IO.Directory.CreateDirectory(root + "\\" + param.companykey);
                }
                root = root + "\\" + param.companykey;

                foreach (WASample item in param.samples)
                {
                    System.IO.File.WriteAllText(root + param.valueid, item.data);
                    System.IO.File.WriteAllText(root + item.authenticatorfactor + "_" + item.minutiaetype + "_" +
                                                       item.bodypart + "_" + param.valueid + ".jpg", item.data);
                }
                ret = 1;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("WAControllerUnitOfWork.SaveDataInHDD Error: " + ex.Message);
            }
            return ret;
        }
    }
}