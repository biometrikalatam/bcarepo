﻿using Bio.Core.Api.Constant;
using Bio.Core.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Bio.Portal.Server.Services.APIRest
{

    /// <summary>
    /// Wrapper API Rest de WS PlugIn
    /// </summary>
    public class PluginProcessController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(typeof(PluginProcessController));

        /// <summary>
        /// Wrapper API Rest de WS PlugIn
        /// </summary>
        /// <param name="xmlParamIn"></param>
        /// <param name="customTimeOut"></param>
        /// <returns></returns>
        [Route("api/v1/pp/process")]
        [HttpPost]
        public BioPortal.Server.Api.Json.XmlParamOut Process([FromBody] BioPortal.Server.Api.Json.XmlParamIn xmlParamIn,
            int customTimeOut = 0)
        {
            try
            {
                Log.Debug("PluginProcessController.Process IN...");
                string sParamIn = XmlUtils.SerializeObject(xmlParamIn);  //XmlHelper.SerializeXmlParamIn(xmlParamIn);

                if (string.IsNullOrEmpty(sParamIn))
                    return new BioPortal.Server.Api.Json.XmlParamOut
                    {
                        ExecutionResult = Errors.IERR_SERIALIZE_XMLPARAMIN,
                        Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                    };

                string sParamOut;
                BioPortal_Server_Plugin_Process BPPPHelper = new BioPortal_Server_Plugin_Process();
                int ret = BPPPHelper.Process(sParamIn, out sParamOut);
                Log.Debug("PluginProcessController.Process Codigo respuesta BioPortal: " + ret +
                            " - Mensaje: " + Errors.GetDescription(ret));

                BioPortal.Server.Api.Json.XmlParamOut response =
                    XmlUtils.DeserializeObject<BioPortal.Server.Api.Json.XmlParamOut>(sParamOut);
                if (ret != 0)
                {
                    response.ExecutionResult = ret;
                    response.Message += ", " + Errors.GetDescription(ret);
                }
                Log.Debug("PluginProcessController.Process OUT!");
                return response;
            }
            catch (Exception ex)
            {
                Log.Error("PluginProcessController.Process Excp Error:", ex);
                return new BioPortal.Server.Api.Json.XmlParamOut
                {
                    ExecutionResult = Errors.IERR_UNKNOWN,
                    Message = Errors.GetDescription(Errors.IERR_UNKNOWN) + " [" + ex.Message + "]"
                };
            }
            
        }


        // GET: api/PluginProcess
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/PluginProcess/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/PluginProcess
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/PluginProcess/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/PluginProcess/5
        public void Delete(int id)
        {
        }
    }
}
