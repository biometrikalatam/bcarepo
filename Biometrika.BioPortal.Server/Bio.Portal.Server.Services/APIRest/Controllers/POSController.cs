﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
using Bio.Core.Constant;
using Bio.Core.Utils;
using Bio.Portal.Server.Common.Entities;
using Bio.Portal.Server.Common.Entities.Database;
using Bio.Portal.Server.Services.APIRest.Models;
using Bio.Portal.Server.Services.Core.Services;
using BioPortal.Server.Api.Model;
using log4net;
using Newtonsoft.Json;
using NHibernate.Mapping.ByCode;
using Swashbuckle.Swagger.Annotations;

namespace Bio.Portal.Server.Services.APIRest
{
    /// <summary>
    /// Controller para manejo integracion con POS de Klap
    /// </summary>
    /// <remarks>
    /// Debe dar soporte a conversion de ISO, verificación con NEC y registro de transacciones en ambas
    /// cédulas, con posibilidad de enrolameinto si verificación es positiva.
    /// </remarks>
    public class POSController : ApiController
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(POSController));

        /// <summary>
        /// Crea transacción de inicio para MatchOnCard
        /// </summary>
        /// <remarks>
        /// Se utiliza para hacer la conversión de minucias de ISO a ISOCompact, registrando una 
        /// transaccion. El TrackId se devuelve y luego se envía en verify, para registrar el resultado 
        /// del MOC, haciendo update de la transaccion antes iniciada, para unificar ambos procesos.
        /// </remarks>
        /// <param name="param"></param>
        /// <response code="200">Tx generada con éxito</response>
        /// <response code="404">Parámetros erroneos</response>
        /// <response code="4xx">Errores a Definir</response>
        /// <response code="500">Error Desconocido Interno</response>
        /// <returns></returns>
        [Route("api/v1/pos/createTx")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "BPResponse", typeof(BPResponse))]
        [SwaggerResponse(HttpStatusCode.NotFound, "BPResponse", typeof(BPResponse))]
        public object CreateTxFromPOS([FromBody] POSTxCreateIn param)
        {
            /*
                Los pasos a realizar son:
                0) Check login Basic Auth 
                1) Leo parametros, parseo y chequeo consistencia de datos y check client en plataforma 
                2) Genero nuevo trackid
                3) Retorno
            */
            BPResponse response = new BPResponse();
            BpCompany _Company;
            string msg;
            try
            {
                LOG.Debug("POSController.CreateTxFromPOS IN...");

                //0) Check login Basic Auth
                if (!BPUnitOfWork.Authorized(Request, out _Company, out msg))
                {
                    LOG.Warn("POSController.TxCreate - Acceso NO Autorizado [" +
                                                    (string.IsNullOrEmpty(msg) ? "" : msg) + "]");
                    response.Code = Errors.IERR_UNAUTHORIZED;
                    response.Message = Errors.GetDescription(response.Code);
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, response);
                }
                else
                {
                    LOG.Debug("POSController.TxCreate - Authorized OK!");
                }

                //0,5 - Added 01-12-2021 - Fix para arreglar potencial bug de componente android para multicaja. 
                //                   Se necesita revisar que si llega companyid = 7 (Biometrika) => se cambie a 25 que es multicaja
                BpCompany _CompanyOut = null;
                if (!BPUnitOfWork.AuthorizedPOSFix(_Company, Properties.Settings.Default.POSControllerCompanyIdReplace, out _CompanyOut))
                {
                    response.Code = Errors.IERR_UNAUTHORIZED;
                    response.Message = Errors.GetDescription(response.Code);
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, response);
                }
                else
                {
                    _Company = _CompanyOut;
                    LOG.Debug("POSController.TxCreate - _Company != null => " + (_Company != null).ToString());
                    LOG.Debug("POSController.TxCreate - _Company.Id => " +
                                    (_Company != null ? _Company.Id.ToString() : "_Company null"));
                }

                //1) Leo parametros, parseo y chequeo consistencia de datos y check client en plataforma 
                if (param == null || string.IsNullOrEmpty(param.clientid))
                {
                    LOG.Warn("POSController.TxCreate - parametro nulo");
                    response.Code = Errors.IERR_BAD_PARAMETER;
                    response.Message = Errors.GetDescription(response.Code) + " - [Parametro Nulo]";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                else
                {
                    if (param.type != 1 && param.type != 2) param.type = 1; //Set default sin imagenes
                    LOG.Debug("POSController.TxCreate - Param OK! param.type = " + param.type.ToString());
                }

                LOG.Debug("POSController.TxCreate - Checking Client = " + param.clientid + " para Company.Id = " +
                            _Company.Id.ToString() + "...");
                int ret = BPUnitOfWork.ClientAuthorized(_Company.Id, param.clientid, out msg);
                if (ret != Errors.IERR_OK)
                {
                    LOG.Warn("POSController.TxCreate - Acceso No Permitido [" + msg + "]");
                    response.Code = Errors.IERR_CLIENT_NOT_AUTHORIZED;
                    response.Message = Errors.GetDescription(response.Code) + " - Acceso No Permitido [" + msg + "]";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                else
                {
                    LOG.Debug("POSController.TxCreate - Client Enabled OK! [" + param.clientid + "]");
                }

                //2) Genero nuevo trackid
                ret = 0;
                string _trackid = null;
                ret = BPUnitOfWork.GenerateTx(_Company, param, out _trackid, out msg);
                if (ret != Errors.IERR_OK)
                {
                    LOG.Warn("POSController.TxCreate - Error generando Transaccion [" + ret.ToString() + " - " + msg + "]");
                    response.Code = ret; // Errors.IERR_CREATING_TX;
                    response.Message = Errors.GetDescription(response.Code) + " - [" + msg + "]";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                else
                {
                    LOG.Debug("POSController.TxCreate - Tx generada ok => _trackid = " + _trackid);
                    response.Code = Errors.IERR_OK;
                    response.Message = null;
                    POSTxCreateOutData outData = new POSTxCreateOutData();
                    outData.trackid = _trackid;
                    response.Data = outData;
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
            }
            catch (Exception ex)
            {
                response.Code = Errors.IERR_UNKNOWN;
                response.Message = Errors.GetDescription(response.Code) + " Excp Error [" + ex.Message + "]";
                LOG.Error("POSController.CreateTxFromPOS - Excp Error:", ex);
            }
            LOG.Debug("POSController.CreateTxFromPOS OUT - Code=" + response.Code.ToString());
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        /// <summary>
        /// Verificación desde POS, para cédula antigua o nueva 
        /// </summary>
        /// <remarks>
        /// Se utiliza desde los POS para realizar y registra la verificación entre cédula antigua y huella, 
        /// o regitrar resultado (actualizar una tx generada en metodo createTx) de una transacción de cédula 
        /// nueva. 
        /// Se actualiza el ActionId y el resultado obtenido.
        /// Además, dependiendo de la configuración del sistema, se enrola en el CompanyId indicado, 
        /// el cual puede ser diferente del registrado en la transacción.
        /// </remarks>
        /// <param name="param">Json con parámetros</param>
        /// <response code="200">Proceso de verificación y registro ok</response>
        /// <response code="404">Parámetros erroneos</response>
        /// <response code="4xx">Errores a Definir</response>
        /// <response code="500">Error Desconocido Interno</response>
        /// <returns>URL donde conectarse para iniciar la verificación.</returns>
        [Route("api/v1/pos/verify")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "BPResponse", typeof(BPResponse))]
        [SwaggerResponse(HttpStatusCode.NotFound, "BPResponse", typeof(BPResponse))]
        public object VerifyFromPOS([FromBody] POSVerifyIn param)
        {
            /*
                Los pasos a realizar son:
                0) Check login Basic Auth 
                1) Leo parametros, parseo y chequeo consistencia de datos y check client en plataforma 
                2) Segun sea verificacion de CN o CA
                    2.1) Si es Cedula Nueva
                        2.1.1) Si es negativa la verificacion MoC => Update en TX
                        2.1.2) Si es positiva la verificacion
                            2.1.2.1) Update Tx con los resultados
                            2.1.2.2) Si está habilitado el enroll, enrolar o modificar de acuerdo a los datos de config
                    2.2) Si es cedula vieja
                        2.2.1) Realizo verificacion entre PDF417 y sample
                        2.2.2) Si es negativa la verificacion NEC => Update en TX
                        2.2.2) Si es positiva la verificacion NEC
                            2.2.2.1) Update Tx con los resultados
                            2.2.2.2) Si está habilitado el enroll, enrolar o modificar de acuerdo a los datos de config
                            2.2.2.3) Armo salida en Data con los datos pareseados desde PDF417
                3) Retorno
            */
            BPResponse response = new BPResponse();
            BpCompany _Company;
            string msg;
            int ret = 0;
            try
            {
                LOG.Debug("POSController.VerifyFromPOS IN...");

                //0) Check login Basic Auth
                if (!BPUnitOfWork.Authorized(Request, out _Company, out msg))
                {
                    LOG.Warn("POSController.VerifyFromPOS - Acceso NO Autorizado [" +
                                                    (string.IsNullOrEmpty(msg) ? "" : msg) + "]");
                    response.Code = Errors.IERR_UNAUTHORIZED;
                    response.Message = Errors.GetDescription(response.Code);
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, response);
                }
                else
                {
                    LOG.Debug("POSController.VerifyFromPOS - Authorized OK!");
                }

                //0,5 - Added 01-12-2021 - Fix para arreglar potencial bug de componente android para multicaja. 
                //                   Se necesita revisar que si llega companyid = 7 (Biometrika) => se cambie a 25 que es multicaja
                if (!BPUnitOfWork.AuthorizedPOSFix(_Company, Properties.Settings.Default.POSControllerCompanyIdReplace, out _Company))
                {
                    response.Code = Errors.IERR_UNAUTHORIZED;
                    response.Message = Errors.GetDescription(response.Code);
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, response);
                }

                //1) Leo parametros, parseo y chequeo consistencia de datos y check client en plataforma 
                if (param == null || string.IsNullOrEmpty(param.clientid))
                {
                    LOG.Warn("POSController.VerifyFromPOS - parametro nulo o faltantes");
                    response.Code = Errors.IERR_BAD_PARAMETER;
                    response.Message = Errors.GetDescription(response.Code) + " - [Parametros Nulos o Faltantes]";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                else
                {
                    LOG.Debug("POSController.VerifyFromPOS - Param OK!");
                }

                LOG.Debug("POSController.VerifyFromPOS - Checking Client = " + param.clientid + " para Company.Id = " +
                            _Company.Id.ToString() + "...");
                ret = BPUnitOfWork.ClientAuthorized(_Company.Id, param.clientid, out msg);
                if (ret != Errors.IERR_OK)
                {
                    LOG.Warn("POSController.VerifyFromPOS - Acceso No Permitido [" + msg + "]");
                    response.Code = Errors.IERR_CLIENT_NOT_AUTHORIZED;
                    response.Message = Errors.GetDescription(response.Code) + " - Acceso No Permitido [" + msg + "]";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                else
                {
                    LOG.Debug("POSController.VerifyFromPOS - Client Enabled OK! [" + param.clientid + "]");
                }

                // 2) Segun sea verificacion de CN o CA
                response = BPUnitOfWork.Verify(param, _Company);
                if (response != null && response.Code == Errors.IERR_OK)
                {
                    LOG.Debug("POSController.VerifyFromPOS OUT - Code=" + response.Code.ToString());
                }
                else
                {
                    if (response != null)
                    {
                        LOG.Debug("POSController.VerifyFromPOS OUT - Code=" + response.Code.ToString() + " - " +
                                    (string.IsNullOrEmpty(response.Message) ? "Msg Null" : response.Message));
                    }
                    else
                    {
                        LOG.Debug("POSController.VerifyFromPOS OUT - response == null!");
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, response);
                }
            }
            catch (Exception ex)
            {
                response.Code = Errors.IERR_UNKNOWN;
                response.Message = Errors.GetDescription(response.Code);
                LOG.Error("POSController.VerifyFromPOS - Excp Error:", ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
            #region Descarte Verify
            /*    
                            if (param.minutiaetype != 0) //Retorno OK
                            {
                                response.Code = Errors.IERR_OK;
                                response.Message = Errors.GetDescription(response.Code);
                                if (string.IsNullOrEmpty(param.barcode)) //Es verify de cedula nueva => Retonra solo ok
                                {
                                    //No hace nada porque Data va null
                                } else //Es cedula vieja => devuelve ok de ok de verify cedula antigua
                                {
                                    POSVerifyOutData outData = new POSVerifyOutData();
                                    if (param.threshold == 0)
                                    { //Devuelvo positivo
                                        outData.result = 1;
                                        outData.score = 8520;
                                        outData.perdonaldata = new PersonalData();
                                        outData.perdonaldata.valueid = "11111111-1";
                                        outData.perdonaldata.phaterlastname = "ApellidoPaterno";
                                        outData.perdonaldata.birthdate = "10/10/1969";
                                        outData.perdonaldata.expirationdate = "11/12/2026";
                                        outData.perdonaldata.sex = "M";
                                        outData.perdonaldata.nationality = "ARG";
                                    } else //Devuelvo negativo
                                    {
                                        outData.result = 2;
                                        outData.score = 0;
                                        outData.perdonaldata = null;
                                    }
                                    response.Data = JsonConvert.SerializeObject(outData);
                                }
                                LOG.Debug("POSController.VerifyFromPOS Retorna OK!");
                            }
                            else //Retorno Error
                            {
                                response.Code = Errors.IERR_UNKNOWN;
                                response.Message = Errors.GetDescription(response.Code);
                                LOG.Debug("POSController.VerifyFromPOS Retorna NOOK!");
                            }
                        }
                        catch (Exception ex)
                        {
                            response.Code = Errors.IERR_UNKNOWN;
                            response.Message = Errors.GetDescription(response.Code);
                            LOG.Error("POSController.VerifyFromPOS - Excp Error:", ex);
                        }
                        LOG.Debug("POSController.VerifyFromPOS OUT - Code=" + response.Code.ToString());
                        return response;
            */
            #endregion Descarte Verify
        }


        /// <summary>
        /// Wrapper API Rest de WS PlugIn
        /// </summary>
        /// <param name="xmlParamIn"></param>
        /// <param name="customTimeOut"></param>
        /// <returns></returns>
        [Route("api/PP/Process")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "XmlParamOut", typeof(BioPortal.Server.Api.Json.XmlParamOut))]
        public BioPortal.Server.Api.Json.XmlParamOut Process([FromBody] BioPortal.Server.Api.Json.XmlParamIn xmlParamIn,
            int customTimeOut = 0)
        {
            try
            {
                LOG.Debug("POSController.Process IN...");
                //0,5 - Added 01-12-2021 - Fix para arreglar potencial bug de componente android para multicaja. 
                //                   Se necesita revisar que si llega companyid = 7 (Biometrika) => se cambie a 25 que es multicaja
                if (xmlParamIn.Companyid != Properties.Settings.Default.POSControllerCompanyIdReplace)
                {
                    LOG.Debug("POSController.Process Cambiando _Company.Id = " + xmlParamIn.Companyid.ToString() + " por Id = " +
                                    Properties.Settings.Default.POSControllerCompanyIdReplace.ToString() + "...");
                    xmlParamIn.Companyid = Properties.Settings.Default.POSControllerCompanyIdReplace;
                }

                string sParamIn = XmlUtils.SerializeObject(xmlParamIn);  //XmlHelper.SerializeXmlParamIn(xmlParamIn);

                if (string.IsNullOrEmpty(sParamIn))
                    return new BioPortal.Server.Api.Json.XmlParamOut
                    {
                        ExecutionResult = Bio.Core.Api.Constant.Errors.IERR_SERIALIZE_XMLPARAMIN,
                        Message = Errors.GetDescription(Bio.Core.Api.Constant.Errors.IERR_SERIALIZE_XMLPARAMIN)
                    };

                string sParamOut;
                BioPortal_Server_Plugin_Process BPPPHelper = new BioPortal_Server_Plugin_Process();
                int ret = BPPPHelper.Process(sParamIn, out sParamOut);
                LOG.Debug("POSController.Process Codigo respuesta BioPortal: " + ret +
                            " - Mensaje: " + Bio.Core.Api.Constant.Errors.GetDescription(ret));

                //Agregado solo para no tocar Bio.Portal.Api.Json
                if (!string.IsNullOrEmpty(sParamOut))
                {
                    sParamOut = sParamOut.Replace("<key>", "<Key>").Replace("</key>", "</Key>");
                    sParamOut = sParamOut.Replace("<value>", "<Value>").Replace("</value>", "</Value>");
                }

                BioPortal.Server.Api.Json.XmlParamOut response =
                    XmlUtils.DeserializeObject<BioPortal.Server.Api.Json.XmlParamOut>(sParamOut);
                if (ret != 0)
                {
                    response.ExecutionResult = ret;
                    response.Message += ", " + Bio.Core.Api.Constant.Errors.GetDescription(ret);
                }
                LOG.Debug("POSController.Process OUT!");
                return response;
            }
            catch (Exception ex)
            {
                LOG.Error("POSController.Process Excp Error:", ex);
                return new BioPortal.Server.Api.Json.XmlParamOut
                {
                    ExecutionResult = Bio.Core.Api.Constant.Errors.IERR_UNKNOWN,
                    Message = Bio.Core.Api.Constant.Errors.GetDescription(Bio.Core.Api.Constant.Errors.IERR_UNKNOWN)
                                + " [" + ex.Message + "]"
                };
            }

        }


        private bool isInvalidParam(POSVerifyIn param)
        {
            return false;
        }


        #region Facial POS

        [Route("api/v1/pos/facial/verify")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "BPResponse", typeof(BPResponse))]
        [SwaggerResponse(HttpStatusCode.NotFound, "BPResponse", typeof(BPResponse))]
        public object VerifyFacialFromPOS([FromBody] POSVerifyFacialIn param)
        {
            /*
                Los pasos a realizar son:
                0) Check login Basic Auth 
                1) Leo parametros, parseo y chequeo consistencia de datos y check client en plataforma 
                2) Intento chequeo con imágenes enviadas. Si viene RUT, chequeo si esta enrolado => check contra BD si param esta enabled
                    2.1) Si es negativo
                        2.1.1) Intento mejorar la imagen de la cédula (Quiza ocn regula? ver) Ver tema de girar la imagen
                    2.2) Si es positivo 
                        2.2.1) Si está habilitado el tema del enroll, se procesa con regula y se enrola la selfie
                3) Retorno
            */
            BPResponse response = new BPResponse();
            BpCompany _Company;
            string msg;
            int ret = 0;
            try
            {
                LOG.Debug("POSController.VerifyFacialFromPOS IN...");

                //0) Check login Basic Auth
                if (!BPUnitOfWork.Authorized(Request, out _Company, out msg))
                {
                    LOG.Warn("POSController.VerifyFacialFromPOS - Acceso NO Autorizado [" +
                                                    (string.IsNullOrEmpty(msg) ? "" : msg) + "]");
                    response.Code = (int)HttpStatusCode.Unauthorized;
                    response.Message = Errors.GetDescription(response.Code);
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, response);
                }
                else
                {
                    LOG.Debug("POSController.VerifyFacialFromPOS - Authorized OK!");
                }

                //0,5 - Added 01-12-2021 - Fix para arreglar potencial bug de componente android para multicaja. 
                //                   Se necesita revisar que si llega companyid = 7 (Biometrika) => se cambie a 25 que es multicaja
                if (!BPUnitOfWork.AuthorizedPOSFix(_Company, Properties.Settings.Default.POSControllerCompanyIdReplace, out _Company))
                {
                    response.Code = (int)HttpStatusCode.Unauthorized;
                    response.Message = "Acceso NO Autorizado";
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, response);
                }

                //1) Leo parametros, parseo y chequeo consistencia de datos y check client en plataforma 
                if (param == null || string.IsNullOrEmpty(param.clientid))
                {
                    LOG.Warn("POSController.VerifyFacialFromPOS - parametro nulo o faltantes");
                    response.Code = (int)HttpStatusCode.BadRequest;
                    response.Message = "Clientid no puede ser nulo o parametros nulos o faltantes";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                else
                {
                    LOG.Debug("POSController.VerifyFacialFromPOS - Param OK!");
                }

                LOG.Debug("POSController.VerifyFacialFromPOS - Checking Client = " + param.clientid + " para Company.Id = " +
                            _Company.Id.ToString() + "...");
                ret = BPUnitOfWork.ClientAuthorized(_Company.Id, param.clientid, out msg);
                if (ret != Errors.IERR_OK)
                {
                    LOG.Warn("POSController.VerifyFacialFromPOS - Acceso No Permitido [" + msg + "]");
                    response.Code = (int)HttpStatusCode.Unauthorized;
                    response.Message = "Acceso No Permitido [" + msg + "]";
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, response);
                }
                else
                {
                    LOG.Debug("POSController.VerifyFacialFromPOS - Client Enabled OK! [" + param.clientid + "]");
                }

                // 2) Segun sea verificacion de CN o CA
                response = BPUnitOfWork.VerifyFacial(param, _Company);
                if (response != null && response.Code == Errors.IERR_OK)
                {
                    LOG.Debug("POSController.VerifyFacialFromPOS OUT - Code=" + response.Code.ToString());
                }
                else
                {
                    if (response != null)
                    {
                        LOG.Debug("POSController.VerifyFacialFromPOS OUT - Code=" + response.Code.ToString() + " - " +
                                    (string.IsNullOrEmpty(response.Message)?"Msg Null":response.Message));
                    } else
                    {
                        LOG.Debug("POSController.VerifyFacialFromPOS OUT - response == null!");
                    }
                    return Request.CreateResponse((HttpStatusCode)response.Code, response);
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = "Error inesperado Excp Error: " + ex.Message;
                LOG.Error("POSController.VerifyFacialFromPOS - Excp Error:", ex);
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
            
        }


        #endregion Facial POS

        #region Utils

        /// <summary>
        /// Toma IP desde request
        /// </summary>
        /// <returns></returns>
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            System.Web.HttpBrowserCapabilities cap = context.Request.Browser;
            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        /// <summary>
        /// API Ping para revisar que servicio esta vivo.
        /// </summary>
        /// <param name="id">Entero de prueba</param>
        /// <returns></returns>
        [Route("api/Ping")]
        [HttpGet]
        public string Ping(int id)
        {
            return "BPWebController Live! = " + id.ToString();
        }

        /// <summary>
        /// Crea respuesta para retornar HTML cuando se pide algo a la API 
        /// desde el mail  (Ej: validar mail)
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private HttpResponseMessage CreateResponseHtml(int code, string msg)
        {
            HttpResponseMessage response;
            try
            {
                LOG.Error("BPWebController.CreateResponseHtml IN...");
                string htmlpage = "<table border=\"0\" width=\"60%\" align=\"center\">" +
                                   "<tr><td align=\"center\"><a href=\"http://www.notariovirtual.cl\" target=\"_blank\">" +
                                   "<img src=\"" + Properties.Settings.Default.SITE_ROOT_URL + "/images/biometrika.png\" /></a></td></tr></table>" +
                            "<table border=\"0\" width=\"60%\" align=\"center\">" +
                              "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                               "<tr><td><p style=\"font-family:'Arial'\"><b>" +
                               ((code == 0) ? "<font color=\"green\">Funcionamiento Correcto!</font>" :
                                           "<font color =\"red\">ERROR</font>")
                                 + "</b></p></td></tr>" +
                                "<tr><td><p style=\"font-family:'Arial'\">" + msg + "</td></tr>" +
                                "<tr><td>&nbsp;</td></tr>" +
                                "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
                                "<a href=\"http://www.biometrikalatam.com\" target=\"_blank\">Biometrika Latam<a>.</p></td></tr>" +
                                "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                            "</table>";
                response = new HttpResponseMessage();
                response.Content = new StringContent(htmlpage);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            }
            catch (Exception ex)
            {
                response = new HttpResponseMessage();
                response.Content = new StringContent("Error generando página de resultado.");
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                LOG.Error("BPWebController.CreateResponseHtml Error: " + ex.Message);
            }
            LOG.Error("BPWebController.CreateResponseHtml OUT!");
            return response;
        }

        #endregion Utils

    }


}