﻿using System;
using log4net;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
using Bio.Core.Constant;
using Bio.Portal.Server.Common.Entities;
using Bio.Portal.Server.Services.APIRest.Models;
using Bio.Portal.Server.Services.Core.Services;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using Bio.Portal.Server.Services.APIRest.Models.WA;
using Bio.Portal.Server.Common.Entities.Database;

namespace Bio.Portal.Server.Services.APIRest.Controllers
{
    public class WAController : ApiController
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(WAController));

        [Route("waapi/v1/Enroll")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "EnrollModelR", typeof(EnrollModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(Error))]
        public object Enroll([FromBody] EnrollModel param)
        {
            EnrollModelR oRet = new EnrollModelR();
            string msg = "";
            BpCompany _Company; 
            try
            {
                LOG.Debug("WAController.Enroll - IN...");

                //0) Check Basic Authentication
                if (!BPUnitOfWork.Authorized(Request, out _Company, out msg))
                {
                    LOG.Warn("WAController.Enroll - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new Error(Errors.IERR_CONX_WS, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msg) ? "" : msg) + "]", null)));
                }


                //1) Leo parametros, parseo y chequeo consistencia de datos(login / token /?)
                if (param == null || param.samples == null || param.samples.Count == 0)
                {
                    LOG.Warn("WAController.Enroll - parametro nulo o lista de samples vacia");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new Error(Errors.IERR_BAD_PARAMETER, "Parametro de entrada nulo o lista de samples vacia. Reintente...", null)));
                }

                //TODO - Check Company Access
                //msg = BPUnitOfWork.CheckStatusCompanyAndUser(param.company, msg);
                //if (param.company <= 0 || msg.Length > 0)
                //{
                //    LOG.Warn("BPWebController.TxCreate - Acceso No Permitido [" + msg + "]");
                //    return Request.CreateResponse(HttpStatusCode.BadRequest,
                //        (new Error(Errors.IERR_BAD_PARAMETER, "Acceso No Permitido [" + msg + "]", null)));
                //}

                switch (param.type)
                {
                    case 0: //Graba sample en disco
                        oRet.iduser = WAControllerUnitOfWork.SaveDataInHDD(param);
                        break;
                    default:
                        break;
                }

                if (oRet.iduser > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, oRet);
                } else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        (new Error(Errors.IERR_UNKNOWN, "WAController.Enroll SaveDataInHDD [" + oRet.iduser + "]", null)));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("WAController.Enroll - Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        (new Error(Errors.IERR_UNKNOWN, "WAController.Enroll Error [" + ex.Message + "]", null)));
            }
        }

        [Route("waapi/v1/GenerateMark")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "EnrollModelR", typeof(MarkModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(Error))]
        public object GenerateMark([FromBody] MarkModel param)
        {
            MarkModelR oRet = new MarkModelR();

            return oRet;
        }

        [Route("waapi/v1/GetBIRs")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "BIRModelR", typeof(BIRModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(Error))]
        public object GetBIRs([FromBody] BIRModel param)
        {
            BIRModelR oRet = new BIRModelR();

            return oRet;
        }

    }

    /*

     Gustavo

 esta es la estructura de la api como la necesitamos usar para registrar acceso


 POST /api/v1.2/access_control/validate_user_access
 {
 "portico_id": "",
 "key": "",
 "rfid": "",
     "validate_rfid": false,
 "with_antispoof": false,
 "device_ip" : "127.0.0.1",
 "timestamp": 1523471581,
 "images": "/9j/4A...jpeg en base64"
     "dev_uuid": "serial de android"
 }

 RESPUESTA

 SI SE VALIDA CORRECTAMENTE
 {
     "error": null,
     "valid_access": true,
     "event_id": null,
     "access_data": {
         "id": "11111111-1",
         "name": "gonzalo",
         "last_name": "mansilla"
     }
     "actuator": null
 }

 SI NO ES RECONOCIDO
 {
     "error": null,
     "valid_access": false,
     "event_id": null,
     "access_data": null,
     "actuator": null
 }

 SI HAY ERROR LA API DEBE GENERAR UN CODIGO HTTP 4XX O 5XX

 los tipos de datos los puedes ver en estas clases (estan en java)

 public class AccessRequest {
     @SerializedName("portico_id")
     private String porticoId;

     @SerializedName("key")
     private String key;

     @SerializedName("rfid")
     private String rfid;

     @SerializedName("validate_rfid")
     private Boolean validateRfid;

     @SerializedName("with_antispoof")
     private Boolean withAntispoof;

     @SerializedName("device_ip")
     private String deviceIp;

     @SerializedName("images")
     private String image;

     @SerializedName("timestamp")
     private Long timestamp;

     @SerializedName("dev_uuid")
     private String uuid;
 }

 public class AccessResponse {
 @SerializedName("error")
 private Error error;

 @SerializedName("valid_access")
 private boolean validAccess;

 @SerializedName("event_id")
 private String eventId;

 @SerializedName("access_data")
 private AccessData accessData;

 @SerializedName("actuator")
 private Actuator actuator;
 }

 public class AccessData {
 @SerializedName("id")
 private String id;

 @SerializedName("name")
 private String name;

 @SerializedName("last_name")
 private String lastName;
 }

 public class Actuator {
     private String endpoint;
     private Integer gpio;
     private Integer time;
 }



     */
}
