using System;
using System.Collections.Generic;
using System.Text;

namespace Innovatrics.AnsiIso
{
    public class IEngineException : Exception
    {
        internal IEngineException(int number)
            : base(FormatMessage(number))
        {
            Number = number;
            if (Enum.IsDefined(typeof(IEngineError), number))
                Code = (IEngineError)number;
        }

        internal static void check(int number)
        {
            if (number != 0)
                throw new IEngineException(number);
        }

        public readonly int Number;
        public readonly IEngineError Code;

        static string FormatMessage(int number)
        {
            return String.Format("{0} (error code {1})", IEngine.GetErrorMessage(number), number);
        }
    }
}
