using System;
using System.Collections.Generic;
using System.Text;

namespace Innovatrics.AnsiIso
{
    public enum IEngineSortOrder
    {
        SORT_NONE = 0,
        SORT_X_ASC = 1,
        SORT_X_DESC = 2,
        SORT_Y_ASC = 3,
        SORT_Y_DESC = 4
    }
}
