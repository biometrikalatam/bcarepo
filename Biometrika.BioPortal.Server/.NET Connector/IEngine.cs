using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Innovatrics.AnsiIso
{
    public class IEngine
    {
        public static void Init() { IEngineException.check(Dll.IEngine_Init()); }
        public static void Terminate() { IEngineException.check(Dll.IEngine_Terminate()); }
        public static void GetVersion(out Version version) { IEngineException.check(Dll.IEngine_GetVersion(out version)); }
        public static string GetErrorMessage(int errcode) {
#if PocketPC
            return PtrToStringAnsi(Dll.IEngine_GetErrorMessage(errcode));
#else
            return Marshal.PtrToStringAnsi(Dll.IEngine_GetErrorMessage(errcode));
#endif
        }
        public static void SetLicenseContent(byte[] licenseContent, int length) { IEngineException.check(Dll.IEngine_SetLicenseContent(licenseContent, length)); }
        public static void GetImageQuality(int width, int height, byte[] rawImage, out int quality) { IEngineException.check(Dll.IEngine_GetImageQuality(width, height, rawImage, out quality)); }
        public static void LoadBMP(string filename, out int width, out int height, byte[] rawImage, ref int length) {
            byte[] asciiString = null;
            if (filename != null)
                asciiString = Encoding.Default.GetBytes(filename + "\0");
            IEngineException.check(Dll.IEngine_LoadBMP(asciiString, out width, out height, rawImage, ref length));
        }
        public static void ConvertBMP(byte[] bmpImage, out int width, out int height, byte[] rawImage, ref int length) { IEngineException.check(Dll.IEngine_ConvertBMP(bmpImage, out width, out height, rawImage, ref length)); }
        public static void ConvertTemplate(TemplateFormat inputTemplateType, byte[] inputTemplate, TemplateFormat outputTemplateType, ref int length, byte[] outputTemplate) { IEngineException.check(Dll.IEngine_ConvertTemplate(inputTemplateType, inputTemplate, outputTemplateType, ref length, outputTemplate)); }

#if PocketPC
        static string PtrToStringAnsi(IntPtr native)
        {
            List<byte> bytes = new List<byte>();
            for (int at = 0; true; ++at)
            {
                byte character = Marshal.ReadByte(native, at);
                if (character == 0)
                    break;
                bytes.Add(character);
            }
            byte[] array = bytes.ToArray();
            return Encoding.ASCII.GetString(array, 0, array.Length);
        }
#endif

        public const int MIN_IMAGE_WIDTH = 90;
        public const int MAX_IMAGE_WIDTH = 1800;
        public const int MIN_IMAGE_HEIGHT = 90;
        public const int MAX_IMAGE_HEIGHT = 1800;
        public const int MAX_ANSI_TEMPLATE_SIZE = 1568;
        public const int MAX_ISO_TEMPLATE_SIZE = 1566;
    }
}
