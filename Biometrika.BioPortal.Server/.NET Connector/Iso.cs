using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Innovatrics.AnsiIso
{
    public class Iso
    {
        public static void ConvertToANSI(byte[] isoTemplate, ref int length, byte[] ansiTemplate) { IEngineException.check(Dll.ISO_ConvertToANSI(isoTemplate, ref length, ansiTemplate)); }
        public static void CreateTemplate(int width, int height, byte[] rawImage, byte[] isoTemplate) { IEngineException.check(Dll.ISO_CreateTemplate(width, height, rawImage, isoTemplate)); }
        public static void CreateTemplateEx(int width, int height, byte[] rawImage, byte[] isoTemplate, string skeletonImageFile, string binarizedImageFile, string minutiaeImageFile) {
            byte[] asciiSkeleton = null;
            if (skeletonImageFile != null)
                asciiSkeleton = Encoding.Default.GetBytes(skeletonImageFile + "\0");
            byte[] asciiBinarized = null;
            if (binarizedImageFile != null)
                asciiBinarized = Encoding.Default.GetBytes(binarizedImageFile + "\0");
            byte[] asciiMinutiae = null;
            if (minutiaeImageFile != null)
                asciiMinutiae = Encoding.Default.GetBytes(minutiaeImageFile + "\0");
            IEngineException.check(Dll.ISO_CreateTemplateEx(width, height, rawImage, isoTemplate, asciiSkeleton, asciiBinarized, asciiMinutiae));
        }
        public static void VerifyMatch(byte[] probeTemplate, byte[] galleryTemplate, int maxRotation, out int score) { IEngineException.check(Dll.ISO_VerifyMatch(probeTemplate, galleryTemplate, maxRotation, out score)); }
        public static void VerifyMatchEx(byte[] probeTemplate, int probeView, byte[] galleryTemplate, int galleryView, int maxRotation, out int score) { IEngineException.check(Dll.ISO_VerifyMatchEx(probeTemplate, probeView, galleryTemplate, galleryView, maxRotation, out score)); }
        public static void GetTemplateParameter(byte[] isoTemplate, TemplateParameter parameter, out int value) { IEngineException.check(Dll.ISO_GetTemplateParameter(isoTemplate, parameter, out value)); }
        public static void SetTemplateParameter(byte[] isoTemplate, TemplateParameter parameter, int value) { IEngineException.check(Dll.ISO_SetTemplateParameter(isoTemplate, parameter, value)); }
        public static void GetFingerView(byte[] isoTemplate, int fingerView, byte[] outTemplate) { IEngineException.check(Dll.ISO_GetFingerView(isoTemplate, fingerView, outTemplate)); }
        public static void DrawMinutiae(byte[] isoTemplate, int width, int height, byte[] inputImage, byte[] outputBmpImage, ref int outputImageLength) { IEngineException.check(Dll.ISO_DrawMinutiae(isoTemplate, width, height, inputImage, outputBmpImage, ref outputImageLength)); }
        public static void GetMinutiae(byte[] isoTemplate, Minutiae[] minutiae, out int minutiaeCount) { IEngineException.check(Dll.ISO_GetMinutiae(isoTemplate, minutiae, out minutiaeCount)); }
        public static void MergeTemplates(byte[] referenceTemplate, byte[] addedTemplate, ref int length, byte[] outTemplate) { IEngineException.check(Dll.ISO_MergeTemplates(referenceTemplate, addedTemplate, ref length, outTemplate)); }
        public static void LoadTemplate(string filename, byte[] isoTemplate) {
            byte[] asciiString = null;
            if (filename != null)
                asciiString = Encoding.Default.GetBytes(filename + "\0");
            IEngineException.check(Dll.ISO_LoadTemplate(asciiString, isoTemplate));
        }
        public static void SaveTemplate(string filename, byte[] isoTemplate) {
            byte[] asciiString = null;
            if (filename != null)
                asciiString = Encoding.Default.GetBytes(filename + "\0");
            IEngineException.check(Dll.ISO_SaveTemplate(asciiString, isoTemplate));
        }

        public static void ConvertToISOCardCC(byte[] isoTemplate, int maximumMinutiaeCount, IEngineSortOrder minutiaeOrder, IEngineSortOrder minutiaeSecondaryOrder, ref int length, byte[] isoCCTemplate)
        {
            IEngineException.check(Dll.ISO_ConvertToISOCardCC(isoTemplate, maximumMinutiaeCount, minutiaeOrder, minutiaeSecondaryOrder, ref length, isoCCTemplate));
        }

        public static void CardCCConvertToISO(byte[] isoCCTemplate, ref int length, byte[] isoTemplate)
        {
            IEngineException.check(Dll.ISO_CARD_CC_ConvertToISO(isoCCTemplate, ref length, isoTemplate));
        }
        public static byte[] CardCCGetMinutiaeData(byte[] isoCCTemplate, out int minutiaeCount)
        {
            byte[] minutiaeTempData = new byte[3 * 256];//maximum possible size of minutiae data block
            int minutiaeDataSize = 0;
            IEngineException.check(Dll.ISO_CARD_CC_GetMinutiaeData(isoCCTemplate, out minutiaeCount, minutiaeTempData, out minutiaeDataSize));
            byte[] minutiaeData = new byte[minutiaeDataSize];
            int i;
            for (i = 0; i < minutiaeDataSize; i++)
                minutiaeData[i] = minutiaeTempData[i];
            return minutiaeData;
        }
        public static byte[] RemoveMinutiae(byte[] isoTemplate, int maxMinutiaeCount)
        {
            int l=IEngine.MAX_ISO_TEMPLATE_SIZE;
            byte[] outTemplate = new byte[l];
            Dll.ISO_RemoveMinutiae(isoTemplate, maxMinutiaeCount,ref l,outTemplate);
            byte[] outData = new byte[l];
            int i;
            for (i = 0; i < l; i++) outData[i] = outTemplate[i];
            return outData;
        }
    }
}
