using System;
using System.Collections.Generic;
using System.Text;

namespace Innovatrics.AnsiIso
{
    public enum ImpressionType
    {
        LIVE_SCAN_PLAIN = 0,
        LIVE_SCAN_ROLLED = 1,
        NONLIVE_SCAN_PLAIN = 2,
        NONLIVE_SCAN_ROLLED = 3,
        SWIPE = 4,
        LIVE_SCAN_CONTACTLESS = 9
    }
}
