using System;
using System.Collections.Generic;
using System.Text;

namespace Innovatrics.AnsiIso
{
    public struct Minutiae
    {
	    public byte angle;
        public ushort x;
        public ushort y;
        public byte type;
    }
}
