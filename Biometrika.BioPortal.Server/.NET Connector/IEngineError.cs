using System;
using System.Collections.Generic;
using System.Text;

namespace Innovatrics.AnsiIso
{
    public enum IEngineError
    {
        NOERROR = 0,
        BADPARAM = 1101,
        BLANKIMAGE = 1114,
        BADIMAGE = 1115,
        INIT = 1116,
        FILE = 1117,
        MEMORY = 1120,
        NULLPARAM = 1121,
        OTHER = 1122,
        BADLICENSE = 1129,
        BADFORMAT = 1132,
        BADVALUE = 1133,
        BADTEMPLATE = 1135,
        READONLY = 1136,
        NOTDEFINED = 1137,
        NULLTEMPLATE = 1138
    }
}
