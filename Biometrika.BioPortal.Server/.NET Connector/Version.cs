using System;
using System.Collections.Generic;
using System.Text;

namespace Innovatrics.AnsiIso
{
    public struct Version
    {
        public uint Major;
        public uint Minor;
    }
}
