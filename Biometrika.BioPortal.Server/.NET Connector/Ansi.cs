using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Innovatrics.AnsiIso
{
    public class Ansi
    {
        public static void ConvertToISO(byte[] ansiTemplate, ref int length, byte[] isoTemplate) { IEngineException.check(Dll.ANSI_ConvertToISO(ansiTemplate, ref length, isoTemplate)); }
        public static void CreateTemplate(int width, int height, byte[] rawImage, byte[] ansiTemplate) { IEngineException.check(Dll.ANSI_CreateTemplate(width, height, rawImage, ansiTemplate)); }
        public static void CreateTemplateEx(int width, int height, byte[] rawImage, byte[] ansiTemplate, string skeletonImageFile, string binarizedImageFile, string minutiaeImageFile) {
            byte[] asciiSkeleton = null;
            if (skeletonImageFile != null)
                asciiSkeleton = Encoding.Default.GetBytes(skeletonImageFile + "\0");
            byte[] asciiBinarized = null;
            if (binarizedImageFile != null)
                asciiBinarized = Encoding.Default.GetBytes(binarizedImageFile + "\0");
            byte[] asciiMinutiae = null;
            if (minutiaeImageFile != null)
                asciiMinutiae = Encoding.Default.GetBytes(minutiaeImageFile + "\0");
            IEngineException.check(Dll.ANSI_CreateTemplateEx(width, height, rawImage, ansiTemplate, asciiSkeleton, asciiBinarized, asciiMinutiae));
        }
        public static void VerifyMatch(byte[] probeTemplate, byte[] galleryTemplate, int maxRotation, out int score) { IEngineException.check(Dll.ANSI_VerifyMatch(probeTemplate, galleryTemplate, maxRotation, out score)); }
        public static void VerifyMatchEx(byte[] probeTemplate, int probeView, byte[] galleryTemplate, int galleryView, int maxRotation, out int score) { IEngineException.check(Dll.ANSI_VerifyMatchEx(probeTemplate, probeView, galleryTemplate, galleryView, maxRotation, out score)); }
        public static void GetTemplateParameter(byte[] ansiTemplate, TemplateParameter parameter, out int value) { IEngineException.check(Dll.ANSI_GetTemplateParameter(ansiTemplate, parameter, out value)); }
        public static void SetTemplateParameter(byte[] ansiTemplate, TemplateParameter parameter, int value) { IEngineException.check(Dll.ANSI_SetTemplateParameter(ansiTemplate, parameter, value)); }
        public static void GetFingerView(byte[] ansiTemplate, int fingerView, byte[] outTemplate) { IEngineException.check(Dll.ANSI_GetFingerView(ansiTemplate, fingerView, outTemplate)); }
        public static void DrawMinutiae(byte[] ansiTemplate, int width, int height, byte[] inputImage, byte[] outputBmpImage, ref int outputImageLength) { IEngineException.check(Dll.ANSI_DrawMinutiae(ansiTemplate, width, height, inputImage, outputBmpImage, ref outputImageLength)); }
        public static void GetMinutiae(byte[] ansiTemplate, Minutiae[] minutiae, out int minutiaeCount) { IEngineException.check(Dll.ANSI_GetMinutiae(ansiTemplate, minutiae, out minutiaeCount)); }
        public static void MergeTemplates(byte[] referenceTemplate, byte[] addedTemplate, ref int length, byte[] outTemplate) { IEngineException.check(Dll.ANSI_MergeTemplates(referenceTemplate, addedTemplate, ref length, outTemplate)); }
        public static void LoadTemplate(string filename, byte[] ansiTemplate) {
            byte[] asciiString = null;
            if (filename != null)
                asciiString = Encoding.Default.GetBytes(filename + "\0");
            IEngineException.check(Dll.ANSI_LoadTemplate(asciiString, ansiTemplate));
        }
        public static void SaveTemplate(string filename, byte[] ansiTemplate) {
            byte[] asciiString = null;
            if (filename != null)
                asciiString = Encoding.Default.GetBytes(filename + "\0");
            IEngineException.check(Dll.ANSI_SaveTemplate(asciiString, ansiTemplate));
        }
        public static byte[] RemoveMinutiae(byte[] ansiTemplate, int maxMinutiaeCount)
        {
            int l = IEngine.MAX_ANSI_TEMPLATE_SIZE;
            byte[] outTemplate = new byte[l];
            Dll.ANSI_RemoveMinutiae(ansiTemplate, maxMinutiaeCount, ref l, outTemplate);
            byte[] outData = new byte[l];
            int i;
            for (i = 0; i < l; i++) outData[i] = outTemplate[i];
            return outData;
        }
    }
}
