using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Innovatrics.AnsiIso
{
    class Dll
    {
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int IEngine_Init();
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int IEngine_Terminate();
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int IEngine_GetVersion(out Version version);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern IntPtr IEngine_GetErrorMessage(int errcode);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int IEngine_SetLicenseContent(byte[] licenseContent, int length);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_ConvertToISO(byte[] ansiTemplate, ref int length, [Out] byte[] isoTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_ConvertToANSI(byte[] isoTemplate, ref int length, [Out] byte[] ansiTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int IEngine_GetImageQuality(int width, int height, byte[] rawImage, out int quality);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int IEngine_LoadBMP(byte[] filename, out int width, out int height, [Out] byte[] rawImage, ref int length);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int IEngine_ConvertBMP(byte[] bmpImage, out int width, out int height, [Out] byte[] rawImage, ref int length);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_CreateTemplate(int width, int height, byte[] rawImage, [Out] byte[] ansiTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_CreateTemplateEx(int width, int height, byte[] rawImage, [Out] byte[] ansiTemplate, byte[] skeletonImageFile, byte[] binarizedImageFile, byte[] minutiaeImageFile);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_VerifyMatch(byte[] probeTemplate, byte[] galleryTemplate, int maxRotation, out int score); 
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_VerifyMatchEx(byte[] probeTemplate, int probeView, byte[] galleryTemplate, int galleryView, int maxRotation, out int score); 
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_CreateTemplate(int width, int height, byte[] rawImage, [Out] byte[] isoTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_CreateTemplateEx(int width, int height, byte[] rawImage, [Out] byte[] isoTemplate, byte[] skeletonImageFile, byte[] binarizedImageFile, byte[] minutiaeImageFile);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_VerifyMatch(byte[] probeTemplate, byte[] galleryTemplate, int maxRotation, out int score); 
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_VerifyMatchEx(byte[] probeTemplate, int probeView, byte[] galleryTemplate, int galleryView, int maxRotation, out int score); 
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_GetTemplateParameter(byte[] ansiTemplate, TemplateParameter parameter, out int value);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_SetTemplateParameter(byte[] ansiTemplate, TemplateParameter parameter, int value);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_GetFingerView(byte[] ansiTemplate, int fingerView, [Out] byte[] outTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_DrawMinutiae(byte[] ansiTemplate, int width, int height, byte[] inputImage, [Out] byte[] outputBmpImage, ref int outputImageLength);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_GetMinutiae(byte[] ansiTemplate, [Out] Minutiae[] minutiae, out int minutiaeCount);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_MergeTemplates(byte[] referenceTemplate, byte[] addedTemplate, ref int length, [Out] byte[] outTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_LoadTemplate(byte[] filename, [Out] byte[] ansiTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_RemoveMinutiae(byte[] ansiTemplate, int maxMinutiaeCount, ref int length, [Out] byte[] outTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ANSI_SaveTemplate(byte[] filename, byte[] ansiTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_GetTemplateParameter(byte[] isoTemplate, TemplateParameter parameter, out int value);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_SetTemplateParameter(byte[] isoTemplate, TemplateParameter parameter, int value);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_GetFingerView(byte[] isoTemplate, int fingerView, [Out] byte[] outTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_DrawMinutiae(byte[] isoTemplate, int width, int height, byte[] inputImage, [Out] byte[] outputBmpImage, ref int outputImageLength);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_GetMinutiae(byte[] isoTemplate, [Out] Minutiae[] minutiae, out int minutiaeCount);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_MergeTemplates(byte[] referenceTemplate, byte[] addedTemplate, ref int length, [Out] byte[] outTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_LoadTemplate(byte[] filename, [Out] byte[] isoTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_RemoveMinutiae(byte[] isoTemplate, int maxMinutiaeCount, ref int length, [Out] byte[] outTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_SaveTemplate(byte[] filename, byte[] isoTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int IEngine_ConvertTemplate(TemplateFormat inputTemplateType, byte[] inputTemplate, TemplateFormat outputTemplateType, ref int length, [Out] byte[] outputTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_ConvertToISOCardCC(byte[] isoTemplate, int maximumMinutiaeCount, IEngineSortOrder minutiaeOrder, IEngineSortOrder minutiaeSecondaryOrder, ref int length, [Out] byte[] isoCCTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_CARD_CC_ConvertToISO(byte[] isoCCTemplate, ref int length, [Out] byte[] isoTemplate);
        [DllImport("iengine_ansi_iso.dll")]
        public static extern int ISO_CARD_CC_GetMinutiaeData(byte[] isoCCTemplate, out int minutiaeCount, [Out] byte[] minutiaeData, out int minutiaeDataSize);
    }
}
