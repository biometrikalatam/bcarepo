using System;
using System.Collections.Generic;
using System.Text;

namespace Innovatrics.AnsiIso
{
    public enum TemplateParameter
    {
        PRODUCT_OWNER = 0,
        PRODUCT_VERSION = 1,
        TEMPLATE_SIZE = 2,
        CAPTURE_EQUIPMENT_COMPLIANCE = 3,
        CAPTURE_EQUIPMENT_ID = 4,
        FINGER_VIEW_COUNT = 5,
        FINGER_POSITION = 10,
        IMPRESSION_TYPE = 11,
        FINGER_QUALITY = 12
    }
}
