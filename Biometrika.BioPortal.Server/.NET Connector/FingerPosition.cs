using System;
using System.Collections.Generic;
using System.Text;

namespace Innovatrics.AnsiIso
{
    public enum FingerPosition
    {
        UNKNOWN_FINGER = 0,
        RIGHT_THUMB = 1,
        RIGHT_INDEX = 2,
        RIGHT_MIDDLE = 3,
        RIGHT_RING = 4,
        RIGHT_LITTLE = 5,
        LEFT_THUMB = 6,
        LEFT_INDEX = 7,
        LEFT_MIDDLE = 8,
        LEFT_RING = 9,
        LEFT_LITTLE = 10
    }
}
