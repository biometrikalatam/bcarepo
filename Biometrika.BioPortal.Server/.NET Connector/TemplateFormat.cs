using System;
using System.Collections.Generic;
using System.Text;

namespace Innovatrics.AnsiIso
{
    public enum TemplateFormat
    {
        ANSI_TEMPLATE = 0,
        ISO_TEMPLATE = 1,
        ILO_SID_TEMPLATE = 2
    }
}
