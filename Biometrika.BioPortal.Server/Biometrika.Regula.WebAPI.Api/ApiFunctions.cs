﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using log4net;

namespace Biometrika.Regula.WebAPI.Api
{
    public class ApiFunctions
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ApiFunctions));

        internal static string _URLAPI = "http://localhost:8089/WebApi/";
        internal static string _USER = "testuser";
        internal static string _PASSWORD = "Regul@SdkTest";

        internal static string Token;
        internal static bool IsLoggedIn;

        public static bool Authenticate()
        {
            return Authenticate(_USER,_PASSWORD);
        }

        public static bool Authenticate(string userName, string password)
        {
            bool ret = false;
            try
            {
                LOG.Debug("ApiFunctions.Authenticate IN...");
                using (var client = new HttpClient())
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                    //ConfigurationManager.RefreshSection("appSettings");
                    var userData = new UserData() { UserId = userName, Password = password };
                    var url = _URLAPI + "Authentication/Authenticate";
                    LOG.Debug("ApiFunctions.Authenticate - Calling Url => " + url);
                    var response = client.PostAsJsonAsync(url, userData).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        LOG.Debug("ApiFunctions.Authenticate - response.IsSuccessStatusCode True");
                        Token = response.Headers.GetValues("X-Token").FirstOrDefault();
                        if (!string.IsNullOrWhiteSpace(Token)) {
                            ret = true;
                            IsLoggedIn = true;
                            LOG.Debug("ApiFunctions.Authenticate - Token = " + Token.Substring(0, 10) + "...");
                        } else
                        {
                            LOG.Warn("ApiFunctions.Authenticate - Token = NULL");
                        }
                    } else
                    {
                        LOG.Warn("ApiFunctions.Authenticate - IsSuccessStatusCode False [" + 
                            (response != null ? "Response NULL" : response.StatusCode.ToString()) + "]"); 
                    }
                }
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("ApiFunctions.Authenticate Error: " + ex.Message);
            }
            LOG.Debug("ApiFunctions.Authenticate OUT!");
            return ret;
        }

        public static string SubmitTransaction(List<Picture> pictureList, int capabilities, int authenticity)
        {
            string ret = null;            
            try
            {
                LOG.Debug("ApiFunctions.SubmitTransaction IN...");
                if (!IsLoggedIn)
                {
                    IsLoggedIn = Authenticate();
                }

                if (IsLoggedIn)
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("X-Token", Token);
                        LOG.Debug("ApiFunctions.SubmitTransaction - Calling " + _URLAPI + "Transaction2/SubmitTransaction?capabilities=" + capabilities +
                                                    "&authenticity=" + authenticity);
                        var response =
                            client.PostAsJsonAsync(_URLAPI + "Transaction2/SubmitTransaction?capabilities=" + capabilities +
                                                    "&authenticity=" + authenticity, pictureList).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            LOG.Debug("ApiFunctions.SubmitTransaction - response.IsSuccessStatusCode = true!");
                            return response.Content.ReadAsStringAsync().Result;
                        }
                        else
                        {
                            LOG.Warn("ApiFunctions.SubmitTransaction - response = " + 
                                (response != null ? "Response NULL" : response.StatusCode.ToString()) + "]");
                            CheckIfAuthenticated(response);
                        }
                    }
                } else
                {
                    LOG.Warn("ApiFunctions.SubmitTransaction - User no logueado!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ApiFunctions.SubmitTransaction Error: " + ex.Message);
            }
            LOG.Debug("ApiFunctions.SubmitTransaction OUT!");
            return null;
        }

        /// <summary>
        /// Toma el status de una transccion dado el id
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public static string GetTransactionStatus(string transactionId)
        {
            string ret = null;
            try
            {
                LOG.Debug("ApiFunctions.GetTransactionStatus IN...");
                if (!IsLoggedIn)
                {
                    IsLoggedIn = Authenticate();
                }

                if (IsLoggedIn)
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("X-Token", Token);
                        LOG.Debug("ApiFunctions.GetTransactionStatus - Calling " +
                            _URLAPI + "Transaction2/GetTransactionStatus?transactionId=" + transactionId);
                        var response =
                            client.GetAsync(_URLAPI + "Transaction2/GetTransactionStatus?transactionId=" + transactionId).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            LOG.Debug("ApiFunctions.GetTransactionStatus - response.IsSuccessStatusCode = true!");
                            var jObject = Newtonsoft.Json.Linq.JObject.Parse(response.Content.ReadAsStringAsync().Result);
                            return jObject.GetValue("Status").ToString();
                        }
                        else
                        {
                            LOG.Warn("ApiFunctions.GetTransactionStatus - response = " +
                                (response != null ? "Response NULL" : response.StatusCode.ToString()) + "]");
                            CheckIfAuthenticated(response);
                        }
                    }
                } else
                {
                    LOG.Warn("ApiFunctions.GetTransactionStatus - User no logueado!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ApiFunctions.GetTransactionStatus Error: " + ex.Message);
            }
            LOG.Debug("ApiFunctions.GetTransactionStatus OUT!");
            return null;
        }

        /// <summary>
        /// Toma el resultado de una transaccion dado un type pedido, de acuerdo al type se recuperan datos 
        /// de diversos temas (MRZ, OCR, etc)
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="resultType"></param>
        /// <returns></returns>
        public static string GetTransactionResult(string transactionId, int resultType)
        {
            string ret = null;
            try
            {
                LOG.Debug("ApiFunctions.GetTransactionResult IN...");
                if (!IsLoggedIn)
                {
                    IsLoggedIn = Authenticate();
                }

                if (IsLoggedIn)
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("X-Token",Token);
                        LOG.Debug("ApiFunctions.GetTransactionResult - Calling " +
                                    _URLAPI + "Transaction2/GetTransactionResult?transactionId=" +
                                                    transactionId + "&resultType=" + resultType);
                        var response =
                            client.GetAsync(_URLAPI + "Transaction2/GetTransactionResult?transactionId=" + 
                                            transactionId + "&resultType=" + resultType).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            LOG.Debug("ApiFunctions.GetTransactionResult - response.IsSuccessStatusCode = true!");
                            var result = response.Content.ReadAsStringAsync().Result;
                            return result.Equals("null") ? null : result;
                        }
                        else
                        {
                            LOG.Warn("ApiFunctions.GetTransactionResult - response = " +
                                (response != null ? "Response NULL" : response.StatusCode.ToString()) + "]");
                            CheckIfAuthenticated(response);
                        }
                    }
                }
                else
                {
                    LOG.Warn("ApiFunctions.GetTransactionResult - User no logueado!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ApiFunctions.GetTransactionResult Error: " + ex.Message);
            }
            LOG.Debug("ApiFunctions.GetTransactionResult OUT!");
            return null;
        }

        public static string GetTransactionImages(string transactionId)
        {
            string ret = null;
            try
            {
                LOG.Debug("ApiFunctions.GetTransactionImages IN...");
                if (!IsLoggedIn)
                {
                    IsLoggedIn = Authenticate();
                }

                if (IsLoggedIn)
                {
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Add("X-Token", Token);
                        LOG.Debug("ApiFunctions.GetTransactionImages - Calling " +
                                   _URLAPI + "Transaction2/GetImages?transactionId=" +
                                                   transactionId);
                        var response =
                            client.GetAsync(_URLAPI + "Transaction2/GetImages?transactionId=" + transactionId).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            LOG.Debug("ApiFunctions.GetTransactionImages - response.IsSuccessStatusCode = true!");
                            var result = response.Content.ReadAsStringAsync().Result;
                            return result.Equals("null") ? null : result;
                        }
                        else
                        {
                            LOG.Warn("ApiFunctions.GetTransactionImages - response = " +
                                (response != null ? "Response NULL" : response.StatusCode.ToString()) + "]");
                            CheckIfAuthenticated(response);
                        }
                    }
                }
                else
                {
                    LOG.Warn("ApiFunctions.GetTransactionImages - User no logueado!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ApiFunctions.GetTransactionImages Error: " + ex.Message);
            }
            LOG.Debug("ApiFunctions.GetTransactionImages OUT!");
            return null;
            
        }

        private static void CheckIfAuthenticated(HttpResponseMessage message)
        {
            string ret = null;
            try
            {
                LOG.Debug("ApiFunctions.CheckIfAuthenticated IN...");
                if (message.StatusCode == HttpStatusCode.Unauthorized)
                {
                    LOG.Debug("ApiFunctions.CheckIfAuthenticated StatusCode = HttpStatusCode.Unauthorized => Desconectando...");
                    Token = string.Empty;
                    IsLoggedIn = false;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("ApiFunctions.CheckIfAuthenticated Error: " + ex.Message);
            }
            LOG.Debug("ApiFunctions.CheckIfAuthenticated OUT!");
        }

        private class UserData
        {
            public string UserId { get; set; }
            public string Password { get; set; }
        }
    }
}
