﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using log4net;
using Newtonsoft.Json;

namespace Biometrika.Regula.WebAPI.Api
{
    public class RegulaHandler
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(RegulaHandler));

        private static string _Token;
        public static bool _IsLoggedIn;

        internal string _URLAPI = "http://localhost:8089/WebApi/";
        internal string _USER = "testuser";
        internal string _PASSWORD = "Regul@SdkTest";

        public bool IsLoggedIn()
        {
            return _IsLoggedIn;
        }

        /// <summary>
        /// Inicializa conexion con webapi server y guarda datos y token para seguir procesando.
        /// </summary>
        /// <returns></returns>
        public int Init()
        {
            try
            {
                if (!_IsLoggedIn)
                {
                    ApiFunctions._URLAPI = _URLAPI;
                    _IsLoggedIn = ApiFunctions.Authenticate(_USER, _PASSWORD);
                }
            }
            catch (Exception ex)
            {

                LOG.Error("RegulaHandler.Init() Error: " + ex.Message);
            }
            return 0;
        }

        /// <summary>
        /// Setes parametros de conexión y llama a Init()
        /// </summary>
        /// <param name="url"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public int Init(string url, string user, string password)
        {
            try
            {
                LOG.Debug("RegulaHandler.Init - IN...");
                _URLAPI = url;
                _USER = user;
                _PASSWORD = password;
                LOG.Debug("RegulaHandler.Init - _URLAPI = " + url + " - User = " + user + " - Psw = " + password.Substring(0, 4) + "..."); 
            }
            catch (Exception ex)
            {
                LOG.Error("RegulaHandler.Init(string url, string user, string password) Error: " + ex.Message);
                return -1;
            }
            LOG.Debug("RegulaHandler.Init OUT!");
            return Init();
        }


        /// <summary>
        /// Dado el front y back del documento, intento reconocer lo mejor posible para llenar los datos en Document
        /// </summary>
        /// <param name="front"></param>
        /// <param name="back"></param>
        /// <param name="document"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        public int RecognizeDocument(string front, string back, int capabilities, out Document document, out string msgerr)
        {
            int ret = 0;
            document = new Document();
            msgerr = null;
            List<Picture> listPict;
            try
            {
                LOG.Debug("RegulaHandler.RecognizeDocument IN...");
                //0.-Si vienen vacios front y back => ret error
                if (string.IsNullOrEmpty(front) && string.IsNullOrEmpty(back))
                {
                    msgerr = "Parametros de entrada en null";
                    return -6; // Errors.IERR_BAD_PARAMETER
                } else {
                    listPict = new List<Picture>();
                }

                //1.- Si viene fron y dorso agrego a lista
                int idx = 0;
                if (!string.IsNullOrEmpty(front)) {
                    Picture picFront = new Picture();
                    picFront.Base64ImageString = front;
                    picFront.Format = ".jpg";
                    picFront.LightIndex = 6;
                    picFront.PageIndex = idx;
                    idx++;
                    listPict.Add(picFront);
                }
                if (!string.IsNullOrEmpty(back))
                {
                    Picture picBack = new Picture();
                    picBack.Base64ImageString = back;
                    picBack.Format = ".jpg";
                    picBack.LightIndex = 6;
                    picBack.PageIndex = idx;
                    idx++;
                    listPict.Add(picBack);
                }

                //2.- LLamo a recognized
                string _currentId = ApiFunctions.SubmitTransaction(listPict, capabilities, 0);

                //3.- Interpreto resultado y lleno document
                if (!string.IsNullOrEmpty(_currentId))
                {
                    _currentId = _currentId.Replace(@"\", string.Empty);
                    _currentId = _currentId.Replace("\"", string.Empty);

                    bool success = false;
                    int idxCounter = 0;
                    var status = ApiFunctions.GetTransactionStatus(_currentId);
                    while (status.Equals("1") || status.Equals("2") && idxCounter < 10)
                    {
                        LOG.Debug("RegulaHandler.RecognizeDocument - Revisando [" + _currentId + "] => #" + (idxCounter + 1).ToString());
                        //bgWorker.ReportProgress(0, "In progress");
                        status = ApiFunctions.GetTransactionStatus(_currentId);
                        idxCounter++;
                        Thread.Sleep(500);
                    }
                    if (status.Equals("4")) //bgWorker.ReportProgress(0, "Error!");
                    {
                        //MessageBox.Show("4-Error");
                        ret = -2;
                        LOG.Error("RegulaHandler.RecognizeDocument - Result Proceso => 4 - ERROR");
                        document = null;
                    }
                    else if (status.Equals("3"))
                    {
                        //MessageBox.Show("3-Finish OK");
                        LOG.Debug("RegulaHandler.RecognizeDocument - Result Proceso => 3 - Finish OK");
                        //bgWorker.ReportProgress(0, "Finished!");
                        success = true;
                    }
                    else
                    {
                        ret = -1;
                        document = null;
                        LOG.Debug("RegulaHandler.RecognizeDocument - Result Proceso Desconocido => " + status);
                    }

                    if (success)
                    {
                        LOG.Debug("RegulaHandler.RecognizeDocument - Recupero informacion y armo salida...");
                        int err = 0;
                        document = ExtractInfo(_currentId, document, out err);
                        if (document != null) document = ExtractImages(_currentId, document);
                        else
                        {
                            msgerr = (err == -9000) ?
                                "Frente y Dorso de documento presentado no corresponde a misma persona. No coiniciden los numeros de documento" : null;
                            ret = err;
                        }
                    }
                } else
                {
                    document = null;
                    LOG.Error("RegulaHandler.RecognizeDocument - Trackid nulo => Sale con error!");
                }

            }
            catch (Exception ex)
            {
                ret = -1;
                document = null;
                LOG.Error("RegulaHandler.RecognizeDocument Error: " + ex.Message);
            }

            LOG.Debug("RegulaHandler.RecognizeDocument OUT!");
            return ret;
        }

        /// <summary>
        /// Dada un trackid recupera las imágenes obtenidas desde el WebApi server
        /// y llena lista en Document
        /// </summary>
        /// <param name="currentId"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        private Document ExtractImages(string currentId, Document document)
        {
            Document outDOCInfoData = null; // (document != null ? document : new Document());
            XmlDocument _DOCUMENT;

            try
            {
                LOG.Debug("RegulaHandler.ExtractImages IN...");
                if (document == null)
                {
                    outDOCInfoData = new Document();
                    outDOCInfoData._DocInfo = new DOCInfoData();
                    outDOCInfoData._DocImages = new List<DOCInfoImage>();
                }
                else
                {
                    outDOCInfoData = document;
                    if (outDOCInfoData._DocImages == null)
                    {
                        outDOCInfoData._DocImages = new List<DOCInfoImage>();
                    }
                }

                //Recupero images recortadas desde las cedulas
                var xmldoc = ApiFunctions.GetTransactionResult(currentId, 6);
                if (!string.IsNullOrEmpty(xmldoc))
                {
                    LOG.Debug("RegulaHandler.ExtractImages - Procesando respuesta XML...");
                    string[] aResult = xmldoc.Split(',');


                    if (aResult != null && aResult.Length > 0) {
                        //outDOCInfoData._DocImages = new List<DOCInfoImage>();
                        for (int i = 0; i < aResult.Length; i++)
                        {
                            aResult[i] = aResult[i].TrimStart('[').TrimEnd(']').TrimStart('\"').TrimEnd('\"');
                            aResult[i] = aResult[i].Replace("\\n", string.Empty);
                            aResult[i] = aResult[i].Replace("\\", string.Empty);


                            _DOCUMENT = new XmlDocument();
                            _DOCUMENT.LoadXml(aResult[i]);
                            XmlNodeList noList = _DOCUMENT.GetElementsByTagName("Document_Image");
                            if (noList != null)
                            {
                                for (int j = 0; j < noList.Count; j++)
                                {
                                    var Item = (XmlElement)noList.Item(j);
                                    int FieldType = Convert.ToInt32(Item.GetElementsByTagName("FieldType").Item(0).InnerText);
                                    DOCInfoImage newImage = new DOCInfoImage();
                                    newImage.code = Item.GetElementsByTagName("FieldType").Item(0).InnerText;
                                    newImage.name = Item.GetElementsByTagName("FieldName").Item(0).InnerText;
                                    newImage.b64image = Item.GetElementsByTagName("File_Image").Item(0).InnerText;
                                    outDOCInfoData._DocImages.Add(newImage);
                                }
                            } else
                            {
                                outDOCInfoData = null;
                                LOG.Debug("RegulaHandler.ExtractInfo - Parseo XML incorrecto!");
                            }
                        }
                    } else
                    {
                        outDOCInfoData = null;
                        LOG.Debug("RegulaHandler.ExtractInfo - Lista de Respuestas nula");
                    }
                } else
                {
                    outDOCInfoData = null;
                    LOG.Debug("RegulaHandler.ExtractInfo - XML nulo desde WebAPI service!");
                }

                //Recupero crop de cedula completa front y back segun lo que venga
                string result = ApiFunctions.GetTransactionResult(currentId, 2);
                if (!string.IsNullOrEmpty(xmldoc))
                {
                    LOG.Debug("RegulaHandler.ExtractImages Cropped Completas - Procesando respuesta XML...");
                    List<ImageResult> lImg = JsonConvert.DeserializeObject<List<ImageResult>>(result);

                    if (lImg.Count > 0)
                    {
                        LOG.Debug("RegulaHandler.ExtractImages Cropped Front Document Adding..."); 
                        DOCInfoImage newImage = new DOCInfoImage();
                        newImage.code = "199";  //Front Cedula Cropped
                        newImage.name = "FrontDocumentCropped";
                        newImage.b64image = lImg[0].Base64ImageString;
                        outDOCInfoData._DocImages.Add(newImage);

                        if (lImg.Count > 1) {  //Hay back que esta en posicion 1
                            LOG.Debug("RegulaHandler.ExtractImages Cropped Back Document Adding...");
                            newImage = new DOCInfoImage();
                            newImage.code = "200";  //Back Cedula Cropped
                            newImage.name = "BackDocumentCropped";
                            newImage.b64image = lImg[1].Base64ImageString;
                            outDOCInfoData._DocImages.Add(newImage);
                        }
                    }
                    else
                    {
                        outDOCInfoData = null;
                        LOG.Debug("RegulaHandler.ExtractImages - Lista de Respuestas nula");
                    }
                }
                else
                {
                    outDOCInfoData = null; 
                    LOG.Debug("RegulaHandler.ExtractImages - JSON nulo desde WebAPI service!");
                }
            }
            catch (Exception ex)
            {
                outDOCInfoData = null;
                LOG.Error("RegulaHandler.ExtractImages Error: " + ex.Message);
            }

            LOG.Debug("RegulaHandler.ExtractImages OUT!");
            return outDOCInfoData;
        }

        /// <summary>
        /// Dada un trackid recupera datos reconocidos desde el webapi server.
        /// Da prioriudad a datos de MRZ sino toma Visuales OCR
        /// </summary>
        /// <param name="currentId"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        private Document ExtractInfo(string currentId, Document document, out int errcode)
        {
            errcode = 0;
            Document outDOCInfoData = null;
            XmlDocument _DOCUMENT;

            try
            {
                LOG.Debug("RegulaHandler.ExtractInfo IN...");

                if (document == null)
                {
                    outDOCInfoData = new Document();
                    outDOCInfoData._DocInfo = new DOCInfoData();
                } else
                {
                    outDOCInfoData = document;
                    if (outDOCInfoData._DocInfo == null)
                    {
                        outDOCInfoData._DocInfo = new DOCInfoData();
                    }
                }
                var xmldoc = ApiFunctions.GetTransactionResult(currentId, 15);

                if (!string.IsNullOrEmpty(xmldoc))
                {
                    LOG.Debug("RegulaHandler.ExtractInfo - Procesando respuesta XML..."); 
                    xmldoc = xmldoc.TrimStart('[').TrimEnd(']').TrimStart('\"').TrimEnd('\"');
                    xmldoc = xmldoc.Replace("\\n", string.Empty);
                    xmldoc = xmldoc.Replace("\\", string.Empty);

                    _DOCUMENT = new XmlDocument();
                    _DOCUMENT.LoadXml(xmldoc);
                    XmlNodeList noList = _DOCUMENT.GetElementsByTagName("Document_Field_Analysis_Info");

                    if (noList != null) {
                        LOG.Debug("RegulaHandler.ExtractInfo - Comienza recorrido de XML para completar salida...");

                        for (int i = 0; i < noList.Count; i++)
                        {
                            var Item = (XmlElement)noList.Item(i);

                            int FieldType = Convert.ToInt32(Item.GetElementsByTagName("FieldType").Item(0).InnerText);
                            int LCID = 0;
                            if (FieldType > 0xFFFF)
                            {
                                LCID = FieldType >> 16;
                                FieldType = (FieldType << 16) >> 16;
                            }

                            switch (FieldType)
                            {
                                case 0: //outDOCInfoData.ft_Document_Class_Code;   //0
                                    outDOCInfoData._DocInfo.ft_Document_Class_Code =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                case 1: //outDOCInfoData.ft_Issuing_State_Code;   //1  (CHL x ejemplo)
                                    outDOCInfoData._DocInfo.ft_Issuing_State_Code =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                case 2: //outDOCInfoData.ft_Document_Number;      //2
                                    outDOCInfoData._DocInfo.ft_Document_Number =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                case 3: //outDOCInfoData.ft_Date_of_Expiry;       //3
                                    outDOCInfoData._DocInfo.ft_Date_of_Expiry =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Date_of_Issue;        //4
                                case 4:
                                    outDOCInfoData._DocInfo.ft_Date_of_Issue =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Date_of_Birth;        //5
                                case 5:
                                    outDOCInfoData._DocInfo.ft_Date_of_Birth =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Place_of_Birth;       //6 - En Extranjero toma la visa 
                                case 6:
                                    outDOCInfoData._DocInfo.ft_Place_of_Birth =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Personal_Number;      //7 - RUT
                                case 7:
                                    outDOCInfoData._DocInfo.ft_Personal_Number =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    //Chequeo que rut reconocido en cada lado es el mismo sino sale 
                                    if (!EqualNumber(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText,
                                                     Item.GetElementsByTagName("Field_Visual").Item(0).InnerText))
                                    {
                                        errcode = -9000;
                                        return null;
                                    }
                                    //Chequeo que rut sea valido formado si es chile => Sino nada para tomar datos de pasaportes y otras cedulas
                                    if ((!string.IsNullOrEmpty(outDOCInfoData._DocInfo.ft_Document_Class_Code) &&
                                          (outDOCInfoData._DocInfo.ft_Document_Class_Code.Equals("IE") ||
                                           outDOCInfoData._DocInfo.ft_Document_Class_Code.Equals("ID"))) &&
                                        ((!string.IsNullOrEmpty(outDOCInfoData._DocInfo.ft_Issuing_State_Code) &&
                                          outDOCInfoData._DocInfo.ft_Issuing_State_Code.Equals("CHL"))
                                       ))
                                    {
                                        RUN run = RUN.ConvertirEx(outDOCInfoData._DocInfo.ft_Personal_Number);
                                        if (run == null) return null;  //Si no reconoce ok => Devuelve nulo 
                                        else outDOCInfoData._DocInfo.ft_Personal_Number = run.ToString(true);
                                    }
                                    break;
                                //outDOCInfoData.ft_Surname;              //8 - Apellido Paterno
                                case 8:
                                    outDOCInfoData._DocInfo.ft_Surname =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Given_Names;          //9 - Nombres
                                case 9:
                                    outDOCInfoData._DocInfo.ft_Given_Names =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Mothers_Name;         // = 10 - App Materno
                                case 10:
                                    outDOCInfoData._DocInfo.ft_Date_of_Issue =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Nationality;          // = 11 - Nacionalidad
                                case 11:
                                    outDOCInfoData._DocInfo.ft_Nationality =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Sex;                  // = 12,
                                case 12:
                                    outDOCInfoData._DocInfo.ft_Sex =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Surname_And_Given_Names; // = 25, Nombre completo
                                case 25:
                                    outDOCInfoData._DocInfo.ft_Surname_And_Given_Names =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Nationality_Code;     // = 26, Codigo Pais (ARG x ej)
                                case 26:
                                    outDOCInfoData._DocInfo.ft_Nationality_Code =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Issuing_State_Name;   // = 38, Pais (Chile por ej)
                                case 38:
                                    outDOCInfoData._DocInfo.ft_Issuing_State_Name =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Place_of_Issue;       // = 39, En Extranjero toma la visa
                                case 39:
                                    outDOCInfoData._DocInfo.ft_Place_of_Issue =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Visa                  // = 100
                                case 100:
                                    outDOCInfoData._DocInfo.ft_Visa =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Age;                  // = 185,
                                case 185:
                                    outDOCInfoData._DocInfo.ft_Age =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                //outDOCInfoData.ft_Profesion;            // = 873070904 / 312 (No documentada)
                                case 873070904: // 312:
                                    outDOCInfoData._DocInfo.ft_Profesion =
                                                        string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                                        Item.GetElementsByTagName("Field_Visual").Item(0).InnerText :
                                                        Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else {

                        LOG.Debug("RegulaHandler.ExtractInfo - Parseo XML incorrecto!");
                    }
                }
            }
            catch (Exception ex)
            {
                outDOCInfoData = null;
                LOG.Error("RegulaHandler.ExtractInfo Error: " + ex.Message);
            }
            
            LOG.Debug("RegulaHandler.ExtractInfo OUT!");
            return outDOCInfoData;
        }

        private bool EqualNumber(string runMRZ, string runVisual)
        {
            try
            {
                if (string.IsNullOrEmpty(runMRZ) || string.IsNullOrEmpty(runVisual)) return true;

                if (!string.IsNullOrEmpty(runMRZ) && !string.IsNullOrEmpty(runVisual))
                {
                    RUN run1 = RUN.ConvertirEx(runMRZ);
                    RUN run2 = RUN.ConvertirEx(runVisual);
                    return (run1.ToString(true).Equals(run2.ToString(true))); 
                }
            }
            catch (Exception ex)
            {
                LOG.Error("RegulaHandler.NotEqualNumber Error: " + ex.Message);
                return false;
            }
            return true;
        }
    }
}
