﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Biometrika.Regula.WebAPI.Api
{
    public class RegulaResult
    {
        internal static XmlDocument _DOCUMENT;

        // <summary>
        // typeProcess constants
        //      Usadas para procesar la o las imagenes enviadas en parámetro, con data y/o images
        // </summary>
        public static int _FLAG_PROCESS_ALL = 0;
        public static int _FLAG_PROCESS_INFO = 1;
        public static int _FLAG_PROCESS_IMAGE = 2;

        // <summary>
        // typeRecognize constants
        //      Usadas para procesar solo un lado de la cedula. Extrae:
        //          _FLAG_INFO_BEST     = 0 => Mejor dato presente en est orden: 1-MRZ -> 2-OCR -> 3-Barcode
        //          _FLAG_INFO_MRZ      = 1 => Datos de MRZ Visual desde la imagen si están presentes
        //          _FLAG_INFO_OCR      = 2 => Datos de OCR Visual desde la imagen si están presentes
        //          _FLAG_INFO_BARCODE  = 3 => Datos de Barcode Visual desde la imagen si están presentes
        // </summary>
        public static int _FLAG_RECOGNIZE_INFO_BEST = 0;
        public static int _FLAG_RECOGNIZE_INFO_MRZ = 1;
        public static int _FLAG_RECOGNIZE_INFO_OCR = 2;
        public static int _FLAG_RECOGNIZE_INFO_BARCODE = 3;

        /// <summary>
        /// Dada un alista de imagenes, que puede ser de 1 o 2, reconoce los 
        /// </summary>
        /// <param name="xmldoc"></param>
        /// <param name="typeRecognize"></param>
        /// <param name="inDOCInfoData"></param>
        /// <param name="outDOCInfoData"></param>
        /// <returns></returns>
        internal static int ProcessDocument(List<string> xmldocList, int typeProcess, out Document outDodument)
        {
            int ret = 0;
            outDodument = null;
            try
            {
                DOCInfoData InfoData = null;
                List<DOCInfoImage> InfoImages = null;
                foreach (var xmldoc in xmldocList)
                {
                    //Si es solo info o debe procesar todo
                    if (typeProcess == _FLAG_PROCESS_INFO || typeProcess == _FLAG_PROCESS_ALL)
                    {
                        ret = ExtractDOCInfo(xmldoc, _FLAG_RECOGNIZE_INFO_BEST, InfoData, out InfoData);
                        if (ret != 0)
                        {

                        }
                        else
                        {
                            ret = ExtractDOCImage(xmldoc, _FLAG_RECOGNIZE_INFO_BEST, InfoImages, out InfoImages);
                            if (ret != 0)
                            {

                            }
                            //else
                            //{
                            //    ret = ExtractDOCInfo(xmldoc, _FLAG_RECOGNIZE_INFO_MRZ, InfoData, out InfoData);
                            //}
                        }
                    }
                }

                outDodument = new Document();
                outDodument._DocInfo = (DOCInfoData)InfoData.Clone();
                outDodument._DocImages = InfoImages;
            }
            catch (Exception ex)
            {
                ret = -1;
            }
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmldoc"></param>
        /// <param name="typeRecognize">0 - MRZ | 1 - Visual | 2 - Barcode</param>
        /// <param name="outDOCInfoData"></param>
        /// <returns></returns>
        internal static int ExtractDOCInfo(string xmldoc, int typeRecognize, DOCInfoData inDOCInfoData, out DOCInfoData outDOCInfoData)
        {
            outDOCInfoData = null;
            int ret = 0;
            XmlDocument _DOCUMENT;

            try
            {
                if (inDOCInfoData == null) outDOCInfoData = new DOCInfoData();
                else outDOCInfoData = inDOCInfoData;

                if (!string.IsNullOrWhiteSpace(xmldoc))
                {
                    xmldoc = xmldoc.TrimStart('[').TrimEnd(']').TrimStart('\"').TrimEnd('\"');
                    xmldoc = xmldoc.Replace("\\n", string.Empty);
                    xmldoc = xmldoc.Replace("\\", string.Empty);

                    _DOCUMENT = new XmlDocument();
                    _DOCUMENT.LoadXml(xmldoc);
                    XmlNodeList noList = _DOCUMENT.GetElementsByTagName("Document_Field_Analysis_Info");

                    for (int i = 0; i < noList.Count; i++)
                    {
                        var Item = (XmlElement)noList.Item(i);

                        int FieldType = Convert.ToInt32(Item.GetElementsByTagName("FieldType").Item(0).InnerText);
                        int LCID = 0;
                        if (FieldType > 0xFFFF)
                        {
                            LCID = FieldType >> 16;
                            FieldType = (FieldType << 16) >> 16;
                        }

                        switch (FieldType)
                        {
                            case 0: //outDOCInfoData.ft_Document_Class_Code;   //0

                                outDOCInfoData.ft_Document_Class_Code = string.IsNullOrEmpty(outDOCInfoData.ft_Document_Class_Code) ?
                                            Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText : outDOCInfoData.ft_Document_Class_Code;

                                if (string.IsNullOrEmpty(outDOCInfoData.ft_Document_Class_Code) ||
                                    (!string.IsNullOrEmpty(Item.GetElementsByTagName("Field_Visual").Item(0).InnerText) &&
                                    !outDOCInfoData.ft_Document_Class_Code.Equals(Item.GetElementsByTagName("Field_Visual").Item(0).InnerText)))
                                {
                                    outDOCInfoData.ft_Document_Class_Code = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                }

                                if (string.IsNullOrEmpty(outDOCInfoData.ft_Document_Class_Code) ||
                                    (!string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) &&
                                    !outDOCInfoData.ft_Document_Class_Code.Equals(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText)))
                                {
                                    outDOCInfoData.ft_Document_Class_Code = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                }

                                //switch (typeRecognize)
                                //{
                                //    case 1:
                                //        outDOCInfoData.ft_Document_Class_Code = string.IsNullOrEmpty(outDOCInfoData.ft_Document_Class_Code) ?
                                //            Item.GetElementsByTagName("Field_Visual").Item(0).InnerText : outDOCInfoData.ft_Document_Class_Code;
                                //        break;
                                //    case 2:
                                //        outDOCInfoData.ft_Document_Class_Code = string.IsNullOrEmpty(outDOCInfoData.ft_Document_Class_Code) ?
                                //            Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText : outDOCInfoData.ft_Document_Class_Code;
                                //        break;
                                //    default:
                                //        outDOCInfoData.ft_Document_Class_Code = string.IsNullOrEmpty(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText) ?
                                //            Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText: outDOCInfoData.ft_Document_Class_Code;
                                //        break;
                                //}
                                break;
                            case 1: //outDOCInfoData.ft_Issuing_State_Code;   //1  (CHL x ejemplo)
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Issuing_State_Code = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Issuing_State_Code = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Issuing_State_Code = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }

                                break;
                            case 2: //outDOCInfoData.ft_Document_Number;      //2
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Document_Number = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Document_Number = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Document_Number = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }

                                break;
                            case 3: //outDOCInfoData.ft_Date_of_Expiry;       //3
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Date_of_Expiry = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Date_of_Expiry = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Date_of_Expiry = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Date_of_Issue;        //4
                            case 4:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Date_of_Issue = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Date_of_Issue = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Date_of_Issue = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Date_of_Birth;        //5
                            case 5:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Date_of_Birth = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Date_of_Birth = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Date_of_Birth = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Place_of_Birth;       //6 - En Extranjero toma la visa 
                            case 6:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Place_of_Birth = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Place_of_Birth = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Place_of_Birth = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }

                                break;
                            //outDOCInfoData.ft_Personal_Number;      //7 - RUT
                            case 7:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Personal_Number = FormatRUT(Item.GetElementsByTagName("Field_Visual").Item(0).InnerText);
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Personal_Number = FormatRUT(Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText);
                                        break;
                                    default:
                                        outDOCInfoData.ft_Personal_Number = FormatRUT(Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText);
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Surname;              //8 - Apellido Paterno
                            case 8:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Surname = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Surname = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Surname = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Given_Names;          //9 - Nombres
                            case 9:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Given_Names = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Given_Names = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Given_Names = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Mothers_Name;         // = 10 - App Materno
                            case 10:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Mothers_Name = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Mothers_Name = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Mothers_Name = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Nationality;          // = 11 - Nacionalidad
                            case 11:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Nationality = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Nationality = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Nationality = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Sex;                  // = 12,
                            case 12:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Sex = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Sex = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Sex = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Surname_And_Given_Names; // = 25, Nombre completo
                            case 25:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Surname_And_Given_Names = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Surname_And_Given_Names = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Surname_And_Given_Names = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Nationality_Code;     // = 26, Codigo Pais (ARG x ej)
                            case 26:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Nationality_Code = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Nationality_Code = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Nationality_Code = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Issuing_State_Name;   // = 38, Pais (Chile por ej)
                            case 38:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Issuing_State_Name = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Issuing_State_Name = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Issuing_State_Name = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Place_of_Issue;       // = 39, En Extranjero toma la visa
                            case 39:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Place_of_Issue = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Place_of_Issue = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Place_of_Issue = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Visa                  // = 100
                            case 100:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Visa = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Visa = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Visa = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Age;                  // = 185,
                            case 185:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Age = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Age = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Age = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            //outDOCInfoData.ft_Profesion;            // = 873070904 / 312 (No documentada)
                            case 873070904: // 312:
                                switch (typeRecognize)
                                {
                                    case 1:
                                        outDOCInfoData.ft_Profesion = Item.GetElementsByTagName("Field_Visual").Item(0).InnerText;
                                        break;
                                    case 2:
                                        outDOCInfoData.ft_Profesion = Item.GetElementsByTagName("Field_Barcode").Item(0).InnerText;
                                        break;
                                    default:
                                        outDOCInfoData.ft_Profesion = Item.GetElementsByTagName("Field_MRZ").Item(0).InnerText;
                                        break;
                                }
                                break;
                            default:
                                break;
                        }

                    }
                }
            }
            catch (Exception)
            {

            }
            return ret;
        }


        /// <summary
        ///  Extraer las fotos desde el escaneo de la imagen de documento
        ///   201 - Photo
        ///   202 - Fingerprint
        ///   204 - Signature
        ///   205 - Barcode
        ///   210 - Ghost Photo
        internal static int ExtractDOCImage(string xmldoc, int typeRecognize, List<DOCInfoImage> inDOCImage, out List<DOCInfoImage> outDOCImage)
        {
            outDOCImage = null;
            int ret = 0;
            XmlDocument _DOCUMENT;

            try
            {
                if (inDOCImage == null) outDOCImage = new List<DOCInfoImage>();
                else outDOCImage = inDOCImage;

                if (!string.IsNullOrWhiteSpace(xmldoc))
                {
                    xmldoc = xmldoc.TrimStart('[').TrimEnd(']').TrimStart('\"').TrimEnd('\"');
                    xmldoc = xmldoc.Replace("\\n", string.Empty);
                    xmldoc = xmldoc.Replace("\\", string.Empty);

                    _DOCUMENT = new XmlDocument();
                    _DOCUMENT.LoadXml(xmldoc);
                    XmlNodeList noList = _DOCUMENT.GetElementsByTagName("Document_Image");

                    for (int i = 0; i < noList.Count; i++)
                    {
                        var Item = (XmlElement)noList.Item(i);

                        //int FieldType = Convert.ToInt32(Item.GetElementsByTagName("FieldType").Item(0).InnerText);

                        DOCInfoImage newImage = new DOCInfoImage();
                        newImage.code = Item.GetElementsByTagName("FieldType").Item(0).InnerText;
                        newImage.name = Item.GetElementsByTagName("FieldName").Item(0).InnerText;
                        newImage.b64image = Item.GetElementsByTagName("File_Image").Item(0).InnerText;
                        outDOCImage.Add(newImage);
                    }
                }
            }
            catch (Exception)
            {

            }
            return ret;
        }

        private static string FormatRUT(string run)
        {
            try
            {
                if (string.IsNullOrEmpty(run)) return null;

                if (run.IndexOf("-") > 0) return run;

                return run.Substring(0, run.Length - 1) + "-" + run.Substring(run.Length - 1);


            }
            catch (Exception)
            {
                return null;
            }
        }

    }

    public class Document : ICloneable
    {
        public DOCInfoData _DocInfo;
        public List<DOCInfoImage> _DocImages;

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class DOCInfoData : ICloneable
    {
        public DOCInfoData() { }

        public string ft_Document_Class_Code;  //0
        public string ft_Issuing_State_Code;   //1  (CHL x ejemplo)
        public string ft_Document_Number;      //2
        public string ft_Date_of_Expiry;       //3
        public string ft_Date_of_Issue;        //4
        public string ft_Date_of_Birth;        //5
        public string ft_Place_of_Birth;       //6 - En Extranjero toma la visa 
        public string ft_Personal_Number;      //7 - RUT
        public string ft_Surname;              //8 - Apellido Paterno
        public string ft_Given_Names;          //9 - Nombres
        public string ft_Mothers_Name;         // = 10 - App Materno
        public string ft_Nationality;          // = 11 - Nacionalidad
        public string ft_Sex;                  // = 12,
        public string ft_Surname_And_Given_Names; // = 25, Nombre completo
        public string ft_Nationality_Code;     // = 26, Codigo Pais (ARG x ej)
        public string ft_Issuing_State_Name;   // = 38, Pais (Chile por ej)
        public string ft_Place_of_Issue;       // = 39, En Extranjero toma la visa
        public string ft_Age;                  // = 185,
        public string ft_Profesion;            // = 873070904 / 312 (No documentada)
        public string ft_Visa;                 // = 100
        public string ft_MRZ_Strings;          // = 51  (String completo de MRZ)

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class DOCInfoImage : ICloneable
    {
        /// <summary>
        ///   199 - Front Cedula Cropped (Inventado por mi este codigo)
        ///   200 - Back Cedula Cropped (Inventado por mi este codigo)
        ///   201 - Photo
        ///   202 - Fingerprint
        ///   204 - Signature
        ///   205 - Barcode
        ///   210 - Ghost Photo
        /// </summary>
        public string code;
        public string name;
        public string b64image;

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class ResponseImagesResult
    {
        public ResponseImagesResult() { }

        public IList<ImageResult> listImages { get; set; }

    }

    public class ImageResult
    {
        public ImageResult() { }

        public string Base64ImageString { get; set; }
        public string Format { get; set; }
        public int LightIndex { get; set; }
        public int PageIndex { get; set; }
    }
 }