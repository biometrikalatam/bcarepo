﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp.WinForms;
using CefSharp;

namespace Biometrika.Jumio.Api.TestForm
{
    public partial class frmBrowser : Form
    {
        private ChromiumWebBrowser chromeBrowser;
        public string _trackid3ro;
        public string _url;


        public frmBrowser()
        {
            InitializeComponent();
        }

        internal void InitializeChromium(string url)
        {
            this.Text = this.Text + " - Transaction Id = " + _trackid3ro;
            CefSettings settings = new CefSettings();
            settings.CefCommandLineArgs.Add("enable-media-stream", "1");
            Cef.Initialize(settings);
            chromeBrowser = new ChromiumWebBrowser(url);

            //this.Controls.Add(chromeBrowser);
            this.panel1.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Cef.Shutdown();
        }

        private void frmBrowser_Load(object sender, EventArgs e)
        {
            InitializeChromium(_url);
        }
    }
}
