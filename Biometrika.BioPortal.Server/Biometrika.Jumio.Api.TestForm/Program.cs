﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace Biometrika.Jumio.Api.TestForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            test();
            //XmlConfigurator.Configure(new FileInfo("Logger.cdg"));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }

        private static void test()
        {
            try
            {
                string s = "{\"timestamp\":\"2020-07-24T14:35:24.356Z\",\"scanReference\":\"974f5b8b-c742-4b9d-b98e-5e85113389dd\",\"document\":{\"type\":\"ID_CARD\",\"dob\":\"1969-09-28\",\"expiry\":\"2022-08-02\",\"firstName\":\"GUSTAVO GERARDO\",\"gender\":\"M\",\"issuingCountry\":\"CHL\",\"issuingDate\":\"2017-08-21\",\"lastName\":\"SUHIT\",\"nationality\":\"ARG\",\"number\":\"601014310\",\"status\":\"DENIED_FRAUD\"},\"transaction\":{\"clientIp\":\"200.83.215.110\",\"customerId\":\"BiometrikaLatam\",\"date\":\"2020-07-24T12:47:43.377Z\",\"merchantScanReference\":\"fc308d9726a44a258a73c34e60b97704\",\"source\":\"WEB_CAM\",\"status\":\"DONE\",\"updatedAt\":\"2020-07-24T13:00:39.573Z\"},\"verification\":{\"mrzCheck\":\"NOT_AVAILABLE\",\"rejectReason\":{\"rejectReasonCode\":\"100\",\"rejectReasonDescription\":\"MANIPULATED_DOCUMENT\",\"rejectReasonDetails\":{\"detailsCode\":\"1002\",\"detailsDescription\":\"DOCUMENT_NUMBER\"}}}}";
                Biometrika.Jumio.Api.DataResponse dr = JsonConvert.DeserializeObject<Biometrika.Jumio.Api.DataResponse>(s);
                if (dr != null && dr.verification.rejectReason != null && dr.verification.rejectReason.rejectReasonDetails != null)
                {
                    //if (dr.verification.rejectReason.rejectReasonDetails.GetType().Equals(typeof(Biometrika.Jumio.Api.RejectReasonDetail))) {
                    //try
                    //{
                    //    Biometrika.Jumio.Api.RejectReasonDetail drd = 
                    //        JsonConvert.DeserializeObject<Biometrika.Jumio.Api.RejectReasonDetail>((string)dr.verification.rejectReason.rejectReasonDetails);
                    //    //(Biometrika.Jumio.Api.RejectReasonDetail)dr.verification.rejectReason.rejectReasonDetails;
                    //}
                    //catch (Exception ex)
                    //{
                    //    LOG.Error(" Error: " + ex.Message);
                    //}

                    //try
                    //{
                    //    List<Biometrika.Jumio.Api.RejectReasonDetail> l =
                    //        JsonConvert.DeserializeObject<List<Biometrika.Jumio.Api.RejectReasonDetail>>((string)dr.verification.rejectReason.rejectReasonDetails);
                    //    //(List<Biometrika.Jumio.Api.RejectReasonDetail>)dr.verification.rejectReason.rejectReasonDetails;
                    //}
                    //catch (Exception ex)
                    //{

                    //    LOG.Error(" Error: " + ex.Message);
                    //}
                    //} else
                    //{
                    //    List<Biometrika.Jumio.Api.RejectReasonDetail> l = 
                    //        (List<Biometrika.Jumio.Api.RejectReasonDetail>)dr.verification.rejectReason.rejectReasonDetails;
                    //}
                }

                s = "{\"timestamp\":\"2020-07-24T15:31:19.132Z\",\"scanReference\":\"f5f325e4-241b-473f-9419-487477cf4d09\",\"document\":{\"type\":\"ID_CARD\",\"dob\":\"1979-06-13\",\"expiry\":\"2021-06-13\",\"firstName\":\"MIRTHA ANDREA\",\"gender\":\"M\",\"issuingCountry\":\"CHL\",\"lastName\":\"RAMIREZ RAMIREZ\",\"nationality\":\"ARG\",\"number\":\"107792585\",\"status\":\"DENIED_FRAUD\"},\"transaction\":{\"clientIp\":\"200.83.215.110\",\"customerId\":\"biometrika\",\"date\":\"2020-07-15T21:21:50.313Z\",\"merchantScanReference\":\"654321\",\"source\":\"WEB_UPLOAD\",\"status\":\"DONE\",\"updatedAt\":\"2020-07-15T21:32:58.570Z\"},\"verification\":{\"mrzCheck\":\"NOT_AVAILABLE\",\"rejectReason\":{\"rejectReasonCode\":\"100\",\"rejectReasonDescription\":\"MANIPULATED_DOCUMENT\",\"rejectReasonDetails\":[{\"detailsCode\":\"1007\",\"detailsDescription\":\"SECURITY_CHECKS\"},{\"detailsCode\":\"1001\",\"detailsDescription\":\"PHOTO\"}]}}}";
                dr = JsonConvert.DeserializeObject<Biometrika.Jumio.Api.DataResponse>(s);
                if (dr != null && dr.verification.rejectReason != null && dr.verification.rejectReason.rejectReasonDetails != null)
                {
                    //try
                    //{
                    //    List<Biometrika.Jumio.Api.RejectReasonDetail> l =
                    //              (List<Biometrika.Jumio.Api.RejectReasonDetail>)dr.verification.rejectReason.rejectReasonDetails;
                    //}
                    //catch (Exception ex)
                    //{

                    //    LOG.Error(" Error: " + ex.Message);
                    //}
                }
            }
            catch (Exception ex)
            {

                string e = " Error: " + ex.Message;
            }
        }

       
    }
}
