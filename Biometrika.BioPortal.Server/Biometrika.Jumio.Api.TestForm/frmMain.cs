﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp.WinForms;
using CefSharp;

namespace Biometrika.Jumio.Api.TestForm
{
    public partial class frmMain : Form
    {
        public ChromiumWebBrowser chromeBrowser;

        Biometrika.Jumio.Api.JumioClient JUMIO_CLIENT;

        public frmMain()
        {
            InitializeComponent();

            
        }

        private void button1_ClickAsync(object sender, EventArgs e)
        {
            

            
        }
       

        private void button1_Click(object sender, EventArgs e)
        {   
            JUMIO_CLIENT = new JumioClient("https://netverify.com/api/v4/", "https://netverify.com/api/authentication/v1/web/",
                                            "https://netverify.com/api/netverify/v2/scans/", 
                                            "https://netverify.com/api/netverify/v2/authentications/", 60000,
                                            "3197f9ae-9d8a-43a0-9d33-5ac873e09a52", "xQY2RccapX85Lru30pLRyrW93UgNiQeO");

            InitiateResponse response = JUMIO_CLIENT.initiateEnrollSync("bk1", "TestForm1", null);

            if (response != null)
            {
                txtTrackId3ro.Text = response.TransactionReference;
                richTextBox1.Text = response.RedirectUrl;

            }
            else
            {
                richTextBox1.Text = "Response NULL";
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            JUMIO_CLIENT = new JumioClient("https://netverify.com/api/v4/", "https://netverify.com/api/authentication/v1/web/",
                                            "https://netverify.com/api/netverify/v2/scans/", 
                                            "https://netverify.com/api/netverify/v2/authentications/", 60000,
                                            "3197f9ae-9d8a-43a0-9d33-5ac873e09a52", "xQY2RccapX85Lru30pLRyrW93UgNiQeO");

            InitiateResponse response = JUMIO_CLIENT.initiateAuthSync(txtTrackId3roForAuth.Text, txtUserReference.Text , "https://localhost.com/callback");

            if (response != null)
            {
                //txtTrackId3roForAuth.Text = response.TransactionReference;
                richTextBox2.Text = response.RedirectUrl;
                txtTrackId3roAuth.Text = response.TransactionReference;
            }
            else
            {
                richTextBox1.Text = "Response NULL";
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            JUMIO_CLIENT = new JumioClient("https://netverify.com/api/v4/", "https://netverify.com/api/authentication/v1/web/",
                                            "https://netverify.com/api/netverify/v2/scans/", 
                                            "https://netverify.com/api/netverify/v2/authentications/", 60000,
                                            "3197f9ae-9d8a-43a0-9d33-5ac873e09a52", "xQY2RccapX85Lru30pLRyrW93UgNiQeO");

            //object response = null;
            if (rbGet.Checked)
            {
                StatusResponse response = JUMIO_CLIENT.scansStatusSync(txtTrackId3roGet.Text, null);
                if (response != null)
                {
                    rtbResultImage.Text = response.Timestamp + " - " + response.Status;
                    
                }
                else
                {
                    rtbResultImage.Text = "Response NULL";
                }
            }
        

            if (rbGet1.Checked)
            {
                DataResponse response = null;
                DataAuthResponse o = null;
                if (chkIsAuth.Checked) o = JUMIO_CLIENT.scansDataAuthSync(txtTrackId3roGet.Text, null);
                else response = JUMIO_CLIENT.scansDataSync(txtTrackId3roGet.Text, null);
                if ((!chkIsAuth.Checked && response != null) || (chkIsAuth.Checked && o != null))
                {
                    
                    if (chkIsAuth.Checked)
                    {
                        rtbResultImage.Text = "RESULT = " + o.transactionResult;
                        byte[] byImage = JUMIO_CLIENT.downloadAuthSync(txtTrackId3roGet.Text, txtClasiffier.Text);

                        if (byImage != null)
                        {
                            System.IO.MemoryStream ms = new System.IO.MemoryStream(byImage);
                            chkIsAuthentication.Image = System.Drawing.Image.FromStream(ms);
                            ms.Close();
                        }
                    } else
                    {
                        rtbResultImage.Text = response.document.firstName + " - " + response.document.lastName;
                    }
                }
                else
                {
                    rtbResultImage.Text = "Response NULL";
                }
            }
            if (rbGet2.Checked)
            {
                ImagesResponse response = JUMIO_CLIENT.scansImageSync(txtTrackId3roGet.Text, null);
                if (response != null)
                {
                    rtbResultImage.Text = response.scanReference + " - " + response.images.Count.ToString();
                    byte[] byImage = JUMIO_CLIENT.downloadSync(txtTrackId3roGet.Text, txtClasiffier.Text);
                    System.IO.File.WriteAllBytes(@"c:\tmp\selfie_ggs_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".jpg", byImage);
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(byImage);
                    chkIsAuthentication.Image = System.Drawing.Image.FromStream(ms);
                    ms.Close();
                }
                else
                {
                    rtbResultImage.Text = "Response NULL";
                }
            }
            if (rbGet1.Checked)
            {
                DataResponse response = null;
                byte[] o = null;
                o = JUMIO_CLIENT.downloadFacemapSync(txtTrackId3roGet.Text, "facemap");

                rtbResultImage.Text = "RESULT = " + ((o == null || o.Length == 0) ? "facemap Nulo" : o.Length.ToString());
               

                if (o != null && o.Length > 0)
                {
                    System.IO.File.WriteAllBytes(@"c:\tmp\facemap_ggs_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".facemap", o);
                }
            }                 
        }


        private void button3_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(richTextBox1.Text) || string.IsNullOrEmpty(txtTrackId3ro.Text))
            {
                MessageBox.Show("Debe inicar el proceso con el boton Init...");
            }
            else
            {
                frmBrowser frmB = new frmBrowser();
                frmB._trackid3ro = txtTrackId3ro.Text;
                frmB._url = richTextBox1.Text;
                frmB.Show(this);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            txtTrackId3ro.Text = txtTrackId3ro.Text;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(richTextBox2.Text) || string.IsNullOrEmpty(txtTrackId3roForAuth.Text))
            {
                MessageBox.Show("Debe inicar el proceso con el boton Init...");
            }
            else
            {
                frmBrowser frmB = new frmBrowser();
                frmB._trackid3ro = txtTrackId3roForAuth.Text;
                frmB._url = richTextBox2.Text;
                frmB.Show(this);
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }
    }
}
