﻿using Microsoft.AspNetCore.Identity;

namespace Bio.Web.Certification.Server.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}