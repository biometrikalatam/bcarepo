﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using RestSharp;

namespace NvProvidersTestCSharp
{
    public class NvProviderHelper
    {
        protected string _Url_Base = null;
        protected IRestClient Client;

        /// <summary>
        /// Constructor, en su instanciación configura el host de conección
        /// </summary>
        /// <param name="host">URI de la API NotarioVirtual</param>
        public NvProviderHelper(string host)
        {
            _Url_Base = host;
            Client = new RestClient(host);
        }


        internal string Notarize(string trackid, int tramiteid, string document, string evidencia, out int code, out string msgerr)
        {
            string ret = null;
            code = 0;
            msgerr = null;
            try
            {
                var request = new RestRequest("Notarize", Method.POST);
                //if (customTimeOut > 0)
                //{
                //    request.Timeout = customTimeOut;
                //    request.AddUrlSegment("customTimeOut", customTimeOut.ToString());
                //}


                Evidence ev = new Evidence();
                ev.name = "Certificacion de Identidad";
                ev.type = 4;
                ev.value = evidencia;
                NotarizeProviderRequestModel req = new NotarizeProviderRequestModel();

                List<Evidence> listEV = new List<Evidence>();
                listEV.Add(ev);
                req.evidence = listEV;
                req.document = document;
                req.tramiteId = 1;
                req.trackId = trackid;

                request.AddJsonBody(req);
                try
                {
                    IRestResponse response = Client.Execute(request);
                    var content = response.Content;
                    NotarizeProviderResponsetModel obj = JsonConvert.DeserializeObject<NotarizeProviderResponsetModel>(content);
                    return content;
                }
                catch (Exception e)
                {
                    return "Error = " + e.Message;
                }
            }
            catch (Exception ex)
            {
                ret = null;
                //LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }
    }


    public class NotarizeProviderRequestModel
    {
        public string trackId { get; set; }
        public string rutnotaria { get; set; }
        public int tramiteId { get; set; }
        public string document { get; set; }
        public List<Evidence> evidence { get; set; }
    }

    public class Evidence
    {
        public string name { get; set; }
        public int type { get; set; }
        public string value { get; set; }

    }

    public class NotarizeProviderResponsetModel
    {
        public int code { get; set; }
        public string msgError { get; set; }
    }
}
