﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NvProvidersTestCSharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            cboProviders.SelectedIndex = 0;
        }

        private void cboProviders_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboProviders.SelectedIndex == 0)
            {
                txtURLBase.Text = "https://fea-ext.notarialeiva.cl:7070/ServicioREST/api/";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            NvProviderHelper helper = new NvProviderHelper(txtURLBase.Text);

            string sret = helper.Notarize(txtTrackId.Text, Convert.ToInt32(txtTramiteId.Text), 
                                          rtbDocumento.Text, rtbEvidencia.Text,
                                          out int code, out string msgerr);

            rtbResult.Text = sret;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtTrackId.Text = Guid.NewGuid().ToString("N");
        }
    }
}
