﻿namespace NvProvidersTestCSharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.cboProviders = new System.Windows.Forms.ComboBox();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.rtbEvidencia = new System.Windows.Forms.RichTextBox();
            this.rtbDocumento = new System.Windows.Forms.RichTextBox();
            this.txtTramiteId = new System.Windows.Forms.TextBox();
            this.txtTrackId = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.txtURLBase = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rtbResult = new System.Windows.Forms.RichTextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.GroupBox2.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.button2);
            this.GroupBox2.Controls.Add(this.button1);
            this.GroupBox2.Controls.Add(this.txtURLBase);
            this.GroupBox2.Controls.Add(this.label6);
            this.GroupBox2.Controls.Add(this.Label5);
            this.GroupBox2.Controls.Add(this.cboProviders);
            this.GroupBox2.Location = new System.Drawing.Point(432, 75);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(400, 501);
            this.GroupBox2.TabIndex = 3;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Providers";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(37, 49);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(46, 13);
            this.Label5.TabIndex = 1;
            this.Label5.Text = "Provider";
            // 
            // cboProviders
            // 
            this.cboProviders.FormattingEnabled = true;
            this.cboProviders.Items.AddRange(new object[] {
            "Gesnot",
            "APPA"});
            this.cboProviders.Location = new System.Drawing.Point(89, 46);
            this.cboProviders.Name = "cboProviders";
            this.cboProviders.Size = new System.Drawing.Size(284, 21);
            this.cboProviders.TabIndex = 0;
            this.cboProviders.SelectedIndexChanged += new System.EventHandler(this.cboProviders_SelectedIndexChanged);
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.button3);
            this.GroupBox1.Controls.Add(this.rtbEvidencia);
            this.GroupBox1.Controls.Add(this.rtbDocumento);
            this.GroupBox1.Controls.Add(this.txtTramiteId);
            this.GroupBox1.Controls.Add(this.txtTrackId);
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Location = new System.Drawing.Point(26, 75);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(400, 585);
            this.GroupBox1.TabIndex = 2;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Datos para Notariar";
            // 
            // rtbEvidencia
            // 
            this.rtbEvidencia.Location = new System.Drawing.Point(20, 341);
            this.rtbEvidencia.Name = "rtbEvidencia";
            this.rtbEvidencia.Size = new System.Drawing.Size(360, 180);
            this.rtbEvidencia.TabIndex = 7;
            this.rtbEvidencia.Text = resources.GetString("rtbEvidencia.Text");
            // 
            // rtbDocumento
            // 
            this.rtbDocumento.Location = new System.Drawing.Point(18, 131);
            this.rtbDocumento.Name = "rtbDocumento";
            this.rtbDocumento.Size = new System.Drawing.Size(360, 180);
            this.rtbDocumento.TabIndex = 6;
            this.rtbDocumento.Text = resources.GetString("rtbDocumento.Text");
            // 
            // txtTramiteId
            // 
            this.txtTramiteId.Location = new System.Drawing.Point(74, 61);
            this.txtTramiteId.Name = "txtTramiteId";
            this.txtTramiteId.Size = new System.Drawing.Size(292, 20);
            this.txtTramiteId.TabIndex = 5;
            this.txtTramiteId.Text = "1";
            // 
            // txtTrackId
            // 
            this.txtTrackId.Location = new System.Drawing.Point(74, 30);
            this.txtTrackId.Name = "txtTrackId";
            this.txtTrackId.Size = new System.Drawing.Size(247, 20);
            this.txtTrackId.TabIndex = 4;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(24, 325);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(54, 13);
            this.Label4.TabIndex = 3;
            this.Label4.Text = "Evidencia";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(23, 115);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(62, 13);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "Documento";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(17, 64);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(51, 13);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "TramiteId";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(23, 33);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(44, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "TrackId";
            // 
            // txtURLBase
            // 
            this.txtURLBase.Location = new System.Drawing.Point(23, 105);
            this.txtURLBase.Name = "txtURLBase";
            this.txtURLBase.Size = new System.Drawing.Size(350, 20);
            this.txtURLBase.TabIndex = 9;
            this.txtURLBase.Text = "https://fea-ext.notarialeiva.cl:7070/ServicioREST/api/";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "URLBase";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(111, 168);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(134, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Notarize";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(111, 211);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(134, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Get Status Notarize";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rtbResult);
            this.groupBox3.Location = new System.Drawing.Point(894, 77);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(407, 519);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Resultado";
            // 
            // rtbResult
            // 
            this.rtbResult.Location = new System.Drawing.Point(19, 28);
            this.rtbResult.Name = "rtbResult";
            this.rtbResult.Size = new System.Drawing.Size(360, 452);
            this.rtbResult.TabIndex = 8;
            this.rtbResult.Text = "";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(327, 28);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(53, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "GUID";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1331, 766);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.GroupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "NVProviders Tool Test...";
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.ComboBox cboProviders;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.RichTextBox rtbEvidencia;
        internal System.Windows.Forms.RichTextBox rtbDocumento;
        internal System.Windows.Forms.TextBox txtTramiteId;
        internal System.Windows.Forms.TextBox txtTrackId;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txtURLBase;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.RichTextBox rtbResult;
        private System.Windows.Forms.Button button3;
    }
}

