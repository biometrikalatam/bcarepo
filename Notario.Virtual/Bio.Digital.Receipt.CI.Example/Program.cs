﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bio.Digital.Receipt.CI.Example
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            //WebRequest wrGETURL;
            //string sURL = "https://maps.googleapis.com/maps/api/staticmap?center=-33.3513061452878,-70.5044428392026&zoom=16" +
            //    "&size=400x300&markers=anchor:24,24%7Cicon:https://tinyurl.com/yyrgzv26%7C-33.3513061452878,-70.5044428392026" +
            //    "&key=AIzaSyCsNCxJprwolv_rncjZ8RJX3LHzkkOsqoA";
            ////wrGETURL = WebRequest.Create(sURL);
            //HttpWebRequest wrGETURL = (HttpWebRequest)WebRequest.Create(sURL);
            //System.Net.HttpWebResponse webresponse = (HttpWebResponse)wrGETURL.GetResponse();
            //string ct = webresponse.ContentType;
            ////WebProxy myProxy = new WebProxy("myproxy", 80);
            ////myProxy.BypassProxyOnLocal = true;

            ////wrGETURL.Proxy = WebProxy.GetDefaultProxy();

            //Stream objStream;
            //objStream = wrGETURL.GetResponse().GetResponseStream();

            //StreamReader objReader = new StreamReader(objStream);
            //BinaryReader breader = new BinaryReader(objStream);
            //byte[] buffer = ReadFully(objStream);
            //byte[] buffer = breader.ReadBytes((int)wrGETURL.ContentLength);

            //objReader.rea
            //string sLine = "";
            //string sTotal = "";
            int i = 0;
            //while (sLine != null)
            //{
            //    i++;
            //    sLine = objReader.ReadLine();
            //    if (sLine != null)
            //        sTotal = sTotal + sLine;
            //}
            //File.WriteAllText(@"c:\de\map.png", sTotal);
            //File.WriteAllBytes(@"c:\de\map.png", System.Text.ASCIIEncoding.ASCII.GetBytes(sTotal));
            //File.WriteAllBytes(@"c:\de\map.png", buffer);
            //string map = Convert.ToBase64String(File.ReadAllBytes(@"c:\de\map_default.png"));
            //i = 0;
        
            //string aux = GetPorcentajeScore(0.5456565, 0.9);
            //aux = GetPorcentajeScore(0, 0.9);
            //aux = GetPorcentajeScore(0.9, 0.9);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        private static string GetPorcentajeScore(double score, double treshold)
        {
            string ret = "0";
            try
            {

                double rts = (25 * (treshold - score)) / treshold;

                ret = (75 + rts).ToString("##.##") + "% / 100%";
            }
            catch (Exception ex)
            {
                ret = "75";

            }
            return ret;
        }

        private static byte[] ReadFully(Stream input)
        {
            try
            {
                int bytesBuffer = 1024;
                byte[] buffer = new byte[bytesBuffer];
                using (MemoryStream ms = new MemoryStream())
                {
                    int readBytes;
                    while ((readBytes = input.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, readBytes);
                    }
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                // Exception handling here:  Response.Write("Ex.: " + ex.Message);
                return null;
            }
        }
    }

   
}
