﻿namespace Bio.Digital.Receipt.CI.Example
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.gbCI = new System.Windows.Forms.GroupBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtRestURL = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.chkWS = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.rtbResulTx = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.picCedula = new System.Windows.Forms.PictureBox();
            this.picQRCheckCI = new System.Windows.Forms.PictureBox();
            this.picSelfie = new System.Windows.Forms.PictureBox();
            this.picFoto = new System.Windows.Forms.PictureBox();
            this.rtbResultado = new System.Windows.Forms.RichTextBox();
            this.txtCompanyId = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtValueid = new System.Windows.Forms.TextBox();
            this.txtTypeid = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtIntervalCheck = new System.Windows.Forms.TextBox();
            this.btnStopCheckCI = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.txtAppId = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtURLCI = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.btnStartCheck = new System.Windows.Forms.Button();
            this.btnCIBorrar = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.labChecking = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTokenQRCI = new System.Windows.Forms.TextBox();
            this.picTokenQRCI = new System.Windows.Forms.PictureBox();
            this.button8 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.timerRest = new System.Windows.Forms.Timer(this.components);
            this.timerRest2 = new System.Windows.Forms.Timer(this.components);
            this.gbCI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCedula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQRCheckCI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSelfie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTokenQRCI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // gbCI
            // 
            this.gbCI.Controls.Add(this.button6);
            this.gbCI.Controls.Add(this.button5);
            this.gbCI.Controls.Add(this.label15);
            this.gbCI.Controls.Add(this.label16);
            this.gbCI.Controls.Add(this.txtRestURL);
            this.gbCI.Controls.Add(this.label14);
            this.gbCI.Controls.Add(this.chkWS);
            this.gbCI.Controls.Add(this.button4);
            this.gbCI.Controls.Add(this.button3);
            this.gbCI.Controls.Add(this.label13);
            this.gbCI.Controls.Add(this.label12);
            this.gbCI.Controls.Add(this.rtbResulTx);
            this.gbCI.Controls.Add(this.label8);
            this.gbCI.Controls.Add(this.label6);
            this.gbCI.Controls.Add(this.label7);
            this.gbCI.Controls.Add(this.label5);
            this.gbCI.Controls.Add(this.picCedula);
            this.gbCI.Controls.Add(this.picQRCheckCI);
            this.gbCI.Controls.Add(this.picSelfie);
            this.gbCI.Controls.Add(this.picFoto);
            this.gbCI.Controls.Add(this.rtbResultado);
            this.gbCI.Controls.Add(this.txtCompanyId);
            this.gbCI.Controls.Add(this.label11);
            this.gbCI.Controls.Add(this.txtValueid);
            this.gbCI.Controls.Add(this.txtTypeid);
            this.gbCI.Controls.Add(this.label3);
            this.gbCI.Controls.Add(this.label4);
            this.gbCI.Controls.Add(this.button1);
            this.gbCI.Controls.Add(this.label2);
            this.gbCI.Controls.Add(this.label1);
            this.gbCI.Controls.Add(this.txtIntervalCheck);
            this.gbCI.Controls.Add(this.btnStopCheckCI);
            this.gbCI.Controls.Add(this.label27);
            this.gbCI.Controls.Add(this.txtAppId);
            this.gbCI.Controls.Add(this.label25);
            this.gbCI.Controls.Add(this.label24);
            this.gbCI.Controls.Add(this.txtURLCI);
            this.gbCI.Controls.Add(this.label22);
            this.gbCI.Controls.Add(this.btnStartCheck);
            this.gbCI.Controls.Add(this.btnCIBorrar);
            this.gbCI.Controls.Add(this.label21);
            this.gbCI.Controls.Add(this.txtMail);
            this.gbCI.Controls.Add(this.labChecking);
            this.gbCI.Controls.Add(this.label20);
            this.gbCI.Controls.Add(this.txtTokenQRCI);
            this.gbCI.Controls.Add(this.picTokenQRCI);
            this.gbCI.Controls.Add(this.button8);
            this.gbCI.Location = new System.Drawing.Point(23, 118);
            this.gbCI.Name = "gbCI";
            this.gbCI.Size = new System.Drawing.Size(1282, 600);
            this.gbCI.TabIndex = 156;
            this.gbCI.TabStop = false;
            this.gbCI.Text = "CI via NV...";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.button6.Location = new System.Drawing.Point(22, 228);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(224, 38);
            this.button6.TabIndex = 217;
            this.button6.Text = "CI Sin Pasos Facial";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(251, 89);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(79, 19);
            this.button5.TabIndex = 216;
            this.button5.Text = "Rest API";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(364, 89);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 13);
            this.label15.TabIndex = 215;
            this.label15.Text = "Prod";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(336, 89);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(22, 13);
            this.label16.TabIndex = 214;
            this.label16.Text = "QA";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // txtRestURL
            // 
            this.txtRestURL.Location = new System.Drawing.Point(65, 66);
            this.txtRestURL.Name = "txtRestURL";
            this.txtRestURL.Size = new System.Drawing.Size(345, 20);
            this.txtRestURL.TabIndex = 213;
            this.txtRestURL.Text = "http://qaservicewebapi.enotario.cl";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 69);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 13);
            this.label14.TabIndex = 212;
            this.label14.Text = "URL WS:";
            // 
            // chkWS
            // 
            this.chkWS.AutoSize = true;
            this.chkWS.Location = new System.Drawing.Point(453, 249);
            this.chkWS.Name = "chkWS";
            this.chkWS.Size = new System.Drawing.Size(62, 17);
            this.chkWS.TabIndex = 211;
            this.chkWS.Text = "Via WS";
            this.chkWS.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(622, 174);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 23);
            this.button4.TabIndex = 210;
            this.button4.Text = "Get CI [Rest]";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(475, 64);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(117, 23);
            this.button3.TabIndex = 209;
            this.button3.Text = "Get Token CI [Rest]";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 283);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 13);
            this.label13.TabIndex = 208;
            this.label13.Text = "Log de Operación";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(807, 283);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 207;
            this.label12.Text = "Datos Tx";
            // 
            // rtbResulTx
            // 
            this.rtbResulTx.BackColor = System.Drawing.Color.PapayaWhip;
            this.rtbResulTx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbResulTx.Location = new System.Drawing.Point(777, 298);
            this.rtbResulTx.Margin = new System.Windows.Forms.Padding(2);
            this.rtbResulTx.Name = "rtbResulTx";
            this.rtbResulTx.ReadOnly = true;
            this.rtbResulTx.Size = new System.Drawing.Size(321, 280);
            this.rtbResulTx.TabIndex = 206;
            this.rtbResulTx.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1138, 441);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 13);
            this.label8.TabIndex = 205;
            this.label8.Text = "QR Verificación CI";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1065, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 204;
            this.label6.Text = "Selfie";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1065, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 203;
            this.label7.Text = "Foto desde Cedula";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(807, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 201;
            this.label5.Text = "Cedula";
            // 
            // picCedula
            // 
            this.picCedula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picCedula.Location = new System.Drawing.Point(794, 60);
            this.picCedula.Margin = new System.Windows.Forms.Padding(2);
            this.picCedula.Name = "picCedula";
            this.picCedula.Size = new System.Drawing.Size(247, 169);
            this.picCedula.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCedula.TabIndex = 200;
            this.picCedula.TabStop = false;
            // 
            // picQRCheckCI
            // 
            this.picQRCheckCI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picQRCheckCI.Location = new System.Drawing.Point(1138, 456);
            this.picQRCheckCI.Margin = new System.Windows.Forms.Padding(2);
            this.picQRCheckCI.Name = "picQRCheckCI";
            this.picQRCheckCI.Size = new System.Drawing.Size(118, 108);
            this.picQRCheckCI.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picQRCheckCI.TabIndex = 199;
            this.picQRCheckCI.TabStop = false;
            // 
            // picSelfie
            // 
            this.picSelfie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picSelfie.Location = new System.Drawing.Point(1058, 167);
            this.picSelfie.Margin = new System.Windows.Forms.Padding(2);
            this.picSelfie.Name = "picSelfie";
            this.picSelfie.Size = new System.Drawing.Size(125, 113);
            this.picSelfie.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSelfie.TabIndex = 198;
            this.picSelfie.TabStop = false;
            // 
            // picFoto
            // 
            this.picFoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFoto.Location = new System.Drawing.Point(1058, 33);
            this.picFoto.Margin = new System.Windows.Forms.Padding(2);
            this.picFoto.Name = "picFoto";
            this.picFoto.Size = new System.Drawing.Size(125, 113);
            this.picFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFoto.TabIndex = 197;
            this.picFoto.TabStop = false;
            // 
            // rtbResultado
            // 
            this.rtbResultado.BackColor = System.Drawing.Color.PapayaWhip;
            this.rtbResultado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbResultado.Location = new System.Drawing.Point(9, 298);
            this.rtbResultado.Margin = new System.Windows.Forms.Padding(2);
            this.rtbResultado.Name = "rtbResultado";
            this.rtbResultado.Size = new System.Drawing.Size(732, 280);
            this.rtbResultado.TabIndex = 196;
            this.rtbResultado.Text = "";
            // 
            // txtCompanyId
            // 
            this.txtCompanyId.Location = new System.Drawing.Point(115, 148);
            this.txtCompanyId.Name = "txtCompanyId";
            this.txtCompanyId.Size = new System.Drawing.Size(45, 20);
            this.txtCompanyId.TabIndex = 194;
            this.txtCompanyId.Text = "1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(46, 151);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 13);
            this.label11.TabIndex = 195;
            this.label11.Text = "Company Id:";
            // 
            // txtValueid
            // 
            this.txtValueid.Location = new System.Drawing.Point(263, 176);
            this.txtValueid.Name = "txtValueid";
            this.txtValueid.Size = new System.Drawing.Size(94, 20);
            this.txtValueid.TabIndex = 192;
            this.txtValueid.Text = "21284415-2";
            // 
            // txtTypeid
            // 
            this.txtTypeid.Location = new System.Drawing.Point(263, 149);
            this.txtTypeid.Name = "txtTypeid";
            this.txtTypeid.Size = new System.Drawing.Size(45, 20);
            this.txtTypeid.TabIndex = 190;
            this.txtTypeid.Text = "RUT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(214, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 191;
            this.label3.Text = "Type Id";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(212, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 193;
            this.label4.Text = "Value id";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(516, 173);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 189;
            this.button1.Text = "Get CI [WS]";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(570, 228);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 187;
            this.label2.Text = "(milisegundos)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(450, 228);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 186;
            this.label1.Text = "Intervalo";
            // 
            // txtIntervalCheck
            // 
            this.txtIntervalCheck.Location = new System.Drawing.Point(504, 225);
            this.txtIntervalCheck.Name = "txtIntervalCheck";
            this.txtIntervalCheck.Size = new System.Drawing.Size(60, 20);
            this.txtIntervalCheck.TabIndex = 185;
            this.txtIntervalCheck.Text = "8000";
            // 
            // btnStopCheckCI
            // 
            this.btnStopCheckCI.Location = new System.Drawing.Point(548, 266);
            this.btnStopCheckCI.Name = "btnStopCheckCI";
            this.btnStopCheckCI.Size = new System.Drawing.Size(95, 23);
            this.btnStopCheckCI.TabIndex = 184;
            this.btnStopCheckCI.Text = "Stop Check CI";
            this.btnStopCheckCI.UseVisualStyleBackColor = true;
            this.btnStopCheckCI.Click += new System.EventHandler(this.btnStopCheckCI_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(68, 178);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 13);
            this.label27.TabIndex = 183;
            this.label27.Text = "App Id:";
            // 
            // txtAppId
            // 
            this.txtAppId.Location = new System.Drawing.Point(115, 178);
            this.txtAppId.Name = "txtAppId";
            this.txtAppId.Size = new System.Drawing.Size(60, 20);
            this.txtAppId.TabIndex = 182;
            this.txtAppId.Text = "7";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(364, 45);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 13);
            this.label25.TabIndex = 178;
            this.label25.Text = "Prod";
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(336, 45);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(22, 13);
            this.label24.TabIndex = 177;
            this.label24.Text = "QA";
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // txtURLCI
            // 
            this.txtURLCI.Location = new System.Drawing.Point(65, 22);
            this.txtURLCI.Name = "txtURLCI";
            this.txtURLCI.Size = new System.Drawing.Size(345, 20);
            this.txtURLCI.TabIndex = 172;
            this.txtURLCI.Text = "http://qaciservice.biometrikalatam.com/WSCI.asmx";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 25);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 13);
            this.label22.TabIndex = 171;
            this.label22.Text = "URL WS:";
            // 
            // btnStartCheck
            // 
            this.btnStartCheck.Location = new System.Drawing.Point(446, 266);
            this.btnStartCheck.Name = "btnStartCheck";
            this.btnStartCheck.Size = new System.Drawing.Size(95, 23);
            this.btnStartCheck.TabIndex = 170;
            this.btnStartCheck.Text = "Start Check CI";
            this.btnStartCheck.UseVisualStyleBackColor = true;
            this.btnStartCheck.Click += new System.EventHandler(this.btnStartCheck_Click);
            // 
            // btnCIBorrar
            // 
            this.btnCIBorrar.Location = new System.Drawing.Point(715, 0);
            this.btnCIBorrar.Name = "btnCIBorrar";
            this.btnCIBorrar.Size = new System.Drawing.Size(89, 23);
            this.btnCIBorrar.TabIndex = 168;
            this.btnCIBorrar.Text = "Borrar";
            this.btnCIBorrar.UseVisualStyleBackColor = true;
            this.btnCIBorrar.Click += new System.EventHandler(this.btnCIBorrar_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(231, 207);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(26, 13);
            this.label21.TabIndex = 166;
            this.label21.Text = "Mail";
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(263, 202);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(151, 20);
            this.txtMail.TabIndex = 165;
            this.txtMail.Text = "gsuhit@gmail.com";
            // 
            // labChecking
            // 
            this.labChecking.AutoSize = true;
            this.labChecking.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labChecking.Location = new System.Drawing.Point(446, 207);
            this.labChecking.Name = "labChecking";
            this.labChecking.Size = new System.Drawing.Size(126, 13);
            this.labChecking.TabIndex = 164;
            this.labChecking.Text = "Check CI Recurrente";
            this.labChecking.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(450, 135);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(73, 13);
            this.label20.TabIndex = 160;
            this.label20.Text = "Token QR CI ";
            // 
            // txtTokenQRCI
            // 
            this.txtTokenQRCI.Location = new System.Drawing.Point(444, 151);
            this.txtTokenQRCI.Name = "txtTokenQRCI";
            this.txtTokenQRCI.Size = new System.Drawing.Size(278, 20);
            this.txtTokenQRCI.TabIndex = 159;
            // 
            // picTokenQRCI
            // 
            this.picTokenQRCI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picTokenQRCI.Location = new System.Drawing.Point(597, 33);
            this.picTokenQRCI.Margin = new System.Windows.Forms.Padding(2);
            this.picTokenQRCI.Name = "picTokenQRCI";
            this.picTokenQRCI.Size = new System.Drawing.Size(125, 113);
            this.picTokenQRCI.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picTokenQRCI.TabIndex = 157;
            this.picTokenQRCI.TabStop = false;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(475, 35);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(117, 23);
            this.button8.TabIndex = 0;
            this.button8.Text = "Get Token CI [WS]";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 200;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::Bio.Digital.Receipt.CI.Example.Properties.Resources.CI_Logo;
            this.pictureBox2.Location = new System.Drawing.Point(32, 11);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(280, 102);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 159;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::Bio.Digital.Receipt.CI.Example.Properties.Resources.NV2_RGB_CF;
            this.pictureBox1.Location = new System.Drawing.Point(1154, 61);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(151, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 158;
            this.pictureBox1.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1224, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 13);
            this.label9.TabIndex = 204;
            this.label9.Text = "Un producto de";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(329, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(762, 72);
            this.label10.TabIndex = 205;
            this.label10.Text = resources.GetString("label10.Text");
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1216, 727);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 23);
            this.button2.TabIndex = 209;
            this.button2.Text = "Salir";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // timerRest
            // 
            this.timerRest.Interval = 5000;
            this.timerRest.Tick += new System.EventHandler(this.timerRest_Tick);
            // 
            // timerRest2
            // 
            this.timerRest2.Interval = 200;
            this.timerRest2.Tick += new System.EventHandler(this.timerRest2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1336, 762);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.gbCI);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Certificación de Identidad - Ejemplo de Integración...";
            this.gbCI.ResumeLayout(false);
            this.gbCI.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCedula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQRCheckCI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSelfie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTokenQRCI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCI;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtAppId;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtURLCI;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnStartCheck;
        private System.Windows.Forms.Button btnCIBorrar;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.Label labChecking;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtTokenQRCI;
        private System.Windows.Forms.PictureBox picTokenQRCI;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button btnStopCheckCI;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIntervalCheck;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.RichTextBox rtbResulTx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox picCedula;
        private System.Windows.Forms.PictureBox picQRCheckCI;
        private System.Windows.Forms.PictureBox picSelfie;
        private System.Windows.Forms.PictureBox picFoto;
        private System.Windows.Forms.RichTextBox rtbResultado;
        private System.Windows.Forms.TextBox txtCompanyId;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtValueid;
        private System.Windows.Forms.TextBox txtTypeid;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox chkWS;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Timer timerRest;
        private System.Windows.Forms.Timer timerRest2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtRestURL;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
    }
}

