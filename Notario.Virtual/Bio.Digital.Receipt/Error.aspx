<%@ Page Language="C#" MasterPageFile="~/RDMasterPageRoot.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="Bio.Digital.Receipt.Error" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <br />
    <br />
    <br />
    <div style="text-align: center">
        <table align="center" style="border-color:#346735; border-style:solid; border-width:1px; vertical-align:middle;">
            <tr>
                <td style=" background-color: #346734;font-size: 14pt; color: White; font-family: Verdana" colspan="2">
                    <span ><strong>Atenci�n</strong></span>
                </td>
            </tr>    
            <tr>
                <td style="width: 20%">
                    <img src="images/Error-Shield.png" height="120" width="120" /></td>
                <td style="width: 80%">
                    <asp:Label ID="labMsg" runat="server" Font-Names="Verdana,Tahoma,Arial" Font-Size="Medium"
                        ForeColor="#346735" Text="Se ha producido un error inesperado. Reinicie..."></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 20%">
                </td>
                <td style="width: 80%">
                </td>
            </tr>
            <tr>
                <td style="width: 20%">
                </td>
                <td style="width: 80%">
                    <asp:Button ID="btnVolver" runat="server" CssClass="boton1140" Text="Volver" OnClick="btnVolver_Click" />
                <br /><br />
                </td>
            <tr style="height:5px;">
                <td style=" background-color: #346734;font-size: 14pt; color: White; font-family: Verdana" colspan="2">
                </td>
            </tr>    
                    
            </tr>
        </table>
    </div>
</asp:Content>
