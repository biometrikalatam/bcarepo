using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Bio.Digital.Receipt.Core.Database;

namespace Bio.Digital.Receipt.Manager.pages.documentos
{
    public partial class resultado_recibos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RDMasterPage.SetMasterPage(Master, Request, null, null,
               "Usuario Conectado : " + (string)Session["userconnected"] + "   ", "Resultado Busqueda Recibos",
               "MD > Recibos > Resultado B�squeda Recibos");

            string fechadesde = Request.Form["ctl00$ContentPlaceHolderMain$labFechaDesde"];
            DateTime dtfechadesde = libs.Utils.GetDateTime(fechadesde, true);
            string fechahasta = Request.Form["ctl00$ContentPlaceHolderMain$labFechaHasta"];
            DateTime dtfechahasta = libs.Utils.GetDateTime(fechahasta, false);
            string aplicacion = Request.Form["ctl00$ContentPlaceHolderMain$cboAplicaciones"];
            int idapp = 0;
            if (!String.IsNullOrEmpty(aplicacion)) idapp = Convert.ToInt32(aplicacion);
            string reciboid = Request.Form["ctl00$ContentPlaceHolderMain$txtReciboId"];
            
            IList<RdRecibos> listaRecibos =
                    AdministradorRdRecibos
                        .BuscarRecibosByFilter(dtfechadesde, dtfechahasta, reciboid, idapp);

            GridView1.DataSource = listaRecibos;
            GridView1.DataBind();

        }

 
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Response.Redirect("buscar_recibos.aspx");
        }
    }
}
