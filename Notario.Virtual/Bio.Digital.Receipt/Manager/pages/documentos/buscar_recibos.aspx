<%@ Page Language="C#" MasterPageFile="~/Manager/RDMasterPage.master" AutoEventWireup="true" CodeBehind="buscar_recibos.aspx.cs" Inherits="Bio.Digital.Receipt.Manager.pages.documentos.buscar_recibos" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">



    <br />

    <table width="90%" align="center" border="0">
        <tr>
            <td align="left" valign="middle" colspan="4" class="titleform">
                Ingrese Filtros
            </td>
        </tr>
        <tr>
        <td align="left" valign="middle" colspan="4">
                <br />
               
            </td>
        </tr>
        <tr>
            <td align="right" valign="middle" class="labelform" style="width:23%">
                Fecha Desde :<br />
            </td>
            <td align="left" valign="middle" style="width:29%">
                <asp:TextBox ID="labFechaDesde" runat="server" CssClass="textboxform160"></asp:TextBox><br />
            </td>
            <td align="right" valign="middle" class="labelform" style="width:18%">
                Fecha Hasta :<br />
            </td>
            <td align="left" valign="middle" style="width:35%">
                <asp:TextBox ID="labFechaHasta" runat="server" CssClass="textboxform160"></asp:TextBox> 
                <br />
            </td>                
        </tr>
        <tr>
            <td align="right" valign="middle" class="labelform" style="width:23%">
                Recibo Id :
            </td>
            <td align="left" valign="middle" style="width:29%">
                <asp:TextBox ID="txtReciboId" CssClass="textboxform160" runat="server"></asp:TextBox></td>
            <td align="right" valign="middle" class="labelform" style="width:18%">
                Aplicación :
            </td>
            <td align="left" valign="middle" style="width:35%">
                <asp:DropDownList ID="cboAplicaciones" runat="server" CssClass="textboxform160" Width="184px">
                    <asp:ListItem Value="0">Seleccionar Aplicaci&#243;n...</asp:ListItem>
                </asp:DropDownList></td>                
        </tr>
        <tr>
            <td align="right" valign="middle" colspan="4">
                <br />
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar Recibo" CssClass="boton1140" OnClick="btnBuscar_Click" /></td>
        </tr>        
    </table>    

</asp:Content>
