<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Recibo_Digital.aspx.cs" Inherits="Bio.Digital.Receipt.Manager.pages.documentos.recibo_digital" %>
<%@ Import namespace="Bio.Digital.Receipt.Manager.database"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>BioPortal Document Manager - Recibo Digital</title>
    
</head>
<BODY leftmargin="0" bottommargin="0" 
	marginheight="0" marginwidth="0" rightmargin="0" topmargin="0" style="font-family:Verdana, Arial;"
	onload="Javascript:window.resizeTo(650,240);">

<div align="center">
  <center>
  <table border="1" width="100%" bordercolor=White bgcolor=#D9FFD9>
    <tr>
      <td width="100%" colspan=2 align=center style="background-color:#356734;">
      <b><font size="5" face="Verdana" style="color:White;">RD - Recibo Digital</font></b>
  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<img border="0" src="../../images/medal_bronze_1.png" width="19" height="17">
	  </td>
   	</tr>
    
    <tr>
      <td>
      
        <div align="center">
         <table border="0" width="100%">
            <tr>
               <tr>
              <td width="20%" align="right"><b><font face="Verdana">Fecha:&nbsp;</font></b></td>
              <td width="20%" align="left">&nbsp;28/09/2009<font face="Verdana"></td>
              <td width="25%" align="right"><b><font size="3" face="Verdana">Time Stamp:&nbsp;</font></b></td>
	          <td width="35%">&nbsp;28/09/2009 10:00:30<font face="Verdana"></font>&nbsp; <font size="3" face="Verdana"><b></b> </td>
            </tr>
            <tr>
              <td width="20%" align="right"><b><font face="Verdana">Descripcion:&nbsp;</font></b></td>
              <td width="40%" colspan="2">
                  Contrato de trabajo del Gerente Comercial&nbsp;<font face="Verdana"></td>
              <td width="20%">&nbsp;</td>
            </tr>
            <tr>
              <td width="20%" align="right"><b><font face="Verdana">Vendedor:&nbsp;</font></b></td>
              <td width="20%">
                  Juan Perez&nbsp;<font face="Verdana"></td>
              <td width="20%" align="right"><b><font face="Verdana">Cliente:&nbsp;</font></b></td>
              <td width="20%">
                  Codelco&nbsp;<font face="Verdana"></td>
            </tr>
		    <tr><td colspan=4>&nbsp;</td>
            </tr>
            <tr border=1 bgcolor=black height=1><td colspan=4></td>
            </tr>
            <tr>
              <td width="20%" align="right"><b><font face="Verdana">Certificador:&nbsp;</font></b></td>
              <td width="60%" colspan="3" rowspan="2">&nbsp;<font face="Verdana" size=2> CN=Biometrika,OU=BK,O=BioK,L=Santiago
                  De Chile,ST=Region Metropolitana,C=CL,EMAIL=notariovirtual@biometrika.cl
			  </td>
            </tr>          
          </table>
          
        </div>
      </td>
    </tr>
    
  </table>
  </center>
</div>


</BODY>
</html>
