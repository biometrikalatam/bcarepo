using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Bio.Digital.Receipt.Core.Database;

namespace Bio.Digital.Receipt.Manager.pages.documentos
{
    public partial class buscar_recibos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RDMasterPage.SetMasterPage(Master, Request, null, null,
                "Usuario Conectado : " + (string)Session["userconnected"] + "   ", "Busqueda Recibos",
                "RD > Recibos > B�squeda Recibos");

            if (!IsPostBack)
            {
                //Cargo Combo
                IList<RdAplicaciones> listApp = AdministradorRdAplicaciones.BuscarAplicaciones();
                ListItem item = null;
                foreach (RdAplicaciones app in listApp)
                {
                    item = new ListItem();
                    item.Value = app.Id.ToString();
                    item.Value = app.Nombre;
                    cboAplicaciones.Items.Add(item);
                }
                return;
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Server.Transfer("~/Manager/pages/documentos/resultado_recibos.aspx",true);
        }
 
    }
}
