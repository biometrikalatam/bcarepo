<%@ Page Language="C#" MasterPageFile="~/Manager/RDMasterPage.master" AutoEventWireup="true" CodeBehind="resultado_recibos.aspx.cs" Inherits="Bio.Digital.Receipt.Manager.pages.documentos.resultado_recibos" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
<script type="text/javascript" language="javascript">

function ShowRecibo(id) {

    window.open('../../../CheckRDResult.aspx?id=' + id,'Recibo_Digital','top=0,left=0,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,fullscreen=yes');

}

function ShowDocumento(id) {

    window.open('download_documento.aspx?id=' + id,'Documento','width=1,height=1,top=2000,left=2000,scrollbars=no,titlebar=no,');

}

</script>

    <br />

    <table width="95%" align="center" border="0">
         <tr>
            <td align="right" valign="middle" colspan="4">
                <asp:Button ID="btnBuscar" runat="server" Text="Volver" CssClass="boton1140" OnClick="btnBuscar_Click" /><br />
            </td>
        </tr>   
        <tr>
            <td align="left" valign="middle" colspan="4" class="titleform">
                Recibos Encontrados
            </td>
        </tr>
        <tr>
            <td align="left" valign="middle" colspan="4">
                <br />
                <asp:GridView ID="GridView1" SkinID="GridView1" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        
                        <asp:BoundField DataField="Id" HeaderText="Id" />
                        <asp:BoundField DataField="Reciboid" HeaderText="Recibo ID" />
                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" />
                        <asp:TemplateField HeaderText="Aplicación">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("RdAplicaciones.Nombre") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("RdAplicaciones.Nombre") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" />
                        <asp:BoundField DataField="Iporigen" HeaderText="IP Origen" />
                        <asp:TemplateField>
                          <ItemTemplate>
                            <a href="javascript:ShowRecibo('<%# Eval("Id")%>')">
                                <img alt="Recibo Digital..." style="border:0;" src="../../images/medal_gold_2.png" /> 
                             </a>
                          </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
               
            </td>
        </tr>     
        <tr>
            <td align="left" valign="middle" colspan="4">
              
            </td>
        </tr>     
    </table>  
    <!-- 
    <asp:CommandField ButtonType="Image" EditImageUrl="~/images/layout_edit.png" ShowEditButton="True" />
    <asp:CommandField ButtonType="Image" DeleteImageUrl="~/images/layout_delete.png"
        ShowDeleteButton="True" />
    -->  
</asp:Content>
