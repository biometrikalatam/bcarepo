using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Xml;
using Bio.Digital.Receipt.Properties;
//using Biokey.pki.mentalis;
using log4net;
using Bio.Core.pki.utils;
using Bio.Core.pki.xmldsig;
using Bio.Digital.Receipt.Api;

namespace Bio.Digital.Receipt.pki
{
    public class XMLSignatureHelper
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(XMLSignatureHelper));

        public static int Firmar(string xmltosign, out string xmlsigned, out string msgerr)
        {
            xmlsigned = null;
            msgerr = null;
            int ret = 0;
            X509Certificate cer = null;
            try
            {
                //AsymmetricAlgorithm key = GetPrivateKey();
                AsymmetricAlgorithm key = GetPrivateKey(0, out cer);

                // Create a new XML document.
                XmlDocument xmlDoc = new XmlDocument();

                // Load an XML file into the XmlDocument object.
                xmlDoc.PreserveWhitespace = false;

                //Saco encabezado si trae
                xmltosign = xmltosign.Substring(xmltosign.IndexOf("<Rd"));

                xmlDoc.LoadXml(xmltosign);

                // Sign the XML document. 
                if (BKSignerXML.SignXml(xmlDoc, key, "#IDRDREF.v1", Settings.Default.CER))
                {
                    xmlsigned = xmlDoc.FirstChild.OuterXml;
                }
                else
                {
                    ret = -2;
                    msgerr = "Error firmando xml";
                }
            }
            catch (Exception ex)
            {
                log.Error("Error", ex);
                msgerr = ex.Message;
                ret = -1;
            }

            return ret;

        }

        public static int Firmar(int idapp, string receiptid, string xmltosign, out string xmlsigned, out string msgerr)
        {
            xmlsigned = null;
            msgerr = null;
            int ret = 0;
            X509Certificate cer = null; 
            try
            {
                log.Debug("Firmar IN [idapp=" + idapp.ToString() + " - receiptid=" + receiptid + " - xmltosign= " +
                           (string.IsNullOrEmpty(xmltosign) ? "NULL" : xmltosign.Substring(0,75) + " ..."));
                //Si es FEA
                if (((Bio.Digital.Receipt.Core.Helpers.RdAplicacionesMemory)(Global.HASHTABLE_RDAPPLICATION[idapp])).RdApp.SignatureType == 2)
                {
                    log.Debug("Firmar IN FEA...");
                    string name = receiptid + ".xml";
                    ret = FEA.FEAHelper.SignCloud(1, name, idapp, xmltosign, out xmlsigned);
                    xmlsigned = xmlsigned.Substring(xmlsigned.IndexOf("<Rd"));
                } else //es firma simple global o por idapp pero local
                {
                    log.Debug("Firmar FSimple...");
                    //AsymmetricAlgorithm key = GetPrivateKey();
                    AsymmetricAlgorithm key = GetPrivateKey(idapp, out cer);
                    if (key != null)
                    {
                        log.Debug("Firmar key != null");
                    } else
                    {
                        log.Debug("Firmar key == null");
                    }

                    // Create a new XML document.
                    XmlDocument xmlDoc = new XmlDocument();

                    // Load an XML file into the XmlDocument object.
                    xmlDoc.PreserveWhitespace = false;

                    //Saco encabezado si trae
                    xmltosign = xmltosign.Substring(xmltosign.IndexOf("<Rd"));
                    log.Debug("Firmar - SubString to sign => " +
                                (string.IsNullOrEmpty(xmltosign) ? "NULL" : xmltosign.Substring(0, 75) + " ..."));
                    xmlDoc.LoadXml(xmltosign);
                    log.Debug("Firmar - LoadXml ok...");
                    // Sign the XML document. 
                    if (BKSignerXML.SignXml(xmlDoc, key, "#IDRDREF.v1", cer))
                    {
                        xmlsigned = xmlDoc.FirstChild.OuterXml;
                        log.Debug("Firmar OK!");
                    }
                    else
                    {
                        ret = -2;
                        msgerr = "Error firmando xml";
                        log.Debug("Firmar Error firmando xml!!");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error", ex);
                msgerr = ex.Message;
                ret = -1;
            }
            
            return ret;

        }


        public static int VerificarFirma(string xmlsigned, out string msgerr)
        {
            //xmlsigned = null;
            msgerr = null;
            int ret = 0;

            try
            {
                AsymmetricAlgorithm key = GetPrivateKey();

                // Create a new XML document.
                XmlDocument xmlDoc = new XmlDocument();

                // Load an XML file into the XmlDocument object.
                xmlDoc.PreserveWhitespace = false;

                xmlDoc.LoadXml(xmlsigned);

                // Sign the XML document. 
                if (BKSignerXML.VerifyXml(xmlDoc, key))
                {
                    ret = 0;
                }
                else
                {
                    ret = Errors.IRET_ERR_SIGNATURE_INVALID;
                    msgerr = "Verificacion Negativa del XML";
                }               
            }
            catch (Exception ex)
            {
                log.Error("Error", ex);
                msgerr = ex.Message;
                ret = -1;
            }

            return ret;

        }

        public static int VerificarFirma(int idapp, string xmlsigned, out string msgerr)
        {
            //xmlsigned = null;
            msgerr = null;
            int ret = 0;
            X509Certificate cer = null;

            try
            {
                AsymmetricAlgorithm key = GetPrivateKey(idapp, out cer);

                // Create a new XML document.
                XmlDocument xmlDoc = new XmlDocument();

                // Load an XML file into the XmlDocument object.
                xmlDoc.PreserveWhitespace = false;

                //xmlsigned = "<? xml version=\"1.0\" encoding=\"UTF-8\" ?>" + xmlsigned;

                xmlDoc.LoadXml(xmlsigned);

                // Sign the XML document. 
                if (BKSignerXML.VerifyXml(xmlDoc, key))
                {
                    ret = 0;
                }
                else
                {
                    ret = Errors.IRET_ERR_SIGNATURE_INVALID;
                    msgerr = "Verificacion Negativa del XML";
                }
            }
            catch (Exception ex)
            {
                log.Error("Error", ex);
                msgerr = ex.Message;
                ret = -1;
            }

            return ret;

        }

        //// Sign an XML file. 
        //// This document cannot be verified unless the verifying 
        //// code has the key with which it was signed.
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="Doc"></param>
        ///// <param name="Key"></param>
        ///// <returns></returns>
        //public static bool SignXml(XmlDocument Doc, AsymmetricAlgorithm Key) //RSA Key)
        //{
        //    try
        //    {
        //        // Check arguments.
        //        if (Doc == null) return false;
        //            //throw new ArgumentException("Doc NULL");
        //        if (Key == null) return false;
        //            //throw new ArgumentException("Key NULL");

        //        // Create a SignedXml object.
        //        SignedXml signedXml = new SignedXml(Doc);

        //        // Add the key to the SignedXml document.
        //        signedXml.SigningKey = Key;

        //        // Create a reference to be signed.
        //        Reference reference = new Reference();
        //        reference.Uri = "#IDRDREF.v1";

        //        // Add an enveloped transformation to the reference.
        //        XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
        //        reference.AddTransform(env);

        //        // Add the reference to the SignedXml object.
        //        signedXml.AddReference(reference);

        //        // Add an RSAKeyValue KeyInfo (optional; helps recipient find key to validate).
        //        KeyInfo keyInfo = new KeyInfo();
        //        keyInfo.AddClause(new RSAKeyValue((RSA)Key));
        //        // Load the X509 certificate.
        //        X509Certificate MSCert = X509Certificate.CreateFromCertFile(Settings.Default.CER);

        //        // Load the certificate into a KeyInfoX509Data object
        //        // and add it to the KeyInfo object.
        //        keyInfo.AddClause(new KeyInfoX509Data(MSCert));
        //        signedXml.KeyInfo = keyInfo;

        //        // Compute the signature.
        //        signedXml.ComputeSignature();

        //        // Get the XML representation of the signature and save
        //        // it to an XmlElement object.
        //        XmlElement xmlDigitalSignature = signedXml.GetXml();

        //        // Append the element to the XML document.
        //        Doc.DocumentElement.AppendChild(Doc.ImportNode(xmlDigitalSignature, true));

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error", ex);
        //        return false;
        //    }

        //    return true;
        //}

        //// Verify the signature of an XML file against an asymmetric 
        //// algorithm and return the result.
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="Doc"></param>
        ///// <param name="key"></param>
        ///// <returns></returns>
        //public static Boolean VerifyXml(XmlDocument Doc, AsymmetricAlgorithm key) //RSA Key)
        //{
        //    // Check arguments.
        //    if (Doc == null)
        //        throw new ArgumentException("Doc");
        //    //if (Key == null)
        //    //    throw new ArgumentException("Key");

        //    // Create a new SignedXml object and pass it
        //    // the XML document class.
        //    SignedXml signedXml = new SignedXml(Doc);

        //    // Find the "Signature" node and create a new
        //    // XmlNodeList object.
        //    XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");

        //    // Throw an exception if no signature was found.
        //    if (nodeList.Count <= 0)
        //    {
        //        throw new CryptographicException("Verification failed: No Signature was found in the document.");
        //    }

        //    // This example only supports one signature for
        //    // the entire XML document.  Throw an exception 
        //    // if more than one signature was found.
        //    if (nodeList.Count >= 2)
        //    {
        //        throw new CryptographicException("Verification failed: More that one signature was found for the document.");
        //    }

        //    // Load the first <signature> node.  
        //    signedXml.LoadXml((XmlElement)nodeList[0]);

        //    // Check the signature and return the result.
        //    return signedXml.CheckSignature();
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static AsymmetricAlgorithm GetPrivateKey()
        {
            AsymmetricAlgorithm ret = null;
            try
            {
                if (!Global.ASYMMETRIC_KEY.Equals(null)) ret = Global.ASYMMETRIC_KEY;
                else {
                    Global.ASYMMETRIC_KEY = BKpkcs12.GetPrivateKeyFromPFX(Settings.Default.PFX, Settings.Default.PFXPSW);
                    ret = Global.ASYMMETRIC_KEY;
                }
            }
            catch (Exception ex)
            {
                log.Error("XMLSignatureHelper.GetPrivateKey", ex);
                ret = null;
            }
            
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static AsymmetricAlgorithm GetPrivateKey(int idapp, out X509Certificate cer)
        {
            AsymmetricAlgorithm ret = null;
            cer = null;
            try
            {
                if (((Bio.Digital.Receipt.Core.Helpers.RdAplicacionesMemory)(Global.HASHTABLE_RDAPPLICATION[idapp])).RdApp.SignatureType == 1)
                {
                    cer = ((Bio.Digital.Receipt.Core.Helpers.RdAplicacionesMemory)(Global.HASHTABLE_RDAPPLICATION[idapp])).MsCert;
                    ret = ((Bio.Digital.Receipt.Core.Helpers.RdAplicacionesMemory)(Global.HASHTABLE_RDAPPLICATION[idapp])).Key;
                }
                else
                {
                    cer = Global.X509CERTIFICATE;
                    ret = Global.ASYMMETRIC_KEY;
                    //if (!Global.ASYMMETRIC_KEY.Equals(null)) ret = Global.ASYMMETRIC_KEY;
                    //else
                    //{
                    //    Global.ASYMMETRIC_KEY = BKpkcs12.GetPrivateKeyFromPFX(Settings.Default.PFX, Settings.Default.PFXPSW);
                    //    ret = Global.ASYMMETRIC_KEY;
                    //}
                }
            }
            catch (Exception ex)
            {
                log.Error("XMLSignatureHelper.GetPrivateKey", ex);
                ret = null;
            }

            return ret;
        }
    }
}
