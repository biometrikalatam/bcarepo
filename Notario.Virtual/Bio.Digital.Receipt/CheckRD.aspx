<%@ Page Language="C#" MasterPageFile="~/RDMasterPageRoot.Master" AutoEventWireup="true" CodeBehind="CheckRD.aspx.cs" Inherits="Bio.Digital.Receipt.CheckRD" Title="Untitled Page" Debug="true" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <br />

    <table align="center" border="0" style="width: 80%; border-color:#346735; border-style:solid; border-width:1px; vertical-align:middle;">
        <tr>
            <td align="left" valign="middle" colspan="4" class="titleform">
                Ingrese Recibo a Chequear...</td>
        </tr>
        <tr>
        <td align="right" valign="middle" colspan="4">
                <br />
            &nbsp;<asp:Label ID="labMsg" runat="server" Font-Names="Arial" Font-Size="Small" ForeColor="Red"></asp:Label></td>
        </tr>
        <!-- 
        <tr>
            <td align="right" valign="top" class="labelform" style="width:15%">
                <br />
                Desde Archivo :
            </td>
            <td colspan="3" align="left" valign="top" style="width:35%">
                <br />
                <asp:FileUpload ID="reciboXMLUpload" CssClass="textboxform160" runat="server" Width="600px" ToolTip="Ingrese path absoluto del archivo xml conteniendo el recibo digital..." /></td>                
        </tr>
        -->
        <tr>
            <td align="right" valign="top" class="labelform" style="width:20%">
                <br />Recibo XML :<br />
            </td>
            <td colspan="3" align="left" valign="top" style="width:35%">
                <br />
            <asp:TextBox ID="txtReciboXML" runat="server" CssClass="textboxform600" Height="300px" Rows="2000" TextMode="MultiLine" Width="700px" OnTextChanged="txtReciboXML_TextChanged"></asp:TextBox>
            <br /><br />
           </td>                
        </tr>
        <tr>
            <td align="right" valign="top" colspan="1" class="labelform" style="width:20%">
                Tipo de Checqueo:
            </td>
            <td align="left" valign="top" colspan="1" class="labelform" style="width:75%">   
                <asp:DropDownList ID="cboTipoCheck" runat="server">
                    <asp:ListItem Selected="True" Value="0">Completa</asp:ListItem>
                </asp:DropDownList>
                <!-- 
                <asp:ListItem Value="0">Solo Existencia</asp:ListItem>
                    <asp:ListItem Value="1">Solo Firma</asp:ListItem>
                    <asp:ListItem Value="2">Solo L&#243;gica de Negocios</asp:ListItem>
                -->
            </td>
            <td align="center" valign="middle" colspan="2">
                <br />
                <asp:Button ID="btnCheck" runat="server" Text="Chequear Recibo" CssClass="boton1140" OnClick="btnCheck_Click" /></td>
        </tr>        
    </table>    
</asp:Content>
