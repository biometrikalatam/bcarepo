﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bio.Digital.Receipt.Api;
using log4net;
using Bio.Digital.Receipt.Core;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.libs;
using Bio.Digital.Receipt.pki;
using System.Xml;

namespace Bio.Digital.Receipt.Core.Services
{
    public class ServiceManager
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(WSBDR));

        internal static int Process(XmlParamIn oXmlIn, out string xmlparamout)
        {
            switch (oXmlIn.Actionid)
            {
                case 1:
                    return GenerateReceipt(oXmlIn, out xmlparamout);
                    break;
                case 2:
                    return VerifyReceipt(oXmlIn, out xmlparamout);
                    break;
                case 3:
                    return GetReceipt(oXmlIn, out xmlparamout);
                    break;
                default:
                    return GenerateReceipt(oXmlIn, out xmlparamout);
                    break;

            }
        }

        private static int GenerateReceipt(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /* 
             *  1.- Serializo param
             *  2.- Genero RD xml y firmo 
             *  3.- Grabo en BD
             *  4.- Retorno
            */
            int ret = Errors.IRET_OK;
            string sXMLTS = null;
            int resTS = 0;
            string sRDEnvelope = null;
            bool isSaveOK = false;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;

            try
            {
                //*  1.- Serializo param
                //  Luego en version final este paso.

                //*  2.- Genero RD xml y firmo 
                DateTime dNow = DateTime.Now;
                string reciboID = Guid.NewGuid().ToString("N");
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                // AGREGAR CONTROL DE LOGIN
                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string strapp;
                int qext = 0;
                int retaux = CheckAppAndCompanyEnabled(oXmlIn.ApplicationId, out qext,  out strapp, out msg);
                if (retaux < 0)
                {
                    oXmlOut.Message = msg;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                } else  //Si lee bien el RdAplicacion => Chequeo cantidad de extensiones y que no lleguen nulas
                {
                    if (qext != oXmlIn.Extensiones.Length)
                    {
                        oXmlOut.Message = "Cantidad de extensiones no coinciden. Esperadas = " + qext 
                            + " - Informadas = " + oXmlIn.Extensiones.Length;
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_PARAM_INCORRECTOS;
                    }

                    bool hayNulos = false;
                    for (int i = 0; i < oXmlIn.Extensiones.Length; i++)
                    {
                        if (oXmlIn.Extensiones[i] == null 
                            || String.IsNullOrEmpty(oXmlIn.Extensiones[i].key)
                            || String.IsNullOrEmpty(oXmlIn.Extensiones[i].value))
                        {
                            oXmlOut.Message = "Existen extensiones nulas. No permitido.";
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                            return Errors.IRET_ERR_PARAM_INCORRECTOS;
                        }
                    }
                }

                
                Bio.Digital.Receipt.Core.RdExtensionItem[] ext = new Bio.Digital.Receipt.Core.RdExtensionItem[oXmlIn.Extensiones.Length];
                Bio.Digital.Receipt.Core.RdExtensionItem item;
                for (int i = 0; i < oXmlIn.Extensiones.Length; i++)
                {
                    item = new RdExtensionItem();
                    item.key = oXmlIn.Extensiones[i].key;
                    item.value = oXmlIn.Extensiones[i].value;
                    ext[i] = item;
                }
                Rd objRD = new Rd(ip, strapp, oXmlIn.Origin.ToString(), dNow, oXmlIn.Description, ext, reciboID, 1);

                //Firmo RD
                string xmlTosign = Bio.Digital.Receipt.libs.XmlUtils.SerializeObject(objRD);
                string xmlsigned = null;
                string msgerr = null;
                int ierr = XMLSignatureHelper.Firmar(oXmlIn.ApplicationId, reciboID, xmlTosign, out xmlsigned, out msgerr);

                //Si está habilitado el TSA => Envio
                if (ierr == 0)
                {
                    if (Global.TSA_ENABLED)
                    {
                        resTS = Global.TSA_MANAGER.GenerateTS(Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(xmlsigned)), out sXMLTS);
                        if (resTS == 0)
                        {
                            sRDEnvelope = "<RDEnvelope>" + xmlsigned + sXMLTS + "</RDEnvelope>";
                            isSaveOK = true;
                        }
                        else
                        {
                            isSaveOK = false;
                            oXmlOut.Message = "Error generando TS";
                            LOG.Error(oXmlOut.Message);
                            oXmlOut.Receipt = null;
                            ret = Errors.IRET_ERR_TSA;
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        }
                    }
                    else
                    {
                        isSaveOK = true;
                        sRDEnvelope = "<RDEnvelope>" + xmlsigned + "</RDEnvelope>";
                    }

                    if (isSaveOK)
                    {
                        try
                        {
                            RdRecibos ord = new RdRecibos();
                            ord.Reciboid = reciboID;
                            ord.Descripcion = oXmlIn.Description;
                            ord.Fecha = dNow;
                            ord.Iporigen = HttpContext.Current.Request.UserHostAddress;
                            ord.RdAplicaciones = AdministradorRdAplicaciones.BuscarRdAplicacionesById(oXmlIn.ApplicationId);
                            ord.Reciboxml = sRDEnvelope; // xmlsigned;
                            ord.Version = "1.0";
                            AdministradorRdRecibos.Save(ord);
                            //PersistentManager.session().Save(ord);
                            //PersistentManager.session().Flush();
                            CopyParams(oXmlIn, out oXmlOut);
                            oXmlOut.Receipt = sRDEnvelope; //xmlsigned;
                            oXmlOut.Trackid = reciboID;
                            oXmlOut.Timestampend = DateTime.Now;
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        }
                        catch (Exception exDB)
                        {
                            LOG.Error("Error salvando en BD [" + exDB.Message + "]");
                            LOG.Error("Error salvando en BD [" + exDB.StackTrace + "]");
                            oXmlOut.Message = "Error salvando en BD [" + exDB.Message + "]";
                            oXmlOut.Receipt = null;
                            ret = Errors.IRET_ERR_SAVE_DB;
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        }
                    }
                }
                else
                {
                    oXmlOut.Message = "Error Firmando [" + msgerr + "]";
                    oXmlOut.Receipt = null;
                    ret = Errors.IRET_ERR_SIGNATURE;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                }
            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                LOG.Error(oXmlOut.Message);
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }

            return ret;
        }

        private static void CopyParams(XmlParamIn oXmlIn, out XmlParamOut oXmlOut)
        {
            oXmlOut = new XmlParamOut();
            try
            {
                oXmlOut.Actionid = oXmlIn.Actionid;
                oXmlOut.Clientid = oXmlIn.Clientid;
                oXmlOut.Companyid = oXmlIn.Companyid;
                oXmlOut.Enduser = oXmlIn.Enduser;
                oXmlOut.Ipenduser = oXmlIn.Ipenduser;
                oXmlOut.Origin = oXmlIn.Origin;
                //oXmlOut. = oXmlIn;

            }
            catch (Exception ex)
            {
                LOG.Error("ServiceManager.CopyParams Error = " + ex.Message);
            }
        }

        
        private static int GetReceipt(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /*  NOTA: Los parametros luego en version final hay que cambiarlo, para que sea dinamico
             *      y se controlen con las extensiones definidas.
             *  1.- Serializo param
             *  2.- Chequeo que exista en BD sino => Retorno que no existe 
             *  3.- Si existe => Check Firma
             *  4.- Retorno
            */
            int ret = Errors.IRET_OK;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;
            try
            {

                //*  2.- Genero RD xml y firmo 
                DateTime dNow = DateTime.Now;
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                // AGREGAR CONTROL DE LOGIN
                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string app;
                int qext;
                int retaux = CheckAppAndCompanyEnabled(oXmlIn.ApplicationId, out qext, out app, out msg);
                if (retaux < 0)
                {
                    oXmlOut.Message = msg;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }

                //*  2.- Check en BD 
                CopyParams(oXmlIn, out oXmlOut);
                RdRecibos dbRecibo = AdministradorRdRecibos.BuscarRdReciboByReciboId(oXmlIn.ReceiptId);

                if (dbRecibo == null)
                {
                    oXmlOut.Trackid = oXmlIn.ReceiptId;
                    oXmlOut.Message = "El recibo  con ID = " + oXmlIn.ReceiptId
                                      + " no se encuentra en el repositorio seguro";
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    ret = Errors.IRET_ERR_RECEIPT_NO_IN_DB;
                }
                else
                {
                    oXmlOut.Trackid = oXmlIn.ReceiptId;
                    oXmlOut.Message = "El recibo  con ID = " + oXmlIn.ReceiptId
                                        + " existe";
                    oXmlOut.Receipt = dbRecibo.Reciboxml;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    ret = Errors.IRET_OK;
                }
            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }

            return ret;
        }



        /// <summary>
        /// Verifica que vengan los parámetros necesarios para cada accion
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static bool IsCorrectParamInForAction(XmlParamIn oXmlIn, out string msgerr)
        {
            msgerr = null;
            bool ret = false;
            try
            {
                if (oXmlIn.Actionid == 1 && (oXmlIn.Extensiones == null || oXmlIn.Extensiones.Length ==0 || oXmlIn.ApplicationId == 0))
                {
                    msgerr = "Extensiones vacias y/o Id Aplicaicon = 0. Imposible Notarizar";
                    ret = false;
                }

                if (oXmlIn.Actionid == 2 && (String.IsNullOrEmpty(oXmlIn.Receipt)))
                {
                    msgerr = "Recibo nulo no se puede verificar.";
                    ret = false;
                }

                if (oXmlIn.Actionid == 3 && (String.IsNullOrEmpty(oXmlIn.ReceiptId) || oXmlIn.ApplicationId == 0))
                {
                    msgerr = "ReceiptId nulo o ApplicationId = 0, no se puede recuperar RDEnvelope";
                    ret = false;
                }

                ret = true;
            }
            catch (Exception ex)
            {
                LOG.Error("ServiceManager.IsCorrectParamInForAction Error - " + ex.Message);
                ret = false;
            }
            return ret;
        }


        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="idaplicacion"></param>
        ///// <param name="origen"></param>
        ///// <param name="descripcion"></param>
        ///// <param name="extensiones"></param>
        ///// <returns></returns>

        //public RDRetorno GenerateReceipt(int idaplicacion, string origen,
        //                              string descripcion, RdExtensionItem[] extensiones)
        //{
        //    /*  NOTA: Los parametros luego en version final hay que cambiarlo, para que sea dinamico
        //     *      y se controlen con las extensiones definidas.
        //     *  1.- Serializo param
        //     *  2.- Genero RD xml y firmo 
        //     *  3.- Grabo en BD
        //     *  4.- Retorno
        //    */
        //    RDRetorno ret = new RDRetorno();
        //    ret.codigo = RET_OK;
        //    string sXMLTS = null;
        //    int resTS = 0;
        //    string sRDEnvelope = null;
        //    bool isSaveOK = false;

        //    try
        //    {
        //        //*  1.- Serializo param
        //        //  Luego en version final este paso.

        //        //*  2.- Genero RD xml y firmo 
        //        DateTime dNow = DateTime.Now;
        //        string reciboID = Guid.NewGuid().ToString("N");
        //        string ip = HttpContext.Current.Request.UserHostAddress;
        //        if (ip == null || ip.Length < 9) ip = "127.0.0.1";

        //        // AGREGAR CONTROL DE LOGIN
        //        //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
        //        //      Obtengo el id de compania para ver si existe, 
        //        //      chequeo user/pass 
        //        string msg;
        //        string strapp;
        //        int retaux = CheckAppAndCompanyEnabled(idaplicacion, out strapp, out msg);
        //        if (retaux < 0)
        //        {
        //            ret.codigo = retaux;
        //            ret.descripcion = msg;
        //            return ret;
        //        }

        //        //string strapp = AdministradorRdAplicaciones.BuscarRdAplicacionesById(idaplicacion).Nombre;
        //        //ADD 
        //        //       1) salida si no encuentra la APP
        //        //       2) Check que compañia no este deshabilitada

        //        Rd objRD = new Rd(ip, strapp, origen, dNow, descripcion, extensiones, reciboID, 1);

        //        //Firmo RD
        //        string xmlTosign = Bio.Digital.Receipt.libs.XmlUtils.SerializeObject(objRD);
        //        string xmlsigned = null;
        //        string msgerr = null;
        //        int ierr = XMLSignatureHelper.Firmar(xmlTosign, out xmlsigned, out msgerr);

        //        //Si está habilitado el TSA => Envio
        //        if (ierr == 0)
        //        {
        //            if (Global.TSA_ENABLED)
        //            {
        //                resTS = Global.TSA_MANAGER.GenerateTS(Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(xmlsigned)), out sXMLTS);
        //                if (resTS == 0)
        //                {
        //                    sRDEnvelope = "<RDEnvelope>" + xmlsigned + sXMLTS + "</RDEnvelope>";
        //                    isSaveOK = true;
        //                }
        //                else
        //                {
        //                    isSaveOK = false;
        //                    ret.codigo = RET_ERR_TSA;
        //                    ret.descripcion = "Error generando TS";
        //                    ret.xmlret = null;
        //                }
        //            }
        //            else
        //            {
        //                isSaveOK = true;
        //                sRDEnvelope = "<RDEnvelope>" + xmlsigned + "</RDEnvelope>";
        //            }

        //            if (isSaveOK)
        //            {
        //                try
        //                {
        //                    RdRecibos ord = new RdRecibos();
        //                    ord.Reciboid = reciboID;
        //                    ord.Descripcion = descripcion;
        //                    ord.Fecha = dNow;
        //                    ord.Iporigen = HttpContext.Current.Request.UserHostAddress;
        //                    ord.RdAplicaciones = AdministradorRdAplicaciones.BuscarRdAplicacionesById(idaplicacion);
        //                    ord.Reciboxml = sRDEnvelope; // xmlsigned;
        //                    ord.Version = "1.0";
        //                    PersistentManager.session().Save(ord);
        //                    PersistentManager.session().Flush();
        //                    ret.xmlret = sRDEnvelope; //xmlsigned;
        //                }
        //                catch (Exception exDB)
        //                {
        //                    ret.codigo = RET_ERR_SAVE_DB;
        //                    ret.descripcion = exDB.Message;
        //                    ret.xmlret = null;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            ret.codigo = RET_ERR_SIGNATURE;
        //            ret.descripcion = msgerr;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error GeneraRecibo", ex);
        //        ret.codigo = RET_ERR_DESCONOCIDO;
        //        ret.descripcion = ex.Message;
        //    }

        //    return ret;
        //}

        /// <summary>
        /// Chequea que la aplicaicon y la compañía estén habilitadas
        /// </summary>
        /// <param name="idaplicacion"></param>
        /// <param name="qextensiones"></param>
        /// <param name="app"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        private static int CheckAppAndCompanyEnabled(int idaplicacion, out int qextensiones, out string app, out string msg)
        {
            int ret = 0;
            msg = null;
            app = null;
            qextensiones = 0;
            try
            {
                RdAplicaciones appObj = AdministradorRdAplicaciones.BuscarRdAplicacionesById(idaplicacion);

                if (appObj == null)
                {
                    msg = "Aplicacion No Existe";
                    return Errors.IRET_ERR_APLICATION_NOT_EXIST;
                }
                else
                {
                    app = appObj.Nombre;

                    if (appObj.Deleted.HasValue)
                    {
                        msg = "Aplicación [" + app + "] Deshabilitada desde " + appObj.Deleted.Value.ToString("dd/MM/yyyy");
                        return Errors.IRET_ERR_APLICATION_DISABLED;
                    }
                    qextensiones = appObj.QExtensiones;
                    Company company = AdministradorCompany.BuscarCompanyById(appObj.Company);
                    if (company == null)
                    {
                        msg = "Company No Existe";
                        return Errors.IRET_ERR_COMPANY_NOT_EXIST;
                    }
                    else if (company.EndDate.HasValue && company.EndDate < DateTime.Now)
                    {
                        msg = "Company deshabilitada desde " + company.EndDate.Value.ToString("dd/MM/yyyy");
                        return Errors.IRET_ERR_COMPANY_DISABLED;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Error CheckCompanyEnabled", ex);
            }
            return ret;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="xmlreceipt"></param>
        ///// <param name="verifytype">1 - Solo existnecia en BD |
        /////                                2 - Existencia en BD y Check Igual contenido 
        ///// </param>
        ///// <returns></returns>
        public static int VerifyReceipt(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /*  NOTA: Los parametros luego en version final hay que cambiarlo, para que sea dinamico
             *      y se controlen con las extensiones definidas.
             *  1.- Serializo param
             *  2.- Chequeo que exista en BD sino => Retorno que no existe 
             *  3.- Si existe => Check Firma
             *  4.- Retorno
            */
            int ret = Errors.IRET_OK;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;
            try
            {

                //*  2.- Genero RD xml y firmo 
                DateTime dNow = DateTime.Now;
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                // AGREGAR CONTROL DE LOGIN
                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string strapp;
                //int retaux = CheckCompanyEnabled(oXmlIn.Companyid, out msg);
                //if (retaux < 0)
                //{
                //    oXmlOut.Message = msg;
                //    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                //}

                //*  2.- Check en BD 
                CopyParams(oXmlIn, out oXmlOut);
                string xmlreceipt = ExtractReceiptFromEnvelope(oXmlIn.Receipt);
                Rd orecibo = libs.XmlUtils.DeserializeObject<Rd>(xmlreceipt);
                RdRecibos dbRecibo = AdministradorRdRecibos.BuscarRdReciboByReciboId(orecibo.reciboID);

                if (dbRecibo == null)
                {
                    oXmlOut.Trackid = oXmlIn.ReceiptId;
                    oXmlOut.Message = "El recibo  con ID = " + orecibo.reciboID
                                      + " no se encuentra en el repositorio seguro";
                    /// 0 - Recibo OK | 1 - Recibo existe pero firma inválida | 2 - recibo no existe
                    oXmlOut.ResultVerify = 2;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    ret = Errors.IRET_ERR_RECEIPT_NO_IN_DB;
                }
                else
                {
                    string msgerr = null;
                    int retV = XMLSignatureHelper.VerificarFirma(dbRecibo.RdAplicaciones.Id, xmlreceipt, out msgerr);
                    if (retV != 0)
                    {
                        oXmlOut.Trackid = oXmlIn.ReceiptId;
                        oXmlOut.Message = "El recibo  con ID = " + orecibo.reciboID
                                          + " tiene firma inválida";
                        /// 0 - Recibo OK | 1 - Recibo existe pero firma inválida | 2 - recibo no existe
                        oXmlOut.ResultVerify = 1;
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        ret = Errors.IRET_ERR_SIGNATURE_INVALID;
                        //return ret;
                    }
                    else
                    {
                        oXmlOut.Trackid = oXmlIn.ReceiptId;
                        oXmlOut.Message = "El recibo  con ID = " + orecibo.reciboID
                                          + " es VALIDO";
                        /// 0 - Recibo OK | 1 - Recibo existe pero firma inválida | 2 - recibo no existe
                        oXmlOut.ResultVerify = 0;
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        ret = Errors.IRET_OK;
                        //return ret;
                    }
                }
                    //}

                    ////*  1.- Serializo param
                    //string xmlreceipt = ExtractReceiptFromEnvelope(xmlenvelope);
                    //Rd orecibo = libs.XmlUtils.DeserializeObject<Rd>(xmlreceipt);

                    //if (orecibo == null)
                    //{
                    //    ret = Errors.IRET_ERR_SCHEMA_RD_INVALID;
                    //    ret.descripcion = "Formato de recibo enviado inválido";
                    //    return ret;
                    //}

                    ////*  2.- Check en BD 
                    //RdRecibos dbRecibo = AdministradorRdRecibos.BuscarRdReciboByReciboId(orecibo.reciboID);

                    //if (dbRecibo == null)
                    //{
                    //    ret.codigo = Errors.IRET_ERR_RECEIPT_NO_IN_DB;
                    //    ret.descripcion = "El recibo  con ID = " + orecibo.reciboID
                    //                      + " no se encuentra en el repositorio seguro";
                    //    return ret;
                    //}
                    //else
                    //{
                    //    valueret++;
                    //}

                    ////Si se debe chequear por igualdad => Sigo
                    //if (verifytype > 1)
                    //{
                    //    XmlDocument rdInNV = new XmlDocument();
                    //    rdInNV.PreserveWhitespace = false;
                    //    rdInNV.LoadXml(dbRecibo.Reciboxml);

                    //    //XmlDocument rdInParam = new XmlDocument();
                    //    //rdInParam.PreserveWhitespace = false;
                    //    //rdInParam.LoadXml(xmlreceipt);
                    //    //string rdInParam = ExtractReceiptFromEnvelope(xmlenvelope);

                    //    if (rdInNV.OuterXml.Equals(xmlenvelope))
                    //    {
                    //        valueret++;
                    //    }
                    //    else
                    //    {
                    //        valueret = -5000; //No Igual
                    //    }
                    //}
                    //else
                    //{
                    //    string msgerr = null;
                    //    int retV = XMLSignatureHelper.VerificarFirma(dbRecibo.Reciboxml, out msgerr);
                    //    if (retV == 0)
                    //    {
                    //        valueret++;
                    //    }
                    //    else
                    //    {
                    //        valueret = retV;
                    //    }

                    //}

                    //ret.xmlret = "<ResultVerify>" +
                    //                "<tipoverify>" +
                    //                    verifytype.ToString() +
                    //                "</tipoverify>" +
                    //                "<result>" +
                    //                    valueret.ToString() +
                    //                "</result>" +
                    //             "</ResultVerify>";

            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //LOG.Error("Error VerifyReceipt", ex);
                //ret.codigo = Errors.IRET_ERR_DESCONOCIDO;
                //ret.descripcion = ex.Message;
            }

            return ret;
        }

        private static int CheckCompanyEnabled(int companyid, out string msg)
        {
            int ret = 0;
            msg = null;
            try
            {
                Company company = AdministradorCompany.BuscarCompanyById(companyid);
                if (company == null)
                {
                    msg = "Company No Existe";
                    return Errors.IRET_ERR_COMPANY_NOT_EXIST;
                }
                else if (company.EndDate.HasValue && company.EndDate < DateTime.Now)
                {
                    msg = "Company deshabilitada desde " + company.EndDate.Value.ToString("dd/MM/yyyy");
                    return Errors.IRET_ERR_COMPANY_DISABLED;
                }
                
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Error CheckCompanyEnabled", ex);
            }
            return ret;
        }

        private static string ExtractReceiptFromEnvelope(string xmlenvelope)
        {
            string sret = null;
            try
            {
                XmlDocument rdEnvelope = new XmlDocument();
                rdEnvelope.PreserveWhitespace = false;
                rdEnvelope.LoadXml(xmlenvelope);

                XmlNode node = rdEnvelope.GetElementsByTagName("Rd")[0];

                sret = node.OuterXml;
            }
            catch (Exception ex)
            {
                LOG.Error("Error ExtractReceiptFromEnvelope", ex);
            }
            return sret;
        }

    }
}