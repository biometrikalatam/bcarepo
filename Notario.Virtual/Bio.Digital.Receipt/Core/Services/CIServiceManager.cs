﻿using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.Core.Helpers;
using Bio.Digital.Receipt.libs;
using Bio.Digital.Receipt.pki;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using System.Web;
using System.Xml;

namespace Bio.Digital.Receipt.Core.Services
{
    public class CIServiceManager
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(CIServiceManager));


        /// <summary>
        /// Procesa segun ActionId, de acuerdo a estos valores:
        ///      1.- GetToken: Dado ciertos parámetros de entrada, retorna un QR para usar de ancla con una certificación de identidad
        ///      2.- IDCardVerification: Dada la imágen de una cédula Chilena de frente, hace OCR. Si ok => Retorna ok mas parseo
        ///      3.- GenerateCI_w_FotoCedulaVsSelfie (+ Enroll directo o manual?): Realiza una verificación de identidad entre Cédula y Selfie, 
        ///            y devuelve Recibo y certificado en PDF (con QR para chequeo en linea)
        ///      4.- GenerateCI_w_SelfieVsBD: Realiza una verificación de identidad entre Selfie y BD, 
        ///            y devuelve Recibo y certificado en PDF (con QR con url para chequeo en linea)
        ///      5.- VerifyCI: Dado un trackid de recibo, verifica que existe y es válido
        ///      6.- GetCI: Dado un trackid de recibo, devuelve RD si existe
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        internal static int Process(XmlParamIn oXmlIn, out string xmlparamout)
        {
            switch (oXmlIn.Actionid)
            {
                case 1:  
                    return GetTokenForCI(oXmlIn, out xmlparamout);
                    break;
                case 2:
                case 22:
                case 23:
                    return IDCardVerification(oXmlIn, out xmlparamout);
                    break;
                case 3:
                case 33:
                    return GenerateCI_w_FotoCedulaVsSelfie(oXmlIn, out xmlparamout);
                    break;
                case 4:
                case 44:
                    return GenerateCI_w_SelfieVsBD(oXmlIn, out xmlparamout);
                    break;
                case 5: 
                    return VerifyCI(oXmlIn, out xmlparamout);
                    break;
                case 6:
                    return GetCI(oXmlIn, out xmlparamout);
                    break;
                case 7:
                    return GetCIIdentity(oXmlIn, out xmlparamout);
                    break;
                case 8:
                case 88:
                    return GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess(oXmlIn, out xmlparamout);
                    break;
                case 9:
                case 99:
                    return GenerateCI_w_SelfieVsBD_WithoutProcess(oXmlIn, out xmlparamout);
                    break;
                default:
                    return GetTokenForCI(oXmlIn, out xmlparamout);
                    break;

            }
        }

        internal static int GenerateCI_w_SelfieVsBD_WithoutProcess(XmlParamIn oXmlIn, out string xmlparamout)
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// Recupera una identidad ya enrolada (Certificacion hecha con Cedula) con ret = 1 
        /// mas los datos enrolados en CIParam, sino 0 y message con el error ocurrido si fue.
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        private static int GetCIIdentity(XmlParamIn oXmlIn, out string xmlparamout)
        {
            int ret = Errors.IRET_OK;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;
            try
            {
                LOG.Debug("CIServiceManager.GetCIIdentity IN...");

                //*  2.- Genero RD xml y firmo 
                DateTime dNow = DateTime.Now;
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                //3.- Chequeo que esté el id de aplicación, 
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string app;
                int qext;
                int retaux = CheckAppAndCompanyEnabled(oXmlIn.ApplicationId, out qext, out app, out msg);
                if (retaux < 0)
                {
                    oXmlOut.Message = msg;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Error("CIServiceManager.GetCIIdentity Error chequeando ingreso - msg=" + msg);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }

                //*  2.- Check en BD 
                CopyParams(oXmlIn, out oXmlOut);
                msg = null;
                CIParam ciparamout;
                string verificationsource; 
                int retget = BPHelper.GetEnrolled(oXmlIn, out ciparamout, out verificationsource);

                if (retget < 0) //Error recuperando => Informo no existe
                {
                    oXmlOut.Message = "Error recuperando Identidad [ret=" + retget + "]";
                    ret = Errors.IRET_OK;
                } else
                {
                    VerificationSource vs = Api.XmlUtils.DeserializeObject<VerificationSource>(verificationsource);
                    if (vs == null)
                    {
                        ret = 0;
                    }
                    else
                    {
                        ret = vs.IsIdentityValidated(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL) ? 1 : 0;
                        oXmlOut.CIParam = ciparamout;
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    }
                }
            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }
            LOG.Debug("CIServiceManager.GetTokenForCI OUT [ret = " + ret + "]");
            return ret;
        }

        /// <summary>
        /// PAra cehqueo en callback de status desde Javascript
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        internal static int CheckStatusCI(string trackid)
        {
            /* 
              *  1.- tomo txCI
              *  2.- Si hay error retorno error en ret
              *  3.- Si no hay error, retorno ResultCode que tiene el valor de verificacion si fue positivo => 1 (Ademas de ActionId == 3)
             */
            int ret = Errors.IRET_OK;
            string msg;
            try
            {
                LOG.Debug("CIServiceManager.CheckStatusCI IN...");

                RdCITx rdCITx = AdministradorRdCITx.GetCITxByTrackId(7, trackid, 0, 0, false, null, null, null, null, null, null, null, out msg);

                if (rdCITx == null)
                {
                    msg = "La transaccion con TrackId = " + trackid + " no se encuentra en el repositorio seguro";
                    LOG.Info("CIServiceManager.CheckStatusCI - " + msg);
                    ret = Errors.IRET_ERR_CI_NO_IN_DB;
                }
                else
                {
                    if (rdCITx.ActionId == 3 || rdCITx.ActionId == 33 ||
                        rdCITx.ActionId == 4 || rdCITx.ActionId == 44 ||
                        rdCITx.ActionId == 8 || rdCITx.ActionId == 88 ||
                        rdCITx.ActionId == 9 || rdCITx.ActionId == 99) ret = rdCITx.ResultCode;
                    else if (DateTime.Now > (rdCITx.DateGen.AddMinutes(rdCITx.QRValidity))) {
                        ret = Errors.IRET_ERR_CI_NO_COMPLETE_DEFEATED;
                    }
                    else ret = Errors.IRET_ERR_CI_NO_COMPLETE;
                }
            }
            catch (Exception ex)
            {   
                msg = "Error Desconocido [" + ex.Message + "]";
                LOG.Error("CIServiceManager.CheckStatusCI Error - " + msg);
                ret = Errors.IRET_ERR_DESCONOCIDO;
            }

            return ret;
        }


        /// <summary>
        /// Dado una imágen de Cedula Chilena de frente, se hace OCR y si se puede se deveulve los valores
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        private static int IDCardVerification(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /*  
             *  1.- Check Acceso
             *  2.- Chequeo que exista CITx en BD sino la creo 
             *  3.- Si existe o cree => Realizo Verify y OCR con BioPortal
             *  4.- Retorno resultado positivo o negativo
            */
            int ret = Errors.IRET_OK;
            XmlParamOut oXmlOut = new XmlParamOut();
            bool PDF417RecognizedOK = false;
            xmlparamout = null;
            try
            {
                LOG.Debug("CIServiceManager.IDCardVerification IN...");
                //*  2.- Genero RD xml y firmo 
                DateTime dNow = DateTime.Now;
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                //3.- Chequeo que esté el id de aplicación, 
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string app;
                int qext;
                int retaux = CheckAppAndCompanyEnabled(oXmlIn.ApplicationId, out qext, out app, out msg);
                if (retaux < 0)
                {
                    oXmlOut.Message = msg;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Error("CIServiceManager.IDCardVerification Error chequeando ingreso - msg=" + msg);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }


                //Obtengo rdCITx si existe con el TrackId del parametro de entrada, que es el trackid generado 
                //para la emision de un QR. Si existe, chequeo que no haya pasado mas tiempo que el configurado en 
                //Config (5 minutos a priori)
                msg = null;
                LOG.Debug("CIServiceManager.IDCardVerification Recuperando rdCITx con TrackId = " + oXmlIn.CIParam.TrackId);

                //Added 26-01 para cotrol de username que realiza la verificacion. Si viene nulo no importa, sino se graba
                string username = Utils.GetUserName(oXmlIn.Additionaldata);
                
                RdCITx rdCITx = AdministradorRdCITx.GetCITxByTrackId(oXmlIn.Actionid, oXmlIn.CIParam.TrackId,
                                                                     oXmlIn.Companyid, oXmlIn.CIParam.ValidityType, 
                                                                     true, oXmlIn.CIParam.TypeId, oXmlIn.CIParam.ValueId, null,
                                                                     oXmlIn.CIParam.DestinaryMail, oXmlIn.CIParam.WorkStationID,
                                                                     oXmlIn.CIParam.GeoRef, username, out msg);
                if (rdCITx == null)
                {
                    oXmlOut.Message = msg;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Error("CIServiceManager.IDCardVerification Error recuperndo rdCITx = " + msg);
                    return Errors.IRET_ERR_CI_TRACKID_NO_IN_DB;
                } else if (rdCITx.ActionId == 3 || rdCITx.ActionId == 33 || rdCITx.ActionId == 4 || rdCITx.ActionId == 44 ||
                           IsTrackIdVencida(rdCITx))
                {
                    oXmlOut.Message = "TrackId ya utilizada en un proceso de CI terminado " + 
                        "[ActionId = " + rdCITx.ActionId.ToString() + " | Fecha Fin = " + rdCITx.LastModify.ToString("dd/MM/yyy HH:mm:ss") + "]";
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Error("CIServiceManager.IDCardVerification Error recuperndo rdCITx = " + oXmlOut.Message);
                    return Errors.IRET_ERR_CI_TRACKID_COMPLETED;
                } else 
                {
                    oXmlIn.CIParam.TrackId = rdCITx.TrackId;
                    LOG.Debug("CIServiceManager.IDCardVerification rdCITx generada");
                }

                //*  2.- Check en BD 
                CopyParams(oXmlIn, out oXmlOut);

                if (oXmlIn == null || oXmlIn.CIParam == null || string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront)) 
                {
                    oXmlOut.Message = "Error parametro [oXmlIn.CIParam.IDCardImageFront Nulo]";
                    LOG.Error("CIServiceManager.IDCardVerification Error [" + oXmlOut.Message + "]");
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }
                msg = null;
                CIParam MRZIDCard = null;
                double score = 0;
                string TrackIdBP = null;
                LOG.Debug("CIServiceManager.IDCardVerification Ingresando verificación Cedula contra BP...");
                int rptaBP = BPHelper.VerifyAndOCRIDCard(oXmlIn, out msg, out MRZIDCard, out TrackIdBP);

                if (rptaBP < 0)
                {
                    oXmlOut.Message = "Error verificando cedula [Err=" + rptaBP + " - MsgErr=" + msg + "]";
                    LOG.Error("CIServiceManager.IDCardVerification Error verificando cedula contra BP [" + oXmlOut.Message + "]");
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    if (rptaBP == Errors.IRET_ERR_CI_IDCARD_INCONSISTENCE_DATA_OCR_RECOGNIZED)
                        return Errors.IRET_ERR_CI_IDCARD_INCONSISTENCE_DATA_OCR_RECOGNIZED;
                    else
                        return Errors.IRET_ERR_CI_IDCARD_PARSING;
                }
                else
                {
                    if (!oXmlIn.CIParam.ValueId.Equals(MRZIDCard.ValueId) && !oXmlIn.CIParam.ValueId.Equals("NA"))
                    {
                        oXmlOut.Message = "ValueId Configurado diferente al ValueId Reconocido. Reintente!" +
                                          " [ValueId Configurado = " + oXmlIn.CIParam.ValueId +
                                          " - ValueId Reconocido = " + (MRZIDCard!=null && !string.IsNullOrEmpty(MRZIDCard.ValueId)?MRZIDCard.ValueId:"NULO") + "]";
                        oXmlOut.Receipt = null;
                        LOG.Error("CIServiceManager.IDCardVerification Exception => " + oXmlOut.Message);
                        ret = Errors.IRET_ERR_CI_VALUEID_INCORRECT;
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return ret;
                    } else //Verifico si viene el parametro para reconocimiento de PDF417 y si es ON lo hago y guardo
                    {
                        if (Utils.IsCedulaAntigua(MRZIDCard.Serial) &&
                            !string.IsNullOrEmpty(oXmlIn.Additionaldata)) // && oXmlIn.Additionaldata.Contains("PDF417Recognition=ON"))
                        {
                            Dictionary<string, string> dicAD = JsonConvert.DeserializeObject<Dictionary<string, string>>(oXmlIn.Additionaldata);
                            if (dicAD.ContainsKey("PDF417Recognition") && dicAD["PDF417Recognition"].Equals("ON"))
                            {
                                string pdf417 = PDF417Helper.ProcesaPDF417Recognition(oXmlIn.CIParam.IDCardImageBack);
                                rdCITx.Pdf417 = pdf417;
                                Dictionary<string, string> dicOut = new Dictionary<string, string>();
                                if (!string.IsNullOrEmpty(rdCITx.Pdf417))
                                {
                                    PDF417RecognizedOK = true;
                                    dicOut.Add("PDF417Recognized","OK");
//                                    oXmlOut.Additionaldata = ;
                                }
                                else
                                {
                                    dicOut.Add("PDF417Recognized", "NOOK");
                                    //oXmlOut.Additionaldata = "PDF417Recognized=NOOK";
                                }
                                oXmlOut.Additionaldata = JsonConvert.SerializeObject(dicOut);
                            }
                        }
                    }

                    oXmlOut.CIParam = MRZIDCard;

                    LOG.Debug("CIServiceManager.IDCardVerification Verificacion Cedula OK [valueid=" + MRZIDCard.ValueId + " - Name=" + MRZIDCard.Name + "]");
                    if (MRZIDCard != null 
                        && !string.IsNullOrEmpty(MRZIDCard.ValueId) && !MRZIDCard.ValueId.Equals("NA")
                            && UpdateCITX(rdCITx, oXmlIn.Actionid, TrackIdBP, MRZIDCard))
                    {
                        oXmlOut.Message = "Se verifico IDCard";
                    } else
                    {
                        oXmlOut.Message = "NO se verifico IDCard";
                    }

                    if (oXmlIn.Actionid == 22) //Debe ser rpta liviana
                    {
                        oXmlOut.CIParam.IDCardImageFront = null;
                        oXmlOut.CIParam.IDCardImageBack = null;
                    }
                    if (oXmlIn.Actionid == 23) //Debe ser rpta liviana SIN Imagenes => Extra liviana
                    {
                        oXmlOut.CIParam.IDCardPhotoImage = null;
                        oXmlOut.CIParam.IDCardSignatureImage = null;
                    }
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    ret = Errors.IRET_OK;
                }

            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                LOG.Error("CIServiceManager.IDCardVerification Exception => " + oXmlOut.Message);
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }
            LOG.Debug("CIServiceManager.IDCardVerification OUT [ret = " + ret + "]");
            return ret;
        }

        internal static bool IsTrackIdVencida(RdCITx rdCITx)
        {
            bool ret = false;
            try
            {
                LOG.Debug("CIServiceManager.IsTrackIdVencida IN...");
                LOG.Debug("CIServiceManager.IsTrackIdVencida => rdCITx.ActionId = " + rdCITx.ActionId.ToString() + " - Now = " +
                          DateTime.Now.ToString() + " - Vencimiento = " + rdCITx.DateGen.AddMinutes(rdCITx.QRValidity).ToString());
                ret = DateTime.Now > rdCITx.DateGen.AddMinutes(rdCITx.QRValidity);
                LOG.Debug("CIServiceManager.IsTrackIdVencida ret = " + ret.ToString());
            }
            catch (Exception ex)
            {
                LOG.Error("CIServiceManager.IsTrackIdVencida Error - " + ex.Message);
                ret = false;
            }
            return ret;
        }

        private static bool UpdateCITX(RdCITx rdCITx, int actionid, string trackidbp, CIParam mRZIDCard)
        {
            bool ret = false;
            string msgerr;
            try
            {
                LOG.Debug("CIServiceManager.UpdateCITX IN...");

                if (mRZIDCard == null) {
                    LOG.Debug("CIServiceManager.UpdateCITX mRZIDCard == null => Return false...");
                    return false;
                }


                rdCITx.ActionId = actionid;
                rdCITx.LastModify = DateTime.Now;
                rdCITx.Name = mRZIDCard.Name;
                rdCITx.PhaterLastName = mRZIDCard.PhaterLastName;
                rdCITx.MotherLastName = mRZIDCard.MotherLastName;
                rdCITx.TrackIdBP = trackidbp;
                rdCITx.TypeId = mRZIDCard.TypeId;
                rdCITx.ValueId = mRZIDCard.ValueId;
                rdCITx.IdCardImageFront = !string.IsNullOrEmpty(mRZIDCard.IDCardImageFront)? mRZIDCard.IDCardImageFront:rdCITx.IdCardImageFront;
                rdCITx.IdCardImageBack = !string.IsNullOrEmpty(mRZIDCard.IDCardImageBack)? mRZIDCard.IDCardImageBack:rdCITx.IdCardImageBack;
                rdCITx.IdCardPhotoImage = !string.IsNullOrEmpty(mRZIDCard.IDCardPhotoImage)? mRZIDCard.IDCardPhotoImage: rdCITx.IdCardPhotoImage;
                rdCITx.IdCardSignatureImage = !string.IsNullOrEmpty(mRZIDCard.IDCardSignatureImage)? mRZIDCard.IDCardSignatureImage:rdCITx.IdCardSignatureImage;
                rdCITx.Selfie = !string.IsNullOrEmpty(mRZIDCard.Selfie) ? mRZIDCard.Selfie : rdCITx.Selfie;
                rdCITx.IssueDate = !string.IsNullOrEmpty(mRZIDCard.IssueDate) ? mRZIDCard.IssueDate : rdCITx.IssueDate;
                rdCITx.ExprationDate = !string.IsNullOrEmpty(mRZIDCard.ExpirationDate) ? mRZIDCard.ExpirationDate : rdCITx.ExprationDate;
                rdCITx.BirthDate = !string.IsNullOrEmpty(mRZIDCard.BirthDate) ? mRZIDCard.BirthDate : rdCITx.BirthDate;
                rdCITx.GeoRef = !string.IsNullOrEmpty(mRZIDCard.GeoRef) ? mRZIDCard.GeoRef : rdCITx.GeoRef;
                rdCITx.WorkstationId = !string.IsNullOrEmpty(mRZIDCard.WorkStationID) ? mRZIDCard.WorkStationID : rdCITx.WorkstationId;
                rdCITx.Serial = !string.IsNullOrEmpty(mRZIDCard.Serial) ? mRZIDCard.Serial : rdCITx.Serial;
                rdCITx.Sex = !string.IsNullOrEmpty(mRZIDCard.Sex) ? mRZIDCard.Sex : rdCITx.Sex;
                rdCITx.Nacionality = !string.IsNullOrEmpty(mRZIDCard.Nacionality) ? mRZIDCard.Nacionality : rdCITx.Nacionality;

                AdministradorRdCITx.UpdateCITx(rdCITx, out msgerr);
                if (!string.IsNullOrEmpty(msgerr)) LOG.Warn("CIServiceManager.UpdateCITX UpdateCITx Error = " + msgerr);
                ret = true;
            }
            catch (Exception ex)
            {
                LOG.Error("CIServiceManager.UpdateCITX Exception => " + ex.Message);
                ret = false;
            }
            return ret;
        }

        /// <summary>
        /// Dado un pedido desde una compañía con user / pasw, le genrea un QR temporal para hacer una CI en cierto tiempo configurado.
        /// Se entrega el mail donde se enviara la copia de la certificacion, y nombre de la persona ademas de de su rut
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        private static int GetTokenForCI(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /*  
             *  1.- Serializo param
             *  2.- Chequeo que exista en BD sino => Retorno que no existe 
             *  3.- Si existe => Genero QR con los datos enviados
             *  4.- Retorno
            */
            int ret = Errors.IRET_OK;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;
            try
            {
                LOG.Debug("CIServiceManager.GetTokenForCI IN...");

                //*  2.- Genero RD xml y firmo 
                DateTime dNow = DateTime.Now;
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                //3.- Chequeo que esté el id de aplicación, 
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string app;
                int qext;
                int retaux = CheckAppAndCompanyEnabled(oXmlIn.ApplicationId, out qext, out app, out msg);
                if (retaux < 0)
                {
                    oXmlOut.Message = msg;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Error("CIServiceManager.GetTokenForCI Error chequeando ingreso - msg=" + msg); 
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }

                //*  2.- Check en BD 
                CopyParams(oXmlIn, out oXmlOut);
                msg = null;
                string trackidnew = AdministradorRdCITx.GetTrackIdUnique();
                //string qrgenerated = GetQR(GetJSONForQR("NV_CI_TackId=" + trackidnew + "&typeid=" + oXmlIn.CIParam.TypeId + "&valueid=" + oXmlIn.CIParam.ValueId +
                //                            "&URL=" + Properties.Settings.Default.BASE_URL_NV_SERVICE);
                string qrgenerated = GetQR(GetJSONForQR(trackidnew, oXmlIn.Companyid, oXmlIn.ApplicationId, 
                                                        oXmlIn.CIParam.TypeId, oXmlIn.CIParam.ValueId,
                                                        Properties.Settings.Default.BASE_URL_NV_SERVICE,
                                                        Properties.Settings.Default.BASE_CI_CHECK, 
                                                        true, BPHelper.CheckEnrolled(oXmlIn), oXmlIn.CIParam.DynamicParam));

                LOG.Debug("CIServiceManager.GetTokenForCI Generando rdCITx con trackid = " + trackidnew);
                string dp = oXmlIn.CIParam.DynamicParam != null ?
                                           JsonConvert.SerializeObject(oXmlIn.CIParam.DynamicParam) : null;
                RdCITx rdCITx = AdministradorRdCITx.CreateCITx(trackidnew, oXmlIn.Actionid, oXmlIn.Companyid, oXmlIn.CIParam.ValidityType, 
                                                               qrgenerated, oXmlIn.CIParam.TypeId, oXmlIn.CIParam.ValueId,
                                                               oXmlIn.CIParam.DestinaryMail, dp, out msg);

                if (rdCITx == null)
                {
                    oXmlOut.Message = msg;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    LOG.Error("CIServiceManager.GetTokenForCI - Error generando rdCTTx = " + msg);
                    return Errors.IRET_ERR_GENERATINGQR;
                }
                else
                {
                    oXmlOut.CIParam.TrackId = rdCITx.TrackId;
                    oXmlOut.CIParam.TypeId = rdCITx.TypeId;
                    oXmlOut.CIParam.ValueId = rdCITx.ValueId;
                    oXmlOut.CIParam.DateGenToken = rdCITx.DateGen.ToString("dd/MM/yyyy HH:mm:ss");
                    oXmlOut.CIParam.QRTokenValidity = rdCITx.QRValidity;
                    oXmlOut.Message = "Se genero QR";
                    oXmlOut.QR = qrgenerated;
                    oXmlOut.CIParam.QRTokenValidity = Properties.Settings.Default.QRValidity;
                    LOG.Debug("CIServiceManager.GetTokenForCI Con trackid = " + trackidnew + " - QRGenerated = " + qrgenerated);
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    ret = Errors.IRET_OK;
                }

            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }
            LOG.Debug("CIServiceManager.GetTokenForCI OUT [ret = " + ret + "]");
            return ret;
        }

   

        /// <summary>
        /// Dada un trackid de CI = ReciboId,  se verifica que la CI se haya hecho completa y se retorna el 
        /// recibo y el PDF si se generó ok, sino se retorna que error dio
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        private static int GetCI(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /* 
             *  1.- Serializo param
             *  2.- Chequeo que exista RDCITx en BD sino => Retorno que no existe 
             *  3.- Si existe => Check que este completo sino retonro error
             *  4.- Si está ok, recupero recibo y PDF y retorno 
            */
            int ret = Errors.IRET_OK;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;
            try
            {
                LOG.Debug("CIServiceManager.GetCI IN...");
                //*  2.- Genero RD xml y firmo 
                DateTime dNow = DateTime.Now;
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                // AGREGAR CONTROL DE LOGIN
                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string app;
                int qext;
                //int retaux = CheckAppAndCompanyEnabled(oXmlIn.ApplicationId, out qext, out app, out msg);
                //if (retaux < 0)
                //{
                //    oXmlOut.Message = msg;
                //    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                //}

                //*  2.- Check en BD si está la RdCITX via el trackid
                CopyParams(oXmlIn, out oXmlOut);
                msg = null;
                //Added 26-01 para cotrol de username que realiza la verificacion. Si viene nulo no importa, sino se graba
                string username = Utils.GetUserName(oXmlIn.Additionaldata);
                RdCITx rdCITx = AdministradorRdCITx.GetCITxByTrackId(oXmlIn.Actionid, oXmlIn.CIParam.TrackId, oXmlIn.Companyid, 0, 
                                            false, oXmlIn.CIParam.TypeId, oXmlIn.CIParam.ValueId, null, oXmlIn.CIParam.DestinaryMail,
                                            oXmlIn.CIParam.WorkStationID, oXmlIn.CIParam.GeoRef, username, out msg);

                if (rdCITx == null)
                {
                    oXmlOut.Message = "La transaccion con TrackId = " + oXmlIn.CIParam.TrackId + " no se encuentra en el repositorio seguro";
                    LOG.Info("CIServiceManager.GetCI - " + oXmlOut.Message);
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    ret = Errors.IRET_ERR_CI_NO_IN_DB;
                }
                else
                { 
                    RdRecibos dbRecibo = AdministradorRdRecibos.BuscarRdReciboById(rdCITx.ReceiptId);

                    if (dbRecibo == null)
                    {
                        oXmlOut.Message = "El recibo  con ID = " + rdCITx.ReceiptId + " no se encuentra en el repositorio seguro";
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        ret = Errors.IRET_ERR_RECEIPT_NO_IN_DB;
                    }
                    else
                    {
                        msg = null;
                        oXmlOut.Receipt = dbRecibo.Reciboxml;
                        oXmlOut.CIParam.CertificatePDF = rdCITx.CertifyPDF;
                        oXmlOut.QR = GetQR(Properties.Settings.Default.BASE_CI_CHECK + "?trackid=" + rdCITx.TrackId);
                        oXmlOut.ResultVerify = rdCITx.ResultCode;
                        oXmlOut.CIParam.DateGenToken = rdCITx.DateGen.ToString("dd/MM/yyyy");
                        oXmlOut.CIParam.QRTokenValidity = rdCITx.QRValidity;
                        oXmlOut.CIParam.DestinaryMail = rdCITx.DestinataryMail;
                        oXmlOut.CIParam.Name = rdCITx.Name;
                        oXmlOut.CIParam.PhaterLastName = rdCITx.PhaterLastName;
                        oXmlOut.CIParam.MotherLastName = rdCITx.MotherLastName;
                        oXmlOut.CIParam.Nacionality = rdCITx.Nacionality;
                        oXmlOut.CIParam.Sex = rdCITx.Sex;
                        oXmlOut.CIParam.BirthDate = rdCITx.BirthDate;
                        oXmlOut.CIParam.ExpirationDate = rdCITx.ExprationDate;
                        oXmlOut.CIParam.WorkStationID = rdCITx.WorkstationId;
                        oXmlOut.CIParam.GeoRef = rdCITx.GeoRef;
                        oXmlOut.CIParam.DestinaryMail = rdCITx.DestinataryMail;
                        oXmlOut.CIParam.TrackId = rdCITx.TrackId;
                        oXmlOut.CIParam.TypeId = rdCITx.TypeId;
                        oXmlOut.CIParam.ValueId = rdCITx.ValueId;
                        oXmlOut.CIParam.IDCardImageFront = rdCITx.IdCardImageFront;
                        oXmlOut.CIParam.IDCardImageBack = rdCITx.IdCardImageBack;
                        oXmlOut.CIParam.IDCardPhotoImage = rdCITx.IdCardPhotoImage;
                        oXmlOut.CIParam.IDCardSignatureImage = rdCITx.IdCardSignatureImage;
                        oXmlOut.CIParam.Selfie = rdCITx.Selfie;
                        oXmlOut.CIParam.Serial = rdCITx.Serial;
                        oXmlOut.CIParam.IssueDate = rdCITx.IssueDate;
                        oXmlOut.CIParam.Score = rdCITx.Score;
                        oXmlOut.CIParam.Threshold = rdCITx.Threshold;
                        oXmlOut.CIParam.Map = rdCITx.Map;
                        oXmlOut.Message = "La CI con TrackId = " + rdCITx.TrackId + " existe.";
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        ret = rdCITx.ResultCode;
                    }
                }
            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }

            return ret;
        }


        #region Auxiliares

        /// <summary>
        /// Copia parametros de entrada en los de salida
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="oXmlOut"></param>
        private static void CopyParams(XmlParamIn oXmlIn, out XmlParamOut oXmlOut)
        {
            oXmlOut = new XmlParamOut();
            try
            {
                oXmlOut.Actionid = oXmlIn.Actionid;
                oXmlOut.Clientid = oXmlIn.Clientid;
                oXmlOut.Companyid = oXmlIn.Companyid;
                oXmlOut.Enduser = oXmlIn.Enduser;
                oXmlOut.Ipenduser = oXmlIn.Ipenduser;
                oXmlOut.Origin = oXmlIn.Origin;
                oXmlOut.CIParam = oXmlIn.CIParam;
                //oXmlOut. = oXmlIn;

            }
            catch (Exception ex)
            {
                LOG.Error("ServiceManager.CopyParams Error = " + ex.Message);
            }
        }

        /// <summary>
        /// GEnera un QR en base64 con el parámtero indicado como urlci
        /// </summary>
        /// <param name="urlci"></param>
        /// <returns></returns>
        internal static string GetQR(string urlci)
        {
            string qr = "";
            try
            {
                Bio.Core.QR.FormQR formqr = new Bio.Core.QR.FormQR();
                qr = formqr.GetImgQRB64(urlci);
                LOG.Debug("ciServiceManager.GetQR QR Generated = " + qr);
            }
            catch (Exception ex)
            {
                qr = null;
                LOG.Error("ciServiceManager.GetQR Error - " + ex.Message);
            }
            return qr;
        }


        private static string GetJSONForQR(string trackidnew, int companyid, int applicationid, string typeId, string valueId, 
            string base_Url, string base_Url_Check, bool locationEnabled, int enrolled, List<Param> dynamicparam)
        {
            string json = "";
            try
            {
                JsonQR jsonqr = new JsonQR(trackidnew, companyid, applicationid, typeId, valueId, base_Url, 
                                            base_Url_Check, locationEnabled, enrolled);

                //Added 15-11-2020 por parametros variables
                /*
                bool LivenessEnabled = true

                Timeout para el proceso completo de liveness
                int LivenessTimeout = 10000

                Indica si se debe detener el proceso si la cara se pierde de la cámara
                bool LivenessFaceMissing = true

                Indica la seleccion de camara por defecto para la selfie (sin liveness), front/back
                string SelfieFacing = "front"
                */
                if (dynamicparam != null)
                {
                    if (CIParam.ContainsKey("LivenessEnabled", dynamicparam))
                    {
                        jsonqr.LivenessEnabled = Convert.ToBoolean(CIParam.GetValue("LivenessEnabled", dynamicparam));
                    }
                    if (CIParam.ContainsKey("LivenessTimeout", dynamicparam))
                    {
                        jsonqr.LivenessTimeout = Convert.ToInt32(CIParam.GetValue("LivenessTimeout", dynamicparam));
                    }
                    if (CIParam.ContainsKey("LivenessFaceMissing", dynamicparam))
                    {
                        jsonqr.LivenessFaceMissing = Convert.ToBoolean(CIParam.GetValue("LivenessFaceMissing", dynamicparam));
                    }
                    if (CIParam.ContainsKey("SelfieFacing", dynamicparam))
                    {
                        jsonqr.SelfieFacing = CIParam.GetValue("SelfieFacing", dynamicparam);
                    }
                }

                json = Newtonsoft.Json.JsonConvert.SerializeObject(jsonqr);
                LOG.Debug("ciServiceManager.GetJSONForQR QR Generated = " + json);
            }
            catch (Exception ex)
            {
                json = null;
                LOG.Error("ciServiceManager.GetJSONForQR Error - " + ex.Message);
            }
            return json;
        }


        #endregion Auxiliares

        /// <summary>
        /// Idem de GenerateCI_w_FotoCedulaVsSelfie pero sin hacer OCR ni Verificacion Facial porque está hecho desde fuera
        /// en nuestro Web CI (Con Facetec o Jumio)
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        internal static int GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /* 
             *  1.- Chequeo Acceso
             *  2.- Verifico que la info venga ok
             *  3.- Depende resultado de 2
             *      3.1.- Si dio negativo, Salgo sin respuesta (Veo de grabar tx con resultado para log
             *      3.2.- Si dio positivo
             *            3.2.1.- Creo Recibo XML y guardo en BD
             *            3.2.2.- Creo PDF certificado con servicio de DE y guardo en BD
             *  4.- Retorno 
            */
            int ret = Errors.IRET_OK;
            string sXMLTS = null;
            int resTS = 0;
            string sRDEnvelope = null;
            bool isSaveOK = false;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;

            try
            {
                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess IN...");

                string reciboID = AdministradorRdCITx.GetTrackIdUnique();// Guid.NewGuid().ToString("N");
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                // AGREGAR CONTROL DE LOGIN
                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string strapp;
                int qext = 0;
                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess CheckAppAndCompanyEnabled IN...");
                int retaux = CheckAppAndCompanyEnabled(oXmlIn.ApplicationId, out qext, out strapp, out msg);
                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess CheckAppAndCompanyEnabled Out...");
                if (retaux < 0)
                {
                    oXmlOut.Message = msg;
                    LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess Error chequeando ingreso - msg=" + msg);
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return retaux;
                }
                else  //Si esta todo ok, proceso 
                {
                    //Obtengo rdCITx si existe con el TrackId del parametro de entrada, que es el trackid generado 
                    //para la emision de un QR. Si existe, chequeo que no haya pasado mas tiempo que el configurado en 
                    //Config (5 minutos a priori)
                    msg = null;
                    LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess Recuperando rdCITx con TrackId = " + oXmlIn.CIParam.TrackId);
                    //Added 26-01 para cotrol de username que realiza la verificacion. Si viene nulo no importa, sino se graba
                    string username = Utils.GetUserName(oXmlIn.Additionaldata);
                    RdCITx rdCITx = AdministradorRdCITx.GetCITxByTrackId(oXmlIn.Actionid, oXmlIn.CIParam.TrackId,
                                                                         oXmlIn.Companyid, oXmlIn.CIParam.ValidityType, true,
                                                                         oXmlIn.CIParam.TypeId, oXmlIn.CIParam.ValueId, null,
                                                                         oXmlIn.CIParam.DestinaryMail, oXmlIn.CIParam.WorkStationID,
                                                                         oXmlIn.CIParam.GeoRef, username, out msg);

                    if (rdCITx == null)
                    {
                        oXmlOut.Message = msg;
                        LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess Error recuperanto rcCITx - msg=" + msg);
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_CI_TRACKID_NO_IN_DB;
                    } else
                    {
                        //Copio trackid para actualizar luego en la BD. Dado que recien se creo la Tx debo tomar aca el TrackId
                        oXmlIn.CIParam.TrackId = rdCITx.TrackId;  
                    }
                    //else if (rdCITx.ActionId == 3 || rdCITx.ActionId == 33 || rdCITx.ActionId == 4 || rdCITx.ActionId == 44 ||
                    //         IsTrackIdVencida(rdCITx))
                    //{
                    //    oXmlOut.Message = "TrackId ya utilizada en un proceso de CI terminado " +
                    //        "[ActionId = " + rdCITx.ActionId.ToString() + " | Fecha Fin = " + rdCITx.LastModify.ToString("dd/MM/yyy HH:mm:ss") + "]";
                    //    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    //    LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess Error recuperndo rdCITx = " + oXmlOut.Message);
                    //    return Errors.IRET_ERR_CI_TRACKID_COMPLETED;
                    //}

                    //Added para manejar el llamado liviano, que no venga la info de cedula porque ya se hizo => llenamos oXmlIn desde BD
                    // sino, si el estado es que no se hizo IDCardVerification antes, lo hacemos, y llenamos oXmlIn para seguir
                    bool sigue = true;
                    //if (rdCITx.ActionId == 2 || rdCITx.ActionId == 22) //Si es 2 o 22 se hizo ya el IDCardVerification => lleno oXmlIn desde BD
                    //{
                    //    sigue = CopyFromBDtoParamin(rdCITx, oXmlIn, out oXmlIn);
                    //}
                    //else
                    //{
                    //    oXmlIn.CIParam.TrackId = rdCITx.TrackId;
                    //    sigue = VerififyIdCardAndFillParamin(oXmlIn, out oXmlIn);
                    //}

                    //if (!sigue)
                    //{
                    //    oXmlOut.Message = msg;
                    //    LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess Error recuperanto rcCITx - msg=" + msg);
                    //    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    //    return Errors.IRET_ERR_CI_NEGATIVE_VERIFICATION;
                    //}
                    //else
                    //{
                        //Actualizo el ValueId si se envió "NA" en la llamada
                        if (rdCITx.ValueId.Equals("NA") && !oXmlIn.CIParam.ValueId.Equals("NA"))
                        {
                            rdCITx.ValueId = oXmlIn.CIParam.ValueId;
                            string msgerr = "";
                            AdministradorRdCITx.UpdateCITx(rdCITx, out msgerr);
                            LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess Actualiza CITx.Id = " + rdCITx.Id +
                                          " con ValueId = " + rdCITx.ValueId + " [MsgRet = " +
                                          (string.IsNullOrEmpty(msgerr) ? "Sin Error" : msgerr));
                        }
                    //}


                    //*  2.- Hago verify contra bioportal
                    msg = null;
                    int resultverify = 0;
                    double score = 0;
                    string TrackIdBP = null;
                    LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess Verificando contra BP...");
                    int rptaBP = ExtractDataVerificationFromParamIn(oXmlIn, out msg, out resultverify, out score, out TrackIdBP); //BPHelper.VerifyCIvsCedula(oXmlIn, out msg, out resultverify, out score, out TrackIdBP);

                    if (rptaBP < 0)
                    {
                        oXmlOut.Message = "Error verificando identidad [Err=" + rptaBP + " - MsgErr=" + msg + "]";
                        LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - " + oXmlOut.Message);
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_CI_VERYFING;
                    }
                    else
                    {
                        //Genero Certificado PDF con DE si que verifico positivo, sino error
                        if (resultverify == 2) //Verificacion negativa
                        {
                            oXmlOut.Message = "Verificación Negativa [Score=" + score.ToString() + "]";
                            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - " + oXmlOut.Message);
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                            return Errors.IRET_ERR_CI_NEGATIVE_VERIFICATION;
                        }
                        else
                        {
                            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - Verificación Positiva [Score=" + score.ToString() + "]");
                            //Genero PDF via DE
                            string PdfCertifyB64;
                            string PdfDecJuradaB64;
                            string QRverify = GetQR(Properties.Settings.Default.BASE_CI_CHECK + "?trackid=" + oXmlIn.CIParam.TrackId);
                            string TrackIDE = null;
                            string Map = null;
                            msg = null;
                            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - Generando Certificado PDF...");
                            int rptaDE = DEHelper.GeneraCertifyPDF(oXmlIn, score, QRverify, out msg,
                                out PdfCertifyB64, out PdfDecJuradaB64, out TrackIDE, out Map);

                            if (rptaDE < 0)
                            {
                                oXmlOut.Message = "Error generando PDF Certify [Err=" + rptaDE + " - MsgErr=" + msg + "]";
                                LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - " + oXmlOut.Message);
                                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                return Errors.IRET_ERR_CI_PDF_GENERATING;
                            }
                            else
                            {
                                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - Certificado PDF Generado = " + PdfCertifyB64);

                                oXmlIn.CIParam.Map = Map;
                                DateTime dNow = DateTime.Now;
                                RdExtensionItem[] ext = new RdExtensionItem[24];
                                RdExtensionItem item;

                                int index = 0;
                                item = new RdExtensionItem();
                                item.key = "TrackIdCI";
                                item.value = oXmlIn.CIParam.TrackId;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "IDCardImage";
                                item.value = oXmlIn.CIParam.IDCardImageFront;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "IDCardPhotoImage";
                                item.value = oXmlIn.CIParam.IDCardPhotoImage;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "IDCardSignatureImage";
                                item.value = oXmlIn.CIParam.IDCardSignatureImage;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Selfie";
                                item.value = oXmlIn.CIParam.Selfie;
                                ext[index++] = item;

                                item = new RdExtensionItem();
                                item.key = "TypeId";
                                item.value = oXmlIn.CIParam.TypeId;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "ValueId";
                                item.value = oXmlIn.CIParam.ValueId;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Name";
                                item.value = (string.IsNullOrEmpty(oXmlIn.CIParam.Name) ? "" : oXmlIn.CIParam.Name) + " " +
                                             (string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName) ? "" : oXmlIn.CIParam.PhaterLastName) + " " +
                                             (string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName) ? "" : oXmlIn.CIParam.MotherLastName); ;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "BirthDate";
                                item.value = oXmlIn.CIParam.BirthDate;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "IssueDate";
                                item.value = oXmlIn.CIParam.IssueDate;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Sex";
                                item.value = (!string.IsNullOrEmpty(oXmlIn.CIParam.Sex) &&
                                              (oXmlIn.CIParam.Sex.Equals("M") || oXmlIn.CIParam.Sex.Equals("F"))) ?
                                              oXmlIn.CIParam.Sex : "Desconocido";
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Nacionality";
                                item.value = oXmlIn.CIParam.Nacionality;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Serial";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.Serial) ? "" : oXmlIn.CIParam.Serial;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "ExpirationDate";
                                item.value = oXmlIn.CIParam.ExpirationDate;
                                ext[index++] = item;

                                item = new RdExtensionItem();
                                item.key = "GeoRef";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef) ? "" : oXmlIn.CIParam.GeoRef;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "WorkStationID";
                                item.value = oXmlIn.CIParam.WorkStationID;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "ValidityType";
                                item.value = oXmlIn.CIParam.ValidityType.ToString();
                                ext[index++] = item;
                                //item = new RdExtensionItem();
                                //item.key = "";
                                //item.value = oXmlIn.CIParam;
                                //ext[index++] = item;

                                item = new RdExtensionItem();
                                item.key = "PDFCICertify";
                                item.value = PdfCertifyB64;
                                ext[index++] = item;
                                //item = new RdExtensionItem();
                                //item.key = "PDFCIDeclaracionJurada";
                                //item.value = PdfDecJuradaB64;
                                //ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "QRCIVerify";
                                item.value = QRverify;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Score";
                                item.value = score.ToString("##.##") + "%";
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Pais";
                                item.value = "Chile";
                                ext[index++] = item;

                                item = new RdExtensionItem();
                                item.key = "IDCardImageBack";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? "" : oXmlIn.CIParam.IDCardImageBack;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "ManualSignatureImage";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.ManualSignatureImage) ? "" : oXmlIn.CIParam.ManualSignatureImage;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "URLVideo";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.URLVideo) ? "" : oXmlIn.CIParam.URLVideo;
                                ext[index++] = item;

                                //item = new RdExtensionItem();
                                //item.key = "IDCardPhotoImageDJ";
                                //item.value = oXmlIn.CIParam.IDCardPhotoImage;
                                //ext[index++] = item;
                                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - Generando Recibo Digital - ReciboId = " + reciboID);
                                Rd objRD = new Rd(ip, strapp, oXmlIn.Origin.ToString(), dNow, oXmlIn.Description, ext, reciboID, 1);

                                //Firmo RD
                                string xmlTosign = Bio.Digital.Receipt.libs.XmlUtils.SerializeObject(objRD);
                                string xmlsigned = null;
                                string msgerr = null;
                                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - Firmando Recibo Digital - ReciboId = " + reciboID);
                                int ierr = XMLSignatureHelper.Firmar(oXmlIn.ApplicationId, reciboID, xmlTosign, out xmlsigned, out msgerr);

                                //Si está habilitado el TSA => Envio
                                if (ierr == 0)
                                {
                                    if (Global.TSA_ENABLED)
                                    {
                                        LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - Agregando TS al Recibo Digital - ReciboId = " + reciboID);
                                        resTS = Global.TSA_MANAGER.GenerateTS(Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(xmlsigned)), out sXMLTS);
                                        if (resTS == 0)
                                        {
                                            sRDEnvelope = "<RDEnvelope>" + xmlsigned + sXMLTS + "</RDEnvelope>";
                                            isSaveOK = true;
                                        }
                                        else
                                        {
                                            isSaveOK = false;
                                            oXmlOut.Message = "Error generando TS";
                                            LOG.Error(oXmlOut.Message);
                                            oXmlOut.Receipt = null;
                                            ret = Errors.IRET_ERR_TSA;
                                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                        }
                                    }
                                    else
                                    {
                                        isSaveOK = true;
                                        sRDEnvelope = "<RDEnvelope>" + xmlsigned + "</RDEnvelope>";
                                    }

                                    if (isSaveOK)
                                    {
                                        try
                                        {
                                            CopyParams(oXmlIn, out oXmlOut);
                                            RdRecibos ord = new RdRecibos();
                                            ord.Reciboid = reciboID;
                                            ord.Descripcion = string.IsNullOrEmpty(oXmlIn.Description) ? "Certificacion de Identidad" : oXmlIn.Description;
                                            ord.Fecha = dNow;
                                            ord.Iporigen = HttpContext.Current.Request.UserHostAddress;
                                            ord.RdAplicaciones = AdministradorRdAplicaciones.BuscarRdAplicacionesById(oXmlIn.ApplicationId);
                                            ord.Reciboxml = sRDEnvelope; // xmlsigned;
                                            ord.Version = "1.0";
                                            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - Salvando en BD el Recibo Digital - ReciboId = " + reciboID);
                                            ord = AdministradorRdRecibos.Save(ord);
                                            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - Salvado en BD el Recibo Digital - ReciboId = " + reciboID);
                                            //PersistentManager.session().Save(ord);
                                            //PersistentManager.session().Flush();

                                            //Update RdCITx
                                            msg = null;
                                            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - Updating rdCITx en BD TrackId = " + oXmlIn.CIParam.TrackId);
                                            AdministradorRdCITx.UpdateCITx(oXmlIn, ord, TrackIdBP, TrackIDE, PdfCertifyB64, resultverify, score, Properties.Settings.Default.CIBPTreshold,
                                                                           oXmlIn.CIParam.Name, oXmlIn.CIParam.PhaterLastName, oXmlIn.CIParam.MotherLastName, null, out msg);
                                            //AdministradorRdCITx.UpdateCITx(oXmlIn, ord, TrackIdBP, TrackIDE, PdfCertifyB64, resultverify, out msg);

                                            oXmlOut.Receipt = sRDEnvelope; //xmlsigned;
                                            oXmlOut.Trackid = reciboID;
                                            oXmlOut.CIParam.TrackId = rdCITx.TrackId;
                                            oXmlOut.CIParam.Score = score.ToString();
                                            oXmlOut.ResultVerify = resultverify;
                                            oXmlOut.CIParam.CertificatePDF = PdfCertifyB64;
                                            oXmlOut.QR = QRverify;
                                            oXmlOut.Timestampend = DateTime.Now;

                                            if (Properties.Settings.Default.CIInformResultByMail == 1)
                                            {
                                                //Added 10-05-2020 - Send mail HTMl con certificados attachados. MAndo PDF + XML
                                                LOG.Debug("Informo por mail " + oXmlOut.CIParam.DestinaryMail + " a destinatario enviadno docs...");
                                                string[] strbody = new string[2];
                                                strbody[0] = "Usted completó la Certificación de identidad para el documento <b>" +
                                                                oXmlOut.CIParam.TypeId + oXmlOut.CIParam.ValueId + "</b>";
                                                strbody[1] = "El codigo unico del tramite para consultas es: <b>" + oXmlOut.CIParam.TrackId + "</b>";
                                                byte[] docpdf = Convert.FromBase64String(oXmlOut.CIParam.CertificatePDF);
                                                byte[] docxml = System.Text.Encoding.UTF8.GetBytes(oXmlOut.Receipt);
                                                //Send PDF

                                                bool bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                                                                    "Certificación de Identidad completada con exito [PDF]", strbody, docpdf, "pdf",
                                                                    oXmlOut.CIParam.TrackId + ".pdf");
                                                LOG.Debug("Enviado PDF => " + (bret.ToString()));
                                                //bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                                                //                    "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
                                                //                    oXmlOut.CIParam.TrackId + ".xml");
                                                //LOG.Debug("Enviado XML => " + (bret.ToString()));
                                            }

                                            //Added 20-04-2020 - Return CallBack generico si UrlCallback != null en RdAplicaciones
                                            CheckSendCallback(ord.RdAplicaciones, sRDEnvelope);

                                            if (oXmlIn.Actionid == 88) //Pide respuesta liviana, por lo que se elimina todo lo pesado
                                            {
                                                oXmlOut.Receipt = null;
                                                oXmlOut.CIParam.CertificatePDF = null;
                                                oXmlOut.CIParam.IDCardImageFront = null;
                                                oXmlOut.CIParam.IDCardImageBack = null;
                                                oXmlOut.CIParam.IDCardPhotoImage = null;
                                                oXmlOut.CIParam.IDCardSignatureImage = null;
                                                oXmlOut.CIParam.Selfie = null;
                                                oXmlOut.CIParam.Map = null;
                                                oXmlOut.QR = null;
                                            }

                                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);

                                            ret = Errors.IRET_OK;
                                        }
                                        catch (Exception exDB)
                                        {
                                            LOG.Error("Error salvando en BD [" + exDB.Message + "]");
                                            oXmlOut.Message = "Error salvando en BD [" + exDB.Message + "]";
                                            oXmlOut.Receipt = null;
                                            ret = Errors.IRET_ERR_SAVE_DB;
                                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                        }
                                    }
                                }
                                else
                                {
                                    oXmlOut.Message = "Error Firmando [" + msgerr + "]";
                                    LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess - " + oXmlOut.Message);
                                    oXmlOut.Receipt = null;
                                    ret = Errors.IRET_ERR_SIGNATURE;
                                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                LOG.Error(oXmlOut.Message);
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }
            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess OUT!");
            return ret;
        }

        internal static int GenerateSignerTx_From_BPWeb(RdCITx tx, XmlParamIn oXmlIn, int _resultVerify, double _score, 
                                                        bool sendMails, string jsonform, out string xmlparamout)
        {
            /* 
             *  1.- Verifico que la info venga ok
             *  2.- Sin Importar el resultado de verificacion
             *     2.1.- Si dio positivo o Negativo
             *            2.2.1.- Creo Recibo XML y guardo en BD
             *            2.2.2.- Creo PDF con Poder de TramitesTag con servicio de DE y guardo en BD
             *  3.- Retorno 
            */
            int ret = Errors.IRET_OK;
            string sXMLTS = null;
            int resTS = 0;
            string sRDEnvelope = null;
            bool isSaveOK = false;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;
            string securitycode = "Null";

            try
            {
                LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb IN...");

                string reciboID = AdministradorRdRecibos.GetTrackIdUnique();// Guid.NewGuid().ToString("N");
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip))
                    ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9)
                    ip = "127.0.0.1";

                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb CheckAppAndCompanyEnabled IN...");
                string msg;
                string strapp;
                int qext = 0;
                LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb CheckAppAndCompanyEnabled IN...");
                RdAplicaciones app = AdministradorRdAplicaciones.BuscarRdAplicacionesById(oXmlIn.ApplicationId);
                LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb CheckAppAndCompanyEnabled Out...");
                if (app == null)
                {
                    oXmlOut.Message = "Recuperacion ApplicationId = " + oXmlIn.ApplicationId + " => NUll";
                    LOG.Error("CIServiceManager.GenerateSignerTx_From_BPWeb Recuperacion ApplicationId = " + oXmlIn.ApplicationId + " => NUll");
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_API_ERR_NOT_FOUND * -1;
                }
                msg = null;
                LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb Generando CI con TrackId = " + oXmlIn.CIParam.TrackId);
                RdCITx rdCITx = tx; //Viene por parametro
                                    //AdministradorRdCITx.GetCITxByTrackId(oXmlIn.Actionid, oXmlIn.CIParam.TrackId,
                                    //                                                    oXmlIn.Companyid, oXmlIn.CIParam.ValidityType, true,
                                    //                                                    oXmlIn.CIParam.TypeId, oXmlIn.CIParam.ValueId, null,
                                    //                                                    oXmlIn.CIParam.DestinaryMail, out msg);

                if (rdCITx == null)
                {
                    oXmlOut.Message = msg;
                    LOG.Error("CIServiceManager.GenerateSignerTx_From_BPWeb Error recuperanto rcCITx - msg=" + msg);
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_CI_TRACKID_NO_IN_DB;
                }
                else
                {
                    //Copio trackid para actualizar luego en la BD. Dado que recien se creo la Tx debo tomar aca el TrackId
                    oXmlIn.CIParam.TrackId = rdCITx.TrackId;
                }


                //Added para manejar el llamado liviano, que no venga la info de cedula porque ya se hizo => llenamos oXmlIn desde BD
                // sino, si el estado es que no se hizo IDCardVerification antes, lo hacemos, y llenamos oXmlIn para seguir
                bool sigue = true;

                //Actualizo el ValueId si se envió "NA" en la llamada
                if (rdCITx.ValueId.Equals("NA") && !oXmlIn.CIParam.ValueId.Equals("NA"))
                {
                    rdCITx.ValueId = oXmlIn.CIParam.ValueId;
                    string msgerr = "";
                    AdministradorRdCITx.UpdateCITx(rdCITx, out msgerr);
                    LOG.Error("CIServiceManager.GenerateSignerTx_From_BPWeb Actualiza CITx.Id = " + rdCITx.Id +
                                    " con ValueId = " + rdCITx.ValueId + " [MsgRet = " +
                                    (string.IsNullOrEmpty(msgerr) ? "Sin Error" : msgerr));
                }
                //}


                //*  2.- Hago verify contra bioportal
                msg = null;
                int resultverify = 0;
                double score = 0;
                string TrackIdBP = tx.TrackIdBP;
                LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb Verificando contra BP...");
                resultverify = _resultVerify;
                score = _score;
                //int rptaBP = ExtractDataVerificationFromParamIn(oXmlIn, out msg, out resultverify, out score, out TrackIdBP); //BPHelper.VerifyCIvsCedula(oXmlIn, out msg, out resultverify, out score, out TrackIdBP);

                //if (rptaBP < 0)
                //{
                //    oXmlOut.Message = "Error verificando identidad [Err=" + rptaBP + " - MsgErr=" + msg + "]";
                //    LOG.Error("CIServiceManager.GenerateCI_From_BPWeb - " + oXmlOut.Message);
                //    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IRET_ERR_CI_VERYFING;
                //}
                //else
                //{
                //Genero Certificado PDF con DE si que verifico positivo, sino error
                //if (resultverify == 2) //Verificacion negativa
                //{
                //    //TODO - Grabar en BD que el resultado fue negativo
                //    oXmlOut.Message = "Verificación Negativa [Score=" + score.ToString() + "]";
                //    LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - " + oXmlOut.Message);
                //    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IRET_ERR_CI_NEGATIVE_VERIFICATION;
                //}
                //else 

                //Es positivo o negativo => No es bloqueante, es solo informativo y se usa para enviar al DE para generar el Poder.
                if (resultverify == 1 || resultverify == 2) 
                {
                    LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Verificación " + 
                              (resultverify==1?"Positiva":"Negativa") + " [Score=" + score.ToString() + 
                              (string.IsNullOrEmpty(tx.Threshold)?"":"/ Umbral=" + tx.Threshold) + "]");
                    //Genero PDF via DE
                    string PdfCertifyB64;
                    string PdfDecJuradaB64;
                    string QRverify = null; //GetQR(Properties.Settings.Default.BASE_SIGNER_CHECK + "?trackid=" + oXmlIn.CIParam.TrackId);
                    string TrackIDE = null;
                    string Map = null;
                    msg = null;
                    LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Generando Documento PDF...");
                    int rptaDE = DEHelper.GeneraDEPDF(oXmlIn, score, QRverify, tx.Form, tx.CarRegisterImageFront, tx.CarRegisterImageBack, 
                                                out msg, out PdfCertifyB64, out PdfDecJuradaB64, out TrackIDE, out Map, out securitycode);

                    if (rptaDE < 0)
                    {
                        oXmlOut.Message = "Error generando PDF Document [Err=" + rptaDE + " - MsgErr=" + msg + "]";
                        LOG.Error("CIServiceManager.GenerateSignerTx_From_BPWeb - " + oXmlOut.Message);
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_CI_PDF_GENERATING;
                    }
                    else
                    {
                        LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Documento PDF Generado = " + PdfCertifyB64);

                        oXmlIn.CIParam.Map = Map;
                        DateTime dNow = DateTime.Now;
                        RdExtensionItem[] ext = new RdExtensionItem[29];
                        RdExtensionItem item;

                        int index = 0;
                        item = new RdExtensionItem();
                        item.key = "TrackIdCI";
                        item.value = oXmlIn.CIParam.TrackId;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "SecurityCode";
                        item.value = securitycode;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "IDCardImage";
                        item.value = oXmlIn.CIParam.IDCardImageFront;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "IDCardPhotoImage";
                        item.value = oXmlIn.CIParam.IDCardPhotoImage;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "IDCardSignatureImage";
                        item.value = oXmlIn.CIParam.IDCardSignatureImage;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Selfie";
                        item.value = oXmlIn.CIParam.Selfie;
                        ext[index++] = item;

                        item = new RdExtensionItem();
                        item.key = "TypeId";
                        item.value = oXmlIn.CIParam.TypeId;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "ValueId";
                        item.value = oXmlIn.CIParam.ValueId;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Name";
                        item.value = (string.IsNullOrEmpty(oXmlIn.CIParam.Name) ? "" : oXmlIn.CIParam.Name) + " " +
                                        (string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName) ? "" : oXmlIn.CIParam.PhaterLastName) + " " +
                                        (string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName) ? "" : oXmlIn.CIParam.MotherLastName);
                        ;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "BirthDate";
                        item.value = oXmlIn.CIParam.BirthDate;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "IssueDate";
                        item.value = oXmlIn.CIParam.IssueDate;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Sex";
                        item.value = (!string.IsNullOrEmpty(oXmlIn.CIParam.Sex) &&
                                        (oXmlIn.CIParam.Sex.Equals("M") || oXmlIn.CIParam.Sex.Equals("F"))) ?
                                        oXmlIn.CIParam.Sex : "Desconocido";
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Nacionality";
                        item.value = oXmlIn.CIParam.Nacionality;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Serial";
                        item.value = string.IsNullOrEmpty(oXmlIn.CIParam.Serial) ? "" : oXmlIn.CIParam.Serial;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "ExpirationDate";
                        item.value = oXmlIn.CIParam.ExpirationDate;
                        ext[index++] = item;

                        item = new RdExtensionItem();
                        item.key = "GeoRef";
                        item.value = string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef) ? "" : oXmlIn.CIParam.GeoRef;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "WorkStationID";
                        item.value = oXmlIn.CIParam.WorkStationID;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "ValidityType";
                        item.value = oXmlIn.CIParam.ValidityType.ToString();
                        ext[index++] = item;
                        //item = new RdExtensionItem();
                        //item.key = "";
                        //item.value = oXmlIn.CIParam;
                        //ext[index++] = item;

                        item = new RdExtensionItem();
                        item.key = "PDFCICertify";
                        item.value = PdfCertifyB64;
                        ext[index++] = item;
                        //item = new RdExtensionItem();
                        //item.key = "PDFCIDeclaracionJurada";
                        //item.value = PdfDecJuradaB64;
                        //ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "QRCIVerify";
                        item.value = QRverify;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Score";
                        item.value = score.ToString("##.##") + "%";
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Pais";
                        item.value = "Chile";
                        ext[index++] = item;

                        item = new RdExtensionItem();
                        item.key = "IDCardImageBack";
                        item.value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? "" : oXmlIn.CIParam.IDCardImageBack;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "ManualSignatureImage";
                        item.value = string.IsNullOrEmpty(oXmlIn.CIParam.ManualSignatureImage) ? "" : oXmlIn.CIParam.ManualSignatureImage;

                        //TODO agregar URL Video
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "URLVideo";
                        item.value = string.IsNullOrEmpty(oXmlIn.CIParam.URLVideo) ? "" : oXmlIn.CIParam.URLVideo;
                        ext[index++] = item;

                        //Agrego info de TramitesTAG
                        item = new RdExtensionItem();
                        item.key = "CarRegisterFront";
                        item.value = string.IsNullOrEmpty(tx.CarRegisterImageFront) ? "" : tx.CarRegisterImageFront;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "CarRegisterBack";
                        item.value = string.IsNullOrEmpty(tx.CarRegisterImageBack) ? "" : tx.CarRegisterImageBack;
                        ext[index++] = item;

                        item = new RdExtensionItem();
                        item.key = "Writing";
                        item.value = string.IsNullOrEmpty(tx.WritingImage) ? "" : tx.WritingImage;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Form";
                        item.value = string.IsNullOrEmpty(tx.Form) ? "" : tx.Form;
                        ext[index++] = item;

                        //item = new RdExtensionItem();
                        //item.key = "IDCardPhotoImageDJ";
                        //item.value = oXmlIn.CIParam.IDCardPhotoImage;
                        //ext[index++] = item;
                        LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Generando Recibo Digital - ReciboId = " + reciboID);
                        Rd objRD = new Rd(ip, app.Nombre, oXmlIn.Origin.ToString(), dNow, oXmlIn.Description, ext, reciboID, 1);

                        //Firmo RD
                        string xmlTosign = Bio.Digital.Receipt.libs.XmlUtils.SerializeObject(objRD);
                        string xmlsigned = null;
                        string msgerr = null;
                        LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Firmando Recibo Digital - ReciboId = " + reciboID);
                        int ierr = XMLSignatureHelper.Firmar(oXmlIn.ApplicationId, reciboID, xmlTosign, out xmlsigned, out msgerr);

                        //Si está habilitado el TSA => Envio
                        if (ierr == 0)
                        {
                            if (Global.TSA_ENABLED)
                            {
                                LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Agregando TS al Recibo Digital - ReciboId = " + reciboID);
                                resTS = Global.TSA_MANAGER.GenerateTS(Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(xmlsigned)), out sXMLTS);
                                if (resTS == 0)
                                {
                                    sRDEnvelope = "<RDEnvelope>" + xmlsigned + sXMLTS + "</RDEnvelope>";
                                    isSaveOK = true;
                                }
                                else
                                {
                                    isSaveOK = false;
                                    oXmlOut.Message = "Error generando TS";
                                    LOG.Error(oXmlOut.Message);
                                    oXmlOut.Receipt = null;
                                    ret = Errors.IRET_ERR_TSA;
                                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                }
                            }
                            else
                            {
                                isSaveOK = true;
                                sRDEnvelope = "<RDEnvelope>" + xmlsigned + "</RDEnvelope>";
                            }

                            if (isSaveOK)
                            {
                                try
                                {
                                    CopyParams(oXmlIn, out oXmlOut);
                                    RdRecibos ord = new RdRecibos();
                                    ord.Reciboid = reciboID;
                                    ord.Descripcion = string.IsNullOrEmpty(oXmlIn.Description) ? "Certificacion de Identidad" : oXmlIn.Description;
                                    ord.Fecha = dNow;
                                    ord.Iporigen = HttpContext.Current.Request.UserHostAddress;
                                    ord.RdAplicaciones = AdministradorRdAplicaciones.BuscarRdAplicacionesById(oXmlIn.ApplicationId);
                                    ord.Reciboxml = sRDEnvelope; // xmlsigned;
                                    ord.Version = "1.0";
                                    LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Salvando en BD el Recibo Digital - ReciboId = " + reciboID);
                                    ord = AdministradorRdRecibos.Save(ord);
                                    LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Salvado en BD el Recibo Digital - ReciboId = " + reciboID);
                                    //PersistentManager.session().Save(ord);
                                    //PersistentManager.session().Flush();

                                    //Update RdCITx
                                    msg = null;
                                    LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Updating rdCITx en BD TrackId = " + oXmlIn.CIParam.TrackId);
                                    AdministradorRdCITx.UpdateCITxSigner(tx, oXmlIn, ord, TrackIdBP, TrackIDE, PdfCertifyB64, resultverify, score, Properties.Settings.Default.CIBPTreshold,
                                                                    oXmlIn.CIParam.Name, oXmlIn.CIParam.PhaterLastName, oXmlIn.CIParam.MotherLastName, securitycode, out msg);
                                    //AdministradorRdCITx.UpdateCITx(oXmlIn, ord, TrackIdBP, TrackIDE, PdfCertifyB64, resultverify, out msg);

                                    oXmlOut.Receipt = sRDEnvelope; //xmlsigned;
                                    oXmlOut.Trackid = reciboID;
                                    oXmlOut.CIParam.TrackId = rdCITx.TrackId;
                                    oXmlOut.ResultVerify = resultverify;
                                    oXmlOut.CIParam.CertificatePDF = PdfCertifyB64;
                                    oXmlOut.QR = QRverify;
                                    oXmlOut.Timestampend = DateTime.Now;

                                    if (Properties.Settings.Default.CIInformResultByMail == 1 && sendMails)
                                    {
                                        //Added 10-05-2020 - Send mail HTMl con certificados attachados. MAndo PDF + XML
                                        LOG.Debug("Informo por mail " + oXmlOut.CIParam.DestinaryMail + " a destinatario enviadno docs...");
                                        string[] strbody = new string[2];
                                        strbody[0] = "Usted completó la Certificación de identidad para el documento <b>" +
                                                        oXmlOut.CIParam.TypeId + oXmlOut.CIParam.ValueId + "</b>";
                                        strbody[1] = "El codigo unico del tramite para consultas es: <b>" + oXmlOut.CIParam.TrackId + "</b>";
                                        byte[] docpdf = Convert.FromBase64String(oXmlOut.CIParam.CertificatePDF);
                                        byte[] docxml = System.Text.Encoding.UTF8.GetBytes(oXmlOut.Receipt);
                                        //Send PDF

                                        bool bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                                                            "Certificación de Identidad completada con exito [PDF]", strbody, docpdf, "pdf",
                                                            oXmlOut.CIParam.TrackId + ".pdf");
                                        LOG.Debug("Enviado PDF => " + (bret.ToString()));
                                        bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                                                            "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
                                                            oXmlOut.CIParam.TrackId + ".xml");
                                        LOG.Debug("Enviado XML => " + (bret.ToString()));
                                    }

                                    //Added 20-04-2020 - Return CallBack generico si UrlCallback != null en RdAplicaciones
                                    CheckSendCallback(ord.RdAplicaciones, sRDEnvelope);

                                    if (oXmlIn.Actionid == 88) //Pide respuesta liviana, por lo que se elimina todo lo pesado
                                    {
                                        oXmlOut.Receipt = null;
                                        oXmlOut.CIParam.CertificatePDF = null;
                                        oXmlOut.CIParam.IDCardImageFront = null;
                                        oXmlOut.CIParam.IDCardImageBack = null;
                                        oXmlOut.CIParam.IDCardPhotoImage = null;
                                        oXmlOut.CIParam.IDCardSignatureImage = null;
                                        oXmlOut.CIParam.Selfie = null;
                                        oXmlOut.CIParam.Map = null;
                                        oXmlOut.CIParam.ManualSignatureImage = null;
                                        oXmlOut.QR = null;
                                    }

                                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);

                                    ret = Errors.IRET_OK;
                                }
                                catch (Exception exDB)
                                {
                                    LOG.Error("CIServiceManager.GenerateSignerTx_From_BPWeb - Error salvando en BD [" + exDB.Message + "]");
                                    oXmlOut.Message = "Error salvando en BD [" + exDB.Message + "]";
                                    oXmlOut.Receipt = null;
                                    ret = Errors.IRET_ERR_SAVE_DB;
                                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                }
                            }
                        }
                        else
                        {
                            oXmlOut.Message = "Error Firmando [" + msgerr + "]";
                            LOG.Error("CIServiceManager.GenerateSignerTx_From_BPWeb - " + oXmlOut.Message);
                            oXmlOut.Receipt = null;
                            ret = Errors.IRET_ERR_SIGNATURE;
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        }
                    }
                }
                //}
                //}
            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                LOG.Error(oXmlOut.Message);
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }
            LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb OUT!");
            return ret;
        }

        /// <summary>
        /// PAra la certificacion cunado viene hecho el OCR y verifiy desde afuera, se recupera el score
        /// y trackid para completar el certificado.
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="msg"></param>
        /// <param name="resultverify"></param>
        /// <param name="score"></param>
        /// <param name="trackIdBP"></param>
        /// <returns></returns>
        private static int ExtractDataVerificationFromParamIn(XmlParamIn oXmlIn, out string msg, 
                                                              out int resultverify, out double score, out string trackIdBP)
        {
            int ret = -1;
            score = 0;
            trackIdBP = null;
            msg = null;
            resultverify = 2;
            try
            {
                if (string.IsNullOrEmpty(oXmlIn.Additionaldata))
                {
                    msg = "Additional Data nulo. No fue informado resultado de verificacion";
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }

                //                AdditionalData ad = AdditionalData.DeserializeFromXml(oXmlIn.Additionaldata);
                //AdditionalData ad = JsonConvert.DeserializeObject<AdditionalData>(oXmlIn.Additionaldata);
                string[] s1 = oXmlIn.Additionaldata.Split('|');


                if (s1 == null || s1.Length != 2)
                {
                    msg = "Error deserializando AdditionalData!";
                    return Errors.IERR_DESERIALIZE_XMLPARAMOUT;
                }

                //score = (int)ad.GetValue("Score");
                //trackIdBP = (string)ad.GetValue("TrackId");
                score = Convert.ToDouble(s1[0].Split('=')[1]) * 10;
                trackIdBP = s1[1].Split('=')[1];

                resultverify = (score <= Properties.Settings.Default.CIExtTreshold) ? 2 : 1;

                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Recibe el video, imagen, mac, georeferenciacion, etc, para certificar y devuelve recibo
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        internal static int GenerateCI_w_FotoCedulaVsSelfie(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /* 
             *  1.- Chequeo Acceso
             *  2.- Hago verify contra bioportal
             *  3.- Depende resultado de 2
             *      3.1.- Si dio negativo, update en CItx o create con error ResultCode
             *      3.2.- Si dio positivo
             *            3.2.1.- Creo Recibo XML y guardo en BD
             *            3.2.2.- Creo PDF certificado con servicio de DE y guardo en BD
             *  4.- Retorno 
            */
            int ret = Errors.IRET_OK;
            string sXMLTS = null;
            int resTS = 0;
            string sRDEnvelope = null;
            bool isSaveOK = false;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;
           
            try
            {
                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie IN...");

                //string reciboID = AdministradorRdCITx.GetTrackIdUnique();// Guid.NewGuid().ToString("N");
                string reciboID = AdministradorRdRecibos.GetTrackIdUnique();// Guid.NewGuid().ToString("N");
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                // AGREGAR CONTROL DE LOGIN
                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string strapp;
                int qext = 0;
                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie CheckAppAndCompanyEnabled IN...");
                int retaux = CheckAppAndCompanyEnabled(oXmlIn.ApplicationId, out qext, out strapp, out msg);
                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie CheckAppAndCompanyEnabled Out...");
                if (retaux < 0)
                {
                    oXmlOut.Message = msg;
                    LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie Error chequeando ingreso - msg=" + msg);
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return retaux;
                }
                else  //Si esta todo ok, proceso 
                {
                    //Obtengo rdCITx si existe con el TrackId del parametro de entrada, que es el trackid generado 
                    //para la emision de un QR. Si existe, chequeo que no haya pasado mas tiempo que el configurado en 
                    //Config (5 minutos a priori)
                    msg = null;
                    LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie Recuperando rdCITx con TrackId = " + oXmlIn.CIParam.TrackId);
                    //Added 26-01 para cotrol de username que realiza la verificacion. Si viene nulo no importa, sino se graba
                    string username = Utils.GetUserName(oXmlIn.Additionaldata);
                    RdCITx rdCITx = AdministradorRdCITx.GetCITxByTrackId(oXmlIn.Actionid, oXmlIn.CIParam.TrackId, 
                                                                         oXmlIn.Companyid, oXmlIn.CIParam.ValidityType, true, 
                                                                         oXmlIn.CIParam.TypeId, oXmlIn.CIParam.ValueId, null, 
                                                                         oXmlIn.CIParam.DestinaryMail, oXmlIn.CIParam.WorkStationID,
                                                                         oXmlIn.CIParam.GeoRef, username, out msg);
                    
                    if (rdCITx == null)
                    {
                        oXmlOut.Message = msg;
                        LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie Error recuperanto rcCITx - msg=" + msg);
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_CI_TRACKID_NO_IN_DB;
                    } else if (rdCITx.ActionId == 3 || rdCITx.ActionId == 33 || rdCITx.ActionId == 4 || rdCITx.ActionId == 44 ||
                               IsTrackIdVencida(rdCITx))
                    {
                        oXmlOut.Message = "TrackId ya utilizada en un proceso de CI terminado o Vencida " +
                            "[ActionId = " + rdCITx.ActionId.ToString() + " | Fecha Fin = " + rdCITx.LastModify.ToString("dd/MM/yyy HH:mm:ss") + "]";
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        LOG.Error("CIServiceManager.IDCardVerification Error recuperndo rdCITx = " + oXmlOut.Message);
                        return Errors.IRET_ERR_CI_TRACKID_COMPLETED;
                    }

                    //Added para manejar el llamado liviano, que no venga la info de cedula porque ya se hizo => llenamos oXmlIn desde BD
                    // sino, si el estado es que no se hizo IDCardVerification antes, lo hacemos, y llenamos oXmlIn para seguir
                    bool sigue = true;
                    if (rdCITx.ActionId == 2 || rdCITx.ActionId == 22) //Si es 2 o 22 se hizo ya el IDCardVerification => lleno oXmlIn desde BD
                    {
                        sigue = CopyFromBDtoParamin(rdCITx, oXmlIn, out oXmlIn);
                    } else
                    {
                        oXmlIn.CIParam.TrackId = rdCITx.TrackId;
                        sigue = VerififyIdCardAndFillParamin(oXmlIn, out oXmlIn);
                    }

                    if (!sigue)
                    {
                        oXmlOut.Message = msg;
                        LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie Error recuperanto rcCITx - msg=" + msg);
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_CI_NEGATIVE_VERIFICATION;
                    } else
                    {
                        //Actualizo el ValueId si se envió "NA" en la llamada
                        if (rdCITx.ValueId.Equals("NA") && !oXmlIn.CIParam.ValueId.Equals("NA")) {
                            rdCITx.ValueId = oXmlIn.CIParam.ValueId;
                            string msgerr = "";
                            AdministradorRdCITx.UpdateCITx(rdCITx, out msgerr);
                            LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie Actualiza CITx.Id = " + rdCITx.Id +
                                          " con ValueId = " + rdCITx.ValueId + " [MsgRet = " + 
                                          (string.IsNullOrEmpty(msgerr)?"Sin Error": msgerr));
                        }
                    }


                    //*  2.- Hago verify contra bioportal
                    msg = null;
                    int resultverify = 0;
                    double score = 0;
                    string TrackIdBP = null;
                    LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie Verificando contra BP...");
                    int rptaBP = BPHelper.VerifyCIvsCedula(oXmlIn, out msg, out resultverify, out score, out TrackIdBP);

                    if (rptaBP < 0)
                    {
                        oXmlOut.Message = "Error verificando identidad [Err=" + rptaBP + " - MsgErr=" + msg + "]";
                        LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - " + oXmlOut.Message);
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_CI_VERYFING;
                    } else
                    {
                        //Genero Certificado PDF con DE si que verifico positivo, sino error
                        if (resultverify == 2) //Verificacion negativa
                        {
                            oXmlOut.Message = "Verificación Negativa [Score=" + score.ToString() + "]";
                            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - " + oXmlOut.Message);
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                            return Errors.IRET_ERR_CI_NEGATIVE_VERIFICATION;
                        }
                        else
                        {
                            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - Verificación Positiva [Score=" + score.ToString() + "]");
                            //Genero PDF via DE
                            string PdfCertifyB64;
                            string PdfDecJuradaB64;
                            string QRverify = GetQR(Properties.Settings.Default.BASE_CI_CHECK + "?trackid=" + oXmlIn.CIParam.TrackId);
                            string TrackIDE = null;
                            string Map = null;
                            msg = null;
                            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - Generando Certificado PDF...");
                            int rptaDE = DEHelper.GeneraCertifyPDF(oXmlIn, score, QRverify, out msg, 
                                out PdfCertifyB64, out PdfDecJuradaB64, out TrackIDE, out Map);

                            if (rptaDE < 0)
                            {
                                oXmlOut.Message = "Error generando PDF Certify [Err=" + rptaDE + " - MsgErr=" + msg + "]";
                                LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - " + oXmlOut.Message);
                                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                return Errors.IRET_ERR_CI_PDF_GENERATING;
                            }
                            else
                            {
                                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - Certificado PDF Generado = " + PdfCertifyB64);

                                oXmlIn.CIParam.Map = Map;
                                DateTime dNow = DateTime.Now;
                                RdExtensionItem[] ext = new RdExtensionItem[24];
                                RdExtensionItem item;
                              
                                int index = 0;
                                item = new RdExtensionItem();
                                item.key = "TrackIdCI";
                                item.value = oXmlIn.CIParam.TrackId;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "IDCardImage";
                                item.value = oXmlIn.CIParam.IDCardImageFront;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "IDCardPhotoImage";
                                item.value = oXmlIn.CIParam.IDCardPhotoImage;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "IDCardSignatureImage";
                                item.value = oXmlIn.CIParam.IDCardSignatureImage;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Selfie";
                                item.value = oXmlIn.CIParam.Selfie;
                                ext[index++] = item;

                                item = new RdExtensionItem();
                                item.key = "TypeId";
                                item.value = oXmlIn.CIParam.TypeId;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "ValueId";
                                item.value = oXmlIn.CIParam.ValueId;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Name";
                                item.value = (string.IsNullOrEmpty(oXmlIn.CIParam.Name) ? "" : oXmlIn.CIParam.Name) + " " +
                                             (string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName) ? "" : oXmlIn.CIParam.PhaterLastName) + " " +
                                             (string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName) ? "" : oXmlIn.CIParam.MotherLastName); ;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "BirthDate";
                                item.value = oXmlIn.CIParam.BirthDate;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "IssueDate";
                                item.value = oXmlIn.CIParam.IssueDate;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Sex";
                                item.value = (!string.IsNullOrEmpty(oXmlIn.CIParam.Sex) &&
                                              (oXmlIn.CIParam.Sex.Equals("M") || oXmlIn.CIParam.Sex.Equals("F"))) ?
                                              oXmlIn.CIParam.Sex : "Desconocido";
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Nacionality";
                                item.value = oXmlIn.CIParam.Nacionality;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Serial";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.Serial)?"": oXmlIn.CIParam.Serial;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "ExpirationDate";
                                item.value = oXmlIn.CIParam.ExpirationDate;
                                ext[index++] = item;

                                item = new RdExtensionItem();
                                item.key = "GeoRef";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef)?"": oXmlIn.CIParam.GeoRef;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "WorkStationID";
                                item.value = oXmlIn.CIParam.WorkStationID;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "ValidityType";
                                item.value = oXmlIn.CIParam.ValidityType.ToString();
                                ext[index++] = item;
                                //item = new RdExtensionItem();
                                //item.key = "";
                                //item.value = oXmlIn.CIParam;
                                //ext[index++] = item;

                                item = new RdExtensionItem();
                                item.key = "PDFCICertify";
                                item.value = PdfCertifyB64;
                                ext[index++] = item;
                                //item = new RdExtensionItem();
                                //item.key = "PDFCIDeclaracionJurada";
                                //item.value = PdfDecJuradaB64;
                                //ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "QRCIVerify";
                                item.value = QRverify;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Score";
                                item.value = score.ToString("##.##") + "%";
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Pais";
                                item.value = "Chile";
                                ext[index++] = item;

                                item = new RdExtensionItem();
                                item.key = "IDCardImageBack";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? "" : oXmlIn.CIParam.IDCardImageBack;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "ManualSignatureImage";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.ManualSignatureImage) ? "" : oXmlIn.CIParam.ManualSignatureImage;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "URLVideo";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.URLVideo) ? "" : oXmlIn.CIParam.URLVideo;
                                ext[index++] = item;

                                //item = new RdExtensionItem();
                                //item.key = "IDCardPhotoImageDJ";
                                //item.value = oXmlIn.CIParam.IDCardPhotoImage;
                                //ext[index++] = item;
                                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - Generando Recibo Digital - ReciboId = " + reciboID); 
                                Rd objRD = new Rd(ip, strapp, oXmlIn.Origin.ToString(), dNow, oXmlIn.Description, ext, reciboID, 1);

                                //Firmo RD
                                string xmlTosign = Bio.Digital.Receipt.libs.XmlUtils.SerializeObject(objRD);
                                string xmlsigned = null;
                                string msgerr = null;
                                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - Firmando Recibo Digital - ReciboId = " + reciboID);
                                int ierr = XMLSignatureHelper.Firmar(oXmlIn.ApplicationId, reciboID, xmlTosign, out xmlsigned, out msgerr);

                                //Si está habilitado el TSA => Envio
                                if (ierr == 0)
                                {
                                    if (Global.TSA_ENABLED)
                                    {
                                        LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - Agregando TS al Recibo Digital - ReciboId = " + reciboID);
                                        resTS = Global.TSA_MANAGER.GenerateTS(Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(xmlsigned)), out sXMLTS);
                                        if (resTS == 0)
                                        {
                                            sRDEnvelope = "<RDEnvelope>" + xmlsigned + sXMLTS + "</RDEnvelope>";
                                            isSaveOK = true;
                                        }
                                        else
                                        {
                                            isSaveOK = false;
                                            oXmlOut.Message = "Error generando TS";
                                            LOG.Error(oXmlOut.Message);
                                            oXmlOut.Receipt = null;
                                            ret = Errors.IRET_ERR_TSA;
                                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                        }
                                    }
                                    else
                                    {
                                        isSaveOK = true;
                                        sRDEnvelope = "<RDEnvelope>" + xmlsigned + "</RDEnvelope>";
                                    }

                                    if (isSaveOK)
                                    {
                                        try
                                        {
                                            CopyParams(oXmlIn, out oXmlOut);
                                            RdRecibos ord = new RdRecibos();
                                            ord.Reciboid = reciboID;
                                            ord.Descripcion = string.IsNullOrEmpty(oXmlIn.Description)?"Certificacion de Identidad": oXmlIn.Description;
                                            ord.Fecha = dNow;
                                            ord.Iporigen = HttpContext.Current.Request.UserHostAddress;
                                            ord.RdAplicaciones = AdministradorRdAplicaciones.BuscarRdAplicacionesById(oXmlIn.ApplicationId);
                                            ord.Reciboxml = sRDEnvelope; // xmlsigned;
                                            ord.Version = "1.0";
                                            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - Salvando en BD el Recibo Digital - ReciboId = " + reciboID);
                                            ord = AdministradorRdRecibos.Save(ord);
                                            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - Salvado en BD el Recibo Digital - ReciboId = " + reciboID);
                                            //PersistentManager.session().Save(ord);
                                            //PersistentManager.session().Flush();

                                            //Update RdCITx
                                            msg = null;
                                            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - Updating rdCITx en BD TrackId = " + oXmlIn.CIParam.TrackId);
                                            AdministradorRdCITx.UpdateCITx(oXmlIn, ord, TrackIdBP, TrackIDE, PdfCertifyB64, resultverify, score, Properties.Settings.Default.CIBPTreshold, 
                                                                           oXmlIn.CIParam.Name,oXmlIn.CIParam.PhaterLastName, oXmlIn.CIParam.MotherLastName, null, out msg);
                                            //AdministradorRdCITx.UpdateCITx(oXmlIn, ord, TrackIdBP, TrackIDE, PdfCertifyB64, resultverify, out msg);

                                            oXmlOut.Receipt = sRDEnvelope; //xmlsigned;
                                            oXmlOut.Trackid = reciboID;
                                            oXmlOut.CIParam.TrackId = rdCITx.TrackId;
                                            oXmlOut.CIParam.Score = score.ToString();
                                            oXmlOut.ResultVerify = resultverify;
                                            oXmlOut.CIParam.CertificatePDF = PdfCertifyB64;
                                            oXmlOut.QR = QRverify;
                                            oXmlOut.Timestampend = DateTime.Now;

                                            if (Properties.Settings.Default.CIInformResultByMail == 1)
                                            {
                                                //Added 10-05-2020 - Send mail HTMl con certificados attachados. MAndo PDF + XML
                                                LOG.Debug("Informo por mail " + oXmlOut.CIParam.DestinaryMail + " a destinatario enviadno docs...");
                                                string[] strbody = new string[2];
                                                strbody[0] = "Usted completó la Certificación de identidad para el documento <b>" +
                                                                oXmlOut.CIParam.TypeId + oXmlOut.CIParam.ValueId + "</b>";
                                                strbody[1] = "El codigo unico del tramite para consultas es: <b>" + oXmlOut.CIParam.TrackId + "</b>";
                                                byte[] docpdf = Convert.FromBase64String(oXmlOut.CIParam.CertificatePDF);
                                                byte[] docxml = System.Text.Encoding.UTF8.GetBytes(oXmlOut.Receipt);
                                                //Send PDF

                                                bool bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                                                                    "Certificación de Identidad completada con exito [PDF]", strbody, docpdf, "pdf",
                                                                    oXmlOut.CIParam.TrackId + ".pdf");
                                                LOG.Debug("Enviado PDF => " + (bret.ToString()));
                                                //bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                                                //                    "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
                                                //                    oXmlOut.CIParam.TrackId + ".xml");
                                                //LOG.Debug("Enviado XML => " + (bret.ToString()));
                                            }

                                            //Added 20-04-2020 - Return CallBack generico si UrlCallback != null en RdAplicaciones
                                            CheckSendCallback(ord.RdAplicaciones, sRDEnvelope);

                                            if (oXmlIn.Actionid == 33) //Pide respuesta liviana, por lo que se elimina todo lo pesado
                                            {
                                                oXmlOut.Receipt = null;
                                                oXmlOut.CIParam.CertificatePDF = null;
                                                oXmlOut.CIParam.IDCardImageFront = null;
                                                oXmlOut.CIParam.IDCardImageBack = null;
                                                oXmlOut.CIParam.IDCardPhotoImage = null;
                                                oXmlOut.CIParam.IDCardSignatureImage = null;
                                                oXmlOut.CIParam.Selfie = null;
                                                oXmlOut.CIParam.Map = null;
                                                oXmlOut.QR = null;
                                            }

                                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                                
                                            ret = Errors.IRET_OK;
                                        }
                                        catch (Exception exDB)
                                        {
                                            LOG.Error("Error salvando en BD [" + exDB.Message + "]");
                                            oXmlOut.Message = "Error salvando en BD [" + exDB.Message + "]";
                                            oXmlOut.Receipt = null;
                                            ret = Errors.IRET_ERR_SAVE_DB;
                                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                        }
                                    }
                                }
                                else
                                {
                                    oXmlOut.Message = "Error Firmando [" + msgerr + "]";
                                    LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - " + oXmlOut.Message);
                                    oXmlOut.Receipt = null;
                                    ret = Errors.IRET_ERR_SIGNATURE;
                                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                }
                            }
                        }
                    }
                }

            }    
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                LOG.Error(oXmlOut.Message);
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }
            LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie OUT!");
            return ret;
        }

        /// <summary>
        /// Idem de GenerateCI_w_FotoCedulaVsSelfie pero sin hacer OCR ni Verificacion Facial porque está hecho desde fuera
        /// en nuestro Web CI (Con Facetec o Jumio)
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        internal static int GenerateCI_From_BPWeb(RdCITx tx, XmlParamIn oXmlIn, int _resultVerify, double _score, bool sendMails,
                                                  out string xmlparamout)
        {
            /* 
             *  1.- Verifico que la info venga ok
             *  2.- Depende resultado de verificacion
             *      2.1.- Si dio negativo, veo de grabar tx con resultado para log
             *      2.2.- Si dio positivo
             *            2.2.1.- Creo Recibo XML y guardo en BD
             *            2.2.2.- Creo PDF certificado con servicio de DE y guardo en BD
             *  3.- Retorno 
            */
            int ret = Errors.IRET_OK;
            string sXMLTS = null;
            int resTS = 0;
            string sRDEnvelope = null;
            bool isSaveOK = false;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;

            try
            {
                LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb IN...");

                string reciboID = AdministradorRdRecibos.GetTrackIdUnique();// Guid.NewGuid().ToString("N");
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb CheckAppAndCompanyEnabled IN...");
                string msg;
                string strapp;
                int qext = 0;
                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess CheckAppAndCompanyEnabled IN...");
                RdAplicaciones app = AdministradorRdAplicaciones.BuscarRdAplicacionesById(oXmlIn.ApplicationId);
                LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess CheckAppAndCompanyEnabled Out...");
                if (app == null)
                {
                    oXmlOut.Message = "Recuperacion ApplicationId = " + oXmlIn.ApplicationId + " => NUll";
                    LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie_WithoutProcess Recuperacion ApplicationId = " + oXmlIn.ApplicationId + " => NUll");
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_API_ERR_NOT_FOUND * -1;
                }
                msg = null;
                LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb Generando CI con TrackId = " + oXmlIn.CIParam.TrackId);
                RdCITx rdCITx = tx; //Viene por parametro
                    //AdministradorRdCITx.GetCITxByTrackId(oXmlIn.Actionid, oXmlIn.CIParam.TrackId,
                    //                                                    oXmlIn.Companyid, oXmlIn.CIParam.ValidityType, true,
                    //                                                    oXmlIn.CIParam.TypeId, oXmlIn.CIParam.ValueId, null,
                    //                                                    oXmlIn.CIParam.DestinaryMail, out msg);

                if (rdCITx == null)
                {
                    oXmlOut.Message = msg;
                    LOG.Error("CIServiceManager.GenerateCI_From_BPWeb Error recuperanto rcCITx - msg=" + msg);
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_CI_TRACKID_NO_IN_DB;
                }
                else
                {
                    //Copio trackid para actualizar luego en la BD. Dado que recien se creo la Tx debo tomar aca el TrackId
                    oXmlIn.CIParam.TrackId = rdCITx.TrackId;
                }
                   

                //Added para manejar el llamado liviano, que no venga la info de cedula porque ya se hizo => llenamos oXmlIn desde BD
                // sino, si el estado es que no se hizo IDCardVerification antes, lo hacemos, y llenamos oXmlIn para seguir
                bool sigue = true;
                   
                //Actualizo el ValueId si se envió "NA" en la llamada
                if (rdCITx.ValueId.Equals("NA") && !oXmlIn.CIParam.ValueId.Equals("NA"))
                {
                    rdCITx.ValueId = oXmlIn.CIParam.ValueId;
                    string msgerr = "";
                    AdministradorRdCITx.UpdateCITx(rdCITx, out msgerr);
                    LOG.Error("CIServiceManager.GenerateCI_From_BPWeb Actualiza CITx.Id = " + rdCITx.Id +
                                    " con ValueId = " + rdCITx.ValueId + " [MsgRet = " +
                                    (string.IsNullOrEmpty(msgerr) ? "Sin Error" : msgerr));
                }
                //}


                //*  2.- Hago verify contra bioportal
                msg = null;
                int resultverify = 0;
                double score = 0;
                string TrackIdBP = tx.TrackIdBP;
                LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb Verificando contra BP...");
                resultverify = _resultVerify;
                score = _score;
                //int rptaBP = ExtractDataVerificationFromParamIn(oXmlIn, out msg, out resultverify, out score, out TrackIdBP); //BPHelper.VerifyCIvsCedula(oXmlIn, out msg, out resultverify, out score, out TrackIdBP);

                //if (rptaBP < 0)
                //{
                //    oXmlOut.Message = "Error verificando identidad [Err=" + rptaBP + " - MsgErr=" + msg + "]";
                //    LOG.Error("CIServiceManager.GenerateCI_From_BPWeb - " + oXmlOut.Message);
                //    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IRET_ERR_CI_VERYFING;
                //}
                //else
                //{
                //Genero Certificado PDF con DE si que verifico positivo, sino error
                if (resultverify == 2) //Verificacion negativa
                {
                    //TODO - Grabar en BD que el resultado fue negativo
                    oXmlOut.Message = "Verificación Negativa [Score=" + score.ToString() + "]";
                    LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb - " + oXmlOut.Message);
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_CI_NEGATIVE_VERIFICATION;
                }
                else if (resultverify == 1) //Es positivo
                {
                    LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb - Verificación Positiva [Score=" + score.ToString() + "]");
                    //Genero PDF via DE
                    string PdfCertifyB64;
                    string PdfDecJuradaB64;
                    string QRverify = GetQR(Properties.Settings.Default.BASE_CI_CHECK + "?trackid=" + oXmlIn.CIParam.TrackId);
                    string TrackIDE = null;
                    string Map = null;
                    msg = null;
                    LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb - Generando Certificado PDF...");
                    int rptaDE = DEHelper.GeneraCertifyPDF(oXmlIn, score, QRverify, out msg,
                        out PdfCertifyB64, out PdfDecJuradaB64, out TrackIDE, out Map);

                    if (rptaDE < 0)
                    {
                        oXmlOut.Message = "Error generando PDF Certify [Err=" + rptaDE + " - MsgErr=" + msg + "]";
                        LOG.Error("CIServiceManager.GenerateCI_From_BPWeb - " + oXmlOut.Message);
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_CI_PDF_GENERATING;
                    }
                    else
                    {
                        LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb - Certificado PDF Generado = " + PdfCertifyB64);

                        oXmlIn.CIParam.Map = Map;
                        DateTime dNow = DateTime.Now;
                        RdExtensionItem[] ext = new RdExtensionItem[24];
                        RdExtensionItem item;

                        int index = 0;
                        item = new RdExtensionItem();
                        item.key = "TrackIdCI";
                        item.value = oXmlIn.CIParam.TrackId;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "IDCardImage";
                        item.value = oXmlIn.CIParam.IDCardImageFront;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "IDCardPhotoImage";
                        item.value = oXmlIn.CIParam.IDCardPhotoImage;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "IDCardSignatureImage";
                        item.value = oXmlIn.CIParam.IDCardSignatureImage;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Selfie";
                        item.value = oXmlIn.CIParam.Selfie;
                        ext[index++] = item;

                        item = new RdExtensionItem();
                        item.key = "TypeId";
                        item.value = oXmlIn.CIParam.TypeId;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "ValueId";
                        item.value = oXmlIn.CIParam.ValueId;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Name";
                        item.value = (string.IsNullOrEmpty(oXmlIn.CIParam.Name) ? "" : oXmlIn.CIParam.Name) + " " +
                                        (string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName) ? "" : oXmlIn.CIParam.PhaterLastName) + " " +
                                        (string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName) ? "" : oXmlIn.CIParam.MotherLastName); ;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "BirthDate";
                        item.value = oXmlIn.CIParam.BirthDate;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "IssueDate";
                        item.value = oXmlIn.CIParam.IssueDate;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Sex";
                        item.value = (!string.IsNullOrEmpty(oXmlIn.CIParam.Sex) &&
                                        (oXmlIn.CIParam.Sex.Equals("M") || oXmlIn.CIParam.Sex.Equals("F"))) ?
                                        oXmlIn.CIParam.Sex : "Desconocido";
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Nacionality";
                        item.value = oXmlIn.CIParam.Nacionality;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Serial";
                        item.value = string.IsNullOrEmpty(oXmlIn.CIParam.Serial) ? "" : oXmlIn.CIParam.Serial;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "ExpirationDate";
                        item.value = oXmlIn.CIParam.ExpirationDate;
                        ext[index++] = item;

                        item = new RdExtensionItem();
                        item.key = "GeoRef";
                        item.value = string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef) ? "" : oXmlIn.CIParam.GeoRef;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "WorkStationID";
                        item.value = oXmlIn.CIParam.WorkStationID;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "ValidityType";
                        item.value = oXmlIn.CIParam.ValidityType.ToString();
                        ext[index++] = item;
                        //item = new RdExtensionItem();
                        //item.key = "";
                        //item.value = oXmlIn.CIParam;
                        //ext[index++] = item;

                        item = new RdExtensionItem();
                        item.key = "PDFCICertify";
                        item.value = PdfCertifyB64;
                        ext[index++] = item;
                        //item = new RdExtensionItem();
                        //item.key = "PDFCIDeclaracionJurada";
                        //item.value = PdfDecJuradaB64;
                        //ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "QRCIVerify";
                        item.value = QRverify;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Score";
                        item.value = score.ToString("##.##") + "%";
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "Pais";
                        item.value = "Chile";
                        ext[index++] = item;

                        item = new RdExtensionItem();
                        item.key = "IDCardImageBack";
                        item.value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? "" : oXmlIn.CIParam.IDCardImageBack;
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "ManualSignatureImage";
                        item.value = string.IsNullOrEmpty(oXmlIn.CIParam.ManualSignatureImage) ? "" : oXmlIn.CIParam.ManualSignatureImage;

                        //TODO agregar URL Video
                        ext[index++] = item;
                        item = new RdExtensionItem();
                        item.key = "URLVideo";
                        item.value = string.IsNullOrEmpty(oXmlIn.CIParam.URLVideo) ? "" : oXmlIn.CIParam.URLVideo;
                        ext[index++] = item;

                        //item = new RdExtensionItem();
                        //item.key = "IDCardPhotoImageDJ";
                        //item.value = oXmlIn.CIParam.IDCardPhotoImage;
                        //ext[index++] = item;
                        LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb - Generando Recibo Digital - ReciboId = " + reciboID);
                        Rd objRD = new Rd(ip, app.Nombre, oXmlIn.Origin.ToString(), dNow, oXmlIn.Description, ext, reciboID, 1);

                        //Firmo RD
                        string xmlTosign = Bio.Digital.Receipt.libs.XmlUtils.SerializeObject(objRD);
                        string xmlsigned = null;
                        string msgerr = null;
                        LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb - Firmando Recibo Digital - ReciboId = " + reciboID);
                        int ierr = XMLSignatureHelper.Firmar(oXmlIn.ApplicationId, reciboID, xmlTosign, out xmlsigned, out msgerr);

                        //Si está habilitado el TSA => Envio
                        if (ierr == 0)
                        {
                            if (Global.TSA_ENABLED)
                            {
                                LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb - Agregando TS al Recibo Digital - ReciboId = " + reciboID);
                                resTS = Global.TSA_MANAGER.GenerateTS(Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(xmlsigned)), out sXMLTS);
                                if (resTS == 0)
                                {
                                    sRDEnvelope = "<RDEnvelope>" + xmlsigned + sXMLTS + "</RDEnvelope>";
                                    isSaveOK = true;
                                }
                                else
                                {
                                    isSaveOK = false;
                                    oXmlOut.Message = "Error generando TS";
                                    LOG.Error(oXmlOut.Message);
                                    oXmlOut.Receipt = null;
                                    ret = Errors.IRET_ERR_TSA;
                                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                }
                            }
                            else
                            {
                                isSaveOK = true;
                                sRDEnvelope = "<RDEnvelope>" + xmlsigned + "</RDEnvelope>";
                            }

                            if (isSaveOK)
                            {
                                try
                                {
                                    CopyParams(oXmlIn, out oXmlOut);
                                    RdRecibos ord = new RdRecibos();
                                    ord.Reciboid = reciboID;
                                    ord.Descripcion = string.IsNullOrEmpty(oXmlIn.Description) ? "Certificacion de Identidad" : oXmlIn.Description;
                                    ord.Fecha = dNow;
                                    ord.Iporigen = HttpContext.Current.Request.UserHostAddress;
                                    ord.RdAplicaciones = AdministradorRdAplicaciones.BuscarRdAplicacionesById(oXmlIn.ApplicationId);
                                    ord.Reciboxml = sRDEnvelope; // xmlsigned;
                                    ord.Version = "1.0";
                                    LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb - Salvando en BD el Recibo Digital - ReciboId = " + reciboID);
                                    ord = AdministradorRdRecibos.Save(ord);
                                    LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb - Salvado en BD el Recibo Digital - ReciboId = " + reciboID);
                                    //PersistentManager.session().Save(ord);
                                    //PersistentManager.session().Flush();

                                    //Update RdCITx
                                    msg = null;
                                    LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb - Updating rdCITx en BD TrackId = " + oXmlIn.CIParam.TrackId);
                                    AdministradorRdCITx.UpdateCITx(oXmlIn, ord, TrackIdBP, TrackIDE, PdfCertifyB64, resultverify, score, Properties.Settings.Default.CIBPTreshold,
                                                                    oXmlIn.CIParam.Name, oXmlIn.CIParam.PhaterLastName, oXmlIn.CIParam.MotherLastName,
                                                                    oXmlIn.CIParam.URLVideo, out msg);
                                    //AdministradorRdCITx.UpdateCITx(oXmlIn, ord, TrackIdBP, TrackIDE, PdfCertifyB64, resultverify, out msg);

                                    oXmlOut.Receipt = sRDEnvelope; //xmlsigned;
                                    oXmlOut.Trackid = reciboID;
                                    oXmlOut.CIParam.TrackId = rdCITx.TrackId;
                                    oXmlOut.ResultVerify = resultverify;
                                    oXmlOut.CIParam.CertificatePDF = PdfCertifyB64;
                                    oXmlOut.QR = QRverify;
                                    oXmlOut.Timestampend = DateTime.Now;

                                    if (Properties.Settings.Default.CIInformResultByMail == 1 && sendMails)
                                    {
                                        //Added 10-05-2020 - Send mail HTMl con certificados attachados. MAndo PDF + XML
                                        LOG.Debug("Informo por mail " + oXmlOut.CIParam.DestinaryMail + " a destinatario enviadno docs...");
                                        string[] strbody = new string[2];
                                        strbody[0] = "Usted completó la Certificación de identidad para el documento <b>" +
                                                        oXmlOut.CIParam.TypeId + oXmlOut.CIParam.ValueId + "</b>";
                                        strbody[1] = "El codigo unico del tramite para consultas es: <b>" + oXmlOut.CIParam.TrackId + "</b>";
                                        byte[] docpdf = Convert.FromBase64String(oXmlOut.CIParam.CertificatePDF);
                                        byte[] docxml = System.Text.Encoding.UTF8.GetBytes(oXmlOut.Receipt);
                                        //Send PDF

                                        bool bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                                                            "Certificación de Identidad completada con exito [PDF]", strbody, docpdf, "pdf",
                                                            oXmlOut.CIParam.TrackId + ".pdf");
                                        LOG.Debug("Enviado PDF => " + (bret.ToString()));
                                        bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                                                            "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
                                                            oXmlOut.CIParam.TrackId + ".xml");
                                        LOG.Debug("Enviado XML => " + (bret.ToString()));
                                    }

                                    //Added 20-04-2020 - Return CallBack generico si UrlCallback != null en RdAplicaciones
                                    CheckSendCallback(ord.RdAplicaciones, sRDEnvelope);

                                    if (oXmlIn.Actionid == 88) //Pide respuesta liviana, por lo que se elimina todo lo pesado
                                    {
                                        oXmlOut.Receipt = null;
                                        oXmlOut.CIParam.CertificatePDF = null;
                                        oXmlOut.CIParam.IDCardImageFront = null;
                                        oXmlOut.CIParam.IDCardImageBack = null;
                                        oXmlOut.CIParam.IDCardPhotoImage = null;
                                        oXmlOut.CIParam.IDCardSignatureImage = null;
                                        oXmlOut.CIParam.Selfie = null;
                                        oXmlOut.CIParam.Map = null;
                                        oXmlOut.CIParam.ManualSignatureImage = null;
                                        oXmlOut.QR = null;
                                    }

                                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);

                                    ret = Errors.IRET_OK;
                                }
                                catch (Exception exDB)
                                {
                                    LOG.Error("Error salvando en BD [" + exDB.Message + "]");
                                    oXmlOut.Message = "Error salvando en BD [" + exDB.Message + "]";
                                    oXmlOut.Receipt = null;
                                    ret = Errors.IRET_ERR_SAVE_DB;
                                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                }
                            }
                        }
                        else
                        {
                            oXmlOut.Message = "Error Firmando [" + msgerr + "]";
                            LOG.Error("CIServiceManager.GenerateCI_From_BPWeb - " + oXmlOut.Message);
                            oXmlOut.Receipt = null;
                            ret = Errors.IRET_ERR_SIGNATURE;
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        }
                    }
                }
                //}
                //}
            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                LOG.Error(oXmlOut.Message);
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }
            LOG.Debug("CIServiceManager.GenerateCI_From_BPWeb OUT!");
            return ret;
        }

        private static bool VerififyIdCardAndFillParamin(XmlParamIn oXmlIn, out XmlParamIn oXmlInOut)
        {
            bool ret = true;
            oXmlInOut = (XmlParamIn)oXmlIn.Clone();
            try
            {
                string msg;
                CIParam MRZIDCard = null;
                double score = 0;
                string TrackIdBP = null;
                LOG.Debug("CIServiceManager.VerififyIdCardAndFillParamin Ingresando verificación Cedula contra BP...");
                int rptaBP = BPHelper.VerifyAndOCRIDCard(oXmlIn, out msg, out MRZIDCard, out TrackIdBP);

                if (rptaBP < 0)
                {
                    msg = "Error verificando cedula [Err=" + rptaBP + " - MsgErr=" + msg + "]";
                    LOG.Error("CIServiceManager.VerififyIdCardAndFillParamin Error verificando cedula contra BP [" + msg + "]");
                    ret = false;
                }
                else
                {
                    if (!oXmlIn.CIParam.ValueId.Equals(MRZIDCard.ValueId) && !oXmlIn.CIParam.ValueId.Equals("NA"))
                    {
                        string Message = "ValueId Configurado diferente al ValueId Reconocido. Reintente!" +
                                          " [ValueId Configurado = " + oXmlIn.CIParam.ValueId +
                                          " - ValueId Reconocido = " + (MRZIDCard != null && !string.IsNullOrEmpty(MRZIDCard.ValueId) ? MRZIDCard.ValueId : "NULO") + "]";
                        LOG.Error("CIServiceManager.VerififyIdCardAndFillParamin Exception => " + Message);
                        return false;
                    }
                    oXmlInOut.CIParam = MRZIDCard;
                    oXmlInOut.CIParam.Name = string.IsNullOrEmpty(oXmlInOut.CIParam.Name) ? 
                                                    oXmlIn.CIParam.Name : oXmlInOut.CIParam.Name;
                    oXmlInOut.CIParam.PhaterLastName = string.IsNullOrEmpty(oXmlInOut.CIParam.PhaterLastName) ? 
                                                            oXmlIn.CIParam.PhaterLastName : oXmlInOut.CIParam.PhaterLastName;
                    oXmlInOut.CIParam.MotherLastName = string.IsNullOrEmpty(oXmlInOut.CIParam.MotherLastName) ? 
                                                        oXmlIn.CIParam.MotherLastName : oXmlInOut.CIParam.MotherLastName;
                    oXmlInOut.CIParam.Selfie = oXmlIn.CIParam.Selfie;
                    oXmlInOut.CIParam.WorkStationID = oXmlIn.CIParam.WorkStationID;
                    oXmlInOut.CIParam.GeoRef = oXmlIn.CIParam.GeoRef;
                    oXmlInOut.CIParam.TrackId = oXmlIn.CIParam.TrackId;
                    oXmlInOut.CIParam.Serial = string.IsNullOrEmpty(oXmlInOut.CIParam.Serial) ? oXmlIn.CIParam.Serial : oXmlInOut.CIParam.Serial;
                    oXmlInOut.CIParam.DestinaryMail = string.IsNullOrEmpty(oXmlInOut.CIParam.DestinaryMail) ?
                                                        oXmlIn.CIParam.DestinaryMail: oXmlInOut.CIParam.DestinaryMail;
                    

                }  //VER DONDE SE PIERDE EL MAIL => PROBAR
            }
            catch (Exception ex)
            {
                LOG.Error("CIServiceManager.CopyFromBDtoParamin Error = " + ex.Message);
                ret = false;
            }
            return ret;
        }

        private static bool CopyFromBDtoParamin(RdCITx rdCITx, XmlParamIn oXmlIn, out XmlParamIn oXmlInOut)
        {
            bool ret = true;
            oXmlInOut = (XmlParamIn)oXmlIn.Clone();
            try
            {
                //Added 24-11-2019 - Si viene valores en XmlParamIn es porque se puede haber arreglado algo manualemnte
                //mejorando reconocimiento automatico. Se supone que Valueid y Serial no se modificaron y son la clave
                oXmlInOut.CIParam.Name = string.IsNullOrEmpty(oXmlIn.CIParam.Name)?rdCITx.Name: oXmlIn.CIParam.Name;
                oXmlInOut.CIParam.PhaterLastName = string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName)?
                                                        rdCITx.PhaterLastName:oXmlIn.CIParam.PhaterLastName;
                oXmlInOut.CIParam.MotherLastName = string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName)?
                                                        rdCITx.MotherLastName:oXmlIn.CIParam.MotherLastName;
                //rdCITx.TrackIdBP = trackidbp;
                oXmlInOut.CIParam.TypeId = string.IsNullOrEmpty(oXmlIn.CIParam.TypeId)?rdCITx.TypeId:oXmlIn.CIParam.TypeId;
                oXmlInOut.CIParam.ValueId = (string.IsNullOrEmpty(oXmlIn.CIParam.ValueId) && !oXmlIn.CIParam.ValueId.Equals("NA")) ?
                                                   oXmlIn.CIParam.ValueId: rdCITx.ValueId;
                oXmlInOut.CIParam.IDCardImageFront = rdCITx.IdCardImageFront;
                oXmlInOut.CIParam.IDCardImageBack = rdCITx.IdCardImageBack;
                oXmlInOut.CIParam.IDCardPhotoImage = rdCITx.IdCardPhotoImage;
                oXmlInOut.CIParam.IDCardSignatureImage = rdCITx.IdCardSignatureImage;
                oXmlInOut.CIParam.IssueDate = string.IsNullOrEmpty(oXmlIn.CIParam.IssueDate)?rdCITx.IssueDate:oXmlIn.CIParam.IssueDate;
                oXmlInOut.CIParam.ExpirationDate = string.IsNullOrEmpty(oXmlIn.CIParam.ExpirationDate)?
                                                            rdCITx.ExprationDate:oXmlIn.CIParam.ExpirationDate;
                oXmlInOut.CIParam.BirthDate = string.IsNullOrEmpty(oXmlIn.CIParam.BirthDate)?rdCITx.BirthDate: oXmlIn.CIParam.BirthDate;
                oXmlInOut.CIParam.Serial = string.IsNullOrEmpty(oXmlIn.CIParam.Serial)?rdCITx.Serial:oXmlIn.CIParam.Serial;
                oXmlInOut.CIParam.Sex = string.IsNullOrEmpty(oXmlIn.CIParam.Sex)?rdCITx.Sex:oXmlIn.CIParam.Sex;
                oXmlInOut.CIParam.Nacionality = string.IsNullOrEmpty(oXmlIn.CIParam.Nacionality)?
                                                    rdCITx.Nacionality:oXmlIn.CIParam.Nacionality;
                oXmlInOut.CIParam.TrackId = rdCITx.TrackId;
                oXmlInOut.CIParam.DestinaryMail = !string.IsNullOrEmpty(rdCITx.DestinataryMail)? rdCITx.DestinataryMail: oXmlInOut.CIParam.DestinaryMail;

            }
            catch (Exception ex)
            {
                LOG.Error("CIServiceManager.CopyFromBDtoParamin Error = " + ex.Message);
                ret = false;
            }
            return ret;
        }



        /// <summary>
        /// Recibe el video, imagen, mac, georeferenciacion, etc, para certificar y devuelve recibo
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        internal static int GenerateCI_w_SelfieVsBD(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /* 
             *  1.- Chequeo Acceso
             *  2.- Hago verify contra bioportal
             *  3.- Depende resultado de 2
             *      3.1.- Si dio negativo, update en CItx o create con error ResultCode
             *      3.2.- Si dio positivo
             *            3.2.1.- Creo Recibo XML y guardo en BD
             *            3.2.2.- Creo PDF certificado con servicio de DE y guardo en BD
             *  4.- Retorno 
            */
            int ret = Errors.IRET_OK;
            string sXMLTS = null;
            int resTS = 0;
            string sRDEnvelope = null;
            bool isSaveOK = false;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;

            try
            {
                LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD IN...");

                string reciboID = AdministradorRdCITx.GetTrackIdUnique();// Guid.NewGuid().ToString("N");
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                // AGREGAR CONTROL DE LOGIN
                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string strapp;
                int qext = 0;
                LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD CheckAppAndCompanyEnabled IN...");
                int retaux = CheckAppAndCompanyEnabled(oXmlIn.ApplicationId, out qext, out strapp, out msg);
                LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD CheckAppAndCompanyEnabled Out...");
                if (retaux < 0)
                {
                    oXmlOut.Message = msg;
                    LOG.Error("CIServiceManager.GenerateCI_w_SelfieVsBD Error chequeando ingreso - msg=" + msg);
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return retaux;
                }
                else  //Si esta todo ok, proceso 
                {
                    //Obtengo rdCITx si existe con el TrackId del parametro de entrada, que es el trackid generado 
                    //para la emision de un QR. Si existe, chequeo que no haya pasado mas tiempo que el configurado en 
                    //Config (5 minutos a priori)
                    msg = null;
                    LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD Recuperando rdCITx con TrackId = " + oXmlIn.CIParam.TrackId);
                    //Added 26-01 para cotrol de username que realiza la verificacion. Si viene nulo no importa, sino se graba
                    string username = Utils.GetUserName(oXmlIn.Additionaldata);
                    RdCITx rdCITx = AdministradorRdCITx.GetCITxByTrackId(oXmlIn.Actionid, oXmlIn.CIParam.TrackId,
                                                                         oXmlIn.Companyid, oXmlIn.CIParam.ValidityType, true,
                                                                         oXmlIn.CIParam.TypeId, oXmlIn.CIParam.ValueId, null,
                                                                         oXmlIn.CIParam.DestinaryMail, oXmlIn.CIParam.WorkStationID,
                                                                         oXmlIn.CIParam.GeoRef, username, out msg);

                    if (rdCITx == null)
                    {
                        oXmlOut.Message = msg;
                        LOG.Error("CIServiceManager.GenerateCI_w_SelfieVsBD Error recuperanto rcCITx - msg=" + msg);
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_CI_TRACKID_NO_IN_DB;
                    }
                    else if (rdCITx.ActionId == 3 || rdCITx.ActionId == 33 || rdCITx.ActionId == 4 || rdCITx.ActionId == 44 ||
                             IsTrackIdVencida(rdCITx))
                    {
                        oXmlOut.Message = "TrackId ya utilizada en un proceso de CI terminado " +
                            "[ActionId = " + rdCITx.ActionId.ToString() + " | Fecha Fin = " + rdCITx.LastModify.ToString("dd/MM/yyy HH:mm:ss") + "]";
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        LOG.Error("CIServiceManager.GenerateCI_w_SelfieVsBD Error recuperndo rdCITx = " + oXmlOut.Message);
                        return Errors.IRET_ERR_CI_TRACKID_COMPLETED;
                    }

                    //Added para manejar el llamado liviano, que no venga la info de cedula porque ya se hizo => llenamos oXmlIn desde BD
                    // sino, si el estado es que no se hizo IDCardVerification antes, lo hacemos, y llenamos oXmlIn para seguir
                    bool sigue = true;
                    //if (rdCITx.ActionId == 2 || rdCITx.ActionId == 22) //Si es 2 o 22 se hizo ya el IDCardVerification => lleno oXmlIn desde BD
                    //{
                        sigue = CopyFromBDtoParamin(rdCITx, oXmlIn, out oXmlIn);
                    //}
                    //else
                    //{
                    //    sigue = VerififyIdCardAndFillParamin(oXmlIn, out oXmlIn);
                    //}

                    if (!sigue)
                    {
                        oXmlOut.Message = msg;
                        LOG.Error("CIServiceManager.GenerateCI_w_SelfieVsBD Error recuperanto rcCITx - msg=" + msg);
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_CI_NEGATIVE_VERIFICATION;
                    } else
                    {
                        //Actualizo el ValueId si se envió "NA" en la llamada
                        if (rdCITx.ValueId.Equals("NA") && !oXmlIn.CIParam.ValueId.Equals("NA"))
                        {
                            rdCITx.ValueId = oXmlIn.CIParam.ValueId;
                            string msgerr = "";
                            AdministradorRdCITx.UpdateCITx(rdCITx, out msgerr);
                            LOG.Error("CIServiceManager.GenerateCI_w_SelfieVsBD Actualiza CITx.Id = " + rdCITx.Id +
                                          " con ValueId = " + rdCITx.ValueId + " [MsgRet = " +
                                          (string.IsNullOrEmpty(msgerr) ? "Sin Error" : msgerr));
                        }
                    }


                    //*  2.- Hago verify and get contra bioportal
                    //      2.1.- No existe o matching negativos => Aborta
                    //      2.2.- Matching positivo => Genero Certificado (Debo llenar lo necesario para completar)  
                    msg = null;
                    int resultverify = 0;
                    double score = 0;
                    string TrackIdBP = null;
                    LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD Verificando contra BP...");
                    CIParam _CIParamOut;
                    int rptaBP = BPHelper.VerifyCIvsBD(oXmlIn, out msg, out resultverify, out score, 
                                                       out TrackIdBP, out _CIParamOut);

                    if (rptaBP < 0)
                    {
                        oXmlOut.Message = "Error verificando identidad [Err=" + rptaBP + " - MsgErr=" + msg + "]";
                        LOG.Error("CIServiceManager.GenerateCI_w_SelfieVsBD - " + oXmlOut.Message);
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_CI_VERYFING;
                    }
                    else
                    {
                        //Genero Certificado PDF con DE si que verifico positivo, sino error
                        if (resultverify == 2) //Verificacion negativa
                        {
                            oXmlOut.Message = "Verificación Negativa [Score=" + score.ToString() + "]";
                            LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD - " + oXmlOut.Message);
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                            return Errors.IRET_ERR_CI_NEGATIVE_VERIFICATION;
                        }
                        else
                        {
                            LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD - Verificación Positiva [Score=" + score.ToString() + "]");
                            oXmlIn.CIParam = _CIParamOut;
                            if (string.IsNullOrEmpty(oXmlIn.CIParam.TrackId))
                            {
                                oXmlIn.CIParam.TrackId = AdministradorRdCITx.GetTrackIdUnique();
                            }
                            //Genero PDF via DE
                            string PdfCertifyB64;
                            string PdfDecJuradaB64;

                            string QRverify = GetQR(Properties.Settings.Default.BASE_CI_CHECK + "?trackid=" + oXmlIn.CIParam.TrackId);
                            string TrackIDE = null;
                            string Map;
                            msg = null;
                            LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD - Generando Certificado PDF...");
                            int rptaDE = DEHelper.GeneraCertifyPDF(oXmlIn, score, QRverify, out msg, 
                                out PdfCertifyB64, out PdfDecJuradaB64, out TrackIDE, out Map);

                            if (rptaDE < 0)
                            {
                                oXmlOut.Message = "Error generando PDF Certify [Err=" + rptaDE + " - MsgErr=" + msg + "]";
                                LOG.Error("CIServiceManager.GenerateCI_w_SelfieVsBD - " + oXmlOut.Message);
                                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                return Errors.IRET_ERR_CI_PDF_GENERATING;
                            }
                            else
                            {
                                LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD - Certificado PDF Generado = " + PdfCertifyB64);
                                oXmlIn.CIParam.Map = Map;

                                DateTime dNow = DateTime.Now;
                                RdExtensionItem[] ext = new RdExtensionItem[24];
                                RdExtensionItem item;
                                
                                int index = 0;
                                item = new RdExtensionItem();
                                item.key = "TrackIdCI";
                                item.value = oXmlIn.CIParam.TrackId;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "IDCardImage";
                                item.value = oXmlIn.CIParam.IDCardImageFront;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "IDCardPhotoImage";
                                item.value = oXmlIn.CIParam.IDCardPhotoImage;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "IDCardSignatureImage";
                                item.value = oXmlIn.CIParam.IDCardSignatureImage;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Selfie";
                                item.value = oXmlIn.CIParam.Selfie;
                                ext[index++] = item;

                                item = new RdExtensionItem();
                                item.key = "TypeId";
                                item.value = oXmlIn.CIParam.TypeId;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "ValueId";
                                item.value = oXmlIn.CIParam.ValueId;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Name";
                                item.value = (string.IsNullOrEmpty(oXmlIn.CIParam.Name) ? "" : oXmlIn.CIParam.Name) + " " +
                                             (string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName) ? "" : oXmlIn.CIParam.PhaterLastName) + " " +
                                             (string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName) ? "" : oXmlIn.CIParam.MotherLastName); ;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "BirthDate";
                                item.value = oXmlIn.CIParam.BirthDate;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "IssueDate";
                                item.value = oXmlIn.CIParam.IssueDate;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Sex";
                                item.value = (!string.IsNullOrEmpty(oXmlIn.CIParam.Sex) &&
                                              (oXmlIn.CIParam.Sex.Equals("M") || oXmlIn.CIParam.Sex.Equals("F"))) ?
                                              oXmlIn.CIParam.Sex : "Desconocido";
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Nacionality";
                                item.value = oXmlIn.CIParam.Nacionality;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Serial";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.Serial) ? "" : oXmlIn.CIParam.Serial;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "ExpirationDate";
                                item.value = oXmlIn.CIParam.ExpirationDate;
                                ext[index++] = item;

                                item = new RdExtensionItem();
                                item.key = "GeoRef";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef) ? "" : oXmlIn.CIParam.GeoRef;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "WorkStationID";
                                item.value = oXmlIn.CIParam.WorkStationID;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "ValidityType";
                                item.value = oXmlIn.CIParam.ValidityType.ToString();
                                ext[index++] = item;
                                //item = new RdExtensionItem();
                                //item.key = "";
                                //item.value = oXmlIn.CIParam;
                                //ext[index++] = item;

                                item = new RdExtensionItem();
                                item.key = "PDFCICertify";
                                item.value = PdfCertifyB64;
                                ext[index++] = item;
                                //item = new RdExtensionItem();
                                //item.key = "PDFCIDeclaracionJurada";
                                //item.value = PdfDecJuradaB64;
                                //ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "QRCIVerify";
                                item.value = QRverify;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Score";
                                item.value = score.ToString("##.##") + "%";
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "Pais";
                                item.value = "Chile";
                                ext[index++] = item;

                                item = new RdExtensionItem();
                                item.key = "IDCardImageBack";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? "" : oXmlIn.CIParam.IDCardImageBack;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "ManualSignatureImage";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.ManualSignatureImage) ? "" : oXmlIn.CIParam.ManualSignatureImage;
                                ext[index++] = item;
                                item = new RdExtensionItem();
                                item.key = "URLVideo";
                                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.URLVideo) ? "" : oXmlIn.CIParam.URLVideo;
                                ext[index++] = item;

                                //item = new RdExtensionItem();
                                //item.key = "IDCardPhotoImageDJ";
                                //item.value = oXmlIn.CIParam.IDCardPhotoImage;
                                //ext[index++] = item;
                                LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD - Generando Recibo Digital - ReciboId = " + reciboID);
                                Rd objRD = new Rd(ip, strapp, oXmlIn.Origin.ToString(), dNow, oXmlIn.Description, ext, reciboID, 1);

                                //Firmo RD
                                string xmlTosign = Bio.Digital.Receipt.libs.XmlUtils.SerializeObject(objRD);
                                string xmlsigned = null;
                                string msgerr = null;
                                LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD - Firmando Recibo Digital - ReciboId = " + reciboID);
                                int ierr = XMLSignatureHelper.Firmar(oXmlIn.ApplicationId, reciboID, xmlTosign, out xmlsigned, out msgerr);

                                //Si está habilitado el TSA => Envio
                                if (ierr == 0)
                                {
                                    if (Global.TSA_ENABLED)
                                    {
                                        LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - Agregando TS al Recibo Digital - ReciboId = " + reciboID);
                                        resTS = Global.TSA_MANAGER.GenerateTS(Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(xmlsigned)), out sXMLTS);
                                        if (resTS == 0)
                                        {
                                            sRDEnvelope = "<RDEnvelope>" + xmlsigned + sXMLTS + "</RDEnvelope>";
                                            isSaveOK = true;
                                        }
                                        else
                                        {
                                            isSaveOK = false;
                                            oXmlOut.Message = "Error generando TS";
                                            LOG.Error(oXmlOut.Message);
                                            oXmlOut.Receipt = null;
                                            ret = Errors.IRET_ERR_TSA;
                                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                        }
                                    }
                                    else
                                    {
                                        isSaveOK = true;
                                        sRDEnvelope = "<RDEnvelope>" + xmlsigned + "</RDEnvelope>";
                                    }

                                    if (isSaveOK)
                                    {
                                        try
                                        {
                                            CopyParams(oXmlIn, out oXmlOut);
                                            RdRecibos ord = new RdRecibos();
                                            ord.Reciboid = reciboID;
                                            ord.Descripcion = string.IsNullOrEmpty(oXmlIn.Description) ? "Certificacion de Identidad" : oXmlIn.Description;
                                            ord.Fecha = dNow;
                                            ord.Iporigen = HttpContext.Current.Request.UserHostAddress;
                                            ord.RdAplicaciones = AdministradorRdAplicaciones.BuscarRdAplicacionesById(oXmlIn.ApplicationId);
                                            ord.Reciboxml = sRDEnvelope; // xmlsigned;
                                            ord.Version = "1.0";
                                            LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD - Salvando en BD el Recibo Digital - ReciboId = " + reciboID);
                                            ord = AdministradorRdRecibos.Save(ord);
                                            LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD - Salvado en BD el Recibo Digital - ReciboId = " + reciboID);
                                            //PersistentManager.session().Save(ord);
                                            //PersistentManager.session().Flush();

                                            //Update RdCITx
                                            msg = null;
                                            LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD - Updating rdCITx en BD TrackId = " + oXmlIn.CIParam.TrackId);
                                            AdministradorRdCITx.UpdateCITx(oXmlIn, ord, TrackIdBP, TrackIDE, PdfCertifyB64, resultverify, score, Properties.Settings.Default.CIBPTreshold,
                                                                           oXmlIn.CIParam.Name, oXmlIn.CIParam.PhaterLastName, oXmlIn.CIParam.MotherLastName, null, out msg);
                                            //AdministradorRdCITx.UpdateCITx(oXmlIn, ord, TrackIdBP, TrackIDE, PdfCertifyB64, resultverify, out msg);

                                            oXmlOut.Receipt = sRDEnvelope; //xmlsigned;
                                            oXmlOut.Trackid = reciboID;
                                            oXmlOut.CIParam.TrackId = rdCITx.TrackId;
                                            oXmlOut.ResultVerify = resultverify;
                                            oXmlOut.CIParam.CertificatePDF = PdfCertifyB64;
                                            oXmlOut.QR = QRverify;
                                            oXmlOut.Timestampend = DateTime.Now;

                                            if (Properties.Settings.Default.CIInformResultByMail == 1)
                                            {
                                                //Added 10-05-2020 - Send mail HTMl con certificados attachados. MAndo PDF + XML
                                                LOG.Debug("Informo por mail " + oXmlOut.CIParam.DestinaryMail + " a destinatario enviadno docs...");
                                                string[] strbody = new string[2];
                                                strbody[0] = "Usted completó la Certificación de identidad para el documento <b>" +
                                                                oXmlOut.CIParam.TypeId + oXmlOut.CIParam.ValueId + "</b>";
                                                strbody[1] = "El codigo unico del tramite para consultas es: <b>" + oXmlOut.CIParam.TrackId + "</b>";
                                                byte[] docpdf = Convert.FromBase64String(oXmlOut.CIParam.CertificatePDF);
                                                byte[] docxml = System.Text.Encoding.UTF8.GetBytes(oXmlOut.Receipt); 
                                                //Send PDF

                                                bool bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                                                                    "Certificación de Identidad completada con exito [PDF]", strbody, docpdf, "pdf",
                                                                    oXmlOut.CIParam.TrackId + ".pdf");
                                                LOG.Debug("Enviado PDF => " + (bret.ToString()));
                                                //bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                                                //                    "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
                                                //                    oXmlOut.CIParam.TrackId + ".xml");
                                                //LOG.Debug("Enviado XML => " + (bret.ToString()));
                                            }

                                            //Added 20-04-2020 - Return CallBack generico si UrlCallback != null en RdAplicaciones
                                            CheckSendCallback(ord.RdAplicaciones, sRDEnvelope);

                                            if (oXmlIn.Actionid == 44) //Pide respuesta liviana, por lo que se elimina todo lo pesado
                                            {
                                                oXmlOut.Receipt = null;
                                                oXmlOut.CIParam.CertificatePDF = null;
                                                oXmlOut.CIParam.IDCardImageFront = null;
                                                oXmlOut.CIParam.IDCardImageBack = null;
                                                oXmlOut.CIParam.IDCardPhotoImage = null;
                                                oXmlOut.CIParam.IDCardSignatureImage = null;
                                                oXmlOut.CIParam.Selfie = null;
                                                oXmlOut.CIParam.Map = null;
                                                oXmlOut.QR = null;
                                            }

                                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);


                                            ret = Errors.IRET_OK;
                                        }
                                        catch (Exception exDB)
                                        {
                                            LOG.Error("Error salvando en BD [" + exDB.Message + "]");
                                            oXmlOut.Message = "Error salvando en BD [" + exDB.Message + "]";
                                            oXmlOut.Receipt = null;
                                            ret = Errors.IRET_ERR_SAVE_DB;
                                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                        }
                                    }
                                }
                                else
                                {
                                    oXmlOut.Message = "Error Firmando [" + msgerr + "]";
                                    LOG.Error("CIServiceManager.GenerateCI_w_SelfieVsBD - " + oXmlOut.Message);
                                    oXmlOut.Receipt = null;
                                    ret = Errors.IRET_ERR_SIGNATURE;
                                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                oXmlOut.Message = "CIServiceManager.GenerateCI_w_SelfieVsBD Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                LOG.Error(oXmlOut.Message);
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }
            LOG.Debug("CIServiceManager.GenerateCI_w_SelfieVsBD OUT!");
            return ret;
        }

        internal static string GetStatus(RdCITx tx)
        {
            LOG.Debug("CIServiceManager.GetStatus IN => tx != nul => " + (tx != null).ToString());
            string ret = "PENDING";
            if (tx == null) return ret;
            try
            {
                switch (tx.ActionId)
                {
                    case -701:
                            ret = "ERRFAILE3ero [" + tx.ActionId + "]";
                            break;
                    case -702:
                            ret = "ERRNULL3ro [" + tx.ActionId + "]";
                            break;
                    case -703:
                        ret = "ERRMANIPULATED [" + tx.ActionId + "]";
                        break;
                    case -704:
                            ret = "ERRIDTYPENOTSUPPORTED [" + tx.ActionId + "]";
                            break;
                    case -705:
                            ret = "ERRCOUNTRYNOTSUPPORTED [" + tx.ActionId + "]";
                            break;
                    case -706:
                            ret = "ERRIDNOTLEGIBLE [" + tx.ActionId + "]";
                            break;
                    case -707:
                            ret = "ERRDOCNOTUPLOADED [" + tx.ActionId + "]";
                            break;
                    case -708:
                            ret = "ERRTRACKIDNOTEXIST [" + tx.ActionId + "]";
                            break;
                    case -10:
                        ret = "EXPIRED [" + tx.ActionId + "]";
                        break;
                    case -11:
                        ret = "NEGATIVE [" + tx.ActionId + "]";
                        break;
                    case -12:
                        ret = "FRAUD [" + tx.ActionId + "]";
                        break;
                    case 1:
                    case 2:
                    case 22:
                    case 23:
                    case 10: //Ok Poder TTag
                        if (IsTrackIdVencida(tx))
                        {
                            ret = "EXPIRED [" + tx.ActionId + "]";
                        } else
                        {
                            ret = "IN PROGRESS [" + tx.ActionId + "]";
                        }
                        break;
                    case 3:
                    case 33:
                    case 8:
                    case 88:
                    case 4:
                    case 44:
                    case 9:
                    case 99:
                    case 18: //Ok Poder TTag
                    case 19: //Ok Poder TTag
                        ret = "DONE"; //DONE
                        break;
                    default:
                        ret = "NA [" + tx.ActionId + "]";
                        break;
                }
            }
            catch (Exception ex)
            {
                ret = "UNKNOW";
                LOG.Error("CIServiceManager.GetStatus Error: " + ex.Message);
            }
            LOG.Error("CIServiceManager.GetStatus return => " + ret);
            return ret;
        }

        internal static string GetStatusDescription(int actionId)
        {
            LOG.Debug("CIServiceManager.GetStatusDescription IN => actionId => " + actionId.ToString());
            string ret = "Estado Desconocido";
            switch (actionId)
            {
                case -701:
                    ret = "ERR FAILED 3ero";
                    break;
                case -702:
                    ret = "ERR NULL 3ro";
                    break;
                case -703:
                    ret = "ERR FRAUD DOCUMENT MANIPULATED";
                    break;
                case -704:
                    ret = "ERR IDTYPE NOT SUPPORTED";
                    break;
                case -705:
                    ret = "ERR COUNTRY NOT SUPPORTED";
                    break;
                case -706:
                    ret = "ERR ID NOT LEGIBLE";
                    break;
                case -707:
                    ret = "ERR DOC NOT UPLOADED";
                    break;
                case -708:
                    ret = "ERR TRACK ID NOTEXIST";
                    break;
                case -10:
                    ret = "EXPIRED";
                    break;
                case -11:
                    ret = "NEGATIVE";
                    break;
                case -12:
                    ret = "FRAUD";
                    break;
                case 1:
                    ret = "CREATED";
                    break;
                case 2:
                case 22:
                case 23:
                    ret = "OCR RECOGNIZED [" + actionId + "]";
                    break;
                case 3:
                case 33:
                case 8:
                case 88:
                    ret = "DONE DOCUMENT_SELFIE [" + actionId + "]";
                    break;
                case 4:
                case 44:
                case 9:
                case 99:
                    ret = "DONE CI SELFIE_BD [" + actionId + "]";
                    break;
                case 5:
                    ret = "CI VERIFY";
                    break;
                case 6:
                    ret = "CI GET";
                    break;
                case 7:
                    ret = "CI GET IDENTITY";
                    break;
                case 18: //Ok Poder TTag
                case 19: //Ok Poder TTag
                    ret = "DONE DOCUMENT [" + actionId + "]";
                    break;
                default:
                    ret = "Estado Desconocido";
                    break;
            }
            LOG.Error("CIServiceManager.GetStatusDescription return => " + ret);
            return ret;
        }

        /// <summary>
        /// Graba en directorio el video o imagen del primer parametro en B64 
        /// segun sea 1 (video) o 2 (image) el parametro 2
        /// </summary>
        /// <param name="name"></param>
        /// <param name="strVideoOrImage"></param>
        /// <param name="type"></param>
        internal static void SaveDebugData(string name, string strVideoOrImage, int type)
        {
            try
            {
                string semilla = DateTime.Now.ToString("yyyyMMddHHmmss") + "_";
                if (string.IsNullOrEmpty(strVideoOrImage))
                {
                    LOG.Debug("CIServiceManager.SaveDebugData strVideoOrImage = null and type = " + type);
                }
                else
                {
                    if (type == 3) //Es para grabar XmlParamIn y Out plano en txt
                    {
                        System.IO.File.WriteAllText(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + semilla + name + ".xml", strVideoOrImage);
                    }
                    else
                    {
                        byte[] byVideoOrImage = Convert.FromBase64String(strVideoOrImage);
                        switch (type)
                        {
                            case 1: //es video
                                System.IO.File.WriteAllBytes(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + semilla + name + ".mp4", byVideoOrImage);
                                break;
                            case 2: //es imagen
                                System.IO.File.WriteAllBytes(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + semilla + name + ".jpg", byVideoOrImage);
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIServiceManager.SaveDebugData Error - " + ex.Message);
            }
        }



        private static string GetValueFromExtension(Api.RdExtensionItem[] extensiones, string key)
        {
            string ret = null;
            bool bFound = false;
            try
            {
                foreach (Api.RdExtensionItem item in extensiones)
                {
                    if (item.key.Equals(key))
                    {
                        ret = (string)item.value;
                        LOG.Error("CIServiceManager.GetValueFromExtension Found Key = " + key);
                        bFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                bFound = false;
                LOG.Error("CIServiceManager.GetValueFromExtension Error - " + ex.Message);
            }
            if (!bFound) LOG.Error("CIServiceManager.GetValueFromExtension NOT Found Key = " + key);
            return ret;
        }


        /// <summary>
        /// Verifica que vengan los parámetros necesarios para cada accion
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static bool IsCorrectParamInForAction(XmlParamIn oXmlIn, out string msgerr)
        {
            msgerr = null;
            bool ret = false;
            try
            {

                //Generate Token
                if ((oXmlIn.Actionid == 1) && (oXmlIn.Companyid == 0 || oXmlIn.ApplicationId == 0) && 
                    (oXmlIn.CIParam == null || string.IsNullOrEmpty(oXmlIn.CIParam.TypeId) || string.IsNullOrEmpty(oXmlIn.CIParam.ValueId))
                    || string.IsNullOrEmpty(oXmlIn.CIParam.DestinaryMail))
                {
                    LOG.Info("CIServiceManager.IsCorrectParamInForAction - ActionId = 1 => " +
                        "CompanyId = " + oXmlIn.Companyid + " - AppId = " + oXmlIn.ApplicationId + 
                        " - TypeId = " + (string.IsNullOrEmpty(oXmlIn.CIParam.TypeId)?"NULL": oXmlIn.CIParam.TypeId) +
                        " - ValueId = " + (string.IsNullOrEmpty(oXmlIn.CIParam.ValueId) ? "NULL" : oXmlIn.CIParam.ValueId) +
                        " - DestinaryMail = " + (string.IsNullOrEmpty(oXmlIn.CIParam.DestinaryMail) ? "NULL" : oXmlIn.CIParam.DestinaryMail));
                    msgerr = "ComanyId = 0 y/o Id Aplicaicon = 0 o TypeId/ValueId/DestinataryMail nulos. Imposible Generar Token CI";
                    ret = false;
                }

                //IDCardVerification + OCR
                if (oXmlIn.Actionid == 2 && 
                    (oXmlIn.Companyid == 0 || oXmlIn.ApplicationId == 0 || oXmlIn.CIParam == null || 
                     string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront)) &&
                      (oXmlIn.CIParam == null || string.IsNullOrEmpty(oXmlIn.CIParam.TypeId) || string.IsNullOrEmpty(oXmlIn.CIParam.ValueId)))
                {
                    msgerr = "Companyid = 0 o ApplicationId = 0 o IDCardImage nula, no se puede verificar IDCard o TypeId/ValueId Nulos ";
                    ret = false;
                }

                //Verify Cedula vs Selfie
                //(oXmlIn.Extensiones == null || oXmlIn.Extensiones.Length != 7 || oXmlIn.Companyid == 0 || oXmlIn.ApplicationId == 0))
                if (oXmlIn.Actionid == 3 && 
                   (oXmlIn.CIParam == null || string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront) || string.IsNullOrEmpty(oXmlIn.CIParam.Selfie) || string.IsNullOrEmpty(oXmlIn.CIParam.DestinaryMail) ||
                    oXmlIn.Companyid == 0 || oXmlIn.ApplicationId == 0))
                {
                    LOG.Info("CIServiceManager.IsCorrectParamInForAction - ActionId = 2 => IDCardImage = null => " + 
                        string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront) + " - Selfie = null => " + string.IsNullOrEmpty(oXmlIn.CIParam.Selfie)
                        + " - CompanyId = " + oXmlIn.Companyid + " - AppId = " + oXmlIn.ApplicationId);
                    msgerr = "Error en parametros de entrada.";
                    ret = false;
                }

                if (oXmlIn.Actionid == 6 &&
                   (oXmlIn.CIParam == null || string.IsNullOrEmpty(oXmlIn.CIParam.TrackId) || oXmlIn.Companyid == 0 || oXmlIn.ApplicationId == 0))
                {
                    LOG.Info("CIServiceManager.IsCorrectParamInForAction - ActionId = 6 => TrackId = null => " +
                        string.IsNullOrEmpty(oXmlIn.CIParam.TrackId) + " - CompanyId = " + oXmlIn.Companyid + " - AppId = " + oXmlIn.ApplicationId);
                    msgerr = "Error en parametros de entrada.";
                    ret = false;
                }

                ret = true;
            }
            catch (Exception ex)
            {
                LOG.Error("CIServiceManager.IsCorrectParamInForAction Error - " + ex.Message);
                ret = false;
            }
            return ret;
        }


        /// <summary>
        /// Chequea que la aplicaicon y la compañía estén habilitadas
        /// </summary>
        /// <param name="idaplicacion"></param>
        /// <param name="qextensiones"></param>
        /// <param name="app"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal static int CheckAppAndCompanyEnabled(int idaplicacion, out int qextensiones, out string app, out string msg)
        {
            int ret = 0;
            msg = null;
            app = null;
            qextensiones = 0;
            try
            {
                RdAplicaciones appObj = AdministradorRdAplicaciones.BuscarRdAplicacionesById(idaplicacion);

                if (appObj == null)
                {
                    msg = "Aplicacion No Existe";
                    return Errors.IRET_ERR_APLICATION_NOT_EXIST;
                }
                else
                {
                    app = appObj.Nombre;

                    if (appObj.Deleted.HasValue)
                    {
                        msg = "Aplicación [" + app + "] Deshabilitada desde " + appObj.Deleted.Value.ToString("dd/MM/yyyy");
                        return Errors.IRET_ERR_APLICATION_DISABLED;
                    }
                    qextensiones = appObj.QExtensiones;
                    Company company = AdministradorCompany.BuscarCompanyById(appObj.Company);
                    if (company == null)
                    {
                        msg = "Company No Existe";
                        return Errors.IRET_ERR_COMPANY_NOT_EXIST;
                    }
                    else if (company.EndDate.HasValue && company.EndDate < DateTime.Now)
                    {
                        msg = "Company deshabilitada desde " + company.EndDate.Value.ToString("dd/MM/yyyy");
                        return Errors.IRET_ERR_COMPANY_DISABLED;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Error CheckCompanyEnabled", ex);
            }
            return ret;
        }

        internal static int CheckAppAndCompanyEnabled(int companyid, int idaplicacion, out int qextensiones, out string app, out string msg)
        {
            int ret = 0;
            msg = null;
            app = null;
            qextensiones = 0;
            try
            {
                RdAplicaciones appObj = AdministradorRdAplicaciones.BuscarRdAplicacionesById(idaplicacion);

                if (appObj == null)
                {
                    msg = "Aplicacion No Existe";
                    return Errors.IRET_ERR_APLICATION_NOT_EXIST;
                }
                else
                {
                    app = appObj.Nombre;

                    if (appObj.Deleted.HasValue)
                    {
                        msg = "Aplicación [" + app + "] Deshabilitada desde " + appObj.Deleted.Value.ToString("dd/MM/yyyy");
                        return Errors.IRET_ERR_APLICATION_DISABLED;
                    }
                    qextensiones = appObj.QExtensiones;
                    Company company = AdministradorCompany.BuscarCompanyById(companyid);
                    if (company == null)
                    {
                        msg = "Company No Existe";
                        return Errors.IRET_ERR_COMPANY_NOT_EXIST;
                    }
                    else if (company.EndDate.HasValue && company.EndDate < DateTime.Now)
                    {
                        msg = "Company deshabilitada desde " + company.EndDate.Value.ToString("dd/MM/yyyy");
                        return Errors.IRET_ERR_COMPANY_DISABLED;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Error CheckCompanyEnabled", ex);
            }
            return ret;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="xmlreceipt"></param>
        ///// <param name="verifytype">1 - Solo existnecia en BD |
        /////                                2 - Existencia en BD y Check Igual contenido 
        ///// </param>
        ///// <returns></returns>
        public static int VerifyCI(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /*  NOTA: Los parametros luego en version final hay que cambiarlo, para que sea dinamico
             *      y se controlen con las extensiones definidas.
             *  1.- Serializo param
             *  2.- Chequeo que exista en BD sino => Retorno que no existe 
             *  3.- Si existe => Check Firma
             *  4.- Retorno
            */
            int ret = Errors.IRET_OK;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;
            try
            {

                //*  2.- Genero RD xml y firmo 
                DateTime dNow = DateTime.Now;
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                // AGREGAR CONTROL DE LOGIN
                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string strapp;
                //int retaux = CheckCompanyEnabled(oXmlIn.Companyid, out msg);
                //if (retaux < 0)
                //{
                //    oXmlOut.Message = msg;
                //    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                //}

                //*  2.- Check en BD 
                CopyParams(oXmlIn, out oXmlOut);
                string xmlreceipt = ExtractReceiptFromEnvelope(oXmlIn.Receipt);
                Rd orecibo = libs.XmlUtils.DeserializeObject<Rd>(xmlreceipt);
                RdRecibos dbRecibo = AdministradorRdRecibos.BuscarRdReciboByReciboId(orecibo.reciboID);

                if (dbRecibo == null)
                {
                    oXmlOut.Trackid = oXmlIn.ReceiptId;
                    oXmlOut.Message = "El recibo  con ID = " + orecibo.reciboID
                                      + " no se encuentra en el repositorio seguro";
                    /// 0 - Recibo OK | 1 - Recibo existe pero firma inválida | 2 - recibo no existe
                    oXmlOut.ResultVerify = 2;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    ret = Errors.IRET_ERR_RECEIPT_NO_IN_DB;
                }
                else
                {
                    string msgerr = null;
                    int retV = XMLSignatureHelper.VerificarFirma(dbRecibo.RdAplicaciones.Id, xmlreceipt, out msgerr);
                    if (retV != 0)
                    {
                        oXmlOut.Trackid = oXmlIn.ReceiptId;
                        oXmlOut.Message = "El recibo  con ID = " + orecibo.reciboID
                                          + " tiene firma inválida";
                        /// 0 - Recibo OK | 1 - Recibo existe pero firma inválida | 2 - recibo no existe
                        oXmlOut.ResultVerify = 1;
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        ret = Errors.IRET_ERR_SIGNATURE_INVALID;
                        //return ret;
                    }
                    else
                    {
                        oXmlOut.Trackid = oXmlIn.ReceiptId;
                        oXmlOut.Message = "El recibo  con ID = " + orecibo.reciboID
                                          + " es VALIDO";
                        /// 0 - Recibo OK | 1 - Recibo existe pero firma inválida | 2 - recibo no existe
                        oXmlOut.ResultVerify = 0;
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        ret = Errors.IRET_OK;
                        //return ret;
                    }
                }

            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }

            return ret;
        }

        private static int CheckCompanyEnabled(int companyid, out string msg)
        {
            int ret = 0;
            msg = null;
            try
            {
                Company company = AdministradorCompany.BuscarCompanyById(companyid);
                if (company == null)
                {
                    msg = "Company No Existe";
                    return Errors.IRET_ERR_COMPANY_NOT_EXIST;
                }
                else if (company.EndDate.HasValue && company.EndDate < DateTime.Now)
                {
                    msg = "Company deshabilitada desde " + company.EndDate.Value.ToString("dd/MM/yyyy");
                    return Errors.IRET_ERR_COMPANY_DISABLED;
                }

            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Error CheckCompanyEnabled", ex);
            }
            return ret;
        }

        private static string ExtractReceiptFromEnvelope(string xmlenvelope)
        {
            string sret = null;
            try
            {
                XmlDocument rdEnvelope = new XmlDocument();
                rdEnvelope.PreserveWhitespace = false;
                rdEnvelope.LoadXml(xmlenvelope);

                XmlNode node = rdEnvelope.GetElementsByTagName("Rd")[0];

                sret = node.OuterXml;
            }
            catch (Exception ex)
            {
                LOG.Error("Error ExtractReceiptFromEnvelope", ex);
            }
            return sret;
        }

        private static void CheckSendCallback(RdAplicaciones rdAplicaciones, string rdenvelope)
        {
            try
            {
                LOG.Debug("CIServiceManager.CheckSendCallback IN...");
                if (rdAplicaciones != null && !string.IsNullOrEmpty(rdAplicaciones.UrlCallback))
                {
                    //Added 07-05-2020 - Update internametne estado de Evidencia en NV para evitr usar WebHook
                    if (Properties.Settings.Default.NVUpadteCILocal)
                    {
                        rest.Controllers.NotarizeController.ProcessWebhookReceiveCI(rdenvelope);
                    }
                                       
                    RestClient Client = new RestClient();
                    LOG.Debug("CIServiceManager.CheckSendCallback - UrlCallback = " + rdAplicaciones.UrlCallback + "...");
                    var request = new RestRequest(rdAplicaciones.UrlCallback, Method.POST);
                    request.Timeout = 60000;
                    request.AddJsonBody(rdenvelope);
                    
                    IRestResponse response = Client.Execute(request);
                    var content = response.Content;
                    LOG.Debug("CIServiceManager.CheckSendCallback - Respuesta de destino = " +
                              (content != null ? content.ToString() : "NULL"));
                } else
                {
                    LOG.Debug("CIServiceManager.CheckSendCallback - No send callback...");
                    if (rdAplicaciones == null)
                    {
                        LOG.Debug("CIServiceManager.CheckSendCallback - rdAplicaciones=null => true");
                    } else
                    {
                        LOG.Debug("CIServiceManager.CheckSendCallback string.IsNullOrEmpty(rdAplicaciones.UrlCallback) => " +
                            (string.IsNullOrEmpty(rdAplicaciones.UrlCallback)).ToString());
                    }

                }

            }
            catch (Exception ex)
            {
                LOG.Error("CIServiceManager.CheckSendCallback Error: " + ex.Message);
            }
            LOG.Debug("CIServiceManager.CheckSendCallback OUT!");
        }

        #region V2

        //internal static int GenerateNv2Tx_From_BPWeb(RdCITx tx, XmlParamIn oXmlIn, int _resultVerify, double _score,
        //                                                bool sendMails, string jsonform, out string xmlparamout)
        //{
        //    /* 
        //     *  1.- Verifico que la info venga ok
        //     *  2.- Sin Importar el resultado de verificacion
        //     *     2.1.- Si dio positivo o Negativo
        //     *            2.2.1.- Creo Recibo XML y guardo en BD
        //     *            2.2.2.- Creo PDF con Doc original + CI y guardo en BD (Vero
        //     *  3.- Retorno 
        //    */
        //    int ret = Errors.IRET_OK;
        //    string sXMLTS = null;
        //    int resTS = 0;
        //    string sRDEnvelope = null;
        //    bool isSaveOK = false;
        //    XmlParamOut oXmlOut = new XmlParamOut();
        //    xmlparamout = null;
        //    string securitycode = "Null";

        //    try
        //    {
        //        LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb IN...");

        //        string reciboID = AdministradorRdRecibos.GetTrackIdUnique();// Guid.NewGuid().ToString("N");
        //        string ip = HttpContext.Current.Request.UserHostAddress;
        //        if (String.IsNullOrEmpty(ip))
        //            ip = oXmlIn.Ipenduser;
        //        if (ip == null || ip.Length < 9)
        //            ip = "127.0.0.1";

        //        //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
        //        //      Obtengo el id de compania para ver si existe, 
        //        //      chequeo user/pass 
        //        LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb CheckAppAndCompanyEnabled IN...");
        //        string msg;
        //        string strapp;
        //        int qext = 0;
        //        LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb CheckAppAndCompanyEnabled IN...");
        //        RdAplicaciones app = AdministradorRdAplicaciones.BuscarRdAplicacionesById(oXmlIn.ApplicationId);
        //        LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb CheckAppAndCompanyEnabled Out...");
        //        if (app == null)
        //        {
        //            oXmlOut.Message = "Recuperacion ApplicationId = " + oXmlIn.ApplicationId + " => NUll";
        //            LOG.Error("CIServiceManager.GenerateSignerTx_From_BPWeb Recuperacion ApplicationId = " + oXmlIn.ApplicationId + " => NUll");
        //            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
        //            return Errors.IRET_API_ERR_NOT_FOUND * -1;
        //        }
        //        msg = null;
        //        LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb Generando CI con TrackId = " + oXmlIn.CIParam.TrackId);
        //        RdCITx rdCITx = tx; //Viene por parametro
        //                            //AdministradorRdCITx.GetCITxByTrackId(oXmlIn.Actionid, oXmlIn.CIParam.TrackId,
        //                            //                                                    oXmlIn.Companyid, oXmlIn.CIParam.ValidityType, true,
        //                            //                                                    oXmlIn.CIParam.TypeId, oXmlIn.CIParam.ValueId, null,
        //                            //                                                    oXmlIn.CIParam.DestinaryMail, out msg);

        //        if (rdCITx == null)
        //        {
        //            oXmlOut.Message = msg;
        //            LOG.Error("CIServiceManager.GenerateSignerTx_From_BPWeb Error recuperanto rcCITx - msg=" + msg);
        //            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
        //            return Errors.IRET_ERR_CI_TRACKID_NO_IN_DB;
        //        }
        //        else
        //        {
        //            //Copio trackid para actualizar luego en la BD. Dado que recien se creo la Tx debo tomar aca el TrackId
        //            oXmlIn.CIParam.TrackId = rdCITx.TrackId;
        //        }


        //        //Added para manejar el llamado liviano, que no venga la info de cedula porque ya se hizo => llenamos oXmlIn desde BD
        //        // sino, si el estado es que no se hizo IDCardVerification antes, lo hacemos, y llenamos oXmlIn para seguir
        //        bool sigue = true;

        //        //Actualizo el ValueId si se envió "NA" en la llamada
        //        if (rdCITx.ValueId.Equals("NA") && !oXmlIn.CIParam.ValueId.Equals("NA"))
        //        {
        //            rdCITx.ValueId = oXmlIn.CIParam.ValueId;
        //            string msgerr = "";
        //            AdministradorRdCITx.UpdateCITx(rdCITx, out msgerr);
        //            LOG.Error("CIServiceManager.GenerateSignerTx_From_BPWeb Actualiza CITx.Id = " + rdCITx.Id +
        //                            " con ValueId = " + rdCITx.ValueId + " [MsgRet = " +
        //                            (string.IsNullOrEmpty(msgerr) ? "Sin Error" : msgerr));
        //        }
        //        //}


        //        //*  2.- Hago verify contra bioportal
        //        msg = null;
        //        int resultverify = 0;
        //        double score = 0;
        //        string TrackIdBP = tx.TrackIdBP;
        //        LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb Verificando contra BP...");
        //        resultverify = _resultVerify;
        //        score = _score;
        //        //int rptaBP = ExtractDataVerificationFromParamIn(oXmlIn, out msg, out resultverify, out score, out TrackIdBP); //BPHelper.VerifyCIvsCedula(oXmlIn, out msg, out resultverify, out score, out TrackIdBP);

        //        //if (rptaBP < 0)
        //        //{
        //        //    oXmlOut.Message = "Error verificando identidad [Err=" + rptaBP + " - MsgErr=" + msg + "]";
        //        //    LOG.Error("CIServiceManager.GenerateCI_From_BPWeb - " + oXmlOut.Message);
        //        //    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
        //        //    return Errors.IRET_ERR_CI_VERYFING;
        //        //}
        //        //else
        //        //{
        //        //Genero Certificado PDF con DE si que verifico positivo, sino error
        //        //if (resultverify == 2) //Verificacion negativa
        //        //{
        //        //    //TODO - Grabar en BD que el resultado fue negativo
        //        //    oXmlOut.Message = "Verificación Negativa [Score=" + score.ToString() + "]";
        //        //    LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - " + oXmlOut.Message);
        //        //    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
        //        //    return Errors.IRET_ERR_CI_NEGATIVE_VERIFICATION;
        //        //}
        //        //else 

        //        //Es positivo o negativo => No es bloqueante, es solo informativo y se usa para enviar al DE para generar el Poder.
        //        if (resultverify == 1 || resultverify == 2)
        //        {
        //            LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Verificación " +
        //                      (resultverify == 1 ? "Positiva" : "Negativa") + " [Score=" + score.ToString() +
        //                      (string.IsNullOrEmpty(tx.Threshold) ? "" : "/ Umbral=" + tx.Threshold) + "]");
        //            //Genero PDF via DE
        //            string PdfCertifyB64;
        //            string PdfDecJuradaB64;
        //            string QRverify = null; //GetQR(Properties.Settings.Default.BASE_SIGNER_CHECK + "?trackid=" + oXmlIn.CIParam.TrackId);
        //            string TrackIDE = null;
        //            string Map = null;
        //            msg = null;
        //            LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Generando Documento PDF...");
        //            int rptaDE = DEHelper.GeneraDEPDF(oXmlIn, score, QRverify, tx.Form, tx.CarRegisterImageFront, tx.CarRegisterImageBack,
        //                                        out msg, out PdfCertifyB64, out PdfDecJuradaB64, out TrackIDE, out Map, out securitycode);

        //            if (rptaDE < 0)
        //            {
        //                oXmlOut.Message = "Error generando PDF Document [Err=" + rptaDE + " - MsgErr=" + msg + "]";
        //                LOG.Error("CIServiceManager.GenerateSignerTx_From_BPWeb - " + oXmlOut.Message);
        //                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
        //                return Errors.IRET_ERR_CI_PDF_GENERATING;
        //            }
        //            else
        //            {
        //                LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Documento PDF Generado = " + PdfCertifyB64);

        //                oXmlIn.CIParam.Map = Map;
        //                DateTime dNow = DateTime.Now;
        //                RdExtensionItem[] ext = new RdExtensionItem[29];
        //                RdExtensionItem item;

        //                int index = 0;
        //                item = new RdExtensionItem();
        //                item.key = "TrackIdCI";
        //                item.value = oXmlIn.CIParam.TrackId;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "SecurityCode";
        //                item.value = securitycode;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "IDCardImage";
        //                item.value = oXmlIn.CIParam.IDCardImageFront;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "IDCardPhotoImage";
        //                item.value = oXmlIn.CIParam.IDCardPhotoImage;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "IDCardSignatureImage";
        //                item.value = oXmlIn.CIParam.IDCardSignatureImage;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "Selfie";
        //                item.value = oXmlIn.CIParam.Selfie;
        //                ext[index++] = item;

        //                item = new RdExtensionItem();
        //                item.key = "TypeId";
        //                item.value = oXmlIn.CIParam.TypeId;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "ValueId";
        //                item.value = oXmlIn.CIParam.ValueId;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "Name";
        //                item.value = (string.IsNullOrEmpty(oXmlIn.CIParam.Name) ? "" : oXmlIn.CIParam.Name) + " " +
        //                                (string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName) ? "" : oXmlIn.CIParam.PhaterLastName) + " " +
        //                                (string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName) ? "" : oXmlIn.CIParam.MotherLastName);
        //                ;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "BirthDate";
        //                item.value = oXmlIn.CIParam.BirthDate;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "IssueDate";
        //                item.value = oXmlIn.CIParam.IssueDate;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "Sex";
        //                item.value = (!string.IsNullOrEmpty(oXmlIn.CIParam.Sex) &&
        //                                (oXmlIn.CIParam.Sex.Equals("M") || oXmlIn.CIParam.Sex.Equals("F"))) ?
        //                                oXmlIn.CIParam.Sex : "Desconocido";
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "Nacionality";
        //                item.value = oXmlIn.CIParam.Nacionality;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "Serial";
        //                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.Serial) ? "" : oXmlIn.CIParam.Serial;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "ExpirationDate";
        //                item.value = oXmlIn.CIParam.ExpirationDate;
        //                ext[index++] = item;

        //                item = new RdExtensionItem();
        //                item.key = "GeoRef";
        //                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef) ? "" : oXmlIn.CIParam.GeoRef;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "WorkStationID";
        //                item.value = oXmlIn.CIParam.WorkStationID;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "ValidityType";
        //                item.value = oXmlIn.CIParam.ValidityType.ToString();
        //                ext[index++] = item;
        //                //item = new RdExtensionItem();
        //                //item.key = "";
        //                //item.value = oXmlIn.CIParam;
        //                //ext[index++] = item;

        //                item = new RdExtensionItem();
        //                item.key = "PDFCICertify";
        //                item.value = PdfCertifyB64;
        //                ext[index++] = item;
        //                //item = new RdExtensionItem();
        //                //item.key = "PDFCIDeclaracionJurada";
        //                //item.value = PdfDecJuradaB64;
        //                //ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "QRCIVerify";
        //                item.value = QRverify;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "Score";
        //                item.value = score.ToString("##.##") + "%";
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "Pais";
        //                item.value = "Chile";
        //                ext[index++] = item;

        //                item = new RdExtensionItem();
        //                item.key = "IDCardImageBack";
        //                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? "" : oXmlIn.CIParam.IDCardImageBack;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "ManualSignatureImage";
        //                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.ManualSignatureImage) ? "" : oXmlIn.CIParam.ManualSignatureImage;

        //                //TODO agregar URL Video
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "URLVideo";
        //                item.value = string.IsNullOrEmpty(oXmlIn.CIParam.URLVideo) ? "" : oXmlIn.CIParam.URLVideo;
        //                ext[index++] = item;

        //                //Agrego info de TramitesTAG
        //                item = new RdExtensionItem();
        //                item.key = "CarRegisterFront";
        //                item.value = string.IsNullOrEmpty(tx.CarRegisterImageFront) ? "" : tx.CarRegisterImageFront;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "CarRegisterBack";
        //                item.value = string.IsNullOrEmpty(tx.CarRegisterImageBack) ? "" : tx.CarRegisterImageBack;
        //                ext[index++] = item;

        //                item = new RdExtensionItem();
        //                item.key = "Writing";
        //                item.value = string.IsNullOrEmpty(tx.WritingImage) ? "" : tx.WritingImage;
        //                ext[index++] = item;
        //                item = new RdExtensionItem();
        //                item.key = "Form";
        //                item.value = string.IsNullOrEmpty(tx.Form) ? "" : tx.Form;
        //                ext[index++] = item;

        //                //item = new RdExtensionItem();
        //                //item.key = "IDCardPhotoImageDJ";
        //                //item.value = oXmlIn.CIParam.IDCardPhotoImage;
        //                //ext[index++] = item;
        //                LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Generando Recibo Digital - ReciboId = " + reciboID);
        //                Rd objRD = new Rd(ip, app.Nombre, oXmlIn.Origin.ToString(), dNow, oXmlIn.Description, ext, reciboID, 1);

        //                //Firmo RD
        //                string xmlTosign = Bio.Digital.Receipt.libs.XmlUtils.SerializeObject(objRD);
        //                string xmlsigned = null;
        //                string msgerr = null;
        //                LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Firmando Recibo Digital - ReciboId = " + reciboID);
        //                int ierr = XMLSignatureHelper.Firmar(oXmlIn.ApplicationId, reciboID, xmlTosign, out xmlsigned, out msgerr);

        //                //Si está habilitado el TSA => Envio
        //                if (ierr == 0)
        //                {
        //                    if (Global.TSA_ENABLED)
        //                    {
        //                        LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Agregando TS al Recibo Digital - ReciboId = " + reciboID);
        //                        resTS = Global.TSA_MANAGER.GenerateTS(Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(xmlsigned)), out sXMLTS);
        //                        if (resTS == 0)
        //                        {
        //                            sRDEnvelope = "<RDEnvelope>" + xmlsigned + sXMLTS + "</RDEnvelope>";
        //                            isSaveOK = true;
        //                        }
        //                        else
        //                        {
        //                            isSaveOK = false;
        //                            oXmlOut.Message = "Error generando TS";
        //                            LOG.Error(oXmlOut.Message);
        //                            oXmlOut.Receipt = null;
        //                            ret = Errors.IRET_ERR_TSA;
        //                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        isSaveOK = true;
        //                        sRDEnvelope = "<RDEnvelope>" + xmlsigned + "</RDEnvelope>";
        //                    }

        //                    if (isSaveOK)
        //                    {
        //                        try
        //                        {
        //                            CopyParams(oXmlIn, out oXmlOut);
        //                            RdRecibos ord = new RdRecibos();
        //                            ord.Reciboid = reciboID;
        //                            ord.Descripcion = string.IsNullOrEmpty(oXmlIn.Description) ? "Certificacion de Identidad" : oXmlIn.Description;
        //                            ord.Fecha = dNow;
        //                            ord.Iporigen = HttpContext.Current.Request.UserHostAddress;
        //                            ord.RdAplicaciones = AdministradorRdAplicaciones.BuscarRdAplicacionesById(oXmlIn.ApplicationId);
        //                            ord.Reciboxml = sRDEnvelope; // xmlsigned;
        //                            ord.Version = "1.0";
        //                            LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Salvando en BD el Recibo Digital - ReciboId = " + reciboID);
        //                            ord = AdministradorRdRecibos.Save(ord);
        //                            LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Salvado en BD el Recibo Digital - ReciboId = " + reciboID);
        //                            //PersistentManager.session().Save(ord);
        //                            //PersistentManager.session().Flush();

        //                            //Update RdCITx
        //                            msg = null;
        //                            LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb - Updating rdCITx en BD TrackId = " + oXmlIn.CIParam.TrackId);
        //                            AdministradorRdCITx.UpdateCITxSigner(tx, oXmlIn, ord, TrackIdBP, TrackIDE, PdfCertifyB64, resultverify, score, Properties.Settings.Default.CIBPTreshold,
        //                                                            oXmlIn.CIParam.Name, oXmlIn.CIParam.PhaterLastName, oXmlIn.CIParam.MotherLastName, securitycode, out msg);
        //                            //AdministradorRdCITx.UpdateCITx(oXmlIn, ord, TrackIdBP, TrackIDE, PdfCertifyB64, resultverify, out msg);

        //                            oXmlOut.Receipt = sRDEnvelope; //xmlsigned;
        //                            oXmlOut.Trackid = reciboID;
        //                            oXmlOut.CIParam.TrackId = rdCITx.TrackId;
        //                            oXmlOut.ResultVerify = resultverify;
        //                            oXmlOut.CIParam.CertificatePDF = PdfCertifyB64;
        //                            oXmlOut.QR = QRverify;
        //                            oXmlOut.Timestampend = DateTime.Now;

        //                            if (Properties.Settings.Default.CIInformResultByMail == 1 && sendMails)
        //                            {
        //                                //Added 10-05-2020 - Send mail HTMl con certificados attachados. MAndo PDF + XML
        //                                LOG.Debug("Informo por mail " + oXmlOut.CIParam.DestinaryMail + " a destinatario enviadno docs...");
        //                                string[] strbody = new string[2];
        //                                strbody[0] = "Usted completó la Certificación de identidad para el documento <b>" +
        //                                                oXmlOut.CIParam.TypeId + oXmlOut.CIParam.ValueId + "</b>";
        //                                strbody[1] = "El codigo unico del tramite para consultas es: <b>" + oXmlOut.CIParam.TrackId + "</b>";
        //                                byte[] docpdf = Convert.FromBase64String(oXmlOut.CIParam.CertificatePDF);
        //                                byte[] docxml = System.Text.Encoding.UTF8.GetBytes(oXmlOut.Receipt);
        //                                //Send PDF

        //                                bool bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
        //                                                    "Certificación de Identidad completada con exito [PDF]", strbody, docpdf, "pdf",
        //                                                    oXmlOut.CIParam.TrackId + ".pdf");
        //                                LOG.Debug("Enviado PDF => " + (bret.ToString()));
        //                                bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
        //                                                    "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
        //                                                    oXmlOut.CIParam.TrackId + ".xml");
        //                                LOG.Debug("Enviado XML => " + (bret.ToString()));
        //                            }

        //                            //Added 20-04-2020 - Return CallBack generico si UrlCallback != null en RdAplicaciones
        //                            CheckSendCallback(ord.RdAplicaciones, sRDEnvelope);

        //                            if (oXmlIn.Actionid == 88) //Pide respuesta liviana, por lo que se elimina todo lo pesado
        //                            {
        //                                oXmlOut.Receipt = null;
        //                                oXmlOut.CIParam.CertificatePDF = null;
        //                                oXmlOut.CIParam.IDCardImageFront = null;
        //                                oXmlOut.CIParam.IDCardImageBack = null;
        //                                oXmlOut.CIParam.IDCardPhotoImage = null;
        //                                oXmlOut.CIParam.IDCardSignatureImage = null;
        //                                oXmlOut.CIParam.Selfie = null;
        //                                oXmlOut.CIParam.Map = null;
        //                                oXmlOut.CIParam.ManualSignatureImage = null;
        //                                oXmlOut.QR = null;
        //                            }

        //                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);

        //                            ret = Errors.IRET_OK;
        //                        }
        //                        catch (Exception exDB)
        //                        {
        //                            LOG.Error("CIServiceManager.GenerateSignerTx_From_BPWeb - Error salvando en BD [" + exDB.Message + "]");
        //                            oXmlOut.Message = "Error salvando en BD [" + exDB.Message + "]";
        //                            oXmlOut.Receipt = null;
        //                            ret = Errors.IRET_ERR_SAVE_DB;
        //                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    oXmlOut.Message = "Error Firmando [" + msgerr + "]";
        //                    LOG.Error("CIServiceManager.GenerateSignerTx_From_BPWeb - " + oXmlOut.Message);
        //                    oXmlOut.Receipt = null;
        //                    ret = Errors.IRET_ERR_SIGNATURE;
        //                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
        //                }
        //            }
        //        }
        //        //}
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
        //        oXmlOut.Receipt = null;
        //        LOG.Error(oXmlOut.Message);
        //        ret = Errors.IRET_ERR_DESCONOCIDO;
        //        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
        //    }
        //    LOG.Debug("CIServiceManager.GenerateSignerTx_From_BPWeb OUT!");
        //    return ret;
        //}


        #endregion V2
    }
}


