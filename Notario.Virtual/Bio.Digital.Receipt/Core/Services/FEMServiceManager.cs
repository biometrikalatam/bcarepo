﻿using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.Core.Helpers;
using Bio.Digital.Receipt.pki;
using log4net;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using System.Web;
using System.Xml;

namespace Bio.Digital.Receipt.Core.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class FEMServiceManager
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(FEMServiceManager));

        internal static int Process(XmlParamIn oXmlIn, out string xmlparamout)
        {
            switch (oXmlIn.Actionid)
            { 
                case 1:  //Generacion para windows que viene con marca de agua incluida
                    return GenerateFEM(oXmlIn, out xmlparamout);
                    break;
                case 2:
                    return VerifyFEM(oXmlIn, out xmlparamout);
                    break;
                case 3:
                    return GetFEM(oXmlIn, out xmlparamout);
                    break;
                case 4: //Generacion para mobile que viene SIN marca de agua incluida
                    return GenerateFEM(oXmlIn, out xmlparamout);
                    break;
                default:
                    return GenerateFEM(oXmlIn, out xmlparamout);
                    break;

            }
        }


        /// <summary>
        /// Recibe el video, imagen, mac, georeferenciacion, etc, para certificar y devuelve recibo
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        private static int GenerateFEM(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /* 
             *  1.- Proceso Video
             *      1.1.- Saco Hash
             *      1.2.- Guardo en Storage Azure
             *  2.- Genero RD xml y firmo 
             *  3.- Grabo en BD
             *  4.- Retorno
            */
            int ret = Errors.IRET_OK;
            string sXMLTS = null;
            int resTS = 0;
            string sRDEnvelope = null;
            bool isSaveOK = false;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;
            string strHashVideo;
            byte[] arrVideo;
            string msgerrup;
            string urlvideoinstorage;
            string _strImgeMobileProcessed = null;
            try
            {
                LOG.Debug("FEMServiceManager.GenerateFEM IN...");

                string reciboID = Guid.NewGuid().ToString("N");
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                //*  1.- Proceso Video
                //     1.1.- Saco Hash
                string strVideo = GetValueFromExtension(oXmlIn.Extensiones, "Video");
                if (String.IsNullOrEmpty(strVideo))
                {
                    oXmlOut.Message = "Error Recupoerando Video";
                    LOG.Error(oXmlOut.Message);
                    oXmlOut.Receipt = null;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                } else
                {
                   
                    string _CompanyId = DatabaseHelper.GetCompanyName(oXmlIn.Companyid);
                    string _FirmanteId = GetValueFromExtension(oXmlIn.Extensiones, "FirmanteId");
                    string _DocumentId = GetValueFromExtension(oXmlIn.Extensiones, "DocumentId");
                    LOG.Debug("FEMServiceManager.GenerateFEM Starting FEM de Company = " + _CompanyId + " - FirmanteId = " + _FirmanteId + " - DocumentId = " + _DocumentId);
                    LOG.Debug("FEMServiceManager.GenerateFEM Generating Hash de strVideo = " + strVideo.Substring(0, 15));
                    arrVideo = Convert.FromBase64String(strVideo);
                    if (Properties.Settings.Default.DebugVideoImagen == 1 || Properties.Settings.Default.DebugVideoImagen == 3)
                    {
                        SaveDebugData(_CompanyId.ToString() + "_" + _FirmanteId.ToString() + "_" + _DocumentId.ToString(), strVideo, 1);
                    }

                    //Si es actionid = 4 => Hay que agregar la marca de agua al video => Convert

                    if (oXmlIn.Actionid == 4)
                    {
                        /*
                            1.- Grabo en Tmp el video identificandolo
                            2.- Agrego WM con datos específicos
                            3.- Retorno video resultado en byte[]
                        */
                        string strPaso = "Adding WM In...";
                        try
                        {
                            //I - Primero agrego WM en Video
                            string fullpathtmp = Global.TMP_PATH + reciboID + ".videoin.avi";
                            LOG.Debug("FEMServiceManager.GenerateFEM - Salvando video en tmp => " + fullpathtmp);
                            strPaso = "Salvando video en tmp";
                            System.IO.File.WriteAllBytes(fullpathtmp, arrVideo);
                            LOG.Debug("FEMServiceManager.GenerateFEM - Sixe video original = " + arrVideo.Length + " bytes");

                            byte[] videoout = null;
                            LOG.Debug("FEMServiceManager.GenerateFEM - VIDEO_HELPER.AddWaterMark call...");
                            strPaso = "Adding WM In...";

                            int iRet = 0;
                            Thread thread = new Thread(() =>
                            {
                                iRet = 0; // Bio.Core.Video.VideoProcessor.AddWaterMark(fullpathtmp, Global.ROOT + Properties.Settings.Default.WaterMarkFEM,
                                          //                              _CompanyId, _FirmanteId, _DocumentId, out videoout);
                                //iRet = Global.VIDEO_HELPER.AddWaterMark(fullpathtmp, Global.ROOT + Properties.Settings.Default.WaterMarkFEM,
                                //        _CompanyId, _FirmanteId, _DocumentId, out videoout);
                            });
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                            thread.Join();

                            thread.Abort();
                            thread = null;

                            strPaso = "AddWaterMark Out!";
                            LOG.Debug("FEMServiceManager.GenerateFEM - VIDEO_HELPER.AddWaterMark Out => CodeRet = " + iRet);
                            if (iRet != 0 || videoout == null)
                            {
                                oXmlOut.Message = "Error AddingWM In Video - [iRet = " + iRet + " - videoout==null =>" + (videoout == null).ToString() + "]";
                                LOG.Error(oXmlOut.Message);
                                //oXmlOut.Receipt = null;
                                //xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                                //return Errors.IRET_ERR_ADDINGWATERMARK;
                            }
                            else
                            {
                                arrVideo = videoout;  //Piso video original para procesar el que tiene Marca de Agua
                                LOG.Debug("FEMServiceManager.GenerateFEM - Size video converted = " + videoout.Length + " bytes");
                            }
                            strPaso = "AddWaterMark In Video OK!";
                            //Elimino tmp files
                            strPaso = "Elimina temp...";
                            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(Global.TMP_PATH);
                            foreach (System.IO.FileInfo fileToDelete in di.GetFiles(reciboID + "*"))  //Con comodin para seleccionar todos los archivos genreados desde esta semilla ReciboID
                            {
                                System.IO.File.Delete(fileToDelete.FullName);
                                LOG.Debug("FEMServiceManager.GenerateFEM - File tmp eliminado => " + fileToDelete.FullName);
                            }
                        }
                        catch (Exception ex)
                        {
                            oXmlOut.Message = "Error AddingWM In Video -[" + strPaso + " - " + ex.Message + "] - Usa Video Recibido sin procesar...";
                            LOG.Error(oXmlOut.Message);
                            //oXmlOut.Receipt = null;
                            //xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                            //return Errors.IRET_ERR_ADDINGWATERMARK;
                        }

                        //II - Segundo agrego WM en foto
                        //i.- Agrego los textos en marca de imagen de FE3D
                        try { 
                            strPaso = "Adding WM a foto... 0";
                            byte[] byImageWM = System.IO.File.ReadAllBytes(Global.ROOT + Properties.Settings.Default.WaterMarkFEM);
                            strPaso = "Adding WM a foto... 1";
                            MemoryStream msin = new MemoryStream(byImageWM);
                            strPaso = "Adding WM a foto... 2";
                            MemoryStream msou = new MemoryStream();
                            strPaso = "Adding WM a foto... 3";
                            Global.IMAGE_HELPER.AddWaterMark(msin, null, msou, "Arial", 16, System.Drawing.FontStyle.Regular, System.Drawing.Color.Black, 65, 120, 40,
                                _CompanyId, _FirmanteId, _DocumentId);
                            strPaso = "Adding WM a foto... 4";
                            //ii.- Superpongo imagen en la foto 
                            string strPhotoB64 = GetValueFromExtension(oXmlIn.Extensiones, "Photo");
                            if (Properties.Settings.Default.DebugVideoImagen == 2 || Properties.Settings.Default.DebugVideoImagen == 3)
                            {
                                SaveDebugData(_CompanyId.ToString() + "_" + _FirmanteId.ToString() + "_" + _DocumentId.ToString(), strPhotoB64, 2);
                            }
                            LOG.Debug("FEMServiceManager.GenerateFEM - byPhotoB64 = " + (!String.IsNullOrEmpty(strPhotoB64)?strPhotoB64.Substring(0,20)+"...":"NULL")); 
                            byte[] byPhoto = Convert.FromBase64String(strPhotoB64);
                            LOG.Debug("FEMServiceManager.GenerateFEM - byPhoto.len = " + (byPhoto != null ? byPhoto.Length.ToString() : "null"));
                            Image imageBase;
                            if (byPhoto != null)
                            {
                                MemoryStream msbackimage = new MemoryStream(byPhoto);
                                strPaso = "Adding WM a foto... 5";
                                imageBase = Image.FromStream(msbackimage);
                                msbackimage.Dispose();
                                msbackimage = null;
                            } else
                            {
                                imageBase = Global.IMAGE_FE3DLOGO;
                            }
                            strPaso = "Adding WM a foto... 6";
                            Image imageWM = Image.FromStream(msou);
                            strPaso = "Adding WM a foto... 7";
                            Image imageFinal = Global.IMAGE_HELPER.OverlayImages(imageBase, imageWM, 20, 20);
                            strPaso = "Adding WM a foto... 8";
                            MemoryStream msFinal = new MemoryStream();
                            strPaso = "Adding WM a foto... 9";
                            imageFinal.Save(msFinal, ImageFormat.Jpeg);
                            strPaso = "Adding WM a foto... 10";
                            _strImgeMobileProcessed = Convert.ToBase64String(msFinal.ToArray());
                            strPaso = "Added WM a foto OK!";

                            LOG.Debug("FEMServiceManager.GenerateFEM - Liberando recursos auxiliares para foto...");
                            byImageWM = null;
                            msin.Dispose();
                            msin = null;
                            msou.Dispose();
                            msou = null;
                            imageBase.Dispose();
                            imageBase = null;
                            imageWM.Dispose();
                            imageWM = null;
                            msFinal.Dispose();
                            msFinal = null;
                            imageFinal.Dispose();
                            imageFinal = null;
                            LOG.Debug("FEMServiceManager.GenerateFEM - Recursos auxiliares para foto Liberados!");

                        }
                        catch (Exception ex)
                        {
                            oXmlOut.Message = "Error AddingWM -[" + strPaso + " - " + ex.Message + "]";
                            LOG.Error(oXmlOut.Message);
                            oXmlOut.Receipt = null;
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                            return Errors.IRET_ERR_ADDINGWATERMARK;
                        }
                        
                    }

                    LOG.Debug("FEMServiceManager.GenerateFEM Generating Hash de arrVideo.length = " + arrVideo.Length);
                    SHA512Managed algoritmoHash = new SHA512Managed();
                    byte[] hashVideo = algoritmoHash.ComputeHash(arrVideo);
                    LOG.Debug("FEMServiceManager.GenerateFEM SHA512Managed.ComputeHash(arrVideo) de hashVideo.length = " + hashVideo.Length);
                    strHashVideo = Convert.ToBase64String(hashVideo);
                    LOG.Debug("FEMServiceManager.GenerateFEM Hash Generated! strHashVideo = " + strHashVideo);

                    //1.2.- Guardo en Storage de Azure
                    int iret = StorageHelper.UploadToStorageFEMInAzure(arrVideo, _CompanyId, _FirmanteId + "_" + _DocumentId, 
                                                                       out urlvideoinstorage, out msgerrup);

                    if (iret < 0)
                    {
                        oXmlOut.Message = "Error Uploading Video in Storage [" + msgerrup + "]";
                        LOG.Error(oXmlOut.Message);
                        oXmlOut.Receipt = null;
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_SAVE_DB;
                    } else
                    {
                        LOG.Debug("FEMServiceManager.GenerateFEM ubido a Storage File = " + urlvideoinstorage);
                    }
                }

                //*  2.- Genero RD xml y firmo 
                DateTime dNow = DateTime.Now;

                // AGREGAR CONTROL DE LOGIN
                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string strapp;
                int qext = 0;
                int retaux = CheckAppAndCompanyEnabled(oXmlIn.ApplicationId, out qext, out strapp, out msg);
                if (retaux < 0)
                {
                    oXmlOut.Message = msg;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }
                else  //Si lee bien el RdAplicacion => Chequeo cantidad de extensiones y que no lleguen nulas
                {
                    if (qext != oXmlIn.Extensiones.Length)
                    {
                        oXmlOut.Message = "Cantidad de extensiones no coinciden. Esperadas = " + qext
                            + " - Informadas = " + oXmlIn.Extensiones.Length;
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        return Errors.IRET_ERR_PARAM_INCORRECTOS;
                    }

                    bool hayNulos = false;
                    for (int i = 0; i < oXmlIn.Extensiones.Length; i++)
                    {
                        if (oXmlIn.Extensiones[i] == null
                            || String.IsNullOrEmpty(oXmlIn.Extensiones[i].key)
                            || String.IsNullOrEmpty(oXmlIn.Extensiones[i].value))
                        {
                            oXmlOut.Message = "Existen extensiones nulas. No permitido.";
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                            return Errors.IRET_ERR_PARAM_INCORRECTOS;
                        }
                    }
                }


                Bio.Digital.Receipt.Core.RdExtensionItem[] ext = new Bio.Digital.Receipt.Core.RdExtensionItem[oXmlIn.Extensiones.Length + 1];
                Bio.Digital.Receipt.Core.RdExtensionItem item;
                for (int i = 0; i < oXmlIn.Extensiones.Length; i++)
                {
                    item = new RdExtensionItem();
                    item.key = oXmlIn.Extensiones[i].key;
                    if (item.key.Equals("Video"))
                    {
                        item.value = strHashVideo;
                    }
                    else if (item.key.Equals("Photo") && !string.IsNullOrEmpty(_strImgeMobileProcessed) && oXmlIn.Actionid == 4)
                    {
                        item.value = _strImgeMobileProcessed;
                    } else 
                    {
                        item.value = oXmlIn.Extensiones[i].value;
                    }
                    ext[i] = item;
                }
                item = new RdExtensionItem();
                item.key = "qrlinkfem";
                item.value = GetQR(urlvideoinstorage);
                ext[oXmlIn.Extensiones.Length] = item;
                Rd objRD = new Rd(ip, strapp, oXmlIn.Origin.ToString(), dNow, oXmlIn.Description, ext, reciboID, 1);

                //Firmo RD
                string xmlTosign = Bio.Digital.Receipt.libs.XmlUtils.SerializeObject(objRD);
                string xmlsigned = null;
                string msgerr = null;
                int ierr = XMLSignatureHelper.Firmar(oXmlIn.ApplicationId, reciboID, xmlTosign, out xmlsigned, out msgerr);

                //Si está habilitado el TSA => Envio
                if (ierr == 0)
                {
                    if (Global.TSA_ENABLED)
                    {
                        resTS = Global.TSA_MANAGER.GenerateTS(Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(xmlsigned)), out sXMLTS);
                        if (resTS == 0)
                        {
                            sRDEnvelope = "<RDEnvelope>" + xmlsigned + sXMLTS + "</RDEnvelope>";
                            isSaveOK = true;
                        }
                        else
                        {
                            isSaveOK = false;
                            oXmlOut.Message = "Error generando TS";
                            LOG.Error(oXmlOut.Message);
                            oXmlOut.Receipt = null;
                            ret = Errors.IRET_ERR_TSA;
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        }
                    }
                    else
                    {
                        isSaveOK = true;
                        sRDEnvelope = "<RDEnvelope>" + xmlsigned + "</RDEnvelope>";
                    }

                    if (isSaveOK)
                    {
                        try
                        {
                            RdRecibos ord = new RdRecibos();
                            ord.Reciboid = reciboID;
                            ord.Descripcion = oXmlIn.Description;
                            ord.Fecha = dNow;
                            ord.Iporigen = HttpContext.Current.Request.UserHostAddress;
                            ord.RdAplicaciones = AdministradorRdAplicaciones.BuscarRdAplicacionesById(oXmlIn.ApplicationId);
                            ord.Reciboxml = sRDEnvelope; // xmlsigned;
                            ord.Version = "1.0";
                            AdministradorRdRecibos.Save(ord);
                            //PersistentManager.session().Save(ord);
                            //PersistentManager.session().Flush();
                            CopyParams(oXmlIn, out oXmlOut);
                            oXmlOut.Receipt = sRDEnvelope; //xmlsigned;
                            oXmlOut.Trackid = reciboID;
                            oXmlOut.Timestampend = DateTime.Now;
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        }
                        catch (Exception exDB)
                        {
                            LOG.Error("Error salvando en BD [" + exDB.Message + "]");
                            oXmlOut.Message = "Error salvando en BD [" + exDB.Message + "]";
                            oXmlOut.Receipt = null;
                            ret = Errors.IRET_ERR_SAVE_DB;
                            xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        }
                    }
                }
                else
                {
                    oXmlOut.Message = "Error Firmando [" + msgerr + "]";
                    oXmlOut.Receipt = null;
                    ret = Errors.IRET_ERR_SIGNATURE;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                }
            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                LOG.Error(oXmlOut.Message);
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }

            return ret;
        }

        /// <summary>
        /// Graba en directorio el video o imagen del primer parametro en B64 
        /// segun sea 1 (video) o 2 (image) el parametro 2
        /// </summary>
        /// <param name="name"></param>
        /// <param name="strVideoOrImage"></param>
        /// <param name="type"></param>
        internal static void SaveDebugData(string name, string strVideoOrImage, int type)
        {
            try
            {
                string semilla = DateTime.Now.ToString("yyyyMMddHHmmss") + "_";
                if (string.IsNullOrEmpty(strVideoOrImage))
                {
                    LOG.Debug("FEMServiceManager.SaveDebugData strVideoOrImage = null and type = " + type);
                } else
                {
                    if (type == 3) //Es para grabar XmlParamIn y Out plano en txt
                    {
                        System.IO.File.WriteAllText(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + semilla + name + ".xml", strVideoOrImage);
                    }
                    else
                    {
                        byte[] byVideoOrImage = Convert.FromBase64String(strVideoOrImage);
                        switch (type)
                        {
                            case 1: //es video
                                System.IO.File.WriteAllBytes(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + semilla + name + ".mp4", byVideoOrImage);
                                break;
                            case 2: //es imagen
                                System.IO.File.WriteAllBytes(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + semilla + name + ".jpg", byVideoOrImage);
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("FEMServiceManager.SaveDebugData Error - " + ex.Message);
            }
        }

        private static string GetQR(string urlvideoinstorage)
        {
            string qr = "";
            try
            {
                Bio.Core.QR.FormQR formqr = new Bio.Core.QR.FormQR();
                qr = formqr.GetImgQRB64(urlvideoinstorage);
            }
            catch (Exception ex)
            {
                qr = null;
                LOG.Error("FEMServiceManager.GetQR Error - " + ex.Message);
            }
            return qr;
        }

        private static string GetValueFromExtension(Api.RdExtensionItem[] extensiones, string key)
        {
            string ret = null;
            bool bFound = false;
            try
            {
                foreach (Api.RdExtensionItem item in extensiones)
                {
                    if (item.key.Equals(key)) {
                        ret = (string)item.value;
                        LOG.Error("FEMServiceManager.GetValueFromExtension Found Key = " + key);
                        bFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                bFound = false;
                LOG.Error("FEMServiceManager.GetValueFromExtension Error - " + ex.Message);
            }
            if (!bFound) LOG.Error("FEMServiceManager.GetValueFromExtension NOT Found Key = " + key);
            return ret;
        }

        private static void CopyParams(XmlParamIn oXmlIn, out XmlParamOut oXmlOut)
        {
            oXmlOut = new XmlParamOut();
            try
            {
                oXmlOut.Actionid = oXmlIn.Actionid;
                oXmlOut.Clientid = oXmlIn.Clientid;
                oXmlOut.Companyid = oXmlIn.Companyid;
                oXmlOut.Enduser = oXmlIn.Enduser;
                oXmlOut.Ipenduser = oXmlIn.Ipenduser;
                oXmlOut.Origin = oXmlIn.Origin;
                //oXmlOut. = oXmlIn;

            }
            catch (Exception ex)
            {
                LOG.Error("ServiceManager.CopyParams Error = " + ex.Message);
            }
        }


        private static int GetFEM(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /*  NOTA: Los parametros luego en version final hay que cambiarlo, para que sea dinamico
             *      y se controlen con las extensiones definidas.
             *  1.- Serializo param
             *  2.- Chequeo que exista en BD sino => Retorno que no existe 
             *  3.- Si existe => Check Firma
             *  4.- Retorno
            */
            int ret = Errors.IRET_OK;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;
            try
            {

                //*  2.- Genero RD xml y firmo 
                DateTime dNow = DateTime.Now;
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                // AGREGAR CONTROL DE LOGIN
                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string app;
                int qext;
                int retaux = CheckAppAndCompanyEnabled(oXmlIn.ApplicationId, out qext, out app, out msg);
                if (retaux < 0)
                {
                    oXmlOut.Message = msg;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }

                //*  2.- Check en BD 
                CopyParams(oXmlIn, out oXmlOut);
                RdRecibos dbRecibo = AdministradorRdRecibos.BuscarRdReciboByReciboId(oXmlIn.ReceiptId);

                if (dbRecibo == null)
                {
                    oXmlOut.Trackid = oXmlIn.ReceiptId;
                    oXmlOut.Message = "El recibo  con ID = " + oXmlIn.ReceiptId
                                      + " no se encuentra en el repositorio seguro";
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    ret = Errors.IRET_ERR_RECEIPT_NO_IN_DB;
                }
                else
                {
                    oXmlOut.Trackid = oXmlIn.ReceiptId;
                    oXmlOut.Message = "El recibo  con ID = " + oXmlIn.ReceiptId
                                        + " existe";
                    oXmlOut.Receipt = dbRecibo.Reciboxml;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    ret = Errors.IRET_OK;
                }
            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }

            return ret;
        }



        /// <summary>
        /// Verifica que vengan los parámetros necesarios para cada accion
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static bool IsCorrectParamInForAction(XmlParamIn oXmlIn, out string msgerr)
        {
            msgerr = null;
            bool ret = false;
            try
            {

                //Generate
                if ((oXmlIn.Actionid == 1 || oXmlIn.Actionid == 4) && (oXmlIn.Extensiones == null || oXmlIn.Extensiones.Length == 0 || oXmlIn.ApplicationId == 0))
                {
                    msgerr = "Extensiones vacias y/o Id Aplicaicon = 0. Imposible Certificar";
                    ret = false;
                }

                //Verify
                if (oXmlIn.Actionid == 2 && (String.IsNullOrEmpty(oXmlIn.Receipt)))
                {
                    msgerr = "FEM nulo no se puede verificar.";
                    ret = false;
                }

                //Get
                if (oXmlIn.Actionid == 3 && (String.IsNullOrEmpty(oXmlIn.ReceiptId) || oXmlIn.ApplicationId == 0))
                {
                    msgerr = "ReceiptId nulo o ApplicationId = 0, no se puede recuperar FEM";
                    ret = false;
                }

                ret = true;
            }
            catch (Exception ex)
            {
                LOG.Error("FEMServiceManager.IsCorrectParamInForAction Error - " + ex.Message);
                ret = false;
            }
            return ret;
        }


        /// <summary>
        /// Chequea que la aplicaicon y la compañía estén habilitadas
        /// </summary>
        /// <param name="idaplicacion"></param>
        /// <param name="qextensiones"></param>
        /// <param name="app"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        private static int CheckAppAndCompanyEnabled(int idaplicacion, out int qextensiones, out string app, out string msg)
        {
            int ret = 0;
            msg = null;
            app = null;
            qextensiones = 0;
            try
            {
                RdAplicaciones appObj = AdministradorRdAplicaciones.BuscarRdAplicacionesById(idaplicacion);

                if (appObj == null)
                {
                    msg = "Aplicacion No Existe";
                    return Errors.IRET_ERR_APLICATION_NOT_EXIST;
                }
                else
                {
                    app = appObj.Nombre;

                    if (appObj.Deleted.HasValue)
                    {
                        msg = "Aplicación [" + app + "] Deshabilitada desde " + appObj.Deleted.Value.ToString("dd/MM/yyyy");
                        return Errors.IRET_ERR_APLICATION_DISABLED;
                    }
                    qextensiones = appObj.QExtensiones;
                    Company company = AdministradorCompany.BuscarCompanyById(appObj.Company);
                    if (company == null)
                    {
                        msg = "Company No Existe";
                        return Errors.IRET_ERR_COMPANY_NOT_EXIST;
                    }
                    else if (company.EndDate.HasValue && company.EndDate < DateTime.Now)
                    {
                        msg = "Company deshabilitada desde " + company.EndDate.Value.ToString("dd/MM/yyyy");
                        return Errors.IRET_ERR_COMPANY_DISABLED;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Error CheckCompanyEnabled", ex);
            }
            return ret;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="xmlreceipt"></param>
        ///// <param name="verifytype">1 - Solo existnecia en BD |
        /////                                2 - Existencia en BD y Check Igual contenido 
        ///// </param>
        ///// <returns></returns>
        public static int VerifyFEM(XmlParamIn oXmlIn, out string xmlparamout)
        {
            /*  NOTA: Los parametros luego en version final hay que cambiarlo, para que sea dinamico
             *      y se controlen con las extensiones definidas.
             *  1.- Serializo param
             *  2.- Chequeo que exista en BD sino => Retorno que no existe 
             *  3.- Si existe => Check Firma
             *  4.- Retorno
            */
            int ret = Errors.IRET_OK;
            XmlParamOut oXmlOut = new XmlParamOut();
            xmlparamout = null;
            try
            {

                //*  2.- Genero RD xml y firmo 
                DateTime dNow = DateTime.Now;
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (String.IsNullOrEmpty(ip)) ip = oXmlIn.Ipenduser;
                if (ip == null || ip.Length < 9) ip = "127.0.0.1";

                // AGREGAR CONTROL DE LOGIN
                //3.- Chequeo que esté el id de aplicación, con etiquetas a cambiar
                //      Obtengo el id de compania para ver si existe, 
                //      chequeo user/pass 
                string msg;
                string strapp;
                //int retaux = CheckCompanyEnabled(oXmlIn.Companyid, out msg);
                //if (retaux < 0)
                //{
                //    oXmlOut.Message = msg;
                //    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                //}

                //*  2.- Check en BD 
                CopyParams(oXmlIn, out oXmlOut);
                string xmlreceipt = ExtractReceiptFromEnvelope(oXmlIn.Receipt);
                Rd orecibo = libs.XmlUtils.DeserializeObject<Rd>(xmlreceipt);
                RdRecibos dbRecibo = AdministradorRdRecibos.BuscarRdReciboByReciboId(orecibo.reciboID);

                if (dbRecibo == null)
                {
                    oXmlOut.Trackid = oXmlIn.ReceiptId;
                    oXmlOut.Message = "El recibo  con ID = " + orecibo.reciboID
                                      + " no se encuentra en el repositorio seguro";
                    /// 0 - Recibo OK | 1 - Recibo existe pero firma inválida | 2 - recibo no existe
                    oXmlOut.ResultVerify = 2;
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    ret = Errors.IRET_ERR_RECEIPT_NO_IN_DB;
                }
                else
                {
                    string msgerr = null;
                    int retV = XMLSignatureHelper.VerificarFirma(dbRecibo.RdAplicaciones.Id, xmlreceipt, out msgerr);
                    if (retV != 0)
                    {
                        oXmlOut.Trackid = oXmlIn.ReceiptId;
                        oXmlOut.Message = "El recibo  con ID = " + orecibo.reciboID
                                          + " tiene firma inválida";
                        /// 0 - Recibo OK | 1 - Recibo existe pero firma inválida | 2 - recibo no existe
                        oXmlOut.ResultVerify = 1;
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        ret = Errors.IRET_ERR_SIGNATURE_INVALID;
                        //return ret;
                    }
                    else
                    {
                        oXmlOut.Trackid = oXmlIn.ReceiptId;
                        oXmlOut.Message = "El recibo  con ID = " + orecibo.reciboID
                                          + " es VALIDO";
                        /// 0 - Recibo OK | 1 - Recibo existe pero firma inválida | 2 - recibo no existe
                        oXmlOut.ResultVerify = 0;
                        xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                        ret = Errors.IRET_OK;
                        //return ret;
                    }
                }
                
            }
            catch (Exception ex)
            {
                oXmlOut.Message = "Error Desconocido [" + ex.Message + "]";
                oXmlOut.Receipt = null;
                ret = Errors.IRET_ERR_DESCONOCIDO;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
            }

            return ret;
        }

        private static int CheckCompanyEnabled(int companyid, out string msg)
        {
            int ret = 0;
            msg = null;
            try
            {
                Company company = AdministradorCompany.BuscarCompanyById(companyid);
                if (company == null)
                {
                    msg = "Company No Existe";
                    return Errors.IRET_ERR_COMPANY_NOT_EXIST;
                }
                else if (company.EndDate.HasValue && company.EndDate < DateTime.Now)
                {
                    msg = "Company deshabilitada desde " + company.EndDate.Value.ToString("dd/MM/yyyy");
                    return Errors.IRET_ERR_COMPANY_DISABLED;
                }

            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("Error CheckCompanyEnabled", ex);
            }
            return ret;
        }

        private static string ExtractReceiptFromEnvelope(string xmlenvelope)
        {
            string sret = null;
            try
            {
                XmlDocument rdEnvelope = new XmlDocument();
                rdEnvelope.PreserveWhitespace = false;
                rdEnvelope.LoadXml(xmlenvelope);

                XmlNode node = rdEnvelope.GetElementsByTagName("Rd")[0];

                sret = node.OuterXml;
            }
            catch (Exception ex)
            {
                LOG.Error("Error ExtractReceiptFromEnvelope", ex);
            }
            return sret;
        }
    }
}