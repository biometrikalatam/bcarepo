using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Bio.Digital.Receipt.Core
{
    public partial class Rd
    {



#region I/O
        public static string SaveRDToFile(Rd obj, string path)
        {
            string sRes = null;

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Rd));
                TextWriter writer = new StreamWriter(path);
                serializer.Serialize(writer, obj);
                writer.Close();
            }
            catch (Exception ex)
            {
                sRes = "Error al salvar el recibo en " + path + " - [" + ex.Message + "]";
            }
            return sRes;
        }

        public static Rd LoadRDFromFile(string path)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Rd));
                TextReader reader = new StreamReader(path);
                Rd cf = (Rd)serializer.Deserialize(reader);
                reader.Close();
                return cf;
            }
            catch
            {
                return null;
            }
        }
#endregion I/O

    }
}
