/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Digital.Receipt.Core.Database
{
	/// <summary>
	/// RdAplicaciones object for NHibernate mapped table 'rd_aplicaciones'.
	/// </summary>
	[Serializable]
	public class RdAplicaciones : INotifyPropertyChanged
	{
		#region Member Variables
		protected int _id;
        protected string _nombre;
        protected string _code;
        protected int _checkingen;
        protected int _qextensiones;
        protected int _company;
        protected int _signaturetype;
        protected string _certificatepfx;
        protected string _signaturepsw;
        protected string _certificatecer;
        protected string _urlfea;
        protected string _appfea;
        protected string _userfea;
        protected DateTime? _deleted;
        protected string _urlcallback;

        protected IList<RdExtensiones> _rdextensiones;
		protected IList<RdRecibos> _rdrecibos;
		public virtual event PropertyChangedEventHandler PropertyChanged;
		
		#endregion
		#region Constructors
			
		public RdAplicaciones() {}
					
		public RdAplicaciones(string nombre, int checkingen, int company) 
		{
			this._nombre= nombre;
			this._checkingen= checkingen;
            this._company = company;
		}

        public RdAplicaciones(string nombre, int company) 
		{
			this._nombre= nombre;
            this._company = company;
		}

        public RdAplicaciones(string nombre, int checkingen, int qextensiones, int company, int signaturetype, string certificatepfx, string signaturepsw,
                              string certificatecer, string urlfea, string appfea, string userfea)
        {
            this._nombre = nombre;
            this._checkingen = checkingen;
            this._qextensiones = qextensiones;
            this._company = company;
            this._signaturetype = signaturetype;
            this._certificatepfx = certificatepfx;
            this._signaturepsw = signaturepsw;
            this._certificatecer = certificatecer;
            this._urlfea = urlfea;
            this._appfea = appfea;
            this._userfea = userfea;
        }



        #endregion
        #region Public Properties
        public virtual int Id
		{
			get { return _id; }
			set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
		}
		public  virtual string Nombre
		{
			get { return _nombre; }
			set {
				if ( value != null && value.Length > 50)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Nombre cannot contain more than 50 characters");
				if (value != this._nombre){_nombre= value;NotifyPropertyChanged("Nombre");}}
		}
        public virtual string Code
        {
            get { return _code; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Code cannot contain more than 50 characters");
                if (value != this._code) { _code = value; NotifyPropertyChanged("Code"); }
            }
        }
        public virtual int Checkingen
        {
            get { return _checkingen; }
            set { if (value != this._checkingen) { _checkingen = value; NotifyPropertyChanged("Checkingen"); } }
        }
        public virtual int QExtensiones
        {
            get { return _qextensiones; }
            set { if (value != this._qextensiones) { _qextensiones = value; NotifyPropertyChanged("QExtensiones"); } }
        }
        public virtual int Company
        {
            get { return _company; }
            set { if (value != this._company) { _company = value; NotifyPropertyChanged("Company"); } }
        }

        public virtual int SignatureType
        {
            get { return _signaturetype; }
            set { if (value != this._signaturetype) { _signaturetype = value; NotifyPropertyChanged("SignatureType"); } }
        }
        public virtual string CertificatePFX
        {
            get { return _certificatepfx; }
            set
            {
               if (value != this._certificatepfx) { _certificatepfx = value; NotifyPropertyChanged("CertificatePFX"); }
            }
        }

        public virtual string SignaturePsw
        {
            get { return _signaturepsw; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "SignaturePsw cannot contain more than 0 characters");
                if (value != this._signaturepsw) { _signaturepsw = value; NotifyPropertyChanged("SignaturePsw"); }
            }
        }
        public virtual string CertificateCER
        {
            get { return _certificatecer; }
            set
            {
                if (value != this._certificatecer) { _certificatecer = value; NotifyPropertyChanged("CertificateCER"); }
            }
        }
        public virtual string URLFEA
        {
            get { return _urlfea; }
            set
            {
                if (value != null && value.Length > 150)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "URLFEA cannot contain more than 0 characters");
                if (value != this._urlfea) { _urlfea = value; NotifyPropertyChanged("URLFEA"); }
            }
        }

        public virtual string APPFEA
        {
            get { return _appfea; }
            set
            {
                if (value != null && value.Length > 150)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "APPFEA cannot contain more than 0 characters");
                if (value != this._appfea) { _appfea = value; NotifyPropertyChanged("APPFEA"); }
            }
        }

        public virtual string USERFEA
        {
            get { return _userfea; }
            set
            {
                if (value != null && value.Length > 150)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "USERFEA cannot contain more than 0 characters");
                if (value != this._userfea) { _userfea = value; NotifyPropertyChanged("USERFEA"); }
            }
        }
        public virtual DateTime? Deleted
        {
            get { return _deleted; }
            set { if (value != this._deleted) { _deleted = value; NotifyPropertyChanged("Deleted"); } }
        }

        public virtual string UrlCallback
        {
            get { return _urlcallback; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "UrlCallback cannot contain more than 0 characters");
                if (value != this._urlcallback) { _urlcallback = value; NotifyPropertyChanged("UrlCallback"); }
            }
        }

        public virtual IList<RdExtensiones> RdExtensiones
		{
			get { return _rdextensiones; }
			set {_rdextensiones= value; }
		}
		public  virtual IList<RdRecibos> RdRecibos
		{
			get { return _rdrecibos; }
			set {_rdrecibos= value; }
		}
		protected virtual void NotifyPropertyChanged(String info)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(info));
			}
		}
		
		#endregion
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			RdAplicaciones castObj = (RdAplicaciones)obj;
			return ( castObj != null ) &&
			this._id == castObj.Id;
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57;
			hash = 27 * hash * _id.GetHashCode();
			return hash;
		}
		#endregion
		
	}
}
