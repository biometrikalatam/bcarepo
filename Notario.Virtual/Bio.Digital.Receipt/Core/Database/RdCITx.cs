﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.Core.Database
{

    /// <summary>
    /// 
    /// </summary>
    public class RdCITx
    {
        /// <summary>
        ///     
        /// </summary>
        public RdCITx() { }

        private int id;
        private string trackId;
        private int receiptId;
        private int type; //0-CI | 1-Document TTag u otros
        private int actionId;
        private string trackIdBP;
        private string trackIdDE;
        private DateTime dateGen;
        private DateTime lastModify;
        private int validityType;
        private int validityDays;
        private int resultCode;
        private string certifyPDF;
        private string qRGenerated;
        private int qRValidity;
        private int companyId;
        private string destinatarymail;
        private string typeId;
        private string valueId;
        private string name;
        private string phaterlastname;
        private string motherlastname;
        private string idcardimagefront;
        private string idcardimageback;
        private string idcardphotoimage;
        private string idcardsignatureimage;
        private string selfie;
        private string fingersample;
        private string fingersamplejpg;
        private string pdf417;
        private string workstationid;
        private string georef;
        private string sex;
        private string birthdate;
        private string issuedate;
        private string exprationdate;
        private string serial;
        private string nacionality;
        private string score;
        private string threshold;
        private string map;
        private string urlbpweb;
        private string listmailsdistribution;
        private string cellphone;
        private string manualsignatureimage;
        private string carregisterimagefront;
        private string carregisterimageback;
        private string writingimage;
        private string form;
        private string securitycode;
        private string taxidcompany;
        private string dynamicparam;
        private string videourl;
        private string username;

        public virtual int Id { get => id; set => id = value; }
        public virtual string TrackId { get => trackId; set => trackId = value; }
        public virtual int ReceiptId { get => receiptId; set => receiptId = value; }
        public virtual int Type { get => type; set => type = value; }
        public virtual int ActionId { get => actionId; set => actionId = value; }
        public virtual string TrackIdBP { get => trackIdBP; set => trackIdBP = value; }
        public virtual string TrackIdDE { get => trackIdDE; set => trackIdDE = value; }
        public virtual DateTime DateGen { get => dateGen; set => dateGen = value; }
        public virtual DateTime LastModify { get => lastModify; set => lastModify = value; }
        public virtual int ValidityType { get => validityType; set => validityType = value; }
        public virtual int ValidityDays { get => validityDays; set => validityDays = value; }
        public virtual int ResultCode { get => resultCode; set => resultCode = value; }
        public virtual string CertifyPDF { get => certifyPDF; set => certifyPDF = value; }
        public virtual string QRGenerated { get => qRGenerated; set => qRGenerated = value; }
        public virtual int QRValidity { get => qRValidity; set => qRValidity = value; }
        public virtual int CompanyId { get => companyId; set => companyId = value; }
        public virtual string DestinataryMail { get => destinatarymail; set => destinatarymail = value; }
        public virtual string TypeId { get => typeId; set => typeId = value; }
        public virtual string ValueId { get => valueId; set => valueId = value; }
        public virtual string Name { get => name; set => name = value; }
        public virtual string PhaterLastName { get => phaterlastname; set => phaterlastname = value; }
        public virtual string MotherLastName { get => motherlastname; set => motherlastname = value; }
        public virtual string IdCardImageFront { get => this.idcardimagefront; set => this.idcardimagefront = value; }
        public virtual string IdCardImageBack { get => this.idcardimageback; set => this.idcardimageback = value; }
        public virtual string IdCardPhotoImage { get => this.idcardphotoimage; set => this.idcardphotoimage = value; }
        public virtual string IdCardSignatureImage { get => this.idcardsignatureimage; set => this.idcardsignatureimage = value; }
        public virtual string Selfie { get => this.selfie; set => this.selfie = value; }
        public virtual string FingerSample { get => this.fingersample; set => this.fingersample = value; }
        public virtual string FingerSampleJpg { get => this.fingersamplejpg; set => this.fingersamplejpg = value; }
        public virtual string Pdf417 { get => this.pdf417; set => this.pdf417 = value; }
        public virtual string WorkstationId { get => this.workstationid; set => this.workstationid = value; }
        public virtual string GeoRef { get => this.georef; set => this.georef = value; }
        public virtual string Sex { get => this.sex; set => this.sex = value; }
        public virtual string BirthDate { get => this.birthdate; set => this.birthdate = value; }
        public virtual string IssueDate { get => this.issuedate; set => this.issuedate = value; }
        public virtual string ExprationDate { get => this.exprationdate; set => this.exprationdate = value; }
        public virtual string Serial { get => this.serial; set => this.serial = value; }
        public virtual string Nacionality { get => this.nacionality; set => this.nacionality = value; }
        public virtual string Score { get => this.score; set => this.score = value; }
        public virtual string Threshold { get => this.threshold; set => this.threshold = value; }
        public virtual string Map { get => this.map; set => this.map = value; }
        public virtual string UrlBPWeb { get => this.urlbpweb; set => this.urlbpweb = value; }
        public virtual string ListMailsDistribution { get => this.listmailsdistribution; set => this.listmailsdistribution = value; }
        public virtual string CellPhone { get => this.cellphone; set => this.cellphone = value; }
        public virtual string ManualSignatureImage { get => this.manualsignatureimage; set => this.manualsignatureimage = value; }

        public virtual string CarRegisterImageFront { get => this.carregisterimagefront; set => this.carregisterimagefront = value; }
        public virtual string CarRegisterImageBack { get => this.carregisterimageback; set => this.carregisterimageback = value; }
        public virtual string WritingImage { get => this.writingimage; set => this.writingimage = value; }
        public virtual string Form { get => this.form; set => this.form = value; }
        public virtual string SecurityCode { get => this.securitycode; set => this.securitycode = value; }
        public virtual string TaxIdCompany { get => this.taxidcompany; set => this.taxidcompany = value; }
        public virtual string DynamicParam { get => this.dynamicparam; set => this.dynamicparam = value; }
        public virtual string VideoURL { get => this.videourl; set => this.videourl = value; }
        public virtual string UserName { get => this.username; set => this.username = value; }

    }
}