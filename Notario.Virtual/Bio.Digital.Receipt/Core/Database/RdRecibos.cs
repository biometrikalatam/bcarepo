/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Digital.Receipt.Core.Database
{
	/// <summary>
	/// RdRecibos object for NHibernate mapped table 'rd_recibos'.
	/// </summary>
	[Serializable]
	public class RdRecibos : INotifyPropertyChanged
	{
		#region Member Variables
		protected int _id;
		protected string _reciboid;
		protected DateTime _fecha;
		protected RdAplicaciones _rdaplicaciones;
		protected string _reciboxml;
		protected string _version;
		protected string _descripcion;
		protected string _iporigen;
		public virtual event PropertyChangedEventHandler PropertyChanged;
		
		#endregion
		#region Constructors
			
		public RdRecibos() {}
					
		public RdRecibos(string reciboid, DateTime fecha, RdAplicaciones rdaplicaciones, string reciboxml, string version, string descripcion, string iporigen) 
		{
			this._reciboid= reciboid;
			this._fecha= fecha;
			this._rdaplicaciones= rdaplicaciones;
			this._reciboxml= reciboxml;
			this._version= version;
			this._descripcion= descripcion;
			this._iporigen= iporigen;
		}

		public RdRecibos(string reciboid, RdAplicaciones rdaplicaciones, string reciboxml)
		{
			this._reciboid= reciboid;
			this._rdaplicaciones= rdaplicaciones;
			this._reciboxml= reciboxml;
		}
		
		#endregion
		#region Public Properties
		public  virtual int Id
		{
			get { return _id; }
			set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
		}
		public  virtual string Reciboid
		{
			get { return _reciboid; }
			set {
				if ( value != null && value.Length > 50)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Reciboid cannot contain more than 50 characters");
				if (value != this._reciboid){_reciboid= value;NotifyPropertyChanged("Reciboid");}}
		}
		public  virtual DateTime Fecha
		{
			get { return _fecha; }
			set {if (value != this._fecha){_fecha= value;NotifyPropertyChanged("Fecha");}}
		}
		public  virtual RdAplicaciones RdAplicaciones
		{
			get { return _rdaplicaciones; }
			set {_rdaplicaciones= value; }
		}
		public  virtual string Reciboxml
		{
			get { return _reciboxml; }
			set {
				if ( value != null && value.Length > 2147483647)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Reciboxml cannot contain more than 2147483647 characters");
				if (value != this._reciboxml){_reciboxml= value;NotifyPropertyChanged("Reciboxml");}}
		}
		public  virtual string Version
		{
			get { return _version; }
			set {
				if ( value != null && value.Length > 15)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Version cannot contain more than 15 characters");
				if (value != this._version){_version= value;NotifyPropertyChanged("Version");}}
		}
		public  virtual string Descripcion
		{
			get { return _descripcion; }
			set {
				if ( value != null && value.Length > 50)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Descripcion cannot contain more than 50 characters");
				if (value != this._descripcion){_descripcion= value;NotifyPropertyChanged("Descripcion");}}
		}
		public  virtual string Iporigen
		{
			get { return _iporigen; }
			set {
				if ( value != null && value.Length > 15)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Iporigen cannot contain more than 15 characters");
				if (value != this._iporigen){_iporigen= value;NotifyPropertyChanged("Iporigen");}}
		}
		protected virtual void NotifyPropertyChanged(String info)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(info));
			}
		}
		
		#endregion
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			RdRecibos castObj = (RdRecibos)obj;
			return ( castObj != null ) &&
			this._id == castObj.Id;
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57;
			hash = 27 * hash * _id.GetHashCode();
			return hash;
		}
		#endregion

        #region Operaciones Generales
	    
	    
        #endregion Operaciones Generales

    }
}
