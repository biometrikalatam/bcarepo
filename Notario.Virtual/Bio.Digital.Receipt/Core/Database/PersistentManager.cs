using System;
using System.Collections.Generic;
using System.Text;
using NHibernate;
using System.Web;
using NHibernate.Cfg;
using System.IO;

namespace Bio.Digital.Receipt.Core.Database
{
    public static class PersistentManager
    {
        private const string CurrentSessionKey = "nhibernate.current_session";
        private static readonly ISessionFactory sessionFactory;
        public static String PathConf;

        static PersistentManager()
        {
            string root = HttpContext.Current.Server.MapPath("."); 

            Configuration cfg = new Configuration();
            cfg.Configure(Path.Combine(  root , "Config","Bio.Digital.Receipt.cfg.xml"));
            sessionFactory = cfg.BuildSessionFactory();//new Configuration().Configure().BuildSessionFactory();
        }

        public static ISession session()
        {
            HttpContext context = HttpContext.Current;
            ISession currentSession = context.Items[CurrentSessionKey] as ISession;

            if (currentSession == null || !currentSession.IsOpen)
            {
                currentSession = sessionFactory.OpenSession();
                context.Items[CurrentSessionKey] = currentSession;
            }

            return currentSession;
        }

        public static void CloseSession()
        {
            HttpContext context = HttpContext.Current;
            ISession currentSession = context.Items[CurrentSessionKey] as ISession;

            if (currentSession == null)
            {
                // No current session
                return;
            }
            currentSession.Close();
            context.Items.Remove(CurrentSessionKey);
        }

        public static void CloseSessionFactory()
        {
            if (sessionFactory != null)
            {
                sessionFactory.Close();
            }
        }
    }

}

