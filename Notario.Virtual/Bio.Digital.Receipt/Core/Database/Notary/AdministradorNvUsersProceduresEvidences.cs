﻿using System;
using System.Collections.Generic;
using Bio.Digital.Receipt.Api;
using log4net;
using NHibernate;

namespace Bio.Digital.Receipt.Core.Database
{
    internal class AdministradorNvUsersProceduresEvidences
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdministradorNvUsersProceduresEvidences));

        internal static NvUsersProceduresEvidences GetNvUserProcedureEvidenceByTrackIdCI(string trackidci, bool withPhater = false)
        {
            NvUsersProceduresEvidences oRet = null;
            try
            {
                LOG.Debug("AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByTrackIdCI IN...");
                using (ISession session = PersistentManager.session())
                {
                    oRet = session.CreateCriteria(typeof(NvUsersProceduresEvidences))
                                     .Add(NHibernate.Criterion.Expression.Eq("TrackIdExt", trackidci))
                                     .UniqueResult<NvUsersProceduresEvidences>();
                    LOG.Debug("AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByTrackIdCI - Recuperada Evidencia Id = " +
                               (oRet==null?"NULL":oRet.Id.ToString()));

                    if (oRet != null && withPhater)
                    {
                        NHibernateUtil.Initialize(oRet.NvUsersProcedures);
                    }
                }
            }
            catch (Exception ex)
            {
                oRet = null;
                LOG.Error("AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByTrackIdCI Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByTrackIdCI OUT!");
            return oRet;
        }

        internal static int UpdateNvUserProcedureEvidence(NvUsersProceduresEvidences nvUPE)
        {
            int ret = Errors.IRET_OK;
            try
            {
                LOG.Debug("AdministradorNvUsersProceduresEvidences.UpdateNvUserProcedureEvidence IN...");
                using (ISession session = PersistentManager.session())
                {
                    session.SaveOrUpdate(nvUPE);
                    session.Flush();
                }
                ret = Errors.IRET_OK;
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProceduresEvidences.UpdateNvUserProcedureEvidence Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProceduresEvidences.UpdateNvUserProcedureEvidence OUT!");
            return ret;
        }

        internal static List<string> GetNvUserProcedureEvidenceByIdProcedure(int idProcedure)
        {
            List<string> listTrackIdCI = new List<string>();
            List<NvUsersProceduresEvidences> oRet = null;
            try
            {
                LOG.Debug("AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByIdProcedure IN...");
                NvUsersProcedures oNUP = AdministradorNvUsersProcedures.GetNvUsersProcedureById(idProcedure);
                if (oNUP != null)
                {
                    using (ISession session = PersistentManager.session())
                    {
                        oRet = (List<NvUsersProceduresEvidences>)session.CreateCriteria(typeof(NvUsersProceduresEvidences))
                                        .Add(NHibernate.Criterion.Expression.Eq("NvUsersProcedures", oNUP))
                                        .List<NvUsersProceduresEvidences>();
                        LOG.Debug("AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByIdProcedure - Recuperada Evidencia Q = " +
                                   (oRet == null ? "NULL" : oRet.Count.ToString()));
                    }
                    if (oRet != null)
                    {
                        foreach (NvUsersProceduresEvidences item in oRet)
                        {
                            //Si es de tipi CI y hay tRackid => lo agrego a la salida
                            if (item.Type == 1 && !string.IsNullOrEmpty(item.TrackIdExt))
                            {
                                LOG.Debug("AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByIdProcedure - Addeded TrackIdCI = " +
                                            item.TrackIdExt);
                                listTrackIdCI.Add(item.TrackIdExt);
                            }
                        }
                    }
                } else
                {
                    listTrackIdCI = null;
                    LOG.Warn("AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByIdProcedure Procedure con Id = " +
                                idProcedure + " no pudo ser recuperado! Sale con lista de traciIdCI en Nulo...");
                }
            }
            catch (Exception ex)
            {
                listTrackIdCI = null;
                LOG.Error("AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByIdProcedure Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByIdProcedure OUT!");
            return listTrackIdCI;
        }

        internal static List<NvUsersProceduresEvidences> GetListNvUserProcedureEvidenceByIdProcedure(int idProcedure)
        {
            List<NvUsersProceduresEvidences> oRet = null;
            try
            {
                LOG.Debug("AdministradorNvUsersProceduresEvidences.GetListNvUserProcedureEvidenceByIdProcedure IN...");
                NvUsersProcedures oNUP = AdministradorNvUsersProcedures.GetNvUsersProcedureById(idProcedure);
                if (oNUP != null)
                {
                    using (ISession session = PersistentManager.session())
                    {
                        oRet = (List<NvUsersProceduresEvidences>)session.CreateCriteria(typeof(NvUsersProceduresEvidences))
                                        .Add(NHibernate.Criterion.Expression.Eq("NvUsersProcedures", oNUP))
                                        .List<NvUsersProceduresEvidences>();
                        LOG.Debug("AdministradorNvUsersProceduresEvidences.GetListNvUserProcedureEvidenceByIdProcedure - Recuperada Evidencia Q = " +
                                   (oRet == null ? "NULL" : oRet.Count.ToString()));
                    }
                }
                else
                {
                    oRet = null;
                    LOG.Warn("AdministradorNvUsersProceduresEvidences.GetListNvUserProcedureEvidenceByIdProcedure Procedure con Id = " +
                                idProcedure + " no pudo ser recuperado! Sale con lista en Nulo...");
                }
            }
            catch (Exception ex)
            {
                oRet = null;
                LOG.Error("AdministradorNvUsersProceduresEvidences.GetListNvUserProcedureEvidenceByIdProcedure Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProceduresEvidences.GetListNvUserProcedureEvidenceByIdProcedure OUT!");
            return oRet;
        }
    }
}