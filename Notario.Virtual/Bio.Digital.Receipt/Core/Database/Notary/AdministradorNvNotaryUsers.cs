using System;
using System.Collections.Generic;
using Bio.Digital.Receipt.Api;
using log4net;
using NHibernate;
//using NHibernate.Expression;

namespace Bio.Digital.Receipt.Core.Database
{

    /// <summary>
    /// 
    /// </summary>
    public class AdministradorNvNotaryUsers
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorNvNotaryUsers));

        public static NvNotaryUsers BuscarNvNotaryUserByUserId(string userid)
        {
            NvNotaryUsers rec = null;

            try
            {
                log.Debug("AdministradorNvNotaryUser.BuscarNvNotaryUserByUserId In - UserId = " + userid);
                using (ISession session = PersistentManager.session())
                {
                    rec = session.CreateCriteria(typeof(NvNotaryUsers))
                                     .Add(NHibernate.Criterion.Expression.Eq("UserId", userid))
                                     .UniqueResult<NvNotaryUsers>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorNvNotaryUsers.BuscarNvNotaryUserByUserId", ex);
            }

            return rec;
        }

        public static NvNotaryUsers BuscarNvNotaryUserByRut(string rut)
        {
            NvNotaryUsers rec = null;

            try
            {
                log.Debug("AdministradorNvNotaryUser.BuscarNvNotaryUserByRut In - Rut = " + rut);
                using (ISession session = PersistentManager.session())
                {
                    rec = session.CreateCriteria(typeof(NvNotaryUsers))
                                     .Add(NHibernate.Criterion.Expression.Eq("Rut", rut))
                                     .UniqueResult<NvNotaryUsers>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorNvNotaryUsers.BuscarNvNotaryUserByRut", ex);
            }

            return rec;
        }

        public static NvNotaryUsers BuscarNvNotaryUserById(int id)
        {
            NvNotaryUsers rec = null;

            try
            {
                log.Debug("AdministradorNvNotaryUser.BuscarNvNotaryUserById In - Id = " + id.ToString());
                using (ISession session = PersistentManager.session())
                {
                    //Company company = AdministradorCompany.BuscarCompanyById(1);
                    //IList<Company> lUsers = session.CreateCriteria(typeof(Company)).List<Company>();
                    //IList<NvUsersProcedures> lup = session.CreateCriteria(typeof(NvUsersProcedures)).List<NvUsersProcedures>();
                    //IList<NvUsers> lUsers = session.CreateCriteria(typeof(NvUsers)).List<NvUsers>();

                    rec = session.CreateCriteria(typeof(NvNotaryUsers))
                                     .Add(NHibernate.Criterion.Expression.Eq("Id", id))
                                     .UniqueResult<NvNotaryUsers>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorNvNotaryUsers.BuscarNvNotaryUserById", ex);
            }

            return rec;
        }

        public static NvNotaryUsers BuscarNvNotaryUserById(int id, bool checkIfMailValidated)
        {
            NvNotaryUsers rec = null;

            try
            {
                log.Debug("AdministradorNvNotaryUsers.BuscarNvNotaryUserById IN - Id = " + id.ToString());
                using (ISession session = PersistentManager.session())
                {
                    //Company company = AdministradorCompany.BuscarCompanyById(1);
                    //IList<Company> lUsers = session.CreateCriteria(typeof(Company)).List<Company>();
                    //IList<NvUsersProcedures> lup = session.CreateCriteria(typeof(NvUsersProcedures)).List<NvUsersProcedures>();
                    //IList<NvUsers> lUsers = session.CreateCriteria(typeof(NvUsers)).List<NvUsers>();

                    rec = session.CreateCriteria(typeof(NvNotaryUsers))
                                     .Add(NHibernate.Criterion.Expression.Eq("Id", id))
                                     .UniqueResult<NvNotaryUsers>();
                }

                if (rec != null && checkIfMailValidated)
                {
                    log.Debug("AdministradorNvNotaryUsers.BuscarNvNotaryUserById - Check si esta bloqueado manual o por mail no validado...");
                    if (rec.IsBlocked == 1)
                    {
                        log.Debug("AdministradorNvNotaryUsers.BuscarNvNotaryUserById - User blocked!");
                        rec = null;
                    } else
                    {
                        log.Debug("AdministradorNvNotaryUsers.BuscarNvNotaryUserById - User OK!");
                    }
                } else
                {
                    log.Debug("AdministradorNvNotaryUsers.BuscarNvNotaryUserById - User null [id=" + id + "]");
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorNvNotaryUsers.BuscarNvNotaryUserById Error", ex);
            }
            log.Debug("AdministradorNvNotaryUsers.BuscarNvNotaryUserById OUT!");
            return rec;
        }

        internal static NvNotaryUsers CheckOrCreateNvUser(string mail, string rut)
        {

            NvNotaryUsers usercreated = null;
            try
            {
                log.Debug("AdministradorNvNotaryUsers.CheckOrCreateNvUser IN - mail = " + mail + " | rut = " + rut);
                usercreated = BuscarNvNotaryUserByMail(mail);

                //Si es nulo => no existe => lo creo con valores default cuando sea neceario
                if (usercreated == null)
                {
                    usercreated = new NvNotaryUsers();
                    log.Debug("AdministradorNvNotaryUsers.CheckOrCreateNvUser Creo nuevo NvUser...");
                    usercreated.DateCreate = DateTime.Now;
                    usercreated.Rut = string.IsNullOrEmpty(rut)?"99999999-9":rut;
                    usercreated.Mail = mail;
                    usercreated.Password = Helpers.CryptoHelper.Encrypt("passworddefault");
                    if (string.IsNullOrEmpty(usercreated.Password))
                    {
                        log.Warn("AdministradorNvNotaryUsers.CheckOrCreateNvUser - Error encriptando clave!");
                        return null;
                    }
                    usercreated.Name = "Anonimo";
                    usercreated.LastName = "Anonimo";
                    usercreated.LastName = "Anonimo";
                    usercreated.IsBlocked = 0;
                    usercreated.NotaryId = 1;
                    usercreated.Rol = 3; //Funcionario
                    //usercreated.DateBlocked = DateTime.Now;
                    //usercreated.CodeValidation = GenerateCode();
                    //usercreated.DateLastLogin = DateTime.Now;
                    using (ISession session = PersistentManager.session())
                    {
                        session.Save(usercreated);
                        session.Flush();
                    }
                    log.Debug("AdministradorNvNotaryUsers.CheckOrCreateNvUser Usuario registrado => Anonimo => " +
                               " Mail = " + usercreated.Mail);

                    //Envio mail en paralelo para la verificación del mail
                    //string[] mailsto = new string[1];
                    //mailsto[0] = usercreated.Mail;

                    ////string body = "<table border=\"0\" width=\"60%\" align=\"center\">" +
                    ////            "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                    ////            "<tr><td align=\"center\">" +
                    ////                "</td><image src=\"cid:HDIImage\" /></tr>" +
                    ////          "<tr><td><p style=\"font-family:'Arial'\">Estimad@, Notario Virtual le indica el código de confirmación " +
                    ////          "de su registro en el servicio: <b>" + usercreated.CodeValidation + "</b></p></td></tr>" +
                    ////          "<tr><td><p style=\"font-family:'Arial'\">Ingréselo en el sitio para completar el registro o presione " +
                    ////          "<a href=\"" + Properties.Settings.Default.URLBaseAPIRest + "MailValidation?mail=" +
                    ////              usercreated.Mail + "&code=" + usercreated.CodeValidation + "\">" +
                    ////              "<font style=\"color:ligth-blue; font-family:'Arial'\">aqui</font></a> para validarlo automáticamente.</p></td></tr>" +
                    ////              "<tr><td><p>&nbsp;</p></td></tr>" +
                    ////              "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
                    ////              "<a href=\"http://www.notariovirtual.cl\" target=\"_blank\">Notario Virtual<a>.</p></td></tr>" +
                    ////              "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                    ////              "</table>";
                    //string body = "<table border=\"0\" width=\"60%\" align=\"center\">" +
                    //           "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                    //           "<tr><td align=\"center\">" +
                    //               "</td></tr>" +
                    //         "<tr><td><p style=\"font-family:'Arial'\">Estimad@, Notario Virtual le indica el código de confirmación " +
                    //         "de su registro en el servicio: <b>" + usercreated.CodeValidation + "</b></p></td></tr>" +
                    //         "<tr><td><p style=\"font-family:'Arial'\">Ingréselo en el sitio para completar el registro o presione " +
                    //         "<a href=\"" + Properties.Settings.Default.URLBaseAPIRest + "api/MailValidationHtml?mail=" +
                    //         usercreated.Mail + "&code=" + usercreated.CodeValidation + "\">" +
                    //         "<font style=\"color:ligth-blue; font-family:'Arial'\">aqui</font></a> para validarlo automáticamente.</p></td></tr>" +
                    //         "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
                    //         "<a href=\"http://www.notariovirtual.cl\" target=\"_blank\">Notario Virtual<a>.</p></td></tr>" +
                    //         "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                    //         "</table>";
                    //string body2 = @"<table border=""0"" width=""60%"" align=""center"">" +
                    //               @"<tr><td align=""center""><a href=""http://www.notariovirtual.cl"" target=""_blank"">" +
                    //               @"<img src=""cid:HDIImage"" /></a></td></tr></table>" + body;

                    //bool bret = libs.Utils.SendMailHtml(mailsto, "Notario Virtual Notify - Verificacion de mail",
                    //                                               body2, null, null, null);
                    //if (bret) log.Debug("AdministradorNvNotaryUsers.RegisterNvUser - Codigo: " + usercreated.CodeValidation + ", enviado a => " + usercreated.Mail);
                    //else log.Debug("AdministradorNvNotaryUsers.RegisterNvUser - Codigo NO enviado a => " + usercreated.Mail);
                }
            }
            catch (Exception ex)
            {
                usercreated = null;
                log.Error("AdministradorNvNotaryUsers.CheckOrCreateNvUser Error: " + ex.Message);
            }
            log.Debug("AdministradorNvNotaryUsers.CheckOrCreateNvUser Out!");
            return usercreated;
        }

        internal static int UpdateNvNotaryUser(NvNotaryUsers user, out string msgerr)
        {
            msgerr = null;
           
            int ret = 0;
            try
            {
                log.Debug("AdministradorNvNotaryUsers.UpdateNvNotaryUser IN...");

                if (user != null)
                {
                    using (ISession session = PersistentManager.session())
                    {
                        session.SaveOrUpdate(user);
                        session.Flush();
                    }
                    log.Debug("AdministradorNvNotaryUsers.UpdateNvNotaryUser User Modificado =>  RUT = " + user.Rut);
                }

                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Error: " + ex.Message;
                log.Error("AdministradorNvNotaryUsers.UpdateNvNotaryUser Error: " + ex.Message);
            }
            log.Debug("AdministradorNvNotaryUsers.UpdateNvNotaryUser Out!");
            return ret;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool IsUserEnabled(int id)
        {
            bool ret = false;

            try
            {
                NvNotaryUsers user = BuscarNvNotaryUserById(id);

                ret = (user != null && user.IsBlocked == 0);
                        //&& (user.DateValidation.HasValue && user.DateValidation.Value != null));
            }
            catch (Exception ex)
            {
                log.Error("AdministradorNvNotaryUsers.BuscarUserById", ex);
            }

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        public static NvNotaryUsers BuscarNvNotaryUserByMail(string mail)
        {
            NvNotaryUsers rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = session.CreateCriteria(typeof(NvNotaryUsers))
                                     .Add(NHibernate.Criterion.Expression.Eq("Mail", mail))
                                     .UniqueResult<NvNotaryUsers>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorNvNotaryUsers.BuscarNvNotaryUserByMail", ex);
            }

            return rec;
        }

       


        #region Register y Login



        /// <summary>
        /// Registra un nuevo user, lo graba en BD, genera codigo random, lo manda por mail, y espera 
        /// ingreso de ese codigo para validar registro final.
        /// Coloca en isBlocked en 1 hasta que se verifique. Fecha igaul al DateInit.
        /// //public virtual string Rut { get; set; }
        /// //public virtual string Mail { get; set; }
        /// //public virtual string Password { get; set; }
        /// //public virtual string Name { get; set; }
        /// //public virtual string PatherLastName { get; set; }
        /// //public virtual string MotherLastName { get; set; }
        /// //public virtual DateTime DateCreate { get; set; }
        /// //public virtual int IsBlocked { get; set; }
        /// //public virtual DateTime DateLastLogin { get; set; }
        /// //public virtual int IdCompany { get; set; }
        /// //public virtual DateTime? DateBlocked
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="mail"></param>
        /// <param name="password"></param>
        /// <param name="name"></param>
        /// <param name="patherlastname"></param>
        /// <param name="motherlastname"></param>
        /// <returns></returns>
        public static int RegisterNvUser(string rut, string mail, string password, string name, string patherlastname, 
                                         int rol, int notaryid, out string msgerr, out NvNotaryUsers usercreated)
        {
            msgerr = null;
            usercreated = new NvNotaryUsers(); ;
            int ret = 0;
            try
            {
                log.Debug("AdministradorNvNotaryUsers.RegisterNvUser IN...");
                usercreated.DateCreate = DateTime.Now;
                usercreated.Rut = rut;
                usercreated.Mail = mail;
                usercreated.Password = Helpers.CryptoHelper.Encrypt(password);
                if (string.IsNullOrEmpty(usercreated.Password))
                {
                    log.Warn("AdministradorNvNotaryUsers.RegisterNvUser - Error encriptando clave!");
                    return Errors.IRET_ERR_PROCESSING_PASSWORD;
                }
                usercreated.Name = name;
                usercreated.LastName = patherlastname;
                //usercreated.LastName = motherlastname;
                usercreated.IsBlocked = 0;
                usercreated.DateBlocked = null; //   DateTime.Now;
                usercreated.NotaryId = notaryid;
                usercreated.Rol = rol;
                //usercreated.CodeValidation = GenerateCode();
                //usercreated.DateLastLogin = DateTime.Now;
                using (ISession session = PersistentManager.session())
                {
                    session.Save(usercreated);
                    session.Flush();
                }
                //log.Debug("AdministradorNvNotaryUsers.RegisterNvUser Usuario registrado => Rut = " + usercreated.Rut +
                //           " - Mail = " + usercreated.Mail);
                //string[] mailsto = new string[1];
                //mailsto[0] = usercreated.Mail;
                //bool bret = libs.Utils.SendMail(mailsto, "Notario Virtual - Registro...", 
                //    "Estimad@, Notario Virtual le indica el código de confirmación de su registro en el servicio: " +
                //    usercreated.CodeValidation + " - Ingréselo en el sitio para completar el registro!", null, null,null);
                //if (bret) log.Debug("AdministradorNvNotaryUsers.RegisterNvUser - Codigo enviado a => " + usercreated.Mail);
                //else log.Debug("AdministradorNvNotaryUsers.RegisterNvUser - Codigo NO enviado a => " + usercreated.Mail);
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                log.Error("AdministradorNvNotaryUsers.RegisterNvUser Error: " + ex.Message);
            }
            log.Debug("AdministradorNvNotaryUsers.RegisterNvUser Out!");
            return ret;
        }

        private static string GenerateCode()
        {
            string sret = null;
            try
            {
                int longitud = 8;
                Guid miGuid = Guid.NewGuid();
                sret = miGuid.ToString().ToUpper().Replace("-", string.Empty).Substring(0, longitud);
                log.Debug("AdministradorNvUser.GenerateCode - Code Generado = " + sret);
            }
            catch (Exception ex)
            {
                sret = "AX8ERQWP";
                log.Error(" Error: " + ex.Message);
            }
            return sret;
        }

 
        /// <summary>
        /// Hace el login a la aplicación
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static int Login(string userin, string password, out string msgerr, out NvNotaryUsers user)
        {
            int ret = Errors.IRET_OK;
            msgerr = null;
            user = null;
            try
            {
                log.Debug("AdministradorNvNotaryUsers.Login IN...");

                user = BuscarNvNotaryUserByRut(userin);

                if (user == null)
                {
                    log.Debug("AdministradorNvNotaryUsers.Login Usuario No Existe => User = " + userin);
                    msgerr = "Usuario inexistente";
                    ret = Errors.IRET_ERR_USER_NOT_EXIST * -1;
                }
                else if (user.IsBlocked == 1)
                {
                    log.Debug("AdministradorNvNotaryUsers.Login Usuario Bloqueado => Rut = " + user.Rut +
                         " - Mail = " + user.Mail);
                    msgerr = "Usuario Bloqueado!";
                    ret = Errors.IRET_API_ERR_USER_BLOCKED * -1;
                }
                else
                {
                    if (user.Password.Equals(password)) //Helpers.CryptoHelper.Encrypt(password)))
                    {
                        //user.DateLastLogin = DateTime.Now;
                        //using (ISession session = PersistentManager.session())
                        //{
                        //    session.SaveOrUpdate(user);
                        //    session.Flush();
                        //}
                        log.Debug("AdministradorNvNotaryUsers.Login Usuario Validado => Rut = " + user.Rut +
                             " - Mail = " + user.Mail);
                        ret = Errors.IRET_OK;
                    }
                    else
                    {
                        log.Debug("AdministradorNvNotaryUsers.Login Login Clave No Coincide => Rut = " + user.Rut +
                             " - Mail = " + user.Mail);
                        ret = Errors.IRET_ERR_INVALID_PASSWORD;
                        msgerr = "Clave no coincide!";
                    }
                }
            }
            catch (Exception ex)
            {
                user = null;
                ret = Errors.IRET_ERR_DESCONOCIDO;
                msgerr = "Error: " + ex.Message;
                log.Error("AdministradorNvNotaryUsers.Login Error: " + ex.Message);
            }
            log.Debug("AdministradorNvNotaryUsers.Login Out!");
            return ret;
        }

        internal static int CheckStatusMailUser(int userId)
        {
            int ret = 0;
            NvNotaryUsers user = null;
            try
            {
                log.Debug("AdministradorNvNotaryUsers.CheckStatusMailUser IN...");

                user = BuscarNvNotaryUserById(userId);

                if (user != null)
                {
                    log.Debug("AdministradorNvNotaryUsers.CheckStatusMailUser Usuario Validado => Rut = " + user.Rut +
                         " - Mail = " + user.Mail);
                    ret = user.IsBlocked;
                }
                else
                {
                    ret = -1;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                log.Error("AdministradorNvNotaryUsers.CheckStatusMailUser Error: " + ex.Message);
            }
            log.Debug("AdministradorNvNotaryUsers.CheckStatusMailUser Out!");
            return ret;
        }

        internal static int ValidateRegisterNvUser(string mail, string code, out string msgerr)
        {
            msgerr = null;
            NvNotaryUsers user = new NvNotaryUsers(); ;
            int ret = 0;
            try
            {
                log.Debug("AdministradorNvNotaryUsers.ValidateRegisterNvUser IN...");

                user = BuscarNvNotaryUserByMail(mail);

                //if (user.CodeValidation.Equals(code))
                //{
                //    user.DateValidation = DateTime.Now;
                //    user.DateLastLogin = DateTime.Now;
                //    user.IsBlocked = 0;
                //    using (ISession session = PersistentManager.session())
                //    {
                //        session.SaveOrUpdate(user);
                //        session.Flush();
                //    }
                //}

                log.Debug("AdministradorNvNotaryUsers.ValidateRegisterNvUser Usuario Validado => Rut = " + user.Rut +
                           " - Mail = " + user.Mail);
               
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Error: " + ex.Message;
                log.Error("AdministradorNvNotaryUsers.ValidateRegisterNvUser Error: " + ex.Message);
            }
            log.Debug("AdministradorNvNotaryUsers.ValidateRegisterNvUser Out!");
            return ret;
        }

        internal static int ValidateMailNvUser(string mail, string code, out string msgerr)
        {
            msgerr = null;
            NvNotaryUsers user = new NvNotaryUsers(); ;
            int ret = 0;
            try
            {
                log.Debug("AdministradorNvNotaryUsers.ValidateMailNvUser IN...");

                user = BuscarNvNotaryUserByMail(mail);

                //if (user != null && user.IsBlocked == 1 && (!user.DateValidation.HasValue))
                //{
                //    if (user.CodeValidation.Equals(code))
                //    {
                //        user.DateValidation = DateTime.Now;
                //        user.DateLastLogin = DateTime.Now;
                //        user.IsBlocked = 0;
                //        using (ISession session = PersistentManager.session())
                //        {
                //            session.SaveOrUpdate(user);
                //            session.Flush();
                //        }
                //        //Informo x mail
                //        string[] strbody = new string[1];
                //        strbody[0] = "El mail <b>" + user.Mail + "</b> ha sido verificado con exito!";
                //        libs.Utils.NVNotify(user.Mail, "Verificacion de mail completada con exito", strbody, null, null, null);
                //    } else
                //    {
                //        ret = Errors.IRET_API_ERR_CODE;
                //    }
                //} else
                //{
                //    ret = Errors.IRET_ERR_MAIL_VALIDATED;
                //}

                log.Debug("AdministradorNvNotaryUsers.ValidateMailNvUser Mail Validado =>  Mail = " + user.Mail);

            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Error: " + ex.Message;
                log.Error("AdministradorNvNotaryUsers.ValidateMailNvUser Error: " + ex.Message);
            }
            log.Debug("AdministradorNvNotaryUsers.ValidateMailNvUser Out!");
            return ret;
        }

        internal static int DeleteNvUser(string mail, out string msgerr)
        {
            msgerr = null;
            NvNotaryUsers user = new NvNotaryUsers(); ;
            int ret = 0;
            try
            {
                log.Debug("AdministradorNvNotaryUsers.DeleteNvUser IN...");

                user = BuscarNvNotaryUserByMail(mail);

                if (user != null)
                {
                    using (ISession session = PersistentManager.session())
                    {
                        session.Delete(user);
                        session.Flush();
                    }
                }
                log.Debug("AdministradorNvNotaryUsers.DeleteNvUser User Eliminado =>  Mail = " + user.Mail);

                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Error: " + ex.Message;
                log.Error("AdministradorNvNotaryUsers.DeleteNvUser Error: " + ex.Message);
            }
            log.Debug("AdministradorNvNotaryUsers.DeleteNvUser Out!");
            return ret;
        }






        #endregion Register y Login

    }
}
