using System;
using System.Text;
using System.Collections.Generic;


namespace Bio.Digital.Receipt.Core.Database {

    [Serializable]
    public class NvNotaryUsers
    {
        public NvNotaryUsers() { }

        public virtual int Id { get; set; }
        public virtual string Rut { get; set; }
        public virtual string Mail { get; set; }
        public virtual string UserId { get; set; }
        public virtual string Password { get; set; }
        public virtual string Name { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime DateCreate { get; set; }
        public virtual int IsBlocked { get; set; }
        protected DateTime? _DateBlocked;
        public virtual int NotaryId { get; set; }
        /*
            0-superadmin (Creaci�n de nuevas notarias con administradores)
            1-admin (Creaci�n de usuarios y administraciones del dominio de la notaria)
            2-notario (Todo lo de funcionario y auditor adem�s de poder cerrar/completar el tr�mite) 
            3-funcionario (Permite hacer ciertas modificaciones, editar el tr�mite, completar la videollamada, etc)
            4-auditor (Solo lectura)
         */
        public virtual int Rol { get; set; }
        //public virtual DateTime DateLastLogin { get; set; }
        //protected DateTime? _DateValidation;


        public virtual DateTime? DateBlocked
        {
            get { return _DateBlocked; }
            set { if (value != this._DateBlocked) { _DateBlocked = value; } }
        }
        //public virtual string CodeValidation { get; set; }
        //public virtual DateTime? DateValidation
        //{
        //    get { return _DateValidation; }
        //    set { if (value != this._DateValidation) { _DateValidation = value; } }
        //}
        //public List<NvUsersProcedures> NvUsersProcedures { get; set; }
    }
}
