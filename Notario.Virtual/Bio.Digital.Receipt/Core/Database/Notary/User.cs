using System;
using System.Text;
using System.Collections.Generic;


namespace Bio.Digital.Receipt.Core.Database {
    
    public class User {
        public User() { }
        public virtual int Id { get; set; }
        //public virtual int CompanyUser { get; set; }
        public virtual string Username { get; set; }
        public virtual string Email { get; set; }
        public virtual string Password { get; set; }
        public virtual string PasswordSalt { get; set; }
        public virtual int PasswordFormat { get; set; }
        public virtual string PasswordQuestion { get; set; }
        public virtual string PasswordAnswer { get; set; }
        public virtual int FailedPasswordAttemptCount { get; set; }
        public virtual DateTime? FailedPasswordAttemptWindowStart { get; set; }
        public virtual int FailedPasswordAnswerAttemptCount { get; set; }
        public virtual DateTime? FailedPasswordAnswerAttemptWindowStart { get; set; }
        public virtual DateTime? LastPasswordChangedDate { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual DateTime? LastActivityDate { get; set; }
        public virtual bool IsApproved { get; set; }
        public virtual bool IsLockedOut { get; set; }
        public virtual DateTime? LastLockOutDate { get; set; }
        public virtual DateTime? LastLoginDate { get; set; }
        public virtual string Comments { get; set; }
        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        public virtual string Phone { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
        public virtual int? Employee { get; set; }

        //public List<NvUsersProcedures> NvUsersProcedures { get; set; }
    }
}
