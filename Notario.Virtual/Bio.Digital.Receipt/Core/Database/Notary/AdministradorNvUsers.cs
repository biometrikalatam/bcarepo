using System;
using System.Collections.Generic;
using Bio.Digital.Receipt.Api;
using log4net;
using NHibernate;
//using NHibernate.Expression;

namespace Bio.Digital.Receipt.Core.Database
{

    /// <summary>
    /// 
    /// </summary>
    public class AdministradorNvUsers
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorNvUsers));

        public static NvUsers BuscarNvUserByRut(string rut)
        {
            NvUsers rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = session.CreateCriteria(typeof(NvUsers))
                                     .Add(NHibernate.Criterion.Expression.Eq("Rut", rut))
                                     .UniqueResult<NvUsers>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorUsers.BuscarNvUserByRut", ex);
            }

            return rec;
        }

        public static NvUsers BuscarNvUserById(int id)
        {
            NvUsers rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    //Company company = AdministradorCompany.BuscarCompanyById(1);
                    //IList<Company> lUsers = session.CreateCriteria(typeof(Company)).List<Company>();
                    //IList<NvUsersProcedures> lup = session.CreateCriteria(typeof(NvUsersProcedures)).List<NvUsersProcedures>();
                    //IList<NvUsers> lUsers = session.CreateCriteria(typeof(NvUsers)).List<NvUsers>();

                    rec = session.CreateCriteria(typeof(NvUsers))
                                     .Add(NHibernate.Criterion.Expression.Eq("Id", id))
                                     .UniqueResult<NvUsers>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorUsers.BuscarNvUserById", ex);
            }

            return rec;
        }

        public static NvUsers BuscarNvUserById(int id, bool checkIfMailValidated)
        {
            NvUsers rec = null;

            try
            {
                log.Debug("AdministradorUsers.BuscarNvUserById IN...");
                using (ISession session = PersistentManager.session())
                {
                    //Company company = AdministradorCompany.BuscarCompanyById(1);
                    //IList<Company> lUsers = session.CreateCriteria(typeof(Company)).List<Company>();
                    //IList<NvUsersProcedures> lup = session.CreateCriteria(typeof(NvUsersProcedures)).List<NvUsersProcedures>();
                    //IList<NvUsers> lUsers = session.CreateCriteria(typeof(NvUsers)).List<NvUsers>();

                    rec = session.CreateCriteria(typeof(NvUsers))
                                     .Add(NHibernate.Criterion.Expression.Eq("Id", id))
                                     .UniqueResult<NvUsers>();
                }

                if (rec != null && checkIfMailValidated)
                {
                    log.Debug("AdministradorUsers.BuscarNvUserById - Check si esta bloqueado manual o por mail no validado...");
                    if (rec.IsBlocked == 1)
                    {
                        log.Debug("AdministradorUsers.BuscarNvUserById - User blocked!");
                        rec = null;
                    } else
                    {
                        log.Debug("AdministradorUsers.BuscarNvUserById - User OK!");
                    }
                } else
                {
                    log.Debug("AdministradorUsers.BuscarNvUserById - User null [id=" + id + "]");
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorUsers.BuscarNvUserById Error", ex);
            }
            log.Debug("AdministradorUsers.BuscarNvUserById OUT!");
            return rec;
        }

        internal static NvUsers CheckOrCreateNvUser(string mail, string rut)
        {
           
            NvUsers usercreated = null;
            try
            {
                log.Debug("AdministradorUsers.CheckOrCreateNvUser IN...");
                usercreated = BuscarNvUserByMail(mail);

                //Si es nulo => no existe => lo creo con valores default cuando sea neceario
                if (usercreated == null)
                {
                    usercreated = new NvUsers();
                    log.Debug("AdministradorUsers.CheckOrCreateNvUser Creo nuevo NvUser...");
                    usercreated.DateCreate = DateTime.Now;
                    usercreated.Rut = string.IsNullOrEmpty(rut)?"99999999-9":rut;
                    usercreated.Mail = mail;
                    usercreated.Password = Helpers.CryptoHelper.Encrypt("passworddefault");
                    if (string.IsNullOrEmpty(usercreated.Password))
                    {
                        log.Warn("AdministradorUsers.CheckOrCreateNvUser - Error encriptando clave!");
                        return null;
                    }
                    usercreated.Name = "Anonimo";
                    usercreated.PatherLastName = "Anonimo";
                    usercreated.MotherLastName = "Anonimo";
                    usercreated.IsBlocked = 1;
                    usercreated.IdCompany = 1;
                    //usercreated.DateBlocked = DateTime.Now;
                    usercreated.CodeValidation = GenerateCode();
                    usercreated.DateLastLogin = DateTime.Now;
                    using (ISession session = PersistentManager.session())
                    {
                        session.Save(usercreated);
                        session.Flush();
                    }
                    log.Debug("AdministradorUsers.CheckOrCreateNvUser Usuario registrado => Anonimo => " +
                               " Mail = " + usercreated.Mail);

                    //Envio mail en paralelo para la verificación del mail
                    string[] mailsto = new string[1];
                    mailsto[0] = usercreated.Mail;

                    //string body = "<table border=\"0\" width=\"60%\" align=\"center\">" +
                    //            "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                    //            "<tr><td align=\"center\">" +
                    //                "</td><image src=\"cid:HDIImage\" /></tr>" +
                    //          "<tr><td><p style=\"font-family:'Arial'\">Estimad@, Notario Virtual le indica el código de confirmación " +
                    //          "de su registro en el servicio: <b>" + usercreated.CodeValidation + "</b></p></td></tr>" +
                    //          "<tr><td><p style=\"font-family:'Arial'\">Ingréselo en el sitio para completar el registro o presione " +
                    //          "<a href=\"" + Properties.Settings.Default.URLBaseAPIRest + "MailValidation?mail=" +
                    //              usercreated.Mail + "&code=" + usercreated.CodeValidation + "\">" +
                    //              "<font style=\"color:ligth-blue; font-family:'Arial'\">aqui</font></a> para validarlo automáticamente.</p></td></tr>" +
                    //              "<tr><td><p>&nbsp;</p></td></tr>" +
                    //              "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
                    //              "<a href=\"http://www.notariovirtual.cl\" target=\"_blank\">Notario Virtual<a>.</p></td></tr>" +
                    //              "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                    //              "</table>";
                    string body = "<table border=\"0\" width=\"60%\" align=\"center\">" +
                               "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                               "<tr><td align=\"center\">" +
                                   "</td></tr>" +
                             "<tr><td><p style=\"font-family:'Arial'\">Estimad@, Notario Virtual le indica el código de confirmación " +
                             "de su registro en el servicio: <b>" + usercreated.CodeValidation + "</b></p></td></tr>" +
                             "<tr><td><p style=\"font-family:'Arial'\">Ingréselo en el sitio para completar el registro o presione " +
                             "<a href=\"" + Properties.Settings.Default.URLBaseAPIRest + "api/MailValidationHtml?mail=" +
                             usercreated.Mail + "&code=" + usercreated.CodeValidation + "\">" +
                             "<font style=\"color:ligth-blue; font-family:'Arial'\">aqui</font></a> para validarlo automáticamente.</p></td></tr>" +
                             "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
                             "<a href=\"http://www.notariovirtual.cl\" target=\"_blank\">Notario Virtual<a>.</p></td></tr>" +
                             "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                             "</table>";
                    string body2 = @"<table border=""0"" width=""60%"" align=""center"">" +
                                   @"<tr><td align=""center""><a href=""http://www.notariovirtual.cl"" target=""_blank"">" +
                                   @"<img src=""cid:HDIImage"" /></a></td></tr></table>" + body;

                    bool bret = libs.Utils.SendMailHtml(mailsto, "Notario Virtual Notify - Verificacion de mail",
                                                                   body2, null, null, null);
                    if (bret) log.Debug("AdministradorUsers.RegisterNvUser - Codigo: " + usercreated.CodeValidation + ", enviado a => " + usercreated.Mail);
                    else log.Debug("AdministradorUsers.RegisterNvUser - Codigo NO enviado a => " + usercreated.Mail);
                }
            }
            catch (Exception ex)
            {
                usercreated = null;
                log.Error("AdministradorUsers.CheckOrCreateNvUser Error: " + ex.Message);
            }
            log.Debug("AdministradorUsers.CheckOrCreateNvUser Out!");
            return usercreated;
        }

       


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool IsUserEnabled(int id)
        {
            bool ret = false;

            try
            {
                NvUsers user = BuscarNvUserById(id);

                ret = (user != null && user.IsBlocked==0 && 
                        (user.DateValidation.HasValue && user.DateValidation.Value != null));
            }
            catch (Exception ex)
            {
                log.Error("AdministradorUsers.BuscarUserById", ex);
            }

            return ret;
        }

        public static NvUsers BuscarNvUserByMail(string mail)
        {
            NvUsers rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = session.CreateCriteria(typeof(NvUsers))
                                     .Add(NHibernate.Criterion.Expression.Eq("Mail", mail))
                                     .UniqueResult<NvUsers>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorUsers.BuscarNvUserByMail", ex);
            }

            return rec;
        }

        #region Register y Login



        /// <summary>
        /// Registra un nuevo user, lo graba en BD, genera codigo random, lo manda por mail, y espera 
        /// ingreso de ese codigo para validar registro final.
        /// Coloca en isBlocked en 1 hasta que se verifique. Fecha igaul al DateInit.
        /// //public virtual string Rut { get; set; }
        /// //public virtual string Mail { get; set; }
        /// //public virtual string Password { get; set; }
        /// //public virtual string Name { get; set; }
        /// //public virtual string PatherLastName { get; set; }
        /// //public virtual string MotherLastName { get; set; }
        /// //public virtual DateTime DateCreate { get; set; }
        /// //public virtual int IsBlocked { get; set; }
        /// //public virtual DateTime DateLastLogin { get; set; }
        /// //public virtual int IdCompany { get; set; }
        /// //public virtual DateTime? DateBlocked
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="mail"></param>
        /// <param name="password"></param>
        /// <param name="name"></param>
        /// <param name="patherlastname"></param>
        /// <param name="motherlastname"></param>
        /// <returns></returns>
        public static int RegisterNvUser(string rut, string mail, string password, string name, string patherlastname, 
                                         string motherlastname, out string msgerr, out NvUsers usercreated)
        {
            msgerr = null;
            usercreated = new NvUsers(); ;
            int ret = 0;
            try
            {
                log.Debug("AdministradorUsers.RegisterNvUser IN...");
                usercreated.DateCreate = DateTime.Now;
                usercreated.Rut = rut;
                usercreated.Mail = mail;
                usercreated.Password = Helpers.CryptoHelper.Encrypt(password);
                if (string.IsNullOrEmpty(usercreated.Password))
                {
                    log.Warn("AdministradorUsers.RegisterNvUser - Error encriptando clave!");
                    return Errors.IRET_ERR_PROCESSING_PASSWORD;
                }
                usercreated.Name = name;
                usercreated.PatherLastName = patherlastname;
                usercreated.MotherLastName = motherlastname;
                usercreated.IsBlocked = 1;
                usercreated.DateBlocked = DateTime.Now;
                usercreated.CodeValidation = GenerateCode();
                usercreated.DateLastLogin = DateTime.Now;
                using (ISession session = PersistentManager.session())
                {
                    session.Save(usercreated);
                    session.Flush();
                }
                log.Debug("AdministradorUsers.RegisterNvUser Usuario registrado => Rut = " + usercreated.Rut +
                           " - Mail = " + usercreated.Mail);
                string[] mailsto = new string[1];
                mailsto[0] = usercreated.Mail;
                bool bret = libs.Utils.SendMail(mailsto, "Notario Virtual - Registro...", 
                    "Estimad@, Notario Virtual le indica el código de confirmación de su registro en el servicio: " +
                    usercreated.CodeValidation + " - Ingréselo en el sitio para completar el registro!", null, null,null);
                if (bret) log.Debug("AdministradorUsers.RegisterNvUser - Codigo enviado a => " + usercreated.Mail);
                else log.Debug("AdministradorUsers.RegisterNvUser - Codigo NO enviado a => " + usercreated.Mail);
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                log.Error("AdministradorUsers.RegisterNvUser Error: " + ex.Message);
            }
            log.Debug("AdministradorUsers.RegisterNvUser Out!");
            return ret;
        }

        private static string GenerateCode()
        {
            string sret = null;
            try
            {
                int longitud = 8;
                Guid miGuid = Guid.NewGuid();
                sret = miGuid.ToString().ToUpper().Replace("-", string.Empty).Substring(0, longitud);
                log.Debug("AdministradorNvUser.GenerateCode - Code Generado = " + sret);
            }
            catch (Exception ex)
            {
                sret = "AX8ERQWP";
                log.Error(" Error: " + ex.Message);
            }
            return sret;
        }

 
        /// <summary>
        /// Hace el login a la aplicación
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static int Login(string userid, string password, out string msgerr, out NvUsers user)
        {
            int ret = 0;
            msgerr = null;
            user = null;
            try
            {
                log.Debug("AdministradorNvUsers.Login IN...");

                user = BuscarNvUserByRut(userid);

                if (user == null)
                {
                    log.Debug("AdministradorNvUsers.Login Usuario No Existe => Rut = " + userid);
                    msgerr = "Usuario no existe!";
                    ret = Errors.IRET_API_ERR_USER_EXIST * -1;

                }
                else if (user.IsBlocked == 1)
                {
                    log.Debug("AdministradorNvUsers.Login Usuario Bloqueado => Rut = " + user.Rut +
                         " - Mail = " + user.Mail);
                    msgerr = "Usuario Bloqueado!";
                    ret = Errors.IRET_API_ERR_USER_BLOCKED * -1;
                }
                else if (user.Password.Equals(Helpers.CryptoHelper.Encrypt(password)))
                {
                    user.DateLastLogin = DateTime.Now;
                    using (ISession session = PersistentManager.session())
                    {
                        session.SaveOrUpdate(user);
                        session.Flush();
                    }
                    log.Debug("AdministradorNvUsers.Login Usuario Validado => Rut = " + user.Rut +
                         " - Mail = " + user.Mail);
                    ret = Errors.IRET_OK;
                }
                else
                {
                    ret = Errors.IRET_ERR_INVALID_PASSWORD;
                    msgerr = "Clave no coincide!";
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                msgerr = "Error: " + ex.Message;
                log.Error("AdministradorNvUsers.Login Error: " + ex.Message);
            }
            log.Debug("AdministradorNvUsers.Login Out!");
            return ret;
        }

        internal static int UpdateNvUser(NvUsers user, out string msgerr)
        {
            msgerr = null;

            int ret = 0;
            try
            {
                log.Debug("AdministradorNvUsers.UpdateNvUser IN...");

                if (user != null)
                {
                    using (ISession session = PersistentManager.session())
                    {
                        session.SaveOrUpdate(user);
                        session.Flush();
                    }
                    log.Debug("AdministradorNvUsers.UpdateNvUser User Modificado =>  RUT = " + user.Rut);
                }

                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "UpdateNvUser Error: " + ex.Message;
                log.Error("AdministradorNvUsers.UpdateNvUser Error: " + ex.Message);
            }
            log.Debug("AdministradorNvUsers.UpdateNvUser Out!");
            return ret;
        }

        internal static int CheckStatusMailUser(int userId)
        {
            int ret = 0;
            NvUsers user = null;
            try
            {
                log.Debug("AdministradorUsers.CheckStatusMailUser IN...");

                user = BuscarNvUserById(userId);

                if (user != null)
                {
                    log.Debug("AdministradorUsers.CheckStatusMailUser Usuario Validado => Rut = " + user.Rut +
                         " - Mail = " + user.Mail);
                    ret = user.IsBlocked;
                }
                else
                {
                    ret = -1;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                log.Error("AdministradorUsers.CheckStatusMailUser Error: " + ex.Message);
            }
            log.Debug("AdministradorUsers.CheckStatusMailUser Out!");
            return ret;
        }

        internal static int ValidateRegisterNvUser(string mail, string code, out string msgerr)
        {
            msgerr = null;
            NvUsers user = new NvUsers(); ;
            int ret = 0;
            try
            {
                log.Debug("AdministradorUsers.ValidateRegisterNvUser IN...");

                user = BuscarNvUserByMail(mail);

                if (user.CodeValidation.Equals(code))
                {
                    user.DateValidation = DateTime.Now;
                    user.DateLastLogin = DateTime.Now;
                    user.IsBlocked = 0;
                    using (ISession session = PersistentManager.session())
                    {
                        session.SaveOrUpdate(user);
                        session.Flush();
                    }
                }

                log.Debug("AdministradorUsers.ValidateRegisterNvUser Usuario Validado => Rut = " + user.Rut +
                           " - Mail = " + user.Mail);
               
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Error: " + ex.Message;
                log.Error("AdministradorUsers.ValidateRegisterNvUser Error: " + ex.Message);
            }
            log.Debug("AdministradorUsers.ValidateRegisterNvUser Out!");
            return ret;
        }

        internal static int ValidateMailNvUser(string mail, string code, out string msgerr)
        {
            msgerr = null;
            NvUsers user = new NvUsers(); ;
            int ret = 0;
            try
            {
                log.Debug("AdministradorUsers.ValidateMailNvUser IN...");

                user = BuscarNvUserByMail(mail);

                if (user != null && user.IsBlocked == 1 && (!user.DateValidation.HasValue))
                {
                    if (user.CodeValidation.Equals(code))
                    {
                        user.DateValidation = DateTime.Now;
                        user.DateLastLogin = DateTime.Now;
                        user.IsBlocked = 0;
                        using (ISession session = PersistentManager.session())
                        {
                            session.SaveOrUpdate(user);
                            session.Flush();
                        }
                        //Informo x mail
                        string[] strbody = new string[1];
                        strbody[0] = "El mail <b>" + user.Mail + "</b> ha sido verificado con exito!";
                        libs.Utils.NVNotify(user.Mail, "Verificacion de mail completada con exito", strbody, null, null, null);
                    } else
                    {
                        ret = Errors.IRET_API_ERR_CODE;
                    }
                } else
                {
                    ret = Errors.IRET_ERR_MAIL_VALIDATED;
                }

                log.Debug("AdministradorUsers.ValidateMailNvUser Mail Validado =>  Mail = " + user.Mail);

            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Error: " + ex.Message;
                log.Error("AdministradorUsers.ValidateMailNvUser Error: " + ex.Message);
            }
            log.Debug("AdministradorUsers.ValidateMailNvUser Out!");
            return ret;
        }

        internal static int DeleteNvUser(string mail, out string msgerr)
        {
            msgerr = null;
            NvUsers user = new NvUsers(); ;
            int ret = 0;
            try
            {
                log.Debug("AdministradorUsers.DeleteNvUser IN...");

                user = BuscarNvUserByMail(mail);

                if (user != null)
                {
                    using (ISession session = PersistentManager.session())
                    {
                        session.Delete(user);
                        session.Flush();
                    }
                }
                log.Debug("AdministradorUsers.DeleteNvUser User Eliminado =>  Mail = " + user.Mail);

                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "Error: " + ex.Message;
                log.Error("AdministradorUsers.DeleteNvUser Error: " + ex.Message);
            }
            log.Debug("AdministradorUsers.DeleteNvUser Out!");
            return ret;
        }








        #endregion Register y Login

    }
}
