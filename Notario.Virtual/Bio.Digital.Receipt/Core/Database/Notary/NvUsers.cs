using System;
using System.Text;
using System.Collections.Generic;


namespace Bio.Digital.Receipt.Core.Database {

    [Serializable]
    public class NvUsers {
        public NvUsers() { }

        protected DateTime? _DateBlocked;
        protected DateTime? _DateValidation;


        public virtual int Id { get; set; }
        /*
            0-superadmin (Creaci�n de nuevas empresas con administradores)
            1-admin (Creaci�n de usuarios y administraciones del dominio de la empresa)
            2-creador (Permite crear/iniciar un nuevo tr�mite que generar� un costo para la empresa, adem�s de su seguimiento como auditor)
            3-auditor (Solo lectura revisi�n y descargas)
        */
        public virtual int Rol { get; set; }
        //public virtual int NotaryId { get; set; }
        public virtual int CompanyId { get; set; }
        public virtual string Rut { get; set; }
        public virtual string Mail { get; set; }
        public virtual string Password { get; set; }
        public virtual string Name { get; set; }
        public virtual string PatherLastName { get; set; }
        public virtual string MotherLastName { get; set; }
        public virtual DateTime DateCreate { get; set; }
        
        public virtual int IsBlocked { get; set; }
        public virtual DateTime DateLastLogin { get; set; }
        public virtual int IdCompany { get; set; }

  
        public virtual DateTime? DateBlocked
        {
            get { return _DateBlocked; }
            set { if (value != this._DateBlocked) { _DateBlocked = value; } }
        }
        public virtual string CodeValidation { get; set; }
        public virtual DateTime? DateValidation
        {
            get { return _DateValidation; }
            set { if (value != this._DateValidation) { _DateValidation = value; } }
        }
        //public List<NvUsersProcedures> NvUsersProcedures { get; set; }
    }
}
