using System;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Bio.Digital.Receipt.Core.Database {

    public class NvUsersProceduresEvidences
    {
        public virtual int Id { get; set; }
        [JsonIgnore]
        public virtual NvUsersProcedures NvUsersProcedures { get; set; }
        public virtual int IdEvidence { get; set; }
        //public virtual NvProcedures NvProcedures { get; set; }
        //public virtual NvProceduresEvidences NvProceduresEvidences { get; set; }
        public virtual string Name { get; set; }

        // 1 - CI | 2 - Video | 3 - URLVideo | 4 - Image | 5 - PDF
        public virtual int Type { get; set; }
        public virtual string Value { get; set; }
        public virtual string TrackIdExt { get; set; }
        public virtual int Status { get; set; }
        public virtual string QRCI { get; set; }
        public virtual string URLCI { get; set; }
        

    }
}
