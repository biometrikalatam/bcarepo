using System;

namespace Bio.Digital.Receipt.Core.Database
{

    [Serializable]
    public class NvUsersProceduresSignatory
    {
        public NvUsersProceduresSignatory()
        {
           
        }

        public NvUsersProceduresSignatory(int _order, string _typeid, string _valueid, string _name, string _lastname,
                           string _mail, string _phone, string _taxidcompany, int _workflowid)
        {
            Signatoryorder = _order;
            Typeid = _typeid; 
            Valueid = _valueid;
            Name = _name;
            Lastname = _lastname;
            Mail = _mail;
            Phone = _phone;
            Workflowid = _workflowid;
            Taxidcompany = _taxidcompany;
        }

        public virtual int Id { get; set; }
        public virtual NvUsersProcedures NvUsersProcedures { get; set; }
        public virtual int Status { get; set; }  //0-Creado | 1-
        public virtual int Signatoryorder { get; set; }
        public virtual DateTime? Createdate { get; set; }
        public virtual DateTime? Expirationdatesignature { get; set; }
        public virtual DateTime? Lastmodify { get; set; }
        public virtual string Typeid { get; set; }
        public virtual string Valueid { get; set; }
        public virtual string Name { get; set; }
        public virtual string Lastname { get; set; }
        public virtual string Sex { get; set; }
        public virtual DateTime? Birthdate { get; set; }
        public virtual DateTime? Issuedate { get; set; }
        public virtual string Serial { get; set; }
        public virtual DateTime? Expirationdate { get; set; }
        public virtual string Nacionality { get; set; }
        public virtual string Mail { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Idcardimagefront { get; set; }
        public virtual string Idcardimageback { get; set; }
        public virtual string Idcardphotoimage { get; set; }
        public virtual string Idcardsignatureimage { get; set; }
        public virtual string Selfie { get; set; }
        public virtual string Fingersample { get; set; }
        public virtual string Fingersampleimage { get; set; }
        public virtual string Manualsignatureimage { get; set; }
        public virtual string Workstationid { get; set; }
        public virtual string Georef { get; set; }
        public virtual string Map { get; set; }
        public virtual string Videourl { get; set; }
        public virtual string Urlbpweb { get; set; }
        public virtual string Trackidci { get; set; }
        public virtual string Taxidcompany { get; set; }
        public virtual int Workflowid { get; set; }
        public virtual string Score { get; set; }
        public virtual string Threshold { get; set; }
        public virtual string Verifyresult { get; set; }
    }

}
