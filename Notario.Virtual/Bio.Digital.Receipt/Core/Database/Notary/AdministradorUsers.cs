using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
//using NHibernate.Expression;

namespace Bio.Digital.Receipt.Core.Database
{

    /// <summary>
    /// 
    /// </summary>
    public class AdministradorUsers
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorUsers));

        public static Company BuscarCompanyByRut(string rut)
        {
            Company rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session().CreateCriteria(typeof(Company))
                                     .Add(NHibernate.Criterion.Expression.Eq("Rut", rut))
                                     .UniqueResult<Company>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorUsers.BuscarCompanyByRut", ex);
            }

            return rec;
        }

        public static User BuscarUserById(int id)
        {
            User rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    //Company company = AdministradorCompany.BuscarCompanyById(1);
                    //IList<Company> lUsers = session.CreateCriteria(typeof(Company)).List<Company>();
                    //IList<User> lUsers = session.CreateCriteria(typeof(User)).List<User>();

                    rec = session.CreateCriteria(typeof(User))
                                     .Add(NHibernate.Criterion.Expression.Eq("Id", id))
                                     .UniqueResult<User>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorUsers.BuscarUserById", ex);
            }

            return rec;
        }

        public static bool IsUserEnabled(int id)
        {
            bool ret = false;

            try
            {
                User user = BuscarUserById(id);

                ret = (user != null && !user.IsLockedOut);
            }
            catch (Exception ex)
            {
                log.Error("AdministradorUsers.BuscarUserById", ex);
            }

            return ret;
        }

    }
}
