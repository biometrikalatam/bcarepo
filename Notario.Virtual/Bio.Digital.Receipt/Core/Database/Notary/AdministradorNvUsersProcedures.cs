using System;
using System.Collections;
using System.Collections.Generic;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database.Notary.config;
using Bio.Digital.Receipt.rest;
using Bio.Digital.Receipt.rest.Controllers;
using Bio.Digital.Receipt.rest.Models;
using Biometrika.Flow.API.Responses;
using log4net;
using NHibernate;
using NHibernate.Criterion;
//using NHibernate.Expression;

namespace Bio.Digital.Receipt.Core.Database
{

    /// <summary>
    /// 
    /// </summary>
    public class AdministradorNvUsersProcedures
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdministradorNvUsersProcedures));


        /// <summary>
        /// Genera un TrickId y revisa que no este duplicado
        /// </summary>
        /// <returns></returns>
        internal static string GetTrackIdUnique()
        {
            NvUsersProcedures nvUP;
            string retGuid = null;
            try
            {
                bool isOK = false;
                LOG.Debug("AdministradorNvUsersProcedures.GetTrackIdUnique IN...");
                while (!isOK)
                {
                    retGuid = Guid.NewGuid().ToString("N");
                    LOG.Debug("AdministradorNvUsersProcedures.GetTrackIdUnique - Chequeando TrackId = " + retGuid);
                    using (ISession session = PersistentManager.session())
                    {
                        nvUP = PersistentManager.session().CreateCriteria(typeof(NvUsersProcedures))
                                         .Add(NHibernate.Criterion.Expression.Eq("TrackId", retGuid))
                                         .UniqueResult<NvUsersProcedures>();
                    }
                    if (nvUP != null)
                    {
                        LOG.Debug("AdministradorNvUsersProcedures.GetTrackIdUnique - Existe GUID => Se genera una nueva...");
                        nvUP = null;
                    }
                    else
                    {
                        LOG.Debug("AdministradorNvUsersProcedures.GetTrackIdUnique GUID OK!");
                        isOK = true;
                    }
                }
            }
            catch (Exception ex)
            {
                retGuid = Guid.NewGuid().ToString("N"); ;
                LOG.Error("AdministradorNvUsersProcedures.GetTrackIdUnique Error", ex);
            }
            LOG.Debug("AdministradorNvUsersProcedures.GetTrackIdUnique OUT!");
            return retGuid;
        }

        internal static int CreateNvUserProcedure(string trackId, int userId, int notaryId, int procedureId, string document,
                                            List<rest.Models.Evidence> evidences, string payment, string message, out int providerout,
                                            out string msgerr, out NvUsersProcedures nvUsersProceduresCreated)
        {
            nvUsersProceduresCreated = null;
            int ret = Errors.IRET_OK;
            msgerr = null;
            providerout = 0;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.CreateNvUserProcedure IN...");
                nvUsersProceduresCreated = new NvUsersProcedures();
                nvUsersProceduresCreated.IdUser = userId;
                nvUsersProceduresCreated.IdNotary = notaryId;
                nvUsersProceduresCreated.IdNotaryProcedure = procedureId;
                nvUsersProceduresCreated.IdProcedure = procedureId;
                nvUsersProceduresCreated.NameProcedure = AdministradorNvConfig.GetNameProcedure(notaryId, procedureId);
                nvUsersProceduresCreated.IdProvider = AdministradorNvConfig.GetProvider(notaryId);
                nvUsersProceduresCreated.DocumentB64 = document;
                nvUsersProceduresCreated.DocumentB64Original = document;
                providerout = nvUsersProceduresCreated.IdProvider;
                nvUsersProceduresCreated.Status = 1; //Ingresado y pagado
                nvUsersProceduresCreated.PaymentStatus = 0; //Sin Order creada
                nvUsersProceduresCreated.TrackId = trackId;
                nvUsersProceduresCreated.Message = message;
                nvUsersProceduresCreated.DateInit = DateTime.Now;
                nvUsersProceduresCreated.Price = AdministradorNvConfig.GetPrice(notaryId, procedureId);
                //using (ISession session = PersistentManager.session())
                //{
                //    session.Save(nvUsersProceduresCreated);
                //    session.Flush();
                //}

                if (evidences != null && evidences.Count > 0)
                {
                    LOG.Debug("AdministradorNvUsersProcedures.CreateNvUserProcedure - Cargando evidencias...");
                    List<NvUsersProceduresEvidences> lNvEvidences = new List<NvUsersProceduresEvidences>();
                    foreach (rest.Models.Evidence item in evidences)
                    {
                        NvUsersProceduresEvidences nvEv = new NvUsersProceduresEvidences();
                        nvEv.Id = 1;
                        nvEv.Name = item.name;
                        nvEv.Type = item.type;
                        nvEv.Value = item.value;
                        nvEv.NvUsersProcedures = nvUsersProceduresCreated;
                        lNvEvidences.Add(nvEv);
                    }

                    nvUsersProceduresCreated.NvUsersProceduresEvidences = lNvEvidences;
                } else
                {
                    LOG.Debug("AdministradorNvUsersProcedures.CreateNvUserProcedure - No hay evidencias para cargar!");
                }
                using (ISession session = PersistentManager.session())
                {
                    session.Save(nvUsersProceduresCreated);
                    session.Flush();
                }
                LOG.Debug("AdministradorNvUsersProcedures.CreateNvUserProcedure Salvado en BD OK! => Id = " +
                           nvUsersProceduresCreated.Id);
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                nvUsersProceduresCreated = null;
                msgerr = "Error Creando NvUsersProcedures [" + ex.Message + "]";
                LOG.Error("AdministradorNvUsersProcedures.CreateNvUserProcedure Error", ex);
            }
            LOG.Debug("AdministradorNvUsersProcedures.CreateNvUserProcedure OUT!");
            return ret;
        }



        /// <summary>
        /// Actualiza la info cuando se recibe en el webhook un RDEnvelope
        /// 1.- Recupero el NvUsersProceduresEvidences
        /// 2.- Actualizo ingresando el trackid y el RDEnevlope
        /// 3.- Update  NvUsersProcedures para update de document, concatenando el doc original y el PDF de la CI y cambia estado
        /// </summary>
        /// <param name="sRdEnvelope"></param>
        /// <param name="htRD"></param>
        /// <param name="trackidci"></param>
        /// <returns></returns>
        internal static int UpdateInfoCIinBD(string sRdEnvelope, Hashtable htRD, out string trackidci, out NvUsersProcedures nvup)
        {
            int ret = Errors.IRET_OK;
            trackidci = null;
            nvup = null;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.UpdateInfoCIinBD IN...");

                if (htRD != null && htRD.Count > 0 && !string.IsNullOrEmpty(sRdEnvelope))
                {
                    NvUsersProceduresEvidences NvUPE =
                        AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByTrackIdCI((string)htRD["TrackIdCI"], true);

                    if (NvUPE != null)
                    {
                        NvUPE.Status = 1;
                        NvUPE.Value = sRdEnvelope;
                        ret = AdministradorNvUsersProceduresEvidences.UpdateNvUserProcedureEvidence(NvUPE);

                        if (ret < 0)
                        {
                            LOG.Warn("AdministradorNvUsersProcedures.UpdateInfoCIinBD Error: actualizando evidencia TrackIdCI = " + trackidci);
                        } else
                        {
                            ret = AdministradorNvUsersProcedures.UpdateDocumentWithCI(NvUPE.NvUsersProcedures, (string)htRD["PDFCICertify"], out nvup);
                        }
                    } else
                    {
                        LOG.Warn("AdministradorNvUsersProcedures.UpdateInfoCIinBD Error: No se encuentra registrada la CI inidicada [" +
                            trackidci + "]");
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.UpdateInfoCIinBD Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProcedures.UpdateInfoCIinBD OUT!");
            return ret;
        }

        internal static int GetNvUsersProcedureByFilter(int notaryid, string mailnotary, string mail, DateTime from, DateTime till,
                                        string trackid, int status, out string msgerr, out List<NvUsersProcedures> listProcedures,
                                        bool withChild = false)
        {
            int ret = Errors.IRET_OK;
            msgerr = null;
            listProcedures = null;
            int iduser = 0;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter IN...");
                /*
                    public virtual int IdNotaryProcedure { get; set; }
                    public virtual int IdProcedure { get; set; }
                    public virtual int IdUser { get; set; }
                    public virtual DateTime DateInit { get; set; }
                    public virtual int Status { get; set; }
                    public virtual int StatusNotary { get; set; }
                */

                if (!string.IsNullOrEmpty(mail))
                {
                    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Busco id de user para mail = " + mail);
                    iduser = (AdministradorNvUsers.BuscarNvUserByMail(mail)).Id;
                    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set IdUser = " + iduser.ToString());
                }

                using (ISession sess = PersistentManager.session())
                {
                    ICriteria crit = sess.CreateCriteria(typeof(NvUsersProcedures));

                    if (notaryid > 0)
                    {
                        crit.Add(Restrictions.Eq("IdNotary", notaryid));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set IdNotary = " + notaryid);
                    }
                    if (!string.IsNullOrEmpty(trackid) && trackid.Trim().Length > 0)
                    {
                        crit.Add(Expression.Eq("TrackId", trackid));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set TrackId = " + trackid);
                    }
                    
                    if (from != null && till != null && 
                        string.IsNullOrEmpty(trackid)) //Solo agrego filtro de fecha si no viene trackid que es unico
                    {
                        //crit.Add(Expression.Ge("DateInit", from));
                        crit.Add(Expression.Between("DateInit", from, till));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set Desde = " +
                                    from.ToString("dd/MM/yyyy HH:mm:ss") + " - Hasta = " + till.ToString("dd/MM/yyyy HH:mm:ss"));
                    }

                    //if (till != null)
                    //{
                    //    crit.Add(Expression.Le("DateInit", till));
                    //    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set Hasta = " +
                    //                till.ToString("dd/MM/yyyy HH:mm:ss"));
                    //}
                    
                    if (status >= 0)
                    {
                        crit.Add(Expression.InsensitiveLike("Status", status));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set Status = " + status);
                    }

                    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - IN get procedures con filtro...");
                    listProcedures = (List<NvUsersProcedures>)crit.AddOrder(Order.Desc("Id")).List<NvUsersProcedures>();
                    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilte - listProcedures.len = " + 
                               listProcedures.Count.ToString());

                    if (withChild && listProcedures != null)
                    {
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Entra a carga de detalles de Eviudencias y Signatories...");
                        foreach (NvUsersProcedures item in listProcedures)
                        {
                            NHibernateUtil.Initialize(item.NvUsersProceduresEvidences);
                            NHibernateUtil.Initialize(item.NvUsersProceduresSignatory);
                        }
                    }
                    //listProcedures = crit.List();
                }

                if (!withChild)
                {
                    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Lim�a detalles...");
                    foreach (NvUsersProcedures item in listProcedures)
                    {
                        item.NvUsersProceduresEvidences = null;
                        item.NvUsersProceduresSignatory = null;
                    }
                }
            }
            catch (Exception ex)
            {
                listProcedures = null;
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - OUT! - Len Ret = " +
                            ((listProcedures == null)?"Null":listProcedures.Count.ToString()));
            return ret;
        }

        /// <summary>
        /// Dado un registro lo updatea en BD
        /// </summary>
        /// <param name="nvUP"></param>
        /// <returns></returns>
        internal static bool Update(NvUsersProcedures nvUP)
        {
            bool ret = false;
           
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.Update IN...");
                using (ISession session = PersistentManager.session())
                {
                    session.SaveOrUpdate(nvUP);
                    session.Flush();
                }
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("AdministradorNvUsersProcedures.Update Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProcedures.Update OUT!");
            return ret;
        }

        /// <summary>
        /// Dada el el 
        /// 1.- Actualizo NvUsersProcedures agregando el PDF de CI al final del document existente en BD
        /// </summary>
        /// <param name="nvUsersProcedures"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        private static int UpdateDocumentWithCI(NvUsersProcedures nvusersprocedure, string pdfcertify, out NvUsersProcedures nvup)
        {
            int ret = 0;
            nvup = null;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.UpdateDocumentWithCI IN...");
                if (nvusersprocedure == null || string.IsNullOrEmpty(pdfcertify))
                {
                    ret = -1;
                    LOG.Error("AdministradorNvUsersProcedures.UpdateDocumentWithCI Error parametros para merge nulo");
                } else
                {
                    LOG.Debug("AdministradorNvUsersProcedures.UpdateDocumentWithCI - Haciendo merge de doc con CI...");
                    LOG.Debug("AdministradorNvUsersProcedures.UpdateDocumentWithCI - Len nvusersprocedure.DocumentB64 = " +
                        nvusersprocedure.DocumentB64.Length.ToString());

                    //Added 10/12 - Porque se completa cunado se manda a notarizar desde revision
                    //string[] listFiles = new string[2];
                    //listFiles[0] = nvusersprocedure.DocumentB64Original;
                    //listFiles[1] = pdfcertify;
                    //nvusersprocedure.DocumentB64 = Helpers.DEHelper.MergeFiles(listFiles);
                    nvusersprocedure.DocumentB64 = nvusersprocedure.DocumentB64Original;
                    LOG.Debug("AdministradorNvUsersProcedures.UpdateDocumentWithCI - Merge ok. [nvusersprocedure.DocumentB64!=null] => " +
                                   (nvusersprocedure.DocumentB64 != null).ToString());
                    if (nvusersprocedure.DocumentB64 != null)
                    {
                        nvusersprocedure.Status = StatusNvUsersProcedures.STATUS_PAYMENT_PENDING;
                        LOG.Debug("AdministradorNvUsersProcedures.UpdateDocumentWithCI - NEW Len nvusersprocedure.DocumentB64 = " +
                                    nvusersprocedure.DocumentB64.Length.ToString());

                        //Si esta habilitado debug graba pdf para ver como lo forma
                        if (Properties.Settings.Default.DebugVideoImagen == 1 || Properties.Settings.Default.DebugVideoImagen == 3) 
                        {
                            try
                            {
                                string semilla = DateTime.Now.ToString("yyyyMMddHHmmss") + "_";
                                System.IO.File.WriteAllBytes(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + semilla +
                                            nvusersprocedure.TrackId + ".pdf", Convert.FromBase64String(nvusersprocedure.DocumentB64));
                            }
                            catch (Exception ex)
                            {
                                LOG.Error("AdministradorNvUsersProcedures.UpdateDocumentWithCI Error grabando pdf orginal por debug => " + ex.Message);
                            }   
                        }
                    }
                    LOG.Debug("AdministradorNvUsersProcedures.UpdateDocumentWithCI - Saving in BD...");
                    using (ISession session = PersistentManager.session())
                    {
                        session.SaveOrUpdate(nvusersprocedure);
                        session.Flush();
                    }
                    nvup = nvusersprocedure;
                    LOG.Debug("AdministradorNvUsersProcedures.UpdateDocumentWithCI - Savied!");
                }
                ret = 0;
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("AdministradorNvUsersProcedures.UpdateDocumentWithCI Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProcedures.UpdateDocumentWithCI OUT!");
            return ret;
        }

        /// <summary>
        /// Crea tramite en modo no logueado, entonces hay datos que se agregan genericos.
        /// </summary>
        /// <param name="trackId"></param>
        /// <param name="userId"></param>
        /// <param name="notaryId"></param>
        /// <param name="procedureId"></param>
        /// <param name="document"></param>
        /// <param name="trackidci"></param>
        /// <param name="qrci"></param>
        /// <param name="provideridout"></param>
        /// <param name="msgerr"></param>
        /// <param name="nvUsersProceduresCreated"></param>
        /// <returns></returns>
        internal static int CreateNvUserProcedure(string trackId, int userId, int notaryId, int procedureId, string document, string msgcustomer,
            string trackidci, string qrci, out int provideridout, out string msgerr, out NvUsersProcedures nvUsersProceduresCreated)
        {
            nvUsersProceduresCreated = null;
            int ret = Errors.IRET_OK;
            msgerr = null;
            provideridout = 0;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.CreateNvUserProcedure IN...");
                nvUsersProceduresCreated = new NvUsersProcedures();
                nvUsersProceduresCreated.IdUser = userId;
                nvUsersProceduresCreated.IdNotary = notaryId;
                nvUsersProceduresCreated.IdNotaryProcedure = procedureId;
                nvUsersProceduresCreated.IdProcedure = procedureId;
                nvUsersProceduresCreated.NameProcedure = AdministradorNvConfig.GetNameProcedure(notaryId, procedureId);
                nvUsersProceduresCreated.IdProvider = AdministradorNvConfig.GetProvider(notaryId);
                nvUsersProceduresCreated.DocumentB64 = string.IsNullOrEmpty(document) ?"PENDIENTE": document;
                nvUsersProceduresCreated.DocumentB64Original = string.IsNullOrEmpty(document) ? "PENDIENTE" : document;
                provideridout = nvUsersProceduresCreated.IdProvider;
                //Chequea si mail est� verificado sino status pending
                // Si devuelve 0 => esta validado
                //  sino Falta validar
                if (AdministradorNvUsers.CheckStatusMailUser(userId) == 0)
                {
                    nvUsersProceduresCreated.Status = StatusNvUsersProcedures.STATUS_CI_PENDING;
                } else {
                    nvUsersProceduresCreated.Status = StatusNvUsersProcedures.STATUS_MAIL_VERIFY_PENDING;
                }
                nvUsersProceduresCreated.PaymentStatus = 0; //Aun no creada Order en Flow
                nvUsersProceduresCreated.TrackId = trackId;
                nvUsersProceduresCreated.Message = string.IsNullOrEmpty(msgcustomer)?"Sin Comentarios": msgcustomer;
                nvUsersProceduresCreated.DateInit = DateTime.Now;
                nvUsersProceduresCreated.Price = AdministradorNvConfig.GetPrice(notaryId, procedureId);

                if (!string.IsNullOrEmpty(trackidci) && !string.IsNullOrEmpty(qrci))
                {
                    NvUsersProceduresEvidences ev = new NvUsersProceduresEvidences();
                    ev.Name = "Certificacion de Identidad";
                    ev.TrackIdExt = trackidci;
                    ev.Type = 1;
                    ev.Status = 0;
                    ev.QRCI = qrci;
                    ev.NvUsersProcedures = nvUsersProceduresCreated;
                    List<NvUsersProceduresEvidences> lEv = new List<NvUsersProceduresEvidences>();
                    lEv.Add(ev);
                    nvUsersProceduresCreated.NvUsersProceduresEvidences = lEv;
                }

                using (ISession session = PersistentManager.session())
                {
                    session.Save(nvUsersProceduresCreated);
                    session.Flush();
                }
                LOG.Debug("AdministradorNvUsersProcedures.CreateNvUserProcedure Salvado en BD OK! => Id = " +
                           nvUsersProceduresCreated.Id + " - UserId = " + userId);
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                nvUsersProceduresCreated = null;
                msgerr = "Error Creando NvUsersProcedures [" + ex.Message + "]";
                LOG.Error("AdministradorNvUsersProcedures.CreateNvUserProcedure Error", ex);
            }
            LOG.Debug("AdministradorNvUsersProcedures.CreateNvUserProcedure OUT!");
            return ret;
        }

        internal static int Write(NvUsersProcedures procedure)
        {
            throw new NotImplementedException();
        }

        internal static int SetNvUserProcedureNotarizing(string trackId)
        {
            NvUsersProcedures bvUPtoUpdate = null;
            int ret = Errors.IRET_OK;
           
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.SetNvUserProcedureNotarizing IN...");
                
                using (ISession session = PersistentManager.session())
                {
                    bvUPtoUpdate = PersistentManager.session().CreateCriteria(typeof(NvUsersProcedures))
                                         .Add(NHibernate.Criterion.Expression.Eq("TrackId", trackId))
                                         .UniqueResult<NvUsersProcedures>();
                    if (bvUPtoUpdate != null)
                    {
                        bvUPtoUpdate.Status = StatusNvUsersProcedures.STATUS_IN_PROCESS_NOTARIZING; //Notarizando
                        session.SaveOrUpdate(bvUPtoUpdate);
                        session.Flush();
                        LOG.Debug("AdministradorNvUsersProcedures.SetNvUserProcedureNotarizing Salvado en BD OK!");
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.SetNvUserProcedureNotarizing Error", ex);
            }
            LOG.Debug("AdministradorNvUsersProcedures.SetNvUserProcedureNotarizing OUT!");
            return ret;
        }

        /// <summary>
        /// Recuepra un tramite por su id
        /// </summary>
        /// <param name="idProcedure"></param>
        /// <returns></returns>
        internal static NvUsersProcedures GetNvUsersProcedureById(int idProcedure)
        {
            NvUsersProcedures bvUPtoRet = null;

            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureById IN...");

                using (ISession session = PersistentManager.session())
                {
                    bvUPtoRet = session.CreateCriteria(typeof(NvUsersProcedures))
                                         .Add(NHibernate.Criterion.Expression.Eq("Id", idProcedure))
                                         .UniqueResult<NvUsersProcedures>();
                }
            }
            catch (Exception ex)
            {
                bvUPtoRet = null;
                LOG.Error("AdministradorNvUsersProcedures.GetNvUsersProcedureById Error", ex);
            }
            LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureById OUT!");
            return bvUPtoRet;
        }

        /// <summary>
        /// Recuepra un tramite por su TrackId
        /// </summary>
        /// <param name="trackId"></param>
        /// <returns></returns>
        internal static NvUsersProcedures GetNvUsersProcedureByTrackId(string trackId, out string msgerr, bool withChild = false)
        {
            NvUsersProcedures bvUPtoRet = null;
            msgerr = null;

            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId IN...");

                using (ISession session = PersistentManager.session())
                {
                    bvUPtoRet = session.CreateCriteria(typeof(NvUsersProcedures))
                                         .Add(NHibernate.Criterion.Expression.Eq("TrackId", trackId))
                                         .UniqueResult<NvUsersProcedures>();
                    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId - Procedure recuperado => " +
                                ((bvUPtoRet!=null).ToString()));
                    if (withChild && bvUPtoRet != null)
                    {
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId - Llenando details...");
                        if (bvUPtoRet.NvUsersProceduresEvidences == null ||
                            bvUPtoRet.NvUsersProceduresEvidences.Count == 0)
                        {
                            NHibernateUtil.Initialize(bvUPtoRet.NvUsersProceduresEvidences);
                        }
                        if (bvUPtoRet.NvUsersProceduresSignatory == null ||
                            bvUPtoRet.NvUsersProceduresSignatory.Count == 0)
                        {
                            NHibernateUtil.Initialize(bvUPtoRet.NvUsersProceduresSignatory);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bvUPtoRet = null;
                msgerr = "Error recuperando el procedure desde BD [" + ex.Message + "]";
                LOG.Error("AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId Error", ex);
            }
            LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId OUT!");
            return bvUPtoRet;
        }

        internal static int SetNvUserProcedureNotarized(int code, string msgerr, string trackid, string document, string urlvalidator, string notaryidvalidator)
        {
            NvUsersProcedures bvUPtoUpdate = null;
            int ret = Errors.IRET_OK;

            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.SetNvUserProcedureNotarized IN...");

                using (ISession session = PersistentManager.session())
                {
                    bvUPtoUpdate = PersistentManager.session().CreateCriteria(typeof(NvUsersProcedures))
                                         .Add(NHibernate.Criterion.Expression.Eq("TrackId", trackid))
                                         .UniqueResult<NvUsersProcedures>();
                    if (bvUPtoUpdate != null)
                    {
                        if (code == 200)
                        {
                            bvUPtoUpdate.Status = StatusNvUsersProcedures.STATUS_NOTARIZED; //Notarizado
                            bvUPtoUpdate.DocumentB64Notarized = document;
                            bvUPtoUpdate.UrlValidator = urlvalidator;
                            bvUPtoUpdate.NotaryIdValidator = notaryidvalidator;
                            bvUPtoUpdate.DateFinish = DateTime.Now;
                            bvUPtoUpdate.StatusNotary = 200;
                            bvUPtoUpdate.StatusMsgNotary = "Tramite Notarizado OK";
                        } else 
                        {
                            if (code == 1009) bvUPtoUpdate.Status = StatusNvUsersProcedures.STATUS_REJECTED; //Notarizado
                            bvUPtoUpdate.StatusNotary = code;
                            bvUPtoUpdate.StatusMsgNotary = msgerr;
                        }
                        
                        session.SaveOrUpdate(bvUPtoUpdate);
                        session.Flush();
                        LOG.Debug("AdministradorNvUsersProcedures.SetNvUserProcedureNotarized Salvado en BD OK!");

                        //Informo x mail
                        //string sdoc = File.ReadAllText(@"c:\tmp\doc.base64.txt");
                        if (code == 200)
                        {
                            byte[] doc = Convert.FromBase64String(bvUPtoUpdate.DocumentB64Notarized);
                            string[] strbody = new string[3];
                            strbody[0] = "Su tr�mite <b>" + bvUPtoUpdate.NameProcedure + "</b> a finalizado con exito. Felicitaciones! Adjunto encontrar� el archivo PDF notarizado.";
                            strbody[1] = "El codigo unico del tramite para consultas o descargas desde el sitio de Notario Virtual es: <b>" +
                                        bvUPtoUpdate.TrackId + "</b>.";
                            strbody[2] = "Para chequeo del tr�mite en linea en la notaria, conectese a la direcci�n " +
                                         "<a href=\"http://www.verify.cl\">" + bvUPtoUpdate.UrlValidator + "<a/>, ingresando el codigo de verifciacion: " +
                                         "<b>" + bvUPtoUpdate.NotaryIdValidator + "</b>";
                            libs.Utils.NVNotify(AdministradorNvUsers.BuscarNvUserById(bvUPtoUpdate.IdUser).Mail,
                                                "Tramite finalizado con Exito!", strbody, doc, "pdf", bvUPtoUpdate.TrackId + ".pdf");
                        } else if (code == 1009)
                        {
                            string[] strbody = new string[3];
                            strbody[0] = "Su tr�mite <b>" + bvUPtoUpdate.NameProcedure + "</b> a sido rechazado por el notario!";
                            strbody[1] = "El motivo del rechazo por parte del notario es: <b>" + bvUPtoUpdate.StatusMsgNotary + "</b>.";
                            strbody[2] = "Para chequeo del tr�mite en linea, usar el codigo unico del tramite en el sitio de Notario Virtual: <b> " +
                                        bvUPtoUpdate.TrackId + "</b>.";
                            libs.Utils.NVNotify(AdministradorNvUsers.BuscarNvUserById(bvUPtoUpdate.IdUser).Mail,
                                                "Tramite Rechazado!", strbody, null, null, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.SetNvUserProcedureNotarized Error", ex);
            }
            LOG.Debug("AdministradorNvUsersProcedures.SetNvUserProcedureNotarized OUT!");
            return ret;
        }

        internal static int SetNvUserProcedureNotarized(NotarizedProviderResponseModel response)
        {
            NvUsersProcedures bvUPtoUpdate = null;
            int ret = Errors.IRET_OK;

            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.SetNvUserProcedureNotarized IN...");

                using (ISession session = PersistentManager.session())
                {
                    bvUPtoUpdate = PersistentManager.session().CreateCriteria(typeof(NvUsersProcedures))
                                         .Add(NHibernate.Criterion.Expression.Eq("TrackId", response.trackId))
                                         .UniqueResult<NvUsersProcedures>();
                    if (bvUPtoUpdate != null)
                    {
                        bvUPtoUpdate.Status = 3; //Notarizado
                        bvUPtoUpdate.DocumentB64Notarized = response.document;
                        bvUPtoUpdate.UrlValidator = response.urlvalidator;
                        bvUPtoUpdate.NotaryIdValidator = response.notaryidvalidator;
                        bvUPtoUpdate.DateFinish = DateTime.Now;
                        session.SaveOrUpdate(bvUPtoUpdate);
                        session.Flush();
                        LOG.Debug("AdministradorNvUsersProcedures.SetNvUserProcedureNotarized Salvado en BD OK!");
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.SetNvUserProcedureNotarized Error", ex);
            }
            LOG.Debug("AdministradorNvUsersProcedures.SetNvUserProcedureNotarized OUT!");
            return ret;
        }

        internal static int UpdateForInitNotarize(string trackid, string mail, int notaryId, int procedureId, string document, 
                                         List<rest.Models.Evidence> evidences, string payment, string message, 
                                         out int provideridout, out string msgerr, out NvUsersProcedures nvUsersProceduresUpdated)
        {
            nvUsersProceduresUpdated = null;
            int ret = Errors.IRET_OK;
            msgerr = null;
            provideridout = 0;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.UpdateForInitNotarize IN...");

                nvUsersProceduresUpdated = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr);
                provideridout = nvUsersProceduresUpdated.IdProvider;
                nvUsersProceduresUpdated.DocumentB64Original = document;
                nvUsersProceduresUpdated.DocumentB64 = document;
                //nvUsersProceduresUpdated.Status = StatusNvUsersProcedures.STATUS_IN_PROCESS_NOTARIZING; //Ingresado y pagado
                if (!string.IsNullOrEmpty(payment))
                {
                    //nvUsersProceduresUpdated.PaymentInfo = payment;
                    nvUsersProceduresUpdated.PaymentStatus = 2; //Pagado
                }
                if (!string.IsNullOrEmpty(message))
                {
                    nvUsersProceduresUpdated.Message = message;
                }                   

                using (ISession session = PersistentManager.session())
                {
                    session.SaveOrUpdate(nvUsersProceduresUpdated);
                    session.Flush();
                }
                LOG.Debug("AdministradorNvUsersProcedures.UpdateForInitNotarize Salvado en BD OK! => Id = " +
                           nvUsersProceduresUpdated.Id + " - UserId = " + nvUsersProceduresUpdated.IdUser + 
                           " - Mail = " + mail);
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_API_INTERNAL_SERVER_ERROR;
                nvUsersProceduresUpdated = null;
                msgerr = "Error Creando NvUsersProcedures [" + ex.Message + "]";
                LOG.Error("AdministradorNvUsersProcedures.UpdateForInitNotarize Error", ex);
            }
            LOG.Debug("AdministradorNvUsersProcedures.UpdateForInitNotarize OUT!");
            return ret;
        }

        #region V2

        internal static int UpdateInfoCIWebinBD(string nvtrackid, TxSignerModelR oTxSignerModelR, out string trackidci, out NvUsersProcedures nvup)
        {
            int ret = Errors.IRET_OK;
            trackidci = null;
            nvup = null;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.UpdateInfoCIWebinBD IN...");

                if (oTxSignerModelR != null)
                {
                    //Agrego las evidencias adicionale sdel tramite: Actualizo CI + Imagenes + Video
                    LOG.Debug("AdministradorNvUsersProcedures.UpdateInfoCIWebinBD - Agrego nuevas evidencias...");
                    ret = AddNewEvidences(nvtrackid, oTxSignerModelR);

                    if (ret < 0)
                    {
                        LOG.Warn("AdministradorNvUsersProcedures.UpdateInfoCIWebinBD Error: agregando nuevas evidencias en NvProcedure TrackId = " + oTxSignerModelR.trackid);
                    }

                    //Actualizo ahora la CI
                    NvUsersProceduresEvidences NvUPE =
                        AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByTrackIdCI(oTxSignerModelR.trackid, true);

                    if (NvUPE != null)
                    {
                        NvUPE.Status = 1;
                        LOG.Debug("AdministradorNvUsersProcedures.UpdateInfoCIWebinBD - Recupero Receipt con Id = " + oTxSignerModelR.receiptId);
                        NvUPE.Value = AdministradorRdRecibos.BuscarRdReciboById(oTxSignerModelR.receiptId).Reciboxml;
                        LOG.Debug("AdministradorNvUsersProcedures.UpdateInfoCIWebinBD - Receipt asignado...");

                        LOG.Debug("AdministradorNvUsersProcedures.UpdateInfoCIWebinBD - Update en BD de NvUPE...");
                        ret = AdministradorNvUsersProceduresEvidences.UpdateNvUserProcedureEvidence(NvUPE);

                        if (ret < 0)
                        {
                            LOG.Warn("AdministradorNvUsersProcedures.UpdateInfoCIWebinBD Error: actualizando evidencia TrackIdCI = " + trackidci);
                        }
                        else
                        {
                            LOG.Debug("AdministradorNvUsersProcedures.UpdateInfoCIWebinBD - Haciendo merge con Doc + CI con UpdateDocumentWithCI...");
                            ret = AdministradorNvUsersProcedures.UpdateDocumentWithCI(NvUPE.NvUsersProcedures, oTxSignerModelR.document, out nvup);
                        }
                    }
                    else
                    {
                        LOG.Warn("AdministradorNvUsersProcedures.UpdateInfoCIWebinBD Error: No se encuentra registrada la CI inidicada [" +
                            trackidci + "]");
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.UpdateInfoCIWebinBD Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProcedures.UpdateInfoCIWebinBD OUT!");
            return ret;
        }

        private static int AddNewEvidences(string trackid, TxSignerModelR oTxSignerModelR)
        {
            int ret = 0;
            /*
                1. Recupero el NvProcedure
                2. Agrego a la lista de evidecnias, cada evidencia desde oTxSignerModelR.Samples
                3. Grabo de neuvo NvProcedure

            */
            string msgerr;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences IN...");
                NvUsersProcedures nvup = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr, true);

                NvUsersProceduresEvidences nvupe;
                if (nvup != null)
                {

                    LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences - Inicio add => Len Lista Ev = " +
                                    nvup.NvUsersProceduresEvidences.Count.ToString());
                    //Type =>  1 - CI | 2 - Video | 3 - URLVideo | 4 - Image | 5 - PDF
                    //Add Video
                    LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences - Adding Video...");
                    nvupe = new NvUsersProceduresEvidences();
                    nvupe.Name = "video";
                    nvupe.Type = 3;
                    nvupe.NvUsersProcedures = nvup;
                    nvupe.Value = GetValueFromSamples(oTxSignerModelR.Samples, "video");
                    if (!string.IsNullOrEmpty(nvupe.Value)) {
                        nvup.NvUsersProceduresEvidences.Add(nvupe);
                        LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences - Added Video!");
                    }

                    //Add FrontCedula
                    LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences - Adding idcardimagefront...");
                    nvupe = new NvUsersProceduresEvidences();
                    nvupe.Name = "idcardimagefront";
                    nvupe.Type = 4;
                    nvupe.NvUsersProcedures = nvup;
                    nvupe.Value = GetValueFromSamples(oTxSignerModelR.Samples, "idcardimagefront");
                    if (!string.IsNullOrEmpty(nvupe.Value))
                    {
                        nvup.NvUsersProceduresEvidences.Add(nvupe);
                        LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences - Added idcardimagefront!");
                    }

                    //Add BackCedula
                    LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences - Adding idcardimageback...");
                    nvupe = new NvUsersProceduresEvidences();
                    nvupe.Name = "idcardimageback";
                    nvupe.Type = 4;
                    nvupe.NvUsersProcedures = nvup;
                    nvupe.Value = GetValueFromSamples(oTxSignerModelR.Samples, "idcardimageback");
                    if (!string.IsNullOrEmpty(nvupe.Value))
                    {
                        nvup.NvUsersProceduresEvidences.Add(nvupe);
                        LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences - Added idcardimageback!");
                    }

                    //Add Selfie
                    LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences - Adding selfie...");
                    nvupe = new NvUsersProceduresEvidences();
                    nvupe.Name = "selfie";
                    nvupe.Type = 4;
                    nvupe.NvUsersProcedures = nvup;
                    nvupe.Value = GetValueFromSamples(oTxSignerModelR.Samples, "selfie");
                    if (!string.IsNullOrEmpty(nvupe.Value))
                    {
                        nvup.NvUsersProceduresEvidences.Add(nvupe);
                        LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences - Added selfie!");
                    }

                    //Add SignatureManual
                    LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences - Adding manualsignatureimage...");
                    nvupe = new NvUsersProceduresEvidences();
                    nvupe.Name = "manualsignatureimage";
                    nvupe.Type = 4;
                    nvupe.NvUsersProcedures = nvup;
                    nvupe.Value = GetValueFromSamples(oTxSignerModelR.Samples, "manualsignatureimage");
                    if (!string.IsNullOrEmpty(nvupe.Value))
                    {
                        nvup.NvUsersProceduresEvidences.Add(nvupe);
                        LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences - Added manualsignatureimage!");
                    }

                    if (!AdministradorNvUsersProcedures.Update(nvup))
                    {
                        LOG.Warn("AdministradorNvUsersProcedures.AddNewEvidences - Error grabando evidencias adicionales!");
                    } else
                    {
                        LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences - Q LIst Final Saved => " +
                                            nvup.NvUsersProceduresEvidences.Count.ToString());
                    }
                }
                else
                {
                    ret = Errors.IRET_ERR_INFORMED_PROVIDER_SERVICE * -1;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.AddNewEvidences - Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProcedures.AddNewEvidences OUT!");
            return ret;
        }

        private static string GetValueFromSamples(IList<ImageSample> samples, string key)
        {
            string ret = null;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.GetValueFromSamples IN con key = " + key);
                LOG.Debug("AdministradorNvUsersProcedures.GetValueFromSamples - Samples Len = " +
                            ((samples==null)?"Null":samples.Count.ToString()));
                foreach (ImageSample item in samples)
                {
                    if (item.code.Equals(key))
                    {
                        LOG.Debug("AdministradorNvUsersProcedures.GetValueFromSamples OUT Value = " + 
                            (string.IsNullOrEmpty(item.data)?"NULL":item.data.Substring(0, 10)) + "...");
                        return item.data;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("AdministradorNvUsersProcedures.GetValueFromSamples - Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProcedures.GetValueFromSamples OUT NULL!");
            return ret;
        }


        #endregion V2


        #region WebSite Helper

        /// <summary>
        /// Recupera tramites para visualizar en website
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="trackId"></param>
        /// <param name="datefrom"></param>
        /// <param name="datetill"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        internal static List<NvUsersProcedures> GetNvUsersProcedureByFilter(int userid, string trackId, string datefrom, string datetill,
                                                                            int status)
        {
            List<NvUsersProcedures> bvUPtoReturn = null;
            int ret = Errors.IRET_OK;

            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter IN...");

                using (ISession session = PersistentManager.session())
                {
                    ICriteria criteria = session.CreateCriteria(typeof(NvUsersProcedures));

                    if (userid > 0)
                    {
                        criteria.Add(Expression.Eq("IdUser", userid));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - criteria.Added IdUser = " + userid);
                    }

                    if (!string.IsNullOrEmpty(trackId))
                    {
                        criteria.Add(Expression.Eq("TrackId", trackId));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - criteria.Added TrackId = " + trackId);
                    }

                    DateTime _hasta = DateTime.Now;
                    DateTime _desde = _hasta.AddDays(-30);
                    //if (string.IsNullOrEmpty(datefrom) && string.IsNullOrEmpty(datetill))
                    //{
                    //    _hasta = DateTime.Now;
                    //    _desde = _hasta.AddDays(-30);
                    //} else 
                    if (!string.IsNullOrEmpty(datefrom) && string.IsNullOrEmpty(datetill))
                    {
                        try
                        {
                            _desde = DateTime.Parse(datefrom);
                        }
                        catch (Exception ex)
                        {
                            _desde = DateTime.Now.AddDays(-30);
                            LOG.Error("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter Error parsing datefrom => " + datefrom);
                        }
                        _hasta = _desde.AddDays(30);
                    }
                    else if (string.IsNullOrEmpty(datefrom) && !string.IsNullOrEmpty(datetill))
                    {
                        try
                        {
                            _hasta = DateTime.Parse(datetill);
                        }
                        catch (Exception ex)
                        {
                            _hasta = DateTime.Now;
                            LOG.Error("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter Error parsing datefrom => " + datetill);
                        }
                        _desde = _hasta.AddDays(-30);
                    } else  if (!string.IsNullOrEmpty(datefrom) && !string.IsNullOrEmpty(datetill))
                    {
                        try
                        {
                            _desde = DateTime.Parse(datefrom);
                        }
                        catch (Exception ex)
                        {
                            _desde = DateTime.Now.AddDays(-30);
                            LOG.Error("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter Error parsing datefrom => " + datefrom);
                        }
                        try
                        {
                            _hasta = DateTime.Parse(datetill);
                        }
                        catch (Exception ex)
                        {
                            _hasta = DateTime.Now;
                            LOG.Error("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter Error parsing datefrom => " + datetill);
                        }
                        if ((_hasta - _desde).Days > 30)
                        {
                            _desde = _hasta.AddDays(-30);
                        }
                    }

                    criteria.Add(Expression.Between("DateInit", _desde, _hasta));
                    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - criteria.Added Desde = " + datefrom +
                               " - Hasta = " + datetill);

                    if (status > 0)
                    {
                        criteria.Add(Expression.Eq("Status", status));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - criteria.Added Status = " + status);
                    }

                    bvUPtoReturn = (List<NvUsersProcedures>) criteria.AddOrder(Order.Desc("DateInit")).List<NvUsersProcedures>();
                }
            }
            catch (Exception ex)
            {
                bvUPtoReturn = null;
                LOG.Error("AdministradorNvUsersProcedures.SetNvUserProcedureNotarizing Error", ex);
            }
            LOG.Error("AdministradorNvUsersProcedures.SetNvUserProcedureNotarizing OUT!");
            return bvUPtoReturn;
        }

        internal static int UpdateStatusMailValidated(int userid)
        {
            List<NvUsersProcedures> bvUPtoReturn = null;
            int ret = Errors.IRET_OK;

            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.UpdateStatusMailValidated IN...");

                using (ISession session = PersistentManager.session())
                {
                    ICriteria criteria = session.CreateCriteria(typeof(NvUsersProcedures)).
                                            Add(Expression.Eq("IdUser", userid));

                    bvUPtoReturn = (List<NvUsersProcedures>)criteria.List<NvUsersProcedures>();

                    if (bvUPtoReturn != null && bvUPtoReturn.Count > 0)
                    {
                        foreach (NvUsersProcedures item in bvUPtoReturn)
                        {
                            if (item.Status == StatusNvUsersProcedures.STATUS_MAIL_VERIFY_PENDING)
                            {
                                item.Status = StatusNvUsersProcedures.STATUS_CI_PENDING;
                                session.SaveOrUpdate(item);
                                session.Flush();
                                LOG.Debug("AdministradorNvUsersProcedures.UpdateStatusMailValidated " +
                                            "Actualizado ProcedureId = " + item.Id);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.SetNvUserProcedureNotarizing Error", ex);
            }
            LOG.Error("AdministradorNvUsersProcedures.SetNvUserProcedureNotarizing OUT!");
            return ret;
        }

        internal static int DeleteNvProcedures(int userid)
        {
            List<NvUsersProcedures> bvUPtoReturn = null;
            int ret = Errors.IRET_OK;

            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.DeleteNvProcedures IN...");

                using (ISession session = PersistentManager.session())
                {
                    ICriteria criteria = session.CreateCriteria(typeof(NvUsersProcedures)).
                                            Add(Expression.Eq("IdUser", userid));

                    bvUPtoReturn = (List<NvUsersProcedures>)criteria.List<NvUsersProcedures>();

                    if (bvUPtoReturn != null && bvUPtoReturn.Count > 0)
                    {
                        foreach (NvUsersProcedures item in bvUPtoReturn)
                        {
                            session.Delete(item);
                            session.Flush();
                            LOG.Debug("AdministradorNvUsersProcedures.DeleteNvProcedures " +
                                        "Eliminado ProcedureId = " + item.Id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.DeleteNvProcedures Error", ex);
            }
            LOG.Error("AdministradorNvUsersProcedures.DeleteNvProcedures OUT!");
            return ret;
        }

        /// <summary>
        /// Crea salida para cuando desde front end pide data de un procedure. Se colocan todos los datos disponibles
        /// para mostrar la ficha lo mas completa posible.
        /// </summary>
        /// <param name="procedure"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        internal static int CreateResponseTotalProcedure(NvUsersProcedures procedure, out ProcedureResponseModel response)
        {
            int ret = Errors.IRET_ERR_DESCONOCIDO;
            response = new ProcedureResponseModel();
            try
            {
                //1.-Encabezado
                response.code = 200;
                response.msgError = null;

                //2.- Info de procedure
                //2.1.- Info de procedure Base
                response.Id = procedure.Id;
                response.TrackId = procedure.TrackId;
                
                response.Provider =  AdministradorNvConfig.GetNameProvider(procedure.IdProvider, procedure.IdNotary);
                response.ProcedureId = procedure.IdProcedure;
                response.NameProcedure = procedure.NameProcedure;
                response.Price = procedure.Price;
                response.DateInit = procedure.DateInit.ToString("dd/MM/yyyy HH:mm:ss");
                response.DateEnd = procedure.DateFinish.HasValue? procedure.DateFinish.Value.ToString("dd/MM/yyyy HH:mm:ss") : null;
                response.Status = procedure.Status;

                NvUsers _user = AdministradorNvUsers.BuscarNvUserById(procedure.IdUser);
                response.Message = procedure.Message;
                if (_user != null)
                {
                    response.Rut = _user.Rut;
                    response.Mail = _user.Mail;
                } else
                {
                    response.Rut = "No Obtenido";
                    response.Mail = "No Obtenido";
                    response.msgError = "Datos de usuario no obtenidos!";
                }

                //2.3.- Info Notaria/Notario
                response.NotaryId = procedure.IdNotary;
                response.Notary = AdministradorNvConfig.GetNameNotary(procedure.IdProvider, procedure.IdNotary);
                response.UrlValidator = procedure.UrlValidator;
                response.NotaryIdValidator = procedure.NotaryIdValidator;
                response.StatusNotary = procedure.StatusNotary;
                response.StatusMsgNotary = procedure.StatusMsgNotary;

                //2.4.- Info Payment
                response.PaymentUrl = procedure.PaymentUrl;
                response.PaymentToken = procedure.PaymentToken;
                response.PaymentStatus = procedure.PaymentStatus;
                response.PaymentOrder = procedure.PaymentOrder;
                response.PaymentMedia = procedure.PaymentMedia;
                response.PaymentDate = procedure.PaymentDate;

                //3.- Info Evidencias
                ProcedureEvidence ev;
                List<ProcedureEvidence> ProcedureEvidences = new List<ProcedureEvidence>();
                List<NvUsersProceduresEvidences> listEv = 
                    AdministradorNvUsersProceduresEvidences.GetListNvUserProcedureEvidenceByIdProcedure(procedure.Id);

                foreach (NvUsersProceduresEvidences item in listEv)
                {
                    ev = new ProcedureEvidence(item.Id, item.Name, item.Type, item.Value, item.TrackIdExt, item.Status);
                    if (item.Type == 1) //Si es CI parseo y lleno cada item en lista
                    {
                        ev.Value = null; //Si es CI lo pongo en null, porque mando solo foto y datos base, mas una URL para mostrar resultado
                        Hashtable htItems = new Hashtable();
                        if (NotarizeController.ParseRDEnvelope(item.Value, out htItems))
                        {
                            CIItem ciItem;
                            ev.CIItems = new List<CIItem>();
                            foreach (DictionaryEntry de in htItems)
                            {
                                if (de.Key.Equals("IDCardPhotoImage") ||
                                    de.Key.Equals("ValueId") ||
                                    de.Key.Equals("ValueId") ||
                                    de.Key.Equals("Name") ||
                                    de.Key.Equals("Score"))
                                {
                                    ciItem = new CIItem((string)de.Key, (string)de.Value);
                                    ev.CIItems.Add(ciItem);
                                }
                            }
                            if (ev.CIItems.Count > 0)
                            {
                                ciItem = new CIItem("urlcheckci", Properties.Settings.Default.URLCICheck + item.TrackIdExt);
                                ev.CIItems.Add(ciItem);
                            }
                        }
                    }
                    ProcedureEvidences.Add(ev);
                }
                response.ProcedureEvidences = ProcedureEvidences;
                ret = Errors.IRET_OK;
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                response = null;
                LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }


        #endregion WebSite Helper

        #region Payment Handler

        /// <summary>
        /// Dado un procedure o su trackid se genera una order de pago en flow y se actualiza con los 
        /// datos de respuesta en la BD
        /// </summary>
        /// <param name="procedure">Si no viene nulo usamos este</param>
        /// <param name="trackid">Si procedure es nulo, con el trackid recuperamos el procedure desde BD</param>
        /// <returns></returns>
        internal static int CreatePaymentOrderForProcedure(NvUsersProcedures procedure, string trackid)
        {
            int ret = Errors.IRET_OK;
            NvUsersProcedures _procedure = procedure;
            string msgerr;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure IN...");
                if (_procedure == null)
                {
                    LOG.Debug("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure - Recupera procedure de BD con TrackId = " + trackid);
                    _procedure = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr);
                }

                if (_procedure == null)
                {
                    ret = Errors.IRET_ERR_USER_PROCEDURE_NOT_FOUND;
                    LOG.Debug("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure - Procedure no disponible con TrackId = " + trackid);
                } else
                {
                    //Si es 0 => Ya se genero order => devuelvo ok
                    if (procedure.PaymentStatus > 0)
                    {
                        LOG.Debug("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure - Procedure con order ya creada! " + 
                            " Order Nro= " + (string.IsNullOrEmpty(procedure.PaymentOrder)?"NULL": procedure.PaymentOrder) +
                            " - Token = " + (string.IsNullOrEmpty(procedure.PaymentToken) ? "NULL" : procedure.PaymentToken));
                        ret = Errors.IRET_OK;
                    } else
                    {
                        object response;
                        NvUsers user = AdministradorNvUsers.BuscarNvUserById(procedure.IdUser, true);
                        string _mail = (user != null) ? user.Mail : null;
                        if (string.IsNullOrEmpty(_mail))
                        {
                            LOG.Debug("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure - Usuario con id = " +
                                procedure.IdUser + " no disponible o con mail no verificado");
                            ret = Errors.IRET_ERR_MAIL_VALIDATED;
                        } else
                        {

                            string strresponse;
                            int retF = Global.FLOW_API.CreateOrderPayment("Pago Notario Virtual [id=" + procedure.Id + "]", "CLP",
                                                                        procedure.Price.ToString(), _mail, procedure.TrackId, 
                                                                        Properties.Settings.Default.FLOWPaymentMethod,
                                                                        Properties.Settings.Default.FLOWUrlToConfirm,
                                                                        Properties.Settings.Default.FLOWUrlToRedirect, 
                                                                        out strresponse, out response);
                            if (retF == 0)
                            {
                                LOG.Debug("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure - Procesando respuesta...");
                                ResponseCreateOrderPaymentFlow respG = (ResponseCreateOrderPaymentFlow)response;
                                if (respG == null)
                                {
                                    LOG.Debug("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure - Error parseando respuesta");
                                    ret = Errors.IRET_ERR_DESERIALIZE_RESPONSE;
                                } else
                                {
                                    LOG.Debug("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure - Update en BD...");
                                    procedure.PaymentStatus = 1; //Iniciado el payment => Create Order ok
                                    procedure.PaymentInfo = strresponse;
                                    procedure.PaymentOrder = respG.flowOrder.ToString();
                                    procedure.PaymentToken = respG.token;
                                    procedure.PaymentUrl = respG.url;
                                    LOG.Debug("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure - Updating => " +
                                        "ProcedureId=" + procedure.Id + " - PaymentOrder=" + respG.flowOrder.ToString() + 
                                        " - PaymentToken=" + respG.token + " - PaymentUrl=" + respG.url);
                                    using (ISession session = PersistentManager.session())
                                    {
                                        session.SaveOrUpdate(procedure);
                                        session.Flush();
                                    }
                                    LOG.Debug("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure - Updated OK!");
                                    ret = Errors.IRET_OK;
                                }
                            } else
                            {
                                LOG.Debug("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure - Error creando orden de pago [" + retF + "]");
                                ret = Errors.IRET_ERR_CREATING_PAYMENT_ORDER;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure OUT!");
            return ret;
        }

        /// <summary>
        /// Dado un tramite con orden de pago creada, consulta y si ya se pago se actualiza los datos
        /// </summary>
        /// <param name="procedure"></param>
        /// <param name="trackid"></param>
        /// <returns></returns>
        internal static int UpdatePaymentOrderForProcedure(NvUsersProcedures procedure, string trackid)
        {
            int ret = Errors.IRET_OK;
            NvUsersProcedures _procedure = procedure;
            string msgerr;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure IN...");
                if (_procedure == null)
                {
                    LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure - Recupera procedure de BD con TrackId = " + trackid);
                    _procedure = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr);
                }

                if (_procedure == null)
                {
                    ret = Errors.IRET_ERR_USER_PROCEDURE_NOT_FOUND;
                    LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure - Procedure no disponible con TrackId = " + trackid);
                }
                else
                {
                    //Si nos 1 => Devuelvo que no peudo chequear status
                    if (procedure.PaymentStatus != 1)
                    {
                        LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure - Procedure con status diferente a 1!" +
                                  " PaymentStatus=" + procedure.PaymentStatus.ToString()); ;
                        ret = Errors.IRET_ERR_STATUS_PAYMENT_INCORRECT;
                    }
                    else
                    {
                        object response;
                        NvUsers user = AdministradorNvUsers.BuscarNvUserById(procedure.IdUser, true);
                        string _mail = (user != null) ? user.Mail : null;
                        if (string.IsNullOrEmpty(_mail))
                        {
                            LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure - Usuario con id = " +
                                procedure.IdUser + " no disponible o con mail no verificado");
                            ret = Errors.IRET_ERR_MAIL_VALIDATED;
                        }
                        else
                        {

                            string strresponse;
                            int retF = Global.FLOW_API.GetStatusOrderPayment(procedure.PaymentToken, procedure.PaymentOrder,
                                                                             out strresponse, out response);
                            if (retF == 0)
                            {
                                LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure - Procesando respuesta...");
                                ResponseStatusOrderPaymentFlow respG = (ResponseStatusOrderPaymentFlow)response;
                                if (respG == null)
                                {
                                    LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure - Error parseando respuesta");
                                    ret = Errors.IRET_ERR_DESERIALIZE_RESPONSE;
                                }
                                else
                                {
                                    if (respG.status == 2)
                                    {
                                        LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure - Update en BD...");
                                        //procedure.Status = StatusNvUsersProcedures.STATUS_READY_TO_NOTARIZE;
                                        procedure.Status = StatusNvUsersProcedures.STATUS_READY_TO_REVISION_NOTARIZE;
                                        procedure.PaymentStatus = 2; //Iniciado el payment => Create Order ok
                                        procedure.PaymentInfo = strresponse;
                                        procedure.PaymentAmount = (respG.paymentData!=null)?respG.paymentData.balance:0;
                                        procedure.PaymentMedia = (respG.paymentData != null) ? respG.paymentData.media : null;
                                        procedure.PaymentDate = (respG.paymentData != null && !string.IsNullOrEmpty(respG.paymentData.date)) ?
                                                                 respG.paymentData.date : null;
                                        LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure - Updating => " +
                                            "ProcedureId=" + procedure.Id + " - PaymentAmount=" + procedure.PaymentAmount.ToString());
                                        using (ISession session = PersistentManager.session())
                                        {
                                            session.SaveOrUpdate(procedure);
                                            session.Flush();
                                        }
                                        LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure - Updated OK!");

                                        //Informo x mail
                                        string[] strbody = new string[2];
                                        strbody[0] = "Usted complet� el pago del tr�mite <b>" + procedure.NameProcedure + "</b>";
                                        strbody[1] = "El codigo unico del tramite para consultas es: <b>" + procedure.TrackId + "</b>";
                                        libs.Utils.NVNotify(AdministradorNvUsers.BuscarNvUserById(procedure.IdUser).Mail,
                                                            "Pago realizado con exito", strbody, null, null, null);

                                        //Si el pago fue correcto => Mando ya a notarizar
                                        ret = AdministradorNvUsersProcedures.SendProcedureToNotarize(procedure);

                                        //ret = Errors.IRET_OK;
                                    } else
                                    {
                                        LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure - Status informado de orden de pago = " + 
                                            respG.status.ToString());
                                        ret = Errors.IRET_ERR_STATUS_PAYMENT_INCORRECT;
                                    }
                                }
                            }
                            else
                            {
                                LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure - Error chequeando status orden de pago [" + retF + "]");
                                ret = Errors.IRET_ERR_CONSULTING_STATUS_PAYMENT_ORDER;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure OUT!");
            return ret;
        }


        /// <summary>
        /// Si el procedure tiene status pagado => Se puede enviar a notarizar, y actualizar la BD
        /// </summary>
        /// <param name="procedure"></param>
        /// <returns></returns>
        public static int SendProcedureToNotarize(NvUsersProcedures procedure)
        {
            int ret = Errors.IRET_OK;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.SendProcedureToNotarize IN...");

                //1.- Si viene != null y status esta correcto sigue
                if (procedure == null || procedure.Status != StatusNvUsersProcedures.STATUS_READY_TO_NOTARIZE)
                {
                    LOG.Debug("AdministradorNvUsersProcedures.SendProcedureToNotarize - Procedure Status Incorrecto => " +
                                                    ((procedure == null)?"NULL": procedure.Status.ToString()));
                }

                //2) De acuerdo a lo seleccionado se envia a notarizar al notario seleccionado por el provider adecuado
                string _UrlBase = AdministradorNvConfig.GetUrlBase(procedure.IdProvider); //Hacer primero HashTable de providers para acceder rapido
                LOG.Debug("AdministradorNvUsersProcedures.SendProcedureToNotarize - UrlBase Provider = " + _UrlBase);
                List <rest.Evidence> evidences = 
                    AdministradorNvConfig.GenerateListEvidences(
                        AdministradorNvUsersProceduresEvidences.GetListNvUserProcedureEvidenceByIdProcedure(procedure.Id));
                LOG.Debug("AdministradorNvUsersProcedures.SendProcedureToNotarize - Evidencias recuperadas desde BD = " +
                                    ((evidences != null) ? evidences.Count : 0));
                //string[] listFiles = new string[evidences.Count + 1];
                //listFiles[0] = paramIn.document;
                //listFiles[1] = ExtractCIPdf(evidences[0]);
                //string _DocumentWithEvidences = DEHelper.MergeFiles(listFiles);
                LOG.Debug("AdministradorNvUsersProcedures.SendProcedureToNotarize - Sending to notarize...");
                NotarizeProviderResponsetModel response = NvProviderHelper.Notarize(_UrlBase, procedure.TrackId, procedure.IdProcedure,
                                                                        procedure.DocumentB64, evidences);
                LOG.Debug("AdministradorNvUsersProcedures.SendProcedureToNotarize - response = " + 
                                        ((response!=null)?response.code.ToString():"NULL"));
                //5) Se procesa respuesta
                //   5.1) Actualiza NvUsersProcedures con status recibido
                if (response != null && response.code == 200)
                {
                    LOG.Debug("AdministradorNvUsersProcedures.SendProcedureToNotarize - Sending actualize status a Notarizing...");
                    ret = AdministradorNvUsersProcedures.SetNvUserProcedureNotarizing(procedure.TrackId);
                    if (ret < 0)
                    {
                        LOG.Debug("AdministradorNvUsersProcedures.SendProcedureToNotarize - Error actualizando estado de tramite [" + ret + "]");
                    } else
                    {
                        LOG.Debug("AdministradorNvUsersProcedures.SendProcedureToNotarize - Tramite actualizado a estado NOTARIZING!");

                        //Informo x mail
                        string[] strbody = new string[2];
                        strbody[0] = "El tramite <b>" + procedure.NameProcedure + "</b> fue enviado a notarizar con exito. " +
                            "Cuando este notarizado se le enviara el tramite a su mail.";
                        strbody[1] = "El codigo unico del tramite para consultas es: <b>" + procedure.TrackId + "</b>";
                        libs.Utils.NVNotify(AdministradorNvUsers.BuscarNvUserById(procedure.IdUser).Mail,
                                            "Tramite enviado a notarizar", strbody, null, null, null);
                    }
                }
                else
                {
                    ret = Errors.IRET_ERR_INFORMED_PROVIDER_SERVICE;
                    LOG.Error("AdministradorNvUsersProcedures.SendProcedureToNotarize - Error informado por el provider en proceso de notarizacion.");
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.SendProcedureToNotarize - Error: " + ex.Message);
            }
            return Errors.IRET_OK;
        }

        #endregion Payment Handler

        #region v2021

        internal static int GetNvUsersProcedureByFilter(int notaryid, int userid, int procedureid, DateTime from, DateTime till,
                                        string trackid, int status, out string msgerr, out List<NvUsersProcedures> listProcedures,
                                        bool withChild = false)
        {
            int ret = Errors.IRET_OK;
            msgerr = null;
            listProcedures = null;
            int iduser = 0;
            NvUsers _User = null;
            try
            {
                LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter IN...");

                if (userid > 0)
                {

                    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Busco iduser  = " + userid.ToString());
                    _User = AdministradorNvUsers.BuscarNvUserById(userid);
                    //if (_User != null)
                    //{

                    //} else
                    if (_User == null)
                    {
                        msgerr = "El User no existe!";
                        return Errors.IRET_ERR_USER_NOT_EXIST * -1;
                    }
                }
                //else
                //{
                //    msgerr = "El IdUser debe ser informado para la consulta!";
                //    return Errors.IRET_API_BAD_REQUEST * -1;
                //}

                using (ISession sess = PersistentManager.session())
                {
                    ICriteria crit = sess.CreateCriteria(typeof(NvUsersProcedures));

                    if (userid > 0)
                    {
                        crit.Add(Restrictions.Eq("IdUser", userid));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set IdUser = " + userid);
                    }

                    if (notaryid > 0)
                    {
                        crit.Add(Restrictions.Eq("IdNotary", notaryid));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set IdNotary = " + notaryid);
                    }

                    if (procedureid > 0)
                    {
                        crit.Add(Restrictions.Eq("IdProcedure", procedureid));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set IdProcedure = " + procedureid);
                    }
                    

                    if (!string.IsNullOrEmpty(trackid) && trackid.Trim().Length > 0)
                    {
                        crit.Add(Expression.Eq("TrackId", trackid));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set TrackId = " + trackid);
                    }

                    if (from != null && till != null &&
                        string.IsNullOrEmpty(trackid)) //Solo agrego filtro de fecha si no viene trackid que es unico
                    {
                        //crit.Add(Expression.Ge("DateInit", from));
                        crit.Add(Expression.Between("DateInit", from, till));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set Desde = " +
                                    from.ToString("dd/MM/yyyy HH:mm:ss") + " - Hasta = " + till.ToString("dd/MM/yyyy HH:mm:ss"));
                    }

                    //if (till != null)
                    //{
                    //    crit.Add(Expression.Le("DateInit", till));
                    //    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set Hasta = " +
                    //                till.ToString("dd/MM/yyyy HH:mm:ss"));
                    //}

                    //Si es negativo agrego filtro de que status sea > 0 porque dejamos Status = -1 para una baja logica (CANCELED)
                    if (status >= 0)
                    {
                        crit.Add(Expression.Eq("Status", status));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set Status = " + status);
                    } else
                    {
                        crit.Add(Expression.Ge("Status", 0));
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Set Status > 0! (todos)");

                    }

                    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - IN get procedures con filtro...");
                    listProcedures = (List<NvUsersProcedures>)crit.AddOrder(Order.Desc("Id")).List<NvUsersProcedures>();
                    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilte - listProcedures.len = " +
                               listProcedures.Count.ToString());

                    if (withChild && listProcedures != null)
                    {
                        LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Entra a carga de detalles de Eviudencias y Signatories...");
                        foreach (NvUsersProcedures item in listProcedures)
                        {
                            NHibernateUtil.Initialize(item.NvUsersProceduresEvidences);
                            NHibernateUtil.Initialize(item.NvUsersProceduresSignatory);
                        }
                    }
                    //listProcedures = crit.List();
                }

                if (!withChild)
                {
                    LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - Lim�a detalles...");
                    foreach (NvUsersProcedures item in listProcedures)
                    {
                        item.NvUsersProceduresEvidences = null;
                        item.NvUsersProceduresSignatory = null;
                    }
                }
            }
            catch (Exception ex)
            {
                listProcedures = null;
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter - OUT! - Len Ret = " +
                            ((listProcedures == null) ? "Null" : listProcedures.Count.ToString()));
            return ret;
        }


        //internal static int UpdateInfoSignatoryInBD(NvUsersProcedures procedureIn, string sRdEnvelope, Hashtable htRD, out string trackidci, out NvUsersProcedures nvup)
        //{
        //    int ret = Errors.IRET_OK;
        //    trackidci = null;
        //    nvup = null;
        //    try
        //    {
        //        LOG.Debug("AdministradorNvUsersProcedures.UpdateInfoSignatoryInBD IN...");

        //        if (htRD != null && htRD.Count > 0 && !string.IsNullOrEmpty(sRdEnvelope))
        //        {
        //            NvUsersProceduresEvidences NvUPE =
        //                AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByTrackIdCI((string)htRD["TrackIdCI"], true);

        //            if (NvUPE != null)
        //            {
        //                NvUPE.Status = 1;
        //                NvUPE.Value = sRdEnvelope;
        //                ret = AdministradorNvUsersProceduresEvidences.UpdateNvUserProcedureEvidence(NvUPE);

        //                if (ret < 0)
        //                {
        //                    LOG.Warn("AdministradorNvUsersProcedures.UpdateInfoSignatoryInBD Error: actualizando evidencia TrackIdCI = " + trackidci);
        //                }
        //                else
        //                {
        //                    ret = AdministradorNvUsersProcedures.UpdateDocumentWithCI(NvUPE.NvUsersProcedures, (string)htRD["PDFCICertify"], out nvup);
        //                }
        //            }
        //            else
        //            {
        //                LOG.Warn("AdministradorNvUsersProcedures.UpdateInfoSignatoryInBD Error: No se encuentra registrada la CI inidicada [" +
        //                    trackidci + "]");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = Errors.IRET_ERR_DESCONOCIDO;
        //        LOG.Error("AdministradorNvUsersProcedures.UpdateInfoSignatoryInBD Error: " + ex.Message);
        //    }
        //    LOG.Debug("AdministradorNvUsersProcedures.UpdateInfoSignatoryInBD OUT!");
        //    return ret;
        //}


        #endregion v2021
    }

    public class StatusNvUsersProcedures
    {
        internal static int STATUS_CANCELLED = -1;
        //PENDIENTE DE VERIFICACION DE CORREO, PENDIENTE DE CERTIFICACION DE IDENTIDAD, PENDIENTE DE PAGO, EN TRAMITE/PROCESO, NOTARIZADA, RECHAZADA
        internal static int STATUS_MAIL_VERIFY_PENDING = 0;
        internal static int STATUS_CI_PENDING = 1;
        internal static int STATUS_PAYMENT_PENDING = 2;
        internal static int STATUS_READY_TO_REVISION_NOTARIZE = 3;
        internal static int STATUS_READY_TO_NOTARIZE = 4;
        internal static int STATUS_IN_PROCESS_NOTARIZING = 5;
        internal static int STATUS_REJECTED = 6;
        internal static int STATUS_NOTARIZED = 7;

    }

    
}
