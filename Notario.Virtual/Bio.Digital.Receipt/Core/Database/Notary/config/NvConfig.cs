﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;

namespace Bio.Digital.Receipt.Core.Database.Notary.config
{
    public class NvConfig
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NvConfig));

        public NvConfig() { }
        public List<NvProvider> NvProviders { get; set; }

        /// <summary>
        /// Contendrá las notarias para accso rapido, donde la clave de acceso es:
        ///    [providerid-notaryid]
        /// </summary>
        internal Hashtable ht_NOTARIES;

        internal bool Initialize()
        {
            bool ret = false;
            try
            {
                LOG.Debug("NvConfig.Initialize IN...");
                ht_NOTARIES = new Hashtable();
                foreach (NvProvider item in NvProviders)
                {
                    foreach (NvNotary item2 in item.Notarias)
                    {
                        NvNotaryHT newN = new NvNotaryHT(item2, item.Id, item.Name, item.UrlBase);
                        ht_NOTARIES.Add(item.Id + "-" + item2.Id, newN);
                        LOG.Debug("NvConfig.Initialize - Cargada => " + item.Id + "-" + item2.Id + " = " + item2.Name + "...");
                    }
                }
                ret = true;
                LOG.Debug("NvConfig.Initialize - Inicializado OK - Cantidad de Notarias cargadas = " + ht_NOTARIES.Count);
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("NvConfig.Initialize Error - " + ex.Message);
            }
            LOG.Debug("NvConfig.Initialize OUT!");
            return ret;
        }
    }

    public class NvProvider
    {
        public NvProvider() { }
        public int Id { get; set; }
        public string Name { get; set; }
        public string UrlBase { get; set; }
        public List<NvNotary> Notarias { get; set; }

    }


    public class NvNotaryHT
    {
        NvNotaryHT() { }

        public NvNotaryHT(NvNotary _NvNotary, int _Id, string _Name, string _UrlBase)
        {
            oNvNotary = _NvNotary;
            IdProvider = _Id;
            NameProvider = _Name;
            UrlBaseProvider = _UrlBase;
        }

        public NvNotary oNvNotary { get; set; }
        public int IdProvider { get; set; }
        public string NameProvider { get; set; }
        public string UrlBaseProvider { get; set; }
    }
}