using System;
using System.Text;
using System.Collections.Generic;


namespace Bio.Digital.Receipt.Core.Database.Notary.config {

    [Serializable]
    public class NvNotary {
        public NvNotary() { }
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Rut { get; set; }
        public virtual string Notaryname { get; set; }
        public virtual string Notarynamealternate { get; set; }
        public virtual string Phone { get; set; }
        public virtual DateTime Dateinit { get; set; }
        public virtual DateTime? Enddate { get; set; }
        public virtual int Status { get; set; }

        public List<NvNotaryProcedure> NvNotaryProcedures { get; set; }
        //public List<NvUsersProcedures> NvUsersProcedures { get; set; }
        
    }
}
