using System;
using System.Collections.Generic;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.rest;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using NHibernate;
//using NHibernate.Expression;

namespace Bio.Digital.Receipt.Core.Database.Notary.config
{

    /// <summary>
    /// 
    /// </summary>
    public class AdministradorNvConfig
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdministradorNvConfig));

        internal static int GetProvider(int notaryId)
        {
            int ret = Errors.IRET_OK;
            try
            {
                LOG.Debug("AdministradorNvConfig.GetProvider IN");
                if (Global.NV_CONFIG == null) {
                    LOG.Debug("AdministradorNvConfig.GetProvider  Global.NV_CONFIG == null");
                    ret = Errors.IRET_ERR_NVCONFIG_NO_CONFIGURED;
                } else
                {
                    foreach (NvProvider item in Global.NV_CONFIG.NvProviders)
                    {
                        foreach (NvNotary item2 in item.Notarias)
                        {
                            if (item2.Id == notaryId)
                            {
                                ret = item.Id;
                                LOG.Debug("AdministradorNvConfig.GetProvider ProviderId retornado = " + ret);
                                break;
                            }
                        }
                    }
                    if (ret == 0) LOG.Debug("AdministradorNvConfig.GetProvider No Encotnro Provider!");
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvConfig.GetProvider Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvConfig.GetProvider OUT!");
            return ret;
        }

        internal static double GetPrice(int notaryId, int idNotaryProcedure)
        {
            double ret = 0;
            try
            {
                LOG.Debug("AdministradorNvConfig.GetPrice IN");
                if (Global.NV_CONFIG == null)
                {
                    LOG.Debug("AdministradorNvConfig.GetPrice Global.NV_CONFIG == null");
                    ret = Errors.IRET_ERR_NVCONFIG_NO_CONFIGURED;
                }
                else
                {
                    foreach (NvProvider item in Global.NV_CONFIG.NvProviders)
                    {
                        foreach (NvNotary item2 in item.Notarias)
                        {
                            if (item2.Id == notaryId)
                            {
                                foreach (NvNotaryProcedure item3 in item2.NvNotaryProcedures)
                                {
                                    if (item3.Id == idNotaryProcedure)
                                    {
                                        ret = item3.Price;
                                        LOG.Debug("AdministradorNvConfig.GetPrice - Price retornado = " + ret);
                                        break;
                                    }
                                }
                                
                            }
                        }
                    }
                    if (ret == 0) LOG.Debug("AdministradorNvConfig.GetPrice No Encotnro Price!");
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("AdministradorNvConfig.GetPrice Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvConfig.GetPrice OUT!");
            return ret;
        }

        /// <summary>
        /// Retorna los tramites disponibles con notaria asociada
        /// </summary>
        /// <returns></returns>
        internal static List<NvProceduresAvailablesModel> GetNvProceduresAvailables()
        {
            List<NvProceduresAvailablesModel> listPA = new List<NvProceduresAvailablesModel>();
            NvProceduresAvailablesModel pAV;
            NvProceduresAvailablesEvidencesModel pAVE;
            try
            {
                LOG.Debug("AdministradorNvConfig.GetNvProceduresAvailables IN");
                if (Global.NV_CONFIG == null)
                {
                    LOG.Debug("AdministradorNvConfig.GetNvProceduresAvailables Global.NV_CONFIG == null");
                    listPA = null;
                }
                else
                {
                    LOG.Debug("AdministradorNvConfig.GetNvProceduresAvailables Comienza proceso de Global.NV_CONFIG.NvProviders...");
                    foreach (NvProvider item in Global.NV_CONFIG.NvProviders)
                    {
                        foreach (NvNotary item2 in item.Notarias)
                        {
                            foreach (NvNotaryProcedure item3 in item2.NvNotaryProcedures)
                            {
                                pAV = new NvProceduresAvailablesModel();
                                pAV.notaryid = item2.Id;
                                pAV.namenotary = item2.Notaryname;
                                pAV.rutnotary = item2.Rut;
                                pAV.procedureid = item3.Id;
                                pAV.nameprocedure = item3.Name;
                                pAV.priceprocedure = item3.Price;
                                if (item3.NvNotaryProcedureEvidences != null &&
                                    item3.NvNotaryProcedureEvidences.Count > 0)
                                {
                                    pAV.evidences = new List<NvProceduresAvailablesEvidencesModel>();
                                    foreach (NvNotaryProcedureEvidence item4 in item3.NvNotaryProcedureEvidences)
                                    {
                                        pAVE = new NvProceduresAvailablesEvidencesModel();
                                        pAVE.id = item4.Id;
                                        pAVE.name = item4.Name;
                                        pAVE.type = item4.Type;
                                        pAV.evidences.Add(pAVE);
                                    }
                                } else
                                {
                                    pAV.evidences = null;
                                }
                                LOG.Debug("AdministradorNvConfig.GetNvProceduresAvailables - Procedure cargado => " + 
                                    " Notary=" + pAV.namenotary + " - RutNotary=" + pAV.rutnotary + 
                                    " Procedure=" + pAV.nameprocedure + " - Q Evidencias = " +
                                    ((pAV.evidences == null || pAV.evidences.Count ==0)?"0": pAV.evidences.Count.ToString()));
                                listPA.Add(pAV);
                            }
                        }
                    }
                    if (listPA == null) LOG.Debug("AdministradorNvConfig.GetNvProceduresAvailables No Encontro Tramites Disponibles!");
                }
            }
            catch (Exception ex)
            {
                listPA = null;
                LOG.Error("AdministradorNvConfig.GetNvProceduresAvailables Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvConfig.GetNvProceduresAvailables OUT!");
            return listPA;
        }

        internal static string GetNameProcedure(int notaryId, int procedureId)
        {
            string ret = "Tramite ID= " + procedureId;
            try
            {
                LOG.Debug("AdministradorNvConfig.GetNameProcedure IN");
                if (Global.NV_CONFIG == null)
                {
                    LOG.Debug("AdministradorNvConfig.GetNameProcedure Global.NV_CONFIG == null");
                    ret = null;
                }
                else
                {
                    foreach (NvProvider item in Global.NV_CONFIG.NvProviders)
                    {
                        foreach (NvNotary item2 in item.Notarias)
                        {
                            if (item2.Id == notaryId)
                            {
                                foreach (NvNotaryProcedure item3 in item2.NvNotaryProcedures)
                                {
                                    if (item3.Id == procedureId)
                                    {
                                        ret = item3.Name;
                                        LOG.Debug("AdministradorNvConfig.GetNameProcedure - Name Procedure retornado = " + ret);
                                        break;
                                    }
                                }

                            }
                        }
                    }
                    if (string.IsNullOrEmpty(ret)) LOG.Debug("AdministradorNvConfig.GetNameProcedure No Encotnro Nombre Procedure!");
                }
            }
            catch (Exception ex)
            {
                ret = "Tramite ID= " + procedureId;
                LOG.Error("AdministradorNvConfig.GetNameProcedure Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvConfig.GetNameProcedure OUT!");
            return ret;
        }

        internal static string GetUrlBase(int providerid)
        {
            string ret = null;
            try
            {
                LOG.Debug("AdministradorNvConfig.GetUrlBase IN");
                if (Global.NV_CONFIG == null)
                {
                    LOG.Debug("AdministradorNvConfig.GetUrlBase  Global.NV_CONFIG == null");
                    ret = null;
                }
                else
                {
                    LOG.Debug("AdministradorNvConfig.GetUrlBase Buscando URL Base de ProviderId = " + providerid);
                    foreach (NvProvider item in Global.NV_CONFIG.NvProviders)
                    {
                        if (item.Id == providerid)
                        {
                            ret = item.UrlBase;
                            LOG.Debug("AdministradorNvConfig.GetUrlBase URL Base de ProviderId " + providerid + 
                                "retornado = " + ret);
                            break;
                        }
                    }
                    if (string.IsNullOrEmpty(ret)) LOG.Warn("AdministradorNvConfig.GetUrlBase No Encotnro URLBase del Provider!");
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("AdministradorNvConfig.GetUrlBase Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvConfig.GetUrlBase OUT!");
            return ret;
        }

        internal static List<rest.Evidence> GenerateListEvidences(List<rest.Models.Evidence> evidences)
        {
            List<rest.Evidence> lEv = new List<rest.Evidence>();
            try
            {
                LOG.Debug("AdministradorNvConfig.GenerateListEvidences - IN..."); 
                if (evidences == null)
                {
                    LOG.Debug("AdministradorNvConfig.GenerateListEvidences - Evidencias ingresadas nulas...");
                    return null;
                } else
                {
                    rest.Evidence lEvItem;
                    LOG.Debug("AdministradorNvConfig.GenerateListEvidences - Cargando evidencias...");
                    foreach (rest.Models.Evidence item in evidences)
                    {
                        lEvItem = new rest.Evidence();
                        lEvItem.name = item.name;
                        lEvItem.type = item.type;
                        lEvItem.value = item.value;
                        lEv.Add(lEvItem);
                        LOG.Debug("AdministradorNvConfig.GenerateListEvidences - Evidancia Agregada => Name=" + 
                            item.name + "-type=" + item.type);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdministradorNvConfig.GenerateListEvidences Error: " + ex.Message);
                lEv = new List<rest.Evidence>();
            }
            LOG.Debug("AdministradorNvConfig.GenerateListEvidences - OUT!");
            return lEv;
        }

        /// <summary>
        /// Formatea lista de evidencias desde lista de evidencias recuperadas desde BD
        /// </summary>
        /// <param name="evidences"></param>
        /// <returns></returns>
        internal static List<rest.Evidence> GenerateListEvidences(List<NvUsersProceduresEvidences> evidences)
        {
            List<rest.Evidence> lEv = new List<rest.Evidence>();
            try
            {
                LOG.Debug("AdministradorNvConfig.GenerateListEvidences - IN...");
                if (evidences == null)
                {
                    LOG.Debug("AdministradorNvConfig.GenerateListEvidences - Evidencias ingresadas nulas...");
                    return null;
                }
                else
                {
                    rest.Evidence lEvItem;
                    LOG.Debug("AdministradorNvConfig.GenerateListEvidences - Cargando evidencias...");
                    foreach (NvUsersProceduresEvidences item in evidences)
                    {
                        lEvItem = new rest.Evidence();
                        lEvItem.name = item.Name;
                        lEvItem.type = item.Type;
                        lEvItem.value = item.Value;
                        lEv.Add(lEvItem);
                        LOG.Debug("AdministradorNvConfig.GenerateListEvidences - Evidancia Agregada => Name=" +
                            item.Name + "-type=" + item.Type);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("AdministradorNvConfig.GenerateListEvidences Error: " + ex.Message);
                lEv = new List<rest.Evidence>();
            }
            LOG.Debug("AdministradorNvConfig.GenerateListEvidences - OUT!");
            return lEv;
        }

        internal static string GetNameProvider(int idProvider, int idNotary)
        {
            string ret = null;
            try
            {
                LOG.Debug("AdministradorNvConfig.GetNameProvider IN");
                ret = ((NvNotaryHT)(Global.NV_CONFIG.ht_NOTARIES[idProvider.ToString() + "-" + idNotary.ToString()])).NameProvider;
                //if (Global.NV_CONFIG == null)
                //{
                //    LOG.Debug("AdministradorNvConfig.GetProvider Global.NV_CONFIG == null");
                //    ret = "No Configurado";
                //}
                //else
                //{
                //    foreach (NvProvider item in Global.NV_CONFIG.NvProviders)
                //    {
                //        foreach (NvNotary item2 in item.Notarias)
                //        {
                //            if (item2.Id == notaryId)
                //            {
                //                ret = item.Name;
                //                LOG.Debug("AdministradorNvConfig.GetProvider ProviderId retornado = " + ret);
                //                break;
                //            }
                //        }
                //    }
                //    if (ret == null) LOG.Debug("AdministradorNvConfig.GetProvider No Encotnro Provider!");
                //}
            }
            catch (Exception ex)
            {
                ret = "No Encontrado [Err]";
                LOG.Error("AdministradorNvConfig.GetProvider Error: " + ex.Message);
            }
            LOG.Debug("AdministradorNvConfig.GetProvider OUT!");
            return ret;
        }

        internal static string GetNameNotary(int idProvider, int idNotary)
        {
            try
            {
                return ((NvNotaryHT)(Global.NV_CONFIG.ht_NOTARIES[idProvider.ToString() + "-" + idNotary.ToString()])).oNvNotary.Name;
            }
            catch (Exception ex)
            {
                LOG.Error("AdministradorNvConfig.GetNameNotary Error: " + ex.Message);
                return "No Encontrado [Err]";
            }
        }

        //public static Company BuscarCompanyByRut(string rut)
        //{
        //    Company rec = null;

        //    try
        //    {
        //        using (ISession session = PersistentManager.session())
        //        {
        //            rec = PersistentManager.session().CreateCriteria(typeof(Company))
        //                             .Add(NHibernate.Criterion.Expression.Eq("Rut", rut))
        //                             .UniqueResult<Company>();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("AdministradorCompany.BuscarCompanyByRut", ex);
        //    }

        //    return rec;
        //}

        //public static Company BuscarCompanyById(int id)
        //{
        //    Company rec = null;

        //    try
        //    {
        //        using (ISession session = PersistentManager.session())
        //        {
        //            rec = PersistentManager.session().CreateCriteria(typeof(Company))
        //                             .Add(NHibernate.Criterion.Expression.Eq("Id", id))
        //                             .UniqueResult<Company>();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("AdministradorCompany.BuscarCompanyById", ex);
        //    }

        //    return rec;
        //}


    }


    //Clases de Retorno para API Rest
    public class NvProceduresAvailablesModel
    {
        public NvProceduresAvailablesModel() { }

        public int notaryid;
        public string rutnotary;
        public string namenotary;
        public int procedureid;
        public string nameprocedure;
        public double priceprocedure;
        public List<NvProceduresAvailablesEvidencesModel> evidences;
    }

    public class NvProceduresAvailablesEvidencesModel
    {
        public NvProceduresAvailablesEvidencesModel() { }
        public NvProceduresAvailablesEvidencesModel(int _id, string _name, int _type) {
            id = _id;
            name = _name;
            type = _type;
        }
        public int id;
        public string name;
        public int type;
    }
}
