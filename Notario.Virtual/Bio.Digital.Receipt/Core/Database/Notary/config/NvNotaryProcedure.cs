using System;
using System.Text;
using System.Collections.Generic;


namespace Bio.Digital.Receipt.Core.Database.Notary.config
{
    
    public class NvNotaryProcedure {
        public NvNotaryProcedure() { }
        public virtual int Id { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
        public virtual DateTime Dateinit { get; set; }
        public virtual int Enabled { get; set; }

        public List<NvNotaryProcedureEvidence> NvNotaryProcedureEvidences { get; set; }
    }
}
