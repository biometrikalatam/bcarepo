﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.Core.Database.Notary.config
{
    public class NvNotaryProcedureEvidence
    {
        public NvNotaryProcedureEvidence() { }
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int Type { get; set; }
    }
}