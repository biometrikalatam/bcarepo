using System;
using System.Text;
using System.Collections.Generic;
using Bio.Digital.Receipt.rest.Models;
using Newtonsoft.Json;

namespace Bio.Digital.Receipt.Core.Database {

    [Serializable]
    public class NvUsersProcedures
    {

        public NvUsersProcedures() { }


        protected DateTime? _DateFinish;

        public virtual int Id { get; set; }
        //public virtual NvNotaryProcedures NvNotaryProcedures { get; set; }
        //public virtual NvNotaries NvNotaries { get; set; }
        public virtual int IdProvider { get; set; }
        public virtual int IdNotary { get; set; }
        public virtual int IdNotaryProcedure { get; set; }
        public virtual string NameProcedure { get; set; }
        public virtual int IdProcedure { get; set; }
        //public virtual User User { get; set; }
        public virtual int IdUser { get; set; }
        public virtual string TrackId { get; set; }
        //public virtual int IdProvider { get; set; }
        public virtual string DocumentB64Original { get; set; }
        public virtual string DocumentB64 { get; set; }
        public virtual string DocumentB64Notarized { get; set; }
        
        public virtual DateTime DateInit { get; set; }

        /// <summary>
        /// estados posibles:
        ///     internal static int STATUS_CANCELED = -1;
        ///     internal static int STATUS_MAIL_VERIFY_PENDING = 0;
        ///     internal static int STATUS_CI_PENDING = 1;
        ///     internal static int STATUS_PAYMENT_PENDING = 2;
        ///     internal static int STATUS_READY_TO_REVISION_NOTARIZE = 3;
        ///     internal static int STATUS_READY_TO_NOTARIZE = 4;
        ///     internal static int STATUS_IN_PROCESS_NOTARIZING = 5;
        ///     internal static int STATUS_REJECTED = 6;
        ///     internal static int STATUS_NOTARIZED = 7;
        /// </summary>
        public virtual int Status { get; set; }
        
        public virtual double Price { get; set; }
        
        public virtual string UrlValidator { get; set; }
        public virtual string NotaryIdValidator { get; set; }
        public virtual string Message { get; set; }

        public virtual int StatusNotary { get; set; }
        public virtual string StatusMsgNotary { get; set; }

        public virtual int PaymentStatus { get; set; }
        public virtual string PaymentOrder { get; set; }
        public virtual string PaymentUrl { get; set; }
        public virtual string PaymentToken { get; set; }
        public virtual string PaymentInfo { get; set; }
        public virtual double PaymentAmount { get; set; }
        public virtual string PaymentMedia { get; set; }
        public virtual string PaymentDate { get; set; }

        public virtual int StatusRevisionNotary { get; set; }
        public virtual int UserRevisionNotary { get; set; }
        public virtual string TextToInclude { get; set; }
        public virtual string ReasonReject { get; set; }

        public virtual DateTime? DateFinish
        {
            get { return _DateFinish; }
            set { if (value != this._DateFinish) { _DateFinish = value; } }
        }


        /// <summary>
        /// 
        /// </summary>
        protected IList<NvUsersProceduresEvidences> _NvUsersProceduresEvidences;
        /// <summary>
        /// 
        /// </summary>
        public virtual IList<NvUsersProceduresEvidences> NvUsersProceduresEvidences
        {
            get { return _NvUsersProceduresEvidences; }
            set { _NvUsersProceduresEvidences = value; }
        }

        protected IList<NvUsersProceduresSignatory> _NvUsersProceduresSignatory;
        /// <summary>
        /// 
        /// </summary>
        public virtual IList<NvUsersProceduresSignatory> NvUsersProceduresSignatory
        {
            get { return _NvUsersProceduresSignatory; }
            set { _NvUsersProceduresSignatory = value; }
        }

        //public IList<NvUsersProceduresEvidences> NvUsersProceduresEvidences { get; set; }
    }
}
