/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Digital.Receipt.Core.Database
{
    /// <summary>
    /// RdAplicaciones object for NHibernate mapped table 'rd_aplicaciones'.
    /// </summary>
    [Serializable]
    public class CompanyWorkflow : INotifyPropertyChanged
    {
        #region Member Variables
        protected int _id;
        protected Company _companyid;
        protected string _name;
        protected int _onboardingmandatory;
        protected string _bpwebtypeonboarding;
        protected string _bpwebtypeverify;
        protected string _receiverlist;
        protected double _threshold;
        protected DateTime _datecreate;
        protected DateTime _dateupdate;
        protected int _autorization;
        protected string _autorizationmessage;
        protected int _signerinclude;
        protected int _checklocksrcei;
        protected int _videoinclude;
        protected string _videomessage;
        protected string _theme;
        protected int _checkadult;
        protected int _checkexpired;
        protected int _georefmandatory;
        protected string _redirecturl;
        protected string _successurl;
        //protected string _workflow; //Json con el workflow a pedir

        protected int _carregisterinclude; // 0-No o 1-Si
        protected int _writinginclude; // 0-No o 1-Si
        protected string _forminclude; // Null=No incluhye form o "stringid"=Nombre el form (para futuro que ser� definible, ahora solo usa el form fijo)


        public virtual event PropertyChangedEventHandler PropertyChanged;

        #endregion
        #region Constructors

        public CompanyWorkflow() { }

        public CompanyWorkflow(Company companyid, string name, string workflow)
        {
            this._name = name;
            this._companyid = companyid;
            //this._workflow = workflow;
        }

        #endregion
        #region Public Properties

        public virtual int CarRegisterInclude
        {
            get { return _carregisterinclude; }
            set { if (value != this._carregisterinclude) { _carregisterinclude = value; NotifyPropertyChanged("CarRegisterInclude"); } }
        }
        public virtual int WritingInclude
        {
            get { return _writinginclude; }
            set { if (value != this._writinginclude) { _writinginclude = value; NotifyPropertyChanged("WritingInclude"); } }
        }
        public virtual string FormInclude
        {
            get { return _forminclude; }
            set {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Nombre cannot contain more than 50 characters");
                if (value != this._forminclude)
                { _forminclude = value; NotifyPropertyChanged("FormInclude"); }
            }
        }

        public virtual int Id
        {
            get { return _id; }
            set { if (value != this._id) { _id = value; NotifyPropertyChanged("Id"); } }
        }
        public virtual Company Company
        {
            get { return _companyid; }
            set { if (value != this._companyid) { _companyid = value; NotifyPropertyChanged("Company"); } }
        }
        public  virtual string Name
		{
			get { return _name; }
			set {
				if ( value != null && value.Length > 25)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Nombre cannot contain more than 25 characters");
				if (value != this._name){_name= value;NotifyPropertyChanged("Name");}}
        }
        public virtual int OnBoardingMandatory
        {
            get => _onboardingmandatory;
            set => _onboardingmandatory = value;
        }
        public virtual string BPWebTypeOnboarding
        {
            get { return _bpwebtypeonboarding; }
            set
            {
                if (value != null && value.Length > 20)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "BPWebTypeOnboarding cannot contain more than 20 characters");
                if (value != this._bpwebtypeonboarding) { _bpwebtypeonboarding = value; NotifyPropertyChanged("BPWebTypeOnboarding"); }
            }
        }
        public virtual string BPWebTypeVerify
        {
            get { return _bpwebtypeverify; }
            set
            {
                if (value != null && value.Length > 20)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "BPWebTypeVerify cannot contain more than 20 characters");
                if (value != this._bpwebtypeverify) { _bpwebtypeverify = value; NotifyPropertyChanged("BPWebTypeVerify"); }
            }
        }
        public virtual string ReceiverList
        {
            get { return _receiverlist; }
            set
            {
                if (value != null && value.Length > 1024)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "ReceiverList cannot contain more than 1024 characters");
                if (value != this._receiverlist) { _receiverlist = value; NotifyPropertyChanged("ReceiverList"); }
            }
        }
        public virtual double Threshold
        {
            get { return _threshold; }
            set { if (value != this._threshold) { _threshold = value; NotifyPropertyChanged("Threshold"); } }
        }
        public virtual DateTime DateCreate
        {
            get { return _datecreate; }
            set { if (value != this._datecreate) { _datecreate = value; NotifyPropertyChanged("DateCreate"); } }
        }
        public virtual DateTime DateUpdate
        {
            get { return _dateupdate; }
            set { if (value != this._dateupdate) { _dateupdate = value; NotifyPropertyChanged("DateUpdate"); } }
        }
        public virtual int Autorization
        {
            get { return _autorization; }
            set { if (value != this._autorization) { _autorization = value; NotifyPropertyChanged("Autorization"); } }
        }
        public virtual string AutorizationMessage
        {
            get { return _autorizationmessage; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "AutorizationMessage cannot contain more than 2147483647 characters");
                if (value != this._autorizationmessage) { _autorizationmessage = value; NotifyPropertyChanged("AutorizationMessage"); }
            }
        }
        public virtual int SignerInclude
        {
            get { return _signerinclude; }
            set { if (value != this._signerinclude) { _signerinclude = value; NotifyPropertyChanged("SignerInclude"); } }
        }
        public virtual int CheckLockSRCeI
        {
            get { return _checklocksrcei; }
            set { if (value != this._checklocksrcei) { _checklocksrcei = value; NotifyPropertyChanged("CheckLockSRCeI"); } }
        }
        public virtual int VideoInclude
        {
            get { return _videoinclude; }
            set { if (value != this._videoinclude) { _videoinclude = value; NotifyPropertyChanged("VideoInclude"); } }
        }
        public virtual string VideoMessage
        {
            get { return _videomessage; }
            set
            {
                if (value != null && value.Length > 2147483647)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "VideoMessage cannot contain more than 2147483647 characters");
                if (value != this._videomessage) { _videomessage = value; NotifyPropertyChanged("VideoMessage"); }
            }
        }
        public virtual string Theme
        {
            get { return _theme; }
            set {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Theme cannot contain more than 50 characters");
                if (value != this._theme) { _theme = value; NotifyPropertyChanged("Theme"); }
            }
        }
        public virtual int CheckAdult
        {
            get => _checkadult;
            set => _checkadult = value;
        }
        public virtual int CheckExpired
        {
            get => _checkexpired;
            set => _checkexpired = value;
        }
        public virtual int GeoRefMandatory
        {
            get => _georefmandatory;
            set => _georefmandatory = value;
        }
        public virtual string RedirectUrl
        {
            get { return _redirecturl; }
            set {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "RedirectUrl cannot contain more than 250 characters");
                if (value != this._redirecturl)
                { _redirecturl = value; NotifyPropertyChanged("RedirectUrl"); }
            }
        }
        public virtual string SuccessUrl
        {
            get { return _successurl; }
            set {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "SuccessUrl cannot contain more than 250 characters");
                if (value != this._successurl)
                { _successurl = value; NotifyPropertyChanged("SuccessUrl"); }
            }
        }
        
        protected virtual void NotifyPropertyChanged(String info)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(info));
			}
		}
		
		#endregion
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			RdAplicaciones castObj = (RdAplicaciones)obj;
			return ( castObj != null ) &&
			this._id == castObj.Id;
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57;
			hash = 27 * hash * _id.GetHashCode();
			return hash;
		}
		#endregion
		
	}
}
