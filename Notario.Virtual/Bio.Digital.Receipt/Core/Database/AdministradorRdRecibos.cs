using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
//using NHibernate.Expression;

namespace Bio.Digital.Receipt.Core.Database
{
    public class AdministradorRdRecibos
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorRdRecibos));

        public static RdRecibos BuscarRdReciboByReciboId(string reciboId)
        {
            RdRecibos rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session().CreateCriteria(typeof(RdRecibos))
                                     .Add(NHibernate.Criterion.Expression.Eq("Reciboid", reciboId))
                                     .UniqueResult<RdRecibos>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorRdRecibos.BuscarRdReciboByReciboId", ex);
            }
            
            return rec;
        }

        public static RdRecibos BuscarRdReciboById(int id)
        {
            RdRecibos rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session().CreateCriteria(typeof(RdRecibos))
                                     .Add(NHibernate.Criterion.Expression.Eq("Id", id))
                                     .UniqueResult<RdRecibos>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorRdRecibos.BuscarRdReciboById", ex);
            }

            return rec;
        }

        public static IList<RdRecibos> BuscarRecibosByFilter(DateTime fdesde,
                                                           DateTime fhasta,
                                                           string reciboId,
                                                           int idapp)
        {
            IList<RdRecibos> lista = null;
            try
            {
                using (ISession session = PersistentManager.session())
                {
                    ICriteria crit = session.CreateCriteria(typeof(RdRecibos));

                    crit.Add(NHibernate.Criterion.Expression.Between("Fecha", fdesde, fhasta));
                    if (idapp != 0)
                    {
                        crit.Add(NHibernate.Criterion.Expression.Eq("RdAplicacion", AdministradorRdAplicaciones.BuscarRdAplicacionesById(idapp)));
                    }

                    if (!String.IsNullOrEmpty(reciboId))
                    {
                        crit.Add(NHibernate.Criterion.Expression.Eq("Reciboid", reciboId));
                    }

                    lista = crit.AddOrder(NHibernate.Criterion.Order.Desc("Fecha")).List<RdRecibos>();
                }

            }
            catch (Exception ex)
            {
                log.Error("Error AdministradorRdRecibos.BuscarRecibosByFilter", ex);
                lista = null;
            }


            return lista;

        }

        public static RdRecibos Save(RdRecibos oRec)
        {
            RdRecibos rec = null;

            try
            {
                log.Debug("AdministradorRdRecibos.Save IN..."); 
                using (ISession session = PersistentManager.session())
                {
                    session.Save(oRec);
                    session.Flush();
                }
                log.Debug("AdministradorRdRecibos.Save Save sin error");
                rec = oRec;
                log.Debug("AdministradorRdRecibos.Save rec != null => " + (rec != null).ToString());
            }
            catch (Exception ex)
            {
                log.Error("AdministradorRdRecibos.Save error ex = ", ex);
                rec = null;
            }
            log.Debug("AdministradorRdRecibos.Save OUT!");
            return rec;
        }

        internal static string GetTrackIdUnique()
        {
            RdCITx rdCITx;
            string retGuid = null;
            try
            {
                bool isOK = false;

                while (!isOK)
                {
                    retGuid = Guid.NewGuid().ToString("N");
                    using (ISession session = PersistentManager.session())
                    {
                        rdCITx = PersistentManager.session().CreateCriteria(typeof(RdRecibos))
                                         .Add(NHibernate.Criterion.Expression.Eq("Reciboid", retGuid))
                                         .UniqueResult<RdCITx>();
                    }
                    if (rdCITx != null)
                    {
                        rdCITx = null;
                    }
                    else
                    {
                        isOK = true;
                    }
                }
            }
            catch (Exception ex)
            {
                retGuid = Guid.NewGuid().ToString("N"); ;
                log.Error("AdministradorRdRecibos.GetTrackIdUnique", ex);
            }

            return retGuid;
        }
    }
}
