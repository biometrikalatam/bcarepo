/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Digital.Receipt.Core.Database
{
	/// <summary>
	/// RdExtensiones object for NHibernate mapped table 'rd_extensiones'.
	/// </summary>
	[Serializable]
	public class RdExtensiones : INotifyPropertyChanged
	{
		#region Member Variables
		protected int _id;
		protected RdAplicaciones _rdaplicaciones;
		protected string _nombre;
		protected string _tipo;
		public virtual event PropertyChangedEventHandler PropertyChanged;
		
		#endregion
		#region Constructors
			
		public RdExtensiones() {}
					
		public RdExtensiones(RdAplicaciones rdaplicaciones, string nombre, string tipo) 
		{
			this._rdaplicaciones= rdaplicaciones;
			this._nombre= nombre;
			this._tipo= tipo;
		}

		public RdExtensiones(RdAplicaciones rdaplicaciones, string nombre)
		{
			this._rdaplicaciones= rdaplicaciones;
			this._nombre= nombre;
		}
		
		#endregion
		#region Public Properties
		public  virtual int Id
		{
			get { return _id; }
			set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
		}
		public  virtual RdAplicaciones RdAplicaciones
		{
			get { return _rdaplicaciones; }
			set {_rdaplicaciones= value; }
		}
		public  virtual string Nombre
		{
			get { return _nombre; }
			set {
				if ( value != null && value.Length > 50)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Nombre cannot contain more than 50 characters");
				if (value != this._nombre){_nombre= value;NotifyPropertyChanged("Nombre");}}
		}
		public  virtual string Tipo
		{
			get { return _tipo; }
			set {
				if ( value != null && value.Length > 10)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Tipo cannot contain more than 10 characters");
				if (value != this._tipo){_tipo= value;NotifyPropertyChanged("Tipo");}}
		}
		protected virtual void NotifyPropertyChanged(String info)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(info));
			}
		}
		
		#endregion
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			RdExtensiones castObj = (RdExtensiones)obj;
			return ( castObj != null ) &&
			this._id == castObj.Id;
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57;
			hash = 27 * hash * _id.GetHashCode();
			return hash;
		}
		#endregion
		
	}
}
