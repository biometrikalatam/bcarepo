﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.Core.Database
{
    public class AdministradorUser
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdministradorUser));

        private bool IsValiduserFormat(string username)
        {
            return username.All(c => IsValidChar(c));
        }

        private bool IsValidChar(char c)
        {
            if (Char.IsLetterOrDigit(c))
                return true;
            if (c == '@')
                return true;

            return false;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        //public bool ValidateUser(string user, string password)
        //{
        //    bool validateUser = false;
        //    string username = user.Trim();
        //    if (user == null)
        //    {
        //        //ValidationDictionary.AddError("", "Usuario no válido");
        //        LOG.Info(string.Format("Intento de autenticación fallido para {0}  pass {1}", username, password));
        //        return false;
        //    }



        //    if (user.IsApproved)
        //    {
        //        if (user != null)
        //            if (UserService.ValidatePassword(user, password))
        //                validateUser = true;
        //            else
        //            {
        //                Log4Bio.Info(string.Format("Intento de autenticación fallido para {0}  pass {1}", username, password));
        //            }
        //        else
        //        {
        //            Log4Bio.Info(string.Concat("Intento de autenticación fallido para ", username));
        //        }
        //    }
        //    else
        //    {
        //        Log4Bio.Info(string.Concat("Intento de autenticación fallido de usuario deshabilitdo para ", username));
        //    }

        //    return validateUser;
        //}

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="username"></param>
        ///// <param name="password"></param>
        ///// <returns></returns>
        //public bool ValidateUser(string username, string password)
        //{
        //    IUser user = UserService.GetByUsername(username);

        //    if (user != null)
        //        return ValidateUser(user, password);
        //    else
        //        return false;
        //}
    }
}