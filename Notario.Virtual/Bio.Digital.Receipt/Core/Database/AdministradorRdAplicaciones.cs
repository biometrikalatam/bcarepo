using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;

namespace Bio.Digital.Receipt.Core.Database
{
    public class AdministradorRdAplicaciones
    {
        public static RdAplicaciones BuscarRdAplicacionesById(int Id)
        {
            RdAplicaciones app = null;

            using (ISession session = PersistentManager.session())
            {

                //app = session.CreateQuery("from RdAplicaciones").UniqueResult<RdAplicaciones>();


                app = session.CreateCriteria(typeof(RdAplicaciones))
                    .Add(Expression.Eq("Id", Id))
                    .UniqueResult<RdAplicaciones>();
            }
            return app;
        }

        public static IList<RdAplicaciones> BuscarAplicaciones()
        {
            IList<RdAplicaciones> aplicaciones = null;
            using (ISession session = PersistentManager.session())
            {
                aplicaciones = session.CreateCriteria(typeof(RdAplicaciones)).List<RdAplicaciones>();
            }
            return aplicaciones;
        }

        internal static int BuscarRdAplicacionesByCode(int companyId, string cICode)
        {
            RdAplicaciones app = null;

            using (ISession session = PersistentManager.session())
            {

                //app = session.CreateQuery("from RdAplicaciones").UniqueResult<RdAplicaciones>();
                app = session.CreateCriteria(typeof(RdAplicaciones))
                    .Add(Expression.Eq("Company", companyId))
                    .Add(Expression.Eq("Code", cICode))
                    .UniqueResult<RdAplicaciones>();
            }
            return (app != null) ? app.Id : 0;
        }
    }
}