using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
//using NHibernate.Expression;

namespace Bio.Digital.Receipt.Core.Database
{

    /// <summary>
    /// 
    /// </summary>
    public class AdministradorCompanyWorkflow
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorCompanyWorkflow));

        public static CompanyWorkflow BuscarCompanyWokflowByName(int companyid, string name)
        {
            CompanyWorkflow rec = null;

            try
            {
                Company company = AdministradorCompany.BuscarCompanyById(companyid);
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session().CreateCriteria(typeof(CompanyWorkflow))
                                     .Add(NHibernate.Criterion.Expression.Eq("Company", company))
                                     .Add(NHibernate.Criterion.Expression.Eq("Name", name))
                                     .UniqueResult<CompanyWorkflow>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorCompanyWorkflow.BuscarCompanyByName", ex);
            }
            
            return rec;
        }

        public static CompanyWorkflow BuscarCompanyWokflowById(int id)
        {
            CompanyWorkflow rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session().CreateCriteria(typeof(CompanyWorkflow))
                                     .Add(NHibernate.Criterion.Expression.Eq("Id", id))
                                     .UniqueResult<CompanyWorkflow>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorCompanyWorkflow.BuscarCompanyWokflowById", ex);
            }

            return rec;
        }

        
    }
}
