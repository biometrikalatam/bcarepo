using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using NHibernate.Criterion;
//using NHibernate.Expression;

namespace Bio.Digital.Receipt.Core.Database
{

    /// <summary>
    /// 
    /// </summary>
    public class AdministradorCompany
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorCompany));

        public static Company RetrieveAuthCredential(string accessname, out string msgErr)
        {
            msgErr = "S/C";
            Company company;
            try
            {
                using (ISession sess = PersistentManager.session())
                {
                    company = PersistentManager.session().CreateCriteria(typeof(Company))
                                                  .Add(Expression.Eq("AccessName", accessname))
                                                  .UniqueResult<Company>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorCompany.RetrieveAuthCredential (accessname)", ex);
                msgErr = ex.Message;
                company = null;
            }
            return company;
        }

        public static Company BuscarCompanyByRut(string rut)
        {
            Company rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session().CreateCriteria(typeof(Company))
                                     .Add(NHibernate.Criterion.Expression.Eq("Rut", rut))
                                     .UniqueResult<Company>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorCompany.BuscarCompanyByRut", ex);
            }
            
            return rec;
        }

        public static Company BuscarCompanyById(int id)
        {
            Company rec = null;

            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rec = PersistentManager.session().CreateCriteria(typeof(Company))
                                     .Add(NHibernate.Criterion.Expression.Eq("Id", id))
                                     .UniqueResult<Company>();
                }
            }
            catch (Exception ex)
            {
                log.Error("AdministradorCompany.BuscarCompanyById", ex);
            }

            return rec;
        }

        
    }
}
