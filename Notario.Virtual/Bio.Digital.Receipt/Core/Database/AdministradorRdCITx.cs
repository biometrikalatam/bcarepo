﻿using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.libs;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.Core.Database
{
    public class AdministradorRdCITx
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(AdministradorRdCITx));

        public static RdCITx CreateCITx(string trackid, int actionid, int companyid, int validityType, string qrgenerated, 
                                        string typeid, string valueid, string mail, string dynamicparam,
                                        out string msg)
        {
            RdCITx rdCITx = null;
            msg = null;
            try
            {
                rdCITx = new RdCITx();
                rdCITx.ActionId = actionid;
                rdCITx.CompanyId = companyid;
                rdCITx.TrackId = trackid;
                rdCITx.ValidityType = validityType;
                rdCITx.DateGen = DateTime.Now;
                rdCITx.LastModify = DateTime.Now;
                rdCITx.QRGenerated = qrgenerated;
                rdCITx.QRValidity = Properties.Settings.Default.QRValidity;
                rdCITx.ValidityDays = Properties.Settings.Default.CIValidityDays;
                rdCITx.TypeId = typeid;
                rdCITx.ValueId = valueid;
                rdCITx.DestinataryMail = mail;
                rdCITx.DynamicParam = dynamicparam;
                using (ISession session = PersistentManager.session())
                {
                    session.Save(rdCITx);
                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                rdCITx = null;
                log.Error("AdministradorRdCITx.CreateCITx", ex);
            }

            return rdCITx;
        }

        internal static RdCITx GetCITxByRdId(int receiptid, out string msg)
        {
            RdCITx rdCITx = null;
            msg = null;
            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rdCITx = session.CreateCriteria(typeof(RdCITx))
                                     .Add(NHibernate.Criterion.Expression.Eq("ReceiptId", receiptid))
                                     .UniqueResult<RdCITx>();
                }
            }
            catch (Exception ex)
            {
                rdCITx = null;
                msg = ex.Message;
                log.Error("AdministradorRdCITx.GetCITxByRdId", ex);
            }

            return rdCITx;
        }

        internal static RdCITx GetCITxByTrackId(int actionid, string trackIdCI, int companyid, int validityType, bool createIfNotExist, 
                                                string typeid, string valueid, string name, string mail, 
                                                string workstationId, string geoRef, string username,  out string msg)
        {
            RdCITx rdCITx = null;
            msg = null;
            try
            {
                if (!string.IsNullOrEmpty(trackIdCI))
                {
                    using (ISession session = PersistentManager.session())
                    {
                        rdCITx = session.CreateCriteria(typeof(RdCITx))
                                         .Add(NHibernate.Criterion.Expression.Eq("TrackId", trackIdCI))
                                         .UniqueResult<RdCITx>();
                    }
                }
                //Si no existe y pide que se cree => se crea
                if (createIfNotExist && rdCITx == null)
                {
                    rdCITx = new RdCITx(); 
                    rdCITx.ActionId = 1; // actionid; => Creo ActionId 1 dado que aun no he ralizado nada
                    rdCITx.CompanyId = companyid;
                    rdCITx.TrackId = GetTrackIdUnique(); // Guid.NewGuid().ToString("N");
                    rdCITx.ValidityType = validityType;
                    rdCITx.DateGen = DateTime.Now;
                    rdCITx.QRValidity = Properties.Settings.Default.QRValidity;
                    rdCITx.ValidityDays = Properties.Settings.Default.CIValidityDays;
                    rdCITx.LastModify = DateTime.Now;
                    rdCITx.TypeId = typeid;
                    rdCITx.ValueId = valueid;
                    rdCITx.Name = name;
                    rdCITx.GeoRef = geoRef;
                    rdCITx.WorkstationId = workstationId;
                    rdCITx.UserName = username;
                    rdCITx.DestinataryMail = string.IsNullOrEmpty(mail) ? "info@notariovirtual.cl" : mail;
                    using (ISession session = PersistentManager.session())
                    {
                        session.Save(rdCITx);
                        session.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                rdCITx = null;
                msg = ex.Message;
                log.Error("AdministradorRdCITx.GetCITxByTrackId", ex);
            }

            return rdCITx;
        }

        internal static void UpdateCITx(XmlParamIn oXmlIn, RdRecibos objRD, string trackIdBP, string trackIDE, string certifyPDF, int resulteCode, double score, double threshold,
                                        string name, string phaterlastname, string motherlastname, string urlvideo, out string msg)
        {
            RdCITx rdCITx = null;
            msg = null;
            try
            {
                log.Debug("AdministradorRdCITx.UpdateCITx IN...");
                using (ISession session = PersistentManager.session())
                {
                    log.Debug("AdministradorRdCITx.UpdateCITx - Recupero txid=" + oXmlIn.CIParam.TrackId);
                    rdCITx = PersistentManager.session().CreateCriteria(typeof(RdCITx))
                                     .Add(NHibernate.Criterion.Expression.Eq("TrackId", oXmlIn.CIParam.TrackId))
                                     .UniqueResult<RdCITx>();
                }
                if (rdCITx != null) {
                    log.Debug("AdministradorRdCITx.UpdateCITx - Comienza a actualizar...");
                    rdCITx.ActionId = oXmlIn.Actionid;
                    rdCITx.CertifyPDF = certifyPDF;
                    log.Debug("AdministradorRdCITx.UpdateCITx - CertifyPDF len = " + 
                              (string.IsNullOrEmpty(rdCITx.CertifyPDF)?"null":rdCITx.CertifyPDF.Length.ToString()));
                    rdCITx.TrackIdBP = trackIdBP;
                    log.Debug("AdministradorRdCITx.UpdateCITx - trackIdBP len = " +
                              (string.IsNullOrEmpty(rdCITx.TrackIdBP) ? "null" : rdCITx.TrackIdBP.Length.ToString()));
                    rdCITx.TrackIdDE = trackIDE;
                    log.Debug("AdministradorRdCITx.UpdateCITx - TrackIdDE len = " +
                              (string.IsNullOrEmpty(rdCITx.TrackIdDE) ? "null" : rdCITx.TrackIdDE.Length.ToString()));
                    rdCITx.ResultCode = resulteCode;
                    rdCITx.Score = score.ToString();
                    log.Debug("AdministradorRdCITx.UpdateCITx - Score len = " +
                              (string.IsNullOrEmpty(rdCITx.Score) ? "null" : rdCITx.Score.Length.ToString()));
                    rdCITx.Threshold = threshold.ToString();
                    log.Debug("AdministradorRdCITx.UpdateCITx - Threshold len = " +
                              (string.IsNullOrEmpty(rdCITx.Threshold) ? "null" : rdCITx.Threshold.Length.ToString()));
                    rdCITx.LastModify = DateTime.Now;
                    rdCITx.ReceiptId = objRD.Id;
                    rdCITx.Name = string.IsNullOrEmpty(name)?rdCITx.Name:name;
                    log.Debug("AdministradorRdCITx.UpdateCITx - Name len = " +
                              (string.IsNullOrEmpty(rdCITx.Name) ? "null" : rdCITx.Name.Length.ToString()));
                    rdCITx.PhaterLastName = string.IsNullOrEmpty(phaterlastname) ? rdCITx.PhaterLastName:phaterlastname;
                    log.Debug("AdministradorRdCITx.UpdateCITx - PhaterLastName len = " +
                              (string.IsNullOrEmpty(rdCITx.PhaterLastName) ? "null" : rdCITx.PhaterLastName.Length.ToString()));
                    rdCITx.MotherLastName = string.IsNullOrEmpty(motherlastname) ? rdCITx.MotherLastName:motherlastname;
                    log.Debug("AdministradorRdCITx.UpdateCITx - MotherLastName len = " +
                              (string.IsNullOrEmpty(rdCITx.MotherLastName) ? "null" : rdCITx.MotherLastName.Length.ToString()));

                    rdCITx.IdCardImageFront = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront) ? oXmlIn.CIParam.IDCardImageFront : rdCITx.IdCardImageFront;
                    log.Debug("AdministradorRdCITx.UpdateCITx - IdCardImageFront len = " +
                              (string.IsNullOrEmpty(rdCITx.IdCardImageFront) ? "null" : rdCITx.IdCardImageFront.Length.ToString()));
                    rdCITx.IdCardImageBack = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? oXmlIn.CIParam.IDCardImageBack : rdCITx.IdCardImageBack;
                    log.Debug("AdministradorRdCITx.UpdateCITx - IdCardImageBack len = " +
                              (string.IsNullOrEmpty(rdCITx.IdCardImageBack) ? "null" : rdCITx.IdCardImageBack.Length.ToString()));
                    rdCITx.IdCardPhotoImage = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardPhotoImage) ? oXmlIn.CIParam.IDCardPhotoImage : rdCITx.IdCardPhotoImage;
                    log.Debug("AdministradorRdCITx.UpdateCITx - IdCardPhotoImage len = " +
                              (string.IsNullOrEmpty(rdCITx.IdCardPhotoImage) ? "null" : rdCITx.IdCardPhotoImage.Length.ToString()));
                    rdCITx.IdCardSignatureImage = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardSignatureImage) ? oXmlIn.CIParam.IDCardSignatureImage : rdCITx.IdCardSignatureImage;
                    log.Debug("AdministradorRdCITx.UpdateCITx - IdCardSignatureImage len = " +
                              (string.IsNullOrEmpty(rdCITx.IdCardSignatureImage) ? "null" : rdCITx.IdCardSignatureImage.Length.ToString()));
                    rdCITx.Selfie = !string.IsNullOrEmpty(oXmlIn.CIParam.Selfie) ? oXmlIn.CIParam.Selfie : rdCITx.Selfie;
                    log.Debug("AdministradorRdCITx.UpdateCITx - Selfie len = " +
                              (string.IsNullOrEmpty(rdCITx.Selfie) ? "null" : rdCITx.Selfie.Length.ToString()));
                    rdCITx.IssueDate = !string.IsNullOrEmpty(oXmlIn.CIParam.IssueDate) ? oXmlIn.CIParam.IssueDate : rdCITx.IssueDate;
                    rdCITx.ExprationDate = !string.IsNullOrEmpty(oXmlIn.CIParam.ExpirationDate) ? oXmlIn.CIParam.ExpirationDate : rdCITx.ExprationDate;
                    rdCITx.BirthDate = !string.IsNullOrEmpty(oXmlIn.CIParam.BirthDate) ? oXmlIn.CIParam.BirthDate : rdCITx.BirthDate;
                    rdCITx.GeoRef = !string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef) ? oXmlIn.CIParam.GeoRef : rdCITx.GeoRef;
                    log.Debug("AdministradorRdCITx.UpdateCITx - GeoRef len = " +
                              (string.IsNullOrEmpty(rdCITx.GeoRef) ? "null" : rdCITx.GeoRef.Length.ToString()));
                    rdCITx.WorkstationId = !string.IsNullOrEmpty(oXmlIn.CIParam.WorkStationID) ? oXmlIn.CIParam.WorkStationID : rdCITx.WorkstationId;
                    log.Debug("AdministradorRdCITx.UpdateCITx - WorkstationId len = " +
                              (string.IsNullOrEmpty(rdCITx.WorkstationId) ? "null" : rdCITx.WorkstationId.Length.ToString()));
                    rdCITx.Serial = !string.IsNullOrEmpty(oXmlIn.CIParam.Serial) ? oXmlIn.CIParam.Serial : rdCITx.Serial;
                    rdCITx.Sex = !string.IsNullOrEmpty(oXmlIn.CIParam.Sex) ? oXmlIn.CIParam.Sex : rdCITx.Sex;
                    rdCITx.Nacionality = !string.IsNullOrEmpty(oXmlIn.CIParam.Nacionality) ? oXmlIn.CIParam.Nacionality : rdCITx.Nacionality;

                    rdCITx.Map = string.IsNullOrEmpty(rdCITx.Map)? Utils.GetMapFromGoogleMap(rdCITx.GeoRef): rdCITx.Map;
                    log.Debug("AdministradorRdCITx.UpdateCITx - Map len = " +
                              (string.IsNullOrEmpty(rdCITx.Map) ? "null" : rdCITx.Map.Length.ToString()));
                    rdCITx.ManualSignatureImage = !string.IsNullOrEmpty(oXmlIn.CIParam.ManualSignatureImage) ? oXmlIn.CIParam.ManualSignatureImage : rdCITx.ManualSignatureImage;
                    log.Debug("AdministradorRdCITx.UpdateCITx - ManualSignatureImage len = " +
                              (string.IsNullOrEmpty(rdCITx.ManualSignatureImage) ? "null" : rdCITx.ManualSignatureImage.Length.ToString()));
                    rdCITx.UrlBPWeb = string.IsNullOrEmpty(rdCITx.UrlBPWeb) ? null : rdCITx.UrlBPWeb;
                    log.Debug("AdministradorRdCITx.UpdateCITx - UrlBPWeb len = " +
                              (string.IsNullOrEmpty(rdCITx.UrlBPWeb) ? "null" : rdCITx.UrlBPWeb.Length.ToString()));
                    rdCITx.CellPhone = string.IsNullOrEmpty(rdCITx.CellPhone) ? null : rdCITx.CellPhone;
                    log.Debug("AdministradorRdCITx.UpdateCITx - CellPhone len = " +
                              (string.IsNullOrEmpty(rdCITx.CellPhone) ? "null" : rdCITx.CellPhone.Length.ToString()));
                    rdCITx.VideoURL = string.IsNullOrEmpty(urlvideo) ? null : urlvideo;
                    log.Debug("AdministradorRdCITx.UpdateCITx - VideoURL len = " +
                              (string.IsNullOrEmpty(rdCITx.VideoURL) ? "null" : rdCITx.VideoURL));
                    log.Debug("AdministradorRdCITx.UpdateCITx - Save in...");
                    using (ISession session = PersistentManager.session())
                    {
                        session.Update(rdCITx);
                        session.Flush();
                    }
                    log.Debug("AdministradorRdCITx.UpdateCITx - Save out!");
                }
                log.Debug("AdministradorRdCITx.UpdateCITx OUT!");
            }
            catch (Exception ex)
            {
                rdCITx = null;
                msg = ex.Message;
                log.Error("AdministradorRdCITx.UpdateCITx", ex);
            }
            
        }

        internal static int SaveCITx(RdCITx oRdCITx)
        {
            int ret = Errors.IRET_OK;
            try
            {
                log.Debug("AdministradorRdCITx.SaveCITx IN...");
                if (oRdCITx != null)
                {
                    using (ISession session = PersistentManager.session())
                    {
                        session.Update(oRdCITx);
                        session.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                log.Error("AdministradorRdCITx.SaveCITx", ex);
            }
            log.Debug("AdministradorRdCITx.SaveCITx OUT!");
            return ret;
        }

        internal static void UpdateCITx(XmlParamIn oXmlIn, RdRecibos objRD, string trackIdBP, string trackIDE, string certifyPDF, 
                                        int resulteCode, out string msg)
        {
            RdCITx rdCITx = null;
            msg = null;
            try
            {
                using (ISession session = PersistentManager.session())
                {
                    rdCITx = PersistentManager.session().CreateCriteria(typeof(RdCITx))
                                     .Add(NHibernate.Criterion.Expression.Eq("TrackId", oXmlIn.CIParam.TrackId))
                                     .UniqueResult<RdCITx>();
                }
                if (rdCITx != null)
                {
                    rdCITx.ActionId = oXmlIn.Actionid;
                    rdCITx.CertifyPDF = certifyPDF;
                    rdCITx.TrackIdBP = trackIdBP;
                    rdCITx.TrackIdDE = trackIDE;
                    rdCITx.ResultCode = resulteCode;
                    rdCITx.LastModify = DateTime.Now;
                    rdCITx.ReceiptId = objRD.Id;
                    rdCITx.Name = string.IsNullOrEmpty(oXmlIn.CIParam.Name) ? rdCITx.Name : oXmlIn.CIParam.Name;
                    rdCITx.PhaterLastName = string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName) ? rdCITx.PhaterLastName : oXmlIn.CIParam.PhaterLastName;
                    rdCITx.MotherLastName = string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName) ? rdCITx.MotherLastName : oXmlIn.CIParam.MotherLastName;

                    rdCITx.IdCardImageFront = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront) ? oXmlIn.CIParam.IDCardImageFront : rdCITx.IdCardImageFront;
                    rdCITx.IdCardImageBack = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? oXmlIn.CIParam.IDCardImageBack : rdCITx.IdCardImageBack;
                    rdCITx.IdCardPhotoImage = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardPhotoImage) ? oXmlIn.CIParam.IDCardPhotoImage : rdCITx.IdCardPhotoImage;
                    rdCITx.IdCardSignatureImage = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardSignatureImage) ? oXmlIn.CIParam.IDCardSignatureImage : rdCITx.IdCardSignatureImage;
                    rdCITx.Selfie = !string.IsNullOrEmpty(oXmlIn.CIParam.Selfie) ? oXmlIn.CIParam.Selfie : rdCITx.Selfie;
                    rdCITx.IssueDate = !string.IsNullOrEmpty(oXmlIn.CIParam.IssueDate) ? oXmlIn.CIParam.IssueDate : rdCITx.IssueDate;
                    rdCITx.ExprationDate = !string.IsNullOrEmpty(oXmlIn.CIParam.ExpirationDate) ? oXmlIn.CIParam.ExpirationDate : rdCITx.ExprationDate;
                    rdCITx.BirthDate = !string.IsNullOrEmpty(oXmlIn.CIParam.BirthDate) ? oXmlIn.CIParam.BirthDate : rdCITx.BirthDate;
                    rdCITx.GeoRef = !string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef) ? oXmlIn.CIParam.GeoRef : rdCITx.GeoRef;
                    rdCITx.WorkstationId = !string.IsNullOrEmpty(oXmlIn.CIParam.WorkStationID) ? oXmlIn.CIParam.WorkStationID : rdCITx.WorkstationId;
                    rdCITx.Serial = !string.IsNullOrEmpty(oXmlIn.CIParam.Serial) ? oXmlIn.CIParam.Serial : rdCITx.Serial;
                    rdCITx.Sex = !string.IsNullOrEmpty(oXmlIn.CIParam.Sex) ? oXmlIn.CIParam.Sex : rdCITx.Sex;


                    using (ISession session = PersistentManager.session())
                    {
                        session.Update(rdCITx);
                        session.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                rdCITx = null;
                msg = ex.Message;
                log.Error("AdministradorRdCITx.UpdateCITx", ex);
            }

        }

        internal static void UpdateCITx(RdCITx rdCITx, out string msg)
        {
            msg = null;
            try
            {
                using (ISession session = PersistentManager.session())
                {
                    session.Update(rdCITx);
                    session.Flush();
                }
                
            }
            catch (Exception ex)
            {
                rdCITx = null;
                msg = ex.Message;
                log.Error("AdministradorRdCITx.UpdateCITx", ex);
            }

        }


        /// <summary>
        /// Genera un TrickId y revisa que no este duplicado
        /// </summary>
        /// <returns></returns>
        internal static string GetTrackIdUnique()
        {
            RdCITx rdCITx;
            string retGuid = null;
            try
            {
                bool isOK = false;

                while (!isOK)
                {
                    retGuid = Guid.NewGuid().ToString("N");
                    using (ISession session = PersistentManager.session())
                    {
                        rdCITx = PersistentManager.session().CreateCriteria(typeof(RdCITx))
                                         .Add(NHibernate.Criterion.Expression.Eq("TrackId", retGuid))
                                         .UniqueResult<RdCITx>();
                    }
                    if (rdCITx != null)
                    {
                        rdCITx = null;
                    } else
                    {
                        isOK = true;
                    }
                }
            }
            catch (Exception ex)
            {
                retGuid = Guid.NewGuid().ToString("N"); ;
                log.Error("AdministradorRdCITx.GetTrackIdUnique", ex);
            }

            return retGuid;
        }

        internal static int CreateCITxWeb(string citrackid, CICreateModel paramIn, BPTxCreateR bPResponse, string trackidbp,
                                          out string msgerr)
        {
            int ret = 0;
            RdCITx rdCITx = null;
            msgerr = null;
            try
            {
                rdCITx = new RdCITx();
                rdCITx.ActionId = DefineActionId(paramIn);
                rdCITx.CompanyId = paramIn.company;
                rdCITx.TrackId = citrackid;
                rdCITx.ValidityType = 0;
                rdCITx.DateGen = DateTime.Now;
                rdCITx.LastModify = DateTime.Now;
                rdCITx.QRGenerated = null;
                rdCITx.QRValidity = Properties.Settings.Default.QRValidity;
                rdCITx.ValidityDays = Properties.Settings.Default.CIValidityDays;
                rdCITx.TypeId = paramIn.typeId;
                rdCITx.ValueId = paramIn.valueId;
                rdCITx.DestinataryMail = paramIn.mailCertify;
                rdCITx.TrackIdBP = trackidbp;
                rdCITx.UrlBPWeb = bPResponse.verifyUrl;
                rdCITx.CellPhone = paramIn.cellNumber;
                if (paramIn.listMailsDestinatary != null)
                {
                    bool first = true;
                    rdCITx.ListMailsDistribution = "";
                    foreach (string item in paramIn.listMailsDestinatary)
                    {
                        if (first)
                        {
                            rdCITx.ListMailsDistribution += item; first = false;
                        } else
                        {
                            rdCITx.ListMailsDistribution += ";" + item;
                        }
                    }
                    log.Debug("AdministradorRdCITx.CreateCITx - rdCITx.ListMailsDistribution = " + rdCITx.ListMailsDistribution);
                }

                using (ISession session = PersistentManager.session())
                {
                    session.Save(rdCITx);
                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                rdCITx = null;
                log.Error("AdministradorRdCITx.CreateCITx", ex);
            }

            return ret;
        }


        internal static int CreateSignerTxWeb(string citrackid, SignerCreateModel paramIn, BPTxCreateR bPResponse, string trackidbp,
                                          out string msgerr)
        {
            int ret = 0;
            RdCITx rdCITx = null;
            msgerr = null;
            try
            {
                rdCITx = new RdCITx();
                rdCITx.Type = 1;
                rdCITx.ActionId = 10; //DefineActionId(paramIn);
                rdCITx.CompanyId = paramIn.company;
                rdCITx.TrackId = citrackid;
                rdCITx.ValidityType = 0;
                rdCITx.DateGen = DateTime.Now;
                rdCITx.LastModify = DateTime.Now;
                rdCITx.QRGenerated = null;
                rdCITx.QRValidity = Properties.Settings.Default.QRValidity;
                rdCITx.ValidityDays = Properties.Settings.Default.CIValidityDays;
                rdCITx.TypeId = paramIn.typeId;
                rdCITx.ValueId = paramIn.valueId;
                rdCITx.DestinataryMail = paramIn.mailCertify;
                rdCITx.TrackIdBP = trackidbp;
                rdCITx.UrlBPWeb = bPResponse.verifyUrl;
                rdCITx.CellPhone = paramIn.cellNumber;
                rdCITx.TaxIdCompany = paramIn.taxidcompany;
                if (paramIn.listMailsDestinatary != null)
                {
                    bool first = true;
                    rdCITx.ListMailsDistribution = "";
                    foreach (string item in paramIn.listMailsDestinatary)
                    {
                        if (first)
                        {
                            rdCITx.ListMailsDistribution += item;
                            first = false;
                        }
                        else
                        {
                            rdCITx.ListMailsDistribution += ";" + item;
                        }
                    }
                    log.Debug("AdministradorRdCITx.CreateSignerTxWeb - rdCITx.ListMailsDistribution = " + rdCITx.ListMailsDistribution);
                }

                using (ISession session = PersistentManager.session())
                {
                    session.Save(rdCITx);
                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                rdCITx = null;
                log.Error("AdministradorRdCITx.CreateSignerTxWeb", ex);
            }

            return ret;
        }

        private static int DefineActionId(CICreateModel paramIn)
        {
            return 1;
        }

        internal static RdCITx GetCITxByTrackId(string trackid, out string msgRet)
        {
            RdCITx rdCITx = null;
            msgRet = null;
            try
            {
                if (!string.IsNullOrEmpty(trackid))
                {
                    using (ISession session = PersistentManager.session())
                    {
                        rdCITx = session.CreateCriteria(typeof(RdCITx))
                                         .Add(NHibernate.Criterion.Expression.Eq("TrackId", trackid))
                                         .UniqueResult<RdCITx>();
                    }
                }
                
            }
            catch (Exception ex)
            {
                rdCITx = null;
                msgRet = ex.Message;
                log.Error("AdministradorRdCITx.GetCITxByTrackId", ex);
            }

            return rdCITx;
        }

        internal static RdCITx GetCITxByTrackIdBP(string trackidbp, out string msgerr)
        {
            RdCITx rdCITx = null;
            msgerr = null;
            try
            {
                if (!string.IsNullOrEmpty(trackidbp))
                {
                    using (ISession session = PersistentManager.session())
                    {
                        rdCITx = session.CreateCriteria(typeof(RdCITx))
                                         .Add(NHibernate.Criterion.Expression.Eq("TrackIdBP", trackidbp))
                                         .UniqueResult<RdCITx>();
                    }
                }

            }
            catch (Exception ex)
            {
                rdCITx = null;
                msgerr = ex.Message;
                log.Error("AdministradorRdCITx.GetCITxByTrackIdBP", ex);
            }

            return rdCITx;
        }

        internal static void UpdateCITxSigner(RdCITx tx, XmlParamIn oXmlIn, RdRecibos objRD, string trackIdBP, string trackIDE, 
                                              string certifyPDF, int resulteCode, double score, double threshold,
                                              string name, string phaterlastname, string motherlastname, 
                                              string securitycode, out string msg)
        {
            RdCITx rdCITx = null;
            msg = null;
            try
            {
                log.Debug("AdministradorRdCITx.UpdateCITx IN...");
                using (ISession session = PersistentManager.session())
                {
                    log.Debug("AdministradorRdCITx.UpdateCITx - Recupero txid=" + oXmlIn.CIParam.TrackId);
                    rdCITx = PersistentManager.session().CreateCriteria(typeof(RdCITx))
                                     .Add(NHibernate.Criterion.Expression.Eq("TrackId", oXmlIn.CIParam.TrackId))
                                     .UniqueResult<RdCITx>();
                }
                if (rdCITx != null)
                {
                    log.Debug("AdministradorRdCITx.UpdateCITx - Comienza a actualizar...");
                    rdCITx.ActionId = oXmlIn.Actionid;
                    rdCITx.CertifyPDF = certifyPDF;
                    log.Debug("AdministradorRdCITx.UpdateCITx - CertifyPDF len = " +
                              (string.IsNullOrEmpty(rdCITx.CertifyPDF) ? "null" : rdCITx.CertifyPDF.Length.ToString()));
                    rdCITx.TrackIdBP = trackIdBP;
                    log.Debug("AdministradorRdCITx.UpdateCITx - trackIdBP len = " +
                              (string.IsNullOrEmpty(rdCITx.TrackIdBP) ? "null" : rdCITx.TrackIdBP.Length.ToString()));
                    rdCITx.TrackIdDE = trackIDE;
                    log.Debug("AdministradorRdCITx.UpdateCITx - TrackIdDE len = " +
                              (string.IsNullOrEmpty(rdCITx.TrackIdDE) ? "null" : rdCITx.TrackIdDE.Length.ToString()));
                    rdCITx.ResultCode = resulteCode;
                    log.Debug("AdministradorRdCITx.UpdateCITx - ResultCode = " + rdCITx.ResultCode.ToString());
                    rdCITx.Score = score.ToString();
                    log.Debug("AdministradorRdCITx.UpdateCITx - Score len = " +
                              (string.IsNullOrEmpty(rdCITx.Score) ? "null" : rdCITx.Score.Length.ToString()));
                    rdCITx.Threshold = threshold.ToString();
                    log.Debug("AdministradorRdCITx.UpdateCITx - Threshold len = " +
                              (string.IsNullOrEmpty(rdCITx.Threshold) ? "null" : rdCITx.Threshold.Length.ToString()));
                    rdCITx.LastModify = DateTime.Now;
                    rdCITx.ReceiptId = objRD.Id;
                    rdCITx.Name = string.IsNullOrEmpty(name) ? rdCITx.Name : name;
                    log.Debug("AdministradorRdCITx.UpdateCITx - Name len = " +
                              (string.IsNullOrEmpty(rdCITx.Name) ? "null" : rdCITx.Name.Length.ToString()));
                    rdCITx.PhaterLastName = string.IsNullOrEmpty(phaterlastname) ? rdCITx.PhaterLastName : phaterlastname;
                    log.Debug("AdministradorRdCITx.UpdateCITx - PhaterLastName len = " +
                              (string.IsNullOrEmpty(rdCITx.PhaterLastName) ? "null" : rdCITx.PhaterLastName.Length.ToString()));
                    rdCITx.MotherLastName = string.IsNullOrEmpty(motherlastname) ? rdCITx.MotherLastName : motherlastname;
                    log.Debug("AdministradorRdCITx.UpdateCITx - MotherLastName len = " +
                              (string.IsNullOrEmpty(rdCITx.MotherLastName) ? "null" : rdCITx.MotherLastName.Length.ToString()));

                    rdCITx.IdCardImageFront = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront) ? oXmlIn.CIParam.IDCardImageFront : rdCITx.IdCardImageFront;
                    log.Debug("AdministradorRdCITx.UpdateCITx - IdCardImageFront len = " +
                              (string.IsNullOrEmpty(rdCITx.IdCardImageFront) ? "null" : rdCITx.IdCardImageFront.Length.ToString()));
                    rdCITx.IdCardImageBack = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? oXmlIn.CIParam.IDCardImageBack : rdCITx.IdCardImageBack;
                    log.Debug("AdministradorRdCITx.UpdateCITx - IdCardImageBack len = " +
                              (string.IsNullOrEmpty(rdCITx.IdCardImageBack) ? "null" : rdCITx.IdCardImageBack.Length.ToString()));
                    rdCITx.IdCardPhotoImage = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardPhotoImage) ? oXmlIn.CIParam.IDCardPhotoImage : rdCITx.IdCardPhotoImage;
                    log.Debug("AdministradorRdCITx.UpdateCITx - IdCardPhotoImage len = " +
                              (string.IsNullOrEmpty(rdCITx.IdCardPhotoImage) ? "null" : rdCITx.IdCardPhotoImage.Length.ToString()));
                    rdCITx.IdCardSignatureImage = !string.IsNullOrEmpty(oXmlIn.CIParam.IDCardSignatureImage) ? oXmlIn.CIParam.IDCardSignatureImage : rdCITx.IdCardSignatureImage;
                    log.Debug("AdministradorRdCITx.UpdateCITx - IdCardSignatureImage len = " +
                              (string.IsNullOrEmpty(rdCITx.IdCardSignatureImage) ? "null" : rdCITx.IdCardSignatureImage.Length.ToString()));
                    rdCITx.Selfie = !string.IsNullOrEmpty(oXmlIn.CIParam.Selfie) ? oXmlIn.CIParam.Selfie : rdCITx.Selfie;
                    log.Debug("AdministradorRdCITx.UpdateCITx - Selfie len = " +
                              (string.IsNullOrEmpty(rdCITx.Selfie) ? "null" : rdCITx.Selfie.Length.ToString()));
                    rdCITx.IssueDate = !string.IsNullOrEmpty(oXmlIn.CIParam.IssueDate) ? oXmlIn.CIParam.IssueDate : rdCITx.IssueDate;
                    rdCITx.ExprationDate = !string.IsNullOrEmpty(oXmlIn.CIParam.ExpirationDate) ? oXmlIn.CIParam.ExpirationDate : rdCITx.ExprationDate;
                    rdCITx.BirthDate = !string.IsNullOrEmpty(oXmlIn.CIParam.BirthDate) ? oXmlIn.CIParam.BirthDate : rdCITx.BirthDate;
                    rdCITx.GeoRef = !string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef) ? oXmlIn.CIParam.GeoRef : rdCITx.GeoRef;
                    log.Debug("AdministradorRdCITx.UpdateCITx - GeoRef len = " +
                              (string.IsNullOrEmpty(rdCITx.GeoRef) ? "null" : rdCITx.GeoRef.Length.ToString()));
                    rdCITx.WorkstationId = !string.IsNullOrEmpty(oXmlIn.CIParam.WorkStationID) ? oXmlIn.CIParam.WorkStationID : rdCITx.WorkstationId;
                    log.Debug("AdministradorRdCITx.UpdateCITx - WorkstationId len = " +
                              (string.IsNullOrEmpty(rdCITx.WorkstationId) ? "null" : rdCITx.WorkstationId.Length.ToString()));
                    rdCITx.Serial = !string.IsNullOrEmpty(oXmlIn.CIParam.Serial) ? oXmlIn.CIParam.Serial : rdCITx.Serial;
                    rdCITx.Sex = !string.IsNullOrEmpty(oXmlIn.CIParam.Sex) ? oXmlIn.CIParam.Sex : rdCITx.Sex;
                    rdCITx.Nacionality = !string.IsNullOrEmpty(oXmlIn.CIParam.Nacionality) ? oXmlIn.CIParam.Nacionality : rdCITx.Nacionality;

                    rdCITx.Map = string.IsNullOrEmpty(rdCITx.Map) ? Utils.GetMapFromGoogleMap(rdCITx.GeoRef) : rdCITx.Map;
                    log.Debug("AdministradorRdCITx.UpdateCITx - Map len = " +
                              (string.IsNullOrEmpty(rdCITx.Map) ? "null" : rdCITx.Map.Length.ToString()));
                    rdCITx.ManualSignatureImage = !string.IsNullOrEmpty(oXmlIn.CIParam.ManualSignatureImage) ? oXmlIn.CIParam.ManualSignatureImage : rdCITx.ManualSignatureImage;
                    log.Debug("AdministradorRdCITx.UpdateCITx - ManualSignatureImage len = " +
                              (string.IsNullOrEmpty(rdCITx.ManualSignatureImage) ? "null" : rdCITx.ManualSignatureImage.Length.ToString()));
                    rdCITx.UrlBPWeb = string.IsNullOrEmpty(rdCITx.UrlBPWeb) ? null : rdCITx.UrlBPWeb;
                    log.Debug("AdministradorRdCITx.UpdateCITx - UrlBPWeb len = " +
                              (string.IsNullOrEmpty(rdCITx.UrlBPWeb) ? "null" : rdCITx.UrlBPWeb.Length.ToString()));
                    rdCITx.CellPhone = string.IsNullOrEmpty(rdCITx.CellPhone) ? null : rdCITx.CellPhone;
                    log.Debug("AdministradorRdCITx.UpdateCITx - CellPhone = " +
                              (string.IsNullOrEmpty(rdCITx.CellPhone) ? "null" : rdCITx.CellPhone));

                    rdCITx.CarRegisterImageFront = tx.CarRegisterImageFront;
                    log.Debug("AdministradorRdCITx.UpdateCITx - CarRegisterImageFront len = " +
                             (string.IsNullOrEmpty(rdCITx.CarRegisterImageFront) ? "null" : rdCITx.CarRegisterImageFront.Length.ToString()));
                    rdCITx.CarRegisterImageBack = tx.CarRegisterImageBack;
                    log.Debug("AdministradorRdCITx.UpdateCITx - CarRegisterImageBack len = " +
                             (string.IsNullOrEmpty(rdCITx.CarRegisterImageBack) ? "null" : rdCITx.CarRegisterImageBack.Length.ToString()));
                    rdCITx.WritingImage = tx.WritingImage;
                    log.Debug("AdministradorRdCITx.UpdateCITx - WritingImage len = " +
                             (string.IsNullOrEmpty(rdCITx.WritingImage) ? "null" : rdCITx.WritingImage.Length.ToString()));
                    rdCITx.Form = tx.Form;
                    log.Debug("AdministradorRdCITx.UpdateCITx - Form len = " +
                             (string.IsNullOrEmpty(rdCITx.Form) ? "null" : rdCITx.Form.Length.ToString()));

                    rdCITx.SecurityCode = securitycode;
                    log.Debug("AdministradorRdCITx.UpdateCITx - SecurityCode = " +
                              (string.IsNullOrEmpty(rdCITx.SecurityCode) ? "null" : rdCITx.SecurityCode));

                    log.Debug("AdministradorRdCITx.UpdateCITx - Save in...");
                    using (ISession session = PersistentManager.session())
                    {
                        session.Update(rdCITx);
                        session.Flush();
                    }
                    log.Debug("AdministradorRdCITx.UpdateCITx - Save out!");
                }
                log.Debug("AdministradorRdCITx.UpdateCITx OUT!");
            }
            catch (Exception ex)
            {
                rdCITx = null;
                msg = ex.Message;
                log.Error("AdministradorRdCITx.UpdateCITx", ex);
            }
        }

        internal static int CreateNotarizeTxWeb(string trackId, v2NotarizeInitModel paramIn, BPTxCreateR bPResponse, string trackidbp, out string msgerr)
        {
            int ret = 0;
            RdCITx rdCITx = null;
            msgerr = null;
            try
            {
                log.Debug("AdministradorRdCITx.CreateNotarizeTxWeb IN...");
                rdCITx = new RdCITx();
                rdCITx.Type = 1;
                rdCITx.ActionId = 10; //DefineActionId(paramIn);
                rdCITx.CompanyId = Properties.Settings.Default.NVCompanyCI;
                rdCITx.TrackId = trackId;
                rdCITx.ValidityType = 0;
                rdCITx.DateGen = DateTime.Now;
                rdCITx.LastModify = DateTime.Now;
                rdCITx.QRGenerated = null;
                rdCITx.QRValidity = Properties.Settings.Default.QRValidity;
                rdCITx.ValidityDays = Properties.Settings.Default.CIValidityDays;
                rdCITx.TypeId = "RUT"; //paramIn.typeId;
                rdCITx.ValueId = paramIn.rut;
                rdCITx.DestinataryMail = paramIn.mail;
                rdCITx.TrackIdBP = trackidbp;
                rdCITx.UrlBPWeb = bPResponse.verifyUrl;
                rdCITx.CellPhone = null;  //paramIn.cellNumber;
                rdCITx.TaxIdCompany = null;  //paramIn.taxidcompany;
                //if (paramIn.listMailsDestinatary != null)
                //{
                //    bool first = true;
                //    rdCITx.ListMailsDistribution = "";
                //    foreach (string item in paramIn.listMailsDestinatary)
                //    {
                //        if (first)
                //        {
                //            rdCITx.ListMailsDistribution += item;
                //            first = false;
                //        }
                //        else
                //        {
                //            rdCITx.ListMailsDistribution += ";" + item;
                //        }
                //    }
                //    log.Debug("AdministradorRdCITx.CreateNotarizeTxWeb - rdCITx.ListMailsDistribution = " + rdCITx.ListMailsDistribution);
                //}

                using (ISession session = PersistentManager.session())
                {
                    session.Save(rdCITx);
                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                rdCITx = null;
                log.Error("AdministradorRdCITx.CreateNotarizeTxWeb", ex);
            }
            log.Debug("AdministradorRdCITx.CreateNotarizeTxWeb OUT!");
            return ret;
        }

        internal static int CreateNotarizeTxWeb(string trackId, Signatory paramIn, BPTxCreateR bPResponse, 
                                                string trackidbp, out string msgerr)
        {
            int ret = 0;
            RdCITx rdCITx = null;
            msgerr = null;
            try
            {
                log.Debug("AdministradorRdCITx.CreateNotarizeTxWeb IN...");
                rdCITx = new RdCITx();
                rdCITx.Type = 1;
                rdCITx.ActionId = 10; //DefineActionId(paramIn);
                rdCITx.CompanyId = Properties.Settings.Default.NVCompanyCI;
                rdCITx.TrackId = trackId;
                rdCITx.ValidityType = 0;
                rdCITx.DateGen = DateTime.Now;
                rdCITx.LastModify = DateTime.Now;
                rdCITx.QRGenerated = null;
                rdCITx.QRValidity = Properties.Settings.Default.QRValidity;
                rdCITx.ValidityDays = Properties.Settings.Default.CIValidityDays;
                rdCITx.TypeId = paramIn.typeid;
                rdCITx.ValueId = paramIn.valueid;
                rdCITx.DestinataryMail = paramIn.mail;
                rdCITx.TrackIdBP = trackidbp;
                rdCITx.UrlBPWeb = bPResponse.verifyUrl;
                rdCITx.CellPhone = null;  //paramIn.cellNumber;
                rdCITx.TaxIdCompany = null;  //paramIn.taxidcompany;
                //if (paramIn.listMailsDestinatary != null)
                //{
                //    bool first = true;
                //    rdCITx.ListMailsDistribution = "";
                //    foreach (string item in paramIn.listMailsDestinatary)
                //    {
                //        if (first)
                //        {
                //            rdCITx.ListMailsDistribution += item;
                //            first = false;
                //        }
                //        else
                //        {
                //            rdCITx.ListMailsDistribution += ";" + item;
                //        }
                //    }
                //    log.Debug("AdministradorRdCITx.CreateNotarizeTxWeb - rdCITx.ListMailsDistribution = " + rdCITx.ListMailsDistribution);
                //}

                using (ISession session = PersistentManager.session())
                {
                    session.Save(rdCITx);
                    session.Flush();
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                rdCITx = null;
                log.Error("AdministradorRdCITx.CreateNotarizeTxWeb", ex);
            }
            log.Debug("AdministradorRdCITx.CreateNotarizeTxWeb OUT!");
            return ret;
        }
    }
}