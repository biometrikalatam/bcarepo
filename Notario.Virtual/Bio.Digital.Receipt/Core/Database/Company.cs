/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Bio.Digital.Receipt.Core.Database
{
	/// <summary>
	/// RdAplicaciones object for NHibernate mapped table 'rd_aplicaciones'.
	/// </summary>
	[Serializable]
	public class Company : INotifyPropertyChanged
	{

        /*
    <property name="Domain" column="domain" type="string" not-null="true" />
    <property name="Additionaldata" column="additionaldata" type="string" not-null="false" />
    <property name="Contactname" column="contactname" type="string" not-null="true" />
    <property name="Status" column="status" type="int" not-null="true" />
    <property name="Holding" column="holding" type="string" not-null="true" />
         */
		#region Member Variables
		protected int _id;
		protected string _rut;
		
        protected string _name;
		protected string _address;
		protected string _phone;
		protected string _phone2;
		protected string _fax;
		
        protected DateTime _createDate;
        protected DateTime? _endDate;
		protected DateTime? _updateDate;
		protected string _domain;
		protected string _additionaldata;
		protected string _contactname;
        
        protected int _status;
        protected int _holding;
        private string _accessname;
        private string _secretkey;
        //protected string _bpwebtypeonboarding;
        //protected string _bpwebtypeverify;
        //protected int _manualsignature;
        protected IList<CompanyWorkflow> _companyworkflow;
        public virtual event PropertyChangedEventHandler PropertyChanged;
		
		#endregion
		#region Constructors
			
		public Company() {}
					
		public Company(string rut, string name, int holding) 
		{
			this._name= name;
			this._rut = rut;
            this._holding = holding;
		}
		
		#endregion
		#region Public Properties
		public  virtual int Id
		{
			get { return _id; }
			set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
		}
		public  virtual string Rut
		{
			get { return _rut; }
			set {
				if ( value != null && value.Length > 15)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Rut cannot contain more than 15 characters");
                if (value != this._rut) { _rut = value; NotifyPropertyChanged("Rut"); }
            }
		}
        public  virtual string Name
		{
			get { return _name; }
			set {
				if ( value != null && value.Length > 100)
					throw new ArgumentOutOfRangeException("value", value.ToString(), "Nombre cannot contain more than 100 characters");
				if (value != this._name){_name= value;NotifyPropertyChanged("Name");}}
		}

        public virtual string Address
        {
            get { return _address; }
            set
            {
                if (value != null && value.Length > 80)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Address cannot contain more than 80 characters");
                if (value != this._address) { _address = value; NotifyPropertyChanged("Address"); }
            }
        }
        public virtual string Phone
        {
            get { return _phone; }
            set
            {
                if (value != null && value.Length > 20)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Phone cannot contain more than 20 characters");
                if (value != this._phone) { _phone = value; NotifyPropertyChanged("Phone"); }
            }
        }
        public virtual string Phone2
        {
            get { return _phone2; }
            set
            {
                if (value != null && value.Length > 20)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Phone2 cannot contain more than 20 characters");
                if (value != this._phone2) { _phone2 = value; NotifyPropertyChanged("Phone2"); }
            }
        }
        public virtual string Fax
        {
            get { return _fax; }
            set
            {
                if (value != null && value.Length > 20)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Fax cannot contain more than 20 characters");
                if (value != this._fax) { _fax = value; NotifyPropertyChanged("Fax"); }
            }
        }

        public virtual DateTime CreateDate
        {
            get { return _createDate; }
            set { if (value != this._createDate) { _createDate = value; NotifyPropertyChanged("CreateDate"); } }
        }
        public virtual DateTime? EndDate
        {
            get { return _endDate; }
            set { if (value != this._endDate) { _endDate = value; NotifyPropertyChanged("EndDate"); } }
        }
        public virtual DateTime? UpdateDate
        {
            get { return _updateDate; }
            set { if (value != this._updateDate) { _updateDate = value; NotifyPropertyChanged("UpdateDate"); } }
        }

        public virtual string Domain
        {
            get { return _domain; }
            set
            {
                if (value != null && value.Length > 15)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Domain cannot contain more than 15 characters");
                if (value != this._domain) { _domain = value; NotifyPropertyChanged("Domain"); }
            }
        }
        public virtual string Additionaldata
        {
            get { return _fax; }
            set
            {
                _fax = value; 
                NotifyPropertyChanged("Additionaldata"); 
            }
        }
        public virtual string Contactname
        {
            get { return _contactname; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("value", value.ToString(), "Contactname cannot contain more than 50 characters");
                if (value != this._contactname) { _contactname = value; NotifyPropertyChanged("Contactname"); }
            }
        }

        public virtual int Status
        {
            get { return _status; }
            set { if (value != this._status) { _status = value; NotifyPropertyChanged("Status"); } }
        }
        public virtual int Holding
        {
            get { return _holding; }
            set { if (value != this._holding) { _holding = value; NotifyPropertyChanged("Holding"); } }
        }

        public virtual string AccessName
        {
            get { return _accessname; }
            set {
                if (value != null)
                    if (value.Length > 50)
                        throw new ArgumentOutOfRangeException("Invalid value for AccessName", value, value.ToString());

                //_isChanged |= (_accessname != value);
                _accessname = value;
            }
        }

        public virtual string SecretKey
        {
            get { return _secretkey; }
            set {
                if (value != null)
                    if (value.Length > 50)
                        throw new ArgumentOutOfRangeException("Invalid value for SecretKey", value, value.ToString());

                //_isChanged |= (_secretkey != value);
                _secretkey = value;
            }
        }

        //public virtual string BPWebTypeOnBoarding
        //{
        //    get { return _bpwebtypeonboarding; }
        //    set
        //    {
        //        if (value != null && value.Length > 20)
        //            throw new ArgumentOutOfRangeException("value", value.ToString(), "BPWebTypeOnBoarding cannot contain more than 20 characters");
        //        if (value != this._bpwebtypeonboarding) { _bpwebtypeonboarding = value; NotifyPropertyChanged("BPWebTypeOnBoarding"); }
        //    }
        //}
        //public virtual string BPWebTypeVerify
        //{
        //    get { return _bpwebtypeverify; }
        //    set
        //    {
        //        if (value != null && value.Length > 20)
        //            throw new ArgumentOutOfRangeException("value", value.ToString(), "BPWebTypeVerify cannot contain more than 20 characters");
        //        if (value != this._bpwebtypeverify) { _bpwebtypeverify = value; NotifyPropertyChanged("BPWebTypeVerify"); }
        //    }
        //}
        //public virtual int ManualSignature
        //{
        //    get { return _manualsignature; }
        //    set { if (value != this._manualsignature) { _manualsignature = value; NotifyPropertyChanged("ManualSignature"); } }
        //}
        public virtual IList<CompanyWorkflow> CompanyWorkflow
        {
            get { return _companyworkflow; }
            set { _companyworkflow = value; }
        }
        protected virtual void NotifyPropertyChanged(String info)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(info));
			}
		}
		
		#endregion
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
			RdAplicaciones castObj = (RdAplicaciones)obj;
			return ( castObj != null ) &&
			this._id == castObj.Id;
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57;
			hash = 27 * hash * _id.GetHashCode();
			return hash;
		}
		#endregion
		
	}
}
