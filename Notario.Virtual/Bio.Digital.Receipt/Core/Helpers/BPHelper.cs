﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Bio.Core.Api;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using Newtonsoft.Json;
using RestSharp;

namespace Bio.Digital.Receipt.Core.Helpers
{

    /// <summary>
    /// 
    /// </summary>
    public class BPHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(BPHelper));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="msg"></param>
        /// <param name="resultverify"></param>
        /// <param name="score"></param>
        /// <param name="trackidbp"></param>
        /// <returns></returns>
        internal static int VerifyCIvsCedula(XmlParamIn oXmlIn, 
              out string msg, out int resultverify, out double score, out string trackidbp)
        {
            //msg = null;
            //resultverify = 1;
            //score = 0.1;
            //trackidbp = "1950ac4827c14334a807c3be19dd470a";
            //return 0;
            int ret = 0;
            resultverify = 2;
            score = -1;
            trackidbp = null;
            msg = null;
            try
            {
                LOG.Debug("BPHelper.VerifyCIvsCedula IN...");
                BioPortal.Server.Api.XmlParamOut paramout = null;
                XmlParamIn xmlParamIn = new XmlParamIn();
                LOG.Debug("BPHelper.VerifyCIvsCedula Verificando " + oXmlIn.CIParam.TypeId + " " + oXmlIn.CIParam.ValueId);
                using (BPWSPlugin.BioPortal_Server_Plugin_Process ws = new BPWSPlugin.BioPortal_Server_Plugin_Process())
                {
                    ws.Url = Properties.Settings.Default.Bio_Digital_Receipt_BPWSPlugin_BioPortal_Server_Plugin_Process;
                    ws.Timeout = Properties.Settings.Default.BPTimeout;
                    BioPortal.Server.Api.XmlParamIn paramin = new BioPortal.Server.Api.XmlParamIn();
                    paramin.Actionid = Properties.Settings.Default.CIBPActionId; // 1201;
                    paramin.Companyid = Properties.Settings.Default.CIBPCompanyId;
                    paramin.Clientid = "NVCI"; // oXmlIn.Clientid;
                    paramin.Origin = Properties.Settings.Default.CIBPOrigin;
                    paramin.Authenticationfactor = 4;
                    paramin.Minutiaetype = 44;
                    paramin.Bodypart = 16;
                    paramin.OperationOrder = 1;

                    paramin.SampleCollection = new List<Sample>();

                    Sample sample2;
        #region Data para verificar con Namku

                    Sample sample = new Sample();
                    sample.Minutiaetype = 41;
                    LOG.Debug("BPHelper.VerifyCIvsCedula IDCardImage = " + oXmlIn.CIParam.IDCardImageFront);
                    sample.Data = oXmlIn.CIParam.IDCardImageFront;
                    paramin.SampleCollection.Add(sample);
                    sample2 = new Sample();
                    sample2.Minutiaetype = 41;
                    LOG.Debug("BPHelper.VerifyCIvsCedula Selfie = " + oXmlIn.CIParam.Selfie);
                    sample2.Data = oXmlIn.CIParam.Selfie;
                    paramin.SampleCollection.Add(sample2);

                    paramin.Threshold = Properties.Settings.Default.CIBPTreshold;

        #endregion Namku

                    paramin.PersonalData = new PersonalData();
                    paramin.PersonalData.Typeid = oXmlIn.CIParam.TypeId;
                    paramin.PersonalData.Valueid = oXmlIn.CIParam.ValueId;
                    paramin.PersonalData.DocImageFront = oXmlIn.CIParam.IDCardImageFront;
                    paramin.PersonalData.Selfie = oXmlIn.CIParam.Selfie;
                    CultureInfo provider = CultureInfo.InvariantCulture;
                    string format = "dd/MM/yyyy";
                    if (oXmlIn.CIParam.BirthDate.IndexOf("-") > 0) format = "dd-MM-yyyy";
                    paramin.PersonalData.Birthdate = DateTime.ParseExact(oXmlIn.CIParam.BirthDate, format, provider);
                    paramin.PersonalData.Documentexpirationdate = DateTime.ParseExact(oXmlIn.CIParam.ExpirationDate, format, provider);
                    paramin.PersonalData.Documentseriesnumber = oXmlIn.CIParam.Serial;
                    paramin.PersonalData.Name = oXmlIn.CIParam.Name;
                    paramin.PersonalData.Patherlastname = oXmlIn.CIParam.PhaterLastName;
                    paramin.PersonalData.Motherlastname = oXmlIn.CIParam.MotherLastName;
                    paramin.PersonalData.Nationality = oXmlIn.CIParam.Nacionality;
                    paramin.PersonalData.Sex = oXmlIn.CIParam.Sex;
                    paramin.PersonalData.Photography = oXmlIn.CIParam.IDCardPhotoImage;
                    paramin.PersonalData.Signatureimage = oXmlIn.CIParam.IDCardSignatureImage;
                    paramin.PersonalData.Verificationsource = "CI";

                    paramin.PersonalData.Dynamicdata = new DynamicDataItem[1];
                    DynamicDataItem item4 = new DynamicDataItem
                    {
                        key = "CompanyNVCIEnroll",
                        value = oXmlIn.Companyid.ToString()
                    };
                    //DynamicDataItem item = new DynamicDataItem();
                    //item.key = "IssueDate";
                    //item.value = !string.IsNullOrEmpty(oXmlIn.CIParam.IssueDate)? oXmlIn.CIParam.IssueDate:"Desconocida";
                    //paramin.PersonalData.Dynamicdata[0] = item;
                    //DynamicDataItem item2 = new DynamicDataItem();
                    //item2.key = "GeoRef";
                    //item2.value = !string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef)? oXmlIn.CIParam.GeoRef:"Desconocido";
                    //paramin.PersonalData.Dynamicdata[1] = item2;
                    //DynamicDataItem item3 = new DynamicDataItem();
                    //item3.key = "WorkStationID";
                    //item3.value = !string.IsNullOrEmpty(oXmlIn.CIParam.WorkStationID)? oXmlIn.CIParam.WorkStationID:"Desconocida";
                    //paramin.PersonalData.Dynamicdata[2] = item3;

                    string xmlin = BioPortal.Server.Api.XmlUtils.SerializeAnObject(paramin);
                    string xmlout;
                    DateTime start = DateTime.Now;
                    int iret = ws.Process(xmlin, out xmlout);
                    DateTime end = DateTime.Now;
                    LOG.Debug("BPHelper.VerifyCIvsCedula Tiempo = " + (end - start).TotalMilliseconds.ToString() + " - ret = " + iret); 
                    if (iret != 0)
                    {
                        ret = iret;
                        msg = "BPHelper.VerifyAndOCRIDCard - Error retornado desde BioPOrtal = " + iret;
                        LOG.Error(msg);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(xmlout))
                        {
                            ret = iret;
                            msg = "BPHelper.VerifyAndOCRIDCard - Error retornado desde BioPOrtal = " + iret;
                            LOG.Error(msg);
                        } else
                        {
                            paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
                            if (paramout != null)
                            {
                                resultverify = Convert.ToInt32(paramout.Result);
                                score = BPHelper.GetPorcentajeScore(paramout.Score, Properties.Settings.Default.CIBPTreshold);
                                trackidbp = paramout.Trackid;
                                LOG.Debug("BPHelper.VerifyCIvsCedula resultverify = " + resultverify + " - score = " + score + " - TrackId = " + trackidbp);
                            } else
                            {
                                ret = Errors.IRET_ERR_CI_ANSWER_BP_NULL;
                                msg = "BPHelper.VerifyAndOCRIDCard - PArseo XmlParamOut Null";
                                LOG.Error(msg);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                msg = "BPHelper.VerifyAndOCRIDCard Error = " + ex.Message;
                LOG.Error(msg);
            }
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oXmlIn"></param>
        /// <param name="msg"></param>
        /// <param name="mRZIDCard"></param>
        /// <param name="trackIdBP"></param>
        /// <returns></returns>
        internal static int VerifyAndOCRIDCard(XmlParamIn oXmlIn, out string msg, out CIParam mRZIDCard, out string trackIdBP)
        {
            //msg = null;
            //mRZIDCard = new CIParam();
            //mRZIDCard.TypeId = "RUT";
            //mRZIDCard.ValueId = "21284415-2";
            //mRZIDCard.Name = "Gustavo Suhit";
            //mRZIDCard.Nacionality = "ARG";
            //mRZIDCard.Sex = "M";
            //mRZIDCard.BirthDate = "28/09/1969";

            //trackIdBP = "1950ac4827c14334a807c3be19dd470a";
            //return 0;
            int ret = 0;
            msg = null;
            trackIdBP = null;
            mRZIDCard = null;
            try
            {
                LOG.Debug("BPHelper.VerifyAndOCRIDCard IN...");
                LOG.Debug("BPHelper.VerifyAndOCRIDCard oXmlIn.CIParam.TrackId = " + oXmlIn.CIParam.TrackId + "...");
                BioPortal.Server.Api.XmlParamOut paramout = null;
                XmlParamIn xmlParamIn = new XmlParamIn();

                using (BioPortalWS.BioPortalServerWS ws = new BioPortalWS.BioPortalServerWS())
                {
                    ws.Url = Properties.Settings.Default.Bio_Digital_Receipt_BioPortalWS_BioPortalServerWS;
                    ws.Timeout = Properties.Settings.Default.BPTimeout;
                    BioPortal.Server.Api.XmlParamIn paramin = new BioPortal.Server.Api.XmlParamIn();
                    paramin.Actionid = 4;
                    paramin.Companyid = Properties.Settings.Default.CIBPCompanyId;
                    paramin.Clientid = "NVCI"; //oXmlIn.Clientid;
                    paramin.Origin = Properties.Settings.Default.CIBPOrigin;
                    paramin.Authenticationfactor = 4;
                    paramin.Minutiaetype = 44;
                    paramin.Bodypart = 16;
                    paramin.Verifybyconnectorid = Properties.Settings.Default.BPConnectorMRZId; // "MRZNamku";
                    paramin.OperationOrder = 3;
                    //paramin.InsertOption = 0; //No hace enroll porque es solo reconocimeinto de datos

                    paramin.PersonalData = new PersonalData();
                    paramin.PersonalData.Typeid = oXmlIn.CIParam.TypeId;
                    paramin.PersonalData.Valueid = oXmlIn.CIParam.ValueId;
                    paramin.PersonalData.DocImageFront = oXmlIn.CIParam.IDCardImageFront;
                    paramin.PersonalData.DocImageBack = oXmlIn.CIParam.IDCardImageBack;
                    LOG.Debug("BPHelper.VerifyAndOCRIDCard oXmlIn.CIParam.TypeId/ValueId = " + oXmlIn.CIParam.TypeId + "/" + oXmlIn.CIParam.ValueId);
                    LOG.Debug("BPHelper.VerifyAndOCRIDCard oXmlIn.CIParam.IDCardImageFront = " + oXmlIn.CIParam.IDCardImageFront.Substring(0,15) + "...");

                    string xmlin = BioPortal.Server.Api.XmlUtils.SerializeAnObject(paramin);
                    string xmlout;
                    DateTime start = DateTime.Now;
                    int iret = ws.Get(xmlin, out xmlout);
                    DateTime end = DateTime.Now;
                    LOG.Debug("BPHelper.VerifyAndOCRIDCard Tiempo = " + (end - start).TotalMilliseconds.ToString() + " - ret = " + iret);
                    if (iret != 0)
                    {
                        ret = iret;
                        msg = "BPHelper.VerifyAndOCRIDCard - Error retornado desde BioPortal = " + iret;
                        if (!string.IsNullOrEmpty(xmlout))
                        {
                            paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
                            if (paramout != null)
                            {
                                msg = msg + " - MsgErr = " +
                                    (string.IsNullOrEmpty(paramout.Message) ? "S/C" : paramout.Message);
                            }
                        }
                        LOG.Error(msg); 
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(xmlout))
                        {
                            ret = Errors.IRET_ERR_CI_ANSWER_BP_NULL;
                            msg = "BPHelper.VerifyAndOCRIDCard - Retorno desde BioPOrtal Null";
                            LOG.Error(msg);
                        }
                        else
                        {
                            paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
                            if (paramout != null)
                            {
                                //Added 16-09-2020 - Recahza si RUT reconocido es distinto al pedido
                                if (!string.IsNullOrEmpty(paramout.PersonalData.Valueid) && !paramout.PersonalData.Valueid.Equals("NA") &&
                                    !string.IsNullOrEmpty(oXmlIn.CIParam.ValueId) && !oXmlIn.CIParam.ValueId.Equals("NA") && 
                                    !paramout.PersonalData.Valueid.Equals(oXmlIn.CIParam.ValueId))
                                {
                                    ret = Errors.IRET_ERR_CI_IDCARD_INCONSISTENCE_DATA_OCR_RECOGNIZED;
                                    msg = "BPHelper.VerifyAndOCRIDCard - Error de inconsistencia entre RUT pedido [" + oXmlIn.CIParam.ValueId +
                                        "] y RUT reconocido [" + paramout.PersonalData.Valueid + "]";
                                    LOG.Error(msg);
                                    return ret;
                                }

                                //Chequeo ciertos valores básicos para rechazar si no reconoció bien
                                    if (string.IsNullOrEmpty(paramout.PersonalData.Valueid) || paramout.PersonalData.Valueid.Equals("NA") || 
                                    string.IsNullOrEmpty(paramout.PersonalData.Patherlastname) ||
                                    string.IsNullOrEmpty(paramout.PersonalData.Name) ||
                                    string.IsNullOrEmpty(paramout.PersonalData.Photography)                                    )
                                {
                                    ret = Errors.IRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED;
                                    msg = "BPHelper.VerifyAndOCRIDCard - Parseo XmlParamOut Null [" + Errors.SRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED + "]";
                                    LOG.Error(msg);
                                    return ret;
                                }

                                //Added 27/11 si vienen datos en los XmlParamIn los uso, porque pueden ser valores 
                                //ingresados a mano
                                mRZIDCard = new CIParam();
                                mRZIDCard.TrackId = oXmlIn.CIParam.TrackId;
                                mRZIDCard.TypeId = string.IsNullOrEmpty(oXmlIn.CIParam.TypeId) ? 
                                                        paramout.PersonalData.Typeid : oXmlIn.CIParam.TypeId;
                                mRZIDCard.ValueId = (string.IsNullOrEmpty(oXmlIn.CIParam.ValueId) || oXmlIn.CIParam.ValueId.Equals("NA")) ? 
                                                        paramout.PersonalData.Valueid : oXmlIn.CIParam.ValueId;
                                mRZIDCard.IDCardImageFront = paramout.PersonalData.DocImageFront;
                                mRZIDCard.IDCardImageBack = paramout.PersonalData.DocImageBack;
                                mRZIDCard.BirthDate = string.IsNullOrEmpty(oXmlIn.CIParam.BirthDate) ?
                                                            paramout.PersonalData.Birthdate.ToString("dd/MM/yyyy") :
                                                            oXmlIn.CIParam.BirthDate;
                                mRZIDCard.ExpirationDate = string.IsNullOrEmpty(oXmlIn.CIParam.ExpirationDate) ?
                                                                paramout.PersonalData.Documentexpirationdate.ToString("dd/MM/yyyy") :
                                                                oXmlIn.CIParam.ExpirationDate;
                                mRZIDCard.IssueDate = paramout.PersonalData.Creation.ToString("dd/MM/yyyy");
                                mRZIDCard.Serial = string.IsNullOrEmpty(oXmlIn.CIParam.Serial) ? 
                                                            paramout.PersonalData.Documentseriesnumber : oXmlIn.CIParam.Serial;
                                mRZIDCard.Name = string.IsNullOrEmpty(oXmlIn.CIParam.Name) ? 
                                                        paramout.PersonalData.Name : oXmlIn.CIParam.Name;
                                mRZIDCard.PhaterLastName = string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName) ? 
                                                        paramout.PersonalData.Patherlastname : oXmlIn.CIParam.PhaterLastName;
                                mRZIDCard.MotherLastName = string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName) ? 
                                                        paramout.PersonalData.Motherlastname : oXmlIn.CIParam.MotherLastName;
                                mRZIDCard.Nacionality = string.IsNullOrEmpty(oXmlIn.CIParam.Nacionality) ? 
                                                        paramout.PersonalData.Nationality : oXmlIn.CIParam.Nacionality;
                                mRZIDCard.Sex = string.IsNullOrEmpty(oXmlIn.CIParam.Sex) ? 
                                                    paramout.PersonalData.Sex : oXmlIn.CIParam.Sex;
                                mRZIDCard.IDCardPhotoImage = paramout.PersonalData.Photography;
                                mRZIDCard.IDCardSignatureImage = paramout.PersonalData.Signatureimage;
                                LOG.Debug("BPHelper.VerifyAndOCRIDCard - ValueId = " + mRZIDCard.ValueId + " - Nombre = " + 
                                    (string.IsNullOrEmpty(mRZIDCard.Name)?"":mRZIDCard.Name) + " " + 
                                    (string.IsNullOrEmpty(mRZIDCard.PhaterLastName) ? "" : mRZIDCard.PhaterLastName) +
                                    (string.IsNullOrEmpty(mRZIDCard.MotherLastName) ? "" : mRZIDCard.MotherLastName));
                                trackIdBP = paramout.Trackid;
                            }
                            else
                            {
                                ret = Errors.IRET_ERR_CI_ANSWER_BP_NULL;
                                msg = "BPHelper.VerifyAndOCRIDCard - PArseo XmlParamOut Null";
                                LOG.Error(msg);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                msg = "BPHelper.VerifyAndOCRIDCard Error = " + ex.Message;
                LOG.Error(msg);
            }
            return ret;
        }

        /// <summary>
        /// Verifica NEC en server con el PDF417 y la muestra que llega (WSQ o RAW) y retorna resultado
        /// </summary>
        /// <param name="bpCompanyId"></param>
        /// <param name="valueId"></param>
        /// <param name="fingerSample"></param>
        /// <param name="pdf417"></param>
        /// <param name="threshold"></param>
        /// <param name="resultVerify"></param>
        /// <param name="score"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int VerifyCIvsCedulaNEC(int bpCompanyId, string valueId, string fingerSample, string pdf417,
                                                double threshold, out int resultVerify, out double score, out string msgerr)
        {
            int ret = 0;
            int actual = -1;
            string xmlparamout;
            resultVerify = 0;
            score = 0;
            msgerr = null;
            try
            {
                LOG.Debug("BPHelper.VerifyCIvsCedulaNEC - IN - [valueid=" + valueId + "]...");
                using (BioPortalWS.BioPortalServerWS target = new BioPortalWS.BioPortalServerWS())
                {
                    target.Timeout = Properties.Settings.Default.BPTimeout;
                    target.Url = Properties.Settings.Default.Bio_Digital_Receipt_BioPortalWS_BioPortalServerWS;
                    target.Url = "http://servicebp.biometrikalatam.com/BioPortal.Server.WS.asmx";
                    LOG.Debug("BPHelper.VerifyCIvsCedulaNEC - Conectando a URL = " + target.Url);
                    BioPortal.Server.Api.XmlParamIn pin = new BioPortal.Server.Api.XmlParamIn();
                    pin.Actionid = 1; //Bio.Core.Api.Constant.Action.ACTION_VERIFY;  //Valor 1
                    pin.Additionaldata = null;
                    pin.Authenticationfactor = 2; //Bio.Core.Api.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FINGERPRINT;  //Valor 2
                    pin.Minutiaetype = 1; //Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_NEC; //Valor 1
                    pin.Bodypart = 1; //Bio.Core.Api.Constant.BodyPart.BODYPART_DEDOPULGARDERECHO; //Valor 1
                    pin.Clientid = "IdClienteTest";
                    pin.Companyid = bpCompanyId;
                    pin.Enduser = "EnUserTest";
                    pin.Ipenduser = "127.0.0.1";
                    pin.Matchingtype = 1;
                    pin.Origin = 1;  //Luego en produccion les inidcamos un valor 

                    //Primero agrego la muestra
                    pin.SampleCollection = new List<Bio.Core.Api.Sample>();
                    Bio.Core.Api.Sample sample = new Bio.Core.Api.Sample();
                    sample.Data = fingerSample;
                    sample.Minutiaetype = 21; //Convert.ToInt32(textBox5.Text); // Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_WSQ;  //Valor 21 o RAW Valor 22
                    sample.Additionaldata = null;
                    pin.SampleCollection.Add(sample);

                    //Segundo agrego PDF417
                    Bio.Core.Api.Sample sample1 = new Bio.Core.Api.Sample();
                    sample1.Data = pdf417;
                    sample1.Minutiaetype = 25; //Bio.Core.Api.Constant.MinutiaeType.MINUTIAETYPE_PDF417CEDULA;  //Valor 25
                    sample1.Additionaldata = null;
                    pin.SampleCollection.Add(sample1);

                    pin.SaveVerified = 1;
                    pin.Threshold = threshold;
                    pin.Userid = 0;
                    pin.Verifybyconnectorid = "DMANec";
                    pin.InsertOption = 1;
                    pin.PersonalData = new Bio.Core.Api.PersonalData();
                    pin.PersonalData.Typeid = "RUT";
                    pin.PersonalData.Valueid = valueId;
                    pin.OperationOrder = 3; //Bio.Core.Api.Constant.OperationOrder.OPERATIONORDER_REMOTEONLY; //Valor 3

                    string xmlparamin = XmlUtils.SerializeAnObject(pin);
                    LOG.Debug("BPHelper.VerifyCIvsCedulaNEC - Calling target.Verify...");
                    actual = target.Verify(xmlparamin, out xmlparamout);
                    LOG.Debug("BPHelper.VerifyCIvsCedulaNEC - Return target.Verify => ret = " + actual.ToString());

                    if (actual == 0 && !string.IsNullOrEmpty(xmlparamout))
                    {
                        LOG.Debug("BPHelper.VerifyCIvsCedulaNEC - Procesando respuesta...");
                        BioPortal.Server.Api.XmlParamOut paramout = XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlparamout);
                        if (paramout != null)
                        {
                            if (paramout.Score >= threshold)
                            {
                                LOG.Debug("BPHelper.VerifyCIvsCedulaNEC - Verify POSITIVO [Score=" + paramout.Score + " / Treshold=" + threshold.ToString()); 
                                resultVerify = 1; 
                            } else
                            {
                                LOG.Debug("BPHelper.VerifyCIvsCedulaNEC - Verify NEGATIVO [Score=" + paramout.Score + " / Treshold=" + threshold.ToString());
                                resultVerify = 2;
                            }
                            score = paramout.Score;
                            ret = 0;
                        }
                    } else
                    {
                        LOG.Error("BPHelper.VerifyCIvsCedulaNEC - Ret BP = " + actual.ToString());
                        if (!string.IsNullOrEmpty(xmlparamout))
                        {
                            BioPortal.Server.Api.XmlParamOut paramout = XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlparamout);
                            LOG.Error("BPHelper.VerifyCIvsCedulaNEC - Error infomrado desde BP => " + (paramout!=null?paramout.Message:"Null"));
                            msgerr = (paramout != null ? paramout.Message : "Null");
                        } else
                        {
                            LOG.Error("BPHelper.VerifyCIvsCedulaNEC - Error infomrado desde BP => NULL");
                            msgerr = "NULL";
                        }
                        ret = Errors.IRET_ERR_CALLING_PROVIDER_SERVICE;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("BPHelper.VerifyCIvsCedulaNEC - Error = " + ex.Message);
            }
            return ret;
        }

        internal static int VerifyCIvsBD(XmlParamIn oXmlIn, out string msg, out int resultverify, 
            out double score, out string trackidbp, out CIParam ciParamOut)
        {
            int ret = 0;
            resultverify = 2;
            score = -1;
            trackidbp = null;
            msg = null;
            ciParamOut = null;
            try
            {
                LOG.Debug("BPHelper.VerifyCIvsBD IN...");
                BioPortal.Server.Api.XmlParamOut paramout = null;
                XmlParamIn xmlParamIn = new XmlParamIn();
                LOG.Debug("BPHelper.VerifyCIvsBD Verificando " + oXmlIn.CIParam.TypeId + " " + oXmlIn.CIParam.ValueId);
                using (BioPortalWS.BioPortalServerWS ws = new BioPortalWS.BioPortalServerWS())
                {
                    ws.Url = Properties.Settings.Default.Bio_Digital_Receipt_BioPortalWS_BioPortalServerWS;
                    ws.Timeout = Properties.Settings.Default.BPTimeout;
                    BioPortal.Server.Api.XmlParamIn paramin = new BioPortal.Server.Api.XmlParamIn();
                    paramin.Actionid = 9; // Properties.Settings.Default.CIBPActionId; // 1201;
                    paramin.Companyid = Properties.Settings.Default.CIBPCompanyId;
                    paramin.Clientid = "NVCI"; // oXmlIn.Clientid;
                    paramin.Origin = Properties.Settings.Default.CIBPOrigin;
                    paramin.Authenticationfactor = 4;
                    paramin.Minutiaetype = 44;
                    paramin.Bodypart = 16;
                    paramin.OperationOrder = 1;

                    paramin.SampleCollection = new List<Sample>();

        #region Data para verificar con Namku

                    Sample sample = new Sample();
                    sample.Minutiaetype = 41;
                    LOG.Debug("BPHelper.VerifyCIvsBD Selfie = " + oXmlIn.CIParam.Selfie);
                    sample.Data = oXmlIn.CIParam.Selfie;
                    paramin.SampleCollection.Add(sample);
                    paramin.Threshold = Properties.Settings.Default.CIBPTreshold;

        #endregion Namku

                    paramin.PersonalData = new PersonalData();
                    paramin.PersonalData.Typeid = oXmlIn.CIParam.TypeId;
                    paramin.PersonalData.Valueid = oXmlIn.CIParam.ValueId;
                    
                    //paramin.PersonalData.Dynamicdata = new DynamicDataItem[2];
                    //DynamicDataItem item2 = new DynamicDataItem();
                    //item2.key = "GeoRef";
                    //item2.value = !string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef) ? oXmlIn.CIParam.GeoRef : "Desconocido";
                    //paramin.PersonalData.Dynamicdata[0] = item2;
                    //DynamicDataItem item3 = new DynamicDataItem();
                    //item3.key = "WorkStationID";
                    //item3.value = !string.IsNullOrEmpty(oXmlIn.CIParam.WorkStationID) ? oXmlIn.CIParam.WorkStationID : "Desconocida";
                    //paramin.PersonalData.Dynamicdata[1] = item3;

                    string xmlin = BioPortal.Server.Api.XmlUtils.SerializeAnObject(paramin);
                    string xmlout;
                    DateTime start = DateTime.Now;
                    int iret = ws.Verify(xmlin, out xmlout);
                    DateTime end = DateTime.Now;
                    LOG.Debug("BPHelper.VerifyCIvsBD Tiempo = " + (end - start).TotalMilliseconds.ToString() + " - ret = " + iret);
                    if (iret != 0)
                    {
                        ret = iret;
                        msg = "BPHelper.VerifyCIvsBD - Error retornado desde BioPOrtal = " + iret;
                        LOG.Error(msg);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(xmlout))
                        {
                            ret = iret;
                            msg = "BPHelper.VerifyCIvsBD - Error retornado desde BioPOrtal = " + iret;
                            LOG.Error(msg);
                        }
                        else
                        {
                            paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
                            if (paramout != null)
                            {
                                resultverify = Convert.ToInt32(paramout.Result);
                                score = BPHelper.GetPorcentajeScore(paramout.Score, Properties.Settings.Default.CIBPTreshold);
                                trackidbp = paramout.Trackid;
                                ciParamOut = oXmlIn.CIParam;
                                //Si match positivo y viene data => LLeno CIPAram para salida
                                if (resultverify == 1 && paramout.PersonalData!=null)
                                {
                                    ciParamOut.Name = paramout.PersonalData.Name;
                                    ciParamOut.PhaterLastName = paramout.PersonalData.Patherlastname;
                                    ciParamOut.MotherLastName = paramout.PersonalData.Motherlastname;
                                    ciParamOut.BirthDate = paramout.PersonalData.Birthdate.ToString("dd/MM/yyyy");
                                    ciParamOut.ExpirationDate = paramout.PersonalData.Documentexpirationdate.ToString("dd/MM/yyyy");
                                    ciParamOut.IDCardImageFront = paramout.PersonalData.DocImageFront;
                                    ciParamOut.IDCardImageBack = paramout.PersonalData.DocImageBack;
                                    ciParamOut.IDCardPhotoImage = oXmlIn.CIParam.Selfie; // paramout.PersonalData.Photography;
                                    ciParamOut.IDCardSignatureImage = paramout.PersonalData.Signatureimage;
                                    ciParamOut.Nacionality = paramout.PersonalData.Nationality;
                                    ciParamOut.Score = paramout.Score.ToString();
                                    ciParamOut.Threshold = Properties.Settings.Default.CIBPTreshold.ToString();
                                    ciParamOut.Sex = paramout.PersonalData.Sex;
                                    ciParamOut.Serial = paramout.PersonalData.Documentseriesnumber;
                                    //ciParamOut. = paramout.PersonalData;
                                }
                                LOG.Debug("BPHelper.VerifyCIvsBD resultverify = " + resultverify + " - score = " + score + " - TrackId = " + trackidbp);
                            }
                            else
                            {
                                ret = Errors.IRET_ERR_CI_ANSWER_BP_NULL;
                                msg = "BPHelper.VerifyCIvsBD - PArseo XmlParamOut Null";
                                LOG.Error(msg);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                msg = "BPHelper.VerifyCIvsBD Error = " + ex.Message;
                LOG.Error(msg);
            }
            return ret;
        }

        internal static int CheckEnrolled(XmlParamIn oXmlIn)
        {
            int ret = 0;
            try
            {
                CIParam ciParamOut;
                string verficationsource;
                LOG.Debug("BPHelper.CheckEnrolled IN...");
                int retget = GetEnrolled(oXmlIn, out ciParamOut, out verficationsource);

                if (retget < 0) //Si da error, por default digo que esta NO Enrolado, para ir a la segura
                {
                    ret = 0;
                } else
                {
                    LOG.Debug("BPHelper.CheckEnrolled verficationsource = " + verficationsource);
                    VerificationSource vs = XmlUtils.DeserializeObject<VerificationSource>(verficationsource);
                    if (vs == null)
                    {
                        ret = 0;
                    } else
                    {
                        ret = vs.IsIdentityValidated(Bio.Core.Matcher.Constant.AuthenticationFactor.AUTHENTICATIONFACTOR_FACIAL) ? 1 : 0;
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("BPHelper.CheckEnrolled Error", ex);
                ret = 0;
            }
            LOG.Debug("BPHelper.CheckEnrolled OUT! [Ret = " + ret + "]");
            return ret;
        }

        internal static int GetEnrolled(XmlParamIn oXmlIn, out CIParam ciParamOut, out string verficationsource)
        {
            string msg;
            int ret = 0;
            ciParamOut = null;
            verficationsource = null;
            try
            {
                LOG.Debug("BPHelper.GetEnrolled IN...");
                BioPortal.Server.Api.XmlParamOut paramout = null;
                //XmlParamIn xmlParamIn = new XmlParamIn();
                LOG.Debug("BPHelper.GetEnrolled Verificando " + oXmlIn.CIParam.TypeId + " " + oXmlIn.CIParam.ValueId);
                using (BioPortalWS.BioPortalServerWS ws = new BioPortalWS.BioPortalServerWS())
                {
                    ws.Url = Properties.Settings.Default.Bio_Digital_Receipt_BioPortalWS_BioPortalServerWS;
                    ws.Timeout = Properties.Settings.Default.BPTimeout;
                    BioPortal.Server.Api.XmlParamIn paramin = new BioPortal.Server.Api.XmlParamIn();
                    paramin.Actionid = Bio.Core.Matcher.Constant.Action.ACTION_GET; // Properties.Settings.Default.CIBPActionId; // 1201;
                    paramin.Companyid = Properties.Settings.Default.CIBPCompanyId;
                    paramin.Clientid = "NVCI"; // oXmlIn.Clientid;
                    paramin.Origin = Properties.Settings.Default.CIBPOrigin;
                    paramin.OperationOrder = 1;
                    paramin.PersonalData = new PersonalData();
                    paramin.PersonalData.Typeid = oXmlIn.CIParam.TypeId;
                    paramin.PersonalData.Valueid = oXmlIn.CIParam.ValueId;

                    string xmlin = BioPortal.Server.Api.XmlUtils.SerializeAnObject(paramin);
                    string xmlout;
                    DateTime start = DateTime.Now;
                    int iret = ws.Get(xmlin, out xmlout);
                    DateTime end = DateTime.Now;
                    LOG.Debug("BPHelper.GetEnrolled Tiempo = " + (end - start).TotalMilliseconds.ToString() + " - ret = " + iret);
                    if (iret != 0)
                    {
                        ret = iret;
                        msg = "BPHelper.GetEnrolled - Error retornado desde BioPortal = " + iret;
                        LOG.Error(msg);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(xmlout))
                        {
                            ret = iret;
                            msg = "BPHelper.GetEnrolled - Retorno Nulo desde BioPortal!";
                            LOG.Error(msg);
                        }
                        else
                        {
                            paramout = BioPortal.Server.Api.XmlUtils.DeserializeObject<BioPortal.Server.Api.XmlParamOut>(xmlout);
                            if (paramout != null)
                            {
                                ciParamOut = (CIParam)oXmlIn.CIParam.Clone();
                                //Si viene data => Lleno CIPAram para salida
                                if (paramout.PersonalData != null)
                                {
                                    ciParamOut.Name = paramout.PersonalData.Name;
                                    ciParamOut.PhaterLastName = paramout.PersonalData.Patherlastname;
                                    ciParamOut.MotherLastName = paramout.PersonalData.Motherlastname;
                                    ciParamOut.BirthDate = paramout.PersonalData.Birthdate.ToString("dd/MM/yyyy");
                                    ciParamOut.ExpirationDate = paramout.PersonalData.Documentexpirationdate.ToString("dd/MM/yyyy");
                                    ciParamOut.IDCardImageFront = paramout.PersonalData.DocImageFront;
                                    ciParamOut.IDCardImageBack = paramout.PersonalData.DocImageBack;
                                    ciParamOut.IDCardPhotoImage = paramout.PersonalData.Photography;
                                    ciParamOut.IDCardSignatureImage = paramout.PersonalData.Signatureimage;
                                    ciParamOut.Nacionality = paramout.PersonalData.Nationality;
                                    ciParamOut.Score = paramout.Score.ToString();
                                    ciParamOut.Threshold = Properties.Settings.Default.CIBPTreshold.ToString();
                                    ciParamOut.Sex = paramout.PersonalData.Sex;
                                    ciParamOut.Serial = paramout.PersonalData.Documentseriesnumber;
                                    verficationsource = paramout.PersonalData.Verificationsource;
                                }
                                LOG.Debug("BPHelper.VerifyCIvsBD Verificationsource = " + verficationsource);
                            }
                            else
                            {
                                ret = Errors.IRET_ERR_CI_ANSWER_BP_NULL;
                                msg = "BPHelper.GetEnrolled - Parseo XmlParamOut Null";
                                LOG.Error(msg);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                msg = "BPHelper.GetEnrolled Error = " + ex.Message;
                LOG.Error(msg);
            }
            return ret;
        }

        internal static double GetPorcentajeScore(double score, double treshold)
        {
            double ret = 0;
            double _BASE = 70;
            try
            {
                try
                {
                    _BASE = Properties.Settings.Default.CIScorePorcentajeBase;
                    if (_BASE == 0) _BASE = 70;
                } catch (Exception ex1)
                {
                    LOG.Error("BPHelper.GetPorcentajeScore - "+ 
                        "Falta configurar o mal configurado en web.config CIScorePorcentajeBase [Error="+
                        ex1.Message + "] => Default=70");
                }

                LOG.Debug("BPHelper.GetPorcentajeScore IN...");
                LOG.Debug("BPHelper.GetPorcentajeScore score/treshold => " + score + "/" + treshold);

                //double rts = (25 * (treshold - score)) / treshold;
                double rts = ((100 - _BASE) * (treshold - score)) / treshold;

                //ret = 75 + rts;
                ret = _BASE + rts;
                
                LOG.Debug("BPHelper.GetPorcentajeScore Porcentaje Seguridad => " + ret.ToString("##.##") + "%");
            }
            catch (Exception ex)
            {
                ret = Properties.Settings.Default.CIScorePorcentajeBase; // 75;
                LOG.Error("BPHelper.GetPorcentajeScore Error => " + ex.Message);
            }
            LOG.Debug("BPHelper.GetPorcentajeScore OUT!");
            return ret;
        }

#region API Rest x BPWeb

        public static int TxCreateSigner(string trackidCI, SignerCreateModel pin, out string msgerr,
                                   out string listMails, out string trackidbp, out BPTxCreateR bptxresponse)
        {
            int ret = 0;
            msgerr = null;
            BPResponseTxCreate bpr = null;
            bptxresponse = null;
            trackidbp = null;
            listMails = null;
            try
            {
                LOG.Debug("BPHelper.TxCreateSigner IN...");
                //Added 12-08-2020
                //Chequeo que la persona este enrolada. 
                //  - Si no lo está => Genero enroll combined
                //  - Si está => Genero verify como pide los parametros del OB o Auth segun Company
                BPUserEnrolledR bpresponse;
                LOG.Debug("BPHelper.TxCreateSigner - Ingresando a CheckIdentityEnrolled con => " +
                          Properties.Settings.Default.CIBPCompanyId + "/" + pin.typeId + "/" + pin.valueId);
                ret = CheckIdentityEnrolled(Properties.Settings.Default.CIBPCompanyId, pin.typeId, pin.valueId,
                                            out bpresponse, out msgerr);
                LOG.Debug("BPHelper.TxCreateSigner - CheckIdentityEnrolled ret = " + ret);
                //Si no hubo error o bien no encontro que este enrolado => Enrolo
                if ((ret == 0 || ret == Errors.IRET_ERR_DONT_CHECK_ENROLLED)) // && bpresponse != null)
                {
                    LOG.Debug("BPHelper.TxCreateSigner - Ingresa a crear TX segun parámetros y workflow pedido = " +
                        pin.company + "/" + pin.workflowname);
                    //Si no hubo error analizo la informacion retornada para determinar los pasos a hacer 
                    // en conjunto con el workflow 
                    Database.CompanyWorkflow cw = Database.AdministradorCompanyWorkflow.
                                        BuscarCompanyWokflowByName(pin.company, pin.workflowname);

                    if (cw == null)
                    {
                        LOG.Warn("BPHelper.TxCreateSigner - Workflow " + pin.company + "/" + pin.workflowname + " no encontrado!. Sale con error " +
                                 Errors.IRET_ERR_WORKFLOW_DONT_EXIST);
                        return Errors.IRET_ERR_WORKFLOW_DONT_EXIST;
                    }
                    listMails = cw.ReceiverList; //Retorno lista de mails a enviar notify si se debe enviar

                    //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    //var client = new RestClient("https://qabpservice5.9.biometrikalatam.com/api/" + "txCreate");
                    LOG.Debug("BPHelper.TxCreateSigner - LLama a " + Properties.Settings.Default.BPWebUrlBase + "txCreate...");
                    var client = new RestClient(Properties.Settings.Default.BPWebUrlBase + "txCreate");
                    client.Timeout = Properties.Settings.Default.BPTimeout;
                    var request = new RestRequest(Method.POST);
                    client.UserAgent = "biometrika";
                    request.AddHeader("Authorization", "Basic " +
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.BPAccessName + ":" +
                                                                            Properties.Settings.Default.BPSecretKey)));
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("Accept", "application/json");
                    //int iaux = pin.company;
                    //pin.company = Properties.Settings.Default.CIBPCompanyId;
                    //string jsonParams = JsonConvert.SerializeObject(pin);
                    //pin.company = iaux;

                    BPTxCreate paramInBPTxCreate = new BPTxCreate();
                    paramInBPTxCreate.callbackurl = pin.callbackUrl;
                    paramInBPTxCreate.company = Properties.Settings.Default.CIBPCompanyId;
                    paramInBPTxCreate.customertrackid = trackidCI;

                    paramInBPTxCreate.redirecturl = pin.redirectUrl;
                    //paramInBPTxCreate.signerinclude = pin.signerInclude;
                    paramInBPTxCreate.theme = pin.theme;
                    paramInBPTxCreate.threshold = pin.threshold;
                    paramInBPTxCreate.typeid = pin.typeId;
                    paramInBPTxCreate.valueid = pin.valueId;
                    paramInBPTxCreate.taxidcompany = pin.taxidcompany;

                    //Check para ver que seteamos
                    //Pongo que no sea mandatorio porque es NV. Si no está enrolado en BP se coloca por default 1 
                    paramInBPTxCreate.onboardingmandatory = cw.OnBoardingMandatory;
                    LOG.Debug("BPHelper.TxCreateSigner - Set paramInBPTxCreate.onboardingmandatory = " + paramInBPTxCreate.onboardingmandatory);
                    //Si esta enrolado => Auth sino enroll combined
                    if (bpresponse == null)
                    {
                        paramInBPTxCreate.onboardingtype = 1; //NO esta enrolado => Fuerzo Onboarding
                    }
                    else if (bpresponse.exist == 1 && bpresponse.isenrolledtv3dplus == 1) //&& bpresponse.isenrolledft
                    {
                        paramInBPTxCreate.onboardingtype = 0; //Ya esta enrolado => Pongo Auth
                    }
                    else
                    {
                        paramInBPTxCreate.onboardingtype = 1; //NO esta enrolado => Fuerzo Onboarding
                    }
                    LOG.Debug("BPHelper.TxCreateSigner - Set paramInBPTxCreate.onboardingtype = " + paramInBPTxCreate.onboardingtype);
                    paramInBPTxCreate.typeverifyonboarding = cw.BPWebTypeOnboarding; // (com == null) ? Properties.Settings.Default.CIBPWebTypeOnBoarding : com.BPWebTypeOnBoarding;
                    LOG.Debug("BPHelper.TxCreateSigner - Set paramInBPTxCreate.typeverifyonboarding = " + paramInBPTxCreate.typeverifyonboarding);
                    paramInBPTxCreate.typeverifyauth = cw.BPWebTypeVerify; // (com == null) ? Properties.Settings.Default.CIBPWebTypeAuth : com.BPWebTypeVerify;
                    LOG.Debug("BPHelper.TxCreateSigner - Set paramInBPTxCreate.typeverifyauth = " + paramInBPTxCreate.typeverifyauth);
                    //paramInBPTxCreate.onboardingtype = 1; //Siempre lo mando en Combined, por si se usa

                    paramInBPTxCreate.signerinclude = cw.SignerInclude;
                    paramInBPTxCreate.autorizationinclude = cw.Autorization;
                    LOG.Debug("BPHelper.TxCreateSigner - Set paramInBPTxCreate.autorizationinclude = " + paramInBPTxCreate.autorizationinclude);
                    paramInBPTxCreate.autorizationmessage = cw.AutorizationMessage;
                    paramInBPTxCreate.checklocksrcei = cw.CheckLockSRCeI;
                    paramInBPTxCreate.videoinclude = cw.VideoInclude;
                    paramInBPTxCreate.videomessage = cw.VideoMessage;
                    paramInBPTxCreate.checkadult = cw.CheckAdult;
                    paramInBPTxCreate.checkexpired = cw.CheckExpired;
                    LOG.Debug("BPHelper.TxCreateSigner - Set paramInBPTxCreate.georefmandatory = " + cw.GeoRefMandatory);
                    paramInBPTxCreate.georefmandatory = cw.GeoRefMandatory;
                    paramInBPTxCreate.redirecturl = cw.RedirectUrl;
                    paramInBPTxCreate.titlebpweb = "Certificacion de Proceso";
                    //Database.Company c = Database.AdministradorCompany.BuscarCompanyById(pin.company);
                    Database.Company com = Database.AdministradorCompany.BuscarCompanyById(pin.company);
                    string _companyName = (com == null ? "" : com.Name);
                    paramInBPTxCreate.messagebpweb = "Nuestro cliente " + _companyName + " encargo la certificacion de identidad de " +
                                                     pin.typeId + " " + pin.valueId;
                    paramInBPTxCreate.theme = cw.Theme;
                    paramInBPTxCreate.successurl = cw.SuccessUrl;
                    paramInBPTxCreate.carregisterinclude = cw.CarRegisterInclude;
                    paramInBPTxCreate.writinginclude = cw.WritingInclude;
                    if (!string.IsNullOrEmpty(pin.taxidcompany))
                    {
                        paramInBPTxCreate.formid = cw.FormInclude + "_empresa";
                    }
                    else
                    {
                        paramInBPTxCreate.formid = cw.FormInclude;
                    }
                    string jsonParams = JsonConvert.SerializeObject(paramInBPTxCreate);
                    request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                    LOG.Debug("BPHelper.TxCreateSigner - call execute...");
                    IRestResponse response = client.Execute(request);

                    if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
                        && !string.IsNullOrEmpty(response.Content))
                    {
                        LOG.Debug("BPHelper.TxCreateSigner - procesando response...");
                        bpr = JsonConvert.DeserializeObject<BPResponseTxCreate>(response.Content);
                        LOG.Debug("BPHelper.TxCreateSigner - BPResponseTxCreate != null => " + (bpr != null).ToString());
                    }
                    else
                    {
                        LOG.Warn("BPHelper.TxCreate - response != null => " + (response != null).ToString() + " - " +
                                 ((response != null) ? response.StatusCode.ToString() : "null"));
                    }

                    if (bpr == null)
                    {
                        bptxresponse = null;
                        ret = -1;
                    }
                    else
                    {
                        ret = 0; //limpio salida para que no de error en el controller
                        LOG.Debug("BPHelper.TxCreate - Set salida urlservice = " + bpr.urlservice);
                        trackidbp = bpr.trackid;
                        bptxresponse = new BPTxCreateR(bpr.urlservice);
                    }
                }
                else if (ret < 0 || bpresponse == null) //Salgo e informo
                {
                    msgerr = "Chequeo de enrolamiento con problemas => ret = " + ret +
                             " - (bpresponse!=null)=" + (bpresponse != null).ToString();
                    LOG.Warn("BPHelper.TxCreate - Chequeo de enrolamiento con problemas => ret = " + ret +
                             " - (bpresponse!=null)=" + (bpresponse != null).ToString());
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "TxCreate Excp: " + ex.Message;
                LOG.Error("BPHelper.TxCreate Excp: " + ex.Message);
            }
            LOG.Debug("BPHelper.TxCreate OUT!");
            return ret;
        }

        public static int TxCreate(string trackidCI, CICreateModel pin, out string msgerr, 
                                   out string listMails, out string trackidbp, out BPTxCreateR bptxresponse)
        {
            int ret = 0;
            msgerr = null;
            BPResponseTxCreate bpr = null;
            bptxresponse = null;
            trackidbp = null;
            listMails = null;
            try
            {
                LOG.Debug("BPHelper.TxCreate IN...");
                //Added 12-08-2020
                //Chequeo que la persona este enrolada. 
                //  - Si no lo está => Genero enroll combined
                //  - Si está => Genero verify como pide los parametros del OB o Auth segun Company
                BPUserEnrolledR bpresponse;
                LOG.Debug("BPHelper.TxCreate - Ingresando a CheckIdentityEnrolled con => " +
                          Properties.Settings.Default.CIBPCompanyId + "/" + pin.typeId + "/" + pin.valueId);
                ret = CheckIdentityEnrolled(Properties.Settings.Default.CIBPCompanyId, pin.typeId, pin.valueId, 
                                            out bpresponse, out msgerr);
                LOG.Debug("BPHelper.TxCreate - CheckIdentityEnrolled ret = " + ret);
                //Si no hubo error o bien no encontro que este enrolado => Enrolo
                if ((ret == 0 || ret == Errors.IRET_ERR_DONT_CHECK_ENROLLED)) // && bpresponse != null)
                {
                    LOG.Debug("BPHelper.TxCreate - Ingresa a crear TX segun parámetros y workflow pedido = " +
                        pin.company + "/" + pin.workflowname);
                    //Si no hubo error analizo la informacion retornada para determinar los pasos a hacer 
                    // en conjunto con el workflow 
                    Database.CompanyWorkflow cw = Database.AdministradorCompanyWorkflow.
                                        BuscarCompanyWokflowByName(pin.company, pin.workflowname);

                    if (cw == null)
                    {
                        LOG.Warn("BPHelper.TxCreate - Workflow " + pin.company + "/" + pin.workflowname + " no encontrado!. Sale con error " +
                                 Errors.IRET_ERR_WORKFLOW_DONT_EXIST);
                        return Errors.IRET_ERR_WORKFLOW_DONT_EXIST;
                    }
                    listMails = cw.ReceiverList; //Retorno lista de mails a enviar notify si se debe enviar

                    //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    //var client = new RestClient("https://qabpservice5.9.biometrikalatam.com/api/" + "txCreate");
                    LOG.Debug("BPHelper.TxCreate - LLama a " + Properties.Settings.Default.BPWebUrlBase + "txCreate...");
                    var client = new RestClient(Properties.Settings.Default.BPWebUrlBase + "txCreate");
                    client.Timeout = Properties.Settings.Default.BPTimeout;
                    var request = new RestRequest(Method.POST);
                    client.UserAgent = "biometrika";
                    request.AddHeader("Authorization", "Basic " + 
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.BPAccessName + ":" +
                                                                            Properties.Settings.Default.BPSecretKey)));
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("Accept", "application/json");
                    //int iaux = pin.company;
                    //pin.company = Properties.Settings.Default.CIBPCompanyId;
                    //string jsonParams = JsonConvert.SerializeObject(pin);
                    //pin.company = iaux;

                    BPTxCreate paramInBPTxCreate = new BPTxCreate();
                    paramInBPTxCreate.callbackurl = pin.callbackUrl;
                    paramInBPTxCreate.company = Properties.Settings.Default.CIBPCompanyId;
                    paramInBPTxCreate.customertrackid = trackidCI;

                    paramInBPTxCreate.redirecturl = pin.redirectUrl;
                    paramInBPTxCreate.successurl = pin.successUrl;
                    //paramInBPTxCreate.signerinclude = pin.signerInclude;
                    paramInBPTxCreate.theme = pin.theme;
                    paramInBPTxCreate.threshold = pin.threshold;
                    paramInBPTxCreate.typeid = pin.typeId;
                    paramInBPTxCreate.valueid = pin.valueId;

                    //Check para ver que seteamos
                    //Pongo que no sea mandatorio porque es NV. Si no está enrolado en BP se coloca por default 1 
                    paramInBPTxCreate.onboardingmandatory = cw.OnBoardingMandatory;
                    LOG.Debug("BPHelper.TxCreate - Set paramInBPTxCreate.onboardingmandatory = " + paramInBPTxCreate.onboardingmandatory);
                    //Si esta enrolado => Auth sino enroll combined
                    if (bpresponse == null)
                    {
                        paramInBPTxCreate.onboardingtype = 1; //NO esta enrolado => Fuerzo Onboarding
                    } else if (bpresponse.exist == 1 && bpresponse.isenrolledtv3dplus == 1) //&& bpresponse.isenrolledft
                    {
                        paramInBPTxCreate.onboardingtype = 0; //Ya esta enrolado => Pongo Auth
                    }
                    else
                    {
                        paramInBPTxCreate.onboardingtype = 1; //NO esta enrolado => Fuerzo Onboarding
                    }
                    LOG.Debug("BPHelper.TxCreate - Set paramInBPTxCreate.onboardingtype = " + paramInBPTxCreate.onboardingtype);
                    paramInBPTxCreate.typeverifyonboarding = cw.BPWebTypeOnboarding; // (com == null) ? Properties.Settings.Default.CIBPWebTypeOnBoarding : com.BPWebTypeOnBoarding;
                    LOG.Debug("BPHelper.TxCreate - Set paramInBPTxCreate.typeverifyonboarding = " + paramInBPTxCreate.typeverifyonboarding);
                    paramInBPTxCreate.typeverifyauth = cw.BPWebTypeVerify; // (com == null) ? Properties.Settings.Default.CIBPWebTypeAuth : com.BPWebTypeVerify;
                    LOG.Debug("BPHelper.TxCreate - Set paramInBPTxCreate.typeverifyauth = " + paramInBPTxCreate.typeverifyauth);
                    //paramInBPTxCreate.onboardingtype = 1; //Siempre lo mando en Combined, por si se usa

                    paramInBPTxCreate.signerinclude = cw.SignerInclude;
                    paramInBPTxCreate.autorizationinclude = cw.Autorization;
                    LOG.Debug("BPHelper.TxCreate - Set paramInBPTxCreate.autorizationinclude = " + paramInBPTxCreate.autorizationinclude);
                    paramInBPTxCreate.autorizationmessage = cw.AutorizationMessage;
                    paramInBPTxCreate.checklocksrcei = cw.CheckLockSRCeI;
                    paramInBPTxCreate.videoinclude = cw.VideoInclude;
                    paramInBPTxCreate.videomessage = cw.VideoMessage;
                    paramInBPTxCreate.checkadult = cw.CheckAdult;
                    paramInBPTxCreate.checkexpired = cw.CheckExpired;
                    LOG.Debug("BPHelper.TxCreate - Set paramInBPTxCreate.georefmandatory = " + cw.GeoRefMandatory);
                    paramInBPTxCreate.georefmandatory = cw.GeoRefMandatory;
                    paramInBPTxCreate.redirecturl = cw.RedirectUrl;
                    paramInBPTxCreate.successurl = cw.SuccessUrl;
                    paramInBPTxCreate.titlebpweb = "Certificacion de Identidad";
                    //Database.Company c = Database.AdministradorCompany.BuscarCompanyById(pin.company);
                    Database.Company com = Database.AdministradorCompany.BuscarCompanyById(pin.company);
                    string _companyName = (com == null ? "" : com.Name);
                    paramInBPTxCreate.messagebpweb = "Nuestro cliente " + _companyName + " encargo la certificacion de identidad de " +
                                                     pin.typeId + " " + pin.valueId;
                    paramInBPTxCreate.theme = cw.Theme;
                    string jsonParams = JsonConvert.SerializeObject(paramInBPTxCreate);
                    request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                    LOG.Debug("BPHelper.TxCreate - call execute...");
                    IRestResponse response = client.Execute(request);

                    if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
                        && !string.IsNullOrEmpty(response.Content))
                    {
                        LOG.Debug("BPHelper.TxCreate - procesando response...");
                        bpr = JsonConvert.DeserializeObject<BPResponseTxCreate>(response.Content);
                        LOG.Debug("BPHelper.TxCreate - BPResponseTxCreate != null => " + (bpr != null).ToString());
                    } else
                    {
                        LOG.Warn("BPHelper.TxCreate - response != null => " + (response != null).ToString() + " - " +
                                 ((response != null) ? response.StatusCode.ToString() : "null"));
                    }

                    if (bpr == null)
                    {
                        bptxresponse = null;
                        ret = -1;
                    }
                    else
                    {
                        ret = 0; //limpio salida para que no de error en el controller
                        LOG.Debug("BPHelper.TxCreate - Set salida urlservice = " + bpr.urlservice);
                        trackidbp = bpr.trackid;
                        bptxresponse = new BPTxCreateR(bpr.urlservice);
                    }
                } else if (ret < 0 || bpresponse == null) //Salgo e informo
                {
                    msgerr = "Chequeo de enrolamiento con problemas => ret = " + ret + 
                             " - (bpresponse!=null)=" + (bpresponse != null).ToString();
                    LOG.Warn("BPHelper.TxCreate - Chequeo de enrolamiento con problemas => ret = " + ret +
                             " - (bpresponse!=null)=" + (bpresponse != null).ToString());
                } 
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "TxCreate Excp: " + ex.Message;
                LOG.Error("BPHelper.TxCreate Excp: " + ex.Message);
            }
            LOG.Debug("BPHelper.TxCreate OUT!");
            return ret;
        }

        private static int CheckIdentityEnrolled(int cIBPCompanyId, string typeId, string valueId, 
                                                 out BPUserEnrolledR bpresponse, out string msgerr)
        {
            int ret = Errors.IRET_OK;
            bpresponse = null;
            msgerr = null;
            try
            {
                LOG.Debug("BPHelper.CheckIdentityEnrolled IN...");
                LOG.Debug("BPHelper.CheckIdentityEnrolled - Calling " + Properties.Settings.Default.BPWebUrlBase + "userEnrolled?" +
                    "companyid=" + cIBPCompanyId + "&typeid=" + typeId + "&valueId=" + valueId);
                var client = new RestClient(Properties.Settings.Default.BPWebUrlBase + "userEnrolled?" +
                    "companyid=" + cIBPCompanyId + "&typeid=" + typeId + "&valueId=" + valueId);
                client.Timeout = Properties.Settings.Default.BPTimeout;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "Basic " +
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.BPAccessName + ":" +
                                                                            Properties.Settings.Default.BPSecretKey)));
                client.UserAgent = "biometrika";
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                //int iaux = pin.company;
                //pin.company = Properties.Settings.Default.CIBPCompanyId;
                //string jsonParams = JsonConvert.SerializeObject(pin);
                //pin.company = iaux;

                //BPUserEnrolled paramInBPUE = new BPUserEnrolled();
                //paramInBPUE.companyid = cIBPCompanyId;
                //paramInBPUE.typeid = typeId;
                //paramInBPUE.valueid = valueId;

                //string jsonParams = JsonConvert.SerializeObject(paramInBPUE);
                //request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                LOG.Debug("BPHelper.CheckIdentityEnrolled - client.Execute...");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
                        && !string.IsNullOrEmpty(response.Content))
                {
                    LOG.Debug("BPHelper.CheckIdentityEnrolled - Procewsando respuesta...");
                    bpresponse = JsonConvert.DeserializeObject<BPUserEnrolledR>(response.Content);
                    ret = (bpresponse != null && bpresponse.code == 0) ? 0 : 
                                    (bpresponse != null ? bpresponse.code: Errors.IRET_ERR_DONT_CHECK_ENROLLED);
                    LOG.Debug("BPHelper.CheckIdentityEnrolled - ret procesado = " + ret);
                } else
                {
                    msgerr = "CheckIdentityEnrolled - Error = " + Errors.IRET_ERR_DONT_CHECK_ENROLLED;
                    LOG.Warn("BPHelper.CheckIdentityEnrolled - Error = " + Errors.IRET_ERR_DONT_CHECK_ENROLLED);
                    ret = Errors.IRET_ERR_DONT_CHECK_ENROLLED; 
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                msgerr = "CheckIdentityEnrolled Excp [" + ex.Message + "]";
                LOG.Error("BPHelper.CheckIdentityEnrolled Excp: " + ex.Message);
            }
            LOG.Debug("BPHelper.CheckIdentityEnrolled OUT");
            return ret;
        }

        public static int TxStatus(string bptrackid, out string msgerr, out BpTxDataResponse bptxresponse)
        {
            int ret = 0;
            msgerr = null;
            BPTxStatusResponse bpsr = null;
            bptxresponse = null;
            try
            {
                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //var client = new RestClient("https://qabpservice5.9.biometrikalatam.com/api/" + "txCreate");
                var client = new RestClient(Properties.Settings.Default.BPWebUrlBase + "txGetStatus?trackid=" + bptrackid);
                client.Timeout = 60000;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "Basic " +
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.BPAccessName + ":" +
                                                                            Properties.Settings.Default.BPSecretKey)));
                client.UserAgent = "biometrika";
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                IRestResponse response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
                    && !string.IsNullOrEmpty(response.Content))
                {
                    bpsr = JsonConvert.DeserializeObject<BPTxStatusResponse>(response.Content);
                }

                if (bpsr == null)
                {
                    bptxresponse = null;
                    ret = Errors.IRET_ERR_DESCONOCIDO;
                }
                else
                {
                    if (bpsr.status == 1 || bpsr.status == 2) //Si es 1 = DONE o 2 = FAILED => Recupero datos
                    {
                        client = new RestClient(Properties.Settings.Default.BPWebUrlBase + "txGetData?trackid=" + bptrackid + "&flag=2");
                        client.Timeout = 3000000;
                        request = new RestRequest(Method.GET);
                        request.AddHeader("Authorization", "Basic " +
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.BPAccessName + ":" +
                                                                            Properties.Settings.Default.BPSecretKey)));
                        client.UserAgent = "biometrika";
                        request.AddHeader("Content-Type", "application/json");
                        request.AddHeader("Accept", "application/json");
                        response = client.Execute(request);

                        if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
                            && !string.IsNullOrEmpty(response.Content))
                        {
                            bptxresponse = JsonConvert.DeserializeObject<BpTxDataResponse>(response.Content);

                            if (bptxresponse == null)
                            {
                                ret = Errors.IRET_ERR_CALLING_PROVIDER_SERVICE;
                            } else
                            {
                                ret = 0;
                            }
                        } else
                        {
                            ret = Errors.IRET_API_INTERNAL_SERVER_ERROR;
                        }
                    } else
                    {
                        ret = 0;
                        bptxresponse = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }

        internal static string GetSample(BpTxDataResponse oBpTxDataResponse, int type)
        {
            string ret = null;
            try
            {
                if (oBpTxDataResponse == null || oBpTxDataResponse.Samples == null) return null;

                foreach (ImageSample item in oBpTxDataResponse.Samples)
                {
                    if ((int)item.typeimage == type)
                    {
                        ret = item.data;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("NotarizeController.GetSample Error: " + ex.Message);
            }
            return ret;
        }

        #endregion API Rest x BPWeb

#region NvProcedure Helper

        /// <summary>
        /// Crea Tx en BPWeb
        /// </summary>
        /// <param name="trackidCI"></param>
        /// <param name="pin"></param>
        /// <param name="msgerr"></param>
        /// <param name="listMails"></param>
        /// <param name="trackidbp"></param>
        /// <param name="bptxresponse"></param>
        /// <returns></returns>
        public static int TxCreateNotarize(string trackidCI, v2NotarizeInitModel pin, out string msgerr,
                            out string listMails, out string trackidbp, out BPTxCreateR bptxresponse)
        {
            int ret = 0;
            msgerr = null;
            BPResponseTxCreate bpr = null;
            bptxresponse = null;
            trackidbp = null;
            listMails = null;
            try
            {
                LOG.Debug("BPHelper.TxCreateNotarize IN...");
                //Added 12-08-2020
                //Chequeo que la persona este enrolada. 
                //  - Si no lo está => Genero enroll combined
                //  - Si está => Genero verify como pide los parametros del OB o Auth segun Company
                BPUserEnrolledR bpresponse;
                LOG.Debug("BPHelper.TxCreateNotarize - Ingresando a CheckIdentityEnrolled con => " +
                          Properties.Settings.Default.CIBPCompanyId + "/RUT/" + pin.rut);
                ret = CheckIdentityEnrolled(Properties.Settings.Default.CIBPCompanyId, "RUT", pin.rut,
                                            out bpresponse, out msgerr);
                LOG.Debug("BPHelper.TxCreateNotarize - CheckIdentityEnrolled ret = " + ret);
                //Si no hubo error o bien no encontro que este enrolado => Enrolo
                if ((ret == 0 || ret == Errors.IRET_ERR_DONT_CHECK_ENROLLED)) // && bpresponse != null)
                {
                    LOG.Debug("BPHelper.TxCreateNotarize - Ingresa a crear TX segun parámetros y workflow pedido = " +
                        Properties.Settings.Default.NVCompanyCI + "/" + pin.workflowname);
                    //Si no hubo error analizo la informacion retornada para determinar los pasos a hacer 
                    // en conjunto con el workflow 
                    pin.workflowname = (string.IsNullOrEmpty(pin.workflowname)) ? "default" : pin.workflowname;
                    Database.CompanyWorkflow cw = Database.AdministradorCompanyWorkflow.
                                        BuscarCompanyWokflowByName(Properties.Settings.Default.NVCompanyCI, pin.workflowname);

                    if (cw == null)
                    {
                        LOG.Warn("BPHelper.TxCreateNotarize - Workflow " + Properties.Settings.Default.NVCompanyCI + "/" + pin.workflowname + " no encontrado!. Sale con error " +
                                 Errors.IRET_ERR_WORKFLOW_DONT_EXIST);
                        return Errors.IRET_ERR_WORKFLOW_DONT_EXIST;
                    }
                    listMails = cw.ReceiverList; //Retorno lista de mails a enviar notify si se debe enviar

                    //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    //var client = new RestClient("https://qabpservice5.9.biometrikalatam.com/api/" + "txCreate");
                    LOG.Debug("BPHelper.TxCreateNotarize - LLama a " + Properties.Settings.Default.BPWebUrlBase + "txCreate...");
                    var client = new RestClient(Properties.Settings.Default.BPWebUrlBase + "txCreate");
                    client.Timeout = Properties.Settings.Default.BPTimeout;
                    var request = new RestRequest(Method.POST);
                    client.UserAgent = "biometrika";
                    request.AddHeader("Authorization", "Basic " +
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.BPAccessName + ":" +
                                                                            Properties.Settings.Default.BPSecretKey)));
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("Accept", "application/json");
                    //int iaux = pin.company;
                    //pin.company = Properties.Settings.Default.CIBPCompanyId;
                    //string jsonParams = JsonConvert.SerializeObject(pin);
                    //pin.company = iaux;

                    BPTxCreate paramInBPTxCreate = new BPTxCreate();
                    paramInBPTxCreate.callbackurl = pin.callbackUrl;
                    paramInBPTxCreate.company = Properties.Settings.Default.CIBPCompanyId;
                    paramInBPTxCreate.customertrackid = trackidCI;

                    paramInBPTxCreate.redirecturl = pin.redirectUrl;
                    //paramInBPTxCreate.signerinclude = pin.signerInclude;
                    paramInBPTxCreate.theme = "nv"; //pin.theme; //TODO - Debe tomar el theme de cada notaria
                    paramInBPTxCreate.threshold = cw.Threshold; // Properties.Settings.Default.CIBPTreshold; //pin.threshold;
                    paramInBPTxCreate.typeid = "RUT"; //pin.typeId;
                    paramInBPTxCreate.valueid = pin.rut;
                    paramInBPTxCreate.taxidcompany = null; //pin.taxidcompany;

                    //Check para ver que seteamos
                    //Pongo que no sea mandatorio porque es NV. Si no está enrolado en BP se coloca por default 1 
                    paramInBPTxCreate.onboardingmandatory = cw.OnBoardingMandatory;
                    LOG.Debug("BPHelper.TxCreateNotarize - Set paramInBPTxCreate.onboardingmandatory = " + paramInBPTxCreate.onboardingmandatory);
                    //Si esta enrolado => Auth sino enroll combined
                    if (bpresponse == null)
                    {
                        paramInBPTxCreate.onboardingtype = 1; //NO esta enrolado => Fuerzo Onboarding
                    }
                    else if (bpresponse.exist == 1 && bpresponse.isenrolledtv3dplus == 1) //&& bpresponse.isenrolledft
                    {
                        paramInBPTxCreate.onboardingtype = 0; //Ya esta enrolado => Pongo Auth
                    }
                    else
                    {
                        paramInBPTxCreate.onboardingtype = 1; //NO esta enrolado => Fuerzo Onboarding
                    }
                    LOG.Debug("BPHelper.TxCreateNotarize - Set paramInBPTxCreate.onboardingtype = " + paramInBPTxCreate.onboardingtype);
                    paramInBPTxCreate.typeverifyonboarding = cw.BPWebTypeOnboarding; // (com == null) ? Properties.Settings.Default.CIBPWebTypeOnBoarding : com.BPWebTypeOnBoarding;
                    LOG.Debug("BPHelper.TxCreateNotarize - Set paramInBPTxCreate.typeverifyonboarding = " + paramInBPTxCreate.typeverifyonboarding);
                    paramInBPTxCreate.typeverifyauth = cw.BPWebTypeVerify; // (com == null) ? Properties.Settings.Default.CIBPWebTypeAuth : com.BPWebTypeVerify;
                    LOG.Debug("BPHelper.TxCreateNotarize - Set paramInBPTxCreate.typeverifyauth = " + paramInBPTxCreate.typeverifyauth);
                    //paramInBPTxCreate.onboardingtype = 1; //Siempre lo mando en Combined, por si se usa

                    paramInBPTxCreate.signerinclude = cw.SignerInclude;
                    paramInBPTxCreate.autorizationinclude = cw.Autorization;
                    LOG.Debug("BPHelper.TxCreateNotarize - Set paramInBPTxCreate.autorizationinclude = " + paramInBPTxCreate.autorizationinclude);
                    paramInBPTxCreate.autorizationmessage = cw.AutorizationMessage;
                    paramInBPTxCreate.checklocksrcei = cw.CheckLockSRCeI;
                    paramInBPTxCreate.videoinclude = cw.VideoInclude;
                    paramInBPTxCreate.videomessage = cw.VideoMessage;
                    paramInBPTxCreate.videomessage = AddRandomKey(paramInBPTxCreate.videomessage);
                    paramInBPTxCreate.checkadult = cw.CheckAdult;
                    paramInBPTxCreate.checkexpired = cw.CheckExpired;
                    LOG.Debug("BPHelper.TxCreateNotarize - Set paramInBPTxCreate.georefmandatory = " + cw.GeoRefMandatory);
                    paramInBPTxCreate.georefmandatory = cw.GeoRefMandatory;
                    paramInBPTxCreate.redirecturl = cw.RedirectUrl;
                    paramInBPTxCreate.titlebpweb = "Certificacion de Proceso";
                    //Database.Company c = Database.AdministradorCompany.BuscarCompanyById(pin.company);
                    Database.Company com = Database.AdministradorCompany.BuscarCompanyById(Properties.Settings.Default.NVCompanyCI);
                    string _companyName = (com == null ? "" : com.Name);
                    paramInBPTxCreate.messagebpweb = "Nuestro cliente " + _companyName + " encargo la certificacion de identidad de " +
                                                     "RUT" + " " + pin.rut;
                    paramInBPTxCreate.theme = cw.Theme;
                    paramInBPTxCreate.successurl = cw.SuccessUrl;
                    paramInBPTxCreate.carregisterinclude = cw.CarRegisterInclude;
                    paramInBPTxCreate.writinginclude = cw.WritingInclude;

                    //if (!string.IsNullOrEmpty(pin.taxidcompany))
                    //{
                    //    paramInBPTxCreate.formid = cw.FormInclude + "_empresa";
                    //}
                    //else
                    //{
                        paramInBPTxCreate.formid = cw.FormInclude;
                    //}
                    string jsonParams = JsonConvert.SerializeObject(paramInBPTxCreate);
                    request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                    LOG.Debug("BPHelper.TxCreateNotarize - call execute...");
                    IRestResponse response = client.Execute(request);

                    if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
                        && !string.IsNullOrEmpty(response.Content))
                    {
                        LOG.Debug("BPHelper.TxCreateNotarize - procesando response...");
                        bpr = JsonConvert.DeserializeObject<BPResponseTxCreate>(response.Content);
                        LOG.Debug("BPHelper.TxCreateNotarize - BPResponseTxCreate != null => " + (bpr != null).ToString());
                    }
                    else
                    {
                        LOG.Warn("BPHelper.TxCreateNotarize - response != null => " + (response != null).ToString() + " - " +
                                 ((response != null) ? response.StatusCode.ToString() : "null"));
                    }

                    if (bpr == null)
                    {
                        bptxresponse = null;
                        ret = -1;
                    }
                    else
                    {
                        ret = 0; //limpio salida para que no de error en el controller
                        LOG.Debug("BPHelper.TxCreateNotarize - Set salida urlservice = " + bpr.urlservice);
                        trackidbp = bpr.trackid;
                        bptxresponse = new BPTxCreateR(bpr.urlservice);
                    }
                }
                else if (ret < 0 || bpresponse == null) //Salgo e informo
                {
                    msgerr = "Chequeo de enrolamiento con problemas => ret = " + ret +
                             " - (bpresponse!=null)=" + (bpresponse != null).ToString();
                    LOG.Warn("BPHelper.TxCreateNotarize - Chequeo de enrolamiento con problemas => ret = " + ret +
                             " - (bpresponse!=null)=" + (bpresponse != null).ToString());
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "TxCreateNotarize Excp: " + ex.Message;
                LOG.Error("BPHelper.TxCreateNotarize Excp: " + ex.Message);
            }
            LOG.Debug("BPHelper.TxCreateNotarize OUT!");
            return ret;
        }

        /// <summary>
        /// Crea Tx en BPWeb
        /// </summary>
        /// <param name="trackidCI"></param>
        /// <param name="pin"></param>
        /// <param name="msgerr"></param>
        /// <param name="listMails"></param>
        /// <param name="trackidbp"></param>
        /// <param name="bptxresponse"></param>
        /// <returns></returns>
        public static int TxCreateNotarize(string trackidCI, CreateProcedureModel paramIn, Signatory pin, out string msgerr,
                            out string listMails, out string trackidbp, out BPTxCreateR bptxresponse)
        {
            int ret = 0;
            msgerr = null;
            BPResponseTxCreate bpr = null;
            bptxresponse = null;
            trackidbp = null;
            listMails = null;
            try
            {
                LOG.Debug("BPHelper.TxCreateNotarize IN...");
                //Added 12-08-2020
                //Chequeo que la persona este enrolada. 
                //  - Si no lo está => Genero enroll combined
                //  - Si está => Genero verify como pide los parametros del OB o Auth segun Company
                BPUserEnrolledR bpresponse;
                LOG.Debug("BPHelper.TxCreateNotarize - Ingresando a CheckIdentityEnrolled con => " +
                          Properties.Settings.Default.CIBPCompanyId + "/RUT/" + pin.valueid);
                ret = CheckIdentityEnrolled(Properties.Settings.Default.CIBPCompanyId, "RUT", pin.valueid,
                                            out bpresponse, out msgerr);
                LOG.Debug("BPHelper.TxCreateNotarize - CheckIdentityEnrolled ret = " + ret);
                //Si no hubo error o bien no encontro que este enrolado => Enrolo
                if ((ret == 0 || ret == Errors.IRET_ERR_DONT_CHECK_ENROLLED)) // && bpresponse != null)
                {
                    LOG.Debug("BPHelper.TxCreateNotarize - Ingresa a crear TX segun parámetros y workflow pedido = " +
                        Properties.Settings.Default.NVCompanyCI + "/" + paramIn.workflowname);
                    //Si no hubo error analizo la informacion retornada para determinar los pasos a hacer 
                    // en conjunto con el workflow 
                    paramIn.workflowname = (string.IsNullOrEmpty(paramIn.workflowname)) ? "default" : paramIn.workflowname;
                    Database.CompanyWorkflow cw = Database.AdministradorCompanyWorkflow.
                                        BuscarCompanyWokflowByName(Properties.Settings.Default.NVCompanyCI, paramIn.workflowname);

                    if (cw == null)
                    {
                        LOG.Warn("BPHelper.TxCreateNotarize - Workflow " + Properties.Settings.Default.NVCompanyCI + "/" + paramIn.workflowname + " no encontrado!. Sale con error " +
                                 Errors.IRET_ERR_WORKFLOW_DONT_EXIST);
                        return Errors.IRET_ERR_WORKFLOW_DONT_EXIST;
                    }
                    listMails = cw.ReceiverList; //Retorno lista de mails a enviar notify si se debe enviar

                    //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    //var client = new RestClient("https://qabpservice5.9.biometrikalatam.com/api/" + "txCreate");
                    LOG.Debug("BPHelper.TxCreateNotarize - LLama a " + Properties.Settings.Default.BPWebUrlBase + "txCreate...");
                    var client = new RestClient(Properties.Settings.Default.BPWebUrlBase + "txCreate");
                    client.Timeout = Properties.Settings.Default.BPTimeout;
                    var request = new RestRequest(Method.POST);
                    client.UserAgent = "biometrika";
                    request.AddHeader("Authorization", "Basic " +
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.BPAccessName + ":" +
                                                                            Properties.Settings.Default.BPSecretKey)));
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("Accept", "application/json");
                    //int iaux = pin.company;
                    //pin.company = Properties.Settings.Default.CIBPCompanyId;
                    //string jsonParams = JsonConvert.SerializeObject(pin);
                    //pin.company = iaux;

                    BPTxCreate paramInBPTxCreate = new BPTxCreate();
                    paramInBPTxCreate.callbackurl = paramIn.callbackUrl;
                    paramInBPTxCreate.company = Properties.Settings.Default.CIBPCompanyId;
                    paramInBPTxCreate.customertrackid = trackidCI;

                    paramInBPTxCreate.redirecturl = paramIn.redirectUrl;
                    //paramInBPTxCreate.signerinclude = pin.signerInclude;
                    paramInBPTxCreate.theme = "nv"; //pin.theme; //TODO - Debe tomar el theme de cada notaria
                    paramInBPTxCreate.threshold = cw.Threshold; // Properties.Settings.Default.CIBPTreshold; //pin.threshold;
                    paramInBPTxCreate.typeid = "RUT"; //pin.typeId;
                    paramInBPTxCreate.valueid = pin.valueid;
                    paramInBPTxCreate.taxidcompany = null; //pin.taxidcompany;

                    //Check para ver que seteamos
                    //Pongo que no sea mandatorio porque es NV. Si no está enrolado en BP se coloca por default 1 
                    paramInBPTxCreate.onboardingmandatory = cw.OnBoardingMandatory;
                    LOG.Debug("BPHelper.TxCreateNotarize - Set paramInBPTxCreate.onboardingmandatory = " + paramInBPTxCreate.onboardingmandatory);
                    //Si esta enrolado => Auth sino enroll combined
                    if (bpresponse == null)
                    {
                        paramInBPTxCreate.onboardingtype = 1; //NO esta enrolado => Fuerzo Onboarding
                    }
                    else if (bpresponse.exist == 1 && bpresponse.isenrolledtv3dplus == 1) //&& bpresponse.isenrolledft
                    {
                        paramInBPTxCreate.onboardingtype = 0; //Ya esta enrolado => Pongo Auth
                    }
                    else
                    {
                        paramInBPTxCreate.onboardingtype = 1; //NO esta enrolado => Fuerzo Onboarding
                    }
                    LOG.Debug("BPHelper.TxCreateNotarize - Set paramInBPTxCreate.onboardingtype = " + paramInBPTxCreate.onboardingtype);
                    paramInBPTxCreate.typeverifyonboarding = cw.BPWebTypeOnboarding; // (com == null) ? Properties.Settings.Default.CIBPWebTypeOnBoarding : com.BPWebTypeOnBoarding;
                    LOG.Debug("BPHelper.TxCreateNotarize - Set paramInBPTxCreate.typeverifyonboarding = " + paramInBPTxCreate.typeverifyonboarding);
                    paramInBPTxCreate.typeverifyauth = cw.BPWebTypeVerify; // (com == null) ? Properties.Settings.Default.CIBPWebTypeAuth : com.BPWebTypeVerify;
                    LOG.Debug("BPHelper.TxCreateNotarize - Set paramInBPTxCreate.typeverifyauth = " + paramInBPTxCreate.typeverifyauth);
                    //paramInBPTxCreate.onboardingtype = 1; //Siempre lo mando en Combined, por si se usa

                    paramInBPTxCreate.signerinclude = cw.SignerInclude;
                    paramInBPTxCreate.autorizationinclude = cw.Autorization;
                    LOG.Debug("BPHelper.TxCreateNotarize - Set paramInBPTxCreate.autorizationinclude = " + paramInBPTxCreate.autorizationinclude);
                    paramInBPTxCreate.autorizationmessage = cw.AutorizationMessage;
                    paramInBPTxCreate.checklocksrcei = cw.CheckLockSRCeI;
                    paramInBPTxCreate.videoinclude = cw.VideoInclude;
                    paramInBPTxCreate.videomessage = cw.VideoMessage;
                    paramInBPTxCreate.videomessage = AddRandomKey(paramInBPTxCreate.videomessage);
                    paramInBPTxCreate.checkadult = cw.CheckAdult;
                    paramInBPTxCreate.checkexpired = cw.CheckExpired;
                    LOG.Debug("BPHelper.TxCreateNotarize - Set paramInBPTxCreate.georefmandatory = " + cw.GeoRefMandatory);
                    paramInBPTxCreate.georefmandatory = cw.GeoRefMandatory;
                    paramInBPTxCreate.redirecturl = cw.RedirectUrl;
                    paramInBPTxCreate.titlebpweb = "Certificacion de Proceso";
                    //Database.Company c = Database.AdministradorCompany.BuscarCompanyById(pin.company);
                    Database.Company com = Database.AdministradorCompany.BuscarCompanyById(Properties.Settings.Default.NVCompanyCI);
                    string _companyName = (com == null ? "" : com.Name);
                    paramInBPTxCreate.messagebpweb = "Nuestro cliente " + _companyName + " encargo la certificacion de identidad de " +
                                                     "RUT" + " " + pin.valueid;
                    paramInBPTxCreate.theme = cw.Theme;
                    paramInBPTxCreate.successurl = cw.SuccessUrl;
                    paramInBPTxCreate.carregisterinclude = cw.CarRegisterInclude;
                    paramInBPTxCreate.writinginclude = cw.WritingInclude;

                    //if (!string.IsNullOrEmpty(pin.taxidcompany))
                    //{
                    //    paramInBPTxCreate.formid = cw.FormInclude + "_empresa";
                    //}
                    //else
                    //{
                    paramInBPTxCreate.formid = cw.FormInclude;
                    //}
                    string jsonParams = JsonConvert.SerializeObject(paramInBPTxCreate);
                    request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                    LOG.Debug("BPHelper.TxCreateNotarize - call execute...");
                    IRestResponse response = client.Execute(request);

                    if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
                        && !string.IsNullOrEmpty(response.Content))
                    {
                        LOG.Debug("BPHelper.TxCreateNotarize - procesando response...");
                        bpr = JsonConvert.DeserializeObject<BPResponseTxCreate>(response.Content);
                        LOG.Debug("BPHelper.TxCreateNotarize - BPResponseTxCreate != null => " + (bpr != null).ToString());
                    }
                    else
                    {
                        LOG.Warn("BPHelper.TxCreateNotarize - response != null => " + (response != null).ToString() + " - " +
                                 ((response != null) ? response.StatusCode.ToString() : "null"));
                    }

                    if (bpr == null)
                    {
                        bptxresponse = null;
                        ret = -1;
                    }
                    else
                    {
                        ret = 0; //limpio salida para que no de error en el controller
                        LOG.Debug("BPHelper.TxCreateNotarize - Set salida urlservice = " + bpr.urlservice);
                        trackidbp = bpr.trackid;
                        bptxresponse = new BPTxCreateR(bpr.urlservice);
                    }
                }
                else if (ret < 0 || bpresponse == null) //Salgo e informo
                {
                    msgerr = "Chequeo de enrolamiento con problemas => ret = " + ret +
                             " - (bpresponse!=null)=" + (bpresponse != null).ToString();
                    LOG.Warn("BPHelper.TxCreateNotarize - Chequeo de enrolamiento con problemas => ret = " + ret +
                             " - (bpresponse!=null)=" + (bpresponse != null).ToString());
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msgerr = "TxCreateNotarize Excp: " + ex.Message;
                LOG.Error("BPHelper.TxCreateNotarize Excp: " + ex.Message);
            }
            LOG.Debug("BPHelper.TxCreateNotarize OUT!");
            return ret;
        }

        /// <summary>
        /// Genera una clave dinamica de 8 caracateres y/o numeros, para que la persona lea en el video
        /// </summary>
        /// <param name="videomessage"></param>
        /// <returns></returns>
        private static string AddRandomKey(string videomessage)
        {
            string key = "123456";
            try
            {
                string retGuid = Guid.NewGuid().ToString("N");
                key = retGuid.Substring(0, 8).ToUpper();
            }
            catch (Exception ex)
            {
                LOG.Error("BPHelper.AddRandomKey Error: " + ex.Message);
            }
            return videomessage + " <b>" + key + "</b>";
        }

        #endregion NvProcedure Helper
    }
}