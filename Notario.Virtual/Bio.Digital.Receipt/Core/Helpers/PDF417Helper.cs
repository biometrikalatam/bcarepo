﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using Bio.Digital.Receipt.Api;
using log4net;
using Pdf417DecoderLibrary;

namespace Bio.Digital.Receipt.Core.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class PDF417Helper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(PDF417Helper));

        internal static string ProcesaPDF417Recognition(string imageBackCedula)
        {
            string strReturn = null; 
            try
            {
                LOG.Debug("PDF417Helper.ProcesaPDF417Recognition - IN...");

                if (string.IsNullOrEmpty(imageBackCedula))
                {
                    LOG.Debug("PDF417Helper.ProcesaPDF417Recognition - Sale porque param entrada es nulo!");
                } else
                {
                    LOG.Debug("PDF417Helper.ProcesaPDF417Recognition - Convierte string entrada en Image...");
                }

                System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(imageBackCedula));
                Image img = Image.FromStream(ms);
                ms.Close();

                LOG.Debug("PDF417Helper.ProcesaPDF417Recognition - Create Bitmap...");
                // load image to bitmap
                Bitmap Pdf417InputImage = new Bitmap((Image)img.Clone());
                //try
                //{
                //    System.IO.File.WriteAllBytes(@"c:\tmp\a\img" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".jpg", 
                //                                 Convert.FromBase64String(imageBackCedula));
                //    //Pdf417InputImage.Save(@"c:\tmp\a\img" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".jpg");
                //}
                //catch (Exception ex)
                //{

                //}
                // decode barcodes
                Pdf417Decoder oPdf417Decoder = new Pdf417Decoder();
                //string CurDir = Environment.CurrentDirectory;
                //string WorkDir = CurDir + "\\PDF417Log";  // CurDir.Replace("bin\\Debug", "Work");
                //if (WorkDir != CurDir && Directory.Exists(WorkDir))
                //    Environment.CurrentDirectory = WorkDir;

                // open trace file
                //Pdf417Trace.Open("Pdf417DecoderTrace.txt");
                //Pdf417Trace.Write("Pdf417DecoderDemo");
                LOG.Debug("PDF417Helper.ProcesaPDF417Recognition - Pdf417Decoder.Decode in...");
                int BarcodesCount = oPdf417Decoder.Decode(Pdf417InputImage);

                // no barcodes were found
                if (BarcodesCount == 0)
                {
                    LOG.Debug("PDF417Helper.ProcesaPDF417Recognition - No reconocio barcode PDF417!");
                }

                // one barcodes was found
                else if (BarcodesCount > 0)
                {
                    // decoding was successful
                    // convert binary data to text string
                    strReturn = Convert.ToBase64String(oPdf417Decoder.BarcodesInfo[0].BarcodeData);
                    LOG.Debug("PDF417Helper.ProcesaPDF417Recognition - PDF417 Reconocido => Length=" + 
                                (string.IsNullOrEmpty(strReturn)?"Null":strReturn.Length.ToString()));
                }
            }
            catch (Exception ex)
            {
                strReturn = null;
                LOG.Error("PDF417Helper.ProcesaPDF417Recognition - Error: " + ex.Message);
            }
            LOG.Debug("PDF417Helper.ProcesaPDF417Recognition - OUT!");
            return strReturn;
        }
    }
}