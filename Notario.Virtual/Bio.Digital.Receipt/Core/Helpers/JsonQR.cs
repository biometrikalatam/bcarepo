﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.Core.Helpers
{
    public class JsonQR
    {
        //string trackidnew, int companyid, string typeId, string valueId, string bASE_URL_NV_SERVICE
        private string _trackId;
        private int _companyId;
        private int _applicationId;
        private string _typeId;
        private string _valueId;
        private string _urlToUse;
        private string _urlToCheck;
        private bool _locationEnabled;
        private int _enrolled;
        //Added 15-11-2020 - DynamicParam
        /*
        Activa o desactiva el liveness
        bool LivenessEnabled = true

        Timeout para el proceso completo de liveness
        int LivenessTimeout = 10000

        Indica si se debe detener el proceso si la cara se pierde de la cámara
        bool LivenessFaceMissing = true

        Indica la seleccion de camara por defecto para la selfie (sin liveness), front/back
        string SelfieFacing = "front"  'o sino "back"
        */
        private bool _livenessenabled = true;
        private int _livenesstimeout = 10000;
        private bool _livenessfacemissing = true;
        private string _slfiefacing = "front";

        public string TrackId { get => this._trackId; set => this._trackId = value; }
        public int CompanyId { get => this._companyId; set => this._companyId = value; }
        public int ApplicationId { get => this._applicationId; set => this._applicationId = value; }
        public string TypeId { get => this._typeId; set => this._typeId = value; }
        public string ValueId { get => this._valueId; set => this._valueId = value; }
        public string UrlToUse { get => this._urlToUse; set => this._urlToUse = value; }
        public string UrlToCheck { get => this._urlToCheck; set => this._urlToCheck = value; }
        public bool LocationEnabled { get => this._locationEnabled; set => this._locationEnabled = value; }
        public int Enrolled { get => this._enrolled; set => this._enrolled = value; }

        public bool LivenessEnabled { get => this._livenessenabled; set => this._livenessenabled = value; }
        public int LivenessTimeout { get => this._livenesstimeout; set => this._livenesstimeout = value; }
        public bool LivenessFaceMissing { get => this._livenessfacemissing; set => this._livenessfacemissing = value; }
        public string SelfieFacing { get => this._slfiefacing; set => this._slfiefacing = value; }

        public JsonQR(string trackid, int companyid, int applicationid, string typeId, string valueId, string url, 
            string urltocheck, bool locationenabled, int enrolled)
        {
            _trackId = trackid;
            _companyId = companyid;
            _applicationId = applicationid;
            _typeId = typeId;
            _valueId = valueId;
            _urlToUse = url;
            _urlToCheck = urltocheck;
            _locationEnabled = locationenabled;
            _enrolled = enrolled;
        }

        public JsonQR() { }

    }
}