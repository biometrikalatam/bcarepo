﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.libs;
using Bio.Digital.Receipt.rest.Models;
using Bio.Digital.Receipt.rest.Models.Signer;
//using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.Api;
using Biometrika.Document.Electronic.Api.Model;
using log4net;
using SautinSoft.Document;


namespace Bio.Digital.Receipt.Core.Helpers
{
    public class DEHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(DEHelper));

        internal static int GeneraCertifyPDF(Bio.Digital.Receipt.Api.XmlParamIn oXmlIn, double score, string qRverify, out string msg, 
            out string pdfCIB64, out string pdfDJB64, out string trackIDE, out string map)
        {
            msg = "";
            //pdfB64 = "PDFPDF=";
            //qRverify = "QRQR=";
            //trackIDE = "02ab5e9dca234490bc097c334b324055";
            //return 0;

            int ret = 0;
            msg = null;
            string xmlparamin;
            string xmlparamout;
            Biometrika.Document.Electronic.Api.XmlParamOut oParamout;
            string path = null;
            string xmlOutDE = null;
            pdfCIB64 = null;
            trackIDE = null;
            map = null;
            pdfDJB64 = null;
            try
            {
                LOG.Debug("DEHelper.GeneraCertifyPDF IN...");

                Biometrika.Document.Electronic.Api.XmlParamIn oParamin = new Biometrika.Document.Electronic.Api.XmlParamIn();
                oParamin.Actionid = 1;  //1 - Generar desde template word | 2 - Firmar un PDF | 3 - Get Documento
                oParamin.IdDE = Properties.Settings.Default.CIDEId;     //Id de tipo de documento documento, informado por Biometrika cuando se crea un nuevo tipo de documento   

                oParamin.EmployeeId = "11111111-1";  //RUT persona que envia a generar el documento o firmar (puede ser generico)
                oParamin.Enduser = "NV_CI";           //Nombre persona que envia a generar el documento o firmar (puede ser generico)          
                oParamin.Ipenduser = "127.0.0.1";    //IP Estacion de trabajo donde se genera el documento
                oParamin.Company = Properties.Settings.Default.CIDECompanyId;                //Id de compañia informada por Biometrika en el momento de dar de alta el servicio
                oParamin.User = "User";              //User para acceso al servicio
                oParamin.Password = "Password";      //Password para acceso al servicio
                oParamin.TypeId = oXmlIn.CIParam.TypeId;            //Tipo de documento del cliente del contrato (Normalemnte en Chile RUT)
                oParamin.ValueId = oXmlIn.CIParam.ValueId;          //Valor de documento del cliente del contrato (Normalemnte en Chile RUT xxxxxxxx-y)
                
                //Cargo los tags a reemplazar en el template del documento word.
                oParamin.Tags = new List<Biometrika.Document.Electronic.Api.Model.Tag>();
                Biometrika.Document.Electronic.Api.Model.Tag item;

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#TrackId#";
                item.Value = oXmlIn.CIParam.TrackId;
                oParamin.Tags.Add(item);


                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#IDNumber#";
                item.Value = oXmlIn.CIParam.ValueId; //oXmlIn.CIParam.TypeId + " " + oXmlIn.CIParam.ValueId;
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Fecha#";
                item.Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Nombre#";
                item.Value = oXmlIn.CIParam.Name + " " +
                             (string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName)?"": oXmlIn.CIParam.PhaterLastName) + " " +
                             (string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName)?"": oXmlIn.CIParam.MotherLastName);
                if (!string.IsNullOrEmpty(item.Value) && item.Value.Length > 25) item.Value = item.Value.Substring(0, 25); 
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#BirthDate#";
                //Chequea si es adulto o menor y agrega
                //oXmlIn.CIParam.BirthDate [Menor] o oXmlIn.CIParam.BirthDate [Mayor]
                item.Value = CheckAdult(oXmlIn.CIParam.BirthDate);  

                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Sex#";
                item.Value = (!string.IsNullOrEmpty(oXmlIn.CIParam.Sex) &&
                              (oXmlIn.CIParam.Sex.Equals("M") || oXmlIn.CIParam.Sex.Equals("F"))) ?
                               oXmlIn.CIParam.Sex : "Desconocido";
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Nacionality#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.Nacionality)?"Desconocida":oXmlIn.CIParam.Nacionality;
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#ExpirationDate#";
                //Chequea si es adulto o menor y agrega
                //oXmlIn.CIParam.ExpirationDate [EXPIRADO] o oXmlIn.CIParam.ExpirationDate [VIGENTE]
                item.Value = CheckVigente(oXmlIn.CIParam.ExpirationDate);
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Serial#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.Serial)?"Desconocida":oXmlIn.CIParam.Serial;
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#IDCardImage#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront) ?
                                                Global.IMAGE_NO_AVAILABLE
                                              : oXmlIn.CIParam.IDCardImageFront;
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#IDCardImageBack#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ?
                                                Global.IMAGE_NO_AVAILABLE
                                              : oXmlIn.CIParam.IDCardImageBack;
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#FingerSample#";
                item.Value = null; //GetImageFromJPGorWSQ(oXmlIn.FingerSampleJpg, oXmlIn.FingerSample);
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#IDCardPhotoImage#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.Selfie)?oXmlIn.CIParam.IDCardPhotoImage : oXmlIn.CIParam.Selfie;
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#IDCardSignatureImage#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardSignatureImage) ? 
                                            Global.IMAGE_NO_AVAILABLE
                                            : oXmlIn.CIParam.IDCardSignatureImage; 
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Map#";
                try
                {
                    map = null;
                    if (!string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef))
                    {
                        map = Bio.Digital.Receipt.libs.Utils.GetMapFromGoogleMap(oXmlIn.CIParam.GeoRef);
                    }
                    //TODO - Georef en el workflow
                    //else if (!string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef))
                    //{
                    //    map = Bio.Digital.Receipt.libs.Utils.GetMapFromGoogleMap(oXmlIn.CIParam.GeoRef);
                    //}
                    if (string.IsNullOrEmpty(map)) map = Global.MAP_DEFAULT;
                }
                catch (Exception ex)
                {
                    map = Global.MAP_DEFAULT;
                }
                item.Value = map;
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#QRVerify#";
                item.Value = string.IsNullOrEmpty(qRverify) ? "" : qRverify;
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Score#";
                item.Value = score.ToString("##.##") + "%";
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Pais#";
                item.Value = "Chile";
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#GeoLoc#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef)? "-33.380877,-70.5365727" : oXmlIn.CIParam.GeoRef;  //Coordenadas de Biometrika si no hay
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#WorkstationId#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.WorkStationID)?"Desconocido": oXmlIn.CIParam.WorkStationID;
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#IDCardPhotoImageDJ#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.Selfie) ? oXmlIn.CIParam.IDCardPhotoImage : oXmlIn.CIParam.Selfie;
                oParamin.Tags.Add(item);

                //oParamin.Tags.Add(item);

                //oParamin.BSSignatures = new List<Biometrika.Document.Electronic.Api.BS>();
                //oParamin.BSSignatures.Add(bs);
                //xmlparamin = Biometrika.Document.Electronic.Api.XmlUtils.SerializeAnObject(oParamin);

                using (WSDE.WSDE ws = new WSDE.WSDE())
                {
                    ws.Timeout = Properties.Settings.Default.DETimeout;
                    ws.Url = Properties.Settings.Default.Bio_Digital_Receipt_WSDE_WSDE;

                    //Cargo el/los mail/s donde se enviará/n copia/s del documento generado 
                    oParamin.Notarize = false;
                    string[] mails = new string[1];
                    mails[0] = string.IsNullOrEmpty(oXmlIn.CIParam.DestinaryMail) ? 
                                  Properties.Settings.Default.DEMailDefault: oXmlIn.CIParam.DestinaryMail;
                    oParamin.MailsToNotify = mails;
                    LOG.Debug("DEHelper.GeneraCertifyPDF Mail to Notify = " + (string.IsNullOrEmpty(mails[0])?"No Mail Configurado!":mails[0]));

                    string sxIN = Biometrika.Document.Electronic.Api.XmlUtils.SerializeObject<Biometrika.Document.Electronic.Api.XmlParamIn>(oParamin);
                    LOG.Debug("DEHelper.GeneraCertifyPDF DEParamIn = " + sxIN); 
                    //LLamo al al web service de generación con parámetros 
                    ret = ws.Process(sxIN, out xmlparamout);
                    xmlOutDE = xmlparamout;
                    LOG.Debug("DEHelper.GeneraCertifyPDF ret = " + ret.ToString() + " - xmlOutDE = " + xmlOutDE);

                    //Deserializo respuesta y greabo documento generado y/o firmado
                    oParamout = Biometrika.Document.Electronic.Api.XmlUtils.DeserializeObject<
                                            Biometrika.Document.Electronic.Api.XmlParamOut>(xmlOutDE);
                    pdfCIB64 = oParamout.De;
                    trackIDE = oParamout.Trackid;
                    if (Properties.Settings.Default.DebugVideoImagen == 1)
                    {
                        path = Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + "de_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                        System.IO.File.WriteAllBytes(path, Convert.FromBase64String(oParamout.De));
                        LOG.Debug("DEHelper.GeneraCertifyPDF Save DE in = " + path);
                    }
                }
                
                //if (GeneraDeclaracionJuradaPDF(oXmlIn, out msg, out pdfDJB64) != 0)
                //{
                //    msg = msg + " [No se pudo generar la Declaración Jurada. Se aborta generación de CI]"; 
                //    pdfCIB64 = null;
                //    ret = Errors.IRET_ERR_CI_DECLARACIONJURADA_INCORRECT;
                //}
            }
            catch (Exception ex)
            {
                msg = "Error Desconocido Notarizando = " + ex.Message;
                LOG.Error("DEHelper.GeneraCertifyPDF Error => " + msg);

               ret = -1;
            }
            LOG.Debug("DEHelper.GeneraCertifyPDF OUT!");
            return ret;

        }

        internal static int GeneraDEPDF(Bio.Digital.Receipt.Api.XmlParamIn oXmlIn, double score, string qRverify, 
                                        string jsonform, string carregisterimagefront, string carregisterimageback,
                                        out string msg, out string pdfDEB64, out string pdfDJB64, out string trackIDE, 
                                        out string map, out string securitycode)
        {
            msg = "";
            //pdfB64 = "PDFPDF=";
            //qRverify = "QRQR=";
            //trackIDE = "02ab5e9dca234490bc097c334b324055";
            //return 0;

            int ret = 0;
            msg = null;
            string xmlparamin;
            string xmlparamout;
            Biometrika.Document.Electronic.Api.XmlParamOut oParamout;
            string path = null;
            string xmlOutDE = null;
            pdfDEB64 = null;
            trackIDE = null;
            map = null;
            pdfDJB64 = null;
            securitycode = "Null";
            try
            {
                LOG.Debug("DEHelper.GeneraDEPDF IN...");

                Biometrika.Document.Electronic.Api.XmlParamIn oParamin = new Biometrika.Document.Electronic.Api.XmlParamIn();
                oParamin.Actionid = 1;  //1 - Generar desde template word | 2 - Firmar un PDF | 3 - Get Documento
                oParamin.IdDE = Properties.Settings.Default.TTagDEId;     //Id de tipo de documento documento, informado por Biometrika cuando se crea un nuevo tipo de documento   

                oParamin.EmployeeId = "11111111-1";  //RUT persona que envia a generar el documento o firmar (puede ser generico)
                oParamin.Enduser = "NV_TTag";           //Nombre persona que envia a generar el documento o firmar (puede ser generico)          
                oParamin.Ipenduser = "127.0.0.1";    //IP Estacion de trabajo donde se genera el documento
                oParamin.Company = Properties.Settings.Default.TTagDECompanyId;                //Id de compañia informada por Biometrika en el momento de dar de alta el servicio
                oParamin.User = "User";              //User para acceso al servicio
                oParamin.Password = "Password";      //Password para acceso al servicio
                oParamin.TypeId = oXmlIn.CIParam.TypeId;            //Tipo de documento del cliente del contrato (Normalemnte en Chile RUT)
                oParamin.ValueId = oXmlIn.CIParam.ValueId;          //Valor de documento del cliente del contrato (Normalemnte en Chile RUT xxxxxxxx-y)


                LOG.Debug("DEHelper.GeneraDEPDF - Deserializar jsonform: (jsonform == null) => " +
                           string.IsNullOrEmpty(jsonform).ToString());
                //FormData _FORM = Newtonsoft.Json.JsonConvert.DeserializeObject<FormData>(jsonform);
                Dictionary<string, string> _FORM = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonform);
                LOG.Debug("DEHelper.GeneraDEPDF - _FORM generado: _FORM != null => " + (_FORM!=null).ToString());

                //Cargo los tags a reemplazar en el template del documento word.
                oParamin.Tags = new List<Biometrika.Document.Electronic.Api.Model.Tag>();
                Biometrika.Document.Electronic.Api.Model.Tag item;

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#fecha#";
                item.Value = DateTime.Now.ToString("dd/MM/yyyy"); // HH:mm:ss");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #fecha# = " + (string.IsNullOrEmpty(item.Value)?"null":item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#rutempresa#";
                item.Value = GetDataFromForm(_FORM, "rutempresa");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #rutempresa# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#razonsocialempresa#";
                item.Value = GetDataFromForm(_FORM, "nombreempresa");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #razonsocialempresa# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#rutfirmante#";
                item.Value = oXmlIn.CIParam.TypeId + " " + oXmlIn.CIParam.ValueId;
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #rutfirmante# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#nombrefirmante#";
                item.Value = (string.IsNullOrEmpty(oXmlIn.CIParam.Name) ? "" : oXmlIn.CIParam.Name) + " " +
                             (string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName) ? "" : oXmlIn.CIParam.PhaterLastName) + " " +
                             (string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName) ? "" : oXmlIn.CIParam.MotherLastName);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #nombrefirmante# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));
                if (!string.IsNullOrEmpty(item.Value) && item.Value.Length > 100)
                {
                    item.Value = item.Value.Substring(0, 100);
                    LOG.Debug("DEHelper.GeneraDEPDF - Cortado #nombrefirmante# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                }
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#telefonofirmante#";
                item.Value = GetDataFromForm(_FORM, "telefonofirmante");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #telefonofirmante# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#mailfirmante#";
                item.Value = GetDataFromForm(_FORM, "mailfirmante");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #mailfirmante# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#calle#";
                item.Value = GetDataFromForm(_FORM, "calle");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #calle# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#numero#";
                item.Value = GetDataFromForm(_FORM, "numero");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #numero# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#departamento#";
                item.Value = GetDataFromForm(_FORM, "departamento");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #departamento# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#tipovivienda#";
                item.Value = GetDataFromForm(_FORM, "tipovivienda");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #tipovivienda# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#comuna#";
                item.Value = GetDataFromForm(_FORM, "comuna");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #comuna# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#ciudad#";
                item.Value = GetDataFromForm(_FORM, "ciudad");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #ciudad# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#patentevehiculo#";
                item.Value = GetDataFromForm(_FORM, "patentevehiculo");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #patentevehiculo# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#marcavehiculo#";
                item.Value = GetDataFromForm(_FORM, "marcavehiculo");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #marcavehiculo# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#modelovehiculo#";
                item.Value = GetDataFromForm(_FORM, "modelovehiculo");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #modelovehiculo# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#aniovehiculo#";
                item.Value = GetDataFromForm(_FORM, "aniovehiculo");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #aniovehiculo# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#colorvehiculo#";
                item.Value = GetDataFromForm(_FORM, "colorvehiculo");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #colorvehiculo# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#firmacliente#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.ManualSignatureImage) ?
                                            Global.IMAGE_NO_AVAILABLE
                                            : oXmlIn.CIParam.ManualSignatureImage;
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #firmacliente# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0,10)));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#cedulafront#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront) ?
                                                Global.IMAGE_NO_AVAILABLE
                                              : oXmlIn.CIParam.IDCardImageFront;
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #cedulafront# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#cedulaback#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? Global.IMAGE_NO_AVAILABLE : oXmlIn.CIParam.IDCardImageBack;
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #cedulaback# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#selfie#";
                item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.Selfie) ? oXmlIn.CIParam.IDCardPhotoImage : oXmlIn.CIParam.Selfie;
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #selfie# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));
                
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#verifyresult#";
                item.Value = ((score >= Convert.ToDouble(oXmlIn.CIParam.Threshold)) ? "POSITIVA" : "Negativa");
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #verifyresult# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#score#";
                item.Value = score.ToString("##.##") + "%";
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #score# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#padronfront#";
                item.Value = string.IsNullOrEmpty(carregisterimagefront) ? Global.IMAGE_NO_AVAILABLE : carregisterimagefront;
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #padronfront# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#padronback#";
                item.Value = string.IsNullOrEmpty(carregisterimageback) ? Global.IMAGE_NO_AVAILABLE : carregisterimageback;
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #padronback# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));


                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#trackid#";
                item.Value = oXmlIn.CIParam.TrackId;
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #trackid# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                string sc = null;
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#securitycode#";
                item.Value = GenerateSecurityCode(oParamin.Tags);
                sc = item.Value;
                securitycode = item.Value;
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #SecurityCode# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

                string QRverify = GetQR(Properties.Settings.Default.BASE_SIGNER_CHECK + "?trackid=" + oXmlIn.CIParam.TrackId + 
                                         "&securitycode=" + sc);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#QRVerify#";
                item.Value = string.IsNullOrEmpty(QRverify) ? "" : QRverify;
                oParamin.Tags.Add(item);
                LOG.Debug("DEHelper.GeneraDEPDF - Set #QRVerify# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));

               

                using (WSDE.WSDE ws = new WSDE.WSDE())
                {
                    LOG.Debug("DEHelper.GeneraDEPDF - Set DETimeout = " + Properties.Settings.Default.DETimeout.ToString());
                    ws.Timeout = Properties.Settings.Default.DETimeout;
                    LOG.Debug("DEHelper.GeneraDEPDF - Set URL DE = " + Properties.Settings.Default.Bio_Digital_Receipt_WSDE_WSDE);
                    ws.Url = Properties.Settings.Default.Bio_Digital_Receipt_WSDE_WSDE;

                    //Cargo el/los mail/s donde se enviará/n copia/s del documento generado 
                    oParamin.Notarize = false;
                    //string[] mails = new string[1];
                    //mails[0] = string.IsNullOrEmpty(oXmlIn.CIParam.DestinaryMail) ?
                    //              Properties.Settings.Default.DEMailDefault : oXmlIn.CIParam.DestinaryMail;
                    oParamin.MailsToNotify = null; //mails;
                    //LOG.Debug("DEHelper.GeneraDEPDF Mail to Notify = " + (string.IsNullOrEmpty(mails[0]) ? "No Mail Configurado!" : mails[0]));
                    LOG.Debug("DEHelper.GeneraDEPDF - Disabled Mail to Notify and Notarize...");
                    
                    string sxIN = Biometrika.Document.Electronic.Api.XmlUtils.SerializeObject<Biometrika.Document.Electronic.Api.XmlParamIn>(oParamin);
                    LOG.Debug("DEHelper.GeneraDEPDF DEParamIn = " + sxIN.Substring(0,200));
                    //LLamo al al web service de generación con parámetros 
                    ret = ws.Process(sxIN, out xmlparamout);
                    xmlOutDE = xmlparamout;
                    LOG.Debug("DEHelper.GeneraDEPDF ret = " + ret.ToString() + " - xmlOutDE = " + xmlOutDE);

                    //Deserializo respuesta y greabo documento generado y/o firmado
                    oParamout = Biometrika.Document.Electronic.Api.XmlUtils.DeserializeObject<
                                        Biometrika.Document.Electronic.Api.XmlParamOut>(xmlOutDE);
                    pdfDEB64 = oParamout.De;
                    trackIDE = oParamout.Trackid;
                    if (Properties.Settings.Default.DebugVideoImagen == 1)
                    {
                        path = Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + "de_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                        System.IO.File.WriteAllBytes(path, Convert.FromBase64String(oParamout.De));
                        LOG.Debug("DEHelper.GeneraDEPDF Save DE in = " + path);
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "Error Desconocido Generando DE = " + ex.Message;
                LOG.Error("DEHelper.GeneraDEPDF Error => " + msg);
                ret = -1;
            }
            LOG.Debug("DEHelper.GeneraDEPDF OUT!");
            return ret;

        }

        

        private static string GenerateSecurityCode(List<Tag> tags)
        {
            string codeReturn = "Null";
            string aux = "";
            try
            {
                LOG.Debug("DEHelper.GenerateSecurityCode IN...");
                if (tags == null)
                {
                    LOG.Warn("DEHelper.GenerateSecurityCode Param tags=NULL => Sale con valor Null");
                    return codeReturn;
                }

                LOG.Debug("DEHelper.GenerateSecurityCode - Concatena " + tags.Count.ToString() + " tags...");
                foreach (Tag item in tags)
                {
                    LOG.Debug("DEHelper.GenerateSecurityCode - Concatena " + item.IdTag + "...");
                    aux = aux + (string.IsNullOrEmpty(item.Value)?"":item.Value);
                }

                LOG.Debug("DEHelper.GenerateSecurityCode - GetBytes...");
                byte[] origen = System.Text.ASCIIEncoding.ASCII.GetBytes(aux);

                LOG.Debug("DEHelper.GenerateSecurityCode - Calcula SHA256..."); 
                byte[] result = Bio.Core.pki.hash.BKHash.Hash(origen, "SHA256");

                codeReturn = Convert.ToBase64String(result);
            }
            catch (Exception ex)
            {
                codeReturn = "Null";
                LOG.Error("DEHelper.GenerateSecurityCode Error: " + ex.Message);
            }
            LOG.Debug("DEHelper.GenerateSecurityCode - OUT! - SecurityCode => " + codeReturn);
            return codeReturn;
        }

        private static string GetQR(string urlci)
        {
            string qr = "";
            try
            {
                LOG.Debug("DEHelper.GetQR IN...");
                Bio.Core.QR.FormQR formqr = new Bio.Core.QR.FormQR();
                qr = formqr.GetImgQRB64(urlci);
                LOG.Debug("DEHelper.GetQR QR Generated = " + qr);
            }
            catch (Exception ex)
            {
                qr = null;
                LOG.Error("DEHelper.GetQR Error - " + ex.Message);
            }
            LOG.Debug("DEHelper.GetQR OUT!");
            return qr;
        }

        //private static string GetDataFromForm(FormData _Form, string _key)
        private static string GetDataFromForm(Dictionary<string, string> _Form, string _key)
        {
            /*
                rutempresa, rutempresa,telefonofirmante,mailfirmante 
                calle,numero,departamento,tipovivienda, comuna, 
                ciudad, patentevehiculo,marcavehiculo,modelovehiculo,aniovehiculo,colorvehiculo 
            */
            string keytoreturn = "";
            try
            {
                if (_Form == null || _Form.Count == 0 || string.IsNullOrEmpty(_key))
                    return "";

                foreach (KeyValuePair<string, string> item in _Form)
                {
                    if (item.Key.Equals(_key))
                    {
                        keytoreturn = item.Value;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DEHelper.GetDataFromForm Error: " + ex.Message);
                keytoreturn = "";
            }
            return keytoreturn;
        }

        private static string CheckVigente(string expirationDate)
        {
            string ret = expirationDate;
            try
            {
                LOG.Error("DEHelper.CheckVigente IN - expirationDate = " +
                            (string.IsNullOrEmpty(expirationDate) ? "NULL" : expirationDate));

                if (string.IsNullOrEmpty(expirationDate))
                {
                    ret = "No reconocido";
                    LOG.Error("DEHelper.CheckVigente - retorna => " + ret);
                    return ret;
                }
                DateTime date = libs.Utils.Parse(expirationDate);

                if (date.Year == 1900) //Si no pudo parsear => Devuelvo el parametro
                {
                    ret = expirationDate;
                    LOG.Error("DEHelper.CheckVigente - Año 1900 => Parseo erroneo => Devuelve parametro = " + expirationDate);
                } else if (DateTime.Now > date)
                {
                    ret = expirationDate + " [EXPIRADO]";
                    LOG.Error("DEHelper.CheckVigente - retorna => " + ret);
                }
                else
                {
                    ret = expirationDate + " [Vigente]";
                    LOG.Error("DEHelper.CheckVigente - retorna => " + ret);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DEHelper.CheckVigente Error: " + ex.Message);
                ret = expirationDate;
            }
            LOG.Error("DEHelper.CheckVigente OUT");
            return ret;
        }

        private static string CheckAdult(string birthDate)
        {
            string ret = birthDate;
            try
            {
                LOG.Error("DEHelper.CheckAdult IN - birthDate = " +
                            (string.IsNullOrEmpty(birthDate) ? "NULL" : birthDate));

                if (string.IsNullOrEmpty(birthDate))
                {
                    ret = "No reconocido";
                    LOG.Error("DEHelper.CheckAdult - retorna => " + ret);
                    return ret;
                }

                DateTime date = libs.Utils.Parse(birthDate);
                if (date.Year == 1900) //Si no pudo parsear => Devuelvo el parametro
                {
                    ret = birthDate;
                    LOG.Error("DEHelper.CheckAdult - Año 1900 => retorna parametro => " + birthDate);
                } else
                {
                    int edad = libs.Utils.CalcularEdad(date);
                    if (edad < 18)
                    {
                        ret = birthDate + " [MENOR]";
                        LOG.Error("DEHelper.CheckAdult - retorna => " + ret);
                    }
                    else
                    {
                        ret = birthDate + " [Mayor]";
                        LOG.Error("DEHelper.CheckAdult - retorna => " + ret);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DEHelper.CheckAdult Error: " + ex.Message);
                ret = birthDate;
            }
            LOG.Error("DEHelper.CheckAdult OUT!");
            return ret;
        }

        internal static int GeneraDeclaracionJuradaPDF(Bio.Digital.Receipt.Api.XmlParamIn oXmlIn, out string msg, out string pdfB64)
        {
            msg = "";
            //pdfB64 = "PDFPDF=";
            //qRverify = "QRQR=";
            //trackIDE = "02ab5e9dca234490bc097c334b324055";
            //return 0;

            int ret = 0;
            msg = null;
            string xmlparamin;
            string xmlparamout;
            Biometrika.Document.Electronic.Api.XmlParamOut oParamout;
            string path = null;
            string xmlOutDE = null;
            pdfB64 = null;
            try
            {
                LOG.Debug("DEHelper.GeneraDeclaracionJuradaPDF IN...");

                Biometrika.Document.Electronic.Api.XmlParamIn oParamin = new Biometrika.Document.Electronic.Api.XmlParamIn();
                oParamin.Actionid = 1;  //1 - Generar desde template word | 2 - Firmar un PDF | 3 - Get Documento
                oParamin.IdDE = Properties.Settings.Default.CIDEIdDecJurada;     //Id de tipo de documento documento, informado por Biometrika cuando se crea un nuevo tipo de documento   

                oParamin.EmployeeId = "11111111-1";  //RUT persona que envia a generar el documento o firmar (puede ser generico)
                oParamin.Enduser = "NV_CI";           //Nombre persona que envia a generar el documento o firmar (puede ser generico)          
                oParamin.Ipenduser = "127.0.0.1";    //IP Estacion de trabajo donde se genera el documento
                oParamin.Company = Properties.Settings.Default.CIDECompanyId;                //Id de compañia informada por Biometrika en el momento de dar de alta el servicio
                oParamin.User = "User";              //User para acceso al servicio
                oParamin.Password = "Password";      //Password para acceso al servicio
                oParamin.TypeId = oXmlIn.CIParam.TypeId;            //Tipo de documento del cliente del contrato (Normalemnte en Chile RUT)
                oParamin.ValueId = oXmlIn.CIParam.ValueId;          //Valor de documento del cliente del contrato (Normalemnte en Chile RUT xxxxxxxx-y)
                
                //Cargo los tags a reemplazar en el template del documento word.
                oParamin.Tags = new List<Biometrika.Document.Electronic.Api.Model.Tag>();
                Biometrika.Document.Electronic.Api.Model.Tag item;
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#TypeId#";
                item.Value = oXmlIn.CIParam.TypeId;
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#ValueId#";
                item.Value = oXmlIn.CIParam.ValueId;
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Fecha#";
                item.Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Nombre#";
                item.Value = oXmlIn.CIParam.Name + " " +
                             (string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName)?"Descnocido": oXmlIn.CIParam.PhaterLastName) + " " +
                             (string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName)?"Desconocido": oXmlIn.CIParam.MotherLastName);
                if (!string.IsNullOrEmpty(item.Value) && item.Value.Length > 25) item.Value = item.Value.Substring(0, 25); 
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Selfie#";
                item.Value = oXmlIn.CIParam.Selfie;  //oXmlIn.CIParam.IDCardPhotoImage;
                oParamin.Tags.Add(item);
                
                using (WSDE.WSDE ws = new WSDE.WSDE())
                {
                    ws.Timeout = Properties.Settings.Default.DETimeout;
                    ws.Url = Properties.Settings.Default.Bio_Digital_Receipt_WSDE_WSDE;

                    //Cargo el/los mail/s donde se enviará/n copia/s del documento generado 
                    oParamin.Notarize = false;
                    string[] mails = new string[1];
                    mails[0] = string.IsNullOrEmpty(oXmlIn.CIParam.DestinaryMail) ? 
                                  Properties.Settings.Default.DEMailDefault: oXmlIn.CIParam.DestinaryMail;
                    oParamin.MailsToNotify = mails;
                    LOG.Debug("DEHelper.GeneraDeclaracionJuradaPDF Mail to Notify = " + (string.IsNullOrEmpty(mails[0])?"No Mail Configurado!":mails[0]));

                    string sxIN = Biometrika.Document.Electronic.Api.XmlUtils.SerializeObject<Biometrika.Document.Electronic.Api.XmlParamIn>(oParamin);
                    LOG.Debug("DEHelper.GeneraCertifyPDF DEParamIn = " + sxIN); 
                    //LLamo al al web service de generación con parámetros 
                    ret = ws.Process(sxIN, out xmlparamout);
                    xmlOutDE = xmlparamout;
                    LOG.Debug("DEHelper.GeneraDeclaracionJuradaPDF ret = " + ret.ToString() + " - xmlOutDE = " + xmlOutDE);

                    //Deserializo respuesta y greabo documento generado y/o firmado
                    oParamout = Biometrika.Document.Electronic.Api.XmlUtils.DeserializeObject<
                                    Biometrika.Document.Electronic.Api.XmlParamOut>(xmlOutDE);
                    pdfB64 = oParamout.De;
                    if (Properties.Settings.Default.DebugVideoImagen == 1)
                    {
                        path = Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + "dj_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                        System.IO.File.WriteAllBytes(path, Convert.FromBase64String(oParamout.De));
                        LOG.Debug("DEHelper.GeneraDeclaracionJuradaPDF Save DE in = " + path);
                    }
                }

            }
            catch (Exception ex)
            {
                msg = "Error Desconocido Notarizando = " + ex.Message;
                LOG.Error("DEHelper.GeneraDeclaracionJuradaPDF Error => " + msg);

               ret = -1;
            }
            LOG.Debug("DEHelper.GeneraDeclaracionJuradaPDF OUT!");
            return ret;

        }

        internal static string MergeFiles(string[] listB64Files)
        {
            string ret = null;
            try
            {
                LOG.Debug("DEHelper.MergeFiles IN...");
                
                // Create single pdf.
                DocumentCore singlePDF = new DocumentCore();

                //foreach (string file in supportedFiles)
                for (int i = 0; i < listB64Files.Length; i++)
                {
                    byte[] byFile = Convert.FromBase64String(listB64Files[i]);
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(byFile);
                    DocumentCore dc = DocumentCore.Load(ms, LoadOptions.PdfDefault);
                    ms.Close();

                    //Console.WriteLine("Adding: {0}...", Path.GetFileName(file));

                    // Create import session.
                    ImportSession session = new ImportSession(dc, singlePDF, StyleImportingMode.KeepSourceFormatting);

                    // Loop through all sections in the source document.
                    foreach (Section sourceSection in dc.Sections)
                    {
                        // Because we are copying a section from one document to another,
                        // it is required to import the Section into the destination document.
                        // This adjusts any document-specific references to styles, bookmarks, etc.
                        //
                        // Importing a element creates a copy of the original element, but the copy
                        // is ready to be inserted into the destination document.
                        Section importedSection = singlePDF.Import<Section>(sourceSection, true, session);

                        // First section start from new page.
                        if (dc.Sections.IndexOf(sourceSection) == 0)
                            importedSection.PageSetup.SectionStart = SectionStart.NewPage;

                        // Now the new section can be appended to the destination document.
                        singlePDF.Sections.Add(importedSection);
                    }
                }

                // Save single PDF to a file.
                //singlePDF.Save(singlePDFPath);
                
                System.IO.MemoryStream msout = new System.IO.MemoryStream();
                singlePDF.Save(msout, SaveOptions.PdfDefault);
                byte[] pdfOUT = msout.ToArray();

                System.IO.File.WriteAllBytes(@"c:\tmp\doc.pdf", pdfOUT);
                // = new byte[msout.Length];
                //msout.Write(pdfOUT, 0, (int)msout.Length);
                ret = Convert.ToBase64String(pdfOUT);
                msout.Close();
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error("DEHelper.MergeFiles Error: " + ex.Message);
            }
            LOG.Debug("DEHelper.MergeFiles OUT!");
            return ret;
        }


        /// <summary>
        /// Determina si un documento en B64 es un PDF
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        internal static bool IsDocumentPDF(string document)
        {
            bool ret = false;
            try
            {
                LOG.Debug("DEHelper.IsDocumentPDF IN...");
                if (string.IsNullOrEmpty(document))
                {
                    LOG.Debug("DEHelper.IsDocumentPDF document is null => false");
                    return false;
                }
                // Create single pdf.
                DocumentCore singlePDF = new DocumentCore();
                byte[] byFile = Convert.FromBase64String(document);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(byFile);
                DocumentCore dc = DocumentCore.Load(ms, LoadOptions.PdfDefault);
                ms.Close();

                ret = (dc != null);
                LOG.Debug("DEHelper.IsDocumentPDF ret = " + (ret.ToString()));
                dc = null;
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("DEHelper.IsDocumentPDF Error: " + ex.Message);
            }
            LOG.Debug("DEHelper.IsDocumentPDF OUT!");
            return ret;
        }


        #region NV v2

        /// <summary>
        /// Genera una hoja de resumen para adosar al documento original 
        /// </summary>
        /// <param name="nvup"></param>
        /// <param name="paramIn"></param>
        /// <param name="docToMerge"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal static int GeneraDocFinalToNotarize(NvUsersProcedures nvup, RevisionCompletedModel paramIn, out string docToMerge, out string msg)
        {
            int ret = 0;
            msg = null;
            string xmlparamin;
            string xmlparamout;
            Biometrika.Document.Electronic.Api.XmlParamOut oParamout;
            string path = null;
            string xmlOutDE = null;
            docToMerge = null;
            //pdfDEB64 = null;
            //trackIDE = null;
            //map = null;
            //pdfDJB64 = null;
            //securitycode = "Null";
            //try
            //{
            //    LOG.Debug("DEHelper.GeneraDEPDF IN...");

            //    Biometrika.Document.Electronic.Api.XmlParamIn oParamin = new Biometrika.Document.Electronic.Api.XmlParamIn();
            //    oParamin.Actionid = 1;  //1 - Generar desde template word | 2 - Firmar un PDF | 3 - Get Documento
            //    oParamin.IdDE = Properties.Settings.Default.TTagDEId;     //Id de tipo de documento documento, informado por Biometrika cuando se crea un nuevo tipo de documento   

            //    oParamin.EmployeeId = "11111111-1";  //RUT persona que envia a generar el documento o firmar (puede ser generico)
            //    oParamin.Enduser = "NV_TTag";           //Nombre persona que envia a generar el documento o firmar (puede ser generico)          
            //    oParamin.Ipenduser = "127.0.0.1";    //IP Estacion de trabajo donde se genera el documento
            //    oParamin.Company = Properties.Settings.Default.TTagDECompanyId;                //Id de compañia informada por Biometrika en el momento de dar de alta el servicio
            //    oParamin.User = "User";              //User para acceso al servicio
            //    oParamin.Password = "Password";      //Password para acceso al servicio
            //    oParamin.TypeId = oXmlIn.CIParam.TypeId;            //Tipo de documento del cliente del contrato (Normalemnte en Chile RUT)
            //    oParamin.ValueId = oXmlIn.CIParam.ValueId;          //Valor de documento del cliente del contrato (Normalemnte en Chile RUT xxxxxxxx-y)


            //    LOG.Debug("DEHelper.GeneraDEPDF - Deserializar jsonform: (jsonform == null) => " +
            //                string.IsNullOrEmpty(jsonform).ToString());
            //    //FormData _FORM = Newtonsoft.Json.JsonConvert.DeserializeObject<FormData>(jsonform);
            //    Dictionary<string, string> _FORM = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonform);
            //    LOG.Debug("DEHelper.GeneraDEPDF - _FORM generado: _FORM != null => " + (_FORM != null).ToString());

            //    //Cargo los tags a reemplazar en el template del documento word.
            //    oParamin.Tags = new List<Biometrika.Document.Electronic.Api.Model.Tag>();
            //    Biometrika.Document.Electronic.Api.Model.Tag item;

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#fecha#";
            //    item.Value = DateTime.Now.ToString("dd/MM/yyyy"); // HH:mm:ss");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #fecha# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#rutempresa#";
            //    item.Value = GetDataFromForm(_FORM, "rutempresa");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #rutempresa# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#razonsocialempresa#";
            //    item.Value = GetDataFromForm(_FORM, "nombreempresa");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #razonsocialempresa# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#rutfirmante#";
            //    item.Value = oXmlIn.CIParam.TypeId + " " + oXmlIn.CIParam.ValueId;
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #rutfirmante# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#nombrefirmante#";
            //    item.Value = (string.IsNullOrEmpty(oXmlIn.CIParam.Name) ? "" : oXmlIn.CIParam.Name) + " " +
            //                    (string.IsNullOrEmpty(oXmlIn.CIParam.PhaterLastName) ? "" : oXmlIn.CIParam.PhaterLastName) + " " +
            //                    (string.IsNullOrEmpty(oXmlIn.CIParam.MotherLastName) ? "" : oXmlIn.CIParam.MotherLastName);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #nombrefirmante# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));
            //    if (!string.IsNullOrEmpty(item.Value) && item.Value.Length > 100)
            //    {
            //        item.Value = item.Value.Substring(0, 100);
            //        LOG.Debug("DEHelper.GeneraDEPDF - Cortado #nombrefirmante# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    }
            //    oParamin.Tags.Add(item);

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#telefonofirmante#";
            //    item.Value = GetDataFromForm(_FORM, "telefonofirmante");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #telefonofirmante# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#mailfirmante#";
            //    item.Value = GetDataFromForm(_FORM, "mailfirmante");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #mailfirmante# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#calle#";
            //    item.Value = GetDataFromForm(_FORM, "calle");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #calle# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#numero#";
            //    item.Value = GetDataFromForm(_FORM, "numero");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #numero# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#departamento#";
            //    item.Value = GetDataFromForm(_FORM, "departamento");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #departamento# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#tipovivienda#";
            //    item.Value = GetDataFromForm(_FORM, "tipovivienda");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #tipovivienda# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#comuna#";
            //    item.Value = GetDataFromForm(_FORM, "comuna");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #comuna# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#ciudad#";
            //    item.Value = GetDataFromForm(_FORM, "ciudad");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #ciudad# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#patentevehiculo#";
            //    item.Value = GetDataFromForm(_FORM, "patentevehiculo");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #patentevehiculo# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#marcavehiculo#";
            //    item.Value = GetDataFromForm(_FORM, "marcavehiculo");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #marcavehiculo# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#modelovehiculo#";
            //    item.Value = GetDataFromForm(_FORM, "modelovehiculo");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #modelovehiculo# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#aniovehiculo#";
            //    item.Value = GetDataFromForm(_FORM, "aniovehiculo");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #aniovehiculo# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#colorvehiculo#";
            //    item.Value = GetDataFromForm(_FORM, "colorvehiculo");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #colorvehiculo# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#firmacliente#";
            //    item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.ManualSignatureImage) ?
            //                                Global.IMAGE_NO_AVAILABLE
            //                                : oXmlIn.CIParam.ManualSignatureImage;
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #firmacliente# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#cedulafront#";
            //    item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageFront) ?
            //                                    Global.IMAGE_NO_AVAILABLE
            //                                    : oXmlIn.CIParam.IDCardImageFront;
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #cedulafront# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#cedulaback#";
            //    item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.IDCardImageBack) ? Global.IMAGE_NO_AVAILABLE : oXmlIn.CIParam.IDCardImageBack;
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #cedulaback# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#selfie#";
            //    item.Value = string.IsNullOrEmpty(oXmlIn.CIParam.Selfie) ? oXmlIn.CIParam.IDCardPhotoImage : oXmlIn.CIParam.Selfie;
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #selfie# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#verifyresult#";
            //    item.Value = ((score >= Convert.ToDouble(oXmlIn.CIParam.Threshold)) ? "POSITIVA" : "Negativa");
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #verifyresult# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#score#";
            //    item.Value = score.ToString("##.##") + "%";
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #score# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#padronfront#";
            //    item.Value = string.IsNullOrEmpty(carregisterimagefront) ? Global.IMAGE_NO_AVAILABLE : carregisterimagefront;
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #padronfront# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));

            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#padronback#";
            //    item.Value = string.IsNullOrEmpty(carregisterimageback) ? Global.IMAGE_NO_AVAILABLE : carregisterimageback;
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #padronback# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));


            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#trackid#";
            //    item.Value = oXmlIn.CIParam.TrackId;
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #trackid# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    string sc = null;
            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#securitycode#";
            //    item.Value = GenerateSecurityCode(oParamin.Tags);
            //    sc = item.Value;
            //    securitycode = item.Value;
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #SecurityCode# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value));

            //    string QRverify = GetQR(Properties.Settings.Default.BASE_SIGNER_CHECK + "?trackid=" + oXmlIn.CIParam.TrackId +
            //                                "&securitycode=" + sc);
            //    item = new Biometrika.Document.Electronic.Api.Model.Tag();
            //    item.IdTag = "#QRVerify#";
            //    item.Value = string.IsNullOrEmpty(QRverify) ? "" : QRverify;
            //    oParamin.Tags.Add(item);
            //    LOG.Debug("DEHelper.GeneraDEPDF - Set #QRVerify# = " + (string.IsNullOrEmpty(item.Value) ? "null" : item.Value.Substring(0, 10)));



            //    using (WSDE.WSDE ws = new WSDE.WSDE())
            //    {
            //        LOG.Debug("DEHelper.GeneraDEPDF - Set DETimeout = " + Properties.Settings.Default.DETimeout.ToString());
            //        ws.Timeout = Properties.Settings.Default.DETimeout;
            //        LOG.Debug("DEHelper.GeneraDEPDF - Set URL DE = " + Properties.Settings.Default.Bio_Digital_Receipt_WSDE_WSDE);
            //        ws.Url = Properties.Settings.Default.Bio_Digital_Receipt_WSDE_WSDE;

            //        //Cargo el/los mail/s donde se enviará/n copia/s del documento generado 
            //        oParamin.Notarize = false;
            //        //string[] mails = new string[1];
            //        //mails[0] = string.IsNullOrEmpty(oXmlIn.CIParam.DestinaryMail) ?
            //        //              Properties.Settings.Default.DEMailDefault : oXmlIn.CIParam.DestinaryMail;
            //        oParamin.MailsToNotify = null; //mails;
            //                                        //LOG.Debug("DEHelper.GeneraDEPDF Mail to Notify = " + (string.IsNullOrEmpty(mails[0]) ? "No Mail Configurado!" : mails[0]));
            //        LOG.Debug("DEHelper.GeneraDEPDF - Disabled Mail to Notify and Notarize...");

            //        string sxIN = Biometrika.Document.Electronic.Api.XmlUtils.SerializeObject<Biometrika.Document.Electronic.Api.XmlParamIn>(oParamin);
            //        LOG.Debug("DEHelper.GeneraDEPDF DEParamIn = " + sxIN.Substring(0, 200));
            //        //LLamo al al web service de generación con parámetros 
            //        ret = ws.Process(sxIN, out xmlparamout);
            //        xmlOutDE = xmlparamout;
            //        LOG.Debug("DEHelper.GeneraDEPDF ret = " + ret.ToString() + " - xmlOutDE = " + xmlOutDE);

            //        //Deserializo respuesta y greabo documento generado y/o firmado
            //        oParamout = Document.Manager.Api.XmlUtils.DeserializeObject<Document.Manager.Api.XmlParamOut>(xmlOutDE);
            //        pdfDEB64 = oParamout.De;
            //        trackIDE = oParamout.Trackid;
            //        if (Properties.Settings.Default.DebugVideoImagen == 1)
            //        {
            //            path = Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + "de_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
            //            System.IO.File.WriteAllBytes(path, Convert.FromBase64String(oParamout.De));
            //            LOG.Debug("DEHelper.GeneraDEPDF Save DE in = " + path);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    msg = "Error Desconocido Generando DE = " + ex.Message;
            //    LOG.Error("DEHelper.GeneraDEPDF Error => " + msg);
            //    ret = -1;
            //}
            //LOG.Debug("DEHelper.GeneraDEPDF OUT!");
            return ret;

        }

        #endregion NV v2


        internal static int GeneraCertifyPDF(RdCITx oXmlIn, double score, double threshold, string qRverify, out string msg,
                                             out string pdfCIB64, out string pdfDJB64, out string trackIDE, out string map)
        {
            msg = "";
            //pdfB64 = "PDFPDF=";
            //qRverify = "QRQR=";
            //trackIDE = "02ab5e9dca234490bc097c334b324055";
            //return 0;

            int ret = 0;
            msg = null;
            string xmlparamin;
            string xmlparamout;
            Biometrika.Document.Electronic.Api.XmlParamOut oParamout;
            string path = null;
            string xmlOutDE = null;
            pdfCIB64 = null;
            trackIDE = null;
            map = null;
            pdfDJB64 = null;
            try
            {
                LOG.Debug("DEHelper.GeneraCertifyPDF IN...");

                Biometrika.Document.Electronic.Api.XmlParamIn oParamin = new Biometrika.Document.Electronic.Api.XmlParamIn();
                oParamin.Actionid = 1;  //1 - Generar desde template word | 2 - Firmar un PDF | 3 - Get Documento
                oParamin.IdDE = Properties.Settings.Default.CIDEId;     //Id de tipo de documento documento, informado por Biometrika cuando se crea un nuevo tipo de documento   

                oParamin.EmployeeId = "11111111-1";  //RUT persona que envia a generar el documento o firmar (puede ser generico)
                oParamin.Enduser = "NV_CI";           //Nombre persona que envia a generar el documento o firmar (puede ser generico)          
                oParamin.Ipenduser = "127.0.0.1";    //IP Estacion de trabajo donde se genera el documento
                oParamin.Company = Properties.Settings.Default.CIDECompanyId;                //Id de compañia informada por Biometrika en el momento de dar de alta el servicio
                oParamin.User = "User";              //User para acceso al servicio
                oParamin.Password = "Password";      //Password para acceso al servicio
                oParamin.TypeId = oXmlIn.TypeId;            //Tipo de documento del cliente del contrato (Normalemnte en Chile RUT)
                oParamin.ValueId = oXmlIn.ValueId;          //Valor de documento del cliente del contrato (Normalemnte en Chile RUT xxxxxxxx-y)

                //Cargo los tags a reemplazar en el template del documento word.
                oParamin.Tags = new List<Biometrika.Document.Electronic.Api.Model.Tag>();
                Biometrika.Document.Electronic.Api.Model.Tag item;

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#TrackId#";
                item.Value = oXmlIn.TrackId;
                oParamin.Tags.Add(item);


                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#IDNumber#";
                item.Value = oXmlIn.ValueId; //oXmlIn.TypeId + " " + oXmlIn.ValueId;
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Fecha#";
                item.Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Nombre#";
                item.Value = oXmlIn.Name + " " +
                             (string.IsNullOrEmpty(oXmlIn.PhaterLastName) ? "" : oXmlIn.PhaterLastName) + " " +
                             (string.IsNullOrEmpty(oXmlIn.MotherLastName) ? "" : oXmlIn.MotherLastName);
                if (!string.IsNullOrEmpty(item.Value) && item.Value.Length > 25)
                    item.Value = item.Value.Substring(0, 25);
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#BirthDate#";
                //Chequea si es adulto o menor y agrega
                //oXmlIn.CIParam.BirthDate [Menor] o oXmlIn.CIParam.BirthDate [Mayor]
                item.Value = CheckAdult(oXmlIn.BirthDate);

                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Sex#";
                item.Value = (!string.IsNullOrEmpty(oXmlIn.Sex) &&
                              (oXmlIn.Sex.Equals("M") || oXmlIn.Sex.Equals("F"))) ?
                               oXmlIn.Sex : "Desconocido";
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Nacionality#";
                item.Value = string.IsNullOrEmpty(oXmlIn.Nacionality) ? "Desconocida" : oXmlIn.Nacionality;
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#ExpirationDate#";
                //Chequea si es adulto o menor y agrega
                //oXmlIn.CIParam.ExpirationDate [EXPIRADO] o oXmlIn.CIParam.ExpirationDate [VIGENTE]
                item.Value = CheckVigente(oXmlIn.ExprationDate);
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Serial#";
                item.Value = string.IsNullOrEmpty(oXmlIn.Serial) ? "Desconocida" : oXmlIn.Serial;
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#IDCardImage#";
                item.Value = string.IsNullOrEmpty(oXmlIn.IdCardImageFront) ?
                                                Global.IMAGE_NO_AVAILABLE
                                              : oXmlIn.IdCardImageFront;
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#IDCardImageBack#";
                item.Value = string.IsNullOrEmpty(oXmlIn.IdCardImageBack) ?
                                                Global.IMAGE_NO_AVAILABLE
                                              : oXmlIn.IdCardImageBack;
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#IDCardPhotoImage#";
                item.Value = string.IsNullOrEmpty(oXmlIn.Selfie) ? oXmlIn.IdCardPhotoImage : oXmlIn.Selfie;
                oParamin.Tags.Add(item);
                
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#FingerSample#";
                item.Value = GetImageFromJPGorWSQ(oXmlIn.FingerSampleJpg, oXmlIn.FingerSample);
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#IDCardSignatureImage#";
                item.Value = string.IsNullOrEmpty(oXmlIn.IdCardSignatureImage) ?
                                            Global.IMAGE_NO_AVAILABLE
                                            : oXmlIn.IdCardSignatureImage;
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Map#";
                try
                {
                    map = null;
                    if (!string.IsNullOrEmpty(oXmlIn.GeoRef))
                    {
                        map = Bio.Digital.Receipt.libs.Utils.GetMapFromGoogleMap(oXmlIn.GeoRef);
                    }
                    //TODO - Georef en el workflow
                    //else if (!string.IsNullOrEmpty(oXmlIn.CIParam.GeoRef))
                    //{
                    //    map = Bio.Digital.Receipt.libs.Utils.GetMapFromGoogleMap(oXmlIn.CIParam.GeoRef);
                    //}
                    if (string.IsNullOrEmpty(map))
                        map = Global.MAP_DEFAULT;
                }
                catch (Exception ex)
                {
                    map = Global.MAP_DEFAULT;
                }
                item.Value = map;
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#QRVerify#";
                item.Value = string.IsNullOrEmpty(qRverify) ? "" : qRverify;
                oParamin.Tags.Add(item);

                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Score#";
                double dScore = (!string.IsNullOrEmpty(oXmlIn.Serial) && oXmlIn.Serial.StartsWith("A")) ?
                                    Utils.GetPorcentajeNEC(Convert.ToDouble(score), Convert.ToDouble(threshold)) :
                                    Convert.ToDouble(score);
                //Utils.GetPorcentajeNEC(score, threshold); //Convert.ToDouble(score) > 100 ? 100 : score;
                item.Value = dScore.ToString("##.##") + "%";
                //item.Value = score.ToString("##.##") + "%";
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#Pais#";
                item.Value = "Chile";
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#GeoLoc#";
                item.Value = string.IsNullOrEmpty(oXmlIn.GeoRef) ? "-33.380877,-70.5365727" : oXmlIn.GeoRef;  //Coordenadas de Biometrika si no hay
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#WorkstationId#";
                item.Value = string.IsNullOrEmpty(oXmlIn.WorkstationId) ? "Desconocido" : oXmlIn.WorkstationId;
                oParamin.Tags.Add(item);
                item = new Biometrika.Document.Electronic.Api.Model.Tag();
                item.IdTag = "#IDCardPhotoImageDJ#";
                item.Value = string.IsNullOrEmpty(oXmlIn.Selfie) ? oXmlIn.IdCardPhotoImage : oXmlIn.Selfie;
                oParamin.Tags.Add(item);

                //oParamin.Tags.Add(item);

                //oParamin.BSSignatures = new List<Biometrika.Document.Electronic.Api.BS>();
                //oParamin.BSSignatures.Add(bs);
                //xmlparamin = Biometrika.Document.Electronic.Api.XmlUtils.SerializeAnObject(oParamin);

                using (WSDE.WSDE ws = new WSDE.WSDE())
                {
                    ws.Timeout = Properties.Settings.Default.DETimeout;
                    ws.Url = Properties.Settings.Default.Bio_Digital_Receipt_WSDE_WSDE;

                    //Cargo el/los mail/s donde se enviará/n copia/s del documento generado 
                    oParamin.Notarize = false;
                    string[] mails = new string[1];
                    mails[0] = string.IsNullOrEmpty(oXmlIn.DestinataryMail) ?
                                  Properties.Settings.Default.DEMailDefault : oXmlIn.DestinataryMail;
                    oParamin.MailsToNotify = mails;
                    LOG.Debug("DEHelper.GeneraCertifyPDF Mail to Notify = " + (string.IsNullOrEmpty(mails[0]) ? "No Mail Configurado!" : mails[0]));

                    string sxIN = Biometrika.Document.Electronic.Api.XmlUtils.SerializeObject<Biometrika.Document.Electronic.Api.XmlParamIn>(oParamin);
                    LOG.Debug("DEHelper.GeneraCertifyPDF DEParamIn = " + sxIN);
                    //LLamo al al web service de generación con parámetros 
                    ret = ws.Process(sxIN, out xmlparamout);
                    xmlOutDE = xmlparamout;
                    LOG.Debug("DEHelper.GeneraCertifyPDF ret = " + ret.ToString() + " - xmlOutDE = " + xmlOutDE);

                    //Deserializo respuesta y greabo documento generado y/o firmado
                    oParamout = Biometrika.Document.Electronic.Api.XmlUtils.DeserializeObject<
                                    Biometrika.Document.Electronic.Api.XmlParamOut>(xmlOutDE);
                    pdfCIB64 = oParamout.De;
                    trackIDE = oParamout.Trackid;
                    if (Properties.Settings.Default.DebugVideoImagen == 1)
                    {
                        path = Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + "de_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                        System.IO.File.WriteAllBytes(path, Convert.FromBase64String(oParamout.De));
                        LOG.Debug("DEHelper.GeneraCertifyPDF Save DE in = " + path);
                    }
                }

                //if (GeneraDeclaracionJuradaPDF(oXmlIn, out msg, out pdfDJB64) != 0)
                //{
                //    msg = msg + " [No se pudo generar la Declaración Jurada. Se aborta generación de CI]"; 
                //    pdfCIB64 = null;
                //    ret = Errors.IRET_ERR_CI_DECLARACIONJURADA_INCORRECT;
                //}
            }
            catch (Exception ex)
            {
                msg = "Error Desconocido Notarizando = " + ex.Message;
                LOG.Error("DEHelper.GeneraCertifyPDF Error => " + msg);

                ret = -1;
            }
            LOG.Debug("DEHelper.GeneraCertifyPDF OUT!");
            return ret;

        }

        

        private static string GetImageFromJPGorWSQ(string fingerSamplejpg, string fingerSample)
        {
            string ret = Global.IMAGE_FINGER_NO_AVAILABLE;
            byte[] raw;
            short w,h;
            try
            {
                if (!string.IsNullOrEmpty(fingerSamplejpg))
                {
                    ret = fingerSamplejpg;
                }
                else if (!string.IsNullOrEmpty(fingerSamplejpg))
                {
                    Bio.Core.Wsq.Decoder.WsqDecoder dec = new Bio.Core.Wsq.Decoder.WsqDecoder();
                    if (dec.DecodeMemory(Convert.FromBase64String(fingerSample), out w, out h, out raw))
                    {
                        Bitmap bmpImage = Bio.Core.Imaging.ImageProcessor.RawToBitmap(raw, w, h);
                        ret = ImageToBase64(bmpImage, System.Drawing.Imaging.ImageFormat.Bmp);
                    }
                } 
            }
            catch (Exception ex)
            {
                ret = Global.IMAGE_FINGER_NO_AVAILABLE;
                LOG.Error("DEHelper.GetImageFromWSQ - Error: " + ex.Message);
            }
            return ret;
        }

        internal static string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            string base64String = null;
            LOG.Debug("DEHelper.ImageToBase64 IN...");
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    image.Save(ms, format);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    base64String = Convert.ToBase64String(imageBytes);
                    LOG.Debug("DEHelper.ImageToBase64 base64String = " + base64String);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("DEHelper.ImageToBase64 Error = " + ex.Message, ex);
                base64String = null;
            }
            LOG.Debug("DEHelper.ImageToBase64 OUT!");
            return base64String;
        }
    }
}