﻿using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.Core.Helpers
{

    /// <summary>
    /// Ayuda para manejo de imagenes
    /// </summary>
    public class ImageHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ImageHelper));

        /// <summary>
        /// Dada una foto sin marca agua de la FE3D, le agrega eso
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="watermarkText"></param>
        /// <param name="outputStream"></param>
        /// <param name="fonttype"></param>
        /// <param name="sizefont"></param>
        /// <param name="fontstyle"></param>
        /// <param name="fontcolor"></param>
        /// <param name="xposinit"></param>
        /// <param name="yposinit"></param>
        /// <param name="distance"></param>
        /// <param name="company"></param>
        /// <param name="firmanteid"></param>
        /// <param name="documentid"></param>
        public void AddWaterMark(MemoryStream ms, string watermarkText, MemoryStream outputStream,
                                string fonttype, float sizefont, FontStyle fontstyle, Color fontcolor,
                                int xposinit, int yposinit, int distance,
                                string company, string firmanteid, string documentid)
        {
            try
            {
                System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
                Graphics gr = Graphics.FromImage(img);
                Color color = fontcolor; // Color.FromArgb(50, 241, 235, 105);

                StringFormat stringFormat = new StringFormat();
                stringFormat.Alignment = StringAlignment.Near;
                stringFormat.LineAlignment = StringAlignment.Near;

                Font font = new Font(fonttype, sizefont, fontstyle);
                int ypos = yposinit; //65
                gr.DrawString(company, font, new SolidBrush(color), new Point(xposinit, ypos), stringFormat);
                gr.DrawString(firmanteid, font, new SolidBrush(color), new Point(xposinit, ypos + distance), stringFormat);
                gr.DrawString(documentid, font, new SolidBrush(color), new Point(xposinit, ypos + (2 * distance)), stringFormat);

                img.Save(outputStream, ImageFormat.Jpeg);

            }
            catch (Exception ex)
            {
                outputStream = null;
                LOG.Error("ImageHelper.AddWaterMark Error", ex);
            }

            //img.Save(@"C:\tmp\aa\WaterMark_ConWM.bmp", ImageFormat.Jpeg);
        }

        /// <summary>
        /// Dada una foto sin marca agua de la FE3D, le agrega eso
        /// </summary>
        /// <param name="imgin"></param>
        /// <param name="watermarkText"></param>
        /// <param name="imgout"></param>
        /// <param name="fonttype"></param>
        /// <param name="sizefont"></param>
        /// <param name="fontstyle"></param>
        /// <param name="fontcolor"></param>
        /// <param name="xposinit"></param>
        /// <param name="yposinit"></param>
        /// <param name="distance"></param>
        /// <param name="company"></param>
        /// <param name="firmanteid"></param>
        /// <param name="documentid"></param>
        void AddWaterMark(Image imgin, string watermarkText, out Image imgout,
                                        string fonttype, float sizefont, FontStyle fontstyle, Color fontcolor,
                                        int xposinit, int yposinit, int distance,
                                        string company, string firmanteid, string documentid)
        {
            try
            {
                imgout = null;
                MemoryStream msin = new MemoryStream();
                imgin.Save(msin, ImageFormat.Jpeg);

                MemoryStream msou = new MemoryStream();
                AddWaterMark(msin, watermarkText, msou, fonttype, sizefont, fontstyle, fontcolor,
                                            xposinit, yposinit, distance, company, firmanteid, documentid);

                imgout = Image.FromStream(msou);
            }
            catch (Exception ex)
            {
                imgout = null;
                LOG.Error("ImageHelper.AddWaterMark Error", ex);
            }
        }


        /// <summary>
        /// Overlay an image over a background image.
        /// </summary>
        /// <param name="backImage">The background image.</param>
        /// <param name="topImage">The topmost image.</param>
        /// <param name="topPosX">An optional adjustment of the top image's "X" position.</param>
        /// <param name="topPosY">An optional adjustment of the top image's "Y" position.</param>
        /// <returns>The overlayed image.</returns>
        /// <exception cref="ArgumentNullException">backImage or topImage</exception>
        /// <exception cref="ArgumentException">Image bounds are greater than background image.;topImage</exception>
        public Image OverlayImages(Image backImage, Image topImage, int topPosX = 0, int topPosY = 0)
        {
            Image imgout;
            try
            {
                if (backImage == null)
                {
                    LOG.Error("ImageHelper.OverlayImages Error BackImage null");
                    return null;
                }
                else if (topImage == null)
                {
                    LOG.Error("ImageHelper.OverlayImages Error topImage null");
                    return null;
                }
                else if ((topImage.Width > backImage.Width) || (topImage.Height > backImage.Height))
                {
                    LOG.Error("ImageHelper.OverlayImages Error topImage debe ser mas chica que backImage");
                    return null;
                }
                else
                {
                    Bitmap bmp = new Bitmap(backImage.Width, backImage.Height);

                    using (Graphics canvas = Graphics.FromImage(bmp))
                    {
                        canvas.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        canvas.DrawImage(image: backImage, destRect: new Rectangle(0, 0, bmp.Width, bmp.Height), srcRect: new Rectangle(0, 0, bmp.Width, bmp.Height), srcUnit: GraphicsUnit.Pixel);
                        canvas.DrawImage(image: topImage, destRect: new Rectangle(topPosX, topPosY, topImage.Width, topImage.Height), srcRect: new Rectangle(0, 0, topImage.Width, topImage.Height), srcUnit: GraphicsUnit.Pixel);

                        canvas.Save();
                    }
                    imgout = bmp;
                }
            }
            catch (Exception ex)
            {
                imgout = null;
                LOG.Error("ImageHelper.OverlayImages Error", ex);
            }
            return imgout;
        }

    }
}