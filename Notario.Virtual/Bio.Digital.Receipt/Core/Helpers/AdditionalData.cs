﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using log4net;

namespace Bio.Digital.Receipt.Core.Helpers
{
    //public class AdditionalData
    //{
    //}
    [Serializable]
    public class AdditionalData
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(AdditionalData));

        public int Score;
        public string TrackId;

        //public List<AdditionalDataItem> AdditionalDataItems;

        //#region Operations
        ///// <summary>
        ///// Retorna la lista de items de key/value
        ///// </summary>
        ///// <returns>Lista de datos dinamicos</returns>
        //public List<AdditionalDataItem> GetItems()
        //{
        //    return AdditionalDataItems;
        //}

        ///// <summary>
        ///// Dada una clave, retorna el valor si existe la clave. sino nulll
        ///// </summary>
        ///// <param name="key">Clave para buscar</param>
        ///// <returns>Objeto con el valor del parametro</returns>
        //public object GetValue(string key)
        //{
        //    object oRes = null;

        //    try
        //    {
        //        if (AdditionalDataItems == null || AdditionalDataItems.Count == 0)
        //            return null;

        //        for (var i = 0; i < AdditionalDataItems.Count; i++)
        //        {
        //            if (key == AdditionalDataItems[i].key)
        //            {
        //                oRes = AdditionalDataItems[i].value;
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Bio.Core.Api.AdditionalData.GetValue", ex);
        //    }

        //    return oRes;
        //}

        ///// <summary>
        ///// Setea un valor en una clave dada
        ///// </summary>
        ///// <param name="key">Clave para actualizar</param>
        ///// <param name="value">Valor del item a actualizar</param>
        ///// <returns>True o false si actualizo bien o mal respectivamente</returns>
        //public bool SetValue(string key, string value)
        //{
        //    bool bRes = false;
        //    try
        //    {
        //        if (AdditionalDataItems == null || AdditionalDataItems.Count == 0)
        //            return false;

        //        for (int i = 0; i < AdditionalDataItems.Count; i++)
        //        {
        //            if (key == (AdditionalDataItems[i]).key)
        //            {
        //                AdditionalDataItems[i].value = value;
        //                bRes = true;
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Bio.Core.Api.AdditionalData.SetValue", ex);
        //    }

        //    return bRes;
        //}

        ///// <summary>
        ///// Agrega un valor en una clave dada
        ///// </summary>
        ///// <param name="key">Clave para agregar</param>
        ///// <param name="value">Valor del item a agregar</param>
        ///// <returns>True o false si agrego bien o mal respectivamente</returns>
        //public bool AddValue(string key, string value)
        //{
        //    bool bRes = false;
        //    try
        //    {
        //        if (AdditionalDataItems == null)
        //            AdditionalDataItems = new List<AdditionalDataItem>();

        //        AdditionalDataItems.Add(new AdditionalDataItem(key, value));
        //        bRes = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Bio.Core.Api.AdditionalData.AddValue", ex);
        //    }

        //    return bRes;
        //}

        //#endregion Operations

        //#region Serialize

        ///// <summary>
        ///// Serializa un objeto DynamicData a disco
        ///// </summary>
        ///// <param name="ddata">DynamicData a serializar</param>
        ///// <param name="path">Path absoluto donde serializar</param>
        ///// <returns>true o false en exito y fracaso respectivamente</returns>
        //public static bool SerializeToFile(AdditionalDataItem ddata, string path)
        //{
        //    bool bRes = false;

        //    try
        //    {
        //        XmlSerializer serializer = new XmlSerializer(typeof(AdditionalDataItem));
        //        TextWriter writer = new StreamWriter(path);
        //        serializer.Serialize(writer, ddata);
        //        writer.Close();
        //        bRes = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Bio.Core.Api.AdditionalData.SaveToFile", ex);
        //    }
        //    return bRes;
        //}

        ///// <summary>
        ///// Deserializa un objeto Parameters desde disco
        ///// </summary>
        ///// <param name="path">Path absoluto donde serializar</param>
        ///// <returns>Objeto deserializado o null de haber error</returns>
        //public static AdditionalData DeserializeFromFile(string path)
        //{
        //    try
        //    {
        //        XmlSerializer serializer = new XmlSerializer(typeof(AdditionalData));
        //        TextReader reader = new StreamReader(path);
        //        AdditionalData cf = (AdditionalData)serializer.Deserialize(reader);
        //        reader.Close();
        //        return cf;
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Bio.Core.Api.AdditionalData.SaveToFile", ex);
        //    }
        //    return null;
        //}

        ///// <summary>
        ///// Serializa un objeto DynamicData y lo devuelve en un string
        ///// </summary>
        ///// <param name="ddata">DynamicData a serializar</param>
        ///// <returns>true o false en exito y fracaso respectivamente</returns>
        //public static string SerializeToXml(AdditionalData ddata)
        //{
        //    string xmlRes = null;

        //    try
        //    {
        //        MemoryStream memoryStream = new MemoryStream();
        //        XmlSerializer xs = new XmlSerializer(ddata.GetType()); //typeof(T));
        //        XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
        //        xs.Serialize(xmlTextWriter, ddata);
        //        memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
        //        xmlRes = UTF8ByteArrayToString(memoryStream.ToArray());
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Bio.Core.Api.AdditionalData.SerializeToXml", ex);
        //    }
        //    return xmlRes;
        //}

        ///// <summary>
        ///// Serializa un objeto Parameters y lo devuelve en un string
        ///// </summary>
        ///// <param name="xmlddata">Xml del objeto a deserializar</param>
        ///// <returns>true o false en exito y fracaso respectivamente</returns>
        //public static AdditionalData DeserializeFromXml(string xmlddata)
        //{
        //    AdditionalData olRes = null;
        //    XmlSerializer xs;
        //    try
        //    {
        //        XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
        //        ns.Add("", "");
        //        xs = new XmlSerializer(typeof(AdditionalData), "");
        //        MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(xmlddata));
        //        XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
        //        olRes = (AdditionalData)xs.Deserialize(memoryStream);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Bio.Core.Api.AdditionalData.DeserializeFromXml", ex);
        //    }
        //    return olRes;
        //}

        //#endregion Serialize

        //private static Byte[] StringToUTF8ByteArray(string pXmlString)
        //{
        //    byte[] byteArray = null;
        //    try
        //    {
        //        UTF8Encoding encoding = new UTF8Encoding();
        //        byteArray = encoding.GetBytes(pXmlString);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Bio.Core.Api.AdditionalData.StringToUTF8ByteArray", ex);
        //    }
        //    return byteArray;
        //}

        //private static string UTF8ByteArrayToString(byte[] characters)
        //{
        //    string constructedString = null;
        //    try
        //    {
        //        UTF8Encoding encoding = new UTF8Encoding();
        //        constructedString = encoding.GetString(characters);
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("Bio.Core.Api.AdditionalData.UTF8ByteArrayToString", ex);
        //    }
        //    return constructedString;
        //}
    }

    ///<summary>
    /// Items de DynamicData
    ///</summary>
    public class AdditionalDataItem
    {
        public AdditionalDataItem()
        {
        }


        public AdditionalDataItem(string pkey, string pvalue)
        {
            _key = pkey;
            _value = pvalue;
        }

        #region Private

        string _key;
        string _value;


        #endregion Private

        #region Public

        ///<summary>
        ///</summary>
        public string key
        {
            get { return _key; }
            set { _key = value; }
        }
        ///<summary>
        ///</summary>
        public string value
        {
            get { return _value; }
            set { _value = value; }
        }

        #endregion Public


    }
}