/*
using MyGeneration/Template/NHibernate (c) by lujan99@usa.net
*/
using Bio.Digital.Receipt.Core.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using log4net;
using Bio.Core.pki.utils;

namespace Bio.Digital.Receipt.Core.Helpers
{
	/// <summary>
	/// RdAplicaciones object for NHibernate mapped table 'rd_aplicaciones'.
	/// </summary>
	[Serializable]
	public class RdAplicacionesMemory : INotifyPropertyChanged
	{
        private static readonly ILog LOG = LogManager.GetLogger(typeof(RdAplicacionesMemory));


        #region Member Variables
        protected RdAplicaciones oRdApp;
  //      protected int _id;
		//protected string _nombre;
		//protected int _checkingen;
  //      protected int _company;
  //      // 0 - Default (Usa Firma Global) | 1 - Firma simle con Certificado en table | 2 - FEA por WS e-sign u otra CA
  //      protected int _signaturetype;
  //      protected string _certificatecer;
  //      protected string _certificatepfx;
  //      protected string _signaturepsw;
  //      protected string _urlfea;
  //      protected string _appfea;
  //      protected DateTime? _deleted;

        private AsymmetricAlgorithm key;
        private X509Certificate msCert;
        private X509Certificate2 msCert2;

        protected IList<RdExtensiones> _rdextensiones;
		public event PropertyChangedEventHandler PropertyChanged;
		
		#endregion
		#region Constructors
			
		public RdAplicacionesMemory() {}

        //public RdAplicacionesMemory(int id, string nombre, int checkingen, int company, int signaturetype,
        //                            string certificatecer, string certificatepfx, string signaturepsw,
        //                            string urlfea, string appfea)
        //{
        //    this._id = id;
        //    this._nombre = nombre;
        //    this._checkingen = checkingen;
        //    this._company = company;
        //    this._signaturetype = signaturetype;
        //    this._certificatecer = certificatecer;
        //    this._certificatepfx = certificatepfx;
        //    this._signaturepsw = signaturepsw;
        //    this._urlfea = urlfea;
        //    this._appfea = appfea;

        //    Initialize();
        //}

        public RdAplicacionesMemory(RdAplicaciones oRd)
        {
            this.oRdApp = oRd;

            Initialize();
        }

        public void Initialize()
        {
            try
            {
                // 0 - Default (Usa Firma Global) | 1 - Firma simle con Certificado en table | 2 - FEA por WS e-sign u otra CA
                LOG.Debug("RdAplicacionesMemory - Initialize RdApp = " + this.oRdApp.Id + "-" + this.oRdApp.Nombre +
                    " - SignatureType = " + this.oRdApp.SignatureType);
                switch (this.oRdApp.SignatureType)
                {
                    case 0: //No hace nada porque usa certificado global.
                        break;
                    case 1: //Levanta Certificados y keys para firmar
                        MsCert = X509Certificate.CreateFromCertFile(this.oRdApp.CertificateCER);
                        MsCert2 = BKpkcs12.BKOpenPFX(this.oRdApp.CertificatePFX, this.oRdApp.SignaturePsw);
                        Key = BKpkcs12.GetPrivateKeyFromPFX(this.oRdApp.CertificatePFX, this.oRdApp.SignaturePsw);
                        LOG.Debug("RdAplicacionesMemory - Loaded OK Certs y Key from " + this.oRdApp.CertificatePFX
                            + " para RdApp = " + this.oRdApp.Id + "-" + this.oRdApp.Nombre);
                        break;
                    case 2:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("RdAplicacionesMemory - Error = " + ex.Message);
            }
        }

        //public void Initialize()
        //{
        //    try
        //    {
        //        // 0 - Default (Usa Firma Global) | 1 - Firma simle con Certificado en table | 2 - FEA por WS e-sign u otra CA
        //        switch (_signaturetype)
        //        {
        //            case 0: //No hace nada porque usa certificado global.
        //                break;
        //            case 1: //Levanta Certificados y keys para firmar
        //                MsCert = X509Certificate.CreateFromCertFile(_certificatecer);
        //                MsCert2 = BKpkcs12.BKOpenPFX(_certificatepfx, _signaturepsw);
        //                Key = BKpkcs12.GetPrivateKeyFromPFX(_certificatepfx, _signaturepsw);
        //                LOG.Debug("RdAplicacionesMemory - Loaded OK Certs y Key from " + _certificatepfx + " para RdApp = " + _id + "-" + _nombre);
        //                break;
        //            case 2:
        //                break;
        //            default:
        //                _signaturetype = 0;
        //                break;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("RdAplicacionesMemory - Error = " + ex.Message);
        //    }
        //}

        #endregion
        #region Public Properties
        //      public virtual int Id
        //{
        //	get { return _id; }
        //	set {if (value != this._id){_id= value;NotifyPropertyChanged("Id");}}
        //}
        //public  virtual string Nombre
        //{
        //	get { return _nombre; }
        //	set {
        //		if ( value != null && value.Length > 50)
        //			throw new ArgumentOutOfRangeException("value", value.ToString(), "Nombre cannot contain more than 50 characters");
        //		if (value != this._nombre){_nombre= value;NotifyPropertyChanged("Nombre");}}
        //}
        //      public virtual int Checkingen
        //      {
        //          get { return _checkingen; }
        //          set { if (value != this._checkingen) { _checkingen = value; NotifyPropertyChanged("Checkingen"); } }
        //      }
        //      public virtual int Company
        //      {
        //          get { return _company; }
        //          set { if (value != this._company) { _company = value; NotifyPropertyChanged("Company"); } }
        //      }

        //      public virtual int SignatureType
        //      {
        //          get { return _signaturetype; }
        //          set { if (value != this._signaturetype) { _signaturetype = value; NotifyPropertyChanged("SignatureType"); } }
        //      }
        //      public virtual string CertificatePFX
        //      {
        //          get { return _certificatepfx; }
        //          set
        //          {
        //             if (value != this._certificatepfx) { _nombre = value; NotifyPropertyChanged("CertificatePFX"); }
        //          }
        //      }
        //      public virtual string CertificateCER
        //      {
        //          get { return _certificatecer; }
        //          set
        //          {
        //              if (value != this._certificatecer) { _nombre = value; NotifyPropertyChanged("CertificateCER"); }
        //          }
        //      }
        //      public virtual string SignaturePsw
        //      {
        //          get { return _signaturepsw; }
        //          set
        //          {
        //              if (value != null && value.Length > 100)
        //                  throw new ArgumentOutOfRangeException("value", value.ToString(), "SignaturePsw cannot contain more than 0 characters");
        //              if (value != this._signaturepsw) { _signaturepsw = value; NotifyPropertyChanged("SignaturePsw"); }
        //          }
        //      }

        //      public virtual string URLFEA
        //      {
        //          get { return _urlfea; }
        //          set
        //          {
        //              if (value != null && value.Length > 150)
        //                  throw new ArgumentOutOfRangeException("value", value.ToString(), "URLFEA cannot contain more than 0 characters");
        //              if (value != this._urlfea) { _urlfea = value; NotifyPropertyChanged("URLFEA"); }
        //          }
        //      }

        //      public virtual string APPFEA
        //      {
        //          get { return _appfea; }
        //          set
        //          {
        //              if (value != null && value.Length > 150)
        //                  throw new ArgumentOutOfRangeException("value", value.ToString(), "APPFEA cannot contain more than 0 characters");
        //              if (value != this._appfea) { _appfea = value; NotifyPropertyChanged("APPFEA"); }
        //          }
        //      }

        //      public virtual DateTime? Deleted
        //      {
        //          get { return _deleted; }
        //          set { if (value != this._deleted) { _deleted = value; NotifyPropertyChanged("Deleted"); } }
        //      }

        //      public virtual IList<RdExtensiones> RdExtensiones
        //{
        //	get { return _rdextensiones; }
        //	set {_rdextensiones= value; }
        //}

        //public RdAplicaciones RdApp { get => oRdApp; set => oRdApp = value; }
        //public AsymmetricAlgorithm Key { get => key; set => key = value; }
        //public X509Certificate MsCert { get => msCert; set => msCert = value; }
        //public X509Certificate2 MsCert2 { get => msCert2; set => msCert2 = value; }

        public RdAplicaciones RdApp { get { return oRdApp; } set { oRdApp = value; } }
        public AsymmetricAlgorithm Key { get { return key; } set { key = value; } }
        public X509Certificate MsCert { get { return msCert; } set { msCert = value; } }
        public X509Certificate2 MsCert2 { get { return msCert2; } set { msCert2 = value; } }


        protected virtual void NotifyPropertyChanged(String info)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(info));
			}
		}
		
		#endregion
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		//public override bool Equals( object obj )
		//{
		//	if( this == obj ) return true;
		//	if( ( obj == null ) || ( obj.GetType() != this.GetType() ) ) return false;
  //          RdAplicacionesMemory castObj = (RdAplicacionesMemory)obj;
		//	return ( castObj != null ) &&
		//	this.oRdApp.Id == castObj.Id;
		//}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		//public override int GetHashCode()
		//{
		//	int hash = 57;
		//	hash = 27 * hash * _id.GetHashCode();
		//	return hash;
		//}
		#endregion
		
	}
}
