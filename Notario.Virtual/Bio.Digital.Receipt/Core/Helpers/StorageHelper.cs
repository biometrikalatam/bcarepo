﻿using log4net;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bio.Digital.Receipt.Core.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class StorageHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(StorageHelper));

        internal static int UploadToStorageFEMInAzure(byte[] arrVideo, string company, string filename, out string urlvideoinstorage, out string msgerr)
        {
            int ret = 0;

            LOG.Debug("StorageHelper.UploadToStorageFEMInAzure IN...");
            //urlvideoinstorage = "http://storage.biometrikalatam.com/femstorage/Company7/2018/11/05/20181105100520_212844152_fem3d.avi";

            //byte[] byVideo = System.IO.File.ReadAllBytes(@"c:\tmp\_FE3d_tmp.avi.compressed");

            string folderfile = company + "/" + GetFolderDate() + filename + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".mp4";
            LOG.Debug("StorageHelper.UploadToStorageFEMInAzure folderfile => " + folderfile);
            ret = ProcessAsync(arrVideo, folderfile, out urlvideoinstorage, out msgerr); //.GetAwaiter().GetResult();
            LOG.Debug("StorageHelper.UploadToStorageFEMInAzure Upload = " + ret + " - Url = " + urlvideoinstorage);
            return ret;
        }

        private static string GetFolderDate()
        {
            string ret = "";
            try
            {
                DateTime now = DateTime.Now;
                string year = now.Year.ToString();
                string month = now.Month.ToString();
                if (month.Length == 1) month = "0" + month;
                string day = now.Day.ToString();
                if (day.Length == 1) day = "0" + day;
                ret = year + "/" + month + "/" + day + "/";
                LOG.Debug("StorageHelper.GetFolderDate FolderDate => " + ret);
            }
            catch (Exception ex)
            {
                LOG.Error("StorageHelper.GetFolderDate Error " + ex.Message);
            }
            return ret;
        }


        internal static int ProcessAsync(byte[] video, string folderfile, out string urlvideoinstorage, out string msgerr)
        {
            int ret = 0;
            msgerr = "";
            urlvideoinstorage = "";
            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null;
            string sourceFile = null;
            string destinationFile = null;

            LOG.Debug("StorageHelper.ProcessAsync IN...");
            // Retrieve the connection string for use with the application. The storage connection string is stored
            // in an environment variable on the machine running the application called storageconnectionstring.
            // If the environment variable is created after the application is launched in a console or with Visual
            // Studio, the shell needs to be closed and reloaded to take the environment variable into account.
            string storageConnectionString = Properties.Settings.Default.StorageConnectionString; // "DefaultEndpointsProtocol=https;AccountName=femstorage;AccountKey=gMksxXzIPz8O+KuCN3aFdPd+s9iydHoMSCmQ5SMNJdNP/vn2nGhru7YUxdm74G2w3Tbdqm8f/zzCLf1QWYrXdA==;EndpointSuffix=core.windows.net"; // Environment.GetEnvironmentVariable("storageconnectionstring");
            LOG.Debug("StorageHelper.ProcessAsync storageConnectionString = " + storageConnectionString);
            // Check whether the connection string can be parsed.
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                try
                {
                    LOG.Debug("StorageHelper.ProcessAsync storageAccount parsed = " + storageAccount.Credentials.AccountName);
                    // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
                    CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                    cloudBlobContainer = cloudBlobClient.GetContainerReference("femstoragedev");
                    bool created = cloudBlobContainer.CreateIfNotExists();
                    LOG.Debug("StorageHelper.ProcessAsync Container = " + cloudBlobContainer.Name);

                    // Get a reference to the blob address, then upload the file to the blob.
                    // Use the value of localFileName for the blob name.
                    LOG.Debug("StorageHelper.ProcessAsync Getting reference to = " + folderfile);
                    CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(folderfile);
                    LOG.Debug("StorageHelper.ProcessAsync Uploading file length = " + video.Length.ToString());
                    cloudBlockBlob.UploadFromByteArray(video, 0, video.Length);
                    urlvideoinstorage = cloudBlockBlob.Uri.AbsoluteUri;
                    LOG.Debug("StorageHelper.ProcessAsync urlvideoinstorage = " + urlvideoinstorage);
                }
                catch (Exception ex)
                {
                    ret = -1;
                    msgerr = ex.Message;
                    LOG.Error("StorageHelper.ProcessAsync Error = " + msgerr);
                }
            }
            else
            {
                ret = -2;
                LOG.Error("StorageHelper.ProcessAsync Error parseando storageConnectionString...");
            }

            LOG.Debug("StorageHelper.ProcessAsync OUT! ret = " + ret.ToString());
            return ret;
        }
    }
}