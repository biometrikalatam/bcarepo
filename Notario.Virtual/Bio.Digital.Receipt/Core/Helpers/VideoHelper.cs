﻿//using Leadtools.Multimedia;
//using LMVIOvLyLib;
//using LMVTextOverlayLib;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bio.Digital.Receipt.Core.Helpers
{
    public class VideoHelper
    {
        //private static readonly ILog LOG = LogManager.GetLogger(typeof(VideoHelper));

        ////public delegate void EndConvertResult();
        ////public event EndConvertResult EndConvert;

        //ConvertCtrl ConvertCtrl1;
        //private byte[] _videoout;
        //System.IO.MemoryStream _MMTarget;
        //UnlockSupport unlocksprt;

        //public bool HayResult = false;
        //public bool _converting = false;

        //public VideoHelper() {
        //    //unlocksprt = new UnlockSupport();
        //    //unlocksprt.Unlock("722010-1580056-000");
        //    ConvertCtrl1 = new ConvertCtrl();

        //    ConvertCtrl1.Complete += ConvertCtrl1_Complete;
        //    ConvertCtrl1.ErrorAbort += ConvertCtrl1_ErrorAbort;
        //    ConvertCtrl1.Progress += ConvertCtrl1_Progress;
        //}

        //public bool UnLock()
        //{
        //    bool bret = false;

        //    try
        //    {
        //        LOG.Debug("VideoProcessor.UnLock IN...");
        //        unlocksprt = new UnlockSupport();
        //        unlocksprt.Unlock("722010-1580056-000");
        //        bret = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Debug("VideoProcessor.UnLock Error - " + ex.Message);
        //        bret = false;
        //    }
        //    LOG.Debug("VideoProcessor.UnLock OUT!");
        //    return bret;

        //}

        //public bool Lock()
        //{
        //    bool bret = false;

        //    try
        //    {
        //        LOG.Debug("VideoProcessor.Lock IN...");
        //        unlocksprt.Lock("722010-1580056-000");
        //        bret = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Debug("VideoProcessor.Lock Error - " + ex.Message);
        //        bret = false;
        //    }
        //    LOG.Debug("VideoProcessor.Lock OUT!");
        //    return bret;

        //}


        //public void StopConverting()
        //{
        //    _converting = false;
        //}

        //public async Task<ResultAWM> AddWaterMark(byte[] videoin, string path, string Company, string TypeId, string ValueId, string DocumentId) //, out byte[] videoout)
        //{

        //    ResultAWM oRet = new ResultAWM(-1, null, null, null);

        //    return await Task.Run(() =>
        //    {
        //        int ret = 0;
        //        string msgerr = null;
        //        string strvideoout = null;
        //        string strfotoout = null;
        //        //videoout = null;
        //        try
        //        {
        //            HayResult = false;
        //            _videoout = null;
        //            _converting = true;

        //            System.IO.MemoryStream MMSource = new System.IO.MemoryStream(videoin);
        //            //_MMTarget = new System.IO.MemoryStream();

        //            ConvertCtrl1.TargetFormat = Leadtools.Multimedia.TargetFormatType.ISO;
        //            ConvertCtrl1.VideoCompressors.H264.Selected = true;
        //            ConvertCtrl1.AudioCompressors.AAC.Selected = true;
        //            //ConvertCtrl1.TargetType = TargetObjectType.Stream;
        //            //ConvertCtrl1.SourceStream = MMSource;


        //            ConvertCtrl1.SourceFile = @"C:\tmp\aa\result.avi"; // CaptureCtrl1.TargetFile;
        //            ConvertCtrl1.TargetFile = @"C:\tmp\aa\result.avi" + ".compressed.mp4"; // CaptureCtrl1.TargetFile + ".compressed";
        //            //ConvertCtrl1.ErrorAbort += ConvertCtrl1_ErrorAbort;
        //            //EventCompleteConvert += ConvertCtrl1_Complete;
        //            //ConvertCtrl1.Progress += this.ConvertCtrl1_Progress;
        //            ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.ImageOverlay);
        //            // get the the image overlay filter object
        //            LMVIOvLy _ImageOverLay = (LMVIOvLy)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor);
        //            _ImageOverLay.OverlayImageFileName = path; // @".\disclaimer_big.bmp";
        //            _ImageOverLay.set_XPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);
        //            _ImageOverLay.set_YPos(LayerTypeConstants.LAYERTYPE_OVERLAYIMAGE, 10);

        //            //Adding Text OverLay
        //            ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.TextOverlay);

        //            // get the the text overlay filter object
        //            LMVTextOverlay _overlay = (LMVTextOverlay)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor + 1);
        //            _overlay.OverlayText = Company;
        //            _overlay.ViewRectLeft = 0;
        //            _overlay.ViewRectTop = 0;
        //            _overlay.ViewRectRight = 800;
        //            _overlay.ViewRectBottom = 600;
        //            _overlay.EnableXYPosition = true;
        //            _overlay.XPos = 80;
        //            _overlay.YPos = 130;
        //            _overlay.FontSize = 12;
        //            _overlay.Bold = true;
        //            _overlay.AutoReposToViewRect = true;

        //            ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.TextOverlay);
        //            LMVTextOverlay _overlay1 = (LMVTextOverlay)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor + 2);
        //            _overlay1.OverlayText = TypeId + " " + ValueId;
        //            _overlay1.ViewRectLeft = 0;
        //            _overlay1.ViewRectTop = 0;
        //            _overlay1.ViewRectRight = 300;
        //            _overlay1.ViewRectBottom = 600;
        //            _overlay.WordWrap = true;
        //            _overlay1.EnableXYPosition = true;
        //            _overlay1.XPos = 80;
        //            _overlay1.YPos = 170;
        //            _overlay1.FontSize = 10;
        //            //_overlay1.FontColor = 16777215;
        //            _overlay1.AutoReposToViewRect = true;

        //            ConvertCtrl1.SelectedVideoProcessors.Add(ConvertCtrl1.VideoProcessors.TextOverlay);
        //            LMVTextOverlay _overlay2 = (LMVTextOverlay)ConvertCtrl1.GetSubObject(ConvertObject.SelVideoProcessor + 3);
        //            _overlay2.OverlayText = DocumentId;
        //            _overlay2.ViewRectLeft = 0;
        //            _overlay2.ViewRectTop = 0;
        //            _overlay2.ViewRectRight = 800;
        //            _overlay2.ViewRectBottom = 600;
        //            _overlay2.EnableXYPosition = true;
        //            _overlay2.XPos = 80;
        //            _overlay2.YPos = 210;
        //            _overlay2.FontSize = 10;
        //            //_overlay1.FontColor = 16777215;
        //            _overlay2.AutoReposToViewRect = true;


        //            ConvertCtrl1.StartConvert();

        //            //while (_converting) {
        //            //}

        //            //if (_videoout == null)
        //            //{
        //            //    LOG.Error("VideoProcessor.Convert - Error Convirtiendo [-2]");
        //            //    ret = -2; //Error convirtiendo
        //            //} else
        //            //{
        //            //    LOG.Debug("VideoProcessor.Convert - Error Convirtiendo [-2]");
        //            //}
        //            //System.Threading.Thread.Sleep(30000);
        //        }
        //        catch (Exception ex)
        //        {
        //            msgerr = ex.Message;
        //            LOG.Error("VideoProcessor.Convert", ex);
        //            ret = -1;
        //        }
        //        return oRet; // new Task<ResultAWM>(() => ResultAWM(ret, msgerr, strvideoout, strfotoout);
        //    });
        //}

        //private void ConvertCtrl1_Progress(object sender, ProgressEventArgs e)
        //{
        //    string s = "Progess";
        //}

        //private void ConvertCtrl1_ErrorAbort(object sender, ErrorAbortEventArgs e)
        //{
        //    string s = "Error";
        //}

        //private void ConvertCtrl1_Complete(object sender, EventArgs e)
        //{
        //    //_videoout = _MMTarget.ToArray();
        //    ConvertCtrl1.StopConvert();
        //    _converting = false;
        //    HayResult = true;
        //}


    }

    public class ResultAWM
    {

        public ResultAWM() { }
        public ResultAWM(int _codigo, string _msgerror, string _videoout, string _fotoout)
        {
            codigo = _codigo;
            msgerror = _msgerror;
            videoout = _videoout;
            fotoout = _fotoout;
        }

        public int codigo;
        public string msgerror;
        public string videoout;
        public string fotoout;
    }
}