﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bio.Digital.Receipt.Core.Database;

namespace Bio.Digital.Receipt.Core.Helpers
{
    public class DatabaseHelper
    {

        static internal IList<RdAplicaciones> GetRdApplications()
        {
            return AdministradorRdAplicaciones.BuscarAplicaciones();
        }

        static internal string GetCompanyName(int companyid)
        {
            string ret;
            try
            {
                ret = AdministradorCompany.BuscarCompanyById(companyid).Name;
                if (String.IsNullOrEmpty(ret)) ret = "Company" + companyid.ToString();
            }
            catch (Exception ex)
            {
                ret = "Company" + companyid.ToString();
            }
            return ret;
        }


    }
}