﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using log4net;

namespace Bio.Digital.Receipt.Core.Helpers
{
    public class CryptoHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(CryptoHelper));

        public static string Encrypt(string texto)
        {
            try
            {
                LOG.Debug("CryptoHelper.Encrypt IN...");
                string key = "biometrika2014"; 
                byte[] keyArray;
                byte[] Arreglo_a_Cifrar = UTF8Encoding.UTF8.GetBytes(texto);

                //Se utilizan las clases de encriptación MD5
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();

                //Algoritmo TripleDES
                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tdes.CreateEncryptor();
                byte[] ArrayResultado = cTransform.TransformFinalBlock(Arreglo_a_Cifrar, 0, Arreglo_a_Cifrar.Length);
                tdes.Clear();

                //se regresa el resultado en forma de una cadena
                texto = Convert.ToBase64String(ArrayResultado, 0, ArrayResultado.Length);
                LOG.Debug("CryptoHelper.Encrypt - Encritado => " + texto + "...");
            }
            catch (Exception ex)
            {
                texto = null;
                LOG.Error("CryptoHelper.Encrypt Error - Error encriptando [" + ex.Message + "]");
            }
            LOG.Debug("CryptoHelper.Encrypt OUT!");
            return texto;
        }

        public static string Decrypt(string textoEncriptado)
        {
            try
            {
                LOG.Debug("CryptoHelper.Decrypt IN...");
                string key = "biometrika2014";
                byte[] keyArray;
                byte[] Array_a_Descifrar = Convert.FromBase64String(textoEncriptado);

                //algoritmo MD5
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tdes.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(Array_a_Descifrar, 0, Array_a_Descifrar.Length);
                tdes.Clear();
                textoEncriptado = UTF8Encoding.UTF8.GetString(resultArray);
                LOG.Debug("CryptoHelper.Decrypt - Desencriptado ok!");
            }
            catch (Exception ex)
            {
                LOG.Error("CryptoHelper.Decrypt Error desencriptando [" + ex.Message + "]");
            }
            LOG.Debug("CryptoHelper.Decrypt IN...");
            return textoEncriptado;
        }
    }
}