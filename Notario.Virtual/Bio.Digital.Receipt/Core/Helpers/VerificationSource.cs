﻿using System;
using System.Collections.Generic;
using log4net;
using BioPortal.Server.Api;


namespace Bio.Digital.Receipt.Core.Helpers
{
    public class VerificationSourceItem
    {
        public VerificationSourceItem() { }

        private int _AuthenticationFactor;
        private int _StatusVerified;  //0- No verificado | 1-Verificado
        private string _TypeVerified; //A-Automatico | M-Manual
        private string _SourceVerified; //Verificador => CI - Cedula de identidad | SRCeI2018 - Registro Civul, etc. 

        public int AuthenticationFactor { get => _AuthenticationFactor; set => _AuthenticationFactor = value; }
        public int StatusVerified { get => _StatusVerified; set => _StatusVerified = value; }
        public string TypeVerified { get => _TypeVerified; set => _TypeVerified = value; }
        public string SourceVerified { get => _SourceVerified; set => _SourceVerified = value; }
    }

    /// <summary>
    /// Clase para serializar el XML de verification source en el campo Verificationsource
    /// en la tabla bp_identity.
    /// El XML queda de la forma:
    /// <?xml version="1.0" encoding="UTF-8"?>
    ///-<VerificationSource xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    ///-<VerificationSourceItems>
    ///	-<VerificationSourceItem>
    ///		<AuthenticationFactor>2</AuthenticationFactor>
    ///		<StatusVerified>1</StatusVerified>
    ///		<TypeVerified>A</TypeVerified>
    ///		<SourceVerified>SRCeI</SourceVerified>
    ///	</VerificationSourceItem>
    ///	-<VerificationSourceItem>
    ///	<AuthenticationFactor>4</AuthenticationFactor>
    ///		<StatusVerified>0</StatusVerified>
    ///		<TypeVerified>A</TypeVerified>
    ///		</VerificationSourceItem>
    ///	</VerificationSourceItems>
    //</VerificationSource>
    /// </summary>
    public class VerificationSource
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(VerificationSource));

#region Properites

        private List<VerificationSourceItem> _VerificationSourceItems;

        public List<VerificationSourceItem> VerificationSourceItems {
            get => _VerificationSourceItems;
            set => _VerificationSourceItems = value;
        }

        public VerificationSource() {
            _VerificationSourceItems = new List<VerificationSourceItem>();
        }


#endregion Properites

#region Static Methods

        //Added 082018 - Para enroll temporales
        //Valores ejemplos
        //   Verificationsource = 2,1,A,SRCeI|4,0,A        => Finger verificado automatico en Registro Civil y facial no verificado
        //                        2,1,A,CI|4,1,M,Usuario1   => Finger verificado automatico contra cedula de identidad y facial manual por Usuario1
        /// <summary>
        /// Verifica status de un AF en un VerificationSource
        /// </summary>
        /// <param name="verificationsource"></param>
        /// <param name="af"></param>
        /// <returns></returns>
        public bool IsIdentityValidated(int af)
        {
            bool ret = false;
            try
            {
                if (_VerificationSourceItems != null || _VerificationSourceItems.Count > 0)
                {
                    foreach (VerificationSourceItem vsi in _VerificationSourceItems)
                    {
                        if (vsi.AuthenticationFactor == af && vsi.StatusVerified == 1)
                        {
                            ret = true;
                            break;
                        }
                    }                   
                }
            }
            catch (Exception ex)
            {
                LOG.Error("VerificationSource.IsIdentityValidated(af)", ex);
                ret = false;
            }
            return ret;
        }

        /// <summary>
        /// Retorna el objeto serializado en XML
        /// </summary>
        /// <returns></returns>
        public string GetXML()
        {
            string ret = null;
            try
            {
                ret = XmlUtils.SerializeObject<VerificationSource>(this);
                ret = ret.Substring(ret.IndexOf("<VerificationSource")); 
            }
            catch (Exception ex)
            {
                LOG.Error("VerificationSource.GetXML", ex);
                ret = "<VerificationSource xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><VerificationSourceItems><VerificationSourceItem><AuthenticationFactor>2</AuthenticationFactor><StatusVerified>0</StatusVerified><TypeVerified>A</TypeVerified></VerificationSourceItem></VerificationSourceItems></VerificationSource>";
            }
            return ret;
        }


        /// <summary>
        /// Setea un AF con sus valores y chequea que si existe hace update, sino
        /// agrega el nuevo elemento a la lista
        /// </summary>
        /// <param name="af"></param>
        /// <param name="status"></param>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public int SetVerificationSource(int af, int status, string type, string name)
        {
            int ret = 0;
            try
            {
                if (IsAFInVerificationSourceItems(af)) {
                    return UpdateVerificationSource(af, status, type, name);
                }
                VerificationSourceItem vsi = new VerificationSourceItem();
                vsi.AuthenticationFactor = af;
                vsi.StatusVerified = status;
                vsi.TypeVerified = type;
                vsi.SourceVerified = name;
                _VerificationSourceItems.Add(vsi);
            }
            catch (Exception ex)
            {
                LOG.Error("VerificationSource.SetVerificationSource(int af, int status, string type, string name)", ex);
                ret = -1;
            }
            return ret;
        }

        /// <summary>
        /// Dado un valor de af con todo slos dmeas datos,hace update si existe. Pero no agrega. 
        /// Si se desea agegar o updatear, hay que usar SetVerificationSource
        /// </summary>
        /// <param name="af"></param>
        /// <param name="status"></param>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public int UpdateVerificationSource(int af, int status, string type, string name)
        {
            int ret = 0;
            try
            {
                if (status == 1)
                {
                    foreach (VerificationSourceItem item in _VerificationSourceItems)
                    {
                        if (item.AuthenticationFactor == af)
                        {
                            //Seteo solo si el status es 1 => verificado, y el name es diferente, para mantener la ultima fuente de verificacion. 
                            //sino no piso para evitar perder una verificacion. 
                            //Si deseo sacar la verificacion, deboi usar ReSet
                            //if (status == 1)
                            //{
                                item.StatusVerified = status;
                                item.TypeVerified = type;
                                item.SourceVerified = name;
                            //}
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("VerificationSource.UpdateVerificationSource(int af, int status, string type, string name)", ex);
                ret = -1;
            }
            return ret; ;
        }

        /// <summary>
        /// Fuerza los valores indicados sin preguntar anterior valor
        /// </summary>
        /// <param name="af"></param>
        /// <param name="status"></param>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public int ReSet(int af, int status, string type, string name)
        {
            int ret = 0;
            bool notSalio = false;
            try
            {
                foreach (VerificationSourceItem item in _VerificationSourceItems)
                {
                    if (item.AuthenticationFactor == af)
                    {
                        item.StatusVerified = status;
                        item.TypeVerified = type;
                        item.SourceVerified = name;
                        break;
                    }
                }
                if (!notSalio)
                {
                    ret = SetVerificationSource(af, status, type, name);
                }
            }
            catch (Exception ex)
            {
                LOG.Error("VerificationSource.ReSet(int af, int status, string type, string name)", ex);
                ret = -1;
            }
            return ret; ;
        }

        /// <summary>
        /// Devuelve si un AF existe en la lista
        /// </summary>
        /// <param name="af"></param>
        /// <returns></returns>
        public bool IsAFInVerificationSourceItems(int af)
        {
            bool ret = false;
            try
            {
                foreach (VerificationSourceItem item in _VerificationSourceItems)
                {
                    if (item.AuthenticationFactor == af) return true;
                }
            }
            catch (Exception ex) 
            {
                LOG.Error("VerificationSource.IsAFInVerificationSourceItems(int af)", ex);
                ret = false;
            }
            return ret;
        }



        /// <summary>
        /// Idem anterior pero entregando el string que está en la BD para que pueda hacerse mas rápido.
        /// </summary>
        /// <param name="xmlvs"></param>
        /// <param name="af"></param>
        /// <returns></returns>
        public static bool IsIdentityValidated(string xmlvs, int af)
        {
            bool ret = false;
            try
            {
                LOG.Debug("VerificationSource.IsIdentityValidated IN...");
                if (!String.IsNullOrEmpty(xmlvs))
                {
                    LOG.Debug("VerificationSource.IsIdentityValidated af = " + af + " xmlvs = " + xmlvs); 
                    VerificationSource vs = XmlUtils.DeserializeObject<VerificationSource>(xmlvs);

                    if (vs != null)
                    {
                        LOG.Debug("VerificationSource.IsIdentityValidated vs deserializado ok...");
                        ret = vs.IsIdentityValidated(af);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("VerificationSource.IsIdentityValidated(string,int)", ex);
                ret = false;
            }
            LOG.Debug("VerificationSource.IsIdentityValidated OUT [ret = " + ret.ToString() + "]");
            return ret;
        }

        #endregion Static Methods
    }

  
}
