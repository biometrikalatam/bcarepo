using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using Bio.Digital.Receipt.Core;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.libs;
using log4net;

namespace Bio.Digital.Receipt
{
    public partial class CheckRDResult : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CheckRDResult));
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string reciboxml = null;
            string tipoverificacion = "0";
            if (!IsPostBack)
            {
                if (Request["Id"] != null)
                {
                    RdRecibos orecibo = AdministradorRdRecibos.BuscarRdReciboById(Convert.ToInt32(Request["Id"]));
                    reciboxml = orecibo.Reciboxml;
                }
                else
                {
                    reciboxml = Request.Form["ctl00$ContentPlaceHolderMain$txtReciboXML"];
                    tipoverificacion = Request.Form["ctl00$ContentPlaceHolderMain$cboTipoCheck"];
                }
                Session["reciboxml"] = reciboxml;
                CheckRecibo(reciboxml, tipoverificacion);
            }
            
        }

        private void CheckRecibo(string reciboxml, string tipoverificacion)
        {
            bool exiteEnDB = false;
            bool fueModificado = false;
            
            try
            {
                //*  1.- Serializo param
                Rd orecibo = XmlUtils.DeserializeObject<Rd>(reciboxml);

                if (orecibo == null)
                {
                    Response.Redirect("Error.aspx?msg='El formato del Recibo Digital presentado es incorrecto. Chequ�elo y reintente...'&action='CheckRD.aspx'" );
                }

                //*  2.- Check en BD 
                RdRecibos dbRecibo = AdministradorRdRecibos.BuscarRdReciboByReciboId(orecibo.reciboID);

                if (dbRecibo != null)
                {
                    exiteEnDB = true;
                }

               // Si se debe chequear por igualdad => Sigo
                if (exiteEnDB && Convert.ToInt32(tipoverificacion) == 0)
                {
                    XmlDocument rdInNV = new XmlDocument();
                    rdInNV.PreserveWhitespace = false;
                    rdInNV.LoadXml(dbRecibo.Reciboxml);

                    XmlDocument rdInParam = new XmlDocument();
                    rdInParam.PreserveWhitespace = false;
                    rdInParam.LoadXml(reciboxml);

                    fueModificado = !(rdInNV.OuterXml.Equals(rdInParam.OuterXml));
                }
                
                //Comienzo a mostar o esconder segun resultado obtenido
                //Lleno valores para mostrar
                foreach (RdExtensionItem ext in orecibo.Extensiones)
                {
                    if (ext.key == "NumeroDocumento")
                    {
                        this.labNrocontrato.Text = ext.value;
                    }
                    if (ext.key == "Cliente")
                    {
                        this.labRazonSocial.Text = ext.value;
                    }
                    if (ext.key == "FechaDocumento")
                    {
                        this.labFecha.Text = ext.value;
                    }
                    if (ext.key == "FechaVencimientoDocumento")
                    {
                        this.labFechaVto.Text = ext.value;
                    }
                    if (ext.key == "Vendedor")
                    {
                        this.labVendedor.Text = ext.value;
                    }
                    if (ext.key == "TipoDocumento")
                    {
                        this.labTipoDocumento.Text = ext.value;
                    }
                }

                //Lleno datos Certificado
                X509Certificate cert = null;
                foreach (object objc in orecibo.Signature[0].KeyInfo.Items)
                {

                    if (objc.GetType().ToString() == "Bio.Digital.Receipt.Core.X509DataType")
                    {
                        Bio.Digital.Receipt.Core.X509DataType ocert = (X509DataType)objc;
                        cert = new X509Certificate((byte[])ocert.Items[0]);
                    }
                }
                 labSubject.Text = cert.Subject;
                labSerialID.Text = cert.GetSerialNumberString();
                //Fechas
                bool errFechas = false;
                labFechaDesde.Text = cert.GetEffectiveDateString();
                DateTime dtDesde = DateTime.Parse(cert.GetEffectiveDateString());
                if (dtDesde > DateTime.Now)
                {
                    labFechaDesde.ForeColor = Color.Red;
                    errFechas = true;
                }
                labFechaHasta.Text = cert.GetExpirationDateString();
                DateTime dtHasta = DateTime.Parse(cert.GetExpirationDateString());
                if (dtHasta < DateTime.Now)
                {
                    labFechaHasta.ForeColor = Color.Red;
                    errFechas = true;
                }
                if (errFechas)
                {
                    imgFechaNOOK.Visible = true;
                    imgFechaOK.Visible = false;
                }
                else
                {
                    imgFechaNOOK.Visible = false;
                    imgFechaOK.Visible = true;
                }
                
                if (exiteEnDB)
                {
                    labResBD.ForeColor = Color.Green;
                    labResBD.Text = "El Recibo Digital presentado EXISTE en Biometrika Notario Virtual";
                    
                    if (!fueModificado)
                    {
                        labResFirma.ForeColor = Color.Green;
                        labResFirma.Text = "El Recibo Digital presentado NO fue modificado";
                        imgVerifyOK.Visible = true;
                        imgVerifyNOOK.Visible = false;
                    } else
                    {
                        labResFirma.ForeColor = Color.Red;
                        labResFirma.Text = "El Recibo Digital presentado FUE modificado";
                        imgVerifyOK.Visible = false;
                        imgVerifyNOOK.Visible = true;
                    }
                } else
                {
                    labResBD.ForeColor = Color.Red;
                    labResBD.Text = "El Recibo Digital presentado NO EXISTE en Biometrika Notario Virtual";
                    labResFirma.Visible = false;
                    imgVerifyOK.Visible = false;
                    imgVerifyNOOK.Visible = true;                    
                } 
                
                
                
            }
            catch (Exception ex)
            {
                log.Error("Error CheckRDResult.CheckRecibo", ex);
                Response.Redirect("Error.aspx?msg='CheckRDResult.CheckRecibo - " + ex.Message + "'");
            }
        }

        protected void picWord_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Rd orecibo = XmlUtils.DeserializeObject<Rd>((string)Session["reciboxml"]);
                //Bajamos archivo
                byte[] myByteArray = null;
                string nombre = null;
                 //Comienzo a mostar o esconder segun resultado obtenido
                //Lleno valores para mostrar
                foreach (RdExtensionItem ext in orecibo.Extensiones)
                {
                    if (ext.key == "Documento")
                    {
                        myByteArray = Convert.FromBase64String(ext.value);
                    }
                    if (ext.key == "NumeroDocumento")
                    {
                        nombre = ext.value;
                    }

                    
                }
                
                if (myByteArray != null && myByteArray.Length > 0)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre + ".doc");
                    Response.AddHeader("Content-Length", myByteArray.Length.ToString());
                    Response.ContentType = "application/msword";
                    //if (doc.Extension == "pdf")
                    //    Response.ContentType = "application/pdf";
                    //else if ((doc.Extension == "docx") || (doc.Extension == "doc"))
                    //    Response.ContentType = "application/msword";
                    //else if ((doc.Extension == "xls") || (doc.Extension == "xlsx"))
                    //    Response.ContentType = "application/msexcel";
                    //else
                    //    Response.ContentType = "application/octet-stream";
                    //Response.ContentType="application/octet-stream";
                    //Reponse.ContentType="application/msword";
                    Response.BinaryWrite(myByteArray);
                    Response.End();
                }
                else
                {
                    log.Debug("download_documento: myByteArray == null");
                }
            }
            catch (Exception ex)
            {
                log.Debug("download_documento:" + ex.Message);
                log.Debug("download_documento Pila:" + ex.StackTrace.ToString());
            }
        }

        protected void picHuellaVendedor_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void picHuellaCliente_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Rd orecibo = XmlUtils.DeserializeObject<Rd>((string)Session["reciboxml"]);
                //Bajamos archivo
                byte[] myByteArray = null;
                string nombre = null;
                //Comienzo a mostar o esconder segun resultado obtenido
                //Lleno valores para mostrar
                foreach (RdExtensionItem ext in orecibo.Extensiones)
                {
                    if (ext.key == "HuellaCliente")
                    {
                        myByteArray = Convert.FromBase64String(ext.value);
                    }
                    if (ext.key == "NumeroDocumento")
                    {
                        nombre = ext.value;
                    }


                }

                if (myByteArray != null && myByteArray.Length > 0)
                {
                    Bio.Wsq.Decoder.WsqDecoder decoder = new Bio.Wsq.Decoder.WsqDecoder();
                    short w = 0;
                    short h = 0;
                    byte[] raw;
                    bool b = decoder.DecodeMemory(myByteArray, out w, out h, out raw);
                    Bitmap bmp = RawImageManipulator.RawToBitmap(raw, w, h);
                    MemoryStream ms = new MemoryStream();
                    bmp.Save(ms, ImageFormat.Jpeg);
                    byte[] bitmapData = ms.ToArray();
                    
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=huella_cliente_" + nombre + ".jpg");
                    Response.AddHeader("Content-Length", bitmapData.Length.ToString());
                    Response.ContentType = "image/jpeg";
                    //if (doc.Extension == "pdf")
                    //    Response.ContentType = "application/pdf";
                    //else if ((doc.Extension == "docx") || (doc.Extension == "doc"))
                    //    Response.ContentType = "application/msword";
                    //else if ((doc.Extension == "xls") || (doc.Extension == "xlsx"))
                    //    Response.ContentType = "application/msexcel";
                    //else
                    //    Response.ContentType = "application/octet-stream";
                    //Response.ContentType="application/octet-stream";
                    //Reponse.ContentType="application/msword";
                    Response.BinaryWrite(bitmapData);
                    Response.End();
                }
                else
                {
                    log.Debug("download_huella_cliente: myByteArray == null");
                }
            }
            catch (Exception ex)
            {
                log.Debug("download_huella_cliente:" + ex.Message);
                log.Debug("download_huella_cliente Pila:" + ex.StackTrace.ToString());
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Response.Redirect("CheckRD.aspx", true);
        }
    }
}
