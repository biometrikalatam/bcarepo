<%@ Page Language="C#" MasterPageFile="~/RDMasterPageRoot.Master" AutoEventWireup="true" CodeBehind="CheckRDResult.aspx.cs" Inherits="Bio.Digital.Receipt.CheckRDResult" Title="Untitled Page" Debug="true" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <table style="width: 80%" align="center">
        <tr>
            <td style="width: 100%" align="right">
                <asp:Button ID="btnBuscar" runat="server" CssClass="boton1140" OnClick="btnBuscar_Click"
                    Text="Volver" /></td>
        </tr>
    </table>
    <br />
    <div >
        <div >
            <table border="0" align="center" style="width: 80%; border-color:#346735; border-style:solid; border-width:1px; vertical-align:middle;" >
                <tr>
                    <td style="width: 25%; text-align: center;" rowspan="7"><asp:Image ID="Image2" runat="server" ImageUrl="~/images/Sello_Huella_NV.JPG" /></td>
                    <td colspan="3" class="title_tabla">
                        Resultado Verificacion de Recibo Digital</td>
                </tr>
                <tr>
                    <td style="width: 150px" class="title_label">
                        Tipo Documento</td>
                    <td class="title_label" colspan="2">
                        N�mero Documento</td>
                </tr>
                <tr>
                    <td style="width: 150px" class="body_label">
                        <asp:Label ID="labTipoDocumento" runat="server" Text="Contrato"></asp:Label></td>
                    <td class="body_label" colspan="2" style="text-align: left">
                        <asp:Label ID="labNrocontrato" runat="server" Text="20091110100110"></asp:Label>
                        &nbsp;<asp:ImageButton ID="picWord" runat="server" ImageUrl="~/images/zoom.png" OnClick="picWord_Click" /></td>
                </tr>
                <tr>
                    <td style="width: 100px" class="title_label">
                        Fecha</td>
                    <td colspan="2" class="title_label">
                        Fecha Vencimiento</td>
                </tr>
                <tr>
                    <td style="width: 100px" class="body_label">
                        <asp:Label ID="labFecha" runat="server" Text="20/10/2009"></asp:Label></td>
                    <td colspan="2">
                        <asp:Label ID="labFechaVto" runat="server" Text="20/10/2010"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 150px; height: 25px;" class="title_label">
                        Raz�n Social Cliente</td>
                    <td colspan="2" style="height: 25px" class="title_label">
                        Vendedor</td>
                </tr>
                <tr>
                    <td style="width: 100px" class="body_label">
                        <asp:Label ID="labRazonSocial" runat="server" Text="Biometrika"></asp:Label>&nbsp;
                        <asp:ImageButton ID="picHuellaCliente" runat="server" ImageUrl="~/images/zoom.png"
                            OnClick="picHuellaCliente_Click" /></td>
                    <td colspan="2">
                        <asp:Label ID="labVendedor" runat="server" Text="NVendedor AVendedor"></asp:Label>&nbsp;
                        <asp:ImageButton ID="picHuellaVendedor" runat="server" ImageUrl="~/images/zoom.png"
                            OnClick="picHuellaVendedor_Click" Visible="False" /></td>
                </tr>
                <tr style="background-color:#346735; height:5px;" >
                    <td colspan="4">
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <table  style="width: 90%; border-color:Black; border-style:solid; border-width:1px" >
                        
                             <tr>
                                <td colspan="4" class="title_label">
                                    Firmado Por</td>
                            </tr>
                            <tr>
                                <td colspan="4" class="body_label">
                                    <asp:Label ID="labSubject" runat="server" Text="CN=Biometrika,OU=BK,O=BioK,L=Santiago De Chile,ST=Region Metropolitana,C=CL,EMAIL=notariovirtual@biometrika.cl"
                                        Width="100%"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 30%" class="title_label">
                                    N�mero Serial Certificado</td>
                                <td style="width: 30%" class="title_label">
                                    Valido Desde</td>
                                <td style="width: 30%" class="title_label">
                                    Valido Hasta</td>
                                <td style="width: 10%; text-align: left;" rowspan="2">
                                    &nbsp;<asp:Image ID="imgFechaOK" runat="server" ImageUrl="~/images/Good-Shield_80x80.jpg" Height="25px" Width="25px" />
                                    <asp:Image ID="imgFechaNOOK" runat="server" ImageUrl="~/images/Error-Shield_80x80.jpg" Height="25px" Width="25px" /></td>
                            </tr>
                            <tr>
                                <td style="width: 30%" class="body_label">
                                    <asp:Label ID="labSerialID" runat="server" Text="60606060606060"></asp:Label></td>
                                <td style="width: 30%" class="body_label">
                                    <asp:Label ID="labFechaDesde" runat="server" Text="01/01/2008"></asp:Label></td>
                                <td style="width: 30%" class="body_label">
                                    <asp:Label ID="labFechaHasta" runat="server" Text="01/01/2010"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px" colspan="4">
                    </td>
                </tr>
                <tr style="background-color:#346735; height:5px;" >
                    <td colspan="4">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; width:30%;" colspan="1" rowspan="2">
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/images/Caja_Fuerte.jpg" Height="105px" Width="122px" /></td>
                    <td style="text-align: center; width: 40%" colspan="1" class="result_label">
                        <asp:Label ID="labResBD" runat="server" Text="El Recibo Digital presentado EXISTE en Biometrika Notario Virtual"></asp:Label></td>
                    <td style="text-align: center;" rowspan="2" colspan="2" align="center">
                        <asp:Image ID="imgVerifyOK" runat="server" ImageAlign="Middle" ImageUrl="~/images/Good-Shield_80x80.jpg" />
                        <asp:Image ID="imgVerifyNOOK" runat="server" ImageAlign="Middle" ImageUrl="~/images/Error-Shield_80x80.jpg" /></td>
                </tr>
                <tr>
                    <td style="text-align: center; width: 40%" colspan="1" class="result_label">
                       <asp:Label ID="labResFirma" runat="server" Text="El Recibo Digital presentado NO fue modificado"></asp:Label></td>
                </tr>                     
            </table>
            <br />
        </div>
    </div>

</asp:Content>
