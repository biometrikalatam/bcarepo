﻿using Bio.Digital.Receipt.Api;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Bio.Digital.Receipt
{
    /// <summary>
    /// Summary description for WSCIProxy
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSCIProxy : System.Web.Services.WebService
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(WSCIProxy));

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlparamin"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        [WebMethod]
        public int Process(string xmlparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;
            string namedebug = "";

            try
            {
                //1.- Deserializo objeto xmlparamin
                LOG.Debug("WSCIProxy.Process Arrived xmlparamin = " + xmlparamin);

                XmlParamIn oXmlIn = Api.XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    LOG.Error("WSCIProxy.Process Error - " + oXmlOut.Message);
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }

                using (WSCIRemote.WSCI ws = new WSCIRemote.WSCI())
                {
                    ws.Url = Properties.Settings.Default.Bio_Digital_Receipt_WSCIRemote_WSCI; //Properties.Settings.Default.ServiceURLBase + "WSCI.asmx";
                    ws.Timeout = Properties.Settings.Default.BPTimeout;
                    LOG.Debug("WSCIProxy.Process - Llama a URL = " + ws.Url + " y retorna resultado recibido!");
                    return ws.Process(xmlparamin, out xmlparamout);
                }
            }
            catch (Exception ex)
            {
                res = Errors.IRET_ERR_DESCONOCIDO;
                oXmlOut.Message = "Error Desconocido - ex = " + ex.Message;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                LOG.Error("WSCIProxy.Process - Exc = ", ex);
            }
            //LOG.Debug("FEM.Process Returned res = " + res.ToString() + " - xmlparamout = " + oXmlOut);
            //if (Properties.Settings.Default.DebugVideoImagen > 0)
            //{
            //    FEMServiceManager.SaveDebugData("XmlParamOut_" + namedebug, xmlparamout, 3);
            //}
            LOG.Debug("WSCIProxy.Process - out por error = " + res); 
            return res;
        }



        /// <summary>
        /// Cehquea status de CI, para control desde servicios de terceros
        /// </summary>
        /// <param name="xmlparamin"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        [WebMethod]
        public int CheckStatusCI(string trackid)
        {

            int res = -1;
            string msgerr = null;
            string namedebug = "";

            try
            {
                //1.- Deserializo objeto xmlparamin
                LOG.Debug("WSCI.CheckStatusCI Arrived trackid = " + trackid);

                //3.- Llamo a ServicesManager.Process con parametros deserializados.
                //res = CIServiceManager.CheckStatusCI(trackid);
                //LOG.Debug("WSCI.Process ret = " + res);
                using (WSCIRemote.WSCI ws = new WSCIRemote.WSCI())
                {
                    ws.Url = Properties.Settings.Default.Bio_Digital_Receipt_WSCIRemote_WSCI; //Properties.Settings.Default.ServiceURLBase + "WSCI.asmx";
                    ws.Timeout = Properties.Settings.Default.BPTimeout;
                    LOG.Debug("WSCIProxy.CheckStatusCI - Llama a URL = " + ws.Url + " y retorna resultado recibido!");
                    return ws.CheckStatusCI(trackid);
                }
            }
            catch (Exception ex)
            {
                res = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("WSCIProxy.CheckStatusCI", ex);
            }
            LOG.Debug("WSCIProxy.CheckStatusCI -  Sale por error = " + res);
            return res;
        }
    }
}
