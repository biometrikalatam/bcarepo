namespace Bio.Wsq
{
	/// <summary>
	/// Descripción breve de WaveletTreeNode.
	/// </summary>
	internal struct WaveletTreeNode
	{
		public int X;
		public int Y;
		public int LenX;
		public int LenY;
		public int OffSet;
		
	}
}
