namespace Bio.Wsq
{
	/// <summary>
	/// Descripción breve de QuantumTreeNode.
	/// </summary>
    internal struct QuantumTreeNode
	{
		public int X;
		public int Y;
		public int LenX;
		public int LenY;
	}
}
