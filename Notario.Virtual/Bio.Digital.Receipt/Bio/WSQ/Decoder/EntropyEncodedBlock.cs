namespace Bio.Wsq.Decoder
{
	/// <summary>
	/// Descripción breve de EntryCodeSegment.
	/// </summary>
	internal sealed class EntropyEncodedBlock :  Table
	{
		private byte tableSelector;

		private Frame blockFrame;
		private HuffmanTables huffmanTables;

		public EntropyEncodedBlock (Frame frame) : base (null)
		{
			if (frame == null)
				throw new DecoderException("Se creo un EntropyEncodedBlock sin frame");
			this.blockFrame = frame;
		}

		public byte this [int index]
		{
			get { return GetByte (index); }
		}

		public byte TableSelector
		{
			get { return tableSelector; }

		}

		public HuffmanTable GetHuffmanTable ()
		{
			if (huffmanTables != null)
				return huffmanTables.GetTable (TableSelector);
			HuffmanTable htbl = BlockFrame.GetHuffmanTable (TableSelector);
			return htbl;
		}

		public Frame BlockFrame
		{
			set { blockFrame = value; }
			get { return blockFrame; }
		}



		public void SetTableSelector (byte selector)//byte[] data)
		{
			tableSelector = selector;//data[p];
		}




		public void SetHuffmanTables (HuffmanTables tables)
		{
			huffmanTables = tables;
		}
	}
}