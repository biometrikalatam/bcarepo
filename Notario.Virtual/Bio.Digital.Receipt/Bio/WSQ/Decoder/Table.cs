using System;

namespace Bio.Wsq.Decoder
{
	/// <summary>
	/// Descripción breve de WsqAgregate.
	/// </summary>
    internal class Table
	{

		internal Table (byte[] data)
		{
			SetData(data);
		}

		internal Table (byte[] data, int start, int length)
		{
			SetData(data, start, length);
		}

		protected byte GetByte (int pos)
		{
			return data [start+pos];
		}

		protected short GetShort (int pos)
		{
			return (short) ((data [start+pos] << 8) | (data [start+pos+1]));
		}

		
		protected static uint GetLong (byte[] data, ref int pos)
		{
			int d0 = data[pos], d1 = data[pos+1];
            ulong result = (uint) (((d0 << 8) | (d1)) << 16);
			d0 = data[pos+2]; d1 = data[pos+3];
			result |= (uint) ((d0 << 8) | (d1));
			pos += 4;
			return (uint) result;
		}

		protected float GetFloat16 (int pos)
		{
			pos += start;
			return GetFloat16(data, ref pos);
		}

		protected static float GetFloat16 (byte[] data, ref int pos)
		{
			int  exp = data [pos++];
			float value = (float) ((data [pos] << 8) | (data [pos+1]));
			pos += 2;
			value /= (float)Math.Pow(10, exp);
			return value;
		}

		protected static float GetSignedFloat32 (byte[] data, ref int pos)
		{
			byte sign = data [pos++];
			byte exp = data [pos++];
			double value = GetLong (data, ref pos);
			value /= Math.Pow(10, exp);
			if (sign != 0)
				value = -value;
			return (float) value;
		}

		internal bool GetByte (int pos, out byte code)
		{
			code = 0;
			if (pos >= datalen)
				return false;

			code = data [start+pos];
			return true;
		}

		internal void SetData(byte[] data)
		{
            this.data = data;
			this.start = 0;
			if (data == null)
				datalen = 0;
			else
				this.datalen = data.Length;
		}

		internal void SetData(byte[] data, int start, int length)
		{
			this.data = data;
			this.start = start;
			if (data == null)
				datalen = 0;
			else
				this.datalen = length;
		}

		private byte[] data;
		private int datalen;
		private int start;

		


		

	}
}