using System;
using System.Web;
using System.Web.Services;
using System.ComponentModel;
using System.Xml;
using Bio.Digital.Receipt.Core;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.libs;
using Bio.Digital.Receipt.pki;
using log4net;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Services;

namespace Bio.Digital.Receipt
{
    /// <summary>
    /// Summary description for WSBDR
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class WSBDR : System.Web.Services.WebService
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(WSBDR));
        //public RdExtensionItem extensionitem;



        [WebMethod]
        public int Process(string xmlparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;

            try
            {
                ////0.- Chequeo licencia valida
                //if (!Global.LicenseValid(Global.SERVICE_WS))
                //{
                //    oXmlOut.Message = Errors.SERR_LICENSE;
                //    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IERR_LICENSE;
                //}

                //1.- Deserializo objeto xmlparamin
                XmlParamIn oXmlIn = Api.XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }

                //2.- Chequeo que est� toda la informaci�n necesaria sino retorno con ERR
                if (!ServiceManager.IsCorrectParamInForAction(oXmlIn, out msgerr))
                {
                    oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                        msgerr + "]";
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }

                //3.- Llamo a ServicesManager.Process con parametros deserializados.
                res = ServiceManager.Process(oXmlIn, out xmlparamout);

            }
            catch (Exception ex)
            {
                res = Errors.IRET_ERR_DESCONOCIDO;
                oXmlOut.Message = "Error Desconocido - ex = " + ex.Message;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                LOG.Error("BioPortalServerWS.Verify", ex);
            }

            return res;
        }








        //#region Constantes

        //        int RET_OK = 0;

        //        int RET_ERR_DESCONOCIDO = -1;
        //        int RET_ERR_PARAM_INCORRECTOS = -2;
        //        int RET_ERR_SCHEMA_RD_INVALID = -3;
        //        int RET_ERR_SAVE_DB = -4;
        //        int RET_ERR_RECEIPT_NO_IN_DB = -5;
        //        int RET_ERR_COMPANY_NOT_EXIST = -6;
        //        int RET_ERR_COMPANY_DISABLED = -7;
        //        int RET_ERR_APLICATION_NOT_EXIST = -8;


        //        int RET_ERR_SIGNATURE = -20;

        //        int RET_ERR_TSA = -40;

        //        #endregion Constantes
    }

    public class RDRetorno
    {
        public int codigo;
        public string descripcion;
        public string xmlret;
    }
    
    
}
