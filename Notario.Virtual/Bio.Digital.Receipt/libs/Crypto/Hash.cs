using System.Text;
using Crypto_1 = System.Security.Cryptography;

namespace Bio.Digital.Receipt.libs.Crypto
{
    /// <summary>
    /// Descripción breve de Hash.
    /// </summary>
    public sealed class Hash
    {
        private Hash()
        {
        }

        public static string PlainUtf8ToMD5(string plain)
        {


            Crypto_1.MD5 md5 = new Crypto_1.MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(Encoding.UTF8.GetBytes(plain));
            return Encoding.UTF8.GetString(Encoder64.Encode(result, 0, result.Length));
        }

        public static string PlainAsciiToMD5(string plain)
        {
            Crypto_1.MD5 md5 = new Crypto_1.MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(Encoding.ASCII.GetBytes(plain));
            return Encoding.ASCII.GetString(Encoder64.Encode(result, 0, result.Length));
        }
    }
}