﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Bio.Digital.Receipt.libs.Crypto
{
    public static class AES
    {
        public const String SUPERCLAVE = "B1ometrika2017#$";

        public static string Decrypt(byte[] message)
        {
            string plaintext = null;
            try
            {
                UTF8Encoding UTF8 = new UTF8Encoding();
                AesManaged tdes = new AesManaged();
                tdes.Key = UTF8.GetBytes(SUPERCLAVE);
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;
                ICryptoTransform decryptor = tdes.CreateDecryptor();
                using (MemoryStream msDecrypt = new MemoryStream(message))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                plaintext = null;
            }
            return plaintext;
        }

        public static byte[] Encrypt(string message)
        {
            UTF8Encoding UTF8 = new UTF8Encoding();
            AesManaged tdes = new AesManaged();
            tdes.Key = UTF8.GetBytes(SUPERCLAVE);
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform encryptor = tdes.CreateEncryptor();
            byte[] result;

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        //Write all data to the stream.
                        swEncrypt.Write(message);
                    }
                    result = msEncrypt.ToArray();
                }
            }

            return result;
        }
    }
}