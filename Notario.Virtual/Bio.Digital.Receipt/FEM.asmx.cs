﻿using System;
using System.Web.Services;
using log4net;
using Bio.Digital.Receipt.Core.Services;
using Bio.Digital.Receipt.Api;

namespace Bio.Digital.Receipt
{
    /// <summary>
    /// Summary description for FEM
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class FEM : System.Web.Services.WebService
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(FEM));
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlparamin"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        [WebMethod]
        public int Process(string xmlparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;
            string namedebug = "";

            try
            {
                ////0.- Chequeo licencia valida
                //if (!Global.LicenseValid(Global.SERVICE_WS))
                //{
                //    oXmlOut.Message = Errors.SERR_LICENSE;
                //    xmlparamout = XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                //    return Errors.IERR_LICENSE;
                //}

                //1.- Deserializo objeto xmlparamin
                //LOG.Debug("FEM.Process Arrived xmlparamin = " + xmlparamin);
                
                XmlParamIn oXmlIn = Api.XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                if (Properties.Settings.Default.DebugVideoImagen > 0)
                {
                    namedebug = oXmlIn.Companyid + "_" + oXmlIn.Actionid;
                    FEMServiceManager.SaveDebugData("XmlParamIn_" + namedebug, xmlparamin, 3);
                }
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }

                //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                if (!FEMServiceManager.IsCorrectParamInForAction(oXmlIn, out msgerr))
                {
                    oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                        msgerr + "]";
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }

                //3.- Llamo a ServicesManager.Process con parametros deserializados.
                res = FEMServiceManager.Process(oXmlIn, out xmlparamout);

            }
            catch (Exception ex)
            {
                res = Errors.IRET_ERR_DESCONOCIDO;
                oXmlOut.Message = "Error Desconocido - ex = " + ex.Message;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                LOG.Error("FEMServiceManager.Process", ex);
            }
            //LOG.Debug("FEM.Process Returned res = " + res.ToString() + " - xmlparamout = " + oXmlOut);
            if (Properties.Settings.Default.DebugVideoImagen > 0)
            {
                FEMServiceManager.SaveDebugData("XmlParamOut_" + namedebug, xmlparamout, 3);
            }
            return res;
        }

    }
}
