﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="qrci.aspx.cs" Inherits="Bio.Digital.Receipt.example.qrci" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="StyleSheetNV.css" rel="stylesheet" />
   
    <title></title>
        <style type="text/css">

       .auto-style7 {
            position: absolute;
            top: 415px;
            left:1194px;
            right: 218px;
            width: 292px;
            text-align:left;
            border:0px solid red;
            height: 22px;
            font-family:Arial;
            font-size:large;
            font-weight:bold;
            color:darkgray;
            text-align:center;
        }

        .auto-style8 {
            position: absolute;
            top: 476px;
            left:1241px;
            right: 199px;
            width: 224px;
            text-align:left;
            border:0px solid red;
            height: 208px;
            margin-top: 6px;
        }

       .auto-style9 {
            position: absolute;
            top: 812px;
            left:1264px;
            right: 103px;
            width: 154px;
            text-align:left;
            border:0px solid red;
            height: 42px;
        }

       .auto-style10 {
            position: absolute;
            top: 437px;
            left:1197px;
            right: 215px;
            width: 292px;
            text-align:left;
            border:0px solid red;
            height: 30px;
            font-family:Arial;
            font-size:x-large;
            font-weight:bold;
            color:darkgray;
            text-align:center;
        }

       .auto-style11 {
            position: absolute;
            top: 700px;
            left:1149px;
            right: 158px;
            width: 397px;
            text-align:left;
            border:0px solid red;
            height: 16px;
            font-family:'Courier New';
            font-size:x-small;
            font-weight:bold;
            color:darkgray;
            text-align:center;
        }
        .auto-style12 {
            position: absolute;
            top: 0px;
            width: 1911px;
            left: 0px;
            height: 965px;
        }
    </style>
</head>
<body onload="ActivarReloj();">


 <form id="form2" runat="server" style="margin-left: 40px">
     <div style="position:absolute; top:0; left:0;">
		<img src="images/FondoCIPOC_II.png" id="fondo" style="background-repeat:no-repeat; " class="auto-style12" />
	</div>
    <div class="auto-style7">
        <asp:Label ID="Label1" runat="server" Text="Verificando"></asp:Label>
     </div>
     <div class="auto-style10">
         <asp:Label ID="labRUT" runat="server" Text="21284415-2"></asp:Label>
     </div>
     <div class="auto-style8">
          <asp:image runat="server" id="imgQR" ImagenUrl="#" width="210" height="210" ImageAlign="AbsMiddle" BorderStyle="Solid" BorderColor="Gray" BorderWidth="1"  />       
     </div>
     <div class="auto-style11">
         <asp:Label ID="labTrackId" runat="server" Text="8556a9957e704b0ca599a7752ea8a08b"></asp:Label>
     </div>
     <div class="auto-style9">
         <asp:ImageButton ID="ImageButton2" runat="server" Height="43px" ImageUrl="~/poc/images/transparente.png" OnClick="ImageButton1_Click" Width="150px" />
     </div>
 
            <asp:TextBox Visible="true" ID="txtName" runat="server" Text="" />
            <asp:Button ID="btnSubmit" Text="Submit" runat="server" Visible="False" />
    </form>



    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[id*=btnSubmit]").click(function (callback) {
                var name = '<%=labTrackId.Text%>'; // $.trim($("[id*=txtName]").val());
                $.ajax({
                    type: "POST",
                    url: "http://localhost:8888/WSCI.asmx/CheckStatusCI",
                    data: "{ trackid: '" + name + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                         //alert(r.d);
                         if (r.d == 1 || r.d == -413) {
                            SigueProceso(name, r.d);
                         } else {
                             //alert('Sigue esperando');
                             ActivarCheck();
                         }
                    },
                    error: function (r) {
                        alert(r.responseText);
                    },
                    failure: function (r) {
                        alert(r.responseText);
                    }
                });
                return false;
            });
        });

        function ActivarCheck() {

            CheckStatus();
 
            //Actualizando check cada 5 segundos
            ahoraSonLas = setTimeout(ActivarCheck,5000); 
        } 
  
        function DetenerReloj(){
            // Deteniendo el setTimeout
            clearTimeout(ahoraSonLas);
            // Volviendo el boton 'Activar Check' a la normalidad
            document.getElementById('botonActivar').disabled=false;
        }

        function CheckStatus() {
            //alert('p1')
            var name = '<%=labTrackId.Text%>'; 
            $.ajax({
                type: "POST",
                url: "http://localhost:8888/WSCI.asmx/CheckStatusCI",
                data: "{ trackid: '" + name + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    //alert(r.d);
                    if (r.d == 1 || r.d == -413) {
                        if (r.d = -413) {
                            clearTimeout(ahoraSonLas);
                            alert('El TrackId = ' + name + ' se encuentra vencida y sin completar. Reinicie el proceso...');
                        }
                        SigueProceso(name, 1); //r.d);
                    } else {
                        //alert('Sigue esperando');
                    }
                },
                error: function (r) {
                    alert(r.responseText);
                },
                failure: function (r) {
                    alert(r.responseText);
                }
            });
        }

        function SigueProceso(trackid, status) {
             document.location.href = "CIResult.aspx?trackid=" + trackid + "&status=" + status;
        }
    </script>
    
</body>
</html>
