﻿using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.Core.Services;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Bio.Digital.Receipt.example
{
    public partial class CIResult : System.Web.UI.Page
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(CIResult));

        protected void Page_Load(object sender, EventArgs e)
        {
            string urlgooglemaps;
            if (!IsPostBack)
            {
                try
                {
                    LOG.Debug("POC CIResult.Page_Load IN...");
                    string trackid = Request.QueryString["trackid"];
                    LOG.Debug("POC CIResult.Page_Load - Ciew TrackId = " + trackid);
                    string status = Request.QueryString["status"];
                    if (string.IsNullOrEmpty(status)) status = "1";
                    //labTrackId.Text = trackid;
                    if (!string.IsNullOrEmpty(status) & status.Equals("1"))
                    {

                        string xmlparamin, xmlparamout;
                        Bio.Digital.Receipt.Api.XmlParamIn oParamin = new Bio.Digital.Receipt.Api.XmlParamIn();
                        oParamin.Actionid = 6;
                        oParamin.Origin = 1;
                        oParamin.Companyid = 1;
                        oParamin.ApplicationId = 7;
                        oParamin.CIParam = new Bio.Digital.Receipt.Api.CIParam();
                        oParamin.CIParam.TrackId = trackid;
                        //oParamin.CIParam.TypeId = "RUT";
                        //oParamin.CIParam.ValueId = rut;
                        //oParamin.CIParam.DestinaryMail = mail;
                        xmlparamin = Bio.Digital.Receipt.Api.XmlUtils.SerializeAnObject(oParamin);

                        LOG.Debug("POC CIResult.Page_Load - GetTx via CIServiceManager.Process...");
                        int ret = CIServiceManager.Process(oParamin, out xmlparamout);
                        if (ret == 1)
                        {
                            LOG.Debug("POC CIResult.Page_Load - ret == 1 => Parsea respuesta...");
                            if (!string.IsNullOrEmpty(xmlparamout))
                            {
                                Bio.Digital.Receipt.Api.XmlParamOut oParamout = Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                                if (!string.IsNullOrEmpty(oParamout.Receipt))
                                {
                                    LOG.Debug("POC CIResult.Page_Load - oParamout != null => Parseo OK...");
                                    System.Collections.Hashtable ht = ParseReceipt(oParamout.Receipt, oParamout.CIParam.ValueId);

                                    labTypeIdValueId.Text = (string)ht["TypeId"] + " " + (string)ht["ValueId"];
                                    LabNombre.Text = (string)ht["Name"]; // + " " + (string)ht["PatherLastName"] + " " + (string)ht["MotherLastName"];
                                    labFechaNac.Text = (string)ht["BirthDate"];
                                    labNacionality.Text = (string)ht["Nacionality"];
                                    labScore.Text = (string)ht["Score"];
                                    if (CedulaVencida((string)ht["ExpirationDate"])) labExpirationDay.ForeColor = System.Drawing.Color.Red;
                                    labExpirationDay.Text = (string)ht["ExpirationDate"];

                                    LOG.Debug("POC CIResult.Page_Load - Grabara temporal de CertificatePDF en " +
                                                Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + (string)ht["ValueId"] + "_pdf.pdf ...");
                                    System.IO.File.WriteAllBytes(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + (string)ht["ValueId"] + "_map.png",
                                                                 Convert.FromBase64String(oParamout.CIParam.Map));
                                    LOG.Debug("POC CIResult.Page_Load - Grabara temporal de Map en " +
                                                Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + (string)ht["ValueId"] + "_map.png ...");
                                    System.IO.File.WriteAllBytes(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + (string)ht["ValueId"] + "_pdf.pdf",
                                                                 Convert.FromBase64String(oParamout.CIParam.CertificatePDF));

                                    linkPdf.NavigateUrl = Properties.Settings.Default.URLTempQR + (string)ht["ValueId"] + "_pdf.pdf";
                                    imgCedula.ImageUrl = Properties.Settings.Default.URLTempQR + (string)ht["ValueId"] + "_cedula.jpg";
                                    imgSelfie.ImageUrl = Properties.Settings.Default.URLTempQR + (string)ht["ValueId"] + "_selfie.jpg";
                                    imgMap.ImageUrl = Properties.Settings.Default.URLTempQR + (string)ht["ValueId"] + "_map.png";
                                    LOG.Debug("POC CIResult.Page_Load - URLs =>");
                                    LOG.Debug("POC CIResult.Page_Load - linkPdf.NavigateUrl = " + linkPdf.NavigateUrl);
                                    LOG.Debug("POC CIResult.Page_Load - imgCedula.ImageUrl = " + imgCedula.ImageUrl);
                                    LOG.Debug("POC CIResult.Page_Load - imgSelfie.ImageUrl = " + imgSelfie.ImageUrl);
                                    LOG.Debug("POC CIResult.Page_Load - linkPdf.NavigateUrl = " + linkPdf.NavigateUrl);
                                    //System.IO.File.WriteAllBytes(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + (string)ht["ValueId"] + "_map.png",
                                    //                             GetMapFromGeoRef("https://maps.googleapis.com/maps/api/staticmap?center=" +
                                    //           oParamout.CIParam.GeoRef +
                                    //           "&amp;zoom=16&amp;size=400x300&amp;markers=anchor:24,24%7Cicon:https://tinyurl.com/yyrgzv26%7C" +
                                    //           oParamout.CIParam.GeoRef +
                                    //           "&amp;key=AIzaSyCsNCxJprwolv_rncjZ8RJX3LHzkkOsqoA"));
                                    //laburlgooglemap.Text = "https://maps.googleapis.com/maps/api/staticmap?center=" +
                                    //           oParamout.CIParam.GeoRef +
                                    //           "&amp;zoom=16&amp;size=400x300&amp;markers=anchor:24,24%7Cicon:https://tinyurl.com/yyrgzv26%7C" +
                                    //           oParamout.CIParam.GeoRef +
                                    //           "&amp;key=AIzaSyCsNCxJprwolv_rncjZ8RJX3LHzkkOsqoA";
                                    //imgMap.ImageUrl = laburlgooglemap.Text;
                                } else
                                {
                                    LOG.Debug("POC CIResult.Page_Load - Parseo oParamout == null => Redirect a index...");
                                    Response.Redirect("index.aspx");
                                }
                            } else
                            {
                                LOG.Debug("POC CIResult.Page_Load - Parseo xmlparamout == null => Redirect a index...");
                                Response.Redirect("index.aspx");
                            }
                        }
                        else
                        {
                            LOG.Debug("POC CIResult.Page_Load - ret == " + ret.ToString() + " => Redirect a index...");
                            Response.Redirect("index.aspx");
                        }
                    }
                    else
                    {
                        LOG.Debug("POC CIResult.Page_Load - satatus null => Redirect a index...");
                        Response.Redirect("index.aspx");
                        //Response.Write("Trackid = " + trackid + " - status = " + status);
                    }
                }
                catch (Exception ex)
                {
                    LOG.Error("POC CIResult.Page_Load - Excepcion == " + ex.Message + " => Redirect a index...");
                    string s = ex.Message;
                    Response.Redirect("index.aspx");
                }

            }
        }

        private bool CedulaVencida(string sDate)
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("es-ES");
            bool ret = false;
            DateTime myDate = new DateTime(1900, 1, 1);
            int mes = 0;
            bool hayerr = false;
            DateTime dt = new DateTime();
            try
            {
                LOG.Debug("CIResult.aspx.CedulaVencida IN... " + sDate);
                try
                {
                    dt = DateTime.Parse(sDate, ci);
                    ret = dt < DateTime.Now;
                }
                catch (Exception ex)
                {
                    LOG.Error("CIResult.aspx.CedulaVencida Parse Error = " + ex.Message);
                    hayerr = true;
                }

                if (hayerr)
                {
                    hayerr = false;
                    try
                    {
                        ci = new System.Globalization.CultureInfo("en-US");
                        dt = DateTime.Parse(sDate, ci);
                        ret = dt < DateTime.Now;
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("CIResult.aspx.CedulaVencida Parse Error = " + ex.Message);
                        hayerr = true;
                    }
                }

                if (hayerr)
                {
                    hayerr = false;
                    string d = sDate.Substring(0, 2);
                    string m = sDate.Substring(3, 2);
                    string y = sDate.Substring(6);

                    try
                    {
                        dt = new DateTime(Convert.ToInt32(d), Convert.ToInt32(m), Convert.ToInt32(y));
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("CIResult.aspx.CedulaVencida Parse Error = " + ex.Message);
                        hayerr = false;
                    }

                    try
                    {
                        dt = new DateTime(Convert.ToInt32(m), Convert.ToInt32(d), Convert.ToInt32(y));
                    }
                    catch (Exception ex)
                    {
                        LOG.Error("CIResult.aspx.CedulaVencida Parse Error = " + ex.Message);
                        hayerr = false;
                    }



                    ret = dt < DateTime.Now;
                }

            }
            catch (Exception ex)
            {
                LOG.Error("CIResult.aspx.CedulaVencida Error = " + ex.Message);
                ret = false;
            }
            LOG.Debug("CIResult.aspx.CedulaVencida OUT! Vencida = " + ret.ToString());
            return ret;
        }

        private System.Collections.Hashtable ParseReceipt(string xmlrd, string valueid)
        {
            System.Collections.Hashtable ht = null;
            try
            {
                LOG.Debug("CIResult.aspx.ParseReceipt IN...");
                LOG.Debug("CIResult.aspx.ParseReceipt - xmlrd != null => " + (string.IsNullOrEmpty(xmlrd)).ToString());
                LOG.Debug("CIResult.aspx.ParseReceipt - valueid = " + valueid);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = false;
                LOG.Debug("CIResult.aspx.ParseReceipt - LoadXml...");
                xmlDoc.LoadXml(xmlrd);
                LOG.Debug("CIResult.aspx.ParseReceipt - xmlDoc.GetElementsByTagName('ExtensionItem')...");
                XmlNodeList n = xmlDoc.GetElementsByTagName("ExtensionItem");
                LOG.Debug("CIResult.aspx.ParseReceipt - XmlNodeList != null => " + (n != null).ToString());
                ht = new System.Collections.Hashtable();
                string s;
                LOG.Debug("CIResult.aspx.ParseReceipt - For llenando ht...");
                foreach (XmlNode item in n)
                {
                    if (item.ChildNodes.Count > 1 && !string.IsNullOrEmpty(item.ChildNodes[1].InnerText))
                    {
                        if (!ht.ContainsKey(item.ChildNodes[0].InnerText))
                        {
                            ht.Add(item.ChildNodes[0].InnerText, item.ChildNodes[1].InnerText);
                        }
                    }
                    //int i = 0;
                }
                LOG.Debug("CIResult.aspx.ParseReceipt - ht.length = " + ht.Count.ToString());

                if (ht != null && ht.ContainsKey("IDCardImage") && !string.IsNullOrEmpty((string)ht["IDCardImage"]))
                {
                    LOG.Debug("POC CIResult.Page_Load - Grabara temporal de CedulaFront en " +
                                                    Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + (string)ht["ValueId"] + "_cedula.jpg ...");
                    System.IO.File.WriteAllBytes(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + (string)ht["ValueId"] + "_cedula.jpg",
                                                 Convert.FromBase64String((string)ht["IDCardImage"]));
                }
                if (ht != null && ht.ContainsKey("Selfie") && !string.IsNullOrEmpty((string)ht["Selfie"]))
                {
                    LOG.Debug("POC CIResult.Page_Load - Grabara temporal de Selfie en " +
                                Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + (string)ht["ValueId"] + "_selfie.jpg ...");
                    System.IO.File.WriteAllBytes(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + (string)ht["ValueId"] + "_selfie.jpg",
                                             Convert.FromBase64String((string)ht["Selfie"]));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("POC CIResult.Page_Load Excp = " + ex.Message);
                ht = null;
            }
            return ht;
        }

        protected void imgPdf_Click(object sender, ImageClickEventArgs e)
        {
            //TODO Descarga PDF
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }
    }
}