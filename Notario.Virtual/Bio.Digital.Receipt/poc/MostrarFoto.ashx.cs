using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Drawing;
using System.IO;
using Bio.Core.Imaging;
using System.Drawing.Imaging;


namespace Bio.Digital.Receipt.example
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class MostrarFoto : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string type = context.Request["type"];
            string rut = context.Request["rut"];
            if (rut == null)
                return;
            rut = rut.Trim();
            string imageB64 = (string)Global.QR[rut];
            
            

            byte[] bytesFoto = String.IsNullOrEmpty(imageB64) ? null : Convert.FromBase64String(imageB64);
            if (bytesFoto == null)
            {
                context.Response.Redirect(type.Equals("firma")
                                              ? "../../Images/PScertificado_nofirma.jpg"
                                              : "../../Images/PScertificado_nofoto.jpg");
            }
            else
            {
                try
                {
                    Bio.Core.Imaging.Imagen f = new Imagen(bytesFoto);
                    f.Ajustar(300, 300);
                    //using (Bitmap b = new Bitmap(new MemoryStream(bytesFoto)))
                    using (Bitmap b = new Bitmap(new MemoryStream(f.Data)))
                    {

                        b.Save(context.Response.OutputStream, ImageFormat.Jpeg);
                        context.Response.OutputStream.Flush();
                    }
                }
                catch (Exception e)
                {
                    context.Response.Redirect(type.Equals("firma")
                                              ? "../../Images/PScertificado_nofirma.jpg"
                                              : "../../Images/PScertificado_nofoto.jpg");
                }


            }

            Global.QR.Remove(rut);
            //using (Bitmap b = new Bitmap(new MemoryStream(bytesFoto)))
            //{
            //    context.Response.ContentType = "image/jpeg";
            //    b.Save(context.Response.OutputStream, ImageFormat.Jpeg);
            //    context.Response.OutputStream.Flush();
            //};
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

       
    }
}
