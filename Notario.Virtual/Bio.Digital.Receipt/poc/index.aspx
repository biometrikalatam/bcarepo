﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Bio.Digital.Receipt.example.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Notario Virtual - Certificacion de Identidad v2</title>
    <link href="StyleSheetNV.css" rel="stylesheet" />



   

    <style type="text/css">

        .auto-style7 {
            position: absolute;
            top: 350px;
            left:1070px;
            right: 186px;
            width: 255px;
            text-align:left;
        }

        .auto-style8 {
            position: absolute;
            top: 350px;
            left:1485px;
            right: 12px;
            width: 266px;
            text-align:left;
        }

        .auto-style9 {
            position: absolute;
            top: 90px;
            left:-613px;
            right: 583px;
            width: 296px;
            text-align:left;
            border:0px solid red;
            height: 360px;
        }

         .auto-style10 {
            position: absolute;
            top: 92px;
            left:-281px;
            right: 249px;
            width: 298px;
            text-align:left;
            border:0px solid red;
            height: 357px;
        }

        .auto-style11 {
            position: absolute;
            top: 436px;
            left:-1248px;
            right: 1140px;
            width: 374px;
            text-align:left;
            border:0px solid red;
            height: 40px;
        }
        .auto-style12 {
            position: absolute;
            top: -3px;
            width: 1911px;
            left: 3px;
            height: 965px;
        }
        .auto-style13 {
             position: absolute;
            top: 94px;
            left:55px;
            right: -66px;
            width: 275px;
            text-align:left;
            border:0px solid red;
            height: 352px;
        }

    </style>


   

</head>
<body>
    <div style="position:absolute; top:0; left:0;">
		<img src="images/FondoCIPOC_New_I.png" id="fondo" style="background-repeat:no-repeat; " class="auto-style12" />
	</div>
    
    <form id="form1" runat="server" >
        <div style="width:100%;height:100%;">
                <div class="auto-style7">
                     <asp:TextBox ID="txtRUT" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large" ForeColor="Black" Width="240px" BorderColor="White" BorderStyle="None"></asp:TextBox>
                </div>
                <div class="auto-style8">
                    <asp:TextBox ID="txtMail" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Large" ForeColor="Black" Width="240px" BorderColor="White" BorderStyle="None"></asp:TextBox>
               
                <div class="auto-style9">
                    <asp:ImageButton ID="imgBtnQR" runat="server" Height="357px" Width="294px" ImageUrl="~/poc/images/transparente.png" ImageAlign="Top" OnClick="imgBtnQR_Click" />
                </div>

                <div class="auto-style10">
                    <asp:ImageButton ID="imgBtnWindows" runat="server" Height="352px" Width="295px" ImageUrl="~/poc/images/transparente.png" ImageAlign="Top" OnClientClick="CallCI();" />
                </div>

                <div class="auto-style13">
                    <asp:ImageButton ID="imbBtnCIWeb" runat="server" Height="345px" Width="271px" ImageUrl="~/poc/images/transparente.png" ImageAlign="Top" OnClientClick="CallCIWeb();" OnClick="imbBtnCIWeb_Click" />
                </div>

                <div class="auto-style11">
                    <asp:ImageButton ID="imgMoreInfo" runat="server" Height="40px" Width="363px" ImageUrl="~/poc/images/transparente.png" ImageAlign="Top" OnClick="imgBtnMoreInfo_Click" />
                </div>
        </div>
        <asp:TextBox ID="labTrackId" runat="server" Width="240px" BorderColor="White" BorderStyle="None"></asp:TextBox>    
         
    </form>

   <script src="js/jquery-3.3.1.js"></script>
   <script src="js/modernizr-2.6.2.js"></script>
   <script src="js/sweetalert-dev.js"></script>
   <script src="js/BiometrikaJSHelper.js"></script>
   <script type="text/javascript">

       function CallCIWeb() {
          
       }


       function CallCI() {

           //alert('Paso 1...');
           // Instanciamos BiometrikaClient con la URL de consumo de la aplicación
           var biometrikaClient = new BiometrikaJSHelper('http://localhost:9191/');
           // Seteamos las propiedades requeridas
           biometrikaClient.TypeId = 'RUT'; //$("#TypeId").val().trim();
           biometrikaClient.ValueId = $("#txtRUT").val().trim();
           biometrikaClient.Mail = $("#txtMail").val().trim(); //"gsuhit@gmail.com";
           alert("Param => " + biometrikaClient.TypeId  + "-" + biometrikaClient.ValueId + " " + biometrikaClient.Mail);
           // Llamamos a la captura de CI
           //alert('Paso 2...');
           var biometrikaClientResponse = biometrikaClient.GetCI();
           // Evaluamos respuesta y controlamos errores
           // Respuesta nula
           //alert('Paso 3...'); 

           //var r = biometrikaClientResponse.Status;
           //var r1 = biometrikaClientResponse.Data;

           // if (biometrikaClientResponse == null) {
                //swal("Error", "Error general", "error");
           //     alert("Error general - Respuesta Nula");
           //     return;
            //}
            // Status falso
           if (!biometrikaClientResponse.Status) {
               //swal("Error", biometrikaClientResponse.Message, "error");
               alert("Error - " + biometrikaClientResponse.Message);
               return;
           } else {
               //var index = biometrikaClientResponse.Data.indexOf("TrackIdCI\":\"");	
			   //alert("Posicion = " + index);
			   //var index2 = json.indexOf("\",\"CI");
			   //alert("Posicion = " + index2);
			   //var trackid = json.substring(index+12, json.indexOf("\",\"CI"));
               // Si todo está ok obtenemos la respuesta con la huella y los demas datos de biometrikaClientResponse.Data
               console.log("CIResult.aspx?trackid=" + biometrikaClientResponse.Data + "&status=1");
               //alert(" MAnda a => CIResult.aspx?trackid=" + biometrikaClientResponse.Data + "&status=1");
               //document.location.href = "CIResult.aspx?trackid=" + biometrikaClientResponse.Data + "&status=1";
               //window.location = "CIResult.aspx?trackid=" + biometrikaClientResponse.Data + "&status=1";
               //$("#labTrackId").text(biometrikaClientResponse.Data);
               $("#labTrackId").val(biometrikaClientResponse.Data);
               //$("#labTrackId").Text = biometrikaClientResponse.Data;
           } 
        }

</script>
</body>
</html>

