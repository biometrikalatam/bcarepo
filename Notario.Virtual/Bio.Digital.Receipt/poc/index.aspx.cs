﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bio.Digital.Receipt.example
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                {
                    if (!string.IsNullOrEmpty(labTrackId.Text))
                    {
                        Response.Redirect("CIResult.aspx?trackid=" + labTrackId.Text + "&status=1");
                    } 
                } else
                {
                    labTrackId.Text = "";
                }
            }
            catch (Exception)
            {
                
            }
        }

        protected void btnQR_Click(object sender, EventArgs e)
        {
            Response.Redirect("qrci.aspx?rut=" + txtRUT.Text + "&mail=" + txtMail.Text);
        }

        protected void imgBtnQR_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("qrci.aspx?rut=" + txtRUT.Text + "&mail=" + txtMail.Text);
        }

        protected void imgBtnMoreInfo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("http://www.notariovirtual.cl");
        }

        protected void imbBtnCIWeb_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect(Properties.Settings.Default.URLCIWeb);
        }
    }
}