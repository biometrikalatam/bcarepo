﻿using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bio.Digital.Receipt.example
{
    public partial class qrci : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string urlgooglemaps;
            if (!IsPostBack)
            {
                try
                {

                    string rut = Request.QueryString["rut"];
                    string mail = Request.QueryString["mail"];

                    //labVerificando.Text = "Verificando RUT = " + rut + " [" + mail + "]";

                    string xmlparamin, xmlparamout;
                    Bio.Digital.Receipt.Api.XmlParamIn oParamin = new Bio.Digital.Receipt.Api.XmlParamIn();
                    oParamin.Actionid = 1;
                    oParamin.Origin = 1;
                    oParamin.Companyid = 1;
                    oParamin.ApplicationId = 7;
                    oParamin.CIParam = new Bio.Digital.Receipt.Api.CIParam();
                    oParamin.CIParam.TypeId = "RUT";
                    oParamin.CIParam.ValueId = rut;
                    oParamin.CIParam.DestinaryMail = mail;
                    xmlparamin = Bio.Digital.Receipt.Api.XmlUtils.SerializeAnObject(oParamin);


                    int ret = CIServiceManager.Process(oParamin, out xmlparamout);
                    if (ret == 0)
                    {
                        if (!string.IsNullOrEmpty(xmlparamout))
                        {
                            Bio.Digital.Receipt.Api.XmlParamOut oParamout = Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                            if (!string.IsNullOrEmpty(oParamout.QR))
                            {
                                labRUT.Text = "RUT " + oParamout.CIParam.ValueId;
                                labTrackId.Text = oParamout.CIParam.TrackId;
                                //Bitmap QRbmp = null;
                                //Global.QR.Add(rut,oParamout.QR);
                                byte[] byImgQR = Convert.FromBase64String(oParamout.QR);
                                System.IO.File.WriteAllBytes(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + rut + ".jpg", byImgQR);

                                imgQR.ImageUrl = Properties.Settings.Default.URLTempQR + rut + ".jpg";
                                //imgQR.
                                //System.IO.MemoryStream m = new System.IO.MemoryStream(byImgQR);
                                //QRbmp = new Bitmap(System.Drawing.Image.FromStream(m));
                                //m.Close();
                                //this.picTokenQRCI.Image = QRbmp;
                                //txtTokenQRCI.Text = oParamout.CIParam.TrackId;
                                //_CheckCIAutomatico = true;
                                //Q_Intentos = 1;
                                //labChecking.Visible = true;
                                //System.IO.File.Delete(Global.ROOT + Properties.Settings.Default.DebugVideoImagenPath + rut + ".jpg");
                               
                            }
                        }
                    }
                    else
                    {

                    }
                }
                catch (Exception ex)
                {
                    string s = ex.Message;

                }
            }
            
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("index.aspx");
        }
    }
}