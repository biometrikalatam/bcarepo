﻿function BiometrikaJSHelper(host) {
    this.BaseUrl = host;
    this.Src = "";
    this.TypeId = "";
    this.ValueId = "";
    this.Mail = "";
    this.Hash = Math.floor((Math.random() * 100) + 1);

    this.GetCI = function () {
        //alert('Paso 2.1...');
        var returnData = new BiometrikaClientResponse();

        //alert('Paso 2.2...');
        if (this.TypeId == null || this.TypeId == '') {
            returnData.Status = false;
            returnData.Message = "Ingrese Tipo de Identificación";
            return returnData;
        }
        //alert('Paso 2.3...');
        if (this.ValueId == null || this.ValueId == '') {
            returnData.Status = false;
            returnData.Message = "Ingrese Identificación";
            return returnData;
        }

        //alert('Paso 2.4...');
        this.Src = "CI";
        this.Theme = "theme_ligthgreen";

        var finalUrl = this.BaseUrl +
            "?src=" + this.Src +
            "&TypeId=" + this.TypeId +
            "&ValueId=" + this.ValueId +
            "&Mail=" + this.Mail;
        //alert('url = ' + finalUrl);


        var a = function (callback) {
            //alert('Paso 2.5 Calling...');
            $.ajax({
                type: "GET",
                url: finalUrl,
                async: false,
                crossDomain: true,
                processData: false,
                error: function (error) {
                    var errMsg = "Error al llamar a BiometrikaClient, " + error.statusText;
                    returnData.Status = false;
                    returnData.Message = errMsg;
                    console.log(errMsg);
                    //alert(errMsg);
                },
                success: callback
            });
        };
        //alert('Paso 2.6 - msg = ' + msg);
        a(function (msg) {
            console.log(msg);
//            alert(msg);
            //var obj = jQuery.parseJSON(msg);
            //alert(obj.CI);
            //var mstr = msg;
            //var index = mstr.indexOf("TrackIdCI\":\"");
            //alert("Posicion = " + index);
            //var index2 = mstr.indexOf("\",\"CI");
            //alert("Posicion = " + index2);
            var trackid = msg.TrackIdCI; //mstr.substring(index + 12, mstr.indexOf("\",\"CI"));

            returnData.Status = (trackid != null);
            returnData.Data = trackid;
        });
        console.log('Paso 2.7 - returnData = ' + returnData);
        //alert('Paso 2.7 - returnData = ' + returnData);
         return returnData;
    };
}

function BiometrikaClientResponse() {
    this.Status = false;
    this.Message = "";
    this.Data = "";
}