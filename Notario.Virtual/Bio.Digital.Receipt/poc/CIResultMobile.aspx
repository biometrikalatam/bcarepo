﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CIResultMobile.aspx.cs" Inherits="Bio.Digital.Receipt.example.MobileCIResult" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link href="StyleSheetNV.css" rel="stylesheet" />
    <title></title>

     <style type="text/css">
         
         .auto-style1 {
             font-family:Arial;
             font-size:medium;
             font-weight:bold;
             color:black;
             height:20px;
             text-align:left;
             background-color:lightgray;
         }
         
         .auto-style2 {
             font-family:Arial;
             font-size:medium;
             font-weight:bold;
             color:darkgrey;
             height:20px;
             text-align:left;
         }
         
         .auto-style3 {
             font-family:Arial;
             font-size:1px;
             font-weight:bold;
             color:black;
             height:2px;
             text-align:left;
             background-color:lightgray;
         }

         .auto-style4 {
             width: 100px;
             border:solid;
             border-width:1;
             border-color:darkgrey;
         }
     </style>

</head>
<body>
    <form id="form1" runat="server">
    
       
        <table class="auto-style4">
            <tr>
                <td>
                    <img src="images/TitleMobileResult.png" /></td>
            </tr>
            <tr>
                <td class="auto-style1"><asp:Label ID="Label1" runat="server" width="311px" Text="Documento Verificado"></asp:Label></td>
            </tr>
            <tr alig="center">
                <td align="center" class="auto-style2">&nbsp;&nbsp;&nbsp;<asp:Label ID="labTypeIdValueId" runat="server" Text="RUT 21284415-2"></asp:Label></td>
            </tr>
            <tr>
                <td class="auto-style1">
            <asp:Label ID="Label2" runat="server" width="311px" Text="Nombre"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center"class="auto-style2">&nbsp;&nbsp;&nbsp;<asp:Label ID="LabNombre" runat="server" Text="Gustavo Gerardo Suhit Gallucci"></asp:Label></td>
            </tr>
             <tr>
                <td class="auto-style1">
                <asp:Label ID="Label3" runat="server" width="250px" Text="Foto Cédula"></asp:Label></td>
            </tr>
            <tr>
                <td align="center"><asp:image runat="server" id="imgCedula" ImagenUrl="#" width="253px" height="123px" ImageAlign="AbsMiddle" BorderStyle="Solid" BorderColor="Gray" BorderWidth="1"  /></td>
            </tr>
             <tr>
                <td class="auto-style1">
                <asp:Label ID="Label8" runat="server" width="250px" Text="Selfie"></asp:Label></td>
            </tr>
            <tr>
                <td align="center"><asp:image runat="server" id="imgSelfie" ImagenUrl="#" width="110px" height="110px" ImageAlign="AbsMiddle" BorderStyle="Solid" BorderColor="Gray" BorderWidth="1"  /></td>
            </tr>
            <tr>
                <td class="auto-style1">
                <asp:Label ID="Label6" runat="server" width="250px" Text="Ubicación Generación CI"></asp:Label></td>
            </tr>
            <tr >
                <td align="center">
                    <asp:image runat="server" id="imgMap" ImagenUrl="#" 
                    width="250px" height="180px" ImageAlign="AbsMiddle" BorderStyle="Solid" BorderColor="Gray" BorderWidth="1"  />
 
                </td>
            </tr>
            <tr alig="center" >
                <td class="auto-style1"><asp:Label ID="Label7" runat="server" width="171px" Text="Fecha Nacimiento"></asp:Label></td>
            </tr>
            <tr align="center">
                <td class="auto-style2">&nbsp;&nbsp;&nbsp;<asp:Label ID="labFechaNac" runat="server" Text="28/09/1969"></asp:Label></td>
            </tr>
            <tr align="left">
                <td class="auto-style1"><asp:Label ID="LabelExpiracion" runat="server" width="171px" Text="Fecha Expiración"></asp:Label></td>
            </tr>
            <tr alig="center">
                <td align="center" class="auto-style2">&nbsp;&nbsp;&nbsp;<asp:Label ID="labExpirationDay" runat="server" Text="17/08/2025"></asp:Label></td>
            </tr>
            <tr align="left">
                <td class="auto-style1"><asp:Label ID="Label4" runat="server" width="171px" Text="Nacionalidad"></asp:Label></td>
            </tr>
            <tr alig="center">
                <td align="center" class="auto-style2">&nbsp;&nbsp;&nbsp;<asp:Label ID="labNacionality" runat="server" Text="ARG"></asp:Label></td>
            </tr>
            <tr align="left">
                <td class="auto-style1"><asp:Label ID="Label5" runat="server" width="171px" Text="Nivel Seguridad" Height="16px"></asp:Label></td>
            </tr>
            <tr align="center">
                <td class="auto-style2">&nbsp;&nbsp;&nbsp;<asp:Label ID="labScore" runat="server" Text="0,5465856"></asp:Label></td>
            </tr>
             <tr align="left">
                <td class="auto-style1"><asp:Label ID="Label9" runat="server" width="171px" Text="Certificado PDF" Height="16px"></asp:Label></td>
            </tr>
              <tr alig="center">
                <td align="left">
                    &nbsp;&nbsp;&nbsp;<asp:HyperLink  runat="server" ID="linkPdf" NavigateUrl="#" ImageWidth="40"  ImageHeight="40" ImageUrl="~/poc/images/pdf.jpg">vv</asp:HyperLink>
                 </td>
            </tr>
              <tr align="left">
                <td class="auto-style3">&nbsp;</td>
            </tr>
             <tr alig="center">
                <td align="left">
                    &nbsp;
                 </td>
            </tr>
            <tr >
                <td align="center"><asp:Button ID="btnVolver" runat="server" BackColor="#0099CC" Font-Bold="True" Font-Size="Medium" ForeColor="White" Text="Volver" Width="119px" OnClick="btnVolver_Click" /></td>
            </tr>
         
            <tr>
                <td>
                    <img class="auto-style35" src="images/BottomMobileResult.png" /></td>
            </tr>
        </table>
    </form>
</body>
</html>
