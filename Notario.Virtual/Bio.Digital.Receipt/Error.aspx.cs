using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Bio.Digital.Receipt
{
    public partial class Error : System.Web.UI.Page
    {
        string toRedirect = "~/Manager/pages/login/login.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Abandon();
            if (Request["msg"] != null)
            {
                labMsg.Text = Request["msg"];
            }

            if (Request["action"] != null)
            {
                toRedirect = Request["action"];
            }
            
         }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect(toRedirect);
        }
    }
}
