using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Bio.Digital.Receipt
{
    public partial class CheckRD : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["msg"] != null) labMsg.Text = Request["msg"];
            
            
        }

        protected void btnCheck_Click(object sender, EventArgs e)
        {
            if (!this.reciboXMLUpload.HasFile && String.IsNullOrEmpty(this.txtReciboXML.Text))
            {
                labMsg.Text = "Debe ingresar un path de archivo v�lido o un recibo en formato XML...";
                return;
            }
            //ReplaceComillas();
            Server.Transfer("CheckRDResult.aspx",true);
        }

        private void ReplaceComillas()
        {
            this.txtReciboXML.Text = this.txtReciboXML.Text.Replace("\"", "&quot;");
        }

        protected void txtReciboXML_TextChanged(object sender, EventArgs e)
        {
            //this.txtReciboXML.Text = this.txtReciboXML.Text.Replace("\"", "&quot;");
        }
    }
}
