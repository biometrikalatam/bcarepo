﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class RevisionCompletedModel
    {
        public string trackid { get; set; }           //Trackid del NvProcedure
        public int status { get; set; }               //1-Aprobado | 2-Rechazado
        public string texttoinclude { get; set; }     //Si es status=1 => Mensaje a incluir en el documento
        public string reasonrejection { get; set; }   //Si está rechazado, indica el motivo
        public string userid { get; set; }            //Indica el user que realizo el proceso, para auditoria.      
    }

    public class RevisionCompletedModelR
    {
        public RevisionCompletedModelR() { }

        public RevisionCompletedModelR(int _code, string _msgerr, string _trackId)
        {
            code = _code;
            msgerr = _msgerr;
            trackId = _trackId;
        }

        public int code { get; set; }
        public string msgerr { get; set; }
        public string trackId { get; set; }

    }
}