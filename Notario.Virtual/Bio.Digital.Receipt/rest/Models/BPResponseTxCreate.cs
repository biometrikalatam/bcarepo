﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class BPResponseTxCreate
    {
        public BPResponseTxCreate() { }

        public BPResponseTxCreate(int _code, string _msg, string _trackid)
        {
            code = _code;
            msgerr = _msg;
            trackid = _trackid;
        }

        public BPResponseTxCreate(int _code, string _msg, string _trackid, string _urlservice)
        {
            code = _code;
            msgerr = _msg;
            trackid = _trackid;
            urlservice = _urlservice;
        }

        public int code;
        public string msgerr;
        public string trackid;
        public string urlservice;

    }
}