﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    /*
     {
        “code”: 200,
        “msgError”: null,
        "trackId": "8b1b9985925f447ab19180e10c283687",
        "document": "6/YgjyyauGU/SAugu…..SUGIAIgigui=",,
        “urlvalidator”: “https://www…”,
        “notaryidvalidator”: “263712536”
     } 

    */
    public class NotarizedRequestModel
    {
        public int code { get; set; }
        public string msgError { get; set; }
        public string trackid { get; set; }
        public string document { get; set; }
        public string urlvalidator { get; set; }
        public string notaryidvalidator { get; set; }
    }

 
}