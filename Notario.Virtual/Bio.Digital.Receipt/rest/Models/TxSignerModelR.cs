﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class TxSignerModelR
    {
        public TxSignerModelR() { }

        public TxSignerModelR(int _code, string _msg)
        {
            code = _code;
            msgerr = _msg;
            trackid = null;
            status = "PENDING";
        }

        public TxSignerModelR(int _code, string _msg, string _trackid, string _status, string _statusdescription)
        {
            code = _code;
            msgerr = _msg;
            trackid = _trackid;
            status = _status;
            statusdescription = _statusdescription;
        }

        public int code;
        public string msgerr;
        public string trackid;
        public string status;
        public string statusdescription;
        public int receiptId;
        public int type; //0-CI | 1-Document TTag u otros
        public int actionId;
        public DateTime dateGen;
        public DateTime lastModify;
        public int resultCode;
        public string document;
        public string typeId;
        public string valueId;
        public string taxidcompany;
        public string workstationid;
        public string georef;
        public string score;
        public string threshold;

        public Dictionary<string, string> formdata;
        
        public IList<ImageSample> Samples;

    }
}