﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models.v2021
{
    public class NVResponse
    {
        /// <summary>
        /// 
        /// </summary>
        public NVResponse() { }

        public NVResponse(int _code, string _msg, object _data) {
            code = _code;
            message = _msg;
            data = _data;
        }



        //Codigo de error. 0 - Sin Error | <0 - Error desde Errors
        public int code { get; set; }
        //MEnsjae de error si code es <0
        public string message { get; set; }
        //Objeto con datos en caso de retorno necesario.
        public object data { get; set; }
    }
}