﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class BPTxCreateR
    {

        public BPTxCreateR() { }
        public BPTxCreateR(string _url) {
            verifyUrl = _url;
        }

        public BPTxCreateR(string _trackidci, string _url)
        {
            trackidci = _trackidci;
            verifyUrl = _url;
        }

        public string trackidci;
        public string verifyUrl;
    }
}