﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class SignerCreateModel
    {
        public SignerCreateModel()
        {
        }

        //public CreateModel(string _msgerr)
        //{
        //    msgerr = _msgerr;
        //}

        //TypeVerify _type, int _onboardingmandatory,
        //int _signerinclude, int _checklocksrcei,
        public SignerCreateModel(int _company, int _application, string _typeid, string _valueid, string _taxidcompany, 
                            string _theme, string _customertrackid, string _callbackurl, string _redirecturl, string _successurl, 
                            float _threshold, string _mailCertify, string[] _listmails, string _workflowname)
        {
            company = _company;
            application = _application;
            typeId = _typeid;
            valueId = _valueid;
            mailCertify = _mailCertify;
            taxidcompany = _taxidcompany;
            theme = _theme;
            customerTrackId = _customertrackid;
            callbackUrl = _callbackurl;
            redirectUrl = _redirecturl;
            successUrl = _successurl;
            //type = _type;
            //onboardingmandatory = _onboardingmandatory;
            //signerInclude = _signerinclude;
            //checklockSrcei = _checklocksrcei;
            threshold = _threshold;
            listMailsDestinatary = _listmails;
            workflowname = _workflowname;
        }

        public int company;
        public int application;
        public string typeId;
        public string valueId;
        public string taxidcompany;
        public string mailCertify;
        public string cellNumber;
        public string theme;
        public string customerTrackId;
        public string callbackUrl;
        public string redirectUrl;
        public string successUrl;

        //public TypeVerify type;
        //public int onboardingmandatory; // 0-No o 1-Si
        //public int signerInclude; // 0-No o 1-Si
        //public int checklockSrcei; // 0-No o 1-Si
        public float threshold = 60; //0 a 100 => Default 60
        public string[] listMailsDestinatary;
        public string workflowname;
    }

    ///// <summary>
    ///// Tipos de Verificacion
    ///// </summary>
    //public enum TypeVerify
    //{
    //    tv2d = 1,           //No soportada ahora
    //    tv3d = 2,           //Facetec
    //    tv3dplus = 3,       //Jumio Automatico
    //    tv3dstrong = 4      //Jumio Hibrido
    //}

    public class SignerCreateModelR
    {
        public SignerCreateModelR() { }

        public SignerCreateModelR(string _message, string _trackid, string _url)
        {
            message = _message;
            trackid = _trackid;
            ciwebUrl = _url;
        }

        public string trackid;
        public string message;
        public string ciwebUrl;
    }

    /*
        Pasos posibles a configurar (deben ser en este orden y todo minusculas):
        1-autorization
        2-signerinclude
        3-
    */
    //public class Step
    //{
    //    public Step() { }

    //    public Step(string _name) 
    //    {
    //        name = _name;
    //    }
    //    public string name;
    //}
}