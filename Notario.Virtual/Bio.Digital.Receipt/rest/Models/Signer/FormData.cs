﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models.Signer
{
    public class FormData
    {
        public FormData() {
            FormDataDetail = new Dictionary<string, string>();
        }


        public Dictionary<string, string> FormDataDetail;
    }
}