﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class TxSignerVerifyModelR
    {

        public int code;
        public string msgerr;
        public string trackid;
        public string securitycode;
        public string status;
        public string statusdescription;
        public DateTime date;
        public Dictionary<string, string> infoverify;
    }
}