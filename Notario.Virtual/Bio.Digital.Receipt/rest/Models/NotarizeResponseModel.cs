﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class NotarizeResponseModel
    {
        public NotarizeResponseModel() { }

        public NotarizeResponseModel(int _code, string _msgerr, string _trackId) {
            code = _code;
            msgerr = _msgerr;
            trackId = _trackId;
        }

        public int code { get; set; }
        public string msgerr { get; set; }
        public string trackId { get; set; }
    }

    public class InitNotarizeResponseModel
    {
        public InitNotarizeResponseModel() { }

        public InitNotarizeResponseModel(int _code, string _msgerr, string _trackId, string _trackIdci, string _qr)
        {
            code = _code;
            msgerr = _msgerr;
            trackId = _trackId;
            trackidci = _trackIdci;
            qr = _qr;
        }

        public int code { get; set; }
        public string msgerr { get; set; }
        public string trackId { get; set; }
        public string trackidci { get; set; }
        public string qr { get; set; }
    }
}