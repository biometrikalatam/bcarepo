﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class CIResendModel
    {
        public CIResendModel() { }

        public CIResendModel(int _company, string _trackid)
        {
            company = _company;
            trackid = _trackid;
        }

        public int company;
        public string trackid;
    }
}