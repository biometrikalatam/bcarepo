﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    
    public class BpTxDataResponse
    {
        public BpTxDataResponse() { }

        public BpTxDataResponse(int _code, string _msgerr)
        {
            code = _code;
            msgerr = _msgerr;
        }

        public int code;
        public string msgerr;
        public int company;
        public int operationcode;
        public TypeVerify typeverify;
        public int onboardingmandatory;
        public int result;
        public double score;
        public double threshold;
        public int codereject;
        public string descreject; //Motivo de rechazo, para caso de chequeos hibridos, adulteraciones, etc.
        public string estadolock; //Motivo de rechazo, para caso de chequeos hibridos, adulteraciones, etc.
        public string razonlock; //Motivo de rechazo, para caso de chequeos hibridos, adulteraciones, etc.
        public string vigenciafromlock; //Motivo de rechazo, para caso de chequeos hibridos, adulteraciones, etc.
        public string georef;
        public string enduser;
        public string videourl;

        public TxtData personaldata;
        public string taxidcompany;

        public IList<ImageSample> Samples;
    }

    public class TxtData
    {
        public string typeid;
        public string valueid;
        public string name;
        public string patherlastname;
        public string motherlastname;
        public string sex;
        public string documentseriesnumber;
        public string documentexpirationdate;
        public string visatype;
        public string birthdate;
        public string birthplace;
        public string nationality;
        public string profession;
        public string creation;
        public string verificationsource;
    }

    public class ImageSample
    {
        public ImageSample() { }

        public ImageSample(string _code, TypeImage _typeimage, TypeImageFormat _typeimageformat, string _data)
        {
            code = _code;
            typeimage = _typeimage;
            typeimageformat = _typeimageformat;
            data = _data;
        }

        public ImageSample(TypeImage _typeimage, TypeImageFormat _typeimageformat, string _data)
        {
            typeimage = _typeimage;
            typeimageformat = _typeimageformat;
            data = _data;
        }

        public TypeImage typeimage; //
        public TypeImageFormat typeimageformat = TypeImageFormat.jpg; //0-jpg | 1-png | 2-bmp
        public string code;
        public string data;
    }

    public enum TypeImageFormat
    {
        jpg = 0,
        png = 1,
        bmp = 2,
        pdf = 3,
        json = 4,
        video = 5,
        videourl = 6
    }

    public enum TypeImage
    {
        cedulafront = 0,
        cedulaback = 1,
        selfie = 2,
        cedulasignature = 3,
        cedulafoto = 4,
        manualsignature = 5,
        qr = 6,
        image = 7,
        facemap = 8,
        pdf = 9,
        json = 10,
        video = 11
    }

 
}