﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{

    public class UsersNotaryModel
    {
        public int notaryid { get; set; }
        public string mail { get; set; }   //Del user que inicio el trámite
        public int status { get; set; }
        public int withdetails { get; set; } //0-Liviano | 1-withdetails
    }

    public class UsersNotaryModelR
    {
        public UsersNotaryModelR(List<User> _listUsers)
        {
            Users = _listUsers;
        }

        public List<User> Users { get; set; }
    }

    public class LoginPortalNotaryModel
    {
        public string user { get; set; }
        public string password { get; set; }
    }

    public class LoginPortalNotaryModelR
    {

        public LoginPortalNotaryModelR(int _code, Core.Database.NvNotaryUsers _user)
        {
            code = _code;
            user = _user;
        }

        public int code { get; set; }
        public Core.Database.NvNotaryUsers user { get; set; }
    }

    public class ListProceduresNotaryModel
    {
        public int notaryid { get; set; }
        public string trackid { get; set; }
        public string mail { get; set; }   //Del user que inicio el trámite
        public string mailnotary { get; set; } //De un funcionario de la notaria que tomo o reviso el tramite (ver si lo usamos)
        public DateTime from { get; set; }
        public DateTime till { get; set; }
        public int status { get; set; }
        public int withdetails { get; set; } //0-Liviano | 1-withdetails
    }

    public class ListProceduresNotaryModelR
    {
        public ListProceduresNotaryModelR(List<Procedure> _listProcedures)
        {
            procedures = _listProcedures;
        }

        public List<Procedure> procedures { get; set; }
        //public ListProceduresNotaryModelR(IList<Core.Database.NvUsersProcedures> _listProcedures)
        //{
        //    procedures = _listProcedures;
        //}

        //public IList<Core.Database.NvUsersProcedures> procedures { get; set; }
    }

    public class ModifyProcedureNotaryModel
    {
        public string trackid { get; set; }
        public int status { get; set; }   //1-Aprobado | 2-Rechazado
        public string texttoinclude { get; set; } //Texto para incluir en documento generado
        public string reasonreject { get; set; } //Texto para incluir en motivo de rechazo
    }


    public class Procedure
    {

        public Procedure(Core.Database.NvUsersProcedures _dbProcedure, string _NameUser, string _MailUser, 
                         string _NameUserNotary, string _MailUserNotary)
        {
            dbProcedure = _dbProcedure;
            NameUser = _NameUser;
            MailUser = _MailUser;
            NameUserNotary = _NameUserNotary;
            MailUserNotary = _MailUserNotary;
        }

        public Core.Database.NvUsersProcedures dbProcedure { get; set; }
        public virtual string NameUser { get; set; }
        public virtual string MailUser { get; set; }
        public virtual string NameUserNotary { get; set; }
        public virtual string MailUserNotary { get; set; }



        //protected DateTime? _DateFinish;

        //public virtual int Id { get; set; }
        ////public virtual int IdProvider { get; set; }
        //public virtual int IdNotary { get; set; }
        //public virtual int IdNotaryProcedure { get; set; }
        //public virtual string NameProcedure { get; set; }
        //public virtual int IdProcedure { get; set; }
        //public virtual int IdUser { get; set; }
        //public virtual string MailUser { get; set; }
        //public virtual string TrackId { get; set; }
        ////public virtual int IdProvider { get; set; }
        ////public virtual string DocumentB64Original { get; set; }
        ////public virtual string DocumentB64 { get; set; }
        ////public virtual string DocumentB64Notarized { get; set; }

        //public virtual DateTime DateInit { get; set; }
        //public virtual int Status { get; set; }

        //public virtual double Price { get; set; }

        //public virtual string UrlValidator { get; set; }
        //public virtual string NotaryIdValidator { get; set; }
        //public virtual string Message { get; set; }

        //public virtual int StatusNotary { get; set; }
        //public virtual string StatusMsgNotary { get; set; }

        //public virtual int PaymentStatus { get; set; }
        ////public virtual string PaymentOrder { get; set; }
        ////public virtual string PaymentUrl { get; set; }
        ////public virtual string PaymentToken { get; set; }
        ////public virtual string PaymentInfo { get; set; }
        ////public virtual double PaymentAmount { get; set; }
        ////public virtual string PaymentMedia { get; set; }
        ////public virtual string PaymentDate { get; set; }

        //public virtual int StatusRevisionNotary { get; set; }
        //public virtual int UserRevisionNotary { get; set; }


        //public virtual DateTime? DateFinish
        //{
        //    get { return _DateFinish; }
        //    set { if (value != this._DateFinish) { _DateFinish = value; } }
        //}
    }

    public class ListItemProcedure
    {

        public ListItemProcedure(int _id, string _trackid, string _documento, int _status, string _name, string _tramite, string _fechainit, int _qfirmantes)
        {
            id = _id;
            trackid = _trackid;
            documento = _documento;
            status = _status;
            name = _name;
            tramite = _tramite;
            fechainit = _fechainit;
            qfirmantes = _qfirmantes;
        }

        public virtual int id { get; set; }
        public virtual string trackid { get; set; }
        public virtual string documento { get; set; }
        public virtual int status { get; set; }
        public virtual string name { get; set; }
        public virtual string tramite { get; set; }
        public virtual string fechainit { get; set; }
        public virtual int qfirmantes { get; set; }

    }


    public class User
    {

        //public User(string rut, string mail, )
        //{
        //    dbProcedure = _dbProcedure;
        //    NameUser = _NameUser;
        //    MailUser = _MailUser;
        //    NameUserNotary = _NameUserNotary;
        //    MailUserNotary = _MailUserNotary;
        //}

        public virtual string Rut { get; set; }
        public virtual string Mail { get; set; }
        public virtual string Name { get; set; }
        public virtual int IsBlocked { get; set; }
        public virtual int IdCompany { get; set; }
        public virtual List<int> Roles { get; set; }

    }

    public class ListProceduresPortalPublicModel
    {
        public int notaryid { get; set; }
        public string trackid { get; set; }
        public int userid { get; set; }   //Del user que inicio el trámite
        public int procedureid { get; set; }   //id del proceduta (ej.: Poder Simple)
        public DateTime from { get; set; }
        public DateTime till { get; set; }
        public int status { get; set; }
        public int withdetails { get; set; } //0-Liviano | 1-withdetails
    }

    public class NotaryPortalModels
    {
    }
}