﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class BPTxCreate
    {
        public int company { get; set; }
        public string typeid { get; set; }
        public string valueid { get; set; }
        public string taxidcompany { get; set; }
        public string theme { get; set; }
        public string customertrackid { get; set; }
        public string callbackurl { get; set; }
        public string redirecturl { get; set; }
        public string successurl { get; set; }
        public string typeverifyonboarding { get; set; }
        public string typeverifyauth { get; set; }
        public int onboardingmandatory { get; set; }
        public int onboardingtype { get; set; }
        public int signerinclude { get; set; }
        public int autorizationinclude { get; set; }
        public string autorizationmessage { get; set; }
        public double threshold { get; set; }
        public int checklocksrcei { get; set; } // 0-No o 1-Si
        public string titlebpweb { get; set; }
        public string messagebpweb { get; set; }
        public int videoinclude { get; set; }
        public string videomessage { get; set; }
        public int checkadult { get; set; }
        public int checkexpired { get; set; }
        public int georefmandatory { get; set; }

        public int carregisterinclude { get; set; } // 0-No o 1-Si
        public int writinginclude { get; set; } // 0-No o 1-Si
        public string formid { get; set; } // Null=No incluye form o "stringid"=Nombre el form (para futuro que será definible, ahora solo usa el form fijo)

    }
}