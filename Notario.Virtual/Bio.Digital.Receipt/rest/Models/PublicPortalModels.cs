﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class PublicPortalModels
    {
    }

    public class LoginPublicPortalModel
    {
        public string user { get; set; }
        public string password { get; set; }
    }

    public class LoginPublicPortalModelR
    {

        public LoginPublicPortalModelR(int _code, Core.Database.NvUsers _user)
        {
            code = _code;
            user = _user;
        }

        public int code { get; set; }
        public Core.Database.NvUsers user { get; set; }
    }
}