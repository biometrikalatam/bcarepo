﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class CIRegisterModel
    {
        public CIRegisterModel()
        {
        }

        public CIRegisterModel(int _company, int _application, string _trackid, string _typeid, string _valueid,
                               float _threshold, float _score, string _mailCertify, string _cellNumber,
                               string _selfie, string _fingerSample, string _fingersamplejpg)
        {
            company = _company;
            application = _application;
            trackId = _trackid;
            typeId = _typeid;
            valueId = _valueid;
            mailCertify = _mailCertify;
            cellNumber = _cellNumber;
            threshold = _threshold;
            score = _score;
            selfie = _selfie;
            fingerSample = _fingerSample;
            fingerSampleJpg = _fingersamplejpg;
        }

        public int company;
        public int application;
        public string trackId;
        public string typeId;
        public string valueId;
        public string mailCertify;
        public string cellNumber;
        public float threshold = 60; //0 a 100 => Default 60
        public float score = 0; //0 a 100 => Default 60
        public string selfie;
        public string fingerSample; //WSQ del dedo
        public string fingerSampleJpg; //JPG del dedo
    }

    
    public class CIRegisterModelR
    {
        public CIRegisterModelR()
        {
        }

        public CIRegisterModelR(string _trackid, string _certifyPDF)
        {
            trackId = _trackid;
            certifyPDF = _certifyPDF;
        }

        public string trackId;
        public string certifyPDF;
    }
}