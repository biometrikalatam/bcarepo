﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{

    public class ResponseModel
    {
        public ResponseModel() { }
        public ResponseModel(int _code, string _msg)
        {
            code = _code;
            msgerr = _msg;
        }


        public int code { get; set; }
        public string msgerr { get; set; }
    }


    public class InitProcedureOneRequestModel
    {
        public string rut { get; set; }
        public string mail { get; set; }
        public int notaryId { get; set; }
        public int procedureId { get; set; }
        public string message { get; set; }
        public string document { get; set; }
        public List<Evidence> evidences { get; set; }
    }

    public class NotarizeOneRequestModel
    {
        public string trackid { get; set; }
        public string mail { get; set; }
        public int notaryId { get; set; }
        public int procedureId { get; set; }
        //public int providerId { get; set; }
        public string document { get; set; }
        public List<Evidence> evidences { get; set; }
        public string payment { get; set; }
        public string message { get; set; }
    }

    public class NotarizeRequestModel
    {
        public int userId { get; set; }
        public int notaryId { get; set; }
        public int procedureId { get; set; }
        //public int providerId { get; set; }
        public string document { get; set; }
        public List<Evidence> evidences { get; set; }
        public string payment { get; set; }
        public string message { get; set; }
    }

    public class Evidence
    {
        public string name { get; set; }
        public int type { get; set; }
        public string value { get; set; }
        public string trackidext { get; set; }  //Cuando la evidencia traiga un track id de servicio externo para chequeos posteriores 
                                                //(ej.: Trackid de CI) 
        public int status { get; set; }  //0-Falta | 1-Recibida ok
    }


    public class RegisterRequestModel
    {
        public string rut { get; set; }
        public string name { get; set; }
        public string phaterlastname { get; set; }
        public string motherlastname { get; set; }
        public string mail { get; set; }
        public string password { get; set; }
    }

    public class ValidationRegisterRequestModel
    {
        public string mail { get; set; }
        public string code { get; set; }
    }

    public class LoginRequestModel
    {
        public string mail { get; set; }
        public string password { get; set; }
    }

    public class NotarizedStatusRequestModel
    {
        public string trackid { get; set; }
    }


    #region v2

    public class v2NotarizeInitModel
    {

        public string rut { get; set; }
        public string mail { get; set; }
        public int notaryId { get; set; }
        public int procedureId { get; set; }
        public string message { get; set; }
        public string document { get; set; }
        public List<Evidence> evidences { get; set; }
        public string callbackUrl { get; set; }
        public string redirectUrl { get; set; }
        public string successUrl { get; set; }
        public string workflowname { get; set; }

        //public float threshold = 60; //0 a 100 => Default 60
        //public int company { get; set; }
        //public int application { get; set; }
        //public string typeId { get; set; }
        //public string valueId { get; set; }
        //public string taxidcompany { get; set; }
        //public string mailCertify { get; set; }
        //public string cellNumber { get; set; }
        //public string theme { get; set; }
        //public string customerTrackId { get; set; }
        //public string[] listMailsDestinatary { get; set; }

    }

    #endregion v2

    #region v2021

    public class CreateProcedureModel
    {

        public int iduser { get; set; }
        public int notaryId { get; set; }
        public int procedureId { get; set; }
        public string message { get; set; }
        public string document { get; set; }
        public List<Evidence> evidences { get; set; }
        public List<Signatory> signatories { get; set; }

        public string callbackUrl { get; set; }
        public string redirectUrl { get; set; }
        public string successUrl { get; set; }
        public string workflowname { get; set; }

        //public float threshold = 60; //0 a 100 => Default 60
        //public int company { get; set; }
        //public int application { get; set; }
        //public string typeId { get; set; }
        //public string valueId { get; set; }
        //public string taxidcompany { get; set; }
        //public string mailCertify { get; set; }
        //public string cellNumber { get; set; }
        //public string theme { get; set; }
        //public string customerTrackId { get; set; }
        //public string[] listMailsDestinatary { get; set; }

    }

    public class Signatory
    {
        public string typeid { get; set; }
        public string valueid { get; set; }
        public string mail { get; set; }
        public string phone { get; set; }
    }

    #endregion 2021
}