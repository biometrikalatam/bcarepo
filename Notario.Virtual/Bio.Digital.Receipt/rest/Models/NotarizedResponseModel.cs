﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    /*
     {
        “code”: 200,
        “msgError”: null,
        "trackId": "8b1b9985925f447ab19180e10c283687",
        "document": "6/YgjyyauGU/SAugu…..SUGIAIgigui=",
        “urlvalidator”: “https://www…”,
        “notaryidvalidator”: “263712536”
     } 

    */
    public class NotarizedResponseModel
    {
        public NotarizedResponseModel() { }

        public NotarizedResponseModel(int _code, string _msgerr, string _trackId, string _document, 
                                      string _urlvalidator, string _notaryidvalidator, int _status, 
                                      int _notaryid, int _procedureid, bool _isuserenabled,
                                      int _statuspayment, string _urlpayment, string _tokenpayment, int _statusnotary, string _msgstatusnotary) {
            code = _code;
            msgerr = _msgerr;
            trackId = _trackId;
            document = _document;
            urlvalidator = _urlvalidator;
            notaryidvalidator = _notaryidvalidator;
            status = _status;
            notaryid = _notaryid;
            procedureid = _procedureid;
            isuserenabled = _isuserenabled;
            statuspayment = _statuspayment;
            urlpayment = _urlpayment;
            tokenpayment = _tokenpayment;
            statusnotary = _statusnotary;
            msgstatusnotary = _msgstatusnotary;
        }

        public int code { get; set; }
        public string msgerr { get; set; }
        public string trackId { get; set; }
        public int status { get; set; }
        public int notaryid { get; set; }
        public int procedureid { get; set; }
        public bool isuserenabled { get; set; }
        public int statuspayment { get; set; }
        public string urlpayment { get; set; }
        public string tokenpayment { get; set; }
        public int statusnotary { get; set; }
        public string msgstatusnotary { get; set; }
        public string document { get; set; }
        public string urlvalidator { get; set; }
        public string notaryidvalidator { get; set; }
    }
}