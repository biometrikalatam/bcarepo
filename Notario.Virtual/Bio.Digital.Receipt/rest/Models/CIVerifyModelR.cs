﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class CIVerifyModelR
    {
        public CIVerifyModelR()
        {
        }

        public CIVerifyModelR(string _trackid, string _typeid, string _valueid, double _threshold, double _score, int _resultVerify, string _certifypdf)
        {
            trackId = _trackid;
            typeId = _typeid;
            valueId = _valueid;
            threshold = _threshold;
            score = _score;
            resultVerify = _resultVerify;
            certifypdf = _certifypdf;
        }

        public string trackId;
        public string typeId;
        public string valueId;
        public double threshold = 60; //0 a 100 => Default 60
        public double score = 0; //0 a 100 => Default 60
        public int resultVerify; //1-Positivo | 2 - Negativo
        public string certifypdf;
    }
}