﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class ProcedureResponseModel
    {

        //1.- Info de encabezado
        public int code { get; set; }
        public string msgError { get; set; }

        //2.- Info de procedure

        //2.1.- Info de procedure Base
        public virtual int Id { get; set; }
        public virtual string TrackId { get; set; }
        public virtual string Provider { get; set; }
        
        public virtual int ProcedureId { get; set; }

        public virtual string NameProcedure { get; set; }
        public virtual double Price { get; set; }
        public virtual string DateInit { get; set; }
        public virtual string DateEnd { get; set; }
        public virtual int Status { get; set; }

        //2.2.- Info Usuario
        public virtual string Rut { get; set; }
        public virtual string Mail { get; set; }
        public virtual string Message { get; set; }

        //2.3.- Info Notaria/Notario
        public virtual int NotaryId { get; set; }
        public virtual string Notary { get; set; }
        public virtual string UrlValidator { get; set; }
        public virtual string NotaryIdValidator { get; set; }
        public virtual int StatusNotary { get; set; }
        public virtual string StatusMsgNotary { get; set; }

        //2.4.- Info Payment
        public virtual int PaymentStatus { get; set; }
        public virtual string PaymentOrder { get; set; }
        public virtual string PaymentUrl { get; set; }
        public virtual string PaymentToken { get; set; }
        public virtual string PaymentInfo { get; set; }
        //public virtual double PaymentAmount { get; set; }
        public virtual string PaymentMedia { get; set; }
        public virtual string PaymentDate { get; set; }

        //3.- Info Evidencias
        public List<ProcedureEvidence> ProcedureEvidences;

    }

    public class ProcedureEvidence
    {
        public ProcedureEvidence() { }

        public ProcedureEvidence(int _Id, string _Name, int _Type, string _Value, string _TrackIdExt, int _Status)
        {
            Id = _Id;
            Name = _Name;
            Type = _Type;
            Value = _Value;
            TrackIdExt = _TrackIdExt;
            Status = _Status;
        }


        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int Type { get; set; }
        public virtual string Value { get; set; }
        public virtual string TrackIdExt { get; set; }
        public virtual int Status { get; set; }

        public List<CIItem> CIItems;
    }

    public class CIItem
    {
        public CIItem() { }

        public CIItem(string _Key, string _Value)
        {
            Key = _Key;
            Value = _Value;
            //Type = _Type;
            //Value = _Value;
            //TrackIdExt = _TrackIdExt;
            //Status = _Status;
        }
        public virtual string Key { get; set; }
        public virtual string Value { get; set; }


        //public virtual string IDCardImage { get; set; }
        //public virtual string IDCardPhotoImage { get; set; }
        //public virtual string Selfie { get; set; }
        //public virtual string Map { get; set; }
        //public virtual string Rut { get; set; }
        //public virtual string Name { get; set; }
        //public virtual string WorkStationID { get; set; }
        //public virtual string WorkStationID { get; set; }

    }
}