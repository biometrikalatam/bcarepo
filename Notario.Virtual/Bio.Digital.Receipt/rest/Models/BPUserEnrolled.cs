﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{

    public class BPUserEnrolled
    {
        public int companyid;
        public string typeid;
        public string valueid;
    }

    public class BPUserEnrolledR
    {
        public int code;                    //0- Ejecuto bien sino <0 con error
        public int exist;                   //0-No o 1-Si
        public int isenrolledtv2d;          //0-No o 1-Si
        public int isenrolledtv3d;          //0-No o 1-Si
        public int isenrolledtv3dplus;      //0-No o 1-Si
    }
}