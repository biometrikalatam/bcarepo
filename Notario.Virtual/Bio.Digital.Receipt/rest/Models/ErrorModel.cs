﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Models
{
    public class ErrorModel
    {
        public ErrorModel() { }

        public ErrorModel(int _code, string _message, string _detailmessage) {
            code = _code;
            message = _message;
            detailMessage = _detailmessage;
        }


        public int code;
        public string message;
        public string detailMessage;
    }
}