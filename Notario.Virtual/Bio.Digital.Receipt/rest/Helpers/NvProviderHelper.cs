﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database;
using log4net;
using Newtonsoft.Json;
using NHibernate;
using RestSharp;

namespace Bio.Digital.Receipt.rest
{
    public class NvProviderHelper
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(NvProviderHelper));

        protected string _Url_Base = null;
        protected IRestClient Client;

        /// <summary>
        /// Constructor, en su instanciación configura el host de conección
        /// </summary>
        /// <param name="host">URI de la API NotarioVirtual</param>
        public NvProviderHelper(string host)
        {
            _Url_Base = host;
            LOG.Debug("NvProviderHelper - Creando Client en = " + host);
            Client = new RestClient(host);
            LOG.Debug("NvProviderHelper Creado OK!");
        }


        internal static NotarizeProviderResponsetModel Notarize(string urlbase, string trackid, int tramiteid, string document, 
                                                                List<Evidence> evidencia)
        {
            NotarizeProviderResponsetModel ret = null;
            try
            {
                LOG.Debug("NvProviderHelper.Notarize - IN....");
                IRestClient Client = new RestClient(urlbase);
                LOG.Debug("NvProviderHelper.Notarize - Creado IRestClient => " + urlbase);
                var request = new RestRequest("Notarize", Method.POST);
                LOG.Debug("NvProviderHelper.Notarize - Creada request...");
                #region //Comentado por ahora 
                //if (customTimeOut > 0)
                //{
                //    request.Timeout = customTimeOut;
                //    request.AddUrlSegment("customTimeOut", customTimeOut.ToString());
                //}
                //Evidence ev = new Evidence();
                //ev.name = "Certificacion de Identidad";
                //ev.type = 4;
                //ev.value = evidencia;
                //List<Evidence> listEV = new List<Evidence>();
                //listEV.Add(evidencia);
                #endregion
                LOG.Debug("NvProviderHelper.Notarize - Creando Param Request...");
                NotarizeProviderRequestModel req = new NotarizeProviderRequestModel();
                req.evidences = evidencia;
                req.document = document;
                req.tramiteId = 1;
                req.trackId = trackid;

                request.AddJsonBody(req);
                LOG.Debug("NvProviderHelper.Notarize - Param agregados al body...");
                try
                {
                    //Added 01/07/2020 - PAra controld e flujo manual con Gomila
                    if (Properties.Settings.Default.NVQAManualEnabled)
                    {
                        LOG.Debug("NvProviderHelper.Notarize - Sending mail por QA Manual...");
                        if (QAManualSend(trackid, tramiteid, document, evidencia))
                        {
                            ret = new NotarizeProviderResponsetModel(200, null, null);
                        } else
                        {
                            ret = new NotarizeProviderResponsetModel(500, "Error enviando mails a: " +
                                Properties.Settings.Default.NVQAManualMails + ", para QA Manual!", null);
                        }
                    } else
                    {
                        LOG.Debug("NvProviderHelper.Notarize - LLamando al servicio Notarize...");
                        IRestResponse response = Client.Execute(request);
                        var content = response.Content;
                        LOG.Debug("NvProviderHelper.Notarize - Respuesta content = " + content);
                        ret = JsonConvert.DeserializeObject<NotarizeProviderResponsetModel>(content);
                    }

                    
                }
                catch (Exception e)
                {
                    LOG.Debug("NvProviderHelper.Notarize - Error llamando al provider [" + e.Message + "]");
                    return new NotarizeProviderResponsetModel(Errors.IRET_ERR_CALLING_PROVIDER_SERVICE, 
                        "Error llamando al provider [" + e.Message + "]", null);
                }
            }
            catch (Exception ex)
            {
                LOG.Debug("NvProviderHelper.Notarize - Error desconocido en Notarize [" + ex.Message + "]");
                ret = null;
                //LOG.Error(" Error: " + ex.Message);
            }
            LOG.Debug("NvProviderHelper.Notarize - OUT!");
            return ret;
        }

       


        /// <summary>
        /// Revisa status de tramite en notario y actualiza informacion en NvUsersProcedures
        /// /// </summary>
        /// <param name="urlBase"></param>
        /// <param name="trackId"></param>
        /// <returns></returns>
        internal static NotarizedProviderResponseModel GetStatusNotarize(string urlBase, string trackId)
        {
            NotarizedProviderResponseModel ret = null;
            string msgerr;
            try
            {
                LOG.Debug("NvProviderHelper.GetStatusNotarize - IN....");
                IRestClient Client = new RestClient(urlBase);
                LOG.Debug("NvProviderHelper.GetStatusNotarize - Creado IRestClient => " + urlBase);
                var request = new RestRequest("GetStatusNotarize/" + trackId, Method.GET);
                LOG.Debug("NvProviderHelper.GetStatusNotarize - Creada request...");
                //NotarizeProviderRequestModel req = new NotarizeProviderRequestModel();

                LOG.Debug("NvProviderHelper.GetStatusNotarize - Param agregados al body...");
                try
                {
                    LOG.Debug("NvProviderHelper.GetStatusNotarize - LLamando al servicio Notarize...");
                    IRestResponse response = Client.Execute(request);
                    var content = response.Content;
                    LOG.Debug("NvProviderHelper.GetStatusNotarize - Respuesta content = " + content);
                    ret = JsonConvert.DeserializeObject<NotarizedProviderResponseModel>(content);

                    //Si parsea ok el retorno => Update en NvUsersProcedures
                    if (ret != null)
                    {

                        NvUsersProcedures nvUP = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackId, out msgerr);
                        if (nvUP != null) {
                            /*
                                200  	OK. msgError = null.
                                        document = "6/YgjyyauGU/SAugu…..SUGIAIgigui=".
                                1002	msgError = Un error interno impide obtener el documento de la solicitud. 
                                        ocument = null.
                                1005	msgError = No existe notarización para trackId xxxxxxx.
                                        document = null.
                                1007	msgError = Aun no se notariza el documento para trackId xxxxxxx. 
                                        document = null.
                                1009	msgError = El notario rechazo el documento para trackId xxxxxx. 
                                        document = null.
                                1010	msgError = Un error interno impide comprobar el estado de la solicitud (Cuando se verifica si trackId existe).
                                        document = null.
                            */
                            switch (ret.code)
                            {
                                case 200:
                                    nvUP.Status = StatusNvUsersProcedures.STATUS_NOTARIZED;
                                    nvUP.StatusNotary = 200;
                                    nvUP.DocumentB64Notarized = ret.document;
                                    nvUP.StatusMsgNotary = "Notarized OK";
                                    break;
                                case 1009:
                                    nvUP.Status = StatusNvUsersProcedures.STATUS_REJECTED;
                                    nvUP.StatusNotary = 1009;
                                    nvUP.StatusMsgNotary = ret.msgError;
                                    break;
                                default:
                                    nvUP.StatusNotary = ret.code;
                                    nvUP.StatusMsgNotary = ret.msgError;
                                    break;
                            }
                            bool bret = AdministradorNvUsersProcedures.Update(nvUP);
                            if (!bret) LOG.Error("NvProviderHelper.GetStatusNotarize - Error salvando NvUserProcedure...");
                        }
                    }

                }
                catch (Exception e)
                {
                    LOG.Debug("NvProviderHelper.GetStatusNotarize - Error llamando al provider [" + e.Message + "]");
                    return (new NotarizedProviderResponseModel(Errors.IRET_ERR_CALLING_PROVIDER_SERVICE,
                        "Error llamando al provider [" + e.Message + "]", trackId, null,null,null));
                }
            }
            catch (Exception ex)
            {
                LOG.Debug("NvProviderHelper.GetStatusNotarize - Error desconocido en Notarize [" + ex.Message + "]");
                ret = null;
                //LOG.Error(" Error: " + ex.Message);
            }
            LOG.Debug("NvProviderHelper.GetStatusNotarize - OUT!");
            return ret;
        }

        /// <summary>
        /// Envia mail a la lista de mails para QA Manual configurada. Se utiliza para pruebas con nuevos notarios.
        /// </summary>
        /// <param name="trackid"></param>
        /// <param name="tramiteid"></param>
        /// <param name="document"></param>
        /// <param name="evidencia"></param>
        private static bool QAManualSend(string trackid, int tramiteid, string document, List<Evidence> evidencia)
        {
            bool bRet = true;
            try
            {
                LOG.Debug("NvProviderHelper.QAManualSend IN...");
                LOG.Debug("NvProviderHelper.QAManualSend - Se enviará a = " + Properties.Settings.Default.NVQAManualMails);
                string[] listMails = Properties.Settings.Default.NVQAManualMails.Split(',');

                byte[] doc = Convert.FromBase64String(document);
                string[] strbody = new string[3];
                strbody[0] = "Trámite para Notarizar! Adjunto encontrará el archivo PDF para notarizar y evidencias en anexos dependiendo del trámite seleccionado.";
                strbody[1] = "El codigo unico del tramite para consultas o descargas desde el sitio de Notario Virtual es: <b>" +
                            trackid + "</b>.";
                strbody[2] = "Cantidad de Evidencias Anexas => " + ((evidencia != null) ? evidencia.Count.ToString() : "0");
                byte[] doc1;
                foreach (string oneMail in listMails)
                {
                    bRet = libs.Utils.NVNotify(oneMail, "[QA Manual] Tramite a Notarizar!", strbody, doc, "pdf", trackid + ".pdf");
                    LOG.Debug("NvProviderHelper.QAManualSend - Documento tramite => " + trackid + ".pdf enviado!" );
                    if (bRet && evidencia != null && evidencia.Count > 0)
                    {
                        foreach (Evidence item in evidencia)
                        {
                            string[] strbody1 = new string[2];
                            strbody1[0] = "Adjunto Trámite para Notarizar! Adjunto encontrará el archivo evidencias del trámite = <b>" +
                                        trackid + "</b>.";
                            strbody1[1] = "Evidencia Type = " + item.type.ToString() + " - " + item.name;
                            string tm = "pdf";
                            string ext = ".pdf";
                            //1-CI|2-Texto|3-JPG|4-PDF|5-XML
                            switch (item.type)
                            {
                                case 1:
                                    doc1 = Encoding.ASCII.GetBytes(item.value);
                                    tm = "CI";
                                    ext = ".CI.xml";
                                    break;
                                case 2:
                                    doc1 = Encoding.ASCII.GetBytes(item.value);
                                    tm = "txt";
                                    ext = ".txt";
                                    break;
                                case 3:
                                    doc1 = Convert.FromBase64String(item.value);
                                    tm = "jpg";
                                    ext = ".jpg";
                                    break;
                                case 5:
                                    doc1 = Encoding.ASCII.GetBytes(item.value);
                                    tm = "xml";
                                    ext = ".xml";
                                    break;
                                default:
                                    doc1 = Convert.FromBase64String(item.value);
                                    ext = ".pdf";
                                    tm = "pdf";
                                    break;
                            }
                            bRet = bRet && libs.Utils.NVNotify(oneMail, "[QA Manual] Anexo Tramite a Notarizar!", strbody1, doc1, tm, trackid + ext);
                            LOG.Debug("NvProviderHelper.QAManualSend - Anexo tramite => type = " + tm + " - " + trackid + ext +  " enviado!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bRet = false;
                LOG.Error("NvProviderHelper.QAManualSend Error: " + ex.Message);
            }
           
            LOG.Debug("NvProviderHelper.QAManualSend OUT!");
            return bRet;

        }
    }


    public class NotarizeProviderRequestModel
    {
        public string trackId { get; set; }
        public string rutnotaria { get; set; }
        public int tramiteId { get; set; }
        public string document { get; set; }
        public List<Evidence> evidences { get; set; }
    }

    public class Evidence
    {
        public string name { get; set; }
        public int type { get; set; }
        public string value { get; set; }

    }

    public class NotarizeProviderResponsetModel
    {

        NotarizeProviderResponsetModel() { }

        internal NotarizeProviderResponsetModel(int _code, string _msgerr, string _document)
        {
            code = _code;
            msgError = _msgerr;
            document = _document;
        }
        public int code { get; set; }
        public string msgError { get; set; }
        public string document { get; set; }
    }

    public class NotarizedProviderResponseModel
    {
        public NotarizedProviderResponseModel() { }

        public NotarizedProviderResponseModel(int _code, string _msgerr)
        {
            code = _code;
            msgError = _msgerr;
        }

        public NotarizedProviderResponseModel(int _code, string _msgerr, string _trackId, string _document, string _urlvalidator, string _notaryidvalidator)
        {
            code = _code;
            msgError = _msgerr;
            trackId = _trackId;
            document = _document;
            urlvalidator = _urlvalidator;
            notaryidvalidator = _notaryidvalidator;
        }

        public int code { get; set; }
        public string msgError { get; set; }
        public string trackId { get; set; }
        public string document { get; set; }
        public string urlvalidator { get; set; }
        public string notaryidvalidator { get; set; }
    }

}
