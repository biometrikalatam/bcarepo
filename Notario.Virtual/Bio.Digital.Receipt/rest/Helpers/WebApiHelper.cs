﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace Bio.Digital.Receipt.rest.Helpers
{
    public class WebApiHelper
    {

        internal static HttpResponseMessage CreateResponse(HttpStatusCode code, object objResponse)
        {
            HttpRequestMessage Request = new HttpRequestMessage();
            return Request.CreateResponse((HttpStatusCode)code, objResponse);
        }
            

    }
}