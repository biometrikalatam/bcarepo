﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using Bio.Digital.Receipt.Api;
using Swashbuckle.Swagger.Annotations;
using Bio.Digital.Receipt.rest.Models.v2021;
using System.IO;
using System.Net.Http.Headers;

namespace Bio.Digital.Receipt.rest.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class PublicPortalController : ApiController
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(PublicPortalController));

        #region Login & Register

        /// <summary>
        /// Permite cheuqeo de login 
        /// </summary>
        /// <remarks>
        /// Dado un user (inicial sera RUT luego podra ser RUT@domain) y password, chequea acceso, y retorna datos de usuario incluido perfil. 
        /// Los pasos que se realizan son:
        ///   * 1.- Chequeo de user, si existe y no está bloqueado
        ///   * 2.- Coincidencia con password
        ///   * 3.- Retorna usuario con sus datos, principalmente el perfil para controlar las acciones en el frontend
        /// </remarks>
        /// <param name="param">User/Password</param>
        /// <returns>Retorna usuario con sus datos, principalmente el perfil para controlar las acciones en el frontend</returns>
        [Route("api/portal/v2/login")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "LoginPublicPortalModelR", typeof(LoginPublicPortalModelR))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "ErrorModel", typeof(ErrorModel))]
        public object LoginPublicPortal([FromBody] LoginPublicPortalModel param)
        {
            NvUsers user = null; 
            string msgerr = null;
            try
            {
                LOG.Debug("NotaryPortalController.LoginPublicPortal - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotaryPortalController.LoginPublicPortal - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                LOG.Debug("NotaryPortalController.LoginPublicPortal - Check parametros...");
                if (param == null || string.IsNullOrEmpty(param.user) || string.IsNullOrEmpty(param.password))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos Complete y reintente...")));
                }
                LoginPublicPortalModel paramIn = param; //JsonConvert.DeserializeObject<LoginRequestModel>(strIn);

                //3) Valido Password //
                LOG.Debug("NotaryPortalController.LoginPublicPortal - Check Login user con mail = " + paramIn.user + "...");
                int ret = AdministradorNvUsers.Login(paramIn.user, paramIn.password, out msgerr, out user);

                if (ret < 0)
                {
                    LOG.Warn("NotaryPortalController.LoginPublicPortal - Error validando login usuario [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            (new ErrorModel(ret, "Error validando login",
                            "Error validando login usuario [" + msgerr + "]. Reintente...")));
                }
                else
                {
                    user.Password = null;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotaryPortalController.LoginPublicPortal Error Desconcido procesando validacion de login : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
                                "Error Desconocido procesando validacion de login [" + ex.Message + "]")));
            }
            LOG.Debug("NotaryPortalController.LoginPublicPortal OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, new LoginPublicPortalModelR(Errors.IRET_OK, user)); //(new ResponseModel(200, "Login verificado con exito!"));

        }

        /// <summary>
        /// Boquea/Desbloquea usuario dado
        /// </summary>
        /// <param name="user"></param>
        /// <param name="action">0 - Desbloquea | 1- Bloquea</param>
        /// <returns></returns>
        [Route("api/portal/v2/reset")]
        [HttpGet]
        public object Reset(string user, int action) //[FromUri] object param)
        {
            string trackId = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotaryPortalController.Reset - IN...");

                //1)Chequeo parametroos
                if (string.IsNullOrEmpty(user))
                {
                    LOG.Debug("NotaryPortalController.Reset - Error parametros de entrada [Null]");
                    return (new ResponseModel(Errors.IRET_API_BAD_REQUEST, "Problemas para verificar registro. Complete y reintente..."));
                }

                //2) Reviso que el usuario esté dado de alta por mail
                NvUsers oUser = AdministradorNvUsers.BuscarNvUserByRut(user);

                if (oUser == null)
                {
                    LOG.Error("NotaryPortalController.Reset - Preregistro NO existe! - Por User = " + user);
                    return (new ResponseModel(Errors.IRET_ERR_PREREGISTER_NOT_EXIST, "Preregistro NO existe. Reinicie el proceso..."));
                }

                //3) Bloqueo o desbloqueo User
                if (action == 1)
                {
                    oUser.DateBlocked = DateTime.Today;
                    oUser.IsBlocked = 1;
                }
                else
                {
                    oUser.DateBlocked = null;
                    oUser.IsBlocked = 0;
                }

                int ret = AdministradorNvUsers.UpdateNvUser(oUser, out msgerr);

                if (ret < 0)
                {
                    LOG.Error("NotaryPortalController.Reset - Error actualizando User = " + user + "!");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ResponseModel(ret, "Error actualizando User = " + user + "!")));
                }


            }
            catch (Exception ex)
            {
                LOG.Error("NotaryPortalController.Reset Error Desconcido procesando validacion de mail : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                    (new ResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconocido procesando validacion de mail ["
                                                  + ex.Message + "]")));
            }
            LOG.Error("NotaryPortalController.Reset OUT!");
            return Request.CreateResponse(HttpStatusCode.BadRequest,
                    (new ResponseModel(200, "Reset con exito!")));

        }

        //[Route("api/notary/v2/users")]
        //[HttpGet]
        //[SwaggerResponse(HttpStatusCode.OK, "LoginPortalNotaryModelR", typeof(LoginPortalNotaryModelR))]
        //[SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        //public object UsersNotaryPortal([FromBody] LoginPortalNotaryModel param)
        //{
        //    NvNotaryUsers user = null;
        //    string msgerr = null;
        //    try
        //    {
        //        LOG.Debug("NotaryPortalController.LoginNotaryPortal - IN...");
        //        //0) Check Basic Authentication
        //        if (!NotarizeController.Authorized(Request, out msgerr))
        //        {
        //            LOG.Warn("NotaryPortalController.LoginNotaryPortal - No Autorizado!");
        //            return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //                (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
        //                                (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
        //        }

        //        //1) Deserializo parámetros     
        //        LOG.Debug("NotaryPortalController.LoginNotaryPortal - Check parametros...");
        //        if (param == null || string.IsNullOrEmpty(param.user) || string.IsNullOrEmpty(param.password))
        //        {
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                      (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos Complete y reintente...")));
        //        }
        //        LoginPortalNotaryModel paramIn = param; //JsonConvert.DeserializeObject<LoginRequestModel>(strIn);

        //        //2) Reviso que el usuario esté dado de alta por mail
        //        //LOG.Debug("NotaryPortalController.LoginNotaryPortal - Recuperando user con mail = " + paramIn.mail + "...");
        //        //user = AdministradorNvNotaryUsers.BuscarNvNotaryUserByMail(paramIn.mail);

        //        //if (user == null)
        //        //{
        //        //    LOG.Error("NotarizeController.Login - Usuarios NO existe! - Por Mail = " + paramIn.mail);
        //        //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //        //        (new ErrorModel(Errors.IRET_ERR_USER_NOT_EXIST, "Acceso NO Autorizado [" +
        //        //                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
        //        //}
        //        //if (user.IsBlocked == 1)
        //        //{
        //        //    LOG.Error("NotarizeController.Login - Usuario bloqueado => " + paramIn.mail);
        //        //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //        //        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Usuario bloqueado", "Usuario bloqueado " +
        //        //                            (user.DateBlocked.HasValue ? "desde el " + user.DateBlocked.Value.ToString("dd/MM/yyyy") : ""))));
        //        //}

        //        //3) Valido Password
        //        LOG.Debug("NotaryPortalController.LoginNotaryPortal - Check Login user con mail = " + paramIn.user + "...");
        //        int ret = AdministradorNvNotaryUsers.Login(paramIn.user, paramIn.password, out msgerr, out user);

        //        if (ret < 0)
        //        {
        //            LOG.Warn("NotaryPortalController.LoginNotaryPortal - Error validando login usuario [" + msgerr + "]");
        //            return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //                    (new ErrorModel(ret, "Error validando login",
        //                    "Error validando login usuario [" + msgerr + "]. Reintente...")));
        //        }
        //        else
        //        {
        //            user.Password = null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("NotaryPortalController.LoginNotaryPortal Error Desconcido procesando validacion de login : " + ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                    (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
        //                        "Error Desconocido procesando validacion de login [" + ex.Message + "]")));
        //    }
        //    LOG.Debug("NotaryPortalController.LoginNotaryPortal OUT!");
        //    return Request.CreateResponse(HttpStatusCode.OK, new LoginPortalNotaryModelR(200, user)); //(new ResponseModel(200, "Login verificado con exito!"));

        //}

        #endregion Login & Register

        #region Procedures

        /// <summary>
        /// Inica proceso de notarizacion, generando la transaccion, y registrando los firmantes. 
        /// Envía una URL del BPWeb para recolección de evidencias a cada firmante, y establece el estado inicial.
        /// </summary>  
        /// <remarks>
        /// Inicia proceso de notarizacion, enviando mail a cada firmante y documento. 
        /// Los pasos que se realizan son:
        ///   * 1.- Chequeo de API user y parametros 
        ///   * 2.- Chequeo de empresa y user que inicia el proceso, que esten habilitados y que el user tenga el perfil de inciio de procedure
        ///   * 3.- Se inicia proceso de CI obtieniendo la URL del BPWeb, y enviando mail a cada firmante.
        ///   * 4.- Se graba info en BD del trámite iniciado, status tramite en pendiente de CI
        ///   * 5.- Se responde el trackid del trámite.
        /// </remarks>
        /// <param name="param">Parametros de entrada</param>
        /// <returns></returns>
        [Route("api/portal/v2/createProcedure")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "NVResponse", typeof(NVResponse))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(Error))]
        public object CreateProcedure([FromBody] CreateProcedureModel param) //string mail, int notaryid, int procedureid
        {
            string trackId = null;
            string msgerr = null;
            NVResponse response = null;
            try
            {
                LOG.Debug("PublicPortalController.CreateProcedure - IN...");
                //* 1.- Chequeo de API user y parametros 
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("PublicPortalController.CreateProcedure - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //Deserializo parámetros y chequeo integridad
                if (param == null || param.iduser == 0 || param.procedureId == 0 || param.notaryId == 0)
                {
                    LOG.Debug("PublicPortalController.CreateProcedure - Parametros nulos o incrrectos [iduser/idprocedure/idnotary]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                                                (new ErrorModel(Errors.IRET_API_BAD_REQUEST, 
                                                "Parametros nulos o incrrectos [iduser/idprocedure/idnotary]. Complete y reintente...", null)));
                }
                CreateProcedureModel paramIn = param;

                if (paramIn == null)
                {
                    LOG.Debug("PublicPortalController.CreateProcedure - Error deserializando parametros de entrada [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                                   (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Error deserializando parametros de entrada [Null]", null)));
                }
                else
                {
                    string msgRet = "";
                    
                    if (!NotaryPortalUnitOfWork.IsParamCreateCorrect(paramIn, out msgRet))  // !string.IsNullOrEmpty(msgRet))
                    {
                        LOG.Debug("PublicPortalController.CreateProcedure - Parametros incorrectos!");
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                                (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                    "Problemas para iniciar el tramite: [" + msgRet + "]", null)));
                    }
                }

                LOG.Debug("PublicPortalController.CreateProcedure - Chequeo company y user que esten habilitados: IdUser = " + paramIn.iduser);
                //* 2.- Chequeo de empresa y user que inicia el proceso, que esten habilitados y que el user tenga el perfil de inciio de procedure
                msgerr = "";
                int code = NotaryPortalUnitOfWork.IsEnabledToCreate(paramIn.iduser, out msgerr);
                if (code < 0)
                {
                    LOG.Error("PublicPortalController.CreateProcedure - Imposible iniciar tramite user sin perfil o compania disabled! [" 
                                + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                                                  (new ErrorModel(code, msgerr, trackId)));
                }

                //2) Genero TraciId Unico de trámite
                //trackId = AdministradorNvUsersProcedures.GetTrackIdUnique();
                //if (string.IsNullOrEmpty(trackId))
                //{
                //    LOG.Error("PublicPortalController.CreateProcedure - TrackId generado invalido [Null]");
                //    return Request.CreateResponse(HttpStatusCode.Conflict,
                //        (new NotarizeResponseModel(Errors.IRET_API_CONFLIC, "Problemas para crear id de transaccion unico. Reintente...",
                //        null)));
                //}

                //   * 3.- Se inicia proceso de CI obtieniendo la URL del BPWeb, y enviando mail a cada firmante.
                code = 0;
                msgerr = "";
                string trackid = null;
                code = NotaryPortalUnitOfWork.CreateProcedure(paramIn, out trackid, out msgerr);

                if (code < 0)
                {
                    LOG.Error("PublicPortalController.CreateProcedure - Sale con error [err=" + code + "]");
                    Request.CreateResponse(HttpStatusCode.Conflict,
                                                  (new ErrorModel(code, "Error creando tramite", msgerr)));
                } else
                {
                    LOG.Error("PublicPortalController.CreateProcedure - Tramite creado [trackid=" + trackid + "]");
                    response = new NVResponse(0, "Tramite Creado con Exito!", trackid);
                }

                //3) Get URL de CI WEB para retornar
                //string sURLCI;
                //string sTrackIdCI = GenerateCIWeb(paramIn, out sURLCI, out msgerr);
                //if (string.IsNullOrEmpty(sURLCI) || string.IsNullOrEmpty(sTrackIdCI))
                //{
                //    LOG.Error("PublicPortalController.CreateProcedure - Error creando TraciIdCI o sURLCI para CI");
                //    return Request.CreateResponse(HttpStatusCode.Conflict,
                //        (new NotarizeResponseModel(Errors.IRET_API_ERR_INIT_CI, "Error creando TraciIdCI o sURLCI para CI", null)));
                //}



                ////4) Grabo NvUsersProcedures con status = 0 => Recibido
                //NvUsersProcedures NvUsersProceduresCreated;
                ////string msgerr;
                //int provideridout;

                //int ret = AdministradorNvUsersProcedures.CreateNvUserProcedure(trackId, _NvUser.Id, paramIn.notaryId, paramIn.procedureId,
                //                                              paramIn.document, paramIn.message, sTrackIdCI, sURLCI,
                //                                              out provideridout, out msgerr, out NvUsersProceduresCreated);

                //if (ret < 0 || NvUsersProceduresCreated == null)
                //{
                //    LOG.Error("PublicPortalController.CreateProcedure - Error creando procedure para iniciar proceso [" + msgerr + "]");
                //    return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                //        (new NotarizeResponseModel(Errors.IRET_ERR_NVPROCEDURE_NO_SAVED, "Error creando procedure para iniciar proceso[" + msgerr + "]",
                //        null)));
                //}

                ////Si todo funciono ok => Genro respuesta para retornar.
                //response = new InitNotarizeResponseModel(Errors.IRET_API_OK, null, trackId, sTrackIdCI, sURLCI);

                ////Informo x mail
                //string[] strbody = new string[2];
                //strbody[0] = "Usted a iniciado el trámite <b>" + NvUsersProceduresCreated.NameProcedure + "</b>";
                //strbody[1] = "El codigo unico del tramite para consultas es: <b>" + NvUsersProceduresCreated.TrackId + "</b>";
                //libs.Utils.NVNotify(paramIn.mail, "Inicio de tramite", strbody, null, null, null);

            }
            catch (Exception ex)
            {
                LOG.Error("PublicPortalController.CreateProcedure Error Iniciando trámite: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    (new ErrorModel(Errors.IRET_API_INTERNAL_SERVER_ERROR * -1, "Error Desconcido procesando inicio de tramite ["
                                                  + ex.Message + "]", null)));
            }

            LOG.Debug("PublicPortalController.CreateProcedure - OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, response);

        }

        /// <summary>
        /// Retorna listado de tramites (NvUserProcedures) que cumplan con el filtro enviado.
        /// </summary>
        /// <remarks>
        /// Los posibles son:
        ///   * 1.- int notaryid: Obligatorio para filtar por la notaria en cuestión. 
        ///   * 2.- string trackid: [Opcional] por si se desea un único procedure (Idem que el uso de api/notary/v2/get) Si este valor no es nulo, se ignora rango de fechas.
        ///   * 3.- string mail: [Opcional] Del user que inicio el trámite
        ///   * 4.- string mailnotary: [Opcional] De un funcionario de la notaria que tomo o reviso el tramite (ver si lo usamos)
        ///   * 5.- DateTime from: [Opcional] Si llega nulo junto a till, se toman 3 dias desde la fecha actual para atras.
        ///   * 6.- DateTime till: [Opcional] Si diferencia entra till - from es mas de 15 dias, se ajusta a 15 dias.
        ///   * 7.- int status: [Opcional] Si se necesita por status, se envia sino 0 para todos.
        ///   * 8.- int withdetails: 0-Liviano | 1-withdetails. Esto incluye las evidencias y sus estados o las elimina. Para grillas generales es recoendable 0-Liviano para que sea mas rápido el proceso.
        ///  
        /// Los estados posibles para filtrar son:
        ///   * int STATUS_MAIL_VERIFY_PENDING = 0
        ///   * int STATUS_CI_PENDING = 1
        ///   * int STATUS_PAYMENT_PENDING = 2
        ///   * int STATUS_READY_TO_REVISION_NOTARIZE = 3
        ///   * int STATUS_READY_TO_NOTARIZE = 4
        ///   * int STATUS_IN_PROCESS_NOTARIZING = 5
        ///   * int STATUS_REJECTED = 6
        ///   * int STATUS_NOTARIZED = 7
        ///   
        ///  Los estados que pueden operar desde este portal son desde el 3 en adelante. Hasta el 3, es solo informativo para este portal. 
        /// </remarks>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/portal/v2/procedure/listExtended")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "NVResponse", typeof(NVResponse))]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object ListProceduresPublicPortalExtended([FromBody] ListProceduresPortalPublicModel param)
        {
            List<NvUsersProcedures> listProcedures = null;
            string msgerr = null;
            List<Procedure> listProceduresRet = null;
            try
            {
                LOG.Debug("PublicPortalController.ListProceduresPublicPortalExtended - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("PublicPortalController.ListProceduresPublicPortalExtended - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                LOG.Debug("PublicPortalController.ListProceduresPublicPortalExtended - Check parametros...");
                if (param == null || param.notaryid <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos o incorrectos. Complete y reintente...",
                                                "Parametros nulos o incorrectos. Complete y reintente..")));
                }

                //2) Chequeo parametros y seteo filtros
                if (param.till == null || param.till == DateTime.MinValue)  //Si viene nuelo ambos o uno diferencia de 3 dias solo
                {
                    param.till = DateTime.Now;
                    param.from = param.till.AddDays(-3);
                } else if (param.till != null && param.from == null)
                {
                    param.from = param.till.AddDays(-3);
                } else if (param.till.AddDays(-15) > param.from)  //No dejo mas de 15 dias de rango
                {
                    param.from = param.till.AddDays(-15);
                }
                ListProceduresPortalPublicModel paramIn = param; //JsonConvert.DeserializeObject<LoginRequestModel>(strIn);

                //3) Recupero procedures
                LOG.Debug("PublicPortalController.ListProceduresPublicPortalExtended - Recupero procedures para NotaryId =  " + 
                            param.notaryid.ToString() + " | Desde = " + param.from.ToString("dd/MM/yyyy HH:mm:ss") + 
                            " - Hasta = " + param.till.ToString("dd/MM/yyyy HH:mm:ss") + " | status = " + paramIn.status.ToString() +
                            " | trackid = " + (string.IsNullOrEmpty(paramIn.trackid)?"Null": paramIn.trackid) +
                            " | mail cliente = " + (string.IsNullOrEmpty(paramIn.trackid) ? "Null" : paramIn.trackid));
                int ret = AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter(paramIn.notaryid, paramIn.userid, paramIn.procedureid,
                                                                                     paramIn.from, paramIn.till, paramIn.trackid,
                                                                                     paramIn.status, out msgerr, out listProcedures, 
                                                                                     (paramIn.withdetails==1));

                if (ret < 0)
                {
                    LOG.Error("PublicPortalController.ListProceduresPublicPortalExtended - Error recuperando tramites [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                            (new ErrorModel(ret, "Error recuperando tramites",
                            "Error recuperando tramites [" + msgerr + "]. Reintente...")));
                } else
                {
                    LOG.Error("PublicPortalController.ListProceduresPublicPortalExtended - Ingresando ConvertList..."); 
                    listProceduresRet = NotaryPortalUnitOfWork.ConvertList(listProcedures, paramIn.withdetails);
                    LOG.Error("PublicPortalController.ListProceduresPublicPortal - ConvertList OUT!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("PublicPortalController.ListProceduresPublicPortalExtended - Error Desconcido procesando recuperacion de tramites : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
                                "Error Desconcido procesando recuperacion de tramites [" + ex.Message + "]")));
            }
            LOG.Debug("PublicPortalController.ListProceduresPublicPortalExtended OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, new NVResponse(0,"Listado recuperado con exito!", listProceduresRet));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/portal/v2/procedure/list")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "NVResponse", typeof(NVResponse))]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object ListProceduresPublicPortal([FromBody] ListProceduresPortalPublicModel param)
        {
            List<NvUsersProcedures> listProcedures = null;
            string msgerr = null;
            List<ListItemProcedure> listProceduresRet = null;
            try
            {
                LOG.Debug("PublicPortalController.ListProceduresPublicPortal - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("PublicPortalController.ListProceduresPublicPortal - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                LOG.Debug("PublicPortalController.ListProceduresPublicPortal - Check parametros...");
                if (param == null || param.notaryid <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos o incorrectos. Complete y reintente...",
                                                "Parametros nulos o incorrectos. Complete y reintente..")));
                }

                //2) Chequeo parametros y seteo filtros
                if (param.till == null || param.till == DateTime.MinValue)  //Si viene nuelo ambos o uno diferencia de 3 dias solo
                {
                    param.till = DateTime.Now;
                    param.from = param.till.AddDays(-3);
                }
                else if (param.till != null && param.from == null)
                {
                    param.from = param.till.AddDays(-3);
                }
                else if (param.till.AddDays(-15) > param.from)  //No dejo mas de 15 dias de rango
                {
                    param.from = param.till.AddDays(-15);
                }
                ListProceduresPortalPublicModel paramIn = param; //JsonConvert.DeserializeObject<LoginRequestModel>(strIn);

                //3) Recupero procedures
                LOG.Debug("PublicPortalController.ListProceduresPublicPortal - Recupero procedures para NotaryId =  " +
                            param.notaryid.ToString() + " | Desde = " + param.from.ToString("dd/MM/yyyy HH:mm:ss") +
                            " - Hasta = " + param.till.ToString("dd/MM/yyyy HH:mm:ss") + " | status = " + paramIn.status.ToString() +
                            " | trackid = " + (string.IsNullOrEmpty(paramIn.trackid) ? "Null" : paramIn.trackid) +
                            " | mail cliente = " + (string.IsNullOrEmpty(paramIn.trackid) ? "Null" : paramIn.trackid));
                int ret = AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter(paramIn.notaryid, paramIn.userid, paramIn.procedureid,
                                                                                     paramIn.from, paramIn.till, paramIn.trackid,
                                                                                     paramIn.status, out msgerr, out listProcedures,
                                                                                     (paramIn.withdetails == 1));

                if (ret < 0)
                {
                    LOG.Error("PublicPortalController.ListProceduresPublicPortal - Error recuperando tramites [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                            (new ErrorModel(ret, "Error recuperando tramites",
                            "Error recuperando tramites [" + msgerr + "]. Reintente...")));
                }
                else
                {
                    LOG.Error("PublicPortalController.ListProceduresPublicPortalExtended - Ingresando ConvertList...");
                    listProceduresRet = NotaryPortalUnitOfWork.ConvertListReturn(listProcedures);
                    LOG.Error("PublicPortalController.ListProceduresPublicPortal - ConvertList OUT!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("PublicPortalController.ListProceduresPublicPortal - Error Desconcido procesando recuperacion de tramites : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
                                "Error Desconcido procesando recuperacion de tramites [" + ex.Message + "]")));
            }
            LOG.Debug("PublicPortalController.ListProceduresPublicPortal OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, new NVResponse(0, "Listado recuperado con exito!", listProceduresRet));
        }

        /// <summary>
        /// Recupera los datos de un procedure
        /// </summary>
        /// <remarks>
        /// Recupera los datos de un procedure con detalles (lista de evidencias y estados incluido). Es el mismo resultado 
        /// que si se usa api/notary/v2/list y se envia como filtro el trackid como filtro.
        /// </remarks>
        /// <param name="trackid"></param>
        /// <returns></returns>
        [Route("api/portal/v2/procedure/get")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "NVResponse", typeof(NVResponse))]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object GetProcedure(string trackid)
        {
            int ret = 0;
            List<NvUsersProcedures> listProcedures = null;
            string msgerr = null;
            List<Procedure> listProceduresRet = null;
            try
            {
                LOG.Debug("PublicPortalController.GetProcedure - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("PublicPortalController.GetProcedure - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                LOG.Debug("PublicPortalController.GetProcedure - Check parametros...");
                if (string.IsNullOrEmpty(trackid))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos o incorrectos. Complete y reintente...",
                                                "Parametros nulos o incorrectos. Complete y reintente..")));
                }

                //3) Recupero procedures
                LOG.Debug("PublicPortalController.GetProcedure - Recupero procedures con trackid = " + trackid);
                NvUsersProcedures proc = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr, true);

                if (proc == null)
                {
                    ret = Errors.IRET_ERR_PROCEDURE_NOTFOUND;
                    LOG.Error("PublicPortalController.GetProcedure - Tramite no recuperado [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                            (new ErrorModel(ret, "Tramite no recuperado",
                            (string.IsNullOrEmpty(msgerr)?"Tramite no existe.Reintente..." : 
                                                          "Se ha producido un error [" + msgerr + "]. Reintente..."))));
                }
                else
                {
                    LOG.Error("PublicPortalController.GetProcedure - Ingresando ConvertList...");
                    listProcedures = new List<NvUsersProcedures>();
                    listProcedures.Add(proc);
                    listProceduresRet = NotaryPortalUnitOfWork.ConvertList(listProcedures);
                    LOG.Error("PublicPortalController.GetProcedure - ConvertList OUT!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("PublicPortalController.GetProcedure - Error Desconcido procesando recuperacion de tramites : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
                                "Error Desconcido procesando recuperacion de tramites [" + ex.Message + "]")));
            }
            LOG.Debug("PublicPortalController.GetProcedure OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, new NVResponse(Errors.IRET_API_OK, "No existen errores!", listProceduresRet));
        }

        /// <summary>
        /// Dado un trackid, se coloca en baja logica al trámite (Status = -1 => Cancelled)
        /// </summary>
        /// <param name="trackid"></param>
        /// <returns></returns>
        [Route("api/portal/v2/procedure/delete")]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, "NVResponse", typeof(NVResponse))]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object DeleteProcedure(string trackid)
        {
            int ret = 0;
            string msgerr = null;
            try
            {
                LOG.Debug("PublicPortalController.DeleteProcedure - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("PublicPortalController.DeleteProcedure - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                LOG.Debug("PublicPortalController.DeleteProcedure - Check parametros...");
                if (string.IsNullOrEmpty(trackid))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos o incorrectos. Complete y reintente...",
                                                "Parametros nulos o incorrectos. Complete y reintente..")));
                }

                //3) Recupero procedures
                LOG.Debug("PublicPortalController.DeleteProcedure - Recupero procedures con trackid = " + trackid);
                NvUsersProcedures proc = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr, true);

                if (proc == null)
                {
                    ret = Errors.IRET_ERR_PROCEDURE_NOTFOUND;
                    LOG.Error("PublicPortalController.DeleteProcedure - Tramite no recuperado [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                            (new ErrorModel(ret, "Tramite no recuperado",
                            (string.IsNullOrEmpty(msgerr) ? "Tramite no existe.Reintente..." :
                                                          "Se ha producido un error [" + msgerr + "]. Reintente..."))));
                }
                else
                {
                    LOG.Error("PublicPortalController.GetProcedure - Ingresando ConvertList...");
                    if (!NotaryPortalUnitOfWork.DeleteLogicProcedure(proc, out proc))
                    {
                        LOG.Error("PublicPortalController.DeleteProcedure - Error realizando delete logico...");
                        return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_SAVE_DB, "Error realizando cancelacion de tramite!",
                                "Error realizando cancelacion de tramite!")));
                    } 
                }
            }
            catch (Exception ex)
            {
                LOG.Error("PublicPortalController.GetProcedure - Error Desconcido procesando recuperacion de tramites : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
                                "Error Desconcido procesando recuperacion de tramites [" + ex.Message + "]")));
            }
            LOG.Debug("PublicPortalController.GetProcedure OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, new NVResponse(Errors.IRET_OK, "Tramite eliminado con exito!", trackid));
        }

        /// <summary>
        /// Ofrce descarga de PDF. Si ya está listo entrega el documento Notarizado, sino el actual hasta ese momento. 
        /// </summary>
        /// <param name="trackid"></param>
        /// <returns></returns>
        [Route("api/portal/v2/procedure/download")]
        [HttpGet]
        public object DownloadProcedure(string trackid)
        {
            string msgerr;
            try
            {
                LOG.Debug("PublicPortalController.DownloadProcedure - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotaryPortalController.ModifyRevisionProcedure - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                if (string.IsNullOrEmpty(trackid))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new ErrorModel(Errors.IRET_API_BAD_REQUEST,
                        "TrackId consultado no puede ser nulo", trackid)));
                }

                //2) Recupero tramite y si está statis = Notarzed => Devuelvo sino msg error
                NvUsersProcedures nvUP = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr);

                if (nvUP == null)
                {
                    LOG.Error("PublicPortalController.DownloadProcedure Error: TrackId consultado no existe");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        (new ErrorModel(Errors.IRET_API_ERR_NOT_FOUND,
                        "TrackId consultado no existe", trackid)));
                }
                //else if (nvUP.Status != StatusNvUsersProcedures.STATUS_NOTARIZED)
                //{
                //    LOG.Error("PublicPortalController.DownloadProcedure Error: TrackId consultado aun pendiente de notarizacion [Status=" + nvUP.Status + "]");
                //    return Request.CreateResponse(HttpStatusCode.PartialContent,
                //        (new ErrorModel(Errors.IRET_ERR_PROCEDURE_PENDING,
                //        "TrackId consultado aun pendiente de notarizacion [Status=" + nvUP.Status + "]", trackid)));
                //}
                //else if (string.IsNullOrEmpty(nvUP.DocumentB64Notarized))
                //{
                //    LOG.Error("PublicPortalController.DownloadProcedure Error: TrackId consultado con status ok pero documento inexistente [" + trackid + "]");
                //    return Request.CreateResponse(HttpStatusCode.Conflict,
                //        (new ErrorModel(Errors.IRET_ERR_PROCEDURE_PENDING,
                //        "TrackId consultado con status ok pero documento inexistente", trackid)));
                //}
                else  //Todo ok => Armo salida 
                {
                    //string sfile = System.IO.File.ReadAllText(@"C:\tmp\doc.base64.txt");
                    LOG.Debug("PublicPortalController.DownloadProcedure - Descargando " + nvUP.TrackId + ".pdf");
                    byte[] byFile; 

                    if (string.IsNullOrEmpty(nvUP.DocumentB64Notarized))
                    {
                        LOG.Debug("PublicPortalController.DownloadProcedure - Descargando tramite actual, aun no Notarizado...");
                        byFile = Convert.FromBase64String(nvUP.DocumentB64);
                    } else
                    {
                        LOG.Debug("PublicPortalController.DownloadProcedure - Descargando tramite Notarizado...");
                        byFile = Convert.FromBase64String(nvUP.DocumentB64Notarized);
                    }
                    
                    MemoryStream ms = new MemoryStream(byFile);
                    //var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/test.docx"); ;
                    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                    //var stream =    new FileStream(path, FileMode.Open);
                    result.Content = new StreamContent(ms); // (stream);
                    //result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    //result.Content.Headers.ContentDisposition.FileName = nvUP.TrackId + ".pdf"; // Path.GetFileName(path);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.ContentLength = ms.Length;// stream.Length;
                    LOG.Debug("PublicPortalController.DownloadProcedure - Descarga OK!");
                    return result;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("PublicPortalController.DownloadProcedure Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                       (new ErrorModel(Errors.IRET_API_INTERNAL_SERVER_ERROR,
                       "Error descargadno tramite [" + ex.Message + "]", trackid)));
            }
        }

        /// <summary>
        /// Modifica el procedure para Aprobacion o Rechazo por parte del funcionario.
        /// </summary>
        /// <remarks>
        /// Se envía el trackid del procedur, status de aprobado o rechazado y el texto adecuado segun status:
        /// * 1.- string trackid: Obligatorio
        /// * 2.- int status: 1-Aprobado | 2-Rechazado
        /// * 3.- string texttoinclude: Texto para incluir en documento generado si status = 1
        /// * 4.- string reasonreject: Motivo del rechazo si status = 2
        /// </remarks>
        /// <param name="param"></param>
        /// <returns></returns>
        //[Route("api/notary/v2/modifyrevision")]
        //[HttpPost]
        //[SwaggerResponse(HttpStatusCode.OK)]
        //[SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        //public object ModifyRevisionProcedure([FromBody] ModifyProcedureNotaryModel param)
        //{
        //    int ret = Errors.IRET_ERR_DESCONOCIDO;
        //    List<NvUsersProcedures> listProcedures = null;
        //    string msgerr = null;
        //    List<Procedure> listProceduresRet = null;
        //    try
        //    {
        //        LOG.Debug("NotaryPortalController.ModifyRevisionProcedure - IN...");
        //        //0) Check Basic Authentication
        //        if (!NotarizeController.Authorized(Request, out msgerr))
        //        {
        //            LOG.Warn("NotaryPortalController.ModifyRevisionProcedure - No Autorizado!");
        //            return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //                (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
        //                                (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
        //        }

        //        //1) Deserializo parámetros y controlo consistencia  
        //        LOG.Debug("NotaryPortalController.ModifyRevisionProcedure - Check parametros...");
        //        if (param == null || (param.status != 1 && param.status != 2))
        //        {
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                      (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametro nulo o incorrectos. Complete y reintente...",
        //                                        "Parametro nulo o incorrectos. Complete y reintente..")));
        //        } else if ((param.status == 1 && string.IsNullOrEmpty(param.texttoinclude)) ||
        //                   (param.status == 2 && string.IsNullOrEmpty(param.reasonreject)))
        //        {
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                      (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros incorrectos. Complete y reintente...",
        //                                        "Parametros incorrectos. Complete y reintente..")));
        //        }

        //        //3) Recupero procedures
        //        LOG.Debug("NotaryPortalController.ModifyRevisionProcedure - Modifico procedures con trackid = " + param.trackid);
        //        ret = NotaryPortalUnitOfWork.ModifyRevisionProcedure(param, out msgerr);

        //        if (ret < 0)
        //        {
        //            LOG.Error("NotaryPortalController.ModifyRevisionProcedure - Error modificando tramite con trackid = "
        //                       + param.trackid + " [" + msgerr + "]");
        //            return Request.CreateResponse(HttpStatusCode.Conflict,
        //                    (new ErrorModel(ret, "Error recuperando tramites",
        //                    "Error recuperando tramites [" + msgerr + "]. Reintente...")));
        //        }
        //        //else
        //        //{
        //        //    LOG.Error("NotaryPortalController.ListProceduresPortalNotary - Ingresando ConvertList...");
        //        //    listProceduresRet = NotaryPortalUnitOfWork.ConvertList(listProcedures);
        //        //    LOG.Error("NotaryPortalController.ListProceduresPortalNotary - ConvertList OUT!");
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = Errors.IRET_ERR_DESCONOCIDO;
        //        LOG.Error("NotaryPortalController.ModifyRevisionProcedure - Error Desconcido procesando modificacion de tramite desde notaria : " + ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                    (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
        //                        "Error Desconcido procesando modificacion de tramite desde notaria [" + ex.Message + "]")));
        //    }
        //    LOG.Debug("NotaryPortalController.ModifyRevisionProcedure OUT!");
        //    return Request.CreateResponse(HttpStatusCode.OK);
        //}






        #endregion Procedures

        #region basico
        // GET: api/NotaryPortal
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/NotaryPortal/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/NotaryPortal
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/NotaryPortal/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/NotaryPortal/5
        //public void Delete(int id)
        //{
        //}
        #endregion basico
    }
}
