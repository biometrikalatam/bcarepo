﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using Newtonsoft.Json;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database.Notary.config;
using Bio.Digital.Receipt.Core.Helpers;
using System.Xml;
using System.Net.Http.Headers;
using System.Collections;
using System.IO;
using Bio.Digital.Receipt.Core.Services;
using Swashbuckle.Swagger.Annotations;
using Bio.Digital.Receipt.rest.Models.v2021;

namespace Bio.Digital.Receipt.rest.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class NotaryPortalController : ApiController
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NotaryPortalController));

        #region Login & Register

        /// <summary>
        /// Permite cheuqeo de login 
        /// </summary>
        /// <remarks>
        /// Dado un user (inicial sera RUT luego podra ser RUTdomain) y password, chequea acceso, y retorna datos de usuario incluido perfil. 
        /// Los pasos que se realizan son:
        ///   * 1.- Chequeo de user, si existe y no está bloqueado
        ///   * 2.- Coincidencia con password
        ///   * 3.- Retorna usuario con sus datos, principalmente el perfil para controlar las acciones en el frontend
        /// </remarks>
        /// <param name="param">User/Password</param>
        /// <returns>Retorna usuario con sus datos, principalmente el perfil para controlar las acciones en el frontend</returns>
        [Route("api/notary/v2/login")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "LoginPortalNotaryModelR", typeof(LoginPortalNotaryModelR))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "ErrorModel", typeof(ErrorModel))]
        public object LoginNotaryPortal([FromBody] LoginPortalNotaryModel param)
        {
            NvNotaryUsers user = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotaryPortalController.LoginNotaryPortal - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotaryPortalController.LoginNotaryPortal - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                LOG.Debug("NotaryPortalController.LoginNotaryPortal - Check parametros...");
                if (param == null || string.IsNullOrEmpty(param.user) || string.IsNullOrEmpty(param.password))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos Complete y reintente...")));
                }
                LoginPortalNotaryModel paramIn = param; //JsonConvert.DeserializeObject<LoginRequestModel>(strIn);
           
                //3) Valido Password
                LOG.Debug("NotaryPortalController.LoginNotaryPortal - Check Login user con mail = " + paramIn.user + "...");
                int ret = AdministradorNvNotaryUsers.Login(paramIn.user, paramIn.password, out msgerr, out user);

                if (ret < 0)
                {
                    LOG.Warn("NotaryPortalController.LoginNotaryPortal - Error validando login usuario [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            (new ErrorModel(ret, "Error validando login",
                            "Error validando login usuario [" + msgerr + "]. Reintente...")));
                }
                else
                {
                    user.Password = null;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotaryPortalController.LoginNotaryPortal Error Desconcido procesando validacion de login : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
                                "Error Desconocido procesando validacion de login [" + ex.Message + "]")));
            }
            LOG.Debug("NotaryPortalController.LoginNotaryPortal OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, new LoginPortalNotaryModelR(Errors.IRET_OK, user)); //(new ResponseModel(200, "Login verificado con exito!"));

        }

        /// <summary>
        /// Boquea/Desbloquea usuario dado
        /// </summary>
        /// <param name="user"></param>
        /// <param name="action">0 - Desbloquea | 1- Bloquea</param>
        /// <returns></returns>
        [Route("api/notary/v2/reset")]
        [HttpGet]
        public object Reset(string user, int action) //[FromUri] object param)
        {
            string trackId = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotaryPortalController.Reset - IN...");

                //1)Chequeo parametroos
                if (string.IsNullOrEmpty(user))
                {
                    LOG.Debug("NotaryPortalController.Reset - Error parametros de entrada [Null]");
                    return (new ResponseModel(Errors.IRET_API_BAD_REQUEST, "Problemas para verificar registro. Complete y reintente..."));
                }

                //2) Reviso que el usuario esté dado de alta por mail
                NvNotaryUsers oUser = AdministradorNvNotaryUsers.BuscarNvNotaryUserByRut(user);

                if (oUser == null)
                {
                    LOG.Error("NotarizeController.Reset - Preregistro NO existe! - Por User = " + user);
                    return (new ResponseModel(Errors.IRET_ERR_PREREGISTER_NOT_EXIST, "Preregistro NO existe. Reinicie el proceso..."));
                }

                //3) Bloqueo o desbloqueo User
                if (action == 1)
                {
                    oUser.DateBlocked = DateTime.Today;
                    oUser.IsBlocked = 1;
                } else
                {
                    oUser.DateBlocked = null;
                    oUser.IsBlocked = 0;
                }

                int ret = AdministradorNvNotaryUsers.UpdateNvNotaryUser(oUser, out msgerr);

                if (ret < 0)
                {
                    LOG.Error("NotarizeController.Reset - Error actualizando User = " + user + "!");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ResponseModel(ret, "Error actualizando User = " + user + "!")));
                }


            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.Reset Error Desconcido procesando validacion de mail : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, 
                    (new ResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconocido procesando validacion de mail ["
                                                  + ex.Message + "]")));
            }
            LOG.Error("NotarizeController.Reset OUT!");
            return Request.CreateResponse(HttpStatusCode.BadRequest, 
                    (new ResponseModel(200, "Reset con exito!"))); 

        }

        //[Route("api/notary/v2/users")]
        //[HttpGet]
        //[SwaggerResponse(HttpStatusCode.OK, "LoginPortalNotaryModelR", typeof(LoginPortalNotaryModelR))]
        //[SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        //public object UsersNotaryPortal([FromBody] LoginPortalNotaryModel param)
        //{
        //    NvNotaryUsers user = null;
        //    string msgerr = null;
        //    try
        //    {
        //        LOG.Debug("NotaryPortalController.LoginNotaryPortal - IN...");
        //        //0) Check Basic Authentication
        //        if (!NotarizeController.Authorized(Request, out msgerr))
        //        {
        //            LOG.Warn("NotaryPortalController.LoginNotaryPortal - No Autorizado!");
        //            return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //                (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
        //                                (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
        //        }

        //        //1) Deserializo parámetros     
        //        LOG.Debug("NotaryPortalController.LoginNotaryPortal - Check parametros...");
        //        if (param == null || string.IsNullOrEmpty(param.user) || string.IsNullOrEmpty(param.password))
        //        {
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                      (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos Complete y reintente...")));
        //        }
        //        LoginPortalNotaryModel paramIn = param; //JsonConvert.DeserializeObject<LoginRequestModel>(strIn);

        //        //2) Reviso que el usuario esté dado de alta por mail
        //        //LOG.Debug("NotaryPortalController.LoginNotaryPortal - Recuperando user con mail = " + paramIn.mail + "...");
        //        //user = AdministradorNvNotaryUsers.BuscarNvNotaryUserByMail(paramIn.mail);

        //        //if (user == null)
        //        //{
        //        //    LOG.Error("NotarizeController.Login - Usuarios NO existe! - Por Mail = " + paramIn.mail);
        //        //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //        //        (new ErrorModel(Errors.IRET_ERR_USER_NOT_EXIST, "Acceso NO Autorizado [" +
        //        //                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
        //        //}
        //        //if (user.IsBlocked == 1)
        //        //{
        //        //    LOG.Error("NotarizeController.Login - Usuario bloqueado => " + paramIn.mail);
        //        //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //        //        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Usuario bloqueado", "Usuario bloqueado " +
        //        //                            (user.DateBlocked.HasValue ? "desde el " + user.DateBlocked.Value.ToString("dd/MM/yyyy") : ""))));
        //        //}

        //        //3) Valido Password
        //        LOG.Debug("NotaryPortalController.LoginNotaryPortal - Check Login user con mail = " + paramIn.user + "...");
        //        int ret = AdministradorNvNotaryUsers.Login(paramIn.user, paramIn.password, out msgerr, out user);

        //        if (ret < 0)
        //        {
        //            LOG.Warn("NotaryPortalController.LoginNotaryPortal - Error validando login usuario [" + msgerr + "]");
        //            return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //                    (new ErrorModel(ret, "Error validando login",
        //                    "Error validando login usuario [" + msgerr + "]. Reintente...")));
        //        }
        //        else
        //        {
        //            user.Password = null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("NotaryPortalController.LoginNotaryPortal Error Desconcido procesando validacion de login : " + ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //                    (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
        //                        "Error Desconocido procesando validacion de login [" + ex.Message + "]")));
        //    }
        //    LOG.Debug("NotaryPortalController.LoginNotaryPortal OUT!");
        //    return Request.CreateResponse(HttpStatusCode.OK, new LoginPortalNotaryModelR(200, user)); //(new ResponseModel(200, "Login verificado con exito!"));

        //}




        #endregion Login & Register

        #region Procedures

        /// <summary>
        /// Retorna listado de tramites (NvUserProcedures) que cumplan con el filtro enviado.
        /// </summary>
        /// <remarks>
        /// Los posibles son:
        ///   * 1.- int notaryid: Obligatorio para filtar por la notaria en cuestión. 
        ///   * 2.- string trackid: [Opcional] por si se desea un único procedure (Idem que el uso de api/notary/v2/get) Si este valor no es nulo, se ignora rango de fechas.
        ///   * 3.- string mail: [Opcional] Del user que inicio el trámite
        ///   * 4.- string mailnotary: [Opcional] De un funcionario de la notaria que tomo o reviso el tramite (ver si lo usamos)
        ///   * 5.- DateTime from: [Opcional] Si llega nulo junto a till, se toman 3 dias desde la fecha actual para atras.
        ///   * 6.- DateTime till: [Opcional] Si diferencia entra till - from es mas de 15 dias, se ajusta a 15 dias.
        ///   * 7.- int status: [Opcional] Si se necesita por status, se envia sino 0 para todos.
        ///   * 8.- int withdetails: 0-Liviano | 1-withdetails. Esto incluye las evidencias y sus estados o las elimina. Para grillas generales es recoendable 0-Liviano para que sea mas rápido el proceso.
        ///  
        /// Los estados posibles para filtrar son:
        ///   * int STATUS_MAIL_VERIFY_PENDING = 0
        ///   * int STATUS_CI_PENDING = 1
        ///   * int STATUS_PAYMENT_PENDING = 2
        ///   * int STATUS_READY_TO_REVISION_NOTARIZE = 3
        ///   * int STATUS_READY_TO_NOTARIZE = 4
        ///   * int STATUS_IN_PROCESS_NOTARIZING = 5
        ///   * int STATUS_REJECTED = 6
        ///   * int STATUS_NOTARIZED = 7
        ///   
        ///  Los estados que pueden operar desde este portal son desde el 3 en adelante. Hasta el 3, es solo informativo para este portal. 
        /// </remarks>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/notary/v2/list")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "ListProceduresNotaryModelR", typeof(ListProceduresNotaryModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object ListProceduresPortalNotary([FromBody] ListProceduresNotaryModel param)
        {
            List<NvUsersProcedures> listProcedures = null;
            string msgerr = null;
            List<Procedure> listProceduresRet = null;
            try
            {
                LOG.Debug("NotaryPortalController.ListProceduresPortalNotary - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotaryPortalController.ListProceduresPortalNotary - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                LOG.Debug("NotaryPortalController.ListProceduresPortalNotary - Check parametros...");
                if (param == null || param.notaryid <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos o incorrectos. Complete y reintente...",
                                                "Parametros nulos o incorrectos. Complete y reintente..")));
                }

                //2) Chequeo parametros y seteo filtros
                if (param.till == null || param.till == DateTime.MinValue)  //Si viene nuelo ambos o uno diferencia de 3 dias solo
                {
                    param.till = DateTime.Now;
                    param.from = param.till.AddDays(-3);
                } else if (param.till != null && param.from == null)
                {
                    param.from = param.till.AddDays(-3);
                } else if (param.till.AddDays(-15) > param.from)  //No dejo mas de 15 dias de rango
                {
                    param.from = param.till.AddDays(-15);
                }
                ListProceduresNotaryModel paramIn = param; //JsonConvert.DeserializeObject<LoginRequestModel>(strIn);

                //3) Recupero procedures
                LOG.Debug("NotaryPortalController.ListProceduresPortalNotary - Recupero procedures para NotaryId =  " + 
                            param.notaryid.ToString() + " | Desde = " + param.from.ToString("dd/MM/yyyy HH:mm:ss") + 
                            " - Hasta = " + param.till.ToString("dd/MM/yyyy HH:mm:ss") + " | status = " + paramIn.status.ToString() +
                            " | trackid = " + (string.IsNullOrEmpty(paramIn.trackid)?"Null": paramIn.trackid) +
                            " | mail cliente = " + (string.IsNullOrEmpty(paramIn.trackid) ? "Null" : paramIn.trackid));
                int ret = AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter(paramIn.notaryid, paramIn.mailnotary, 
                                                                paramIn.mail, paramIn.from, paramIn.till, paramIn.trackid,
                                                                paramIn.status, out msgerr, out listProcedures, 
                                                                (paramIn.withdetails==1));

                if (ret < 0)
                {
                    LOG.Error("NotaryPortalController.ListProceduresPortalNotary - Error recuperando tramites [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                            (new ErrorModel(ret, "Error recuperando tramites",
                            "Error recuperando tramites [" + msgerr + "]. Reintente...")));
                } else
                {
                    LOG.Error("NotaryPortalController.ListProceduresPortalNotary - Ingresando ConvertList..."); 
                    listProceduresRet = NotaryPortalUnitOfWork.ConvertList(listProcedures, paramIn.withdetails);
                    LOG.Error("NotaryPortalController.ListProceduresPortalNotary - ConvertList OUT!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotaryPortalController.ListProceduresPortalNotary - Error Desconcido procesando recuperacion de tramites : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
                                "Error Desconcido procesando recuperacion de tramites [" + ex.Message + "]")));
            }
            LOG.Debug("NotaryPortalController.ListProceduresPortalNotary OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, new ListProceduresNotaryModelR(listProceduresRet));
        }

        /// <summary>
        /// Recupera los datos de un procedure
        /// </summary>
        /// <remarks>
        /// Recupera los datos de un procedure con detalles (lista de evidencias y estados incluido). Es el mismo resultado 
        /// que si se usa api/notary/v2/list y se envia como filtro el trackid como filtro.
        /// </remarks>
        /// <param name="trackid"></param>
        /// <returns></returns>
        [Route("api/notary/v2/get")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "ListProceduresNotaryModelR", typeof(ListProceduresNotaryModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object GetProcedure(string trackid)
        {
            int ret = 0;
            List<NvUsersProcedures> listProcedures = null;
            string msgerr = null;
            List<Procedure> listProceduresRet = null;
            try
            {
                LOG.Debug("NotaryPortalController.GetProcedure - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotaryPortalController.GetProcedure - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                LOG.Debug("NotaryPortalController.GetProcedure - Check parametros...");
                if (string.IsNullOrEmpty(trackid))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos o incorrectos. Complete y reintente...",
                                                "Parametros nulos o incorrectos. Complete y reintente..")));
                }

                //3) Recupero procedures
                LOG.Debug("NotaryPortalController.GetProcedure - Recupero procedures con trackid = " + trackid);
                NvUsersProcedures proc = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr, true);

                if (proc == null)
                {
                    ret = -1;
                    LOG.Error("NotaryPortalController.GetProcedure - Error recuperando tramites [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            (new ErrorModel(ret, "Error recuperando tramites",
                            "Error recuperando tramites [" + msgerr + "]. Reintente...")));
                }
                else
                {
                    LOG.Error("NotaryPortalController.GetProcedure - Ingresando ConvertList...");
                    listProcedures = new List<NvUsersProcedures>();
                    listProcedures.Add(proc);
                    listProceduresRet = NotaryPortalUnitOfWork.ConvertList(listProcedures);
                    LOG.Error("NotaryPortalController.GetProcedure - ConvertList OUT!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotaryPortalController.GetProcedure - Error Desconcido procesando recuperacion de tramites : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
                                "Error Desconcido procesando recuperacion de tramites [" + ex.Message + "]")));
            }
            LOG.Debug("NotaryPortalController.GetProcedure OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, new ListProceduresNotaryModelR(listProceduresRet));
        }

        /// <summary>
        /// Modifica el procedure para Aprobacion o Rechazo por parte del funcionario.
        /// </summary>
        /// <remarks>
        /// Se envía el trackid del procedur, status de aprobado o rechazado y el texto adecuado segun status:
        /// * 1.- string trackid: Obligatorio
        /// * 2.- int status: 1-Aprobado | 2-Rechazado
        /// * 3.- string texttoinclude: Texto para incluir en documento generado si status = 1
        /// * 4.- string reasonreject: Motivo del rechazo si status = 2
        /// </remarks>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/notary/v2/modifyrevision")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "NVResponse", typeof(NVResponse))]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object ModifyRevisionProcedure([FromBody] ModifyProcedureNotaryModel param)
        {
            int ret = Errors.IRET_ERR_DESCONOCIDO;
            List<NvUsersProcedures> listProcedures = null;
            string msgerr = null;
            List<Procedure> listProceduresRet = null;
            try
            {
                LOG.Debug("NotaryPortalController.ModifyRevisionProcedure - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotaryPortalController.ModifyRevisionProcedure - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros y controlo consistencia  
                LOG.Debug("NotaryPortalController.ModifyRevisionProcedure - Check parametros...");
                if (param == null || (param.status != 1 && param.status != 2))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametro nulo o incorrectos. Complete y reintente...",
                                                "Parametro nulo o incorrectos. Complete y reintente..")));
                } else if ((param.status == 1 && string.IsNullOrEmpty(param.texttoinclude)) ||
                           (param.status == 2 && string.IsNullOrEmpty(param.reasonreject)))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros incorrectos. Complete y reintente...",
                                                "Parametros incorrectos. Complete y reintente..")));
                }

                //3) Recupero procedures
                LOG.Debug("NotaryPortalController.ModifyRevisionProcedure - Modifico procedures con trackid = " + param.trackid);
                ret = NotaryPortalUnitOfWork.ModifyRevisionProcedure(param, out msgerr);

                if (ret < 0)
                {
                    LOG.Error("NotaryPortalController.ModifyRevisionProcedure - Error modificando tramite con trackid = "
                               + param.trackid + " [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                            (new ErrorModel(ret, "Error recuperando tramites",
                            "Error recuperando tramites [" + msgerr + "]. Reintente...")));
                }
                //else
                //{
                //    LOG.Error("NotaryPortalController.ListProceduresPortalNotary - Ingresando ConvertList...");
                //    listProceduresRet = NotaryPortalUnitOfWork.ConvertList(listProcedures);
                //    LOG.Error("NotaryPortalController.ListProceduresPortalNotary - ConvertList OUT!");
                //}
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NotaryPortalController.ModifyRevisionProcedure - Error Desconcido procesando modificacion de tramite desde notaria : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
                                "Error Desconcido procesando modificacion de tramite desde notaria [" + ex.Message + "]")));
            }
            LOG.Debug("NotaryPortalController.ModifyRevisionProcedure OUT!");
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        //[Route("api/notary/v2/download")]
        //[HttpGet]
        //[SwaggerResponse(HttpStatusCode.OK, "ProceduresPortalNotaryModelR", typeof(ProceduresPortalNotaryModelR))]
        //[SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(Error))]
        //public object Download([FromBody] ProceduresPortalNotaryModel param)
        //{

        //}

        #region v2021

        /// <summary>
        /// Lista tramites disponibles para ofrecer
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/notary/v2/procedure/getAvailables")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "NVResponse", typeof(NVResponse))]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object GetProceduresAvailables()
        {
            string msgerr;
            List<NvProceduresAvailablesModel> listPA = null;
            try
            {
                LOG.Debug("NotaryPortalController.GetProceduresAvailables - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotaryPortalController.ModifyRevisionProcedure - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //2) Recupero Procedures desde NvConfig ahora
                listPA = AdministradorNvConfig.GetNvProceduresAvailables();
                if (listPA == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, 
                            (new ErrorModel(Errors.IRET_ERR_PROCEDURES_NO_CONFIGURED,
                                Errors.SRET_ERR_PROCEDURES_NO_CONFIGURED, null)));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotaryPortalController.Notarized Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error recuperando Tramites Disponibles ["
                                                  + ex.Message + "]", null)));
            }
            return Request.CreateResponse(HttpStatusCode.OK, 
                                          new NVResponse(Errors.IRET_OK, "Listado de tramites obtenido con exito!", listPA));
        }

        /// <summary>
        /// Retorna status del tramite, consultando cada servicio externo relacionado, como CI, FLow y Vonage 
        /// para actualizar lo que corresponda.
        /// </summary>
        /// <param name="trackid"></param>
        /// <returns></returns>
        [Route("api/notary/v2/procedure/status")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "NVResponse", typeof(NVResponse))]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object GetStatus(string trackid)
        {
            string trackId = null;
            string msgerr = null;
            bool _isMailValidated = false;
            try
            {
                LOG.Debug("NotaryPortalController.GetStatus - IN...[trackid=" + trackid + "]");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotaryPortalController.GetStatus - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                if (string.IsNullOrEmpty(trackid))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                                    (new ErrorModel(Errors.IRET_API_BAD_REQUEST,
                                    "TrackId consultado no puede ser nulo", trackid)));
                }

                //2) Recupero NvProcedure
                LOG.Debug("NotaryPortalController.GetStatus - Recuperando NvUserProcedure(" + trackid + ")...");
                NvUsersProcedures _Procedure = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr, true);
                bool bRetrieveAgain = false;
                //Si es esta en estado Notarizando = 4 => chequeo en linea con notario para determinar si ya tenemos respuesta.
                if (_Procedure != null)
                {
                    LOG.Debug("NotaryPortalController.GetStatus - Procedure recuperado [Status = "
                                                + _Procedure.Status.ToString() + "]. Inicia proceso...");
                    //Chequeo verificacion 
                    _isMailValidated = true; // AdministradorNvUsers.IsUserEnabled(procedure.IdUser);

                    //Si esta en status pendietne de validacion de mail => igual lo dejo hacer la CI en paralelo
                    if (_Procedure.Status == StatusNvUsersProcedures.STATUS_CI_PENDING ||
                        _Procedure.Status == StatusNvUsersProcedures.STATUS_MAIL_VERIFY_PENDING)
                    {
                        //1.- Recupero 1 o mas CI pendientes de finalizacion relacionados con el Tramite
                        //List<string> listCIforProcedure =
                        //    AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByIdProcedure(procedure.Id);

                        //1.- Recupero 1 o mas Signtorories para verificar su status
                        IList<NvUsersProceduresSignatory> _ListSignatories = _Procedure.NvUsersProceduresSignatory;

                        int ret = 0;
                        string _rd;
                        RdCITx _CITX = null;
                        NvUsersProcedures nvup = null;
                        LOG.Debug("NotaryPortalController.GetStatus - Recorro cada Signatory para chequear (#Q=" +
                                        (_ListSignatories != null ? _ListSignatories.Count.ToString() : "Null"));
                        if (_ListSignatories != null && _ListSignatories.Count > 0)
                        {
                            string[] listMailsAlarms = Properties.Settings.Default.NvListMailAlarmWebhooks.Split(';');
                            //2.- Por cada TrackidCI recuperado 
                            foreach (NvUsersProceduresSignatory item in _ListSignatories)
                            {
                                //Si el status es 0 => Aun no se chequeo antes o no se habia completado antes => Llamar a BP para revisar status
                                if (item.Status == 0)
                                {
                                    //3.- Consulto status en servicio CI (local en este caso)
                                    ret = NotaryPortalUnitOfWork.GetStatusCIDirectBD(item.Trackidci, out _CITX);
                                    if (ret == 0) //Status devuelto por CI es OK => Actualizo NvUserProcedure
                                    {
                                        //4.- Si completo el CI => Actualizo estado en Tramite
                                        //Hashtable htRD = null;
                                        ////4.1.- Recupero RDEnvelope
                                        //bool bretparse = NotaryPortalUnitOfWork.ParseRDEnvelope(_rd, out htRD);

                                        //if (!bretparse || htRD == null || htRD.Count == 0)
                                        //{
                                        //    //4.1.1.- Si no lo consigue => Manda mail avisando 
                                        //    LOG.Warn("NotarizeController.GetStatus - Error parseando param recibido en GetStatusCI con trackid => " + item);
                                        //    bool bret = libs.Utils.SendMail(listMailsAlarms, "Atención - Error en GetStatusCI", "Error parseando param recibido en GetStatusCI", null, null, null);
                                        //}
                                        //else
                                        //{
                                        //4.1.2.- Si lo consigue => Actualiza NvUsersProceduresEvidences y NvUsersProcedures  
                                        string trackidci;
                                        //int iret = AdministradorNvUsersProcedures.UpdateInfoCIinBD(_rd, htRD, out trackidci, out nvup);
                                        int iret = NotaryPortalUnitOfWork.UpdateInfoSignatoryInMemory(_Procedure, item, _CITX, out trackidci, out nvup);
                                        //int iret = AdministradorNvUsersProcedures.UpdateInfoSignatoryInBD(_Procedure, _rd, htRD, out trackidci, out nvup);
                                        if (iret < 0)
                                        {
                                            //4.1.2.1.- Si hay error => MAnda mail avisando
                                            LOG.Warn("NotarizeController.GetStatus - Error actualizando en memoria info recibida en GetStatusCIDirectBD con trackid => " + item);
                                            bool bret = libs.Utils.SendMail(listMailsAlarms, "Atención - Error en GetStatusCI CI",
                                                "Error en memoria info recibida en GetStatusCIDirectBD - TrackIdCI = " +
                                                (string.IsNullOrEmpty(trackidci) ? "NULL" : trackidci), null, null, null);
                                        }
                                        else
                                        {
                                            LOG.Warn("NotarizeController.GetStatus - UpdateInfoCIinBD OK!");
                                            bRetrieveAgain = true;
                                        }
                                        //}
                                    }
                                }
                            }
                            if (bRetrieveAgain && nvup != null) //Significa que alguno d elos signatory o todos se actualizaron en sus datos, asi que grabo en BD
                            {
                                LOG.Debug("NotarizeController.GetStatus - Ingreso a UpdateBD para Procedure.trackid=" +
                                            nvup.TrackId.ToString() + "...");
                                if (!AdministradorNvUsersProcedures.Update(nvup))
                                {
                                    LOG.Warn("NotarizeController.GetStatus - Error actualizando en BD info recibida en GetStatusCIDirectBD con " +
                                                "Procedure.trackid => " + trackid);
                                    bool bret = libs.Utils.SendMail(listMailsAlarms, "Atención - Error en Update Status CI",
                                                                    "Error actualizando en BD info recibida en GetStatusCIDirectBD con " +
                                                                    "Procedure.trackid => " + (string.IsNullOrEmpty(trackid) ? "NULL" : trackid), null, null, null);
                                }
                                else
                                {
                                    if (!NotaryPortalUnitOfWork.UpdateStatusProcedure(nvup, out nvup))
                                    {
                                        LOG.Warn("NotarizeController.GetStatus - Error actualizando en BD info recibida en GetStatusCIDirectBD con " +
                                                "Procedure.trackid => " + trackid);
                                        bool bret = libs.Utils.SendMail(listMailsAlarms, "Atención - Error en Update Status CI",
                                                                        "Error actualizando en BD info recibida en GetStatusCIDirectBD con " +
                                                                        "Procedure.trackid => " + (string.IsNullOrEmpty(trackid) ? "NULL" : trackid), null, null, null);
                                    }
                                    else
                                    {
                                        LOG.Debug("NotarizeController.GetStatus - Estado actualizado para Procedure.trackid=" + nvup.TrackId
                                                    + " [Status=" + nvup.Status.ToString() + "]");
                                    }
                                }
                            }
                        }
                    }

                    //Los pasos son:
                    //  1.- Si no se creo la orden de pago => Se crea y se devuelve esos datos en esta respuesta
                    //  2.- Si ya fue generada => se chequea status en payment y devuelve resultado aqui
                    //    if (procedure.Status == StatusNvUsersProcedures.STATUS_PAYMENT_PENDING && _isMailValidated)
                    //    {
                    //        LOG.Debug("NotaryPortalController.GetStatus - Status = " + procedure.Status + " => Verificamos via proveedor pagos...");
                    //        string[] listMailsAlarms = Properties.Settings.Default.NvListMailAlarmWebhooks.Split(';');
                    //        //Valores posibles procedure.StatusPayment
                    //        //  0 - Aun no se genero la order
                    //        //  1 - Se genero pero esta pendiente aun
                    //        //  2 - Se completo el pago y debe estar el detalle en PaymentInfo
                    //        switch (procedure.PaymentStatus)
                    //        {
                    //            case 0: //=> Aun no se genero order => Creo order, update y retorno
                    //                int ret = AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure(procedure, procedure.TrackId);
                    //                if (ret < 0)
                    //                {
                    //                    LOG.Error("NotaryPortalController.GetStatus - Error generando orden de pago [" + ret + "]");
                    //                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    //                                    (new NotarizedResponseModel(Errors.IRET_ERR_CREATING_PAYMENT_ORDER,
                    //                                    "Error creando orden de pago. Reintente...", trackid, null, null, null, 0, 0, 0, _isMailValidated, 0,
                    //                                    procedure.PaymentUrl, procedure.PaymentToken, 0, null)));
                    //                }
                    //                else
                    //                {
                    //                    bRetrieveAgain = true;
                    //                }
                    //                break;
                    //            case 1: //=> Ya creada => getstatus, update y retorno
                    //                ret = AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure(procedure, procedure.TrackId);
                    //                if (ret < 0)
                    //                {
                    //                    LOG.Error("NotaryPortalController.GetStatus - Error consultando status orden de pago [" + ret + "]");
                    //                    return Request.CreateResponse(HttpStatusCode.OK,
                    //                                    (new NotarizedResponseModel(Errors.IRET_ERR_STATUS_PAYMENT_INCORRECT,
                    //                                    "Orden de Pago creada. Complete el pago y reintente...", trackid, null, null, null,
                    //                                    procedure.Status, procedure.IdNotary, procedure.IdProcedure, _isMailValidated,
                    //                                    procedure.PaymentStatus, procedure.PaymentUrl, procedure.PaymentToken, 0, null)));
                    //                }
                    //                else
                    //                {
                    //                    bRetrieveAgain = true;
                    //                }
                    //                break;
                    //            default: //2 => Ya pagado => retorno
                    //                if (procedure.PaymentStatus == 2) //Ya pagado => Dejo como está para luego armar la respuesta
                    //                {
                    //                    LOG.Error("NotaryPortalController.GetStatus - Tramite ya pagado con fecha = " +
                    //                        (string.IsNullOrEmpty(procedure.PaymentDate) ? "Fecha No Existe" : procedure.PaymentDate));
                    //                }
                    //                bRetrieveAgain = true;
                    //                break;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (_isMailValidated)
                    //        {
                    //            LOG.Error("NotaryPortalController.GetStatus - Tramite trackid = " + procedure.TrackId +
                    //                        " pendiente de pago, pero mail no confirmado... Aborta!");
                    //        }
                    //    }

                    //    //Si se completo CI y Pago => Mando a notarizar
                    //    if (procedure.Status == StatusNvUsersProcedures.STATUS_READY_TO_NOTARIZE && _isMailValidated)
                    //    {
                    //        LOG.Debug("NotaryPortalController.GetStatus - Status = " + procedure.Status + " => Enviamos a notarizar...");
                    //        int ret = AdministradorNvUsersProcedures.SendProcedureToNotarize(procedure);
                    //        if (ret < 0)
                    //        {
                    //            LOG.Error("NotaryPortalController.GetStatus - Error enviando a notarizar [" + ret + "]");
                    //            return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    //                            (new NotarizedResponseModel(Errors.IRET_ERR_CALLING_PROVIDER_SERVICE,
                    //                            "Error enviando a notarizar. Reintente...", trackid, null, null, null, 0,
                    //                            procedure.IdNotary, procedure.IdProcedure, _isMailValidated, 0,
                    //                            null, null, 0, null)));
                    //        }
                    //        else
                    //        {
                    //            bRetrieveAgain = true;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (_isMailValidated)
                    //        {
                    //            LOG.Warn("NotaryPortalController.GetStatus - Tramite trackid = " + procedure.TrackId +
                    //                        " listo para notarizar, pero mail no confirmado... Aborta!");
                    //        }
                    //    }

                    //    if (procedure.Status == StatusNvUsersProcedures.STATUS_IN_PROCESS_NOTARIZING)
                    //    {
                    //        LOG.Debug("NotaryPortalController.GetStatus - Status = " + procedure.Status + " => Verificamos via provider...");
                    //        //3) LLamo a servicio para revisar status

                    //        //3.1) Debo con el trackid saber la url donde consultar 
                    //        string _UrlBase = AdministradorNvConfig.GetUrlBase(procedure.IdProvider);
                    //        //3.2) Consulto en https://fea-ext.notarialeiva.cl:7070/ServicioREST/api/GetStatusNotarize/8b1b9985925f447ab19180e10c283687
                    //        LOG.Debug("NotaryPortalController.GetStatus - Consultando via URL = " + _UrlBase);
                    //        NotarizedProviderResponseModel response = NvProviderHelper.GetStatusNotarize(_UrlBase, trackid);
                    //        bRetrieveAgain = true;
                    //    }
                    //}

                    //Recupero de nuevo datos para informar si se consulto d enuevo al notario sino sigo con lo que ya recupere
                    if (bRetrieveAgain)
                    {
                        LOG.Debug("NotaryPortalController.GetStatus - Recupero de nuevo desde BD porque se consulto a notario...");
                        _Procedure = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr, true);
                        ////Elimino referencias ciclicas para la serializacion
                        //if (_Procedure != null)
                        //{
                        //    foreach (NvUsersProceduresSignatory itemAux in _Procedure.NvUsersProceduresSignatory)
                        //    {
                        //        itemAux.NvUsersProcedures = null;
                        //    }
                        //    foreach (NvUsersProceduresEvidences itemAux in _Procedure.NvUsersProceduresEvidences)
                        //    {
                        //        itemAux.NvUsersProcedures = null;
                        //    }
                        //}
                    }

                    if (_Procedure == null)
                    {
                        LOG.Error("NotaryPortalController.GetStatus - Tramite no encontrado");
                        return Request.CreateResponse(HttpStatusCode.NotFound,
                                    (new ErrorModel(Errors.IRET_ERR_USER_PROCEDURE_NOT_FOUND,
                                    "Tramite no encontrado con TrackId = " + trackid + ". Reintente...", trackid)));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new NVResponse(Errors.IRET_OK, null,
                                             (new NotarizedResponseModel(Errors.IRET_ERR_PROCEDURE_PENDING,
                                                       null, trackid, null, null, null, _Procedure.Status, _Procedure.IdNotary, _Procedure.IdProcedure, _isMailValidated,
                                                       _Procedure.PaymentStatus, _Procedure.PaymentUrl, _Procedure.PaymentToken, _Procedure.StatusNotary,
                                                       (string.IsNullOrEmpty(_Procedure.StatusMsgNotary) ? "S/C" : _Procedure.StatusMsgNotary.ToString())))));
                        //if (_Procedure.Status != StatusNvUsersProcedures.STATUS_NOTARIZED)
                        //{
                        //    return Request.CreateResponse(HttpStatusCode.OK,
                        //                      (new NotarizedResponseModel(Errors.IRET_ERR_PROCEDURE_PENDING,
                        //                                (!_isMailValidated ? "Warning: Falta Verificar mail. Debe completar este paso para avanzar..." : null),
                        //                                trackid, null, null, null, procedure.Status, procedure.IdNotary, procedure.IdProcedure, _isMailValidated,
                        //                                procedure.PaymentStatus, procedure.PaymentUrl, procedure.PaymentToken, procedure.StatusNotary,
                        //                                (string.IsNullOrEmpty(procedure.StatusMsgNotary) ? "S/C" : procedure.StatusMsgNotary.ToString()))));
                        //}
                        //else
                        //{
                        //    return Request.CreateResponse(HttpStatusCode.OK,
                        //                      (new NotarizedResponseModel(Errors.IRET_API_OK,
                        //                       "Notarizado con exito!", trackid, null,
                        //                       procedure.UrlValidator, procedure.NotaryIdValidator, procedure.Status, procedure.IdNotary, procedure.IdProcedure,
                        //                          _isMailValidated, procedure.PaymentStatus, procedure.PaymentUrl, procedure.PaymentToken, procedure.StatusNotary,
                        //                                (string.IsNullOrEmpty(procedure.StatusMsgNotary) ? "S/C" : procedure.StatusMsgNotary.ToString()))));
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotaryPortalController.GetStatus Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, 
                                (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido procesando Get Status One Notarize ["
                                                  + ex.Message + "]", trackid)));
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        #endregion v2021

        #endregion Procedures

        #region basico
        // GET: api/NotaryPortal
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/NotaryPortal/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/NotaryPortal
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/NotaryPortal/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/NotaryPortal/5
        //public void Delete(int id)
        //{
        //}
        #endregion basico
    }
}
