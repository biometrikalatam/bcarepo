﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using Newtonsoft.Json;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database.Notary.config;
using Bio.Digital.Receipt.Core.Helpers;
using System.Xml;
using System.Net.Http.Headers;
using System.Collections;
using System.IO;
using Bio.Digital.Receipt.Core.Services;
using Swashbuckle.Swagger.Annotations;

namespace Bio.Digital.Receipt.rest.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// //[EnableCors(origins:"*", headers:"*", methods:"*")]
    public class NotarizeController : ApiController
    {
        //Request.CreateResponse(HttpStatusCode.OK, new ResponseModel(200, "Login verificado con exito!"));
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NotarizeController));

        public object BPResponse { get; private set; }

        #region V2

        /// <summary>
        /// Inica proceso de notarizacion, pero ahora en lugar de retornar el QR, retorna una URL del BPWeb para recolección de evidencias.
        /// </summary>  
        /// <remarks>
        /// Inicia proceso de notarizacion, enviando mail, documento y lista de evidencias si aplica. 
        /// Los pasos que se realizan son:
        ///   * 1.- Chequeo de user, si no existe mail, se crea y envia mail para validarlo. Sino solo update de ultimo ingreso
        ///   * 2.- Se inicia proceso de CI con NV CI, y se obtiene la URL del BPWeb para enviar al frontend
        ///   * 3.- Se graba info en BD del trámite iniciado, y status de validation correo en 0 y status tramite en pendiente de CI
        ///   * 4.- Se responde el trackid del trámite y la URL de CI
        /// </remarks>
        /// <param name="param">Parametros de entrada</param>
        /// <returns></returns>
        [Route("api/v2/InitProcedureOne")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "NotarizeResponseModel", typeof(NotarizeResponseModel))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(Error))]
        public object InitProcedureOneV2([FromBody] v2NotarizeInitModel param) //string mail, int notaryid, int procedureid
        {
            string trackId = null;
            string msgerr = null;
            InitNotarizeResponseModel response = null;
            try
            {
                LOG.Debug("NotarizeController.InitProcedureOneV2 - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("SignerController.txCreate - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }


                //1) Deserializo parámetros     
                if (param == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                    (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null)));
                }
                //string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                //LOG.Debug("NotarizeController.InitProcedureOneV2 - Deserializo ParamIn => " + strIn);
                v2NotarizeInitModel paramIn = param;

                if (paramIn == null)
                {
                    LOG.Debug("NotarizeController.InitProcedureOneV2 - Error deserializando parametros de entrada [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                    (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST, "Error deserializando parametros de entrada [Null]", null)));
                }
                else
                {
                    string msgRet = "";
                    if (!IsCorrectDocument(paramIn.document)) //String.IsNullOrEmpty(paramIn.document))
                    {
                        //Added para controlar que si es una CI no viene doc inciial => Le agrego una carátula PDF para 
                        //seguir con mismo proceso que trámite normal. En cada NOtaria SIEMPRE el id tramide = 1 => CI
                        // Entonces si es 1 => agrego caraturla y sigo
                        if (paramIn.procedureId == 1)
                        {
                            paramIn.document = Global.CARATULA_CI;
                        }
                        else
                        {
                            msgRet += (string.IsNullOrEmpty(msgRet) ? "Documento Nulo o de formato incorrecto. Debe ser PDF!" :
                                "|Documento Nulo o de formato incorrecto. Debe ser Word o PDF!");
                        }
                    }
                    if (!IsCorrectMail(paramIn.mail)) //String.IsNullOrEmpty(paramIn.mail))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "Mail Nulo o Mal Formado" : "|Mail Nulo o Mal Formado");
                        ;
                    }
                    if (!IsCorrectRut(paramIn.rut)) // (String.IsNullOrEmpty(paramIn.rut))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "Rut Nulo o Incorrecto" : "|Ru Nulo o Incorrecto");
                    }
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                                (new NotarizeResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                    "Problemas para notarizar. [" + msgRet + "]", null)));
                    }
                }

                LOG.Debug("NotarizeController.InitProcedureOneV2 - Inicio procedure para mail = " + paramIn.mail);
                //1.1) Chequeo si existe, sino lo creo en NvUser
                NvUsers _NvUser = AdministradorNvUsers.CheckOrCreateNvUser(paramIn.mail, paramIn.rut);
                if (_NvUser == null)
                {
                    LOG.Error("NotarizeController.InitProcedureOneV2 - Imposible registrar mail...");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new NotarizeResponseModel(Errors.IRET_API_CONFLIC, "Problemas para registrar mail. Reintente...", trackId)));
                }

                //2) Genero TraciId Unico de trámite
                trackId = AdministradorNvUsersProcedures.GetTrackIdUnique();
                if (string.IsNullOrEmpty(trackId))
                {
                    LOG.Error("NotarizeController.InitProcedureOneV2 - TrackId generado invalido [Null]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new NotarizeResponseModel(Errors.IRET_API_CONFLIC, "Problemas para crear id de transaccion unico. Reintente...",
                        null)));
                }

                //3) Get URL de CI WEB para retornar
                string sURLCI;
                string sTrackIdCI = GenerateCIWeb(paramIn, out sURLCI, out msgerr);
                if (string.IsNullOrEmpty(sURLCI) || string.IsNullOrEmpty(sTrackIdCI))
                {
                    LOG.Error("NotarizeController.InitProcedureOneV2 - Error creando TraciIdCI o sURLCI para CI");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new NotarizeResponseModel(Errors.IRET_API_ERR_INIT_CI, "Error creando TraciIdCI o sURLCI para CI", null)));
                }

                

                //4) Grabo NvUsersProcedures con status = 0 => Recibido
                NvUsersProcedures NvUsersProceduresCreated;
                //string msgerr;
                int provideridout;

                int ret = AdministradorNvUsersProcedures.CreateNvUserProcedure(trackId, _NvUser.Id, paramIn.notaryId, paramIn.procedureId,
                                                              paramIn.document, paramIn.message, sTrackIdCI, sURLCI,
                                                              out provideridout, out msgerr, out NvUsersProceduresCreated);

                if (ret < 0 || NvUsersProceduresCreated == null)
                {
                    LOG.Error("NotarizeController.InitProcedureOneV2 - Error creando procedure para iniciar proceso [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                        (new NotarizeResponseModel(Errors.IRET_ERR_NVPROCEDURE_NO_SAVED, "Error creando procedure para iniciar proceso[" + msgerr + "]",
                        null)));
                }

                //Si todo funciono ok => Genro respuesta para retornar.
                response = new InitNotarizeResponseModel(Errors.IRET_API_OK, null, trackId, sTrackIdCI, sURLCI);

                //Informo x mail
                string[] strbody = new string[2];
                strbody[0] = "Usted a iniciado el trámite <b>" + NvUsersProceduresCreated.NameProcedure + "</b>";
                strbody[1] = "El codigo unico del tramite para consultas es: <b>" + NvUsersProceduresCreated.TrackId + "</b>";
                libs.Utils.NVNotify(paramIn.mail, "Inicio de tramite", strbody, null, null, null);

            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.InitProcedureOneV2 Error Iniciando trámite: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    (new NotarizeResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconcido procesando inicio de tramite ["
                                                  + ex.Message + "]", null)));
            }

            //return  JsonConvert.SerializeObject
            //(new NotarizeResponseModel(0, "Proceso de notarizacion iniciado!"));
            return Request.CreateResponse(HttpStatusCode.OK, response);

        }

        /// <summary>
        /// Retorna status del tramite, consultando cada servicio externo relacionado, como CI, FLow y notario
        /// </summary>
        /// <param name="trackid"></param>
        /// <returns></returns>
        [Route("api/v2/GetStatusOneNotarize")]
        [HttpGet]
        public object GetStatusOneNotarizeV2(string trackid)
        {
            string trackId = null;
            string msgerr = null;
            bool _isMailValidated = false;
            try
            {
                LOG.Debug("NotarizeController.GetStatusOneNotarizeV2 - IN...[trackid=" + trackid + "]");
                //1) Deserializo parámetros     
                if (string.IsNullOrEmpty(trackid))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new NotarizedResponseModel(Errors.IRET_API_BAD_REQUEST,
                        "TrackId consultado no puede ser nulo", trackid, null, null, null, 0, 0, 0, _isMailValidated, 0, null, null, 0, null)));
                }
                //2) Recupero NvProcedure
                NvUsersProcedures procedure = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr);
                bool bRetrieveAgain = false;
                //Si es esta en estado Notarizando = 4 => chequeo en linea con notario para determinar si ya tenemos respuesta.
                if (procedure != null)
                {
                    //Chequeo verificacion 
                    _isMailValidated = AdministradorNvUsers.IsUserEnabled(procedure.IdUser);

                    //Si esta en status pendietne de validacion de mail => igual lo dejo hacer la CI en paralelo
                    if (procedure.Status == StatusNvUsersProcedures.STATUS_CI_PENDING ||
                        procedure.Status == StatusNvUsersProcedures.STATUS_MAIL_VERIFY_PENDING)
                    {
                        //1.- Recupero 1 o mas CI pendientes de finalizacion relacionados con el Tramite
                        List<string> listCIforProcedure =
                            AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByIdProcedure(procedure.Id);
                        int ret = 0;
                        string _rd;
                        if (listCIforProcedure != null && listCIforProcedure.Count > 0)
                        {
                            string[] listMailsAlarms = Properties.Settings.Default.NvListMailAlarmWebhooks.Split(';');
                            //2.- Por cada TrackidCI recuperado 
                            foreach (string item in listCIforProcedure)
                            {
                                //3.- Consulto status en servicio CI (local en este caso)

                                //TODO - Actualizar abajo para recibir objeto y rehacer UpdateInfoCIinBD abajo
                                TxSignerModelR oTxSignerModelR;
                                ret = NotarizeUnitOfWork.GetStatusCIWeb(item, out oTxSignerModelR);
                                if (ret == 1) //Status devuelto por CI es OK => Actualizo NvUserProcedure
                                {
                                    //4.- Si completo el CI => Actualizo estado en Tramite
                                    //Hashtable htRD;
                                    ////4.1.- Recupero RDEnvelope
                                    //bool bretparse = ParseRDEnvelope(_rd, out htRD);

                                    //if (!bretparse || htRD == null || htRD.Count == 0)
                                    if (oTxSignerModelR == null)
                                    {
                                        //4.1.1.- Si no lo consigue => Manda mail avisando 
                                        LOG.Warn("NotarizeController.GetStatusOneNotarizeV2 - Error param recibido (oTxSignerModelR) en GetStatusCIWeb con trackid => " + item);
                                        bool bret = libs.Utils.SendMail(listMailsAlarms, "Atención - Error en GetStatusCIWeb", "Error param recibido en GetStatusCIWeb", null, null, null);
                                    }
                                    else
                                    {
                                        //4.1.2.- Si lo consigue => Actualiza NvUsersProceduresEvidences y NvUsersProcedures  
                                        string trackidci;
                                        NvUsersProcedures nvup;
                                        int iret = AdministradorNvUsersProcedures.UpdateInfoCIWebinBD(trackid, oTxSignerModelR, out trackidci, out nvup);
                                        if (iret < 0)
                                        {
                                            //4.1.2.1.- Si hay error => MAnda mail avisando
                                            LOG.Warn("NotarizeController.GetStatusOneNotarizeV2 - Error parseando param recibido en GetStatusCIWeb con trackid => " + item);
                                            bool bret = libs.Utils.SendMail(listMailsAlarms, "Atención - Error en GetStatusCIWeb CI",
                                                "Error parseando param recibido en GetStatusCIWeb - TrackIdCI = " +
                                                (string.IsNullOrEmpty(trackidci) ? "NULL" : trackidci), null, null, null);
                                        }
                                        else
                                        {
                                            LOG.Debug("NotarizeController.GetStatusOneNotarizeV2 - UpdateInfoCIWebinBD OK!");
                                            bRetrieveAgain = true;
                                        }
                                    }
                                } else if (ret == 2)
                                { 
                                    LOG.Debug("NotarizeController.GetStatusOneNotarizeV2 - Sale porque la tx esta pending aun!");
                                } else {

                                }
                            
                            }
                        }
                    }

                    //Los pasos son:
                    //  1.- Si no se creo la orden de pago => Se crea y se devuelve esos datos en esta respuesta
                    //  2.- Si ya fue generada => se chequea status en payment y devuelve resultado aqui
                    if (procedure.Status == StatusNvUsersProcedures.STATUS_PAYMENT_PENDING && _isMailValidated)
                    {
                        LOG.Debug("NotarizeController.GetStatusOneNotarizeV2 - Status = " + procedure.Status + " => Verificamos via proveedor pagos...");
                        string[] listMailsAlarms = Properties.Settings.Default.NvListMailAlarmWebhooks.Split(';');
                        //Valores posibles procedure.StatusPayment
                        //  0 - Aun no se genero la order
                        //  1 - Se genero pero esta pendiente aun
                        //  2 - Se completo el pago y debe estar el detalle en PaymentInfo
                        switch (procedure.PaymentStatus)
                        {
                            case 0: //=> Aun no se genero order => Creo order, update y retorno
                                int ret = AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure(procedure, procedure.TrackId);
                                if (ret < 0)
                                {
                                    LOG.Error("NotarizeController.GetStatusOneNotarizeV2 - Error generando orden de pago [" + ret + "]");
                                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                                                    (new NotarizedResponseModel(Errors.IRET_ERR_CREATING_PAYMENT_ORDER,
                                                    "Error creando orden de pago. Reintente...", trackid, null, null, null, 0, 0, 0, _isMailValidated, 0,
                                                    procedure.PaymentUrl, procedure.PaymentToken, 0, null)));
                                }
                                else
                                {
                                    bRetrieveAgain = true;
                                }
                                break;
                            case 1: //=> Ya creada => getstatus, update y retorno
                                ret = AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure(procedure, procedure.TrackId);
                                if (ret < 0)
                                {
                                    LOG.Error("NotarizeController.GetStatusOneNotarizeV2 - Error consultando status orden de pago [" + ret + "]");
                                    return Request.CreateResponse(HttpStatusCode.OK,
                                                    (new NotarizedResponseModel(Errors.IRET_ERR_STATUS_PAYMENT_INCORRECT,
                                                    "Orden de Pago creada. Complete el pago y reintente...", trackid, null, null, null,
                                                    procedure.Status, procedure.IdNotary, procedure.IdProcedure, _isMailValidated,
                                                    procedure.PaymentStatus, procedure.PaymentUrl, procedure.PaymentToken, 0, null)));
                                }
                                else
                                {
                                    bRetrieveAgain = true;
                                }
                                break;
                            default: //2 => Ya pagado => retorno
                                if (procedure.PaymentStatus == 2) //Ya pagado => Dejo como está para luego armar la respuesta
                                {
                                    LOG.Error("NotarizeController.GetStatusOneNotarizeV2 - Tramite ya pagado con fecha = " +
                                        (string.IsNullOrEmpty(procedure.PaymentDate) ? "Fecha No Existe" : procedure.PaymentDate));
                                }
                                bRetrieveAgain = true;
                                break;
                        }
                    }
                    else
                    {
                        if (!_isMailValidated)
                        {
                            LOG.Warn("NotarizeController.GetStatusOneNotarizeV2 - Tramite trackid = " + procedure.TrackId +
                                        " pendiente de pago, pero mail no confirmado... Aborta!");
                        }
                    }

                    //Si se completo CI y Pago => Mando a notarizar
                    if (procedure.Status == StatusNvUsersProcedures.STATUS_READY_TO_REVISION_NOTARIZE && _isMailValidated)
                    {
                        LOG.Debug("NotarizeController.GetStatusOneNotarizeV2 - Status = " + procedure.Status + 
                                  " => Pendiente de revision para notarizar...");
                        int ret = AdministradorNvUsersProcedures.SendProcedureToNotarize(procedure);
                        if (ret < 0)
                        {
                            LOG.Error("NotarizeController.GetStatusOneNotarizeV2 - Error enviando a notarizar [" + ret + "]");
                            return Request.CreateResponse(HttpStatusCode.InternalServerError,
                                            (new NotarizedResponseModel(Errors.IRET_ERR_CALLING_PROVIDER_SERVICE,
                                            "Error enviando a notarizar. Reintente...", trackid, null, null, null, 0,
                                            procedure.IdNotary, procedure.IdProcedure, _isMailValidated, 0,
                                            null, null, 0, null)));
                        }
                        else
                        {
                            bRetrieveAgain = true;
                        }
                    }
                    else
                    {
                        if (!_isMailValidated)
                        {
                            LOG.Warn("NotarizeController.GetStatusOneNotarizeV2 - Tramite trackid = " + procedure.TrackId +
                                        " listo para notarizar, pero mail no confirmado... Aborta!");
                        }
                    }

                    //Si se completo CI y Pago => Mando a notarizar
                    if (procedure.Status == StatusNvUsersProcedures.STATUS_READY_TO_NOTARIZE && _isMailValidated)
                    {
                        LOG.Debug("NotarizeController.GetStatusOneNotarizeV2 - Status = " + procedure.Status + " => Enviamos a notarizar...");
                        int ret = AdministradorNvUsersProcedures.SendProcedureToNotarize(procedure);
                        if (ret < 0)
                        {
                            LOG.Error("NotarizeController.GetStatusOneNotarizeV2 - Error enviando a notarizar [" + ret + "]");
                            return Request.CreateResponse(HttpStatusCode.InternalServerError,
                                            (new NotarizedResponseModel(Errors.IRET_ERR_CALLING_PROVIDER_SERVICE,
                                            "Error enviando a notarizar. Reintente...", trackid, null, null, null, 0,
                                            procedure.IdNotary, procedure.IdProcedure, _isMailValidated, 0,
                                            null, null, 0, null)));
                        }
                        else
                        {
                            bRetrieveAgain = true;
                        }
                    }
                    else
                    {
                        if (!_isMailValidated)
                        {
                            LOG.Warn("NotarizeController.GetStatusOneNotarizeV2 - Tramite trackid = " + procedure.TrackId +
                                        " listo para notarizar, pero mail no confirmado... Aborta!");
                        }
                    }

                    if (procedure.Status == StatusNvUsersProcedures.STATUS_IN_PROCESS_NOTARIZING)
                    {
                        LOG.Debug("NotarizeController.GetStatusOneNotarizeV2 - Status = " + procedure.Status + " => Verificamos via provider...");
                        //3) LLamo a servicio para revisar status

                        //3.1) Debo con el trackid saber la url donde consultar 
                        string _UrlBase = AdministradorNvConfig.GetUrlBase(procedure.IdProvider);
                        //3.2) Consulto en https://fea-ext.notarialeiva.cl:7070/ServicioREST/api/GetStatusNotarize/8b1b9985925f447ab19180e10c283687
                        LOG.Debug("NotarizeController.GetStatusOneNotarizeV2 - Consultando via URL = " + _UrlBase);
                        NotarizedProviderResponseModel response = NvProviderHelper.GetStatusNotarize(_UrlBase, trackid);
                        bRetrieveAgain = true;
                    }
                }

                //Recupero de nuevo datos para informar si se consulto d enuevo al notario sino sigo con lo que ya recupere
                if (bRetrieveAgain)
                {
                    LOG.Debug("NotarizeController.GetStatusOneNotarizeV2 - Recupero d enuevo desde BD porque se consulto a notario...");
                    procedure = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr);
                }

                if (procedure == null)
                {
                    LOG.Error("NotarizeController.GetStatusOneNotarizeV2 - Tramite no encontrado");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        (new NotarizedResponseModel(Errors.IRET_ERR_USER_PROCEDURE_NOT_FOUND,
                        "Tramite no encontrado con TrackId = " + trackid + ". Reintente...", trackid, null, null, null, 0, 0, 0, _isMailValidated, 0,
                        null, null, 0, null)));
                }
                else
                {
                    if (procedure.Status != StatusNvUsersProcedures.STATUS_NOTARIZED)
                    {
                        //"Status actual = " + procedure.Status.ToString() +
                        //                            "|Status Payment = " + procedure.PaymentStatus.ToString() +
                        //                            "|Status Notary = " + procedure.StatusNotary.ToString() +
                        //                            "|Status Msg Notary = " +
                        //                            (string.IsNullOrEmpty(procedure.StatusMsgNotary) ? "S/C" : procedure.StatusMsgNotary.ToString())
                        return Request.CreateResponse(HttpStatusCode.OK,
                                          (new NotarizedResponseModel(Errors.IRET_ERR_PROCEDURE_PENDING,
                                                    (!_isMailValidated ? "Warning: Falta Verificar mail. Debe completar este paso para avanzar..." : null),
                                                    trackid, null, null, null, procedure.Status, procedure.IdNotary, procedure.IdProcedure, _isMailValidated,
                                                    procedure.PaymentStatus, procedure.PaymentUrl, procedure.PaymentToken, procedure.StatusNotary,
                                                    (string.IsNullOrEmpty(procedure.StatusMsgNotary) ? "S/C" : procedure.StatusMsgNotary.ToString()))));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                                          (new NotarizedResponseModel(Errors.IRET_API_OK,
                                           "Notarizado con exito!", trackid, null,
                                           procedure.UrlValidator, procedure.NotaryIdValidator, procedure.Status, procedure.IdNotary, procedure.IdProcedure,
                                              _isMailValidated, procedure.PaymentStatus, procedure.PaymentUrl, procedure.PaymentToken, procedure.StatusNotary,
                                                    (string.IsNullOrEmpty(procedure.StatusMsgNotary) ? "S/C" : procedure.StatusMsgNotary.ToString()))));
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.GetStatusOneNotarizeV2 Error: " + ex.Message);
                return (new NotarizedResponseModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido procesando Get Status One Notarize ["
                                                  + ex.Message + "]", trackid, null, null, null, 0, 0, 0, _isMailValidated, 0, null, null, 0, null));
            }
        }

        [Route("api/v2/WebhookBPWebForNVv2")]
        [HttpPost]
        public object WebhookBPWebForNVv2()
        {
            string msgerr;
            try
            {
                LOG.Debug("SignerController.WebhookBPWebForNVv2 - IN...");
                string trackidbp = System.Web.HttpContext.Current.Request.Params["trackid"];
                LOG.Debug("SignerController.WebhookBPWebForNVv2 - TrackIdBP recibido => " + (string.IsNullOrEmpty(trackidbp) ? "Null" : trackidbp));

                //TODO - Sacar Tx de CI desde el trackid de BP
                RdCITx tx = AdministradorRdCITx.GetCITxByTrackIdBP(trackidbp, out msgerr);

                if (tx == null)
                {
                    LOG.Warn("SignerController.WebhookBPWebForNVv2 - RdCITx recuperada nula [" +
                                        (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr) + "]");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                                            new ErrorModel(Errors.IRET_ERR_CI_NO_IN_DB, "RdCITx recuperada nula [" +
                                        (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr) + "]", "TrackId de CI no existe"));
                }

                LOG.Debug("SignerController.WebhookBPWebForNVv2 - Ingresando a UpdateRdCIWebTx...");
                int ret = NotarizeUnitOfWork.UpdateRdCIWebTx(tx, out msgerr, out tx);
                if (ret < 0)
                {
                    LOG.Warn("SignerController.WebhookBPWebForNVv2 - Error en UpdateRdCIWebTx ret = " + ret + "[" +
                             (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr) + "]");
                    return Request.CreateResponse(HttpStatusCode.NotModified, new ErrorModel(ret, "Error Status/Update CI", msgerr));
                }
                //TODO - Recfuperar desde BPWeb la info completa y generar la CI
            }
            catch (Exception ex)
            {
                LOG.Error("SignerController.WebhookBPWebForNVv2 Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            LOG.Debug("SignerController.WebhookBPWebForNVv2 - OUT!");
            return Request.CreateResponse(HttpStatusCode.OK);
        }


        /// <summary>
        /// Salva revisión realizada pro funcionario de notaria, indicando aceptación o rechazo.
        /// </summary>  
        /// <remarks>
        /// Luego de la revisión manual por parte del funcionario, este indica si el trámite está: 
        ///   * Aprobado (status = 1), indicando el motivo del rechazo  
        ///   * Rechazado (status = 2), enviando texto a agregar en el documento como parte del proceso de notarización.  
        /// Los pasos que se realizan son:
        ///   * 1.- Verifico entrada de parametros, si hay error salgo
        ///   * 2.- Recupero tramite, si hay error salgo
        ///   * 3.- Se graba info en BD del trámite y status, avisando el paso a cada parte
        ///   * 4.- Se responde el trackid del trámite y resultado
        /// </remarks>
        /// <param name="param">Parametros de entrada</param>
        /// <returns></returns>
        [Route("api/v2/RevisionCompleted")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "RevisionCompletedR", typeof(RevisionCompletedModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(Error))]
        public object RevisionCompleted([FromBody] RevisionCompletedModel param)
        {
            string trackId = null;
            string msgerr = null;
            NvNotaryUsers _user;
            try
            {
                LOG.Debug("NotarizeController.RevisionCompleted - IN...");
                //1) Deserializo parámetros     
                if (param == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new RevisionCompletedModelR(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos Complete y reintente...", null)));
                    
                }
                //string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                LOG.Debug("NotarizeController.RevisionCompleted - Deserializo ParamIn...");
                RevisionCompletedModel paramIn = param; //JsonConvert.DeserializeObject<NotarizeRequestModel>(strIn);

                if (paramIn == null)
                {
                    LOG.Debug("NotarizeController.RevisionCompleted - Error deserializando parametros de entrada [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, 
                        (new RevisionCompletedModelR(Errors.IRET_ERR_PARAM_INCORRECTOS, 
                        "Problemas para enviar a notarizar. Complete y reintente...", null)));
                }
                else
                {
                    string msgRet = "";
                    if (String.IsNullOrEmpty(paramIn.trackid))
                    {
                        msgRet += "|TrackId";
                    } else
                    {
                        trackId = paramIn.trackid;
                    }
                    _user = AdministradorNvNotaryUsers.BuscarNvNotaryUserByMail(paramIn.userid);
                    if (_user == null)
                    {
                        msgRet += "|Usuario No Existe o Deshabilitado!";
                    }
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, 
                            (new RevisionCompletedModelR(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                "Problemas para notarizar. [" + msgRet + "]", null)));
                    }
                }

                //2) Recupero trámite
                if (string.IsNullOrEmpty(trackId) || string.IsNullOrEmpty(paramIn.userid))
                {
                    LOG.Error("NotarizeController.RevisionCompleted - TrackId o UserId recibido inválido [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, 
                        (new RevisionCompletedModelR(Errors.IRET_ERR_PARAM_INCORRECTOS,
                        "Problemas para procesar transaccion [TrackId o UserId recibido inválido [Null]]. Reintente...", null)));
                }

                int ret = NotarizeUnitOfWork.UpdateNvUserProcedure(paramIn, out msgerr);
               
                //   5.2) Retorno resultado para informar al usuario(y envio mail indicando)
                if (ret < 0)
                {
                    HttpStatusCode code = HttpStatusCode.InternalServerError;

                    if (ret == Errors.IRET_ERR_PROCEDURE_NOTFOUND) { //-421
                        code = HttpStatusCode.NotFound;
                    } else if (ret == -409) //Conflict x incoherencia entre etado y msg asociado Errors.IRET_API_CONFLIC * -1
                    {
                        code = HttpStatusCode.Conflict;
                    } 
                    return Request.CreateResponse(code, (new NotarizeResponseModel(ret, msgerr, trackId)));
                } else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, (new NotarizeResponseModel(ret, null, trackId)));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.RevisionCompleted Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, 
                    (new NotarizeResponseModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconcido procesando revision de notarización ["
                                                  + ex.Message + "]", null)));
            }

            //return  JsonConvert.SerializeObject
            //(new NotarizeResponseModel(0, "Proceso de notarizacion iniciado!"));
            return (new NotarizeResponseModel(Errors.IRET_OK, "Proceso de notarizacion iniciado!", trackId));

        }


        #endregion V2

        /// <summary>
        /// Webhook que recibe el CI desde NOtario, actualizando informacion en BD y cambaindo estado
        /// </summary>
        /// <param name="param"></param>
        [Route("api/WebhookReceiveCI")]
        [HttpPost]
        public object WebhookReceiveCI([FromBody] object param)
        {
            try
            {
                //System.Diagnostics.Debugger.Break();
                LOG.Debug("NotarizeController.WebhookReceiveCI - IN...");
                string[] listMailsAlarms = Properties.Settings.Default.NvListMailAlarmWebhooks.Split(';');
                //1) Deserializo parámetros     
                if (param == null)
                {
                    //Enviar mail avisando error
                    LOG.Debug("NotarizeController.WebhookReceiveCI - Deserializo ParamIn => No recibio parámetro [NULL]");
                    
                    bool bret = libs.Utils.SendMail(listMailsAlarms, "Atención - Error en Webhook CI", "Error en Webhook CI - Param NULL", null, null, null);
                    if (!bret) LOG.Error("NotarizeController.WebhookReceiveCI - Error avisando por mail que Webhooh CI recibio param = NULL");
                }
                string strIn = (string)param;
                ProcessWebhookReceiveCI(strIn);
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.WebhookReceiveCI Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Para reutilizar desde CI
        /// </summary>
        /// <param name="strIn"></param>
        internal static void ProcessWebhookReceiveCI(string strIn)
        {
            try
            {
                LOG.Debug("NotarizeController.ProcessWebhookReceiveCI - IN...");

                LOG.Debug("NotarizeController.ProcessWebhookReceiveCI - Proceso => " + strIn.Substring(0,100));
                string[] listMailsAlarms = Properties.Settings.Default.NvListMailAlarmWebhooks.Split(';');
                Hashtable htRD;
                bool bretparse = ParseRDEnvelope(strIn, out htRD);

                if (!bretparse || htRD == null || htRD.Count == 0)
                {
                    LOG.Warn("NotarizeController.ProcessWebhookReceiveCI - Error parseando param recibido en Webhook => " + strIn);
                    bool bret = libs.Utils.SendMail(listMailsAlarms, "Atención - Error en Webhook CI", "Error parseando param recibido en Webhook", null, null, null);
                }
                else
                {
                    string trackidci;
                    NvUsersProcedures NvUP;
                    int iret = AdministradorNvUsersProcedures.UpdateInfoCIinBD(strIn, htRD, out trackidci, out NvUP);
                    if (iret < 0)
                    {
                        LOG.Warn("NotarizeController.ProcessWebhookReceiveCI - Error parseando param recibido en Webhook => " + strIn);
                        bool bret = libs.Utils.SendMail(listMailsAlarms, "Atención - Error en Webhook CI",
                            "Error parseando param recibido en Webhook - TrackIdCI = " +
                            (string.IsNullOrEmpty(trackidci) ? "NULL" : trackidci), null, null, null);
                    } else 
                    {
                        if (NvUP != null)
                        {
                            //Informo x mail
                            string[] strbody = new string[2];
                            strbody[0] = "Usted completó la Certificación de identidad del trámite <b>" + NvUP.NameProcedure + "</b>";
                            strbody[1] = "El codigo unico del tramite para consultas es: <b>" + NvUP.TrackId + "</b>";
                            libs.Utils.NVNotify(AdministradorNvUsers.BuscarNvUserById(NvUP.IdUser).Mail,
                                                "Certificación de Identidad completada con exito", strbody, null, null, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.ProcessWebhookReceiveCI - Error: " + ex.Message);
            }
            LOG.Debug("NotarizeController.ProcessWebhookReceiveCI - OUT!");
        }

        //[HttpPost]
        //public object WebhookReceivePayment([FromBody] object param)
        [Route("api/WebhookReceivePayment")]
        [HttpPost]
        public object WebhookReceivePayment(string token)
        {
            LOG.Debug("NotarizeController.WebhookReceivePayment - IN...");
            try
            {
                string tokenParam = System.Web.HttpContext.Current.Request.Params["token"];
                if (tokenParam == null)
                {
                    //Enviar mail avisando error
                    LOG.Debug("NotarizeController.WebhookReceivePayment - Deserializo ParamIn => No recibio parámetro [NULL]");
                    //LOG.Debug("NotarizeController.WebhookReceivePayment - Request.Content.ToString() = " + Request.Content.ToString());
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
                else
                {
                    //LOG.Debug("NotarizeController.WebhookReceivePayment - param.GetType().ToString() = " + param.GetType().ToString());
                    try
                    {
                        var req = Request.Content;
                    }
                    catch (Exception ex)
                    {
                        string x = ex.Message;
                    }
                    try
                    {
                        string strIn1 = tokenParam; // ((Newtonsoft.Json.Linq.JObject)param).ToString();
                        LOG.Debug("NotarizeController.WebhookReceivePayment - Deserializo ParamIn => " + strIn1);
                    }
                    catch (Exception ex)
                    {
                        string x = ex.Message;
                    }
                    
                    LOG.Debug("NotarizeController.WebhookReceivePayment - Deserializo ParamIn => " + "");
                }
                
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.WebhookReceivePayment Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            LOG.Debug("NotarizeController.WebhookReceivePayment - OUT!");
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        #region ONE

        /// <summary>
        /// Inica proceso de notarizacion
        /// </summary>  
        /// <remarks>
        /// Inicia proceso de notarizacion, enviando mail, documento y lista de evidencias si aplica. 
        /// Los pasos que se realizan son:
        ///   * 1.- Chequeo de user, si no existe mail, se crea y envia mail para validarlo. Sino solo update de ultimo ingreso
        ///   * 2.- Se inicia proceso de CI con NV CI, y se obtiene el QR para enviar al frontend
        ///   * 3.- Se graba info en BD del trámite iniciado, y status de validation correo en 0 y status tramite en pendiente de CI
        ///   * 4.- Se responde el trackid del trámite y el QR de CI
        /// </remarks>
        /// <param name="mail"></param>
        /// <param name="notaryid"></param>
        /// <param name="procedureid"></param>
        /// <returns></returns>
        [Route("api/InitProcedureOne")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "NotarizeResponseModel", typeof(NotarizeResponseModel))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(Error))]
        public object InitProcedureOne([FromBody] object param) //string mail, int notaryid, int procedureid
        {
            string trackId = null;
            string msgerr = null;
            InitNotarizeResponseModel response = null;
            try
            {
                LOG.Debug("NotarizeController.InitProcedureOne - IN...");
                //1) Deserializo parámetros     
                if (param == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, 
                    (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null)));
                }
                string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                LOG.Debug("NotarizeController.InitProcedureOne - Deserializo ParamIn => " + strIn);
                InitProcedureOneRequestModel paramIn = JsonConvert.DeserializeObject<InitProcedureOneRequestModel>(strIn);

                if (paramIn == null)
                {
                    LOG.Debug("NotarizeController.InitProcedureOne - Error deserializando parametros de entrada [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                    (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST, "Error deserializando parametros de entrada [Null]", null)));
                }
                else
                {
                    string msgRet = "";
                    if (!IsCorrectDocument(paramIn.document)) //String.IsNullOrEmpty(paramIn.document))
                    {
                        //Added para controlar que si es una CI no viene doc inciial => Le agrego una carátula PDF para 
                        //seguir con mismo proceso que trámite normal. En cada NOtaria SIEMPRE el id tramide = 1 => CI
                        // Entonces si es 1 => agrego caraturla y sigo
                        if (paramIn.procedureId == 1)
                        {
                            paramIn.document = Global.CARATULA_CI;
                        }
                        else
                        {
                            msgRet += (string.IsNullOrEmpty(msgRet) ? "Documento Nulo o de formato incorrecto. Debe ser PDF!" :
                                "|Documento Nulo o de formato incorrecto. Debe ser Word o PDF!");
                        }
                    }
                    if (!IsCorrectMail(paramIn.mail)) //String.IsNullOrEmpty(paramIn.mail))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "Mail Nulo o Mal Formado" : "|Mail Nulo o Mal Formado"); ;
                    }
                    if (!IsCorrectRut(paramIn.rut)) // (String.IsNullOrEmpty(paramIn.rut))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "Rut Nulo o Incorrecto" : "|Ru Nulo o Incorrecto");
                    }
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, 
                                (new NotarizeResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                    "Problemas para notarizar. [" + msgRet + "]", null)));
                    }
                }
                
                LOG.Debug("NotarizeController.InitProcedureOne - Inicio procedure para mail = " + paramIn.mail);
                //1.1) Chequeo si existe, sino lo creo en NvUser
                NvUsers _NvUser = AdministradorNvUsers.CheckOrCreateNvUser(paramIn.mail, paramIn.rut);
                if (_NvUser == null)
                {
                    LOG.Error("NotarizeController.InitProcedureOne - Imposible registrar mail...");
                    return Request.CreateResponse(HttpStatusCode.Conflict, 
                        (new NotarizeResponseModel(Errors.IRET_API_CONFLIC, "Problemas para registrar mail. Reintente...", trackId)));
                }

                //2) Get QR de CI para retornar
                string sQRCI;
                string sTrackIdCI = GetQRTokenCI(_NvUser.Rut, _NvUser.Mail, out sQRCI);
                if (string.IsNullOrEmpty(sQRCI) || string.IsNullOrEmpty(sTrackIdCI))
                {
                    LOG.Error("NotarizeController.InitProcedureOne - Error creando TrackIdCI o QR para CI");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new NotarizeResponseModel(Errors.IRET_API_ERR_INIT_CI, "Error creando TraciIdCI o QR para CI", null)));
                }

                //2) Genero TraciId Unico de trámite
                trackId = AdministradorNvUsersProcedures.GetTrackIdUnique();
                if (string.IsNullOrEmpty(trackId))
                {
                    LOG.Error("NotarizeController.InitProcedureOne - TrackId generado invalido [Null]");
                    return Request.CreateResponse(HttpStatusCode.Conflict, 
                        (new NotarizeResponseModel(Errors.IRET_API_CONFLIC, "Problemas para crear id de transaccion unico. Reintente...", 
                        null)));
                }

                //3) Grabo NvUsersProcedures con status = 0 => Recibido
                NvUsersProcedures NvUsersProceduresCreated;
                //string msgerr;
                int provideridout;

                int ret = AdministradorNvUsersProcedures.CreateNvUserProcedure(trackId, _NvUser.Id, paramIn.notaryId, paramIn.procedureId,
                                                              paramIn.document, paramIn.message, sTrackIdCI, sQRCI, 
                                                              out provideridout, out msgerr, out NvUsersProceduresCreated);

                if (ret < 0 || NvUsersProceduresCreated == null)
                {
                    LOG.Error("NotarizeController.InitProcedureOne - Error creando procedure para iniciar proceso [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed, 
                        (new NotarizeResponseModel(Errors.IRET_ERR_NVPROCEDURE_NO_SAVED, "Error creando procedure para iniciar proceso[" + msgerr + "]", 
                        null)));
                }

                //Si todo funciono ok => Genro respuesta para retornar.
                response = new InitNotarizeResponseModel(Errors.IRET_API_OK, null, trackId, sTrackIdCI, null);

                //Informo x mail
                string[] strbody = new string[2];
                strbody[0] = "Usted a iniciado el trámite <b>" + NvUsersProceduresCreated.NameProcedure + "</b>";
                strbody[1] = "El codigo unico del tramite para consultas es: <b>" + NvUsersProceduresCreated.TrackId + "</b>";
                libs.Utils.NVNotify(paramIn.mail, "Inicio de tramite", strbody, null, null, null);

            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.InitProcedureOne Error Iniciando trámite: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, 
                    (new NotarizeResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconcido procesando inicio de tramite ["
                                                  + ex.Message + "]", null)));
            }

            //return  JsonConvert.SerializeObject
            //(new NotarizeResponseModel(0, "Proceso de notarizacion iniciado!"));
            return Request.CreateResponse(HttpStatusCode.OK, response);

        }

        /// <summary>
        /// Hace la notarizacion, tomando como base el mail, dado que esta destinado a notarizar un trámite sin loguearse. 
        /// Se debe chequear que el status sea correcto antes de seguir.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/NotarizeOne")]
        [HttpPost]
        public object NotarizeOne([FromBody] object param)
        {
            string msgerr = null;
            NotarizeOneRequestModel paramIn = null;
            try
            {
                LOG.Debug("NotarizeController.NotarizeOne - IN...");
                //1) Deserializo parámetros     
                if (param == null)
                {
                    return (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null));
                }
                string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                LOG.Debug("NotarizeController.Notarize - Deserializo ParamIn => " + strIn);
                paramIn = JsonConvert.DeserializeObject<NotarizeOneRequestModel>(strIn);

                if (paramIn == null)
                {
                    LOG.Debug("NotarizeController.Notarize - Error deserializando parametros de entrada [Null]");
                    return (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST,
                                        "Problemas para notarizar. Complete y reintente...", paramIn.trackid));
                }
                else
                {
                    string msgRet = "";
                    if (string.IsNullOrEmpty(paramIn.trackid))
                    {
                        msgRet += "|TrackId Nulo!";
                    }
                    if (String.IsNullOrEmpty(paramIn.document))
                    {
                        msgRet += "|Documento";
                    }
                    if (string.IsNullOrEmpty(paramIn.mail))
                    {
                        msgRet += "|Mail Nulo!";
                    }
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST,
                                "Problemas para notarizar. [" + msgRet + "]", null));
                    }
                }

                //2) Verifico status del trámite para ver si sigo, sino respondo que aun no se puede
                NvUsersProcedures nvUsersProcedures = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(paramIn.trackid, out msgerr);

                if (nvUsersProcedures == null)
                {
                    LOG.Error("NotarizeController.Notarize - Trámite no existente trackid = " + paramIn.trackid + " o problemas para obtenerlo");
                    return (new NotarizeResponseModel(Errors.IRET_ERR_USER_PROCEDURE_NOT_FOUND,
                                                      "Trámite no existente para este TrackId o problemas para obtenerlo!", paramIn.trackid));
                }
                else if (nvUsersProcedures.Status != StatusNvUsersProcedures.STATUS_READY_TO_NOTARIZE)
                {
                    LOG.Error("NotarizeController.Notarize - Trámite con trackid = " + paramIn.trackid +
                                " incompleto para notarizar. Status Actual = " + nvUsersProcedures.Status);
                    return (new NotarizeResponseModel(Errors.IRET_ERR_PROCEDURE_NO_READY_TO_NOTARIZE,
                                                       "Trámite con trackid = " + paramIn.trackid +
                                " incompleto para notarizar. Status Actual = " + nvUsersProcedures.Status, paramIn.trackid));
                }

                //3) Si todo está ok, hago update de NvProcedure y envío a quien corresponda
                NvUsersProcedures NvUsersProceduresUpdated;
                //string msgerr;
                int provideridout;

                int ret = AdministradorNvUsersProcedures.UpdateForInitNotarize(paramIn.trackid, paramIn.mail, paramIn.notaryId,
                                paramIn.procedureId, paramIn.document, paramIn.evidences, paramIn.payment, paramIn.message,
                                out provideridout, out msgerr, out NvUsersProceduresUpdated);

                if (ret < 0 || NvUsersProceduresUpdated == null)
                {
                    LOG.Error("NotarizeController.Notarize - Error update para iniciar notarizacion [" + msgerr + "]");
                    return (new NotarizeResponseModel(Errors.IRET_ERR_NVPROCEDURE_NO_SAVED,
                                            "Problemas para actualizar tramite e iniciar notarizacion", paramIn.trackid));
                }

                //4) De acuerdo a lo seleccionado se envia a notarizar al notario seleccionado por el provider adecuado
                string _UrlBase = AdministradorNvConfig.GetUrlBase(provideridout); //Hacer primero HashTable de providers para acceder rapido
                List<Evidence> evidences = AdministradorNvConfig.GenerateListEvidences(paramIn.evidences);
                string[] listFiles = new string[evidences.Count + 1];
                listFiles[0] = paramIn.document;
                listFiles[1] = ExtractCIPdf(evidences[0]);
                string _DocumentWithEvidences = DEHelper.MergeFiles(listFiles);
                NotarizeProviderResponsetModel response = NvProviderHelper.Notarize(_UrlBase, paramIn.trackid, paramIn.procedureId,
                                                                        _DocumentWithEvidences, evidences);

                //5) Se procesa respuesta
                //   5.1) Actualiza NvUsersProcedures con status recibido
                if (response != null && response.code == 200)
                {
                    ret = AdministradorNvUsersProcedures.SetNvUserProcedureNotarizing(paramIn.trackid);
                    if (ret < 0)
                    {
                        msgerr = "Error actualizando estado de tramite!";
                    }
                }
                else
                {
                    ret = Errors.IRET_ERR_INFORMED_PROVIDER_SERVICE;
                    msgerr = "Error informado por el provider en proceso de notarizacion.";
                }
                //   5.2) Retorno resultado para informar al usuario(y envio mail indicando)
                if (ret < 0)
                {
                    return (new NotarizeResponseModel(ret, msgerr, null));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.Notarize Error: " + ex.Message);
                return (new NotarizeResponseModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconcido procesando notarización ["
                                                  + ex.Message + "]", null));
            }

            //return  JsonConvert.SerializeObject
            //(new NotarizeResponseModel(0, "Proceso de notarizacion iniciado!"));
            return (new NotarizeResponseModel(Errors.IRET_OK, "Proceso de notarizacion iniciado!", paramIn.trackid));

        }

        /// <summary>
        /// Retorna status del tramite, consultando cada servicio externo relacionado, como CI, FLow y notario
        /// </summary>
        /// <param name="trackid"></param>
        /// <returns></returns>
        [Route("api/GetStatusOneNotarize")]
        [HttpGet]
        public object GetStatusOneNotarize(string trackid)
        {
            string trackId = null;
            string msgerr = null;
            bool _isMailValidated = false;  
            try
            {
                LOG.Debug("NotarizeController.GetStatusOneNotarize - IN...[trackid=" + trackid + "]");
                //1) Deserializo parámetros     
                if (string.IsNullOrEmpty(trackid))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new NotarizedResponseModel(Errors.IRET_API_BAD_REQUEST,
                        "TrackId consultado no puede ser nulo", trackid, null, null, null, 0, 0, 0, _isMailValidated, 0, null, null, 0, null)));
                }
                //2) Recupero NvProcedure
                NvUsersProcedures procedure = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr);
                bool bRetrieveAgain = false;
                //Si es esta en estado Notarizando = 4 => chequeo en linea con notario para determinar si ya tenemos respuesta.
                if (procedure != null)
                {
                    //Chequeo verificacion 
                    _isMailValidated = AdministradorNvUsers.IsUserEnabled(procedure.IdUser);

                    //Si esta en status pendietne de validacion de mail => igual lo dejo hacer la CI en paralelo
                    if (procedure.Status == StatusNvUsersProcedures.STATUS_CI_PENDING ||
                        procedure.Status == StatusNvUsersProcedures.STATUS_MAIL_VERIFY_PENDING)
                    {
                        //1.- Recupero 1 o mas CI pendientes de finalizacion relacionados con el Tramite
                        List<string> listCIforProcedure =
                            AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByIdProcedure(procedure.Id);
                        int ret = 0;
                        string _rd;
                        if (listCIforProcedure != null && listCIforProcedure.Count > 0)
                        {
                            string[] listMailsAlarms = Properties.Settings.Default.NvListMailAlarmWebhooks.Split(';');
                            //2.- Por cada TrackidCI recuperado 
                            foreach (string item in listCIforProcedure)
                            {
                                //3.- Consulto status en servicio CI (local en este caso)
                                ret = GetStatusCI(item, out _rd);
                                if (ret == 1) //Status devuelto por CI es OK => Actualizo NvUserProcedure
                                {
                                    //4.- Si completo el CI => Actualizo estado en Tramite
                                    Hashtable htRD;
                                    //4.1.- Recupero RDEnvelope
                                    bool bretparse = ParseRDEnvelope(_rd, out htRD);

                                    if (!bretparse || htRD == null || htRD.Count == 0)
                                    {
                                        //4.1.1.- Si no lo consigue => Manda mail avisando 
                                        LOG.Warn("NotarizeController.GetStatusOneNotarize - Error parseando param recibido en GetStatusCI con trackid => " + item);
                                        bool bret = libs.Utils.SendMail(listMailsAlarms, "Atención - Error en GetStatusCI", "Error parseando param recibido en GetStatusCI", null, null, null);
                                    }
                                    else
                                    {
                                        //4.1.2.- Si lo consigue => Actualiza NvUsersProceduresEvidences y NvUsersProcedures  
                                        string trackidci;
                                        NvUsersProcedures nvup;
                                        int iret = AdministradorNvUsersProcedures.UpdateInfoCIinBD(_rd, htRD, out trackidci, out nvup);
                                        if (iret < 0)
                                        {
                                            //4.1.2.1.- Si hay error => MAnda mail avisando
                                            LOG.Warn("NotarizeController.GetStatusOneNotarize - Error parseando param recibido en GetStatusCI con trackid => " + item);
                                            bool bret = libs.Utils.SendMail(listMailsAlarms, "Atención - Error en GetStatusCI CI",
                                                "Error parseando param recibido en GetStatusCI - TrackIdCI = " +
                                                (string.IsNullOrEmpty(trackidci) ? "NULL" : trackidci), null, null, null);
                                        } else
                                        {
                                            LOG.Warn("NotarizeController.GetStatusOneNotarize - UpdateInfoCIinBD OK!");
                                            bRetrieveAgain = true;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Los pasos son:
                    //  1.- Si no se creo la orden de pago => Se crea y se devuelve esos datos en esta respuesta
                    //  2.- Si ya fue generada => se chequea status en payment y devuelve resultado aqui
                    if (procedure.Status == StatusNvUsersProcedures.STATUS_PAYMENT_PENDING && _isMailValidated)
                    {
                        LOG.Debug("NotarizeController.GetStatusOneNotarize - Status = " + procedure.Status + " => Verificamos via proveedor pagos...");
                        string[] listMailsAlarms = Properties.Settings.Default.NvListMailAlarmWebhooks.Split(';');
                        //Valores posibles procedure.StatusPayment
                        //  0 - Aun no se genero la order
                        //  1 - Se genero pero esta pendiente aun
                        //  2 - Se completo el pago y debe estar el detalle en PaymentInfo
                        switch (procedure.PaymentStatus)
                        {
                            case 0: //=> Aun no se genero order => Creo order, update y retorno
                                int ret = AdministradorNvUsersProcedures.CreatePaymentOrderForProcedure(procedure, procedure.TrackId);
                                if (ret < 0)
                                {
                                    LOG.Error("NotarizeController.GetStatusOneNotarize - Error generando orden de pago [" + ret + "]");
                                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                                                    (new NotarizedResponseModel(Errors.IRET_ERR_CREATING_PAYMENT_ORDER,
                                                    "Error creando orden de pago. Reintente...", trackid, null, null, null, 0, 0, 0, _isMailValidated, 0,
                                                    procedure.PaymentUrl, procedure.PaymentToken, 0, null)));
                                } else
                                {
                                    bRetrieveAgain = true;
                                }
                                break;
                            case 1: //=> Ya creada => getstatus, update y retorno
                                ret = AdministradorNvUsersProcedures.UpdatePaymentOrderForProcedure(procedure, procedure.TrackId);
                                if (ret < 0)
                                {
                                    LOG.Error("NotarizeController.GetStatusOneNotarize - Error consultando status orden de pago [" + ret + "]");
                                    return Request.CreateResponse(HttpStatusCode.OK,
                                                    (new NotarizedResponseModel(Errors.IRET_ERR_STATUS_PAYMENT_INCORRECT,
                                                    "Orden de Pago creada. Complete el pago y reintente...", trackid, null, null, null,
                                                    procedure.Status, procedure.IdNotary, procedure.IdProcedure, _isMailValidated,
                                                    procedure.PaymentStatus, procedure.PaymentUrl, procedure.PaymentToken, 0, null)));
                                }
                                else
                                {
                                    bRetrieveAgain = true;
                                }
                                break;
                            default: //2 => Ya pagado => retorno
                                if (procedure.PaymentStatus == 2) //Ya pagado => Dejo como está para luego armar la respuesta
                                {
                                    LOG.Error("NotarizeController.GetStatusOneNotarize - Tramite ya pagado con fecha = " +
                                        (string.IsNullOrEmpty(procedure.PaymentDate) ? "Fecha No Existe" : procedure.PaymentDate)); 
                                }
                                bRetrieveAgain = true;
                                break;
                        }
                    } else
                    {
                        if (_isMailValidated)
                        {
                            LOG.Warn("NotarizeController.GetStatusOneNotarize - Tramite trackid = " + procedure.TrackId + 
                                        " pendiente de pago, pero mail no confirmado... Aborta!");
                        }
                    }

                    //Si se completo CI y Pago => Mando a notarizar
                    if (procedure.Status == StatusNvUsersProcedures.STATUS_READY_TO_NOTARIZE && _isMailValidated)
                    {
                        LOG.Debug("NotarizeController.GetStatusOneNotarize - Status = " + procedure.Status + " => Enviamos a notarizar...");
                        int ret = AdministradorNvUsersProcedures.SendProcedureToNotarize(procedure);
                        if (ret < 0)
                        {
                            LOG.Error("NotarizeController.GetStatusOneNotarize - Error enviando a notarizar [" + ret + "]");
                            return Request.CreateResponse(HttpStatusCode.InternalServerError,
                                            (new NotarizedResponseModel(Errors.IRET_ERR_CALLING_PROVIDER_SERVICE,
                                            "Error enviando a notarizar. Reintente...", trackid, null, null, null, 0,
                                            procedure.IdNotary, procedure.IdProcedure, _isMailValidated, 0, 
                                            null, null, 0, null)));
                        }
                        else
                        {
                            bRetrieveAgain = true;
                        }
                    }
                    else
                    {
                        if (_isMailValidated)
                        {
                            LOG.Warn("NotarizeController.GetStatusOneNotarize - Tramite trackid = " + procedure.TrackId +
                                        " listo para notarizar, pero mail no confirmado... Aborta!");
                        }
                    }

                    if (procedure.Status == StatusNvUsersProcedures.STATUS_IN_PROCESS_NOTARIZING)
                    {
                        LOG.Debug("NotarizeController.GetStatusOneNotarize - Status = " + procedure.Status + " => Verificamos via provider...");
                        //3) LLamo a servicio para revisar status

                        //3.1) Debo con el trackid saber la url donde consultar 
                        string _UrlBase = AdministradorNvConfig.GetUrlBase(procedure.IdProvider);
                        //3.2) Consulto en https://fea-ext.notarialeiva.cl:7070/ServicioREST/api/GetStatusNotarize/8b1b9985925f447ab19180e10c283687
                        LOG.Debug("NotarizeController.GetStatusOneNotarize - Consultando via URL = " + _UrlBase);
                        NotarizedProviderResponseModel response = NvProviderHelper.GetStatusNotarize(_UrlBase, trackid);
                        bRetrieveAgain = true;
                    }
                }

                //Recupero de nuevo datos para informar si se consulto d enuevo al notario sino sigo con lo que ya recupere
                if (bRetrieveAgain)
                {
                    LOG.Debug("NotarizeController.GetStatusOneNotarize - Recupero d enuevo desde BD porque se consulto a notario...");
                    procedure = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr);
                }

                if (procedure == null)
                {
                    LOG.Error("NotarizeController.GetStatusOneNotarize - Tramite no encontrado");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        (new NotarizedResponseModel(Errors.IRET_ERR_USER_PROCEDURE_NOT_FOUND,
                        "Tramite no encontrado con TrackId = " + trackid + ". Reintente...", trackid, null, null, null, 0, 0, 0, _isMailValidated, 0, 
                        null, null, 0, null)));
                }
                else
                {
                    if (procedure.Status != StatusNvUsersProcedures.STATUS_NOTARIZED)
                    {
                        //"Status actual = " + procedure.Status.ToString() +
                        //                            "|Status Payment = " + procedure.PaymentStatus.ToString() +
                        //                            "|Status Notary = " + procedure.StatusNotary.ToString() +
                        //                            "|Status Msg Notary = " +
                        //                            (string.IsNullOrEmpty(procedure.StatusMsgNotary) ? "S/C" : procedure.StatusMsgNotary.ToString())
                        return Request.CreateResponse(HttpStatusCode.OK,
                                          (new NotarizedResponseModel(Errors.IRET_ERR_PROCEDURE_PENDING,
                                                    (!_isMailValidated ? "Warning: Falta Verificar mail. Debe completar este paso para avanzar...":null),
                                                    trackid, null, null, null, procedure.Status, procedure.IdNotary, procedure.IdProcedure, _isMailValidated,
                                                    procedure.PaymentStatus, procedure.PaymentUrl, procedure.PaymentToken, procedure.StatusNotary, 
                                                    (string.IsNullOrEmpty(procedure.StatusMsgNotary) ? "S/C" : procedure.StatusMsgNotary.ToString()))));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                                          (new NotarizedResponseModel(Errors.IRET_API_OK,
                                           "Notarizado con exito!", trackid, null,
                                           procedure.UrlValidator, procedure.NotaryIdValidator, procedure.Status, procedure.IdNotary, procedure.IdProcedure, 
                                              _isMailValidated, procedure.PaymentStatus, procedure.PaymentUrl, procedure.PaymentToken, procedure.StatusNotary,
                                                    (string.IsNullOrEmpty(procedure.StatusMsgNotary) ? "S/C" : procedure.StatusMsgNotary.ToString()))));
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.GetStatusOneNotarize Error: " + ex.Message);
                return (new NotarizedResponseModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido procesando Get Status One Notarize ["
                                                  + ex.Message + "]", trackid, null, null, null, 0, 0, 0, _isMailValidated, 0, null, null, 0, null));
            }
        }


        /// <summary>
        /// Devuelve el tramite con toda la información adicional (eviudencias, pagos, etc) 
        /// para poder mostrar toda la ficha del mismo en el front end
        /// </summary>
        /// <param name="trackid"></param>
        /// <returns></returns>
        [Route("api/GetProcedure")]
        [HttpGet]
        public object GetProcedure(string trackid)
        {
            string trackId = null;
            string msgerr = null;
            bool _isMailValidated = false;
            try
            {
                LOG.Debug("NotarizeController.GetProcedure - IN...[trackid=" + trackid + "]");
                //1) Deserializo parámetros     
                if (string.IsNullOrEmpty(trackid))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new NotarizedResponseModel(Errors.IRET_API_BAD_REQUEST,
                        "TrackId consultado no puede ser nulo", trackid, null, null, null, 0, 0, 0, _isMailValidated, 0, null, null, 0, null)));
                }
                //2) Recupero NvProcedure
                NvUsersProcedures procedure = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr);             

                if (procedure == null)
                {
                    LOG.Error("NotarizeController.GetProcedure - Tramite no encontrado");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        (new NotarizedResponseModel(Errors.IRET_ERR_USER_PROCEDURE_NOT_FOUND,
                        "Tramite no encontrado con TrackId = " + trackid + ". Reintente...", trackid, null, null, null, 0, 0, 0, _isMailValidated, 0, 
                        null, null, 0, null)));
                }
                else
                {
                    //Chequeo verificacion 
                    _isMailValidated = AdministradorNvUsers.IsUserEnabled(procedure.Id);

                    ProcedureResponseModel response;
                    int ret = AdministradorNvUsersProcedures.CreateResponseTotalProcedure(procedure, out response);
                    if (ret < 0 && response != null)
                    {
                        LOG.Error("NotarizeController.GetProcedure - Error generando response de tramite con trackid = " + trackid +
                                    " [" + ret + " - response==null => " + (response==null).ToString() + "]");
                        return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new NotarizedResponseModel(Errors.IRET_ERR_SERIALIZE_RESPONSE,
                            "Tramite no encontrado con TrackId = " + trackid + ". Reintente...", trackid, null, null, null, 0, 0, 0, 
                            _isMailValidated, 0, null, null, 0, null)));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,response);
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.GetProcedure Error: " + ex.Message);
                return (new NotarizedResponseModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido procesando Get Status One Notarize ["
                                                  + ex.Message + "]", trackid, null, null, null, 0, 0, 0, _isMailValidated, 0, null, null, 0, null));
            }
        }


        /// <summary>
        /// Ofrce descarga de PDF notarizado 
        /// </summary>
        /// <param name="trackid"></param>
        /// <returns></returns>
        [Route("api/DownloadProcedure")]
        [HttpGet]
        public object DownloadProcedure(string trackid)
        {
            string msgerr;
            try
            {
                LOG.Debug("NotarizeController.DownloadProcedure - IN...");
                //1) Deserializo parámetros     
                if (string.IsNullOrEmpty(trackid))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST,
                        "TrackId consultado no puede ser nulo", trackid)));
                }

                //2) Recupero tramite y si está statis = Notarzed => Devuelvo sino msg error
                NvUsersProcedures nvUP = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr);

                if (nvUP == null)
                {
                    LOG.Error("NotarizeController.DownloadProcedure Error: TrackId consultado no existe");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        (new NotarizeResponseModel(Errors.IRET_API_ERR_NOT_FOUND,
                        "TrackId consultado no existe", trackid)));
                }
                else if (nvUP.Status != StatusNvUsersProcedures.STATUS_NOTARIZED)
                {
                    LOG.Error("NotarizeController.DownloadProcedure Error: TrackId consultado aun pendiente de notarizacion [Status=" + nvUP.Status + "]");
                    return Request.CreateResponse(HttpStatusCode.PartialContent,
                        (new NotarizeResponseModel(Errors.IRET_ERR_PROCEDURE_PENDING,
                        "TrackId consultado aun pendiente de notarizacion [Status=" + nvUP.Status + "]", trackid)));
                }
                else if (string.IsNullOrEmpty(nvUP.DocumentB64Notarized)) 
                {
                    LOG.Error("NotarizeController.DownloadProcedure Error: TrackId consultado con status ok pero documento inexistente [" + trackid + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new NotarizeResponseModel(Errors.IRET_ERR_PROCEDURE_PENDING,
                        "TrackId consultado con status ok pero documento inexistente", trackid)));
                } else  //Todo ok => Armo salida 
                {
                    //string sfile = System.IO.File.ReadAllText(@"C:\tmp\doc.base64.txt");
                    LOG.Debug("NotarizeController.DownloadProcedure - Descargando " + nvUP.TrackId + ".pdf");
                    byte[] byFile = Convert.FromBase64String(nvUP.DocumentB64Notarized);
                    MemoryStream ms = new MemoryStream(byFile);
                    //var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/test.docx"); ;
                    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                    //var stream =    new FileStream(path, FileMode.Open);
                    result.Content = new StreamContent(ms); // (stream);
                    //result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    //result.Content.Headers.ContentDisposition.FileName = nvUP.TrackId + ".pdf"; // Path.GetFileName(path);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.ContentLength = ms.Length;// stream.Length;
                    LOG.Debug("NotarizeController.DownloadProcedure - Descarga OK!");
                    return result;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.DownloadProcedure Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                       (new NotarizeResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR,
                       "Error descargadno tramite [" + ex.Message + "]", trackid)));
            }
        }
        
        /// <summary>
        /// Ofrce descarga de QR para CI 
        /// </summary>
        /// <param name="trackid CI"></param>
        /// <returns></returns>
        [Route("api/DownloadQRCI")]
        [HttpGet]
        public object DownloadQRCI(string trackidci)
        {
            try
            {
                LOG.Debug("NotarizeController.DownloadQRCI - IN...");
                //1) Deserializo parámetros     
                if (string.IsNullOrEmpty(trackidci))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST,
                        "TrackId consultado no puede ser nulo", trackidci)));
                }

                //2) Recupero tramite y si está statis = Notarzed => Devuelvo sino msg error
                NvUsersProceduresEvidences nvUPE = 
                    AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByTrackIdCI(trackidci);

                if (nvUPE == null)
                {
                    LOG.Error("NotarizeController.DownloadQRCI Error: TrackId consultado no existe");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        (new NotarizeResponseModel(Errors.IRET_API_ERR_NOT_FOUND,
                        "TrackId consultado no existe", trackidci)));
                }
                else if (string.IsNullOrEmpty(nvUPE.QRCI))
                {
                    LOG.Error("NotarizeController.DownloadQRCI Error: TrackId consultado con QR nulo [" + trackidci + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new NotarizeResponseModel(Errors.IRET_ERR_PROCEDURE_PENDING,
                        "TrackId consultado con QR nulo", trackidci)));
                }
                else  //Todo ok => Armo salida 
                {
                    ////Debe verificar si el QR sigue siendo valido apra verificar sino regenerarlo 
                    ////  - iniciar nuevo proceso CI
                    ////  - Update en BD
                    //CheckValidityQR(nvUPE, out nvUPE);

                    //string sfile = System.IO.File.ReadAllText(@"C:\tmp\doc.base64.txt");
                    LOG.Debug("NotarizeController.DownloadQRCI - Descargando " + trackidci + ".pdf");
                    byte[] byFile = Convert.FromBase64String(nvUPE.QRCI);
                    MemoryStream ms = new MemoryStream(byFile);
                    //var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/test.docx"); ;
                    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                    //var stream =    new FileStream(path, FileMode.Open);
                    result.Content = new StreamContent(ms); // (stream);
                    //result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    //result.Content.Headers.ContentDisposition.FileName = trackidci + ".jpg"; // Path.GetFileName(path);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                    result.Content.Headers.ContentLength = ms.Length;// stream.Length;
                    LOG.Debug("NotarizeController.DownloadQRCI - Descarga OK!");
                    return result;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.DownloadQRCI Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                       (new NotarizeResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR,
                       "Error descargadno tramite [" + ex.Message + "]", trackidci)));
            }
        }

        /// <summary>
        /// Reinicia proceso de CI y actualiza BD 
        /// </summary>
        /// <param name="trackid CI"></param>
        /// <returns></returns>
        [Route("api/ReinitProcessCI")]
        [HttpGet]
        public object ReinitProcessCI(string trackidci)
        {
            string trackidcinew;
            string trackidprocedurephater;
            try
            {
                LOG.Debug("NotarizeController.ReinitProcessCI - IN...");
                //1) Deserializo parámetros     
                if (string.IsNullOrEmpty(trackidci))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new InitNotarizeResponseModel(Errors.IRET_API_BAD_REQUEST,
                        "TrackId consultado no puede ser nulo", null, null, null)));
                }

                //2) Recupero tramite y si está statis = Notarzed => Devuelvo sino msg error
                NvUsersProceduresEvidences nvUPE =
                    AdministradorNvUsersProceduresEvidences.GetNvUserProcedureEvidenceByTrackIdCI(trackidci, true);

                if (nvUPE == null)
                {
                    LOG.Error("NotarizeController.ReinitProcessCI Error: TrackId consultado no existe");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        (new InitNotarizeResponseModel(Errors.IRET_API_ERR_NOT_FOUND,
                        "TrackId consultado no existe", null, null, null)));
                }
                else if (nvUPE.Status == 1 && !string.IsNullOrEmpty(nvUPE.Value))
                {
                    LOG.Debug("NotarizeController.ReinitProcessCI - Sale porque status de CI con trackid = " + trackidci + 
                        " es completa! No se puede reiniciar...");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                               (new InitNotarizeResponseModel(Errors.IRET_API_ERR_CI_COMPLETED,
                                   "CI completa. No pude reiniciar proceso de CI", null, trackidci, null)));

                } else 
                {
                    LOG.Debug("NotarizeController.ReinitProcessCI - Generando nuevo Inicio de CI para trackid = " + trackidci);
                    trackidprocedurephater = nvUPE.NvUsersProcedures.TrackId;
                    LOG.Debug("NotarizeController.ReinitProcessCI - Tramite trackid = " + trackidprocedurephater);
                    NvUsers user = AdministradorNvUsers.BuscarNvUserById(nvUPE.NvUsersProcedures.IdUser);
                    string qrnew;
                    trackidcinew = GetQRTokenCI(user.Rut, user.Mail, out qrnew);
                    LOG.Debug("NotarizeController.ReinitProcessCI - Nuevo trackidci = " +
                                        (string.IsNullOrEmpty(trackidcinew) ? "NULL" : trackidcinew));
                    if (string.IsNullOrEmpty(trackidcinew) || string.IsNullOrEmpty(qrnew)) {
                        return Request.CreateResponse(HttpStatusCode.Conflict,
                            (new InitNotarizeResponseModel(Errors.IRET_API_ERR_INIT_CI,
                                "Error generando Tx CI o QR", null, trackidci, null)));
                    }
                    else
                    {
                        nvUPE.TrackIdExt = trackidcinew;
                        nvUPE.QRCI = qrnew;
                        LOG.Debug("NotarizeController.ReinitProcessCI - Update en BD...");
                        int ret = AdministradorNvUsersProceduresEvidences.UpdateNvUserProcedureEvidence(nvUPE);
                        if (ret < 0)
                        {
                            LOG.Debug("NotarizeController.ReinitProcessCI - Error update en BD UpdateNvUserProcedureEvidence = " + ret.ToString()); 
                            return Request.CreateResponse(HttpStatusCode.InternalServerError,
                               (new InitNotarizeResponseModel(Errors.IRET_ERR_SAVE_DB,
                                   "Error grabando en BD nueva TrackId y QR de CI", null, trackidci, null)));

                        } else {
                            LOG.Debug("NotarizeController.ReinitProcessCI - PAr atrackid de tramite = " + trackidprocedurephater +
                                " se genero el nuevo trackidci = " + trackidcinew);
                            return Request.CreateResponse(HttpStatusCode.OK,
                               (new InitNotarizeResponseModel(Errors.IRET_API_OK,
                                   null, trackidprocedurephater, trackidcinew, null)));
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.ReinitProcessCI Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                       (new InitNotarizeResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR,
                       "Error descargadno tramite [" + ex.Message + "]", null, trackidci, null)));
            }
        }


#endregion ONE


        /// <summary>
        /// Inicializa el trámite en modo no logueado. Solo con el mail.
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="notaryid"></param>
        /// <param name="procedureid"></param>
        /// <returns></returns>
        [Route("api/InitProcedure")]
        [HttpGet]
        public object InitProcedure(string mail, int notaryid, int procedureid)
        {
            string trackId = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotarizeController.InitProcedure - IN...");
                //1) Deserializo parámetros     
                if (string.IsNullOrEmpty(mail))
                {
                    return (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST, "Mail nulo. Complete y reintente...", null));
                }
                LOG.Debug("NotarizeController.InitProcedure - Inicio procedure para mail = " + mail);
                //1.1) Chequeo si existe, sino lo creo en NvUser
                NvUsers _NvUser = AdministradorNvUsers.CheckOrCreateNvUser(mail, null);
                if (_NvUser == null)
                {
                    LOG.Error("NotarizeController.InitProcedure - Imposible registrar mail...");
                    return (new NotarizeResponseModel(Errors.IRET_API_CONFLIC, "Problemas para registrar mail. Reintente...", trackId));
                }

                //2) Genero TraciId Unico de trámite
                trackId = AdministradorNvUsersProcedures.GetTrackIdUnique();
                if (string.IsNullOrEmpty(trackId))
                {
                    LOG.Error("NotarizeController.InitProcedure - TrackId generado inválido [Null]");
                    return (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST, "Problemas para crear id de transaccion unico. Reintente...", trackId));
                }

                //3) Grabo NvUsersProcedures con status = 0 => Recibido
                NvUsersProcedures NvUsersProceduresCreated;
                //string msgerr;
                int provideridout;

                int ret = AdministradorNvUsersProcedures.CreateNvUserProcedure(trackId, _NvUser.Id, notaryid, procedureid, null, "S/C", null, null,
                                                                        out provideridout, out msgerr, out NvUsersProceduresCreated);

                if (ret < 0 || NvUsersProceduresCreated == null)
                {
                    LOG.Error("NotarizeController.InitProcedure - Error creando procedure para iniciar proceso [" + msgerr + "]");
                    return (new NotarizeResponseModel(Errors.IRET_ERR_NVPROCEDURE_NO_SAVED, "Problemas para crear inicio de trámite unico. Reintente...", trackId));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.InitProcedure Error Iniciando trámite: " + ex.Message);
                return (new NotarizeResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconcido procesando inicio de tramite ["
                                                  + ex.Message + "]", null));
            }

            //return  JsonConvert.SerializeObject
            //(new NotarizeResponseModel(0, "Proceso de notarizacion iniciado!"));
            return (new NotarizeResponseModel(Errors.IRET_API_OK, "Proceso de notarizacion iniciado!", trackId));

        }

        
        //Pasos para Notarizar:  
        //   Recibo como parametro el JSON con los datos:
        //   {
        //        {
        //          "userId": "1",
        //             "providerId": "1",
        //             "notaryId": "1",
        //             "procedureId": 1,
        //             "document": "JVBERi0xLjc...",
        //             "paymentstatus": "1",
        //             "payment": "Info de Payment",
        //             "evidences": [{
        //                            "name": "Certificacion de Identidad",
        //                             "type": 4,
        //                             "value": "<RDEnvelope><Rd ..."
        //                          }]
        //       }
        //        hago:
        //       1) Deserializo parámetros
        //          1.1) Check parametros entrada
        //       2) Genero TraciId Unico de trámite

        //       3) Grabo NvUsersProcedures con status = 0 => Recibido

        //       4) De acuerdo a lo seleccionado se envia a notarizar al notario seleccionado por el provider adecuado

        //       5) Se procesa respuesta
        //          5.1) Actualiza NvUsersProcedures con status recibido
        //          5.2) Retorno resultado para informar al usuario(y envio mail indicando)
        // 
        /// <summary>
        ///    Notariza 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/Notarize")]
        [HttpPost]
        public object Notarize([FromBody] object param)
        {
            string trackId = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotarizeController.Notarize - IN...");
                //1) Deserializo parámetros     
                if (param==null)
                {
                    return (new NotarizeResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos Complete y reintente...", null));
                }
                string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                LOG.Debug("NotarizeController.Notarize - Deserializo ParamIn => " + strIn);
                NotarizeRequestModel paramIn = JsonConvert.DeserializeObject<NotarizeRequestModel>(strIn);

                if (paramIn == null) 
                {
                    LOG.Debug("NotarizeController.Notarize - Error deserializando parametros de entrada [Null]");
                    return (new NotarizeResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Problemas para notarizar. Complete y reintente...", trackId));
                } else
                {
                    string msgRet = "";
                    if (String.IsNullOrEmpty(paramIn.document))
                    {
                        msgRet += "|Documento";
                    }
                    if (!AdministradorNvUsers.IsUserEnabled(paramIn.userId))
                    {
                        msgRet += "|Usuario Deshabilitado!" ;
                    }
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return (new NotarizeResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, 
                                "Problemas para notarizar. [" + msgRet + "]", null));
                    }
                }

                //2) Genero TraciId Unico de trámite
                trackId = AdministradorNvUsersProcedures.GetTrackIdUnique();

                if (string.IsNullOrEmpty(trackId))
                {
                    LOG.Error("NotarizeController.Notarize - TrackId generado inválido [Null]");
                    return (new NotarizeResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Problemas para crear id de trnsaccion unico. Reintente...", trackId));
                }
                //3) Grabo NvUsersProcedures con status = 0 => Recibido
                NvUsersProcedures NvUsersProceduresCreated;
                //string msgerr;
                int provideridout;
                
                int ret = AdministradorNvUsersProcedures.CreateNvUserProcedure(trackId, paramIn.userId, paramIn.notaryId, 
                    paramIn.procedureId, paramIn.document, paramIn.evidences, paramIn.payment, paramIn.message,
                    out provideridout, out msgerr, out NvUsersProceduresCreated);

                if (ret < 0 || NvUsersProceduresCreated == null)
                {
                    LOG.Error("NotarizeController.Notarize - Error creando procedure para notarizar [" + msgerr + "]");
                    return (new NotarizeResponseModel(Errors.IRET_ERR_NVPROCEDURE_NO_SAVED, "Problemas para crear id de trnsaccion unico. Reintente...", trackId));
                }

                //4) De acuerdo a lo seleccionado se envia a notarizar al notario seleccionado por el provider adecuado
                string _UrlBase = AdministradorNvConfig.GetUrlBase(provideridout); //Hacer primero HashTable de providers para acceder rapido
                List<Evidence> evidences = AdministradorNvConfig.GenerateListEvidences(paramIn.evidences);
                string[] listFiles = new string[evidences.Count + 1];
                listFiles[0] = paramIn.document;
                listFiles[1] = ExtractCIPdf(evidences[0]);
                string _DocumentWithEvidences = DEHelper.MergeFiles(listFiles);
                NotarizeProviderResponsetModel response = NvProviderHelper.Notarize(_UrlBase, trackId, paramIn.procedureId,
                                                                        _DocumentWithEvidences, evidences);

                //5) Se procesa respuesta
                //   5.1) Actualiza NvUsersProcedures con status recibido
                if (response != null && response.code == 200)
                {
                    ret = AdministradorNvUsersProcedures.SetNvUserProcedureNotarizing(trackId);
                    if (ret < 0)
                    {
                        msgerr = "Error actualizando estado de tramite!";
                    }
                } else
                {
                    ret = Errors.IRET_ERR_INFORMED_PROVIDER_SERVICE;
                    msgerr = "Error informado por el provider en proceso de notarizacion.";
                }
                //   5.2) Retorno resultado para informar al usuario(y envio mail indicando)
                if (ret < 0)
                {
                    return (new NotarizeResponseModel(ret, msgerr, null));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.Notarize Error: " + ex.Message);
                return (new NotarizeResponseModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconcido procesando notarización [" 
                                                  + ex.Message + "]", null));
            }

            //return  JsonConvert.SerializeObject
                    //(new NotarizeResponseModel(0, "Proceso de notarizacion iniciado!"));
            return (new NotarizeResponseModel(Errors.IRET_OK, "Proceso de notarizacion iniciado!", trackId));

        }


        [Route("api/Notarized")]
        [HttpPost]
        public object Notarized([FromBody] object param)
        {
            string trackId = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotarizeController.Notarized - IN...");
                //1) Deserializo parámetros     
                if (param == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                               (new NotarizeResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos Complete y reintente...", null)));
                    
                }
                string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                LOG.Debug("NotarizeController.Notarized - Deserializo ParamIn => " + strIn);
                NotarizedRequestModel paramIn = JsonConvert.DeserializeObject<NotarizedRequestModel>(strIn);

                if (paramIn == null)
                {
                    LOG.Debug("NotarizeController.Notarized - Error deserializando parametros de entrada [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                               (new NotarizeResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos Complete y reintente...", trackId)));
                }
                else
                {
                    string msgRet = "";
                    if (paramIn.code == 200)
                    {
                        if (String.IsNullOrEmpty(paramIn.document))
                        {
                            msgRet += "|Documento";
                        }
                        if (!string.IsNullOrEmpty(msgRet))
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest,
                                   (new NotarizeResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Problemas para notarizar. [" + msgRet + "]", null)));
                        }
                    }
                }

                //2) Recupero NvProcedure
                NvUsersProcedures procedure = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(paramIn.trackid, out msgerr);

                if (procedure == null)
                {
                    LOG.Error("NotarizeController.Notarize - Tramite no encontrado");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                               (new NotarizeResponseModel(Errors.IRET_ERR_USER_PROCEDURE_NOT_FOUND,
                                "Tramite no encontrado con TrackId = " + paramIn.trackid + ". Reintente...", paramIn.trackid)));
                }

                //3) Grabo NvUsersProcedures con status = 3 => Si llego, y save documento en doc notarizado y date en finalizado
                int ret = AdministradorNvUsersProcedures.SetNvUserProcedureNotarized(paramIn.code, paramIn.msgError, paramIn.trackid, paramIn.document,
                                                                                     paramIn.urlvalidator, paramIn.notaryidvalidator);
                if (ret < 0)
                {
                    msgerr = "Error actualizando estado de tramite!";
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                               (new NotarizeResponseModel(ret, msgerr, paramIn.trackid)));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                               (new NotarizeResponseModel(200, "Tramite actualizado con exito!", paramIn.trackid)));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.Notarized Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                               (new NotarizeResponseModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido procesando notarización ["
                                                  + ex.Message + "]", null)));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/GetStatusNotarize")]
        [HttpGet]
        public object GetStatusNotarize(string trackid)
        {
            string trackId = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotarizeController.GetStatusNotarize - IN...");
                //1) Deserializo parámetros     
                //if (param == null)
                if (string.IsNullOrEmpty(trackid))
                {
                    return Request.CreateResponse((HttpStatusCode)Errors.IRET_API_BAD_REQUEST, 
                        (new NotarizedProviderResponseModel(Errors.IRET_API_BAD_REQUEST, "TrackId consultado no puede ser nulo")));
                }
                //string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                //LOG.Debug("NotarizeController.GetStatusNotarize - Deserializo ParamIn => " + strIn);
                //NotarizedRequestModel paramIn = JsonConvert.DeserializeObject<NotarizedRequestModel>(strIn);

                //if (paramIn == null)
                //{
                //    LOG.Debug("NotarizeController.GetStatusNotarize - Error deserializando parametros de entrada [Null]");
                //    return Request.CreateResponse((HttpStatusCode)Errors.IRET_API_BAD_REQUEST, 
                //        (new NotarizedProviderResponseModel(Errors.IRET_API_BAD_REQUEST, 
                //                                        "Problemas para notarizar. Complete y reintente...")));
                //}
                //else
                //{
                //    string msgRet = "";
                //    if (String.IsNullOrEmpty(paramIn.document))
                //    {
                //        msgRet += "|Documento";
                //    }
                //    //if (!AdministradorNvUsers.IsUserEnabled(paramIn.userId))
                //    //{
                //    //    msgRet += "|Usuario Deshabilitado!";
                //    //}
                //    if (!string.IsNullOrEmpty(msgRet))
                //    {
                //        return (new NotarizedProviderResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                //                "Problemas para notarizar. [" + msgRet + "]"));
                //    }
                //}

                //2) Recupero NvProcedure
                NotarizedProviderResponseModel response = null;
                NvUsersProcedures procedure = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr);
                bool bRetrieveAgain = false;
                //Si es esta en estado Notarizando = 4 => chequeo en linea con notario para determinar si ya tenemos respuesta.
                if (procedure != null) {
                    if (procedure.Status == StatusNvUsersProcedures.STATUS_IN_PROCESS_NOTARIZING) { 

                        //3) LLamo a servicio para revisar status

                        //3.1) Debo con el trackid saber la url donde consultar 
                        string _UrlBase = AdministradorNvConfig.GetUrlBase(procedure.IdProvider);
                        //3.2) Consulto en https://fea-ext.notarialeiva.cl:7070/ServicioREST/api/GetStatusNotarize/8b1b9985925f447ab19180e10c283687
                        response = NvProviderHelper.GetStatusNotarize(_UrlBase, trackId);
                        bRetrieveAgain = true;
                    }
                } 

                //Recupero de nuevo datos para informar si se consulto d enuevo al notario sino sigo con lo que ya recupere
                if (bRetrieveAgain) procedure = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, out msgerr);

                if (procedure == null)
                {
                    LOG.Error("NotarizeController.Notarize - Tramite no encontrado");
                    return Request.CreateResponse((HttpStatusCode)Errors.IRET_ERR_USER_PROCEDURE_NOT_FOUND, 
                        (new NotarizedProviderResponseModel(Errors.IRET_ERR_USER_PROCEDURE_NOT_FOUND,
                        "Tramite no encontrado con TrackId = " + trackid + ". Reintente...")));
                } else
                {

                    if (procedure.Status == 3) //Ya estaba actalizado y entregado el notarizado => lleno y respondo
                    {
                        //Retorno lo indicado
                        return (new NotarizedProviderResponseModel(200, null, procedure.TrackId, procedure.DocumentB64Notarized,
                                                                   procedure.UrlValidator, procedure.NotaryIdValidator));
                    }
                    else if (response.code == 200 && string.IsNullOrEmpty(response.document))
                    {
                        //Update and Return 
                        //4) Grabo NvUsersProcedures con status = 3 si responde ok => Si llego, y save documento en doc notarizado y date en finalizado
                        int ret = AdministradorNvUsersProcedures.SetNvUserProcedureNotarized(response);
                        if (ret < 0)  //TODO
                        {
                            msgerr = "Error actualizando estado de tramite!";
                            return (new NotarizeResponseModel(ret, msgerr, null));
                        }
                        else
                        {
                            return response;
                        }
                    }
                    else
                    {
                        return response;
                    }
                }

                
               
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.Notarized Error: " + ex.Message);
                return (new NotarizeResponseModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido procesando notarización ["
                                                  + ex.Message + "]", null));
            }
        }

        /// <summary>
        /// Lista tramites disponibles para ofrecer
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/GetProceduresAvailables")]
        [HttpGet]
        public object GetProceduresAvailables()
        {
            List<NvProceduresAvailablesModel> listPA = null;
            try
            {
                LOG.Debug("NotarizeController.GetProceduresAvailables - IN...");
                //2) Recupero Procedures desde NvConfig ahora
                listPA = AdministradorNvConfig.GetNvProceduresAvailables();
                if (listPA==null)
                {
                    return (new NotarizeResponseModel(Errors.IRET_ERR_PROCEDURES_NO_CONFIGURED, 
                        Errors.SRET_ERR_PROCEDURES_NO_CONFIGURED, null));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.Notarized Error: " + ex.Message);
                return (new NotarizeResponseModel(Errors.IRET_ERR_DESCONOCIDO, "Error recuperando Tramites Disponibles ["
                                                  + ex.Message + "]", null));
            }
            return listPA;
        }

        [Route("api/Register")]
        [HttpPost]
        public object Register([FromBody] object param)
        {
            string trackId = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotarizeController.Register - IN...");
                //1) Deserializo parámetros     
                if (param == null)
                {
                    return (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos Complete y reintente..."));
                }
                string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                LOG.Debug("NotarizeController.Register - Deserializo ParamIn => " + strIn);
                RegisterRequestModel paramIn = JsonConvert.DeserializeObject<RegisterRequestModel>(strIn);

                if (paramIn == null)
                {
                    LOG.Debug("NotarizeController.Register - Error deserializando parametros de entrada [Null]");
                    return (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Problemas para registrar. Complete y reintente..."));
                }
                else
                {
                    string msgRet = "";
                    if (String.IsNullOrEmpty(paramIn.rut))
                    {
                        msgRet += "|Rut";
                    }
                    if (String.IsNullOrEmpty(paramIn.mail))
                    {
                        msgRet += "|Usuario Deshabilitado!";
                    }
                    if (String.IsNullOrEmpty(paramIn.password))
                    {
                        msgRet += "|Clave!";
                    }
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                "Problemas para registrar. [" + msgRet + "]"));
                    }
                }

                //2) Reviso que el usuario esté dado de alta por mail
                NvUsers user = AdministradorNvUsers.BuscarNvUserByMail(paramIn.mail);

                if (user != null)
                {
                    LOG.Error("NotarizeController.Register - Usuario ya registrado!");
                    return (new ResponseModel(Errors.IRET_API_ERR_USER_EXIST, "Usuario ya existe. Reintente con otro mail..."));
                }

                //3) Registro User
                NvUsers UserRegistered;

                int ret = AdministradorNvUsers.RegisterNvUser(paramIn.rut, paramIn.mail, paramIn.password,
                    paramIn.name, paramIn.phaterlastname, paramIn.motherlastname, out msgerr, out UserRegistered);

                if (ret < 0)
                {
                    LOG.Error("NotarizeController.Notarize - Error registrando usuario [" + msgerr + "]");
                    return (new ResponseModel(Errors.IRET_ERR_NVPROCEDURE_NO_SAVED, "Error registrando usuario[" + msgerr + "]. Reintente..."));
                }

                
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.Notarize Error: " + ex.Message);
                return (new ResponseModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconcido procesando notarización ["
                                                  + ex.Message + "]"));
            }

            return (new ResponseModel(200, "Registro ingresado con exito!"));

        }

        [Route("api/RegisterValidation")]
        [HttpPost]
        public object RegisterValidation([FromBody] object param)
        {
            string trackId = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotarizeController.RegisterValidation - IN...");
                //1) Deserializo parámetros     
                if (param == null)
                {
                    return (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos Complete y reintente..."));
                }
                string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                LOG.Debug("NotarizeController.RegisterValidation - Deserializo ParamIn => " + strIn);
                ValidationRegisterRequestModel paramIn = JsonConvert.DeserializeObject<ValidationRegisterRequestModel>(strIn);

                if (paramIn == null)
                {
                    LOG.Debug("NotarizeController.RegisterValidation - Error deserializando parametros de entrada [Null]");
                    return (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Problemas para verificar registro. Complete y reintente..."));
                }
                else
                {
                    string msgRet = "";
                    if (String.IsNullOrEmpty(paramIn.mail))
                    {
                        msgRet += "|Usuario Deshabilitado!";
                    }
                    if (String.IsNullOrEmpty(paramIn.code))
                    {
                        msgRet += "|Codigo!";
                    }
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                "Problemas para verificar registro. [" + msgRet + "]"));
                    }
                }

                //2) Reviso que el usuario esté dado de alta por mail
                NvUsers user = AdministradorNvUsers.BuscarNvUserByMail(paramIn.mail);

                if (user == null)
                {
                    LOG.Error("NotarizeController.RegisterValidation - Preregistro NO existe! - Por Mail = " + paramIn.mail);
                    return (new ResponseModel(Errors.IRET_ERR_PREREGISTER_NOT_EXIST, "Preregistro NO existe. Reinicie el proceso..."));
                }

                //3) Valido Registro User
              

                int ret = AdministradorNvUsers.ValidateRegisterNvUser(paramIn.mail, paramIn.code,  out msgerr);

                if (ret < 0)
                {
                    LOG.Error("NotarizeController.RegisterValidation - Error validando registo usuario [" + msgerr + "]");
                    return (new ResponseModel(Errors.IRET_ERR_NVPROCEDURE_NO_SAVED, "Error validando registo usuario[" + msgerr + "]. Reintente..."));
                }


            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.RegisterValidation Error Desconcido procesando validacion de registro : " + ex.Message);
                return (new ResponseModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconcido procesando validacion de registro ["
                                                  + ex.Message + "]"));
            }
            LOG.Error("NotarizeController.RegisterValidation OUT!");
            return (new ResponseModel(200, "Registro validado con exito!"));

        }

        [Route("api/MailValidation")]
        [HttpGet]
        public object MailValidation(string mail, string code) //[FromUri] object param)
        {
            string trackId = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotarizeController.MailValidation - IN...");
                
                //1)Chequeo parametroos
                if (string.IsNullOrEmpty(mail) || string.IsNullOrEmpty(code))
                {
                    LOG.Debug("NotarizeController.MailValidation - Error parametros de entrada [Null]");
                    return (new ResponseModel(Errors.IRET_API_BAD_REQUEST, "Problemas para verificar registro. Complete y reintente..."));
                }

                //2) Reviso que el usuario esté dado de alta por mail
                NvUsers user = AdministradorNvUsers.BuscarNvUserByMail(mail);

                if (user == null)
                {
                    LOG.Error("NotarizeController.MailValidation - Preregistro NO existe! - Por Mail = " + mail);
                    return Request.CreateResponse((HttpStatusCode)Errors.IRET_ERR_PREREGISTER_NOT_EXIST, 
                        (new ResponseModel(Errors.IRET_ERR_PREREGISTER_NOT_EXIST, "Preregistro NO existe. Reinicie el proceso...")));
                    
                }

                //3) Valido Registro User
                int ret = AdministradorNvUsers.ValidateMailNvUser(mail, code, out msgerr);

                if (ret == Errors.IRET_ERR_MAIL_VALIDATED)
                {
                    LOG.Error("NotarizeController.MailValidation - Mail ya validado!");
                    return Request.CreateResponse(HttpStatusCode.OK, 
                        (new ResponseModel(Errors.IRET_API_OK, "Mail ya validado!")));
                } else if (ret < 0)
                {
                    LOG.Error("NotarizeController.MailValidation - Error validando mail usuario [" + msgerr + "]");
                    return Request.CreateResponse((HttpStatusCode)Errors.IRET_API_CONFLIC, 
                        (new ResponseModel(Errors.IRET_API_CONFLIC, "Error validando mail usuario[" + msgerr + "]. Reintente...")));
                }

                //4) Update de todos los trámites iniciados por este mail, y que tengan status de 
                //      Pendiente de Validacion de mail al siguiente estado
                ret = 0;
                ret = AdministradorNvUsersProcedures.UpdateStatusMailValidated(user.Id);

                if (ret < 0)
                {
                    LOG.Error("NotarizeController.MailValidation - Error update de status tramite  [" + msgerr + "]");
                    return Request.CreateResponse((HttpStatusCode)Errors.IRET_API_CONFLIC, 
                        (new ResponseModel(Errors.IRET_API_CONFLIC, "Error cambiando status de trámite a mail validado [" +
                        msgerr + "]. Reintente...")));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.MailValidation Error Desconcido procesando validacion de mail : " + ex.Message);
                return Request.CreateResponse((HttpStatusCode)Errors.IRET_API_INTERNAL_SERVER_ERROR, 
                    (new ResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconocido procesando validacion de mail ["
                                                  + ex.Message + "]")));
            }
            LOG.Error("NotarizeController.MailValidation OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, (new ResponseModel(Errors.IRET_API_OK, "Registro validado con exito!"))); //"<h2>Litole!</h2>"; //

        }

        [Route("api/MailValidationHtml")]
        [HttpGet]
        public object MailValidationHtml(string mail, string code) //[FromUri] object param)
        {
            string trackId = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotarizeController.MailValidation - IN...");

                //1)Chequeo parametroos
                if (string.IsNullOrEmpty(mail) || string.IsNullOrEmpty(code))
                {
                    LOG.Debug("NotarizeController.MailValidation - Error parametros de entrada [Null]");
                    
                    return CreateResponseHtml(1,"Error validando mail. Faltan datos. Comuniquese con soporte de Notario Virtual.");
                    //return "<p>Problemas para verificar registro. Complete y reintente...</p>"; 
                        //(new ResponseModel(Errors.IRET_API_BAD_REQUEST, "Problemas para verificar registro. Complete y reintente..."));
                }

                //2) Reviso que el usuario esté dado de alta por mail
                NvUsers user = AdministradorNvUsers.BuscarNvUserByMail(mail);

                if (user == null)
                {
                    LOG.Error("NotarizeController.MailValidation - Preregistro NO existe! - Por Mail = " + mail);
                    return CreateResponseHtml(1, "Preregistro NO existe. Reinicie el proceso...");
                }

                //3) Valido Registro User
                int ret = AdministradorNvUsers.ValidateMailNvUser(mail, code, out msgerr);

                if (ret == Errors.IRET_ERR_MAIL_VALIDATED)
                {
                    LOG.Error("NotarizeController.MailValidation - Mail ya validado!");
                    return CreateResponseHtml(0, "Mail ya validado!");
                } else if (ret == Errors.IRET_API_ERR_CODE)
                {
                    LOG.Error("NotarizeController.MailValidation - Codigo usado erroneo");
                    return CreateResponseHtml(1, "Codigo enviado erróneo!");
                }
                else if (ret < 0)
                {
                    LOG.Error("NotarizeController.MailValidation - Error validando mail usuario [" + msgerr + "]");
                    return CreateResponseHtml(1, "Error validando mail usuario[" + msgerr + "]. Reintente...");
                }

                //4) Update de todos los trámites iniciados por este mail, y que tengan status de 
                //      Pendiente de Validacion de mail al siguiente estado
                ret = 0;
                ret = AdministradorNvUsersProcedures.UpdateStatusMailValidated(user.Id);

                if (ret < 0)
                {
                    LOG.Error("NotarizeController.MailValidation - Error update de status tramite  [" + msgerr + "]");
                    return CreateResponseHtml(1, "Error cambiando status de trámite a mail validado [" + msgerr + "]. Reintente...");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.MailValidation Error Desconcido procesando validacion de mail : " + ex.Message);
                return CreateResponseHtml(1, "Error Desconocido procesando validacion de mail ["
                                                  + ex.Message + "]");
            }
            LOG.Error("NotarizeController.MailValidation OUT!");
            return CreateResponseHtml(0, "Mail <b>" + mail + "</b> validado con exito!"); //"<h2>Litole!</h2>"; //

        }

        /// <summary>
        /// Crea respuesta para retornar HTML cuando se pide algo a la API 
        /// desde el mail  (Ej: validar mail)
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private HttpResponseMessage CreateResponseHtml(int code, string msg)
        {
            HttpResponseMessage response;
            try
            {
                LOG.Error("NotarizeController.CreateResponseHtml IN...");
                string htmlpage = "<table border=\"0\" width=\"60%\" align=\"center\">" +
                                   "<tr><td align=\"center\"><a href=\"http://www.notariovirtual.cl\" target=\"_blank\">" +
                                   "<img src=\"" + Properties.Settings.Default.URLBaseAPIRest +  "/images/logo_mail.jpg\" /></a></td></tr></table>" +
                            "<table border=\"0\" width=\"60%\" align=\"center\">" +
                              "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                               "<tr><td><p style=\"font-family:'Arial'\"><b>" + 
                               ((code==0)?"<font color=\"green\">Funcionamiento Correcto!</font>":
                                           "<font color =\"red\">ERROR</font>") 
                                 + "</b></p></td></tr>" +
                                "<tr><td><p style=\"font-family:'Arial'\">" + msg + "</td></tr>" +
                                "<tr><td>&nbsp;</td></tr>" +
                                "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
                                "<a href=\"http://www.notariovirtual.cl\" target=\"_blank\">Notario Virtual<a>.</p></td></tr>" +
                                "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                            "</table>";
                response = new HttpResponseMessage();
                response.Content = new StringContent(htmlpage);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            }
            catch (Exception ex)
            {
                response = new HttpResponseMessage();
                response.Content = new StringContent("Error generando página de resultado.");
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                LOG.Error("NotarizeController.CreateResponseHtml Error: " + ex.Message);
            }
            LOG.Error("NotarizeController.CreateResponseHtml OUT!");
            return response;
        }

        [Route("api/Login")]
        [HttpPost]
        public object Login([FromBody] object param)
        {
            string trackId = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotarizeController.Login - IN...");
                //1) Deserializo parámetros     
                if (param == null)
                {
                    return (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos Complete y reintente..."));
                }
                string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                LOG.Debug("NotarizeController.Login - Deserializo ParamIn => " + strIn);
                LoginRequestModel paramIn = JsonConvert.DeserializeObject<LoginRequestModel>(strIn);

                if (paramIn == null)
                {
                    LOG.Debug("NotarizeController.Login - Error deserializando parametros de entrada [Null]");
                    return (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Problemas para chequear login. Complete y reintente..."));
                }
                else
                {
                    string msgRet = "";
                    if (String.IsNullOrEmpty(paramIn.mail))
                    {
                        msgRet += "|Usuario Deshabilitado!";
                    }
                    if (String.IsNullOrEmpty(paramIn.password))
                    {
                        msgRet += "|Clave!";
                    }
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                "Problemas hacer login. [" + msgRet + "]"));
                    }
                }

                //2) Reviso que el usuario esté dado de alta por mail
                NvUsers user = AdministradorNvUsers.BuscarNvUserByMail(paramIn.mail);

                if (user == null)
                {
                    LOG.Error("NotarizeController.Login - USuarios NO existe! - Por Mail = " + paramIn.mail);
                    return (new ResponseModel(Errors.IRET_ERR_USER_NOT_EXIST, "Usuario NO existe. Reinicie el proceso..."));
                }
                if (user.IsBlocked == 1)
                {
                    LOG.Error("NotarizeController.Login - Usuario bloqueado => " + paramIn.mail);
                    return (new ResponseModel(Errors.IRET_API_ERR_USER_BLOCKED, "Usuario bloqueado " +
                        (user.DateBlocked.HasValue?"desde el " +user.DateBlocked.Value.ToString("dd/MM/yyyy"):"")));
                }

                //3) Valido Password
                int ret = -1; // AdministradorNvUsers.Login(paramIn.mail, paramIn.password, out msgerr);

                if (ret < 0)
                {
                    LOG.Error("NotarizeController.Login - Error validando login usuario [" + msgerr + "]");
                    return (new ResponseModel(Errors.IRET_ERR_NVPROCEDURE_NO_SAVED, "Error validando login usuario[" + msgerr + "]. " +
                        "Reintente..."));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.Login Error Desconcido procesando validacion de login : " + ex.Message);
                return (new ResponseModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconcido procesando validacion de login ["
                                                  + ex.Message + "]"));
            }
            LOG.Error("NotarizeController.Login OUT!");
            //return Request.CreateResponse(HttpStatusCode.OK, new ResponseModel(200, "Login verificado con exito!")); //(new ResponseModel(200, "Login verificado con exito!"));
            return Request.CreateResponse(HttpStatusCode.OK, "token"); // new ResponseModel(200, "Login verificado con exito!"));
        }

        /// <summary>
        /// Dado mail elimina el user con ese mail y luego los nv_user_procedures asociados para seguir con pruebas
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [Route("api/Reset")]
        [HttpGet]
        public object Reset(string mail) //[FromUri] object param)
        {
            string trackId = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotarizeController.Reset - IN...");

                //1)Chequeo parametroos
                if (string.IsNullOrEmpty(mail))
                {
                    LOG.Debug("NotarizeController.Reset - Error parametros de entrada [Null]");
                    return (new ResponseModel(Errors.IRET_API_BAD_REQUEST, "Problemas para verificar registro. Complete y reintente..."));
                }

                //2) Reviso que el usuario esté dado de alta por mail
                NvUsers user = AdministradorNvUsers.BuscarNvUserByMail(mail);

                if (user == null)
                {
                    LOG.Error("NotarizeController.Reset - Preregistro NO existe! - Por Mail = " + mail);
                    return (new ResponseModel(Errors.IRET_ERR_PREREGISTER_NOT_EXIST, "Preregistro NO existe. Reinicie el proceso..."));
                }

                //4) Elimino de todos los trámites del user
                int ret = 0;
                ret = AdministradorNvUsersProcedures.DeleteNvProcedures(user.Id);

                if (ret < 0)
                {
                    LOG.Error("NotarizeController.Reset - Error eliminando tramites  [" + msgerr + "]");
                    return (new ResponseModel(Errors.IRET_API_CONFLIC, "Error eliminando tramites [" + msgerr + "]. Reintente..."));
                }

                //3) Valido Elimino User
                ret = AdministradorNvUsers.DeleteNvUser(mail, out msgerr);

                if (ret < 0)
                {
                    LOG.Error("NotarizeController.Reset - Error eliminando User!");
                    return (new ResponseModel(ret, "Error eliminado user!"));
                }


            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.Reset Error Desconcido procesando validacion de mail : " + ex.Message);
                return (new ResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconocido procesando validacion de mail ["
                                                  + ex.Message + "]"));
            }
            LOG.Error("NotarizeController.Reset OUT!");
            return (new ResponseModel(200, "Reset con exito!")); //"<h2>Litole!</h2>"; //

        }


        #region CIWeb


        /// <summary>
        /// Inicia el proceso de transaccion de CI de acuerdo a los parámetros 
        /// </summary>
        /// <remarks>
        /// Los valores enviados como parámetro determiann la forma de verificación que se realizará para 
        /// hacer la certificación, a saber:
        /// * company: id informado en el moemnto del alta del servicio (int)
        /// * application: : id informado en el moemnto del alta del servicio (int)
        /// * typeId: tipo de doc a usar para certificar identidad (RUT, PAS, etc)
        /// * valueId: Nro de documento a verificar. Si es RUT formato xxxxxxxx-y
        /// * mailCertify: mail de persona a certificar. Se envia mail para que inice desde alli el proceso de certificación.
        /// * cellNumber: número de celular apra enviar link de procesod e certificacion via wathsapp [Aun no integrado]
        /// * theme: Nombre del tema gráfico a usar, donde se definen colores, logos, etc. Si va nulo se usa default.
        /// * customerTrackId: valor opcional de id unico interno del sistema que crea transaccion. 
        /// * callbackUrl: URL para callback cuando se termine el proceso de certificacion. Sew hará un POST con el trackid de
        /// la transaccion para que el sistema externo recupere los datos que necesite para seguir con su flujo de negocios.
        /// * redirectUrl: Si se desea redirigir a una pagina (Opcional) 
        /// * workflowname: El nombre del workflow que deberá seguir esta transaccion. Inicialmente usaremos solo "default",
        /// luego agregaremos mantenedor de nuevos workflow para aplicar a diversos casos de negocio.
        /// </remarks>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/ciweb/ciCreateTx")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "CICreateModelR", typeof(CICreateModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(ErrorModel))]
        public object CICreateTx([FromBody] CICreateModel param) //string mail, int notaryid, int procedureid
        {
            /*
                1) Check Parametro 
                2) Check Acceso/Habilitacion
                3) Creo TrackId Nuevo
                4) Creo TxCreate en BioPortal
                5) Grabo en BD
                6) Retorno si hay error
                7) Informo por mail y/o wsap
                8) Retorno si hay error o
                9) Retorno ok
            */
            string trackId = null;
            string msgerr = null;
            CICreateModelR response = null;
            CICreateModel paramIn;
            string warning = null;
            BPTxCreateR BPResponse = null;
            string trackidbp = null;
            try
            {
                LOG.Debug("NotarizeController.CICreateTx - IN...");

                //0) Check Basic Authentication
                if (!Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotarizeController.TxCreate - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }


                //1) Deserializo parámetros y chequeo   
                if (param == null)
                {
                    Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null)));
                }
                //string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                //LOG.Debug("NotarizeController.InitProcedureOne - Deserializo ParamIn => " + strIn);
                //InitProcedureOneRequestModel paramIn = JsonConvert.DeserializeObject<InitProcedureOneRequestModel>(strIn);
                paramIn = param;

                if (paramIn == null)
                {
                    LOG.Debug("NotarizeController.CICreateTx - Error deserializando parametros de entrada [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                            (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Error deserializando parametros de entrada [Null]", null)));
                }
                else
                {
                    string msgRet = "";
                    if (paramIn.company <= 0 || paramIn.application <= 0) //String.IsNullOrEmpty(paramIn.document))
                    {
                          msgRet += (string.IsNullOrEmpty(msgRet) ? "company y application deben tener un valor entero > 0" :
                                "|company y aplication deben tener un valor entero > 0");
                    }
                    if (string.IsNullOrEmpty(paramIn.typeId) || string.IsNullOrEmpty(paramIn.valueId)) 
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "TypeId/ValueId no pueden ser nulos" : "|TypeId/ValueId no pueden ser nulos"); ;
                    }
                    if (!string.IsNullOrEmpty(paramIn.typeId) && !string.IsNullOrEmpty(paramIn.valueId)
                        && paramIn.typeId.Equals("RUT") && !IsCorrectRut(paramIn.valueId)) 
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "Rut Nulo o Incorrecto" : "|Rut Nulo o Incorrecto");
                    }
                    if (string.IsNullOrEmpty(paramIn.mailCertify) && string.IsNullOrEmpty(paramIn.cellNumber))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "Mail o Celular deben ser informados para enviar al usuario" :
                            "|Mail o Celular deben ser informados para enviar al usuario");
                    }
                    LOG.Debug("NotarizeController.CICreateTx - Check listMailsDestinatary en paramIn = " + (paramIn.listMailsDestinatary == null ? "NULL" : "Len =" +
                                                                   paramIn.listMailsDestinatary.Length));
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                                (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                    "Problemas para notarizar. [" + msgRet + "]", null)));
                    }
                }

                //2) Chequeo de acceso y habilitacion
                LOG.Debug("NotarizeController.CICreateTx - Control acceso companyid = " + paramIn.company);
                int qext;
                string strapp, msg;
                int retaux = CIServiceManager.CheckAppAndCompanyEnabled(paramIn.company, paramIn.application, out qext, out strapp, out msg);
                if (retaux < 0)
                {
                    LOG.Error("NotarizeController.CICreateTx - Error chequeando company [" + retaux.ToString() + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new ErrorModel(Errors.IRET_API_CONFLIC, "Error chequeando company [" + retaux.ToString() + "]. Reintente...", null)));
                }

                LOG.Debug("NotarizeController.CICreateTx - Inicio create CI para = " + paramIn.typeId + " " + paramIn.valueId);

                //3) Creo TrackId Nuevo
                trackId = AdministradorRdCITx.GetTrackIdUnique();
                if (string.IsNullOrEmpty(trackId))
                {
                    LOG.Error("NotarizeController.CICreateTx - TrackId generado invalido [Null]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new ErrorModel(Errors.IRET_API_CONFLIC, "Problemas para crear id de transaccion unico. Reintente...",
                        null)));
                }

                //4) Creo TxCreate en BioPortal
                string listMails = null;
                int ret = BPHelper.TxCreate(trackId, paramIn, out msgerr, out listMails, out trackidbp, out BPResponse);

                if (ret < 0 || BPResponse == null)
                {
                    LOG.Error("NotarizeController.CICreateTx -  BPHelper.TxCreate Error => " + 
                        ret + " - " + (string.IsNullOrEmpty(msgerr)?"Null":msgerr));
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new ErrorModel(Errors.IRET_API_CONFLIC, "Problemas iniciar proceso de verificacion en servicio biometrico. "+
                        "TxCreate Error => " + ret + " - " + (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr),
                        null)));
                } else
                {
                    LOG.Debug("NotarizeController.CICreateTx - Revisando lista destinatarios...");
                    LOG.Debug("NotarizeController.CICreateTx - " + (paramIn.listMailsDestinatary == null ? "NULL" : "Len =" +
                                                                    paramIn.listMailsDestinatary.Length));
                    paramIn.listMailsDestinatary = (paramIn.listMailsDestinatary==null || paramIn.listMailsDestinatary.Length==0) ?
                                                    listMails.Replace(";",",").Split(','):
                                                    paramIn.listMailsDestinatary;
                    LOG.Debug("NotarizeController.CICreateTx - Lista definida...");
                    foreach (string item in paramIn.listMailsDestinatary)
                    {
                        LOG.Debug("NotarizeController.CICreateTx - item = " + item);
                    }
                }

                //5) Grabo en BD
                ret = 0;
                string trackidTx = "";
                ret = AdministradorRdCITx.CreateCITxWeb(trackId, paramIn, BPResponse, trackidbp, out msgerr);

                //6) Retorno si hay error
                if (ret < 0)
                {
                    LOG.Error("NotarizeController.CICreateTx - Error creando CI Tx Web [" + ret + "-" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                        (new ErrorModel(Errors.IRET_ERR_CI_TX_WEB_CREATE, "Error creando CI Tx Web [" + msgerr + "]", null)));
                }

                //7 - Informo x mail

                //Primero mando a destinatario de CI que empiece proceso de CI y luego a lista si existe
                //4 - Informo x mail
                LOG.Debug("NotarizeController.CICreateTx - Inicio notify Create CI para = " + paramIn.typeId + " " + paramIn.valueId);
                //Primero mando a destinatario de CI que empiece proceso de CI, luego a los destinatarios adicionales
                string lMails = null;
                string[] sList = null;
                if (param.listMailsDestinatary != null)
                {
                    sList = param.listMailsDestinatary;
                }
                else if (!string.IsNullOrEmpty(listMails))
                {
                    sList = listMails.Replace(';', ',').Split(',');
                }
                //string lMails = (param.listMailsDestinatary!=null) ? param.listMailsDestinatary : listMails;
                bool isFirst = true;
                //if (!string.IsNullOrEmpty(lMails))
                //{
                    
                if (sList != null && sList.Length > 0)
                {
                    foreach (string item in sList) //paramIn.listMailsDestinatary)
                    {
                        if (isFirst)
                        {
                            lMails = item;
                            isFirst = false;
                        }
                        else
                        {
                            lMails += ";" + item;
                        }
                    }
                    LOG.Debug("NotarizeController.CICreateTx - Envaindo a lista de mails = " + 
                                (string.IsNullOrEmpty(lMails) ? "Null" : lMails));
                }
                //}
                retaux = SendToDestinataryCIWeb(null, paramIn.company, trackId, paramIn.typeId, paramIn.valueId, 
                                                BPResponse.verifyUrl, paramIn.mailCertify, lMails,
                                                paramIn.cellNumber, out warning);

                if (retaux < 0)
                {
                    LOG.Warn("NotarizeController.CICreateTx - Error enviando notificaciones = " + retaux);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                                 (new ErrorModel(Errors.IRET_ERR_CI_TX_SEND_NOTIFY, warning, null)));
                }

            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.CICreate Error Iniciando trámite: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    (new ErrorModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconcido procesando inicio de tramite ["
                                                  + ex.Message + "]", null)));
            }

            LOG.Debug("NotarizeController.CICreateT OUT! => BPResponse.verifyUrl = " + BPResponse.verifyUrl);
            //8 - Retorno OK
            return Ok(new CICreateModelR((string.IsNullOrEmpty(warning)?null:warning), trackId, BPResponse.verifyUrl)); // Request.CreateResponse(HttpStatusCode.OK, response);

        }

        /// <summary>
        /// Reenvia notificaciones para inicio de CI
        /// </summary>
        /// <remarks>
        /// Enviando como parámtero el id de compañia y el trackid, si el estado es aun pendiente y NO expirado, 
        /// se reenvía mails (y a futuro whatsapp) con link para inicio de verificación a la persona certificada, y 
        /// mail informativo a la/las persona/s destinatarias de la certificación.  
        /// </remarks>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/ciweb/ciResendTx")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Error", typeof(ErrorModel))]
        public object CIResendTx([FromBody] CIResendModel param) //string mail, int notaryid, int procedureid
        {
            /*
                1) Check Parametro 
                2) Check Acceso/Habilitacion
                3) Recupero Tx
                4) Retorno ok
            */
            CIResendModel paramIn;
            string msgRet = "";
            string warning = null;
            try
            {
                LOG.Debug("NotarizeController.CIResendTx - IN...");

                //0) Check Basic Authentication
                if (!Authorized(Request, out msgRet))
                {
                    LOG.Warn("NotarizeController.TxCreate - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgRet) ? "" : msgRet) + "]", null)));
                }

                //1) Deserializo parámetros y chequeo   
                if (param == null)
                {
                    Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null)));
                }
                //string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                //LOG.Debug("NotarizeController.InitProcedureOne - Deserializo ParamIn => " + strIn);
                //InitProcedureOneRequestModel paramIn = JsonConvert.DeserializeObject<InitProcedureOneRequestModel>(strIn);
                paramIn = param;

                if (paramIn == null)
                {
                    LOG.Debug("NotarizeController.CIResendTx - Error deserializando parametros de entrada [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                            (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Error deserializando parametros de entrada [Null]", null)));
                }
                else
                {
                    if (paramIn.company <= 0) //String.IsNullOrEmpty(paramIn.document))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "company debe tener un valor entero > 0" :
                              "|company debe tener un valor entero > 0");
                    }
                    if (string.IsNullOrEmpty(paramIn.trackid))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "TrackId no puede ser nulo" : "|TrackId no puede ser nulo"); ;
                    }
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                                (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                    "Problemas para notarizar. [" + msgRet + "]", null)));
                    }
                }

                //2) Chequeo de acceso y habilitacion
                //LOG.Debug("NotarizeController.CIResendTx - Control acceso companyid = " + paramIn.company);
                //int qext;
                //string strapp, msg;
                //int retaux = CIServiceManager.CheckAppAndCompanyEnabled(paramIn.company, out qext, out strapp, out msg);
                //if (retaux < 0)
                //{
                //    LOG.Error("NotarizeController.CIResendTx - Check Acceso incorrecto para company = " + paramIn.company + 
                //        "[" + (string.IsNullOrEmpty(msg)?"":msg) + "]");
                //    return Request.CreateResponse(HttpStatusCode.Conflict,
                //        (new ErrorModel(Errors.IRET_API_CONFLIC, "Check Acceso incorrecto para company = " + paramIn.company + 
                //        "[" + (string.IsNullOrEmpty(msg) ? "" : msg) + "]", paramIn.trackid)));
                //}

                //3) Recupero Tx
                RdCITx tx = AdministradorRdCITx.GetCITxByTrackId(paramIn.trackid, out msgRet);
                if (tx == null)
                {
                    LOG.Error("NotarizeController.CIResendTx - RdCITx recuperada [Null] para trackid = " + paramIn.trackid);
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        (new ErrorModel(Errors.IRET_ERR_CI_NO_IN_DB, "RdCITx recuperada[Null] para trackid = " + paramIn.trackid,
                        null)));
                }

                //4 - Informo x mail
                LOG.Debug("NotarizeController.CIResendTx - Inicio resend CI para = " + tx.TypeId + " " + tx.ValueId);
                //Primero mando a destinatario de CI que empiece proceso de CI, luego a los destinatarios adicionales
                int retaux = SendToDestinataryCIWeb(tx, paramIn.company, null, null, null, null, null, null, null, out warning);

                if (retaux < 0)
                {
                    LOG.Warn("NotarizeController.CIResendTx - Error resend CI para = " + tx.TypeId + " " + tx.ValueId +
                        " - ret = " + retaux);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                                 (new ErrorModel(Errors.IRET_ERR_CI_TX_SEND_NOTIFY, warning, null)));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.CIResendTx Error reenviando link para CI : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    (new ErrorModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error reenviando link para CI ["
                                                  + ex.Message + "]", null)));
            }
            LOG.Debug("NotarizeController.CIResendTx - Out!");
            //8 - Retorno OK
            return Ok(); // new CICreateModelR((string.IsNullOrEmpty(warning) ? null : warning), BPResponse.verifyUrl)); // Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Pide estado de una transacción
        /// </summary>
        /// <remarks>
        /// Enviando un trackid de transacción como parámetro del request, por ejemplo:  
        /// [request.AddParameter("trackid", "5d8e15df00af4f3ab7d6eefb9b5f8dec")], se revisa el estado, consultando 
        /// en BioPortal y actualizando los datos internos si es necesario. Si se completa la certificación, se envia 
        /// por mail la certificación a quien corresponda. 
        /// </remarks>
        /// <returns></returns>
        [Route("api/ciweb/CIStatusTx")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "CIStatusModelR", typeof(CIStatusModelR))]
        public object CIStatusTx()
        {
            string msgerr;
            string trackid = null;
            RdCITx tx = null;
            CIStatusModelR oCIStatusModelR = null;
            try
            {
                LOG.Debug("NotarizeController.CIStatusTx - IN...");

                //0) Check Basic Authentication
                if (!Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotarizeController.TxCreate - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                trackid = System.Web.HttpContext.Current.Request.Params["trackid"];
                LOG.Debug("NotarizeController.CIStatusTx - TrackId recibido => " + (string.IsNullOrEmpty(trackid) ? "Null" : trackid));

                tx = AdministradorRdCITx.GetCITxByTrackId(trackid, out msgerr);
                if (tx == null) return Request.CreateResponse(HttpStatusCode.NotFound, 
                                            new ErrorModel(Errors.IRET_ERR_CI_NO_IN_DB, "Error Desconocido CI", "TrackId de CI no existe"));

                RdCITx txout = null;
                int ret = UpdateRdCITx(tx, out msgerr, out txout);
                if (ret < 0) return Request.CreateResponse(HttpStatusCode.NotModified, new ErrorModel(ret, "Error Status/Update CI", msgerr));

                //LLeno salida
                if (txout != null) LOG.Debug("NotarizeController.CIStatusTx - txout.ActionId = " + txout.ActionId.ToString());
                else
                {
                    LOG.Debug("NotarizeController.CIStatusTx - Recupera de nuevo tx con trackid = " + trackid);
                    txout = AdministradorRdCITx.GetCITxByTrackId(trackid, out msgerr);
                    if (txout != null) LOG.Debug("NotarizeController.CIStatusTx - txout.ActionId = " + txout.ActionId.ToString());
                    else {
                        LOG.Debug("NotarizeController.CIStatusTx - Recuperacion NULA!");
                    }
                }
                LOG.Debug("NotarizeController.CIStatusTx - Creando salida...");
                oCIStatusModelR = new CIStatusModelR();
                oCIStatusModelR.code = 0;
                oCIStatusModelR.msgerr = null;
                oCIStatusModelR.trackid = trackid;
                oCIStatusModelR.status = CIServiceManager.GetStatus(txout);
                oCIStatusModelR.statusdescription = (txout != null?CIServiceManager.GetStatusDescription(txout.ActionId):null);
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.CIStatusTx Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new ErrorModel(-1, "Error Desconocido CI", ex.Message));
            }
            LOG.Debug("NotarizeController.CIStatusTx - OUT!");
           
            return Request.CreateResponse(HttpStatusCode.OK, oCIStatusModelR);
        }

        [Route("api/ciweb/WebhookBPWeb")]
        [HttpPost]
        public object WebhookBPWeb()
        {
            string msgerr;
            try
            {
                LOG.Debug("NotarizeController.WebhookBPWeb - IN...");
                string trackidbp = System.Web.HttpContext.Current.Request.Params["trackid"];
                LOG.Debug("NotarizeController.WebhookBPWeb - TrackIdBP recibido => " + (string.IsNullOrEmpty(trackidbp) ? "Null" : trackidbp));

                //TODO - Sacar Tx de CI desde el trackid de BP
                RdCITx tx = AdministradorRdCITx.GetCITxByTrackIdBP(trackidbp, out msgerr);

                if (tx == null)
                {
                    LOG.Warn("NotarizeController.WebhookBPWeb - RdCITx recuperada nula [" +
                                        (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr) + "]");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                                            new ErrorModel(Errors.IRET_ERR_CI_NO_IN_DB, "RdCITx recuperada nula [" +
                                        (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr) + "]", "TrackId de CI no existe"));
                }

                LOG.Debug("NotarizeController.WebhookBPWeb - Ingresando a UpdateRdCITx...");
                int ret = UpdateRdCITx(tx, out msgerr, out tx);
                if (ret < 0)
                {
                    LOG.Warn("NotarizeController.WebhookBPWeb - Error en UpdateRdCITx ret = " + ret + "[" +
                             (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr) + "]");
                    return Request.CreateResponse(HttpStatusCode.NotModified, new ErrorModel(ret, "Error Status/Update CI", msgerr));
                }
                //TODO - Recfuperar desde BPWeb la info completa y generar la CI
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeController.WebhookBPWeb Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            LOG.Debug("NotarizeController.WebhookBPWeb - IN...");
            return Request.CreateResponse(HttpStatusCode.OK);
        }



        #endregion CIWeb

        // GET: api/Notarize
        [Route("api/Notarize/Ping")]
        [HttpGet]
        public HttpResponseMessage Ping()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Accepted, new string[] { "200", "Servicio respondiendo!" });
            return response;
        }

        // GET: api/Notarize/5
        [Route("api/Notarize/GetTest")]
        [HttpGet]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Notarize
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Notarize/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Notarize/5
        public void Delete(int id)
        {
        }




        #region Private

        #region private v2

        /// <summary>
        /// Debe generar la transaccion de la CIWeb con la conexion a BPWeb, y creacion de transaccion en tabla rd_CITx
        /// </summary>
        /// <param name="trackId"></param>
        /// <param name="paramin"></param>
        /// <param name="urlciweb"></param>
        /// <param name="msgerr"></param>
        /// <returns>trackid de CI Web o nulo si falla</returns>
        private string GenerateCIWeb(v2NotarizeInitModel paramin, out string urlciweb, out string msgerr)
        {
            urlciweb = null;
            msgerr = null;
            string trackId = null;
            string sTrackIdCItoReturn = null;
            try
            {
                LOG.Debug("NotarizeController.GenerateCIWeb IN...");

                //3) Creo TrackId Nuevo
                trackId = AdministradorRdCITx.GetTrackIdUnique();
                if (string.IsNullOrEmpty(trackId))
                {
                    LOG.Error("NotarizeController.GenerateCIWeb - TrackId generado invalido [Null]");
                    return null;
                }

                
                //4) Creo TxCreate en BioPortal
                //string msgerr;
                string trackidbp;
                string listMails = null;
                BPTxCreateR BPResponse;
                int ret = BPHelper.TxCreateNotarize(trackId, paramin, out msgerr, out listMails, out trackidbp, out BPResponse);

                if (ret < 0 || BPResponse == null)
                {
                    LOG.Error("NotarizeController.GenerateCIWeb -  BPHelper.TxCreate Error => " +
                        ret + " - " + (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr));
                    return null;
                        //Request.CreateResponse(HttpStatusCode.Conflict,
                        //(new ErrorModel(Errors.IRET_API_CONFLIC, "Problemas iniciar proceso de verificacion en servicio biometrico. " +
                        //"TxCreate Error => " + ret + " - " + (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr),
                        //null)));
                }
                else
                {
                    

                    //5) Grabo en BD
                    ret = 0;
                    ret = AdministradorRdCITx.CreateNotarizeTxWeb(trackId, paramin, BPResponse, trackidbp, out msgerr);

                    //6) Retorno si hay error
                    if (ret < 0)
                    {
                        LOG.Error("NotarizeController.GenerateCIWeb - Error creando CI Tx Web [" + ret + "-" + msgerr + "]");
                        return null;
                    }
                    urlciweb = BPResponse.verifyUrl;
                    sTrackIdCItoReturn = trackId;
                    LOG.Debug("NotarizeController.GenerateCIWeb - Creada CI Web con trackid = " + trackId + " y url = " + urlciweb);

                    //LOG.Debug("SignerController.txCreate - Revisando lista destinatarios...");
                    //LOG.Debug("SignerController.txCreate - " + (paramIn.listMailsDestinatary == null ? "NULL" : "Len =" +
                    //                                                paramIn.listMailsDestinatary.Length));
                    //paramIn.listMailsDestinatary = (paramIn.listMailsDestinatary == null || paramIn.listMailsDestinatary.Length == 0) ?
                    //                                listMails.Replace(";", ",").Split(',') :
                    //                                paramIn.listMailsDestinatary;
                    //LOG.Debug("SignerController.txCreate - Lista definida...");
                    //foreach (string item in paramIn.listMailsDestinatary)
                    //{
                    //    LOG.Debug("NotarizeController.CICreateTx - item = " + item);
                    //}
                }

                //Primero mando a destinatario de CI que empiece proceso de CI y luego a lista si existe
                //4 - Informo x mail
                //LOG.Debug("SignerController.txCreate - Inicio notify Create Transaccion para = RUT " + paramin.rut);
                //Primero mando a destinatario de CI que empiece proceso de CI, luego a los destinatarios adicionales
                //string lMails = null;
                //string[] sList = null;
                //if (paramin.listMailsDestinatary != null)
                //{
                //    sList = param.listMailsDestinatary;
                //}
                //else if (!string.IsNullOrEmpty(listMails))
                //{
                //    sList = listMails.Replace(';', ',').Split(',');
                //}
                ////string lMails = (param.listMailsDestinatary!=null) ? param.listMailsDestinatary : listMails;
                //bool isFirst = true;
                ////if (!string.IsNullOrEmpty(lMails))
                ////{

                //if (sList != null && sList.Length > 0)
                //{
                //    foreach (string item in sList) //paramIn.listMailsDestinatary)
                //    {
                //        if (isFirst)
                //        {
                //            lMails = item;
                //            isFirst = false;
                //        }
                //        else
                //        {
                //            lMails += ";" + item;
                //        }
                //    }
                //    LOG.Debug("SignerController.txCreate - Envaindo a lista de mails = " +
                //                (string.IsNullOrEmpty(lMails) ? "Null" : lMails));
                //}
                
            }
            catch (Exception ex)
            {
                urlciweb = null;
                LOG.Error("NotarizeController.GenerateCIWeb Error: " + ex.Message);
            }
            LOG.Debug("NotarizeController.GenerateCIWeb OUT!");
            return sTrackIdCItoReturn;
        }

        #endregion private v2

        internal static int SendToDestinataryCIWeb(RdCITx tx, int company, string trackid, string typeid, string valueid, 
                                           string url, string mail, string listmails, string cellphone, out string warning)
        {
            int ret = 0;
            warning = null;
            int _company = 0;
            string _trackid = "";
            string _typeid = "";
            string _valueid = "";
            string _url = "";
            string _mail = "";
            string _listmails = "";
            string[] aListMails = null;
            string _cellphone = "";
            warning = "";
            try
            {

                if (tx != null)
                {
                    _company = tx.CompanyId;
                    _trackid = tx.TrackId;
                    _typeid = tx.TypeId;
                    _valueid = tx.ValueId;
                    _url = tx.UrlBPWeb;
                    _listmails = tx.ListMailsDistribution;
                    aListMails = !string.IsNullOrEmpty(_listmails) ? _listmails.Split(';') : null;
                    _cellphone = tx.CellPhone;
                    _mail = tx.DestinataryMail;
                } else
                {
                    _company = company;
                    _trackid = trackid;
                    _typeid = typeid;
                    _valueid = valueid;
                    _url = url;
                    _listmails = listmails;
                    aListMails = !string.IsNullOrEmpty(_listmails) ? _listmails.Split(';') : null;
                    _cellphone = cellphone;
                    _mail = mail;
                }

                if (!string.IsNullOrEmpty(_cellphone))
                {
                    string msgwsap = "";
                    LOG.Debug("NotarizeController.SendToDestinataryCIWeb - Notify by Wsap Nro => " + _cellphone);
                    ret = NotifyByWhatsApp(_cellphone, msgwsap);
                    if (ret < 0)
                    {
                        LOG.Warn("NotarizeController.SendToDestinataryCIWeb - Error notificando por wathsapp [" + ret.ToString() + "]");
                     }
                }

                //Tomo nombre d eemrpesa si está
                string[] strbody = new string[4];
                string strCompany = "Nuestro Cliente";
                try
                {
                    strCompany = AdministradorCompany.BuscarCompanyById(company).Name;
                }
                catch { }
                strbody[0] = "Usted a iniciado un proceso de certificacion pedido por <b>" + strCompany + "</b>.";
                strbody[1] = "El codigo unico de Certificación de Identidad para consultas es: <b>" + _trackid + "</b>.";
                strbody[2] = "Siga los pasos propuestos de Certifricación de Identidad, y cuando lo complete, cierre la ventana del navegador. " +
                    "Enviarémos una copia de la Certificación realizada a su mail y a quien lo requirió";
                //strbody[3] = "<button type=\"button\" style=\"color: white; font - family:'Arial'; background - color: #6EA4CA;\" onclick=\"window.open('" +
                //     BPResponse.verifyUrl + "', '_blank');\">  Empezar proceso de Certificación de Identidad...  </button>";
                strbody[3] = "Inicie el proceso de Certificación de Identidad presionando  <a href=\"" +
                     _url + "\" target =\"_blank\"><font style=\"color:ligth-blue; font-family:'Arial'\">aqui</font></a>.";
                if (libs.Utils.NVNotify(_mail, "Inicio de Certificacion de Identidad", strbody, null, null, null))
                {
                    //Informo a la lista que inicio epl proceso y que le llegar al mail el resultado completo
                    if (aListMails != null && aListMails.Length > 0)
                    {
                        strbody = new string[3];
                        strbody[0] = "Se inicio un proceso de Certificacion de Identidad para <b>" + _typeid + " " + _valueid + "</b>.";
                        strbody[1] = "Cuando la persona termine el proceso de Certificación de Identidad, " +
                            "Enviarémos una copia de la Certificación realizada a su mail.";
                        strbody[2] = "El codigo unico de Certificación de Identidad para consultas es: <b>" + _trackid + "</b>.";
                        foreach (string item in aListMails)
                        {
                            libs.Utils.NVNotify(item, "Inicio de Certificacion de Identidad", strbody, null, null, null);
                        }
                    }
                }
                else
                {
                    warning += "| [Atencion] Hubo un error en el envio de la peticion a Certificacion de Identidad!";
                    ret = Errors.IRET_ERR_CI_TX_SEND_NOTIFY;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }

        internal static int NotifyByWhatsApp(string cellphone, string msgwsap)
        {
            return 0;
        }

        private string ExtractCIPdf(Evidence evidence)
        {
            string ret = null;
            try
            {

                XmlDocument doc = new XmlDocument();
                System.IO.StringReader sr = new System.IO.StringReader(evidence.value);
                doc.Load(sr);
                sr.Close();

                //Display all the book titles.
                XmlNodeList elemList = doc.GetElementsByTagName("ExtensionItem");
                foreach (XmlNode item in elemList)
                {
                    if (item.InnerText.IndexOf("PDFCICertify") >= 0)
                    {
                        ret = item.LastChild.InnerText;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error(" Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Pide QR para verificación de CI via el frontend
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="mail"></param>
        /// <param name="qr"></param>
        /// <returns></returns>
        private string GetQRTokenCI(string rut, string mail, out string qr)
        {
            qr = null;
            string sTrackIdCItoReturn = null;
            try
            {
                LOG.Debug("NotarizeController.GetQRTokenCI IN...");
                Api.XmlParamIn oParamin = new Api.XmlParamIn();
                oParamin.Actionid = 1;
                oParamin.Origin = 1;
                oParamin.Companyid = Convert.ToInt32(Properties.Settings.Default.NVCompanyCI);
                oParamin.ApplicationId = Convert.ToInt32(Properties.Settings.Default.NVAppCI);
                oParamin.CIParam = new Bio.Digital.Receipt.Api.CIParam();
                oParamin.CIParam.TypeId = "RUT";
                oParamin.CIParam.ValueId = rut;
                oParamin.CIParam.DestinaryMail = mail;
                string xmlparamout;
                int ret = Core.Services.CIServiceManager.Process(oParamin, out xmlparamout);
                if (ret == 0)
                {
                    if (!string.IsNullOrEmpty(xmlparamout))
                    {
                        Bio.Digital.Receipt.Api.XmlParamOut oParamout = Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                        sTrackIdCItoReturn = oParamout.CIParam.TrackId;
                        qr = oParamout.QR;
                        LOG.Debug("NotarizeController.GetQRTokenCI TrackIDCI = " + 
                                    (string.IsNullOrEmpty(sTrackIdCItoReturn) ? "NULL" : sTrackIdCItoReturn) +
                                    "sQRtoReturn = " + (string.IsNullOrEmpty(qr)?"NULL": qr));
                    }
                }
                else
                {
                    LOG.Error("NotarizeController.GetQRTokenCI Error retornando QR [" + ret + "]");
                }
            }
            catch (Exception ex)
            {
                qr = null;
                LOG.Error("NotarizeController.GetQRTokenCI Error: " + ex.Message);
            }
            LOG.Debug("NotarizeController.GetQRTokenCI OUT!");
            return sTrackIdCItoReturn;
        }


        /// <summary>
        /// Recupera StatusCI para actualizar
        /// </summary>
        /// <param name="trackid"></param>
        /// <param name="rdout"></param>
        /// <returns></returns>
        private int GetStatusCI(string trackid, out string rdout)
        {
            rdout = null;
            int ret = -1;
            try
            {
                LOG.Debug("NotarizeController.GetStatusCI IN...");
                string xmlparamout;
                Bio.Digital.Receipt.Api.XmlParamIn oParamin = new Bio.Digital.Receipt.Api.XmlParamIn();
                oParamin.Actionid = 6;
                oParamin.Origin = 1;
                oParamin.Companyid = Convert.ToInt32(Properties.Settings.Default.NVCompanyCI);
                oParamin.ApplicationId = Convert.ToInt32(Properties.Settings.Default.NVAppCI);
                oParamin.CIParam = new Bio.Digital.Receipt.Api.CIParam();
                oParamin.CIParam.TrackId = trackid;
                LOG.Debug("NotarizeController.GetStatusCI - Consultando ...");
                ret = Core.Services.CIServiceManager.Process(oParamin, out xmlparamout);
                LOG.Debug("NotarizeController.GetStatusCI - ret = " + ret);
                if (ret == 1)
                {
                    if (!string.IsNullOrEmpty(xmlparamout))
                    {
                        LOG.Debug("NotarizeController.GetStatusCI - Deserializando xmlparamout...");
                        Bio.Digital.Receipt.Api.XmlParamOut oParamout = 
                            Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                        if (oParamout != null && !string.IsNullOrEmpty(oParamout.Receipt))
                        {
                            LOG.Debug("NotarizeController.GetStatusCI - Retornando RD...");
                            rdout = oParamout.Receipt;
                            ret = 1;
                        } else
                        {
                            LOG.Error("NotarizeController.GetStatusCI - Error Deserializando => oParamout == null o Receipt Nulo");
                        }
                    }
                    else
                    {
                        LOG.Warn("NotarizeController.GetStatusCI - Retorno xmlparamout == null");
                    }
                }
                else
                {
                    LOG.Warn("NotarizeController.GetStatusCI - Status CI aun en 0!");
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("NotarizeController.GetStatusCI Error: " + ex.Message);
            }
            LOG.Debug("NotarizeController.GetStatusCI OUT!");
            return ret;
        }


        /// <summary>
        /// Parsea RD recibido desde CI en Webhook para updatear la BD
        /// </summary>
        /// <param name="strIn"></param>
        /// <param name="htRD"></param>
        /// <returns></returns>
        internal static bool ParseRDEnvelope(string strIn, out Hashtable htRD)
        {
            //RdRecibos oRDRet = null;
            bool bret = false;
            htRD = null;
            try
            {
                LOG.Debug("NotarizeController.ParseRDEnvelope IN...");
                //1.- Parseo RDEnvelope
                XmlDocument doc = new XmlDocument();
                System.IO.StringReader sr = new System.IO.StringReader(strIn);
                doc.Load(sr);
                sr.Close();

                if (doc != null)
                {
                    //string sRD = doc.GetElementsByTagName("Rd")[0].OuterXml;

                    //oRDRet = Api.XmlUtils.DeserializeObject<RdRecibos>(sRD);

                    //if (string.IsNullOrEmpty(sRD)) //(oRDRet != null)
                    //{

                    LOG.Debug("NotarizeController.ParseRDEnvelope LoadXML RDEnvelope correcto...");
                    //Display all the book titles.
                    XmlNodeList elemList = doc.GetElementsByTagName("ExtensionItem");
                    htRD = new Hashtable();
                    foreach (XmlNode item in elemList)
                    {
                        htRD.Add(item.FirstChild.InnerText, item.LastChild.InnerText);
                    }
                    LOG.Debug("NotarizeController.ParseRDEnvelope - Len htRD = " + (htRD != null ? "NULL" : htRD.Count.ToString()));
                    bret = true;
                    //} else
                    //{
                    //    LOG.Warn("NotarizeController.ParseRDEnvelope Parseo RD incorrecto. Se aborta parseo!");
                    //}
                }
            }
            catch (Exception ex)
            {
                //oRDRet = null;
                bret = false;
                LOG.Error("NotarizeController.ParseRDEnvelope Error: " + ex.Message);
            }
            LOG.Debug("NotarizeController.ParseRDEnvelope OUT!");
            return bret; // oRDRet;
        }

        /// <summary>
        /// Determina que sea un doc valido => != null y que tenga formato PDF en esta v1
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        private bool IsCorrectDocument(string document)
        {
            bool ret = false;
            try
            {
                LOG.Debug("NotarizeController.IsCorrectDocument IN...");
                if (string.IsNullOrEmpty(document))
                {
                    LOG.Debug("NotarizeController.IsCorrectDocument document==null => true");
                    return false;
                }

                ret = DEHelper.IsDocumentPDF(document);
                LOG.Debug("NotarizeController.IsCorrectDocument Check DEHelper.IsDocumentPDF = " + ret.ToString());
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("NotarizeController.IsCorrectDocument Error: " + ex.Message);
            }
            LOG.Debug("NotarizeController.IsCorrectDocument OUT!");
            return ret;
        }

        /// <summary>
        /// Chequea que rut sea correcto => !=null y bien formado
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        internal static bool IsCorrectRut(string rut)
        {
            bool ret = false;
            try
            {
                LOG.Debug("NotarizeController.IsCorrectRut IN...");
                if (string.IsNullOrEmpty(rut))
                {
                    LOG.Debug("NotarizeController.IsCorrectRut rut==null => true");
                    return false;
                }
                libs.RUN RUN = libs.RUN.Convertir(rut);
                LOG.Debug("NotarizeController.IsCorrectRut RUN Ok => " + (RUN!=null).ToString());
                ret = (RUN != null);
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("NotarizeController.IsCorrectRut Error: " + ex.Message);
            }
            LOG.Debug("NotarizeController.IsCorrectRut OUT!");
            return ret;
        }

        /// <summary>
        /// Chequea mail correcto => Mail!=null y bien formado
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        private bool IsCorrectMail(string mail)
        {
            bool ret = false;
            try
            {
                LOG.Debug("NotarizeController.IsCorrectMail IN...");
                if (string.IsNullOrEmpty(mail))
                {
                    LOG.Debug("NotarizeController.IsCorrectMail mail==null => true");
                    return false;
                }
                
                ret = libs.Utils.ComprobarFormatoEmail(mail);
                LOG.Debug("NotarizeController.IsCorrectMail mail Ok => " + (ret).ToString());

            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("NotarizeController.IsCorrectMail Error: " + ex.Message);
            }
            LOG.Debug("NotarizeController.IsCorrectMail OUT!");
            return ret;
        }

        /// <summary>
        /// Dada una RdCITx, si estado es no terminada => Revisa en BP para traer resultado y generar
        /// la CI, enviando callback y mails a destinatarios si corresponde
        /// </summary>
        /// <param name="tx"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int UpdateRdCITx(RdCITx tx, out string msgerr, out RdCITx txupdated)
        {
            /*
                1.- Check Null y status que sea pendiente aun
                2.- Recupero desde BP status 
                3.- Si termino proceso en BP => Genero CI 
                4.- Si debo informar via callback informo
                5.- Retorno
            */
            int ret = 0;
            msgerr = null;
            txupdated = null;
            try
            {
                LOG.Debug("NotarizeController.UpdateRdCITx IN...");
                //1.- Check Null y status que sea pendiente aun
                if (CIServiceManager.GetStatus(tx).Equals("DONE"))
                {
                    LOG.Debug("NotarizeController.UpdateRdCITx - Sale proque es DONE!");
                    ret = Errors.IRET_API_ERR_CI_COMPLETED;
                    txupdated = tx;
                    return ret;
                }

                if (CIServiceManager.GetStatus(tx).StartsWith("EXPIRED"))
                {
                    LOG.Debug("NotarizeController.UpdateRdCITx - Sale proque es EXPIRED!");
                    tx.ActionId = -10; //Expirada
                    AdministradorRdCITx.UpdateCITx(tx, out msgerr);
                    ret = Errors.IRET_API_ERR_CI_EXPIRED;
                    txupdated = tx;
                    return ret;
                }
                //2.- Recupero desde BP status
                BpTxDataResponse oBpTxDataResponse;
                LOG.Debug("NotarizeController.UpdateRdCITx - Pide status a BP con TrackId = " + tx.TrackIdBP);
                ret = BPHelper.TxStatus(tx.TrackIdBP, out msgerr, out oBpTxDataResponse);
                if (ret < 0)
                {
                    LOG.Debug("NotarizeController.UpdateRdCITx - Retorno de pedido a BP => " + ret + " => Sale!"); 
                    return ret;
                }

                //3.- Si termino proceso en BP => Genero CI 
                if (oBpTxDataResponse == null)
                {
                    LOG.Debug("NotarizeController.UpdateRdCITx - oBpTxDataResponse NULO...");
                    txupdated = tx; //Significa que no hubo error pero esta en pending aun en BPWeb => Inofrmo lo que está en tx
                } else  //Si llego aca es que esta DONE el proceso de verify en BP => Genero CI
                {
                    string xmlparamout = null;
                    XmlParamIn oXmlParamIn = new XmlParamIn();
                    int _resultVerify = oBpTxDataResponse.result;
                    double _score = oBpTxDataResponse.score;
                    LOG.Debug("NotarizeController.UpdateRdCITx - Procesa respuesta desde BP => score = " + _score.ToString() +
                        " - resultverify = " + _resultVerify);
                    if (_resultVerify == 1)
                    {
                        LOG.Debug("NotarizeController.UpdateRdCITx - Result = 1 => Genera CI...");
                        if (oBpTxDataResponse.onboardingmandatory == 0) //Verify Cedula vs Selfie
                        {
                            oXmlParamIn.Actionid = 99;
                        }
                        else //Verify Selfie vs BD
                        {
                            oXmlParamIn.Actionid = 88;
                        }
                        oXmlParamIn.Companyid = tx.CompanyId;
                        oXmlParamIn.Origin = 1;
                        oXmlParamIn.ApplicationId =
                            AdministradorRdAplicaciones.BuscarRdAplicacionesByCode(tx.CompanyId, Properties.Settings.Default.CICode);
                        LOG.Debug("NotarizeController.UpdateRdCITx - ApplicationId recuperado con Code = " +
                                    Properties.Settings.Default.CICode + " => " + oXmlParamIn.ApplicationId);
                        oXmlParamIn.CIParam = new CIParam();
                        oXmlParamIn.CIParam.BirthDate = oBpTxDataResponse.personaldata.birthdate; // tx.BirthDate;
                        oXmlParamIn.CIParam.DestinaryMail = tx.DestinataryMail;
                        oXmlParamIn.CIParam.ExpirationDate = oBpTxDataResponse.personaldata.documentexpirationdate; // tx.ExprationDate;
                        oXmlParamIn.CIParam.GeoRef = tx.GeoRef; //Ver este valor si viene o sale de aca
                        oXmlParamIn.CIParam.IDCardImageBack = BPHelper.GetSample(oBpTxDataResponse, 1); //tx.IdCardImageBack;
                        oXmlParamIn.CIParam.IDCardImageFront = BPHelper.GetSample(oBpTxDataResponse, 0);  //tx.IdCardImageFront;
                        oXmlParamIn.CIParam.IDCardPhotoImage = BPHelper.GetSample(oBpTxDataResponse, 4); //tx.IdCardPhotoImage;
                        oXmlParamIn.CIParam.IDCardSignatureImage = BPHelper.GetSample(oBpTxDataResponse, 3); ; // tx.IdCardSignatureImage;
                        oXmlParamIn.CIParam.IssueDate = null; // tx.IssueDate;
                        oXmlParamIn.CIParam.ManualSignatureImage = BPHelper.GetSample(oBpTxDataResponse, 5); //tx.ManualSignatureImage;
                        oXmlParamIn.CIParam.MotherLastName = oBpTxDataResponse.personaldata.motherlastname; // tx.MotherLastName;
                        oXmlParamIn.CIParam.Nacionality = oBpTxDataResponse.personaldata.nationality; // tx.Nacionality;
                        oXmlParamIn.CIParam.Name = oBpTxDataResponse.personaldata.name; // tx.Name;
                        oXmlParamIn.CIParam.PhaterLastName = oBpTxDataResponse.personaldata.patherlastname; // tx.PhaterLastName;
                        oXmlParamIn.CIParam.Selfie = BPHelper.GetSample(oBpTxDataResponse, 2); //tx.Selfie;
                        oXmlParamIn.CIParam.Score = oBpTxDataResponse.score.ToString(); // tx.Score;
                        oXmlParamIn.CIParam.Serial = oBpTxDataResponse.personaldata.documentseriesnumber; // tx.Serial;
                        oXmlParamIn.CIParam.Sex = oBpTxDataResponse.personaldata.sex; // tx.Sex;
                        oXmlParamIn.CIParam.Threshold = oBpTxDataResponse.threshold.ToString(); // tx.Threshold;
                        oXmlParamIn.CIParam.TrackId = tx.TrackId;
                        oXmlParamIn.CIParam.TypeId = oBpTxDataResponse.personaldata.typeid; // tx.TypeId;
                        oXmlParamIn.CIParam.ValidityType = (int)oBpTxDataResponse.typeverify; // tx.ValidityType;
                        oXmlParamIn.CIParam.ValueId = oBpTxDataResponse.personaldata.valueid; // tx.ValueId;
                        oXmlParamIn.CIParam.WorkStationID = oBpTxDataResponse.enduser;
                        oXmlParamIn.CIParam.GeoRef = oBpTxDataResponse.georef;
                        oXmlParamIn.CIParam.URLVideo = oBpTxDataResponse.videourl;

                        LOG.Debug("NotarizeController.UpdateRdCITx - CIServiceManager.GenerateCI_From_BPWeb ingresando...");
                        ret = CIServiceManager.GenerateCI_From_BPWeb(tx, oXmlParamIn, _resultVerify, _score, false, out xmlparamout);
                        LOG.Debug("NotarizeController.UpdateRdCITx - CIServiceManager.GenerateCI_From_BPWeb  ret 0 " + ret);
                        if (ret == 0) //Si actualizo ok => Mando mail a quien se pidio
                        {
                            LOG.Debug("NotarizeController.UpdateRdCITx - CIServiceManager.GenerateCI_From_BPWeb  sending mails a destinatarios...");
                            ret = SendCIToDestinataries(tx.TrackId);
                            LOG.Debug("NotarizeController.UpdateRdCITx - CIServiceManager.GenerateCI_From_BPWeb ret mailing = " + ret); 
                        }
                    } 

                    if (_resultVerify == 2)
                    {
                        LOG.Debug("NotarizeController.UpdateRdCITx - Updating Verify Negative..."); 
                        tx.ActionId = -11; //Verificacion Negativa
                        tx.Score = _score.ToString();
                        AdministradorRdCITx.UpdateCITx(tx, out msgerr);
                        LOG.Debug("NotarizeController.UpdateRdCITx - Termino Update Negative. MsgErr => " +
                                    (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr));
                        LOG.Debug("NotarizeController.UpdateRdCITx - CIServiceManager.GenerateCI_From_BPWeb sending mails a destinatarios avisando error...");
                        ret = SendCIToDestinatariesXError(tx.TrackId);
                        LOG.Debug("NotarizeController.UpdateRdCITx - CIServiceManager.GenerateCI_From_BPWeb ret mailing = " + ret);
                    }

                    if (_resultVerify == 0)
                    {
                        LOG.Debug("NotarizeController.UpdateRdCITx - _resultVerify = 0 - operationcode = " + oBpTxDataResponse.operationcode);
                        if (oBpTxDataResponse.operationcode < 0)
                        {
                            tx.ActionId = oBpTxDataResponse.operationcode;
                            tx.Score = _score.ToString();
                            AdministradorRdCITx.UpdateCITx(tx, out msgerr);
                            LOG.Debug("NotarizeController.UpdateRdCITx - Termino Update Error [" + oBpTxDataResponse.operationcode 
                                        + "]. MsgErr => " + (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr));
                            LOG.Debug("NotarizeController.UpdateRdCITx - CIServiceManager.GenerateCI_From_BPWeb sending mails a destinatarios avisando error...");
                            ret = SendCIToDestinatariesXError(tx.TrackId);
                            LOG.Debug("NotarizeController.UpdateRdCITx - CIServiceManager.GenerateCI_From_BPWeb ret mailing = " + ret);
                        }
                    }
                    LOG.Debug("NotarizeController.UpdateRdCITx - Recupero tx updated con id => " + (tx != null ? tx.Id.ToString() : "Null")); 
                    txupdated = AdministradorRdCITx.GetCITxByRdId(tx.Id, out msgerr);
                    LOG.Debug("NotarizeController.UpdateRdCITx - txupdated != null => " + (txupdated != null).ToString()); 
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NotarizeController.UpdateRdCITx Error: " + ex.Message);
            }
            LOG.Debug("NotarizeController.UpdateRdCITx OUT!");
            return ret;
        }

        internal static int SendCIToDestinatariesXError(string trackId)
        {
            int ret = 0;
            XmlParamOut oXmlOut;
            string msgerr;
            try
            {
                RdCITx tx = AdministradorRdCITx.GetCITxByTrackId(trackId, out msgerr);
                if (tx != null)
                {
                    //Added 11-09-2020 - Send mail HTMl Avisando error
                    LOG.Debug("NotarizeController.SendCIToDestinatariesXError - Informo por mail " + tx.DestinataryMail + " a destinatario enviadno error...");
                    string[] strbody = new string[2];
                    strbody[0] = "<b><font color=\"red\">Error [" + CIServiceManager.GetStatus(tx)
                                    + "]</b> completando la Certificación de Identidad para el documento <b>" +
                                    tx.TypeId + " " + tx.ValueId + "</b>";
                    strbody[1] = "El codigo unico del tramite para consultas es: <b>" + tx.TrackId + "</b>";
                    //byte[] docpdf = Convert.FromBase64String(tx.CertifyPDF);
                    //byte[] docxml = System.Text.Encoding.UTF8.GetBytes(tx.r);
                    //Send PDF

                    bool bret = libs.Utils.NVNotify(tx.DestinataryMail,
                        "Certificación de Identidad con ERROR [" + tx.ActionId.ToString() + "]", strbody, null, null, null);
                    LOG.Debug("NotarizeController.SendCIToDestinatariesXError - Enviado mail => " + (bret.ToString()));
                    //bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                    //                    "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
                    //                    oXmlOut.CIParam.TrackId + ".xml");
                    //LOG.Debug("Enviado XML => " + (bret.ToString()));

                    if (tx.ListMailsDistribution != null)
                    {

                        string[] _lDestinatary = tx.ListMailsDistribution.Split(';');

                        foreach (string item in _lDestinatary)
                        {
                            LOG.Debug("NotarizeController.SendCIToDestinatariesXError - Informo por mail " + item + " a destinatario enviando error...");
                            strbody[0] = "<b><font color=\"red\">Error [" + CIServiceManager.GetStatus(tx)
                                    + "]</b> completando la Certificación de Identidad para el documento <b>" +
                                    tx.TypeId + " " + tx.ValueId + "</b>";
                            strbody[1] = "El codigo unico del tramite para consultas es: <b>" + tx.TrackId + "</b>";
                            //docpdf = Convert.FromBase64String(tx.CertifyPDF);
                            //byte[] docxml = System.Text.Encoding.UTF8.GetBytes(tx.r);
                            //Send PDF
                            bret = libs.Utils.NVNotify(item,
                                    "Certificación de Identidad con ERROR [" + tx.ActionId.ToString() + "]", strbody, null, null, null);
                            LOG.Debug("NotarizeController.SendCIToDestinatariesXError - Enviado Error => " + (bret.ToString()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("NotarizeController.SendCIToDestinatariesXError - Error: " + ex.Message);
            }
            return ret;
        }

        internal static int SendCIToDestinataries(string trackidCI)
        {
            int ret = 0;
            XmlParamOut oXmlOut;
            string msgerr;
            try
            {
                RdCITx tx = AdministradorRdCITx.GetCITxByTrackId(trackidCI, out msgerr);
                if (tx != null)
                {
                    //Added 10-05-2020 - Send mail HTMl con certificados attachados. MAndo PDF + XML
                    LOG.Debug("Informo por mail " + tx.DestinataryMail + " a destinatario enviadno docs...");
                    string[] strbody = new string[2];
                    strbody[0] = "Usted completó la Certificación de identidad para el documento <b>" +
                                    tx.TypeId + tx.ValueId + "</b>";
                    strbody[1] = "El codigo unico del tramite para consultas es: <b>" + tx.TrackId + "</b>";
                    byte[] docpdf = Convert.FromBase64String(tx.CertifyPDF);
                    //byte[] docxml = System.Text.Encoding.UTF8.GetBytes(tx.r);
                    //Send PDF

                    bool bret = libs.Utils.NVNotify(tx.DestinataryMail,
                                        "Certificación de Identidad completada con exito [PDF]", strbody, docpdf, "pdf",
                                        tx.TrackId + ".pdf");
                    LOG.Debug("Enviado PDF => " + (bret.ToString()));
                    //bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                    //                    "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
                    //                    oXmlOut.CIParam.TrackId + ".xml");
                    //LOG.Debug("Enviado XML => " + (bret.ToString()));

                    if (tx.ListMailsDistribution != null)
                    {
                        string[] _lDestinatary = tx.ListMailsDistribution.Replace(";",",").Split(',');

                        foreach (string item in _lDestinatary)
                        {
                            LOG.Debug("Informo por mail " + item + " a destinatario enviadno docs...");
                            strbody[0] = "Se completó la Certificación de Identidad para el documento <b>" +
                                            tx.TypeId + tx.ValueId + "</b>";
                            strbody[1] = "El codigo unico del tramite para consultas es: <b>" + tx.TrackId + "</b>";
                            docpdf = Convert.FromBase64String(tx.CertifyPDF);
                            //byte[] docxml = System.Text.Encoding.UTF8.GetBytes(tx.r);
                            //Send PDF

                            bret = libs.Utils.NVNotify(item,
                                                "Certificación de Identidad completada con exito [PDF]", strbody, docpdf, "pdf",
                                                tx.TrackId + ".pdf");
                            LOG.Debug("Enviado PDF => " + (bret.ToString()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("NotarizeController Error: " + ex.Message);
            }
            return ret;
        }

        internal static bool Authorized(HttpRequestMessage request, out string msg)
        {
            bool ret = false;
            msg = null;
            try
            {
                LOG.Debug("NotarizeController.Authorized IN...");
                IEnumerable<string> values = new List<string>();
                if (request.Headers.TryGetValues("Authorization", out values))
                {
                    foreach (string item in values)
                    {
                        if (CredentialOk(item, out msg))
                        {
                            ret = true;
                            break;
                        }
                    }
                }
                else
                {
                    LOG.Warn("NotarizeController.Authorized => No recibio Authentication en el Header!");
                }

            }
            catch (Exception ex)
            {
                ret = false;
                msg = "NotarizeController.Authorized Excp: " + ex.Message;
                LOG.Error("NotarizeController.Authorized Excp: " + ex.Message);
            }
            return ret;
        }

        private static bool CredentialOk(string encryptcredential, out string msg)
        {
            bool ret = false;
            msg = "";
            try
            {
                string[] arrS = encryptcredential.Split(' ');
                string plaincredential = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(arrS[1]));

                string[] arrCredential = plaincredential.Split(':');
                string msg1;
                Company company = AdministradorCompany.RetrieveAuthCredential(arrCredential[0], out msg1);

                if (company != null)
                {
                    if (company.Status == 0)
                    { //Si está deshabilitado
                        msg = "La compañia esta inhabilitada para hacer consultas";
                        if (company.EndDate.HasValue)
                        {
                            msg = msg + " desde el " + company.EndDate.Value.ToString("dd/MM/yyyy");
                        }
                        LOG.Debug("NotarizeController.CredentialOk - Acceso NOOK de Username = " + arrCredential[0] +
                                     "[" + msg + "]");
                    }
                    else
                    {
                        if (company.SecretKey.Equals(arrCredential[1].Trim()))
                        {
                            LOG.Debug("NotarizeController.CredentialOk - Acceso OK de Username = " + arrCredential[0]);
                            ret = true;
                        }
                        else
                        {
                            LOG.Debug("NotarizeController.CredentialOk - Acceso NOOK de Username = " + arrCredential[0] +
                                     "[No coinciden SecretKeys => Param=" + arrCredential[1].Trim() + " <> BD=" +
                                     company.SecretKey + "]");
                        }
                    }
                }
                else
                {
                    LOG.Warn("NotarizeController.CredentialOk - Username no encontrado => " + arrCredential[0] + "[msg=" + msg + "]");
                }
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("NotarizeController.CredentialOk Excp: " + ex.Message);
            }
            return ret;
        }


        #endregion Private

    }
}
