﻿using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.Core.Services;
using Bio.Digital.Receipt.libs;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Bio.Digital.Receipt.rest.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class CIController : ApiController
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(CIController));


        /// <summary>
        /// Registra un resultado de transaccion realizada en estacion remota (MOC) 
        /// </summary>
        /// <remarks>
        /// Los valores enviados como parámetro determian la forma de verificación que se realizará para 
        /// hacer la certificación, a saber:
        /// * company: id informado en el moemnto del alta del servicio (int)
        /// * application: : id informado en el moemnto del alta del servicio (int)
        /// * typeId: tipo de doc a usar para certificar identidad (RUT, PAS, etc)
        /// * valueId: Nro de documento a verificar. Si es RUT formato xxxxxxxx-y
        /// * mailCertify: mail de persona a certificar. Se envia mail para que inice desde alli el proceso de certificación.
        /// * cellNumber: número de celular apra enviar link de procesod e certificacion via wathsapp [Aun no integrado]
        /// * theme: Nombre del tema gráfico a usar, donde se definen colores, logos, etc. Si va nulo se usa default.
        /// * customerTrackId: valor opcional de id unico interno del sistema que crea transaccion. 
        /// * callbackUrl: URL para callback cuando se termine el proceso de certificacion. Sew hará un POST con el trackid de
        /// la transaccion para que el sistema externo recupere los datos que necesite para seguir con su flujo de negocios.
        /// * redirectUrl: Si se desea redirigir a una pagina (Opcional) 
        /// * workflowname: El nombre del workflow que deberá seguir esta transaccion. Inicialmente usaremos solo "default",
        /// luego agregaremos mantenedor de nuevos workflow para aplicar a diversos casos de negocio.
        /// </remarks>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/ciweb/ciRegisterTx")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "CIRegisterModelR", typeof(CIRegisterModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(ErrorModel))]
        public object CIRegisterTx([FromBody] CIRegisterModel param) //string mail, int notaryid, int procedureid
        {
            /*
                1) Check Parametro 
                2) Check Acceso/Habilitacion
                3) Check TrackId 
                4) Grabo en BD
                5) Retorno si hay error
                6) Informo por mail y/o wsap
                7) Retorno ok
            */
            string msgerr = null;
            CIRegisterModel paramIn;
            RdCITx oRdCITx = null;
            try
            {
                LOG.Debug("CIController.CIRegisterTx - IN...");

                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("CIController.CIRegisterTx - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }


                //1) Deserializo parámetros y chequeo   
                if (param == null)
                {
                    Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null)));
                }
                //string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                //LOG.Debug("NotarizeController.InitProcedureOne - Deserializo ParamIn => " + strIn);
                //InitProcedureOneRequestModel paramIn = JsonConvert.DeserializeObject<InitProcedureOneRequestModel>(strIn);
                paramIn = param;

                if (paramIn == null)
                {
                    LOG.Debug("CIController.CIRegisterTx - Error deserializando parametros de entrada [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                            (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Error deserializando parametros de entrada [Null]", null)));
                }
                else
                {
                    string msgRet = "";
                    if (paramIn.company <= 0 || paramIn.application <= 0) //String.IsNullOrEmpty(paramIn.document))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "company y application deben tener un valor entero > 0" :
                              "|company y aplication deben tener un valor entero > 0");
                    }
                    if (string.IsNullOrEmpty(paramIn.typeId) || string.IsNullOrEmpty(paramIn.valueId))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "TypeId/ValueId no pueden ser nulos" : "|TypeId/ValueId no pueden ser nulos");
                    }
                    if (string.IsNullOrEmpty(paramIn.mailCertify)) // && string.IsNullOrEmpty(paramIn.cellNumber))
                    {
                        //msgRet += (string.IsNullOrEmpty(msgRet) ? "Mail o Celular deben ser informados para enviar al usuario" :
                        //    "|Mail o Celular deben ser informados para enviar al usuario");
                        paramIn.mailCertify = "info@notariovirtual.cl";
                    }
                    //LOG.Debug("CIController.CIRegisterTx - Check listMailsDestinatary en paramIn = " + (paramIn.listMailsDestinatary == null ? "NULL" : "Len =" +
                    //                                               paramIn.listMailsDestinatary.Length));
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                                (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                    "Problemas para registrar CI. [" + msgRet + "]", null)));
                    }
                }

                //2) Chequeo de acceso y habilitacion
                LOG.Debug("CIController.CIRegisterTx - Control acceso companyid = " + paramIn.company);
                int qext;
                string strapp, msg;
                int retaux = CIServiceManager.CheckAppAndCompanyEnabled(paramIn.company, paramIn.application, out qext, out strapp, out msg);
                if (retaux < 0)
                {
                    LOG.Error("CIController.CIRegisterTx - Error chequeando company [" + retaux.ToString() + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new ErrorModel(Errors.IRET_API_CONFLIC, "Error chequeando company [" + retaux.ToString() + "]. Reintente...", null)));
                }

                LOG.Debug("CIController.CIRegisterTx - Inicio register CI para = " + paramIn.typeId + " " + paramIn.valueId);

                //3) Verifico que existe TrackId 
                oRdCITx = AdministradorRdCITx.GetCITxByTrackId(paramIn.trackId, out msgerr);
                if (oRdCITx == null)
                {
                    LOG.Error("CIController.CIRegisterTx - TrackId generado recuperado Null [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new ErrorModel(Errors.IRET_API_CONFLIC, "Trackid no existe! Reintente...",
                        null)));
                }

                //4)  Grabo en BD
                int ret = 0;
                oRdCITx.ActionId = 10; //Para distinguir la verificacion con huella de la facial esto es Huella vs Cedula
                oRdCITx.FingerSample = paramIn.fingerSample;
                oRdCITx.FingerSampleJpg = paramIn.fingerSampleJpg;
                oRdCITx.Selfie = paramIn.selfie;
                oRdCITx.Threshold = paramIn.threshold.ToString();
                oRdCITx.Score = paramIn.score.ToString();
                oRdCITx.DestinataryMail = paramIn.mailCertify;
                oRdCITx.CellPhone = paramIn.cellNumber;
                if (paramIn.score >= paramIn.threshold) //Positivo => Registro y RD
                {
                    oRdCITx.ResultCode = 1;
                    ret = CIControllerUnitOfWork.GeneraCertify(oRdCITx, strapp, out oRdCITx, out msgerr);
                } else
                {
                    oRdCITx.ResultCode = 2;
                    ret = AdministradorRdCITx.SaveCITx(oRdCITx);
                }
                
                //6) Retorno si hay error
                if (ret < 0)
                {
                    LOG.Error("CIController.CIRegisterTx - Error registrando CI Tx Web [" + ret + "-" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                        (new ErrorModel(Errors.IRET_ERR_CI_TX_WEB_CREATE, "Error registrando CI Tx Web [" + msgerr + "]", null)));
                }

                //7 - Informo x mail
                if (!string.IsNullOrEmpty(paramIn.mailCertify) && !string.IsNullOrEmpty(oRdCITx.CertifyPDF))
                {
                    if (Properties.Settings.Default.CIInformResultByMail == 1 && !string.IsNullOrEmpty(paramIn.mailCertify))
                    {
                        //Added 10-05-2020 - Send mail HTMl con certificados attachados. MAndo PDF + XML
                        LOG.Debug("Informo por mail " + paramIn.mailCertify + " a destinatario enviadno CI...");
                        string[] strbody = new string[2];
                        strbody[0] = "Usted completó la Certificación de identidad para el documento <b>" +
                                       paramIn.typeId + " " + paramIn.valueId + "</b>";
                        strbody[1] = "El codigo unico del tramite para consultas es: <b>" + paramIn.trackId + "</b>";
                        byte[] docpdf = Convert.FromBase64String(oRdCITx.CertifyPDF);
                        //byte[] docxml = System.Text.Encoding.UTF8.GetBytes(oXmlOut.Receipt);
                        //Send PDF

                        bool bret = libs.Utils.NVNotify(paramIn.mailCertify,
                                            "Certificación de Identidad completada con exito [PDF]", strbody, docpdf, "pdf",
                                            paramIn.trackId + ".pdf");
                        LOG.Debug("Enviado PDF => " + (bret.ToString()));
                        //bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                        //                    "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
                        //                    oXmlOut.CIParam.TrackId + ".xml");
                        //LOG.Debug("Enviado XML => " + (bret.ToString()));
                    }

                }


                //if (retaux < 0)
                //{
                //    LOG.Warn("NotarizeController.CICreateTx - Error enviando notificaciones = " + retaux);
                //    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                //                 (new ErrorModel(Errors.IRET_ERR_CI_TX_SEND_NOTIFY, warning, null)));
                //}

            }
            catch (Exception ex)
            {
                LOG.Error("CIController.CIRegisterTx - Error registrando CI: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    (new ErrorModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconcido registrando CI ["
                                                  + ex.Message + "]", null)));
            }

            LOG.Debug("CIController.CIRegisterTx OUT!");
            //8 - Retorno OK
            return Request.CreateResponse(HttpStatusCode.OK,
                                          new CIRegisterModelR(paramIn.trackId, 
                                                               (oRdCITx!=null ? oRdCITx.CertifyPDF : null))); //Ok(); // Request.CreateResponse(HttpStatusCode.OK, response);

        }

       
        [Route("api/ciweb/ciVerifyNECTx")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "CIVerifyModelR", typeof(CIVerifyModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(ErrorModel))]
        public object CIVerifyNECTx([FromBody] CIVerifyModel param)
        {
            /*
                1) Check Parametro 
                2) Check Acceso/Habilitacion
                3) Check TrackId y parametros
                4) Verifico contra BP
                4) Grabo en BD positivo o negativo
                5) Informo por mail y/o wsap
                6) Retorno resultado
            */
            string msgerr = null;
            CIVerifyModel paramIn;
            int resultVerify = 0;
            double scoreOut;
            RdCITx oRdCITx = null;
            try
            {
                LOG.Debug("CIController.CIVerifyNECTx - IN...");

                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("CIController.CIVerifyNECTx - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }


                //1) Deserializo parámetros y chequeo   
                if (param == null)
                {
                    Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null)));
                }
                //string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                //LOG.Debug("NotarizeController.InitProcedureOne - Deserializo ParamIn => " + strIn);
                //InitProcedureOneRequestModel paramIn = JsonConvert.DeserializeObject<InitProcedureOneRequestModel>(strIn);
                paramIn = param;

                if (paramIn == null)
                {
                    LOG.Debug("CIController.CIVerifyNECTx - Error deserializando parametros de entrada [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                            (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Error deserializando parametros de entrada [Null]", null)));
                }
                else
                {
                    string msgRet = "";
                    if (paramIn.company <= 0 || paramIn.application <= 0) //String.IsNullOrEmpty(paramIn.document))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "company y application deben tener un valor entero > 0" :
                              "|company y aplication deben tener un valor entero > 0");
                    }
                    if (string.IsNullOrEmpty(paramIn.typeId) || string.IsNullOrEmpty(paramIn.valueId))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "TypeId/ValueId no pueden ser nulos" : "|TypeId/ValueId no pueden ser nulos");
                    }
                    if (string.IsNullOrEmpty(paramIn.fingerSample)) //string.IsNullOrEmpty(paramIn.pdf417) Lo saco porque puede venir nulo para usar el que ya esta en la BD en RdCITx.Pdf417
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "PDF417/FingerSample no pueden ser nulos" : "|PDF417/FingerSample no pueden ser nulos");
                    }
                    if (string.IsNullOrEmpty(paramIn.mailCertify)) // && string.IsNullOrEmpty(paramIn.cellNumber))
                    {
                        //msgRet += (string.IsNullOrEmpty(msgRet) ? "Mail o Celular deben ser informados para enviar al usuario" :
                        //    "|Mail o Celular deben ser informados para enviar al usuario");
                        paramIn.mailCertify = "info@notariovirtual.cl";
                    }
                    //LOG.Debug("CIController.CIRegisterTx - Check listMailsDestinatary en paramIn = " + (paramIn.listMailsDestinatary == null ? "NULL" : "Len =" +
                    //                                               paramIn.listMailsDestinatary.Length));
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                                (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                    "Problemas para registrar CI. [" + msgRet + "]", null)));
                    }
                }

                //2) Chequeo de acceso y habilitacion
                LOG.Debug("CIController.CIVerifyNECTx - Control acceso companyid = " + paramIn.company);
                int qext;
                string strapp, msg;
                int retaux = CIServiceManager.CheckAppAndCompanyEnabled(paramIn.company, paramIn.application, out qext, out strapp, out msg);
                if (retaux < 0)
                {
                    LOG.Error("CIController.CIVerifyNECTx - Error chequeando company [" + retaux.ToString() + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new ErrorModel(Errors.IRET_API_CONFLIC, "Error chequeando company [" + retaux.ToString() + "]. Reintente...", null)));
                }

                LOG.Debug("CIController.CIVerifyNECTx - Inicio register CI para = " + paramIn.typeId + " " + paramIn.valueId);

                //3) Verifico que existe TrackId 
                oRdCITx = AdministradorRdCITx.GetCITxByTrackId(paramIn.trackId, out msgerr);
                if (oRdCITx == null)
                {
                    LOG.Error("CIController.CIVerifyNECTx - TrackId generado recuperado Null [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new ErrorModel(Errors.IRET_API_CONFLIC, "Trackid no existe! Reintente...",
                        null)));
                }

                //Verifico que venga en param PDF417 o que esté en BD
                string PDF417ToVerify = null;
                if (string.IsNullOrEmpty(paramIn.pdf417) && string.IsNullOrEmpty(oRdCITx.Pdf417))
                {
                    LOG.Error("CIController.CIVerifyNECTx - Pdf417 Null en parametro y en BD. Complete lo que corresponda y reintente...");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new ErrorModel(Errors.IRET_API_CONFLIC, "Pdf417 Null en parametro y en BD. Complete lo que corresponda y reintente...",
                        null)));
                } else
                {
                    PDF417ToVerify = string.IsNullOrEmpty(paramIn.pdf417) ? oRdCITx.Pdf417 : paramIn.pdf417;
                }

                //4) Verifico contra BP
                int ret = 0;
                LOG.Debug("CIController.CIVerifyNECTx - Ingresando a BPHelper.VerifyCIvsCedulaNEC...");
                ret = Core.Helpers.BPHelper.VerifyCIvsCedulaNEC(Properties.Settings.Default.CIBPCompanyId, paramIn.valueId,
                                                                paramIn.fingerSample, PDF417ToVerify, paramIn.threshold,
                                                                out resultVerify, out scoreOut, out msgerr);
                LOG.Debug("CIController.CIVerifyNECTx - Sale de BPHelper.VerifyCIvsCedulaNEC => ret = " + ret.ToString());
                //5)  Grabo en BD 
                oRdCITx.ActionId = 10;  //CI de Huella vs Cedula
                oRdCITx.FingerSample = paramIn.fingerSample;
                oRdCITx.FingerSampleJpg = paramIn.fingerSampleJpg;
                oRdCITx.Selfie = paramIn.selfie;
                oRdCITx.Threshold = paramIn.threshold.ToString();
                oRdCITx.Score = scoreOut.ToString();
                oRdCITx.DestinataryMail = paramIn.mailCertify;
                oRdCITx.CellPhone = paramIn.cellNumber;
                if (scoreOut >= paramIn.threshold) //Positivo => Registro y RD
                {
                    oRdCITx.ResultCode = 1;
                    LOG.Debug("CIController.CIVerifyNECTx - Ingresando a CIControllerUnitOfWork.GeneraCertify...");
                    ret = CIControllerUnitOfWork.GeneraCertify(oRdCITx, strapp, out oRdCITx, out msgerr);
                    LOG.Debug("CIController.CIVerifyNECTx - Sale de CIControllerUnitOfWork.GeneraCertify => ret = " + ret.ToString());
                }
                else
                {
                    LOG.Debug("CIController.CIVerifyNECTx - Save Verify Negativa");
                    oRdCITx.ResultCode = 2;
                    ret = AdministradorRdCITx.SaveCITx(oRdCITx);
                }

                //6) Retorno si hay error
                if (ret < 0)
                {
                    LOG.Error("CIController.CIVerifyNECTx - Error verificando y registrando CI Tx Web NEC [" + ret + "-" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                        (new ErrorModel(Errors.IRET_ERR_CI_TX_WEB_CREATE, "Error verificando y registrando CI Tx Web NEC  [" + msgerr + "]", null)));
                }

                //7 - Informo x mail
                if (!string.IsNullOrEmpty(paramIn.mailCertify)) 
                {
                    if (Properties.Settings.Default.CIInformResultByMail == 1 && !string.IsNullOrEmpty(paramIn.mailCertify))
                    {
                        //Added 10-05-2020 - Send mail HTMl con certificados attachados. MAndo PDF + XML
                        LOG.Debug("Informo por mail " + paramIn.mailCertify + " a destinatario enviadno CI...");
                        string[] strbody = new string[2];
                        strbody[0] = "Usted completó la Certificación de identidad para el documento <b>" +
                                       paramIn.typeId + " " + paramIn.valueId + "</b>";
                        strbody[1] = "El codigo unico del tramite para consultas es: <b>" + paramIn.trackId + "</b>";
                        byte[] docpdf = Convert.FromBase64String(oRdCITx.CertifyPDF);
                        //byte[] docxml = System.Text.Encoding.UTF8.GetBytes(oXmlOut.Receipt);
                        //Send PDF

                        bool bret = libs.Utils.NVNotify(paramIn.mailCertify,
                                            "Certificación de Identidad completada con exito [PDF]", strbody, docpdf, "pdf",
                                            paramIn.trackId + ".pdf");
                        LOG.Debug("CIController.CIVerifyNECTx - Enviado PDF => " + (bret.ToString()));
                        //bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                        //                    "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
                        //                    oXmlOut.CIParam.TrackId + ".xml");
                        //LOG.Debug("Enviado XML => " + (bret.ToString()));
                    }

                }


                //if (retaux < 0)
                //{
                //    LOG.Warn("NotarizeController.CICreateTx - Error enviando notificaciones = " + retaux);
                //    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                //                 (new ErrorModel(Errors.IRET_ERR_CI_TX_SEND_NOTIFY, warning, null)));
                //}

            }
            catch (Exception ex)
            {
                LOG.Error("CIController.CIVerifyNECTx - Error registrando CI: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    (new ErrorModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconcido registrando CI ["
                                                  + ex.Message + "]", null)));
            }

            LOG.Debug("CIController.CIVerifyNECTx OUT!");
            //8 - Retorno OK
            return Request.CreateResponse(HttpStatusCode.OK,
                    (new CIVerifyModelR(paramIn.trackId, "RUT", paramIn.valueId, paramIn.threshold, scoreOut, resultVerify, oRdCITx.CertifyPDF)));
            // Request.CreateResponse(HttpStatusCode.OK, response);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trackidci"></param>
        /// <param name="mail"></param>
        /// <param name="format">pdf|xml</param>
        /// <returns></returns>
        [Route("api/ciweb/ciDownloadCI")]
        [HttpGet]
        public object DownloadCI(string trackidci, string format)
        {
            string msgerr = "";
            try
            {
                LOG.Debug("CIController.DownloadCI - IN...");
                //1) Deserializo parámetros     
                if (string.IsNullOrEmpty(trackidci))
                {
                    LOG.Debug("CIController.DownloadCI - trackid nulo! => Retorno");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST,
                        "TrackId consultado no puede ser nulo", trackidci)));
                }

                //2) Recupero tramite y si está statis = Notarzed => Devuelvo sino msg error
                RdCITx tx = AdministradorRdCITx.GetCITxByTrackId(trackidci, out msgerr);

                if (tx == null)
                {
                    LOG.Error("CIController.DownloadCI Error: TrackId consultado no existe");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        (new NotarizeResponseModel(Errors.IRET_API_ERR_NOT_FOUND,
                        "TrackId consultado no existe", trackidci)));
                }
                else if (string.IsNullOrEmpty(tx.CertifyPDF))
                {
                    LOG.Error("CIController.DownloadCI Error: TrackId consultado con PDF nulo [" + trackidci + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new NotarizeResponseModel(Errors.IRET_ERR_PROCEDURE_PENDING,
                        "TrackId consultado con PDF nulo", trackidci)));
                }
                else  //Todo ok => Armo salida 
                {
                    ////Debe verificar si el QR sigue siendo valido apra verificar sino regenerarlo 
                    ////  - iniciar nuevo proceso CI
                    ////  - Update en BD
                    //CheckValidityQR(nvUPE, out nvUPE);

                    //string sfile = System.IO.File.ReadAllText(@"C:\tmp\doc.base64.txt");
                    LOG.Debug("CIController.DownloadCI - Descargando " + trackidci + ".pdf");
                    byte[] byFile = Convert.FromBase64String(tx.CertifyPDF);
                    MemoryStream ms = new MemoryStream(byFile);
                    //var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/test.docx"); ;
                    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                    //var stream =    new FileStream(path, FileMode.Open);
                    result.Content = new StreamContent(ms); // (stream);
                    //result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    //result.Content.Headers.ContentDisposition.FileName = nvUP.TrackId + ".pdf"; // Path.GetFileName(path);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.ContentLength = ms.Length;// stream.Length;
                    LOG.Debug("CIController.DownloadCI - Descarga OK!");
                    return result;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIController.DownloadCI Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                       (new NotarizeResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR,
                       "Error descargadno CI PDF [" + ex.Message + "]", trackidci)));
            }
        }

        [Route("api/ciweb/ciInformPDFbyMail")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Error", typeof(ErrorModel))]
        public object CIResendTx(string trackid, string mail) //string mail, int notaryid, int procedureid
        {
            /*
                1) Check Parametro 
                2) Check Acceso/Habilitacion
                3) Recupero Tx
                4) Retorno ok
            */
            CIResendModel paramIn;
            string msgRet = "";
            string warning = null;
            try
            {
                LOG.Debug("CIController.ciInformPDFbyMail - IN...");

                //0) Check Basic Authentication
                //if (!Authorized(Request, out msgRet))
                //{
                //    LOG.Warn("NotarizeController.TxCreate - No Autorizado!");
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                //        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                //                        (string.IsNullOrEmpty(msgRet) ? "" : msgRet) + "]", null)));
                //}

                //1) Deserializo parámetros y chequeo   
                if (string.IsNullOrEmpty(trackid) || string.IsNullOrEmpty(mail))
                {
                    Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null)));
                }

                //2) Chequeo de acceso y habilitacion
                //LOG.Debug("NotarizeController.CIResendTx - Control acceso companyid = " + paramIn.company);
                //int qext;
                //string strapp, msg;
                //int retaux = CIServiceManager.CheckAppAndCompanyEnabled(paramIn.company, out qext, out strapp, out msg);
                //if (retaux < 0)
                //{
                //    LOG.Error("NotarizeController.CIResendTx - Check Acceso incorrecto para company = " + paramIn.company + 
                //        "[" + (string.IsNullOrEmpty(msg)?"":msg) + "]");
                //    return Request.CreateResponse(HttpStatusCode.Conflict,
                //        (new ErrorModel(Errors.IRET_API_CONFLIC, "Check Acceso incorrecto para company = " + paramIn.company + 
                //        "[" + (string.IsNullOrEmpty(msg) ? "" : msg) + "]", paramIn.trackid)));
                //}

                //3) Recupero Tx
                RdCITx tx = AdministradorRdCITx.GetCITxByTrackId(trackid, out msgRet);
                if (tx == null)
                {
                    LOG.Error("CIController.ciInformPDFbyMail - RdCITx recuperada [Null] para trackid = " + trackid);
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        (new ErrorModel(Errors.IRET_ERR_CI_NO_IN_DB, "RdCITx recuperada[Null] para trackid = " + trackid,
                        null)));
                }

                //4 - Informo x mail
                LOG.Debug("CIController.ciInformPDFbyMail - Inicio resend CI para = " + tx.TypeId + " " + tx.ValueId);
                //Primero mando a destinatario de CI que empiece proceso de CI, luego a los destinatarios adicionales
                //int retaux = SendToDestinataryCIWeb(tx, paramIn.company, null, null, null, null, null, null, null, out warning);
                LOG.Debug("Informo por mail " + mail + " a destinatario enviadno CI...");
                string[] strbody = new string[2];
                strbody[0] = "Usted completó la Certificación de identidad para el documento <b>" +
                               tx.TypeId + " " + tx.ValueId + "</b>";
                strbody[1] = "El codigo unico del tramite para consultas es: <b>" + trackid + "</b>";
                byte[] docpdf = Convert.FromBase64String(tx.CertifyPDF);
                //byte[] docxml = System.Text.Encoding.UTF8.GetBytes(oXmlOut.Receipt);
                //Send PDF

                bool bret = libs.Utils.NVNotify(mail,
                                    "Certificación de Identidad completada con exito [PDF]", strbody, docpdf, "pdf",
                                    trackid + ".pdf");
                LOG.Debug("Enviado PDF => " + (bret.ToString()));

                if (!bret)
                {
                    LOG.Warn("NotarizeController.CIResendTx - Error resend CI para = " + tx.TypeId + " " + tx.ValueId 
                        + " - trackid = " + tx.TrackId);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                                 (new ErrorModel(Errors.IRET_ERR_CI_TX_SEND_NOTIFY, warning, null)));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIController.ciInformPDFbyMail Error reenviando link para CI : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    (new ErrorModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error reenviando link para CI ["
                                                  + ex.Message + "]", null)));
            }
            LOG.Debug("CIController.ciInformPDFbyMail - Out!");
            //8 - Retorno OK
            return Ok(); // new CICreateModelR((string.IsNullOrEmpty(warning) ? null : warning), BPResponse.verifyUrl)); // Request.CreateResponse(HttpStatusCode.OK, response);
        }


        #region Login

        //[Route("api/ciweb/ciLogin")]
        //[HttpPost]
        //[SwaggerResponse(HttpStatusCode.OK)] //, "CICreateModelR", typeof(CICreateModelR))]
        //[SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(ErrorModel))]
        //public object CILogin(string user, string password) //string mail, int notaryid, int procedureid
        //{
        //    /*
        //        1) Check Parametro 
        //        2) Check Acceso/Habilitacion
        //        3) Check User/passw 
        //        4) Retorno ok/nook
        //    */
        //    string msgerr = null;
        //    try
        //    {
        //        LOG.Debug("CIController.CILogin - IN...");

        //        //0) Check Basic Authentication
        //        if (!NotarizeController.Authorized(Request, out msgerr))
        //        {
        //            LOG.Warn("CIController.CILogin - No Autorizado!");
        //            return Request.CreateResponse(HttpStatusCode.Unauthorized,
        //                (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
        //                                (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
        //        }

        //        if (string.IsNullOrEmpty(user) || string.IsNullOrEmpty(password))
        //        {
        //            LOG.Debug("CIController.CILogin - User/Clave no deben ser nulos");
        //            return Request.CreateResponse(HttpStatusCode.BadRequest,
        //                    (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "User/Clave no deben ser nulos", null)));
        //        }

        //        //2) Chequeo de acceso y habilitacion
        //        //LOG.Debug("CIController.CILogin - Control acceso companyid = " + paramIn.company);
        //        //int qext;
        //        //string strapp, msg;
        //        //int retaux = CIServiceManager.CheckAppAndCompanyEnabled(paramIn.company, paramIn.application, out qext, out strapp, out msg);
        //        //if (retaux < 0)
        //        //{
        //        //    LOG.Error("CIController.CILogin - Error chequeando company [" + retaux.ToString() + "]");
        //        //    return Request.CreateResponse(HttpStatusCode.Conflict,
        //        //        (new ErrorModel(Errors.IRET_API_CONFLIC, "Error chequeando company [" + retaux.ToString() + "]. Reintente...", null)));
        //        //}

        //        //LOG.Debug("CIController.CILogin - Inicio register CI para = " + paramIn.typeId + " " + paramIn.valueId);

        //        //3) Verifico que existe TrackId 
        //        RdCITx oRdCITx = AdministradorRdCITx.GetCITxByTrackId(paramIn.trackId, out msgerr);
        //        if (oRdCITx == null)
        //        {
        //            LOG.Error("CIController.CILogin - TrackId generado recuperado Null [" + msgerr + "]");
        //            return Request.CreateResponse(HttpStatusCode.Conflict,
        //                (new ErrorModel(Errors.IRET_API_CONFLIC, "Trackid no existe! Reintente...",
        //                null)));
        //        }

        //        //4)  Grabo en BD
        //        int ret = 0;
        //        oRdCITx.ActionId = 3;
        //        oRdCITx.FingerSample = paramIn.fingerSample;
        //        oRdCITx.FingerSampleJpg = paramIn.fingerSampleJpg;
        //        oRdCITx.Selfie = paramIn.selfie;
        //        oRdCITx.Threshold = paramIn.threshold.ToString();
        //        oRdCITx.Score = paramIn.score.ToString();
        //        oRdCITx.DestinataryMail = paramIn.mailCertify;
        //        oRdCITx.CellPhone = paramIn.cellNumber;
        //        if (paramIn.score >= paramIn.threshold) //Positivo => Registro y RD
        //        {
        //            oRdCITx.ResultCode = 1;
        //            ret = CIControllerUnitOfWork.GeneraCertify(oRdCITx, strapp, out oRdCITx, out msgerr);
        //        }
        //        else
        //        {
        //            oRdCITx.ResultCode = 2;
        //            ret = AdministradorRdCITx.SaveCITx(oRdCITx);
        //        }

        //        //6) Retorno si hay error
        //        if (ret < 0)
        //        {
        //            LOG.Error("CIController.CIRegisterTx - Error registrando CI Tx Web [" + ret + "-" + msgerr + "]");
        //            return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
        //                (new ErrorModel(Errors.IRET_ERR_CI_TX_WEB_CREATE, "Error registrando CI Tx Web [" + msgerr + "]", null)));
        //        }

        //        //7 - Informo x mail
        //        if (!string.IsNullOrEmpty(paramIn.mailCertify) && !string.IsNullOrEmpty(oRdCITx.CertifyPDF))
        //        {
        //            if (Properties.Settings.Default.CIInformResultByMail == 1 && !string.IsNullOrEmpty(paramIn.mailCertify))
        //            {
        //                //Added 10-05-2020 - Send mail HTMl con certificados attachados. MAndo PDF + XML
        //                LOG.Debug("Informo por mail " + paramIn.mailCertify + " a destinatario enviadno CI...");
        //                string[] strbody = new string[2];
        //                strbody[0] = "Usted completó la Certificación de identidad para el documento <b>" +
        //                               paramIn.typeId + " " + paramIn.valueId + "</b>";
        //                strbody[1] = "El codigo unico del tramite para consultas es: <b>" + paramIn.trackId + "</b>";
        //                byte[] docpdf = Convert.FromBase64String(oRdCITx.CertifyPDF);
        //                //byte[] docxml = System.Text.Encoding.UTF8.GetBytes(oXmlOut.Receipt);
        //                //Send PDF

        //                bool bret = libs.Utils.NVNotify(paramIn.mailCertify,
        //                                    "Certificación de Identidad completada con exito [PDF]", strbody, docpdf, "pdf",
        //                                    paramIn.trackId + ".pdf");
        //                LOG.Debug("Enviado PDF => " + (bret.ToString()));
        //                //bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
        //                //                    "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
        //                //                    oXmlOut.CIParam.TrackId + ".xml");
        //                //LOG.Debug("Enviado XML => " + (bret.ToString()));
        //            }

        //        }


        //        //if (retaux < 0)
        //        //{
        //        //    LOG.Warn("NotarizeController.CICreateTx - Error enviando notificaciones = " + retaux);
        //        //    return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //        //                 (new ErrorModel(Errors.IRET_ERR_CI_TX_SEND_NOTIFY, warning, null)));
        //        //}

        //    }
        //    catch (Exception ex)
        //    {
        //        LOG.Error("CIController.CILogin - Error registrando CI: " + ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError,
        //            (new ErrorModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconcido registrando CI ["
        //                                          + ex.Message + "]", null)));
        //    }

        //    LOG.Debug("CIController.CILogin OUT!");
        //    //8 - Retorno OK
        //    return Ok(); // Request.CreateResponse(HttpStatusCode.OK, response);

        //}


        #endregion Login




        //[Route("api/CI/Proccess/{customTimeOut?}")]
        //[HttpPost]
        //public XmlParamOut Proccess([FromBody] XmlParamIn xmlParamIn,
        //    int customTimeOut = 0)
        //{
        //    LOG.Info("CIController.Proccess");

        //    //string strXmlParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);
        //    string strXmlParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);

        //    if (string.IsNullOrEmpty(strXmlParamIn))
        //        return new XmlParamOut
        //        {
        //            ExecutionResult = Errors.IERR_SERIALIZE_XMLPARAMIN,
        //            Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
        //        };

        //    string strXmlParamOut;
        //    int ret = CIHelper.Proccess(strXmlParamIn, out strXmlParamOut, customTimeOut);

        //    XmlParamOut response = null;
        //    if (!string.IsNullOrEmpty(strXmlParamOut))
        //    {
        //        response = XmlHelper.DeserializeXmlParamOut(strXmlParamOut);
        //    }
        //    else
        //    {
        //        response = new XmlParamOut();
        //    }
        //    if (ret != 0)
        //    {
        //        LOG.Debug("Error generado en SOAP, traspasamos la respuesta a XMLParamOut, Result:" + ret);
        //        response.ExecutionResult = ret;
        //        response.Message += ", " + Errors.GetDescription(ret);
        //    }

        //    return response;
        //}

        //[Route("api/CI/CheckStatusCI/{customTimeOut?}")]
        //[HttpPost]
        //public int CheckStatusCI([FromUri] string trackid,
        //                    int customTimeOut = 0)
        //{
        //    LOG.Info("CIController.Proccess");

        //    if (string.IsNullOrEmpty(trackid))
        //        return Errors.IERR_SERIALIZE_XMLPARAMIN; //new XmlParamOut
        //                                                 //{
        //                                                 //    ExecutionResult = Errors.IERR_SERIALIZE_XMLPARAMIN,
        //                                                 //    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
        //                                                 //};

        //    //string strXmlParamOut;
        //    int ret = CIHelper.CheckStatusCI(trackid, customTimeOut);

        //    //XmlParamOut response = null;
        //    //if (!string.IsNullOrEmpty(strXmlParamOut))
        //    //{
        //    //    response = XmlHelper.DeserializeXmlParamOut(strXmlParamOut);
        //    //}
        //    //else
        //    //{
        //    //    response = new XmlParamOut();
        //    //}
        //    //if (ret != 0)
        //    //{
        //    //    LOG.Debug("Error generado en SOAP, traspasamos la respuesta a XMLParamOut, Result:" + ret);
        //    //    response.ExecutionResult = ret;
        //    //    response.Message += ", " + Errors.GetDescription(ret);
        //    //}

        //    return ret;
        //}

        #region TestMethods

        // GET: api/CI
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/CI/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/CI
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/CI/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/CI/5
        public void Delete(int id)
        {
        }

        #endregion TestMethods
    }
}
