﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using Newtonsoft.Json;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database.Notary.config;
using Bio.Digital.Receipt.Core.Helpers;
using System.Xml;
using System.Drawing;
using System.Net.Http.Headers;
using System.Collections;
using System.IO;
using System.Web.Http.Cors;
using Bio.Digital.Receipt.Core.Services;
using Swashbuckle.Swagger.Annotations;

namespace Bio.Digital.Receipt.rest.Controllers
{
    public class SignerController : ApiController
    {
        //Request.CreateResponse(HttpStatusCode.OK, new ResponseModel(200, "Login verificado con exito!"));
        private static readonly ILog LOG = LogManager.GetLogger(typeof(SignerController));

        
#region TramitesTAG

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/v1/signer/txCreate")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "SignerCreateModelR", typeof(SignerCreateModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(ErrorModel))]
        public object txCreate([FromBody] SignerCreateModel param) //string mail, int notaryid, int procedureid
        {
            /*
                1) Check Parametro 
                2) Check Acceso/Habilitacion
                3) Creo TrackId Nuevo
                4) Creo TxCreate en BioPortal
                5) Grabo en BD
                6) Retorno si hay error
                7) Informo por mail y/o wsap
                8) Retorno si hay error o
                9) Retorno ok
            */
            string trackId = null;
            string msgerr = null;
            SignerCreateModelR response = null;
            SignerCreateModel paramIn;
            string warning = null;
            BPTxCreateR BPResponse = null;
            string trackidbp = null;
            try
            {
                LOG.Debug("SignerController.txCreate - IN...");

                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("SignerController.txCreate - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }


                //1) Deserializo parámetros y chequeo   
                if (param == null)
                {
                    Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null)));
                }
                //string strIn = ((Newtonsoft.Json.Linq.JObject)param).ToString();
                //LOG.Debug("NotarizeController.InitProcedureOne - Deserializo ParamIn => " + strIn);
                //InitProcedureOneRequestModel paramIn = JsonConvert.DeserializeObject<InitProcedureOneRequestModel>(strIn);
                paramIn = param;

                if (paramIn == null)
                {
                    LOG.Debug("SignerController.txCreate - Error deserializando parametros de entrada [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                            (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Error deserializando parametros de entrada [Null]", null)));
                }
                else
                {
                    string msgRet = "";
                    if (paramIn.company <= 0 || paramIn.application <= 0) //String.IsNullOrEmpty(paramIn.document))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "company y application deben tener un valor entero > 0" :
                              "|company y aplication deben tener un valor entero > 0");
                    }
                    if (string.IsNullOrEmpty(paramIn.typeId) || string.IsNullOrEmpty(paramIn.valueId))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "TypeId/ValueId no pueden ser nulos" : "|TypeId/ValueId no pueden ser nulos");
                        ;
                    }
                    if (!string.IsNullOrEmpty(paramIn.typeId) && !string.IsNullOrEmpty(paramIn.valueId)
                        && paramIn.typeId.Equals("RUT") && !NotarizeController.IsCorrectRut(paramIn.valueId))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "Rut Nulo o Incorrecto" : "|Rut Nulo o Incorrecto");
                    }
                    if (!string.IsNullOrEmpty(paramIn.taxidcompany) && !NotarizeController.IsCorrectRut(paramIn.taxidcompany))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "Rut Company Incorrecto" : "|Rut Company Incorrecto");
                    }
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                                (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                    "Problemas para notarizar. [" + msgRet + "]", null)));
                    }
                }

                //2) Chequeo de acceso y habilitacion
                LOG.Debug("SignerController.txCreate - Control acceso companyid = " + paramIn.company);
                int qext;
                string strapp, msg;
                int retaux = CIServiceManager.CheckAppAndCompanyEnabled(paramIn.company, paramIn.application, out qext, out strapp, out msg);
                if (retaux < 0)
                {
                    LOG.Error("SignerController.txCreate - Error chequeando company [" + retaux.ToString() + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new ErrorModel(Errors.IRET_API_CONFLIC, "Error chequeando company[" + retaux.ToString() + "]. Reintente...", null)));
                }

                LOG.Debug("SignerController.txCreate - Inicio create Tx para = " + paramIn.typeId + " " + paramIn.valueId);

                //3) Creo TrackId Nuevo
                trackId = AdministradorRdCITx.GetTrackIdUnique();
                if (string.IsNullOrEmpty(trackId))
                {
                    LOG.Error("SignerController.txCreate - TrackId generado invalido [Null]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new ErrorModel(Errors.IRET_API_CONFLIC, "Problemas para crear id de transaccion unico. Reintente...",
                        null)));
                }

                //4) Creo TxCreate en BioPortal
                string listMails = null;
                int ret = BPHelper.TxCreateSigner(trackId, paramIn, out msgerr, out listMails, out trackidbp, out BPResponse);

                if (ret < 0 || BPResponse == null)
                {
                    LOG.Error("SignerController.txCreate -  BPHelper.TxCreate Error => " +
                        ret + " - " + (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr));
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new ErrorModel(Errors.IRET_API_CONFLIC, "Problemas iniciar proceso de verificacion en servicio biometrico. " +
                        "TxCreate Error => " + ret + " - " + (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr),
                        null)));
                }
                else
                {
                    LOG.Debug("SignerController.txCreate - Revisando lista destinatarios...");
                    LOG.Debug("SignerController.txCreate - " + (paramIn.listMailsDestinatary == null ? "NULL" : "Len =" +
                                                                    paramIn.listMailsDestinatary.Length));
                    paramIn.listMailsDestinatary = (paramIn.listMailsDestinatary == null || paramIn.listMailsDestinatary.Length == 0) ?
                                                    listMails.Replace(";", ",").Split(',') :
                                                    paramIn.listMailsDestinatary;
                    LOG.Debug("SignerController.txCreate - Lista definida...");
                    foreach (string item in paramIn.listMailsDestinatary)
                    {
                        LOG.Debug("NotarizeController.CICreateTx - item = " + item);
                    }
                }

                //5) Grabo en BD
                ret = 0;
                string trackidTx = "";
                ret = AdministradorRdCITx.CreateSignerTxWeb(trackId, paramIn, BPResponse, trackidbp, out msgerr);

                //6) Retorno si hay error
                if (ret < 0)
                {
                    LOG.Error("SignerController.txCreate - Error creando CI Tx Web [" + ret + "-" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                        (new ErrorModel(Errors.IRET_ERR_CI_TX_WEB_CREATE, "Error creando CI Tx Web [" + msgerr + "]", null)));
                }

                //7 - Informo x mail

                //Primero mando a destinatario de CI que empiece proceso de CI y luego a lista si existe
                //4 - Informo x mail
                LOG.Debug("SignerController.txCreate - Inicio notify Create Transaccion para = " + paramIn.typeId + " " + paramIn.valueId);
                //Primero mando a destinatario de CI que empiece proceso de CI, luego a los destinatarios adicionales
                string lMails = null;
                string[] sList = null;
                if (param.listMailsDestinatary != null)
                {
                    sList = param.listMailsDestinatary;
                }
                else if (!string.IsNullOrEmpty(listMails))
                {
                    sList = listMails.Replace(';', ',').Split(',');
                }
                //string lMails = (param.listMailsDestinatary!=null) ? param.listMailsDestinatary : listMails;
                bool isFirst = true;
                //if (!string.IsNullOrEmpty(lMails))
                //{

                if (sList != null && sList.Length > 0)
                {
                    foreach (string item in sList) //paramIn.listMailsDestinatary)
                    {
                        if (isFirst)
                        {
                            lMails = item;
                            isFirst = false;
                        }
                        else
                        {
                            lMails += ";" + item;
                        }
                    }
                    LOG.Debug("SignerController.txCreate - Envaindo a lista de mails = " +
                                (string.IsNullOrEmpty(lMails) ? "Null" : lMails));
                }
                //}
                //retaux = NotarizeController.SendToDestinataryCIWeb(null, paramIn.company, trackId, paramIn.typeId, paramIn.valueId,
                //                                BPResponse.verifyUrl, paramIn.mailCertify, lMails,
                //                                paramIn.cellNumber, out warning);

                //if (retaux < 0)
                //{
                //    LOG.Warn("NotarizeController.CICreateTx - Error enviando notificaciones = " + retaux);
                //    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                //                 (new ErrorModel(Errors.IRET_ERR_CI_TX_SEND_NOTIFY, warning, null)));
                //}

            }
            catch (Exception ex)
            {
                LOG.Error("SignerController.txCreate Error Iniciando trámite: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    (new ErrorModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error Desconcido procesando inicio de tramite ["
                                                  + ex.Message + "]", null)));
            }

            LOG.Debug("SignerController.txCreate OUT! => BPResponse.verifyUrl = " + BPResponse.verifyUrl);
            //8 - Retorno OK
            return Ok(new CICreateModelR((string.IsNullOrEmpty(warning) ? null : warning), trackId, BPResponse.verifyUrl)); // Request.CreateResponse(HttpStatusCode.OK, response);

        }
      

        /// <summary>
        /// Pide estado de una transacción
        /// </summary>
        /// <remarks>
        /// Enviando un trackid de transacción como parámetro del request, por ejemplo:  
        /// [request.AddParameter("trackid", "5d8e15df00af4f3ab7d6eefb9b5f8dec")], se revisa el estado, consultando 
        /// en BioPortal y actualizando los datos internos si es necesario. Si se completa la certificación, se envia 
        /// por mail la certificación a quien corresponda. 
        /// </remarks>
        /// <returns></returns>
        [Route("api/v1/signer/txStatus")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "CIStatusModelR", typeof(CIStatusModelR))]
        public object txStatus()
        {
            string msgerr;
            string trackid = null;
            RdCITx tx = null;
            CIStatusModelR oCIStatusModelR = null;
            try
            {
                LOG.Debug("SignerController.txStatus - IN...");

                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("SignerController.txStatus - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                trackid = System.Web.HttpContext.Current.Request.Params["trackid"];
                LOG.Debug("SignerController.txStatus - TrackId recibido => " + (string.IsNullOrEmpty(trackid) ? "Null" : trackid));

                tx = AdministradorRdCITx.GetCITxByTrackId(trackid, out msgerr);
                if (tx == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                                new ErrorModel(Errors.IRET_ERR_CI_NO_IN_DB, "Error Desconocido CI", "TrackId de Tx no existe"));

                RdCITx txout = null;
                int ret = SignerUnitOfWork.UpdateRdSignerTx(tx, out msgerr, out txout);
                if (ret < 0)
                    return Request.CreateResponse(HttpStatusCode.NotModified, new ErrorModel(ret, "Error Status/Update Tx", msgerr));

                //LLeno salida
                if (txout != null)
                    LOG.Debug("SignerController.txStatus - txout.ActionId = " + txout.ActionId.ToString());
                else
                {
                    LOG.Debug("SignerController.txStatus - Recupera de nuevo tx con trackid = " + trackid);
                    txout = AdministradorRdCITx.GetCITxByTrackId(trackid, out msgerr);
                    if (txout != null)
                        LOG.Debug("SignerController.txStatus - txout.ActionId = " + txout.ActionId.ToString());
                    else
                    {
                        LOG.Debug("SignerController.txStatus - Recuperacion NULA!");
                    }
                }
                LOG.Debug("SignerController.txStatus - Creando salida...");
                oCIStatusModelR = new CIStatusModelR();
                oCIStatusModelR.code = 0;
                oCIStatusModelR.msgerr = null;
                oCIStatusModelR.trackid = trackid;
                oCIStatusModelR.status = CIServiceManager.GetStatus(txout);
                oCIStatusModelR.statusdescription = (txout != null ? CIServiceManager.GetStatusDescription(txout.ActionId) : null);
            }
            catch (Exception ex)
            {
                LOG.Error("SignerController.txStatus Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new ErrorModel(-1, "Error Desconocido CI", ex.Message));
            }
            LOG.Debug("SignerController.txStatus - OUT!");

            return Request.CreateResponse(HttpStatusCode.OK, oCIStatusModelR);
        }

        /// <summary>
        /// Recupera datos relacionados con una transaccion
        /// </summary>
        /// <remarks>
        /// Utiliza Basic Authentication [ej.: request.AddHeader("Authorization", "Basic dXNlcm5hbWU3OnNlY3JldGtleTc=")] - 
        /// Dado un trackid unico, y tipo de data pedida, se recupera toda la data relacionada
        /// con la transaccion del tipo pedido:
        ///     * 0 -> Basico
        ///     * 1 -> 0 + Datos (sin imagenes)
        ///     * 2 -> 1 + Imagenes
        /// </remarks>
        /// <param name="trackid">Track id único que debe haberse generado antes</param>
        /// <param name="flag">0-Basico | 1-0+Texto | 2 - 1+Images</param>
        /// <response code="200">Tx Creada ok</response>
        /// <response code="404">Parámetros erroneos</response>
        /// <response code="4xx">Errores a Definir</response>
        /// <response code="500">Error Desconocido Interno</response>
        /// <returns>Estado de la transaccion</returns>
        [Route("api/v1/signer/txGet")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "TxSignerModelR", typeof(TxSignerModelR))]
        public object txGet()
        {
            string msgerr;
            string trackid = null;
            int flag = 0;
            RdCITx tx = null;
            TxSignerModelR oTxSignerModelR = null;
            try
            {
                LOG.Debug("SignerController.txGet - IN...");

                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("SignerController.txGet - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                trackid = System.Web.HttpContext.Current.Request.Params["trackid"];
                LOG.Debug("SignerController.txGet - TrackId recibido => " + (string.IsNullOrEmpty(trackid) ? "Null" : trackid));
                string sflag = System.Web.HttpContext.Current.Request.Params["flag"];
                if (string.IsNullOrEmpty(sflag))
                    sflag = "0";
                flag = Convert.ToInt32(sflag);
                if (flag < 0 || flag > 2)
                    flag = 0;

                LOG.Debug("SignerController.txGet - Recuperandpo Tx = " + trackid + " desde GetCITxByTrackId...");
                tx = AdministradorRdCITx.GetCITxByTrackId(trackid, out msgerr);
                if (tx == null)
                {
                    LOG.Debug("SignerController.txGet - Retorno desde GetCITxByTrackId nulo => Sale!");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                                new ErrorModel(Errors.IRET_ERR_CI_NO_IN_DB, "Error Recuperando Tx", "TrackId de Signer no existe"));
                }
                //LLeno salida
                LOG.Debug("SignerController.txGet - Creando salida...");
                msgerr = null;
                int ret = SignerUnitOfWork.GeneraResponseGet(tx, flag, out oTxSignerModelR, out msgerr);
                if (ret < 0)
                {
                    LOG.Debug("SignerController.txGet - Retorno desde GeneraResponseGet nulo => Sale!");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                                new ErrorModel(Errors.IRET_ERR_CI_NO_IN_DB, "Error Generando Salida", msgerr));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("SignerController.txGet Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new ErrorModel(-1, "Error Desconocido Signer", ex.Message));
            }
            LOG.Debug("SignerController.txGet - OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, oTxSignerModelR);
        }

        [Route("api/v1/signer/txVerify")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "TxSignerVerifyModelR", typeof(TxSignerVerifyModelR))]
        public object txVerify(string trackid, string securitycode)
        {
            string msgerr;
            int flag = 0;
            RdCITx tx = null;
            TxSignerVerifyModelR oTxSignerVerifyModelR = null;
            try
            {
                LOG.Debug("SignerController.txVerify - IN...");

                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("SignerController.txVerify - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                if (string.IsNullOrEmpty(trackid) || string.IsNullOrEmpty(securitycode))
                {
                    LOG.Debug("SignerController.txVerify - Retorno desde GetCITxByTrackId nulo => Sale!");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                                new ErrorModel(Errors.IERR_SERIALIZE_XMLPARAMIN, "Parametros en null", "TrackId o SecurityCode nulos. No se puede verificar"));
                }
                LOG.Debug("SignerController.txVerify - TrackId recibido => " + trackid + " - securitycode = " + securitycode);

                LOG.Debug("SignerController.txVerify - Recuperandpo Tx = " + trackid + " desde GetCITxByTrackId...");
                tx = AdministradorRdCITx.GetCITxByTrackId(trackid, out msgerr);
                if (tx == null)
                {
                    LOG.Debug("SignerController.txVerify - Retorno desde GetCITxByTrackId nulo => Sale!");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                                new ErrorModel(Errors.IRET_ERR_CI_NO_IN_DB, "Error Recuperando Tx", "TrackId de Signer no existe"));
                }
                //LLeno salida
                LOG.Debug("SignerController.txVerify - Creando salida...");
                if (!securitycode.Equals(tx.SecurityCode))
                {
                    LOG.Debug("SignerController.txVerify - Retorno Security Code distintos => " +
                                                           "securitycode=" + securitycode + " / tx.SecurityCode=" + 
                                                           (string.IsNullOrEmpty(tx.SecurityCode)?"Null":tx.SecurityCode));
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                                new ErrorModel(Errors.IRET_ERR_SIGNATURE_INVALID, "SecurityCode No Coincide", 
                                "El SecurityCode no coincide. Reintente..."));
                }

                msgerr = null;
                int ret = SignerUnitOfWork.GeneraResponseVerify(tx, flag, out oTxSignerVerifyModelR, out msgerr);
                if (ret < 0)
                {
                    LOG.Debug("SignerController.txVerify - Retorno desde GeneraResponseGet nulo => Sale!");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                                new ErrorModel(Errors.IRET_ERR_CI_NO_IN_DB, "Error Generando Salida", msgerr));
                }
            }
            catch (Exception ex)
            {
                LOG.Error("SignerController.txVerify Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new ErrorModel(-1, "Error Desconocido Signer", ex.Message));
            }
            LOG.Debug("SignerController.txVerify - OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, oTxSignerVerifyModelR);
        }

        [Route("api/v1/signer/WebhookBPWebFromSigner")]
        [HttpPost]
        public object WebhookBPWebFromSigner()
        {
            string msgerr;
            try
            {
                LOG.Debug("SignerController.WebhookBPWebFromSigner - IN...");
                string trackidbp = System.Web.HttpContext.Current.Request.Params["trackid"];
                LOG.Debug("SignerController.WebhookBPWebFromSigner - TrackIdBP recibido => " + (string.IsNullOrEmpty(trackidbp) ? "Null" : trackidbp));

                //TODO - Sacar Tx de CI desde el trackid de BP
                RdCITx tx = AdministradorRdCITx.GetCITxByTrackIdBP(trackidbp, out msgerr);

                if (tx == null)
                {
                    LOG.Warn("SignerController.WebhookBPWebFromSigner - RdCITx recuperada nula [" +
                                        (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr) + "]");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                                            new ErrorModel(Errors.IRET_ERR_CI_NO_IN_DB, "RdCITx recuperada nula [" +
                                        (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr) + "]", "TrackId de CI no existe"));
                }

                LOG.Debug("SignerController.WebhookBPWebFromSigner - Ingresando a UpdateRdCITx...");
                int ret = SignerUnitOfWork.UpdateRdSignerTx(tx, out msgerr, out tx);
                if (ret < 0)
                {
                    LOG.Warn("SignerController.WebhookBPWeb - Error en UpdateRdCITx ret = " + ret + "[" +
                             (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr) + "]");
                    return Request.CreateResponse(HttpStatusCode.NotModified, new ErrorModel(ret, "Error Status/Update CI", msgerr));
                }
                //TODO - Recfuperar desde BPWeb la info completa y generar la CI
            }
            catch (Exception ex)
            {
                LOG.Error("SignerController.WebhookBPWebFromSigner Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            LOG.Debug("SignerController.WebhookBPWebFromSigner - IN...");
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /*
     [Route("api/v1/signer/txProcess")]
     [HttpGet]
     [SwaggerResponse(HttpStatusCode.OK, "CIStatusModelR", typeof(CIStatusModelR))]
     public object txProcess()
     {
         string msgerr;
         string trackid = null;
         RdCITx tx = null;
         CIStatusModelR oCIStatusModelR = null;
         try
         {
             LOG.Debug("NotarizeController.CIStatusTx - IN...");

             //0) Check Basic Authentication
             if (!NotarizeController.Authorized(Request, out msgerr))
             {
                 LOG.Warn("NotarizeController.TxCreate - No Autorizado!");
                 return Request.CreateResponse(HttpStatusCode.Unauthorized,
                     (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                     (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
             }

             trackid = System.Web.HttpContext.Current.Request.Params["trackid"];
             LOG.Debug("NotarizeController.CIStatusTx - TrackId recibido => " + (string.IsNullOrEmpty(trackid) ? "Null" : trackid));

             tx = AdministradorRdCITx.GetCITxByTrackId(trackid, out msgerr);
             if (tx == null)
                 return Request.CreateResponse(HttpStatusCode.NotFound,
                             new ErrorModel(Errors.IRET_ERR_CI_NO_IN_DB, "Error Desconocido CI", "TrackId de CI no existe"));

             RdCITx txout = null;
             int ret = SignerUnitOfWork.UpdateRdSignerTx(tx, out msgerr, out txout);
             if (ret < 0)
                 return Request.CreateResponse(HttpStatusCode.NotModified, new ErrorModel(ret, "Error Status/Update CI", msgerr));

             //LLeno salida
             if (txout != null)
                 LOG.Debug("NotarizeController.CIStatusTx - txout.ActionId = " + txout.ActionId.ToString());
             else
             {
                 LOG.Debug("NotarizeController.CIStatusTx - Recupera de nuevo tx con trackid = " + trackid);
                 txout = AdministradorRdCITx.GetCITxByTrackId(trackid, out msgerr);
                 if (txout != null)
                     LOG.Debug("NotarizeController.CIStatusTx - txout.ActionId = " + txout.ActionId.ToString());
                 else
                 {
                     LOG.Debug("NotarizeController.CIStatusTx - Recuperacion NULA!");
                 }
             }
             LOG.Debug("NotarizeController.CIStatusTx - Creando salida...");
             oCIStatusModelR = new CIStatusModelR();
             oCIStatusModelR.code = 0;
             oCIStatusModelR.msgerr = null;
             oCIStatusModelR.trackid = trackid;
             oCIStatusModelR.status = CIServiceManager.GetStatus(txout);
             oCIStatusModelR.statusdescription = (txout != null ? CIServiceManager.GetStatusDescription(txout.ActionId) : null);
         }
         catch (Exception ex)
         {
             LOG.Error("NotarizeController.CIStatusTx Error: " + ex.Message);
             return Request.CreateResponse(HttpStatusCode.InternalServerError, new ErrorModel(-1, "Error Desconocido CI", ex.Message));
         }
         LOG.Debug("NotarizeController.CIStatusTx - OUT!");

         return Request.CreateResponse(HttpStatusCode.OK, oCIStatusModelR);
     }
     */

        #endregion TramitesTAG



        #region TestMethods


        // GET: api/Notarize
        [Route("api/Notarize/Ping")]
        [HttpGet]
        public HttpResponseMessage Ping()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Accepted, new string[] { "200", "Servicio respondiendo!" });
            return response;
        }
        // GET: api/Signer
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Signer/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Signer
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Signer/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Signer/5
        public void Delete(int id)
        {
        }

#endregion TestMethods

    }
}
