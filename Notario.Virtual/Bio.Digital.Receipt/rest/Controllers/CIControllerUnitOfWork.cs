﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bio.Digital.Receipt.Core;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.Core.Helpers;
using Bio.Digital.Receipt.libs;
using Bio.Digital.Receipt.pki;
using log4net;

namespace Bio.Digital.Receipt.rest.Controllers
{
    public class CIControllerUnitOfWork
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(CIControllerUnitOfWork));



        internal static int GeneraCertify(RdCITx oRdCITx, string strapp, out RdCITx outRdCITx, out string msgerr)
        {
            int ret = Api.Errors.IRET_OK;
            outRdCITx = null;
            msgerr = null;
            string sXMLTS = null;
            int resTS = 0;
            string sRDEnvelope = null;
            bool isSaveOK = false;
            try
            {
                LOG.Debug("CIControllerUnitOfWork.GeneraCertify IN" +
                    " - Verificación Positiva de RUT = " + oRdCITx.ValueId + " [Score=" + oRdCITx.Score + "]");
                //Genero PDF via DE
                string PdfCertifyB64;
                string PdfDecJuradaB64;
                string QRverify = Core.Services.CIServiceManager.GetQR(Properties.Settings.Default.BASE_CI_CHECK + "?trackid=" + oRdCITx.TrackId);
                string TrackIDE = null;
                string Map = null;
                string msg = null;
                LOG.Debug("CIControllerUnitOfWork.GeneraCertify - Generando Certificado PDF...");
                int rptaDE = DEHelper.GeneraCertifyPDF(oRdCITx, Convert.ToDouble(oRdCITx.Score), Convert.ToDouble(oRdCITx.Threshold),
                                                       QRverify, out msg, out PdfCertifyB64, out PdfDecJuradaB64, 
                                                       out TrackIDE, out Map);

                if (rptaDE < 0)
                {
                    msgerr = "Error generando PDF Certify [Err=" + rptaDE + " - MsgErr=" + msg + "]";
                    LOG.Error("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - " + msgerr);
                    return Api.Errors.IRET_ERR_CI_PDF_GENERATING;
                }
                else
                {
                    LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - Certificado PDF Generado = " + PdfCertifyB64);

                    oRdCITx.Map = Map;
                    DateTime dNow = DateTime.Now;
                    RdExtensionItem[] ext = new RdExtensionItem[25];
                    RdExtensionItem item;

                    int index = 0;
                    item = new RdExtensionItem();
                    item.key = "TrackIdCI";
                    item.value = oRdCITx.TrackId;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "IDCardImage";
                    item.value = oRdCITx.IdCardImageFront;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "IDCardPhotoImage";
                    item.value = oRdCITx.IdCardPhotoImage;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "IDCardSignatureImage";
                    item.value = oRdCITx.IdCardSignatureImage;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "Selfie";
                    item.value = oRdCITx.Selfie;
                    ext[index++] = item;
                    item.key = "FingerSample";
                    item.value = oRdCITx.FingerSample;
                    ext[index++] = item;

                    item = new RdExtensionItem();
                    item.key = "TypeId";
                    item.value = oRdCITx.TypeId;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "ValueId";
                    item.value = oRdCITx.ValueId;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "Name";
                    item.value = (string.IsNullOrEmpty(oRdCITx.Name) ? "" : oRdCITx.Name) + " " +
                                 (string.IsNullOrEmpty(oRdCITx.PhaterLastName) ? "" : oRdCITx.PhaterLastName) + " " +
                                 (string.IsNullOrEmpty(oRdCITx.MotherLastName) ? "" : oRdCITx.MotherLastName);

                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "BirthDate";
                    item.value = oRdCITx.BirthDate;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "IssueDate";
                    item.value = oRdCITx.IssueDate;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "Sex";
                    item.value = (!string.IsNullOrEmpty(oRdCITx.Sex) &&
                                  (oRdCITx.Sex.Equals("M") || oRdCITx.Sex.Equals("F"))) ?
                                  oRdCITx.Sex : "Desconocido";
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "Nacionality";
                    item.value = oRdCITx.Nacionality;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "Serial";
                    item.value = string.IsNullOrEmpty(oRdCITx.Serial) ? "" : oRdCITx.Serial;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "ExpirationDate";
                    item.value = oRdCITx.ExprationDate;
                    ext[index++] = item;

                    item = new RdExtensionItem();
                    item.key = "GeoRef";
                    item.value = string.IsNullOrEmpty(oRdCITx.GeoRef) ? "" : oRdCITx.GeoRef;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "WorkStationID";
                    item.value = oRdCITx.WorkstationId;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "ValidityType";
                    item.value = oRdCITx.ValidityType.ToString();
                    ext[index++] = item;
                    //item = new RdExtensionItem();
                    //item.key = "";
                    //item.value = oXmlIn.CIParam;
                    //ext[index++] = item;

                    item = new RdExtensionItem();
                    item.key = "PDFCICertify";
                    item.value = PdfCertifyB64;
                    ext[index++] = item;
                    //item = new RdExtensionItem();
                    //item.key = "PDFCIDeclaracionJurada";
                    //item.value = PdfDecJuradaB64;
                    //ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "QRCIVerify";
                    item.value = QRverify;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "Score";
                    //double dScore = Convert.ToDouble(oRdCITx.Score) > 100 ? 100 : Convert.ToDouble(oRdCITx.Score);
                        //Si es cedula vieja => Hago porcentaje de NEC sino dejo score enviado
                    double dScore = (!string.IsNullOrEmpty(oRdCITx.Serial) && oRdCITx.Serial.StartsWith("A")) ?
                                    Utils.GetPorcentajeNEC(Convert.ToDouble(oRdCITx.Score), Convert.ToDouble(oRdCITx.Threshold)) :
                                    Convert.ToDouble(oRdCITx.Score); //Convert.ToDouble(score) > 100 ? 100 : score;
                    item.value = dScore.ToString("##.##") + "%";
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "Pais";
                    item.value = "Chile";
                    ext[index++] = item;

                    item = new RdExtensionItem();
                    item.key = "IDCardImageBack";
                    item.value = string.IsNullOrEmpty(oRdCITx.IdCardImageBack) ? "" : oRdCITx.IdCardImageBack;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "ManualSignatureImage";
                    item.value = string.IsNullOrEmpty(oRdCITx.ManualSignatureImage) ? "" : oRdCITx.ManualSignatureImage;
                    ext[index++] = item;
                    item = new RdExtensionItem();
                    item.key = "URLVideo";
                    item.value = string.IsNullOrEmpty(oRdCITx.VideoURL) ? "" : oRdCITx.VideoURL;
                    ext[index++] = item;

                    //item = new RdExtensionItem();
                    //item.key = "IDCardPhotoImageDJ";
                    //item.value = oXmlIn.CIParam.IDCardPhotoImage;
                    //ext[index++] = item;
                    string reciboID = AdministradorRdRecibos.GetTrackIdUnique();
                    string ip = HttpContext.Current.Request.UserHostAddress;
                    if (String.IsNullOrEmpty(ip) || ip.Length < 9)
                        ip = "127.0.0.1";
                    LOG.Debug("CIServiceManager.GenerateCI_w_FotoCedulaVsSelfie - Generando Recibo Digital - ReciboId = " + reciboID);

                    Core.Rd objRD = new Core.Rd(ip, strapp, "CI", dNow, "Certificacion de Identidad", ext, reciboID, 1);

                    //Firmo RD
                    string xmlTosign = Bio.Digital.Receipt.libs.XmlUtils.SerializeObject(objRD);
                    string xmlsigned = null;
                    msgerr = null;
                    LOG.Debug("CIControllerUnitOfWork.GeneraCertify - Firmando Recibo Digital - ReciboId = " + reciboID);
                    int applicationid = AdministradorRdAplicaciones.BuscarRdAplicacionesByCode(oRdCITx.CompanyId, "APPCI");
                    int ierr = XMLSignatureHelper.Firmar(applicationid, reciboID, xmlTosign, out xmlsigned, out msgerr);

                    //Si está habilitado el TSA => Envio
                    if (ierr == 0)
                    {
                        if (Global.TSA_ENABLED)
                        {
                            LOG.Debug("CIControllerUnitOfWork.GeneraCertify - Agregando TS al Recibo Digital - ReciboId = " + reciboID);
                            resTS = Global.TSA_MANAGER.GenerateTS(Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(xmlsigned)), out sXMLTS);
                            if (resTS == 0)
                            {
                                sRDEnvelope = "<RDEnvelope>" + xmlsigned + sXMLTS + "</RDEnvelope>";
                                isSaveOK = true;
                            }
                            else
                            {
                                isSaveOK = false;
                                msgerr = "Error generando TS";
                                LOG.Error("CIControllerUnitOfWork.GeneraCertify - " + msgerr);
                                ret = Api.Errors.IRET_ERR_TSA;
                            }
                        }
                        else
                        {
                            isSaveOK = true;
                            sRDEnvelope = "<RDEnvelope>" + xmlsigned + "</RDEnvelope>";
                        }

                        if (isSaveOK)
                        {
                            try
                            {
                                RdRecibos ord = new RdRecibos();
                                ord.Reciboid = reciboID;
                                ord.Descripcion = "Certificacion de Identidad";
                                ord.Fecha = dNow;
                                ord.Iporigen = HttpContext.Current.Request.UserHostAddress;
                                ord.RdAplicaciones = AdministradorRdAplicaciones.BuscarRdAplicacionesById(applicationid);
                                ord.Reciboxml = sRDEnvelope; // xmlsigned;
                                ord.Version = "1.0";
                                LOG.Debug("CIControllerUnitOfWork.GeneraCertify - Salvando en BD el Recibo Digital - ReciboId = " + reciboID);
                                ord = AdministradorRdRecibos.Save(ord);
                                LOG.Debug("CIControllerUnitOfWork.GeneraCertify - Salvado en BD el Recibo Digital - ReciboId = " + reciboID);
                                //PersistentManager.session().Save(ord);
                                //PersistentManager.session().Flush();

                                //Update RdCITx
                                msg = null;
                                LOG.Debug("CIControllerUnitOfWork.GeneraCertify - Updating rdCITx en BD TrackId = " + oRdCITx.TrackId);

                                oRdCITx.ReceiptId = ord.Id;
                                oRdCITx.CertifyPDF = PdfCertifyB64;

                                //Check mail para que no sea nulo
                                if (string.IsNullOrEmpty(oRdCITx.DestinataryMail))
                                {
                                    oRdCITx.DestinataryMail = "info@notariovirtual.cl";
                                }
                                ret = AdministradorRdCITx.SaveCITx(oRdCITx);
                                outRdCITx = oRdCITx;
                                
                                //Added 20-04-2020 - Return CallBack generico si UrlCallback != null en RdAplicaciones
                                //CheckSendCallback(ord.RdAplicaciones, sRDEnvelope);

                                ret = Api.Errors.IRET_OK;
                            }
                            catch (Exception exDB)
                            {
                                LOG.Error("CIControllerUnitOfWork.GeneraCertify - Error salvando en BD [" + exDB.Message + "]");
                                msgerr = "Error salvando en BD [" + exDB.Message + "]";
                                ret = Api.Errors.IRET_ERR_SAVE_DB;
                            }
                        }
                    }
                    else
                    {
                        msgerr = "Error Firmando [" + msgerr + "]";
                        LOG.Error("CIControllerUnitOfWork.GeneraCertify - " + msgerr);
                        ret = Api.Errors.IRET_ERR_SIGNATURE;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Api.Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("CIControllerUnitOfWork.GeneraCertify - Error: " + ex.Message);
            }
            return ret;
        }
    }
}