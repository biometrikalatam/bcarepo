﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.rest.Models;
using log4net;

namespace Bio.Digital.Receipt.rest.Controllers
{
    public class NotaryPortalUnitOfWork
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NotaryPortalUnitOfWork));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listProcedures"></param>
        /// <param name="flagliviano">0 - Elimina </param>
        /// <returns></returns>        
        internal static List<Procedure> ConvertList(List<NvUsersProcedures> listProcedures, int flagliviano = 0)
        {
            List<Procedure> ret = null;
            NvUsers user;
            string auxusername;
            NvNotaryUsers notaryuser;
            string auxusernotaryname;
            string auxusernotarymail;
            Procedure proc;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.ConvertList - IN...");
                if (listProcedures == null || listProcedures.Count == 0)
                {
                    LOG.Debug("NotaryPortalUnitOfWork.ConvertList - Param null => Sale con null!");
                    return null;
                }
                ret = new List<Procedure>();

                LOG.Debug("NotaryPortalUnitOfWork.ConvertList - Start convert => len = " + listProcedures.Count.ToString());
                foreach (NvUsersProcedures item in listProcedures)
                {
                    auxusernotaryname = null;
                    auxusernotarymail = null;
                    //Tomo datos de users para completar los datos adicionales
                    user = AdministradorNvUsers.BuscarNvUserById(item.IdUser);
                    auxusername = (string.IsNullOrEmpty(user.Name) ? "" : user.Name) + " " +
                                (string.IsNullOrEmpty(user.PatherLastName) ? "" : user.PatherLastName);
                    if (item.UserRevisionNotary > 0) //Si aun no está asignado a alguien, nadie lo reviso aun
                    {
                        notaryuser = AdministradorNvNotaryUsers.BuscarNvNotaryUserById(item.UserRevisionNotary);
                        auxusernotaryname = (string.IsNullOrEmpty(notaryuser.Name) ? "" : notaryuser.Name) + " " +
                                    (string.IsNullOrEmpty(notaryuser.LastName) ? "" : notaryuser.LastName);
                        auxusernotarymail = notaryuser.Mail;
                    }
                    proc = new Procedure(item, auxusername, user.Mail, auxusernotaryname, auxusernotarymail);

                    //Si es liviano => elimino doc que ocupa mucho
                    if (flagliviano == 0)
                    {
                        proc.dbProcedure.DocumentB64 = null;
                        proc.dbProcedure.DocumentB64Notarized = null;
                        proc.dbProcedure.DocumentB64Original = null;
                    }

                    ret.Add(proc);
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error(" Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.ConvertList - OUT! => len = " + ((ret!=null)?ret.Count.ToString():"Null"));
            return ret;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int ModifyRevisionProcedure(ModifyProcedureNotaryModel param, out string msgerr)
        {
            int ret = Errors.IRET_OK;
            msgerr = null;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.ModifyProcedure - IN...");
                NvUsersProcedures proc = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(param.trackid);

                if (proc == null)
                {
                    msgerr = "Tramite inexistente con trackid = " + param.trackid;
                    ret = Errors.IRET_ERR_PROCEDURE_NOTFOUND;
                } else
                {
                    //Si no esta en estado de revision => Salgo informando
                    //if (proc.Status != StatusNvUsersProcedures.STATUS_READY_TO_REVISION_NOTARIZE)
                    //{
                    //    msgerr = "El estado actual del tramite impide su modificacion"
                    //}


                    //Si esta aprobado => save status, texttoinclude,  
                    if (param.status == 1)
                    {
                        LOG.Debug("NotaryPortalUnitOfWork.ModifyProcedure - Save tramite APROBADO con trackid = " + param.trackid);
                        proc.Status = StatusNvUsersProcedures.STATUS_READY_TO_NOTARIZE;
                        proc.StatusRevisionNotary = param.status;
                        proc.TextToInclude = param.texttoinclude;
                    } else if (param.status == 2)
                    {
                        LOG.Debug("NotaryPortalUnitOfWork.ModifyProcedure - Save tramite RECHAZADO con trackid = " + param.trackid);
                        proc.Status = StatusNvUsersProcedures.STATUS_REJECTED;
                        proc.StatusRevisionNotary = param.status;
                        proc.ReasonReject = param.reasonreject;
                    }
                    if (!AdministradorNvUsersProcedures.Update(proc))
                    {
                        ret = Errors.IRET_ERR_NVPROCEDURE_NO_SAVED * -1;
                        msgerr = "Error salvando tramite modificado con trackid = " + param.trackid;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                msgerr = "Error desconocido ModifyProcedure [" + ex.Message + "]";
                LOG.Error("NotaryPortalUnitOfWork.ModifyProcedure - Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.ModifyProcedure - OUT!");
            return ret;
        }
    }
}