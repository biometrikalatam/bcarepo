﻿using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.Core.Services;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;

namespace Bio.Digital.Receipt.rest.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class CIProxyController : ApiController
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(CIProxyController));


        /// <summary>
        /// Registra un resultado de transaccion realizada en estacion remota (MOC) 
        /// </summary>
        /// <remarks>
        /// Los valores enviados como parámetro determian la forma de verificación que se realizará para 
        /// hacer la certificación, a saber:
        /// * company: id informado en el moemnto del alta del servicio (int)
        /// * application: : id informado en el moemnto del alta del servicio (int)
        /// * typeId: tipo de doc a usar para certificar identidad (RUT, PAS, etc)
        /// * valueId: Nro de documento a verificar. Si es RUT formato xxxxxxxx-y
        /// * mailCertify: mail de persona a certificar. Se envia mail para que inice desde alli el proceso de certificación.
        /// * cellNumber: número de celular apra enviar link de procesod e certificacion via wathsapp [Aun no integrado]
        /// * theme: Nombre del tema gráfico a usar, donde se definen colores, logos, etc. Si va nulo se usa default.
        /// * customerTrackId: valor opcional de id unico interno del sistema que crea transaccion. 
        /// * callbackUrl: URL para callback cuando se termine el proceso de certificacion. Sew hará un POST con el trackid de
        /// la transaccion para que el sistema externo recupere los datos que necesite para seguir con su flujo de negocios.
        /// * redirectUrl: Si se desea redirigir a una pagina (Opcional) 
        /// * workflowname: El nombre del workflow que deberá seguir esta transaccion. Inicialmente usaremos solo "default",
        /// luego agregaremos mantenedor de nuevos workflow para aplicar a diversos casos de negocio.
        /// </remarks>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/ciweb/proxy/ciRegisterTx")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK)] //, "CICreateModelR", typeof(CICreateModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(ErrorModel))]
        public object CIRegisterTx([FromBody] CIRegisterModel param) //string mail, int notaryid, int procedureid
        {
            /*
                1) Check Parametro
                2) Consumo mismo verbo en la URL remota
                3) Retorno lo recibido
            */
            int ret = Errors.IRET_OK;
            string msgerr = null;
            CIRegisterModel paramIn;
            IRestResponse response = null;
            CIRegisterModelR oResponse = null;
            try
            {
                LOG.Debug("CIProxyController.CIRegisterTx - IN...");

                //1) Deserializo parámetros y chequeo   
                if (param == null)
                {
                    Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null)));
                }
                paramIn = param;

                if (paramIn == null)
                {
                    LOG.Debug("CIProxyController.CIRegisterTx - Error deserializando parametros de entrada [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                            (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Error deserializando parametros de entrada [Null]", null)));
                }
                else
                {
                    string msgRet = "";
                    if (paramIn.company <= 0 || paramIn.application <= 0) //String.IsNullOrEmpty(paramIn.document))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "company y application deben tener un valor entero > 0" :
                              "|company y aplication deben tener un valor entero > 0");
                    }
                    if (string.IsNullOrEmpty(paramIn.typeId) || string.IsNullOrEmpty(paramIn.valueId))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "TypeId/ValueId no pueden ser nulos" : "|TypeId/ValueId no pueden ser nulos");
                    }
                    if (string.IsNullOrEmpty(paramIn.mailCertify)) // && string.IsNullOrEmpty(paramIn.cellNumber))
                    {
                        //msgRet += (string.IsNullOrEmpty(msgRet) ? "Mail o Celular deben ser informados para enviar al usuario" :
                        //    "|Mail o Celular deben ser informados para enviar al usuario");
                        paramIn.mailCertify = "info@notariovirtual.cl";
                    }
                    //LOG.Debug("CIController.CIRegisterTx - Check listMailsDestinatary en paramIn = " + (paramIn.listMailsDestinatary == null ? "NULL" : "Len =" +
                    //                                               paramIn.listMailsDestinatary.Length));
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        LOG.Debug("CIProxyController.CIRegisterTx - Error Return Mssage = " + "Problemas para registrar CI. [" + msgRet + "]");
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                                (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                    "Problemas para registrar CI. [" + msgRet + "]", null)));
                    }
                }

                LOG.Info("CIProxyController.CIRegisterTx - Llama a servicio remoto...");
                LOG.Debug("BPHelper.TxCreateSigner - LLama a " + Properties.Settings.Default.URLServiceRemoteForProxy + "api/ciweb/ciRegisterTx...");
                var client = new RestClient(Properties.Settings.Default.URLServiceRemoteForProxy + "api/ciweb/ciRegisterTx");
                client.Timeout = Properties.Settings.Default.TimeoutServiceRemoteForProxy;
                var request = new RestRequest(Method.POST);
                client.UserAgent = "biometrika";
                request.AddHeader("Authorization", "Basic " +
                    Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.AccessNameServiceRemoteForProxy + ":" +
                                                                        Properties.Settings.Default.SecretKeyServiceRemoteForProxy)));
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                //CIRegisterModel CIRModel = new CIRegisterModel(Properties.Settings.Default.ServiceCompany,
                //                                                Properties.Settings.Default.ServiceAppId, trackidci,
                //                                                "RUT", inPersona.Rut, 40, (resultMatch ? 100 : 0), null, null,
                //                                                inPersona.Selfie, inPersona.Wsq, inPersona.JpegOriginal);
                string jsonParams = JsonConvert.SerializeObject(paramIn);
                request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                LOG.Debug("BPHelper.TxCreateSigner - call execute...");
                response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    oResponse = JsonConvert.DeserializeObject<CIRegisterModelR>(response.Content);
                    LOG.Debug("CIProxyController.CIRegisterTx - Response OK");
                }
                else
                {
                    LOG.Debug("CIProxyController.CIRegisterTx - Response Error: " + response.StatusCode.ToString());
                    ErrorModel err = JsonConvert.DeserializeObject<ErrorModel>(response.Content);
                    LOG.Debug("CIProxyController.CIRegisterTx - ErrorModel != null => " + (err != null).ToString());
                    msgerr = "Error del servicio: " + response.StatusCode.ToString() + "[" + err.message + "]";
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                                 (new ErrorModel(err.code, err.message, err.detailMessage))); //response;
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("CIProxyController.CIRegisterTx - Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                                (new ErrorModel(ret, "Error reconocido en Proxy", ex.Message))); //response;
            }

            LOG.Debug("CIProxyController.CIRegisterTx OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, oResponse);//response; //Ok(); // Request
        }


        [Route("api/ciweb/proxy/ciVerifyNECTx")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "CIVerifyModelR", typeof(CIVerifyModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(ErrorModel))]
        public object CIVerifyNECTx([FromBody] CIVerifyModel param)
        {
            /*
                 1) Check Parametro
                 2) Consumo mismo verbo en la URL remota
                 3) Retorno lo recibido
             */
            string msgerr = null;
            CIVerifyModel paramIn;
            int resultVerify = 0;
            double scoreOut;
            CIVerifyModelR oCIVerifyModelR = null;
            try
            {
                LOG.Debug("CIProxyController.CIVerifyNECTx - IN...");

                //1) Deserializo parámetros y chequeo   
                if (param == null)
                {
                    Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null)));
                }
                paramIn = param;

                if (paramIn == null)
                {
                    LOG.Debug("CIProxyController.CIVerifyNECTx - Error deserializando parametros de entrada [Null]");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                            (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Error deserializando parametros de entrada [Null]", null)));
                }
                else
                {
                    string msgRet = "";
                    if (paramIn.company <= 0 || paramIn.application <= 0) //String.IsNullOrEmpty(paramIn.document))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "company y application deben tener un valor entero > 0" :
                              "|company y aplication deben tener un valor entero > 0");
                    }
                    if (string.IsNullOrEmpty(paramIn.typeId) || string.IsNullOrEmpty(paramIn.valueId))
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "TypeId/ValueId no pueden ser nulos" : "|TypeId/ValueId no pueden ser nulos");
                    }
                    if (string.IsNullOrEmpty(paramIn.fingerSample)) //string.IsNullOrEmpty(paramIn.pdf417) Lo saco porque puede venir nulo para usar el que ya esta en la BD en RdCITx.Pdf417
                    {
                        msgRet += (string.IsNullOrEmpty(msgRet) ? "PDF417/FingerSample no pueden ser nulos" : "|PDF417/FingerSample no pueden ser nulos");
                    }
                    if (string.IsNullOrEmpty(paramIn.mailCertify)) // && string.IsNullOrEmpty(paramIn.cellNumber))
                    {
                        //msgRet += (string.IsNullOrEmpty(msgRet) ? "Mail o Celular deben ser informados para enviar al usuario" :
                        //    "|Mail o Celular deben ser informados para enviar al usuario");
                        paramIn.mailCertify = "info@notariovirtual.cl";
                    }
                    //LOG.Debug("CIController.CIRegisterTx - Check listMailsDestinatary en paramIn = " + (paramIn.listMailsDestinatary == null ? "NULL" : "Len =" +
                    //                                               paramIn.listMailsDestinatary.Length));
                    if (!string.IsNullOrEmpty(msgRet))
                    {
                        LOG.Debug("CIProxyController.CIVerifyNECTx - Error Return Mssage = " + "Problemas para verificar NEC. [" + msgRet + "]");
                        return Request.CreateResponse(HttpStatusCode.BadRequest,
                                (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS,
                                    "Problemas para verificar NEC en CI. [" + msgRet + "]", null)));
                    }
                }

                //3-Chequeo remoto
                LOG.Debug("NVHelper.VerifyByNec - LLama a " + Properties.Settings.Default.URLServiceRemoteForProxy + "api/ciweb/ciVerifyNECTx...");
                var client = new RestClient(Properties.Settings.Default.URLServiceRemoteForProxy + "api/ciweb/ciVerifyNECTx");
                client.Timeout = Properties.Settings.Default.TimeoutServiceRemoteForProxy;
                var request = new RestRequest(Method.POST);
                client.UserAgent = "biometrika";
                request.AddHeader("Authorization", "Basic " +
                    Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.AccessNameServiceRemoteForProxy + ":" +
                                                                        Properties.Settings.Default.SecretKeyServiceRemoteForProxy)));
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                //CIVerifyModel CIVModel = new CIVerifyModel(Properties.Settings.Default.ServiceCompany,
                //                                               Properties.Settings.Default.ServiceAppId, trackidci,
                //                                               "RUT", inPersona.Rut, Properties.Settings.Default.NECThreshold,
                //                                               (resultMatch ? 100 : 0), null, null,
                //                                               inPersona.Selfie, fullPdf417, inPersona.Wsq, inPersona.JpegOriginal);
                string jsonParams = JsonConvert.SerializeObject(paramIn);
                request.AddParameter("application/json", jsonParams, ParameterType.RequestBody); // "{\n\t\"userReference\": \"biometrika\",\n\t\"customerInternalReference\": \"bk1\",\n\t\"presets\": [{\n      \"index\": 1,\n      \"country\": \"CHL\",\n      \"type\": \"ID_CARD\"\n    }],\n    \"locale\": \"es\",\n    \"reportingCriteria\": \"my-criteria\",\n    \"workflowId\": 201\n}", ParameterType.RequestBody);
                LOG.Debug("CIProxyController.CIVerifyNECTx - call execute...");
                IRestResponse response = client.Execute(request);
                LOG.Debug("CIProxyController.CIVerifyNECTx - OUT => response = " + (response != null ? response.StatusCode.ToString() : "Null"));
                //return response;
                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    LOG.Debug("CIProxyController.CIVerifyNECTx - Response OK");
                    oCIVerifyModelR = JsonConvert.DeserializeObject<CIVerifyModelR>(response.Content);
                    //if (oCIVerifyModelR != null)
                    //{
                    //    resultMatch = (oCIVerifyModelR.resultVerify == 1);
                    //    score = oCIVerifyModelR.score;
                    //    LOG.Debug("NVHelper.VerifyByNec - Result = " + oCIVerifyModelR.resultVerify.ToString() + "[Score=" +
                    //        oCIVerifyModelR.score.ToString() + "/" + oCIVerifyModelR.threshold);
                    //}
                    //else
                    //{
                    //    LOG.Debug("NVHelper.VerifyByNec - Error parseando retorno...CIVerifyModelR==null");
                    //    ret = Errors.IRET_ERR_DESERIALIZE_RESPONSE;
                    //}
                }
                else
                {
                    LOG.Debug("NVHelper.VerifyByNec - Response Error");
                    ErrorModel err = JsonConvert.DeserializeObject<ErrorModel>(response.Content);
                    LOG.Debug("NVHelper.VerifyByNec - ErrorModel != null => " + (err != null).ToString());
                    msgerr = "Error del servicio: " + response.StatusCode.ToString() + "[" + err.message + "]";
                    //ret = Errors.IRET_ERR_CI_NO_COMPLETE;
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                                (new ErrorModel(err.code, err.message, msgerr))); //response;
                }

            }
            catch (Exception ex)
            {
                LOG.Error("CIProxyController.CIVerifyNECTx - Error Verificnado NEC CI: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    (new ErrorModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Proxy Error Desconcido Verificando NEC CI ["
                                                  + ex.Message + "]", null)));
            }

            //LOG.Debug("CIController.CIVerifyNECTx OUT!");
            ////8 - Retorno OK
            return Request.CreateResponse(HttpStatusCode.OK, oCIVerifyModelR);
                    //(new CIVerifyModelR(paramIn.trackId, "RUT", paramIn.valueId, paramIn.threshold, scoreOut, resultVerify)));
            //; // Request.CreateResponse(HttpStatusCode.OK, response);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trackidci"></param>
        /// <param name="mail"></param>
        /// <param name="format">pdf|xml</param>
        /// <returns></returns>
        [Route("api/ciweb/proxy/ciDownloadCI")]
        [HttpGet]
        public object DownloadCI(string trackidci, string format)
        {
            string msgerr = "";
            try
            {
                LOG.Debug("CIProxyController.DownloadCI - IN...");
                //1) Deserializo parámetros     
                if (string.IsNullOrEmpty(trackidci))
                {
                    LOG.Debug("CIProxyController.DownloadCI - trackid nulo! => Retorno");
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new NotarizeResponseModel(Errors.IRET_API_BAD_REQUEST,
                        "TrackId consultado no puede ser nulo", trackidci)));
                }

                //2) Recupero tramite y si está statis = Notarzed => Devuelvo sino msg error
                RdCITx tx = AdministradorRdCITx.GetCITxByTrackId(trackidci, out msgerr);

                if (tx == null)
                {
                    LOG.Error("CIController.DownloadCI Error: TrackId consultado no existe");
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        (new NotarizeResponseModel(Errors.IRET_API_ERR_NOT_FOUND,
                        "TrackId consultado no existe", trackidci)));
                }
                else if (string.IsNullOrEmpty(tx.CertifyPDF))
                {
                    LOG.Error("CIController.DownloadCI Error: TrackId consultado con PDF nulo [" + trackidci + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                        (new NotarizeResponseModel(Errors.IRET_ERR_PROCEDURE_PENDING,
                        "TrackId consultado con PDF nulo", trackidci)));
                }
                else  //Todo ok => Armo salida 
                {
                    ////Debe verificar si el QR sigue siendo valido apra verificar sino regenerarlo 
                    ////  - iniciar nuevo proceso CI
                    ////  - Update en BD
                    //CheckValidityQR(nvUPE, out nvUPE);

                    //string sfile = System.IO.File.ReadAllText(@"C:\tmp\doc.base64.txt");
                    LOG.Debug("CIController.DownloadCI - Descargando " + trackidci + ".pdf");
                    byte[] byFile = Convert.FromBase64String(tx.CertifyPDF);
                    MemoryStream ms = new MemoryStream(byFile);
                    //var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/test.docx"); ;
                    HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                    //var stream =    new FileStream(path, FileMode.Open);
                    result.Content = new StreamContent(ms); // (stream);
                    //result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    //result.Content.Headers.ContentDisposition.FileName = nvUP.TrackId + ".pdf"; // Path.GetFileName(path);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.ContentLength = ms.Length;// stream.Length;
                    LOG.Debug("CIController.DownloadCI - Descarga OK!");
                    return result;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("CIController.DownloadCI Error: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                       (new NotarizeResponseModel(Errors.IRET_API_INTERNAL_SERVER_ERROR,
                       "Error descargadno CI PDF [" + ex.Message + "]", trackidci)));
            }
        }

        [Route("api/ciweb/proxy/ciInformPDFbyMail")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Error", typeof(ErrorModel))]
        public object CIResendTx(string trackid, string mail) //string mail, int notaryid, int procedureid
        {
            /*
                1) Check Parametro 
                2) Check Acceso/Habilitacion
                3) Recupero Tx
                4) Retorno ok
            */
            CIResendModel paramIn;
            string msgRet = "";
            string warning = null;
            try
            {
                LOG.Debug("CIProxyController.ciInformPDFbyMail - IN...");

                //0) Check Basic Authentication
                //if (!Authorized(Request, out msgRet))
                //{
                //    LOG.Warn("NotarizeController.TxCreate - No Autorizado!");
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                //        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                //                        (string.IsNullOrEmpty(msgRet) ? "" : msgRet) + "]", null)));
                //}

                //1) Deserializo parámetros y chequeo   
                if (string.IsNullOrEmpty(trackid) || string.IsNullOrEmpty(mail))
                {
                    LOG.Error("CIProxyController.ciInformPDFbyMail - Sale por parametros nulos...");
                    Request.CreateResponse(HttpStatusCode.BadRequest,
                        (new ErrorModel(Errors.IRET_API_BAD_REQUEST, "Parametros nulos Complete y reintente...", null)));
                }

                //2) Chequeo de acceso y habilitacion
                //LOG.Debug("NotarizeController.CIResendTx - Control acceso companyid = " + paramIn.company);
                //int qext;
                //string strapp, msg;
                //int retaux = CIServiceManager.CheckAppAndCompanyEnabled(paramIn.company, out qext, out strapp, out msg);
                //if (retaux < 0)
                //{
                //    LOG.Error("NotarizeController.CIResendTx - Check Acceso incorrecto para company = " + paramIn.company + 
                //        "[" + (string.IsNullOrEmpty(msg)?"":msg) + "]");
                //    return Request.CreateResponse(HttpStatusCode.Conflict,
                //        (new ErrorModel(Errors.IRET_API_CONFLIC, "Check Acceso incorrecto para company = " + paramIn.company + 
                //        "[" + (string.IsNullOrEmpty(msg) ? "" : msg) + "]", paramIn.trackid)));
                //}

                LOG.Debug("CIProxyController.ciInformPDFbyMail - LLama a " + Properties.Settings.Default.URLServiceRemoteForProxy + 
                          "api/ciweb/ciInformPDFbyMail...");
                var client = new RestClient(Properties.Settings.Default.URLServiceRemoteForProxy + "api/ciweb/ciInformPDFbyMail?trackid=" +
                                            trackid + "&mail=" + mail);
                client.Timeout = Properties.Settings.Default.TimeoutServiceRemoteForProxy;
                var request = new RestRequest(Method.POST);
                client.UserAgent = "biometrika";
                request.AddHeader("Authorization", "Basic " +
                    Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(Properties.Settings.Default.AccessNameServiceRemoteForProxy + ":" +
                                                                        Properties.Settings.Default.SecretKeyServiceRemoteForProxy)));

                LOG.Debug("CIProxyController.ciInformPDFbyMail - call execute...");
                IRestResponse response = client.Execute(request);
                LOG.Debug("CIProxyController.ciInformPDFbyMail - OUT => response = " + (response != null? response.StatusCode.ToString():"Null"));
                return Ok();
            }
            catch (Exception ex)
            {
                LOG.Error("CIProxyController.ciInformPDFbyMail Error reenviando link para CI : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                    (new ErrorModel(Errors.IRET_API_INTERNAL_SERVER_ERROR, "Error reenviando link para CI ["
                                                  + ex.Message + "]", null)));
            }
                //LOG.Debug("CIController.ciInformPDFbyMail - Out!");
                ////8 - Retorno OK
                //return Ok(); // new CICreateModelR((string.IsNullOrEmpty(warning) ? null : warning), BPResponse.verifyUrl)); // Request.CreateResponse(HttpStatusCode.OK, response);
        }

        
    }
}
