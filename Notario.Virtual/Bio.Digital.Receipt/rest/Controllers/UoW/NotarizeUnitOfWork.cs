﻿using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.Core.Database.Notary.config;
using Bio.Digital.Receipt.Core.Helpers;
using Bio.Digital.Receipt.Core.Services;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.rest.Controllers
{
    public class NotarizeUnitOfWork
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NotarizeUnitOfWork));

        internal static int GetStatusCIWeb(string trackid, out TxSignerModelR oTxSignerModelR)
        {
            RdCITx tx;
            string msgerr;
            oTxSignerModelR = null;
            int ret = -1;
            try
            {
                LOG.Debug("NotarizeUnitOfWork.GetStatusCIWeb - Recuperandpo Tx = " + trackid + " desde GetCITxByTrackId...");
                tx = AdministradorRdCITx.GetCITxByTrackId(trackid, out msgerr);
                if (tx == null)
                {
                    LOG.Debug("NotarizeUnitOfWork.GetStatusCIWeb - Retorno desde GetCITxByTrackId nulo => Sale!");
                    return Errors.IRET_ERR_CI_NO_IN_DB;
                }

                LOG.Debug("NotarizeUnitOfWork.GetStatusCIWeb - Ingresando a UpdateRdCIWebTx...");
                ret = NotarizeUnitOfWork.UpdateRdCIWebTx(tx, out msgerr, out tx);
                if (ret < 0)
                {
                    LOG.Warn("NotarizeUnitOfWork.GetStatusCIWeb - Error en UpdateRdCIWebTx ret = " + ret + "[" +
                             (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr) + "]");
                    return ret;
                }

                //LLeno salida
                LOG.Debug("NotarizeUnitOfWork.GetStatusCIWeb - Creando salida...");
                msgerr = null;
                ret = NotarizeUnitOfWork.GeneraResponseGet(tx, 2, out oTxSignerModelR, out msgerr);
                if (ret < 0)
                {
                    LOG.Debug("NotarizeUnitOfWork.GetStatusCIWeb - Retorno desde GeneraResponseGet nulo => Sale!");
                    return Errors.IRET_ERR_CI_NO_IN_DB;
                } else if (oTxSignerModelR!=null && !oTxSignerModelR.status.Equals("DONE"))
                {
                    ret = 2;
                } else 
                {
                    ret = 1;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeUnitOfWork.GetStatusCIWeb Error: " + ex.Message);
                return Errors.IRET_ERR_DESCONOCIDO;
            }
            return ret;
        }

        internal static int GeneraResponseGet(RdCITx tx, int flag, out TxSignerModelR oTxSignerModelR, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            oTxSignerModelR = null;
            try
            {
                oTxSignerModelR = new TxSignerModelR();
                LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - IN...");
                //PRimero chequeo status => Si no esta DONE => retorno status
                if (!CIServiceManager.GetStatus(tx).Equals("DONE"))
                {
                    LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - No es Status = DONE => Sale...");
                    oTxSignerModelR.code = 0;
                    oTxSignerModelR.msgerr = null;
                    oTxSignerModelR.trackid = tx.TrackId;
                    oTxSignerModelR.status = CIServiceManager.GetStatus(tx);
                    oTxSignerModelR.statusdescription = (tx != null ? CIServiceManager.GetStatusDescription(tx.ActionId) : null);
                    LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Status = " + oTxSignerModelR.status);
                }
                else //Si es DONE => lleno la info de acuerdo al flag
                {
                    LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Lleno salida....");
                    oTxSignerModelR.code = 0;
                    oTxSignerModelR.msgerr = null;
                    oTxSignerModelR.trackid = tx.TrackId;
                    oTxSignerModelR.status = CIServiceManager.GetStatus(tx);
                    oTxSignerModelR.statusdescription = (tx != null ? CIServiceManager.GetStatusDescription(tx.ActionId) : null);
                    oTxSignerModelR.receiptId = tx.ReceiptId;
                    oTxSignerModelR.type = tx.Type; //0-CI | 1-Document TTag u otros
                    oTxSignerModelR.actionId = tx.ActionId;
                    oTxSignerModelR.dateGen = tx.DateGen;
                    oTxSignerModelR.lastModify = tx.LastModify;
                    oTxSignerModelR.resultCode = tx.ResultCode;
                    oTxSignerModelR.typeId = tx.TypeId;
                    oTxSignerModelR.valueId = tx.ValueId;
                    oTxSignerModelR.taxidcompany = tx.TaxIdCompany; //GetTaxIdCompany(tx.Form);
                    oTxSignerModelR.workstationid = tx.WorkstationId;
                    oTxSignerModelR.georef = tx.GeoRef;
                    oTxSignerModelR.score = tx.Score;
                    oTxSignerModelR.threshold = tx.Threshold;

                    if (flag > 0) //Adjunta datos personales y del form
                    {
                        LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Cargo datos personales..:");
                        if (string.IsNullOrEmpty(tx.Form))
                        {
                            oTxSignerModelR.formdata = new Dictionary<string, string>();
                        }
                        else
                        {
                            oTxSignerModelR.formdata = JsonConvert.DeserializeObject<Dictionary<string, string>>(tx.Form);
                        }
                        oTxSignerModelR.formdata.Add("rut", tx.ValueId);
                        oTxSignerModelR.formdata.Add("nombre", tx.Name + (string.IsNullOrEmpty(tx.Name) ? "" : tx.Name) + " " +
                             (string.IsNullOrEmpty(tx.PhaterLastName) ? "" : tx.PhaterLastName) + " " +
                             (string.IsNullOrEmpty(tx.MotherLastName) ? "" : tx.MotherLastName));
                        oTxSignerModelR.formdata.Add("serialcedula", tx.Serial);
                        oTxSignerModelR.formdata.Add("birthdate", tx.BirthDate);
                        oTxSignerModelR.formdata.Add("sex", tx.Sex);
                        oTxSignerModelR.formdata.Add("mail", tx.DestinataryMail);
                    }

                    if (flag > 1) //Adjunta imagenes
                    {
                        LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Cargo imagenes...");
                        LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Cargo Poder...");
                        oTxSignerModelR.document = tx.CertifyPDF;
                        ImageSample oSample;
                        oTxSignerModelR.Samples = new List<ImageSample>();
                        LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Cargo imagenes cedulas...");
                        oTxSignerModelR.Samples.Add(new ImageSample("idcardimagefront", TypeImage.cedulafront, TypeImageFormat.jpg, tx.IdCardImageFront));
                        oTxSignerModelR.Samples.Add(new ImageSample("idcardimageback", TypeImage.cedulaback, TypeImageFormat.jpg, tx.IdCardImageBack));
                        LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Cargo selfie y manualsignature...");
                        oTxSignerModelR.Samples.Add(new ImageSample("selfie", TypeImage.selfie, TypeImageFormat.jpg, tx.Selfie));
                        oTxSignerModelR.Samples.Add(new ImageSample("manualsignatureimage", TypeImage.manualsignature, TypeImageFormat.jpg, tx.ManualSignatureImage));
                        
                        if (!string.IsNullOrEmpty(tx.CarRegisterImageFront))
                        {
                            LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Cargo padron front...");
                            oTxSignerModelR.Samples.Add(new ImageSample("carregisterimagefront", TypeImage.image, TypeImageFormat.jpg, tx.CarRegisterImageFront));
                        }

                        if (!string.IsNullOrEmpty(tx.CarRegisterImageBack))
                        {
                            LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Cargo padron back...");
                            oTxSignerModelR.Samples.Add(new ImageSample("carregisterimageback", TypeImage.image, TypeImageFormat.jpg, tx.CarRegisterImageBack));
                        }

                        if (!string.IsNullOrEmpty(tx.VideoURL))
                        {
                            LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Cargo Video. Len = " + tx.VideoURL + "...");
                            oTxSignerModelR.Samples.Add(new ImageSample("video", TypeImage.video, TypeImageFormat.videourl, tx.VideoURL));
                        }

                        if (!string.IsNullOrEmpty(tx.WritingImage))
                        {
                            LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Cargo escritura. Len = " + tx.WritingImage.Length + "...");
                            LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Escritura = " + tx.WritingImage.Substring(0, 10) + "...");
                            //Chequeo si tiene formato de URLData "[\"data:application/pdf;base64,JVBERi0x...
                            string mimetype = null;
                            string datab64 = null;
                            if (tx.WritingImage.StartsWith("[\"data:"))
                            {
                                mimetype = libs.Utils.GetMimeTypeFromURLData(tx.WritingImage, out datab64);
                            }
                            else
                            {
                                mimetype = libs.Utils.GetMimeType(Convert.FromBase64String(tx.WritingImage));
                                datab64 = tx.WritingImage;
                            }


                            if (mimetype.Equals("PDF"))
                            {
                                LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Es PDF...");
                                oTxSignerModelR.Samples.Add(new ImageSample("writingimage", TypeImage.pdf, TypeImageFormat.pdf, datab64));
                            }
                            else
                            {
                                LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Es imagen JPG...");
                                TypeImageFormat tyf = TypeImageFormat.jpg;
                                if (mimetype.Equals("image/png"))
                                {
                                    LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Es imagen PNG...");
                                    tyf = TypeImageFormat.png;
                                }
                                else if (mimetype.Equals("image/bmp"))
                                {
                                    LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - Es imagen BMP...");
                                    tyf = TypeImageFormat.bmp;
                                }
                                oTxSignerModelR.Samples.Add(new ImageSample("writingimage", TypeImage.image, tyf, datab64));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                oTxSignerModelR.code = -1;
                oTxSignerModelR.msgerr = "NotarizeUnitOfWork.GeneraResponseGet Error: " + ex.Message;
                msgerr = oTxSignerModelR.msgerr;
                oTxSignerModelR.trackid = tx.TrackId;
                oTxSignerModelR.status = CIServiceManager.GetStatus(tx);
                oTxSignerModelR.statusdescription = (tx != null ? CIServiceManager.GetStatusDescription(tx.ActionId) : null);
                LOG.Error("NotarizeUnitOfWork.GeneraResponseGet Error: " + ex.Message);
            }
            LOG.Debug("NotarizeUnitOfWork.GeneraResponseGet - OUT! ret = " + ret);
            return ret;
        }

        internal static int UpdateRdCIWebTx(RdCITx tx, out string msgerr, out RdCITx txupdated)
        {
            /*
                1.- Check Null y status que sea pendiente aun
                2.- Recupero desde BP status 
                3.- Si termino proceso en BP => Termino proceso pedido
                4.- Si debo informar via callback informo
                5.- Retorno
            */
            int ret = 0;
            msgerr = null;
            txupdated = null;
            try
            {
                LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx IN...");
                //1.- Check Null y status que sea pendiente aun
                if (CIServiceManager.GetStatus(tx).Equals("DONE"))
                {
                    LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - Sale proque es DONE!");
                    ret = Errors.IRET_API_ERR_CI_COMPLETED;
                    txupdated = tx;
                    return ret;
                }

                if (CIServiceManager.GetStatus(tx).StartsWith("EXPIRED"))
                {
                    LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - Sale proque es EXPIRED!");
                    tx.ActionId = -10; //Expirada
                    AdministradorRdCITx.UpdateCITx(tx, out msgerr);
                    ret = Errors.IRET_API_ERR_CI_EXPIRED;
                    txupdated = tx;
                    return ret;
                }
                //2.- Recupero desde BP status
                BpTxDataResponse oBpTxDataResponse;
                LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - Pide status a BP con TrackId = " + tx.TrackIdBP);
                ret = BPHelper.TxStatus(tx.TrackIdBP, out msgerr, out oBpTxDataResponse);
                if (ret < 0)
                {
                    LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - Retorno de pedido a BP => " + ret + " => Sale!");
                    return ret;
                }

                //3.- Si termino proceso en BP => Genero CI 
                if (oBpTxDataResponse == null)
                {
                    LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - oBpTxDataResponse NULO...");
                    txupdated = tx; //Significa que no hubo error pero esta en pending aun en BPWeb => Inofrmo lo que está en tx
                }
                else  //Si llego aca es que esta DONE el proceso de verify en BP => Genero CI
                {
                    string xmlparamout = null;
                    XmlParamIn oXmlParamIn = new XmlParamIn();

                    tx = FillTxFromBPResponse(tx, oBpTxDataResponse);

                    int _resultVerify = oBpTxDataResponse.result;
                    double _score = oBpTxDataResponse.score;
                    LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - Procesa respuesta desde BP => score = " + _score.ToString() +
                        " - resultverify = " + _resultVerify);
                    if (_resultVerify == 1) // || _resultVerify == 2)
                    {
                        LOG.Debug("NotarizeController.UpdateRdCIWebTx - Result = 1 => Genera CI...");
                        if (oBpTxDataResponse.onboardingmandatory == 0) //Verify Cedula vs Selfie
                        {
                            oXmlParamIn.Actionid = 19;
                        }
                        else //Verify Selfie vs BD
                        {
                            oXmlParamIn.Actionid = 18;
                        }
                        oXmlParamIn.Companyid = tx.CompanyId;
                        oXmlParamIn.Origin = 1;
                        oXmlParamIn.ApplicationId =
                            AdministradorRdAplicaciones.BuscarRdAplicacionesByCode(tx.CompanyId, Properties.Settings.Default.CICode);
                        LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - ApplicationId recuperado con Code = " +
                                    Properties.Settings.Default.SignerCode + " => " + oXmlParamIn.ApplicationId);
                        oXmlParamIn.CIParam = new CIParam();
                        oXmlParamIn.CIParam.BirthDate = oBpTxDataResponse.personaldata.birthdate; // tx.BirthDate;
                        oXmlParamIn.CIParam.DestinaryMail = tx.DestinataryMail;
                        oXmlParamIn.CIParam.ExpirationDate = oBpTxDataResponse.personaldata.documentexpirationdate; // tx.ExprationDate;
                        oXmlParamIn.CIParam.GeoRef = tx.GeoRef; //Ver este valor si viene o sale de aca
                        oXmlParamIn.CIParam.IDCardImageBack = BPHelper.GetSample(oBpTxDataResponse, 1); //tx.IdCardImageBack;
                        oXmlParamIn.CIParam.IDCardImageFront = BPHelper.GetSample(oBpTxDataResponse, 0);  //tx.IdCardImageFront;
                        oXmlParamIn.CIParam.IDCardPhotoImage = BPHelper.GetSample(oBpTxDataResponse, 4); //tx.IdCardPhotoImage;
                        oXmlParamIn.CIParam.IDCardSignatureImage = BPHelper.GetSample(oBpTxDataResponse, 3);
                        ; // tx.IdCardSignatureImage;
                        oXmlParamIn.CIParam.IssueDate = null; // tx.IssueDate;
                        oXmlParamIn.CIParam.ManualSignatureImage = BPHelper.GetSample(oBpTxDataResponse, 5); //tx.ManualSignatureImage;
                        oXmlParamIn.CIParam.MotherLastName = oBpTxDataResponse.personaldata.motherlastname; // tx.MotherLastName;
                        oXmlParamIn.CIParam.Nacionality = oBpTxDataResponse.personaldata.nationality; // tx.Nacionality;
                        oXmlParamIn.CIParam.Name = oBpTxDataResponse.personaldata.name; // tx.Name;
                        oXmlParamIn.CIParam.PhaterLastName = oBpTxDataResponse.personaldata.patherlastname; // tx.PhaterLastName;
                        oXmlParamIn.CIParam.Selfie = BPHelper.GetSample(oBpTxDataResponse, 2); //tx.Selfie;
                        oXmlParamIn.CIParam.Score = oBpTxDataResponse.score.ToString(); // tx.Score;
                        oXmlParamIn.CIParam.Serial = oBpTxDataResponse.personaldata.documentseriesnumber; // tx.Serial;
                        oXmlParamIn.CIParam.Sex = oBpTxDataResponse.personaldata.sex; // tx.Sex;
                        oXmlParamIn.CIParam.Threshold = oBpTxDataResponse.threshold.ToString(); // tx.Threshold;
                        oXmlParamIn.CIParam.TrackId = tx.TrackId;
                        oXmlParamIn.CIParam.TypeId = oBpTxDataResponse.personaldata.typeid; // tx.TypeId;
                        oXmlParamIn.CIParam.ValidityType = (int)oBpTxDataResponse.typeverify; // tx.ValidityType;
                        oXmlParamIn.CIParam.ValueId = oBpTxDataResponse.personaldata.valueid; // tx.ValueId;
                        oXmlParamIn.CIParam.WorkStationID = oBpTxDataResponse.enduser;
                        oXmlParamIn.CIParam.GeoRef = oBpTxDataResponse.georef;
                        oXmlParamIn.CIParam.URLVideo = oBpTxDataResponse.videourl;

                        LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - CIServiceManager.GenerateNv2Tx_From_BPWeb ingresando...");
                        ret = CIServiceManager.GenerateCI_From_BPWeb(tx, oXmlParamIn, _resultVerify, _score, false, out xmlparamout);
                        LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - CIServiceManager.GenerateNv2Tx_From_BPWeb  ret 0 " + ret);
                        if (ret == 0) //Si actualizo ok => Mando mail a quien se pidio
                        {
                            LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - CIServiceManager.GenerateNv2Tx_From_BPWeb  sending mails a destinatarios...");
                            ret = NotarizeController.SendCIToDestinataries(tx.TrackId);
                            LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - CIServiceManager.GenerateNv2Tx_From_BPWeb ret mailing = " + ret);
                        }
                    }

                    //Comento porque no me importa ahora si es positivo o negativo, solo se informa en el poder pero no es bloqueante
                    if (_resultVerify == 2)
                    {
                        LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - Updating Verify Negative...");
                        tx.ActionId = -11; //Verificacion Negativa
                        tx.Score = _score.ToString();
                        AdministradorRdCITx.UpdateCITx(tx, out msgerr);
                        LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - Termino Update Negative. MsgErr => " +
                                    (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr));
                        LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - CIServiceManager.GenerateCI_From_BPWeb sending mails a destinatarios avisando error...");
                        ret = NotarizeController.SendCIToDestinatariesXError(tx.TrackId);
                        LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - CIServiceManager.GenerateCI_From_BPWeb ret mailing = " + ret);
                    }

                    if (_resultVerify == 0)
                    {
                        LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - _resultVerify = 0 - operationcode = " + oBpTxDataResponse.operationcode);
                        if (oBpTxDataResponse.operationcode < 0)
                        {
                            tx.ActionId = oBpTxDataResponse.operationcode;
                            tx.Score = _score.ToString();
                            AdministradorRdCITx.UpdateCITx(tx, out msgerr);
                            LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - Termino Update Error [" + oBpTxDataResponse.operationcode
                                        + "]. MsgErr => " + (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr));
                            LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - CIServiceManager.GenerateCI_From_BPWeb sending mails a destinatarios avisando error...");
                            ret = NotarizeController.SendCIToDestinatariesXError(tx.TrackId);
                            LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - CIServiceManager.GenerateCI_From_BPWeb ret mailing = " + ret);
                        }
                    }
                    LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - Recupero tx updated con receiptid => " + (tx != null ? tx.ReceiptId.ToString() : "Null"));
                    txupdated = AdministradorRdCITx.GetCITxByRdId(tx.ReceiptId, out msgerr);
                    LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx - txupdated != null => " + (txupdated != null).ToString());
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NotarizeUnitOfWork.UpdateRdCIWebTx Error: " + ex.Message);
            }
            LOG.Debug("NotarizeUnitOfWork.UpdateRdCIWebTx OUT!");
            return ret;
        }

        private static RdCITx FillTxFromBPResponse(RdCITx tx, BpTxDataResponse oBpTxDataResponse)
        {
            try
            {
                LOG.Debug("NotarizeUnitOfWork.FillTxFromBPResponse IN...");
                foreach (ImageSample item in oBpTxDataResponse.Samples)
                {
                    if (item.code.Equals("video"))
                        tx.VideoURL = item.data;

                    if (item.code.Equals("carregisterback"))
                        tx.CarRegisterImageBack = item.data;

                    if (item.code.Equals("writing"))
                        tx.WritingImage = item.data;

                    if (item.code.Equals("form"))
                        tx.Form = item.data;

                    if (item.code.Equals("manualsignature"))
                        tx.ManualSignatureImage = item.data;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotarizeUnitOfWork.FillTxFromBPResponse Error: " + ex.Message);
            }
            LOG.Debug("NotarizeUnitOfWork.FillTxFromBPResponse IN...");
            return tx;
        }

        internal static int UpdateNvUserProcedure(RevisionCompletedModel paramIn, out string msgerr)
        {
            int ret = -1;
            msgerr = null;
            try
            {
                LOG.Debug("NotarizeUnitOfWork.UpdateNvUserProcedure IN...");
                //1-Aprobado | 2-Rechazado
                if ((paramIn.status == 1 && string.IsNullOrEmpty(paramIn.texttoinclude)) ||
                    (paramIn.status == 2 && string.IsNullOrEmpty(paramIn.reasonrejection))) {
                    msgerr = "El status informado es " + paramIn.status.ToString() + " pero la infomracion adicional necesaria es nula.";
                    LOG.Debug("NotarizeUnitOfWork.UpdateNvUserProcedure - Sale por conflicto [-409] => Status = " + paramIn.status +
                        " [texttoinclude = " + (string.IsNullOrEmpty(paramIn.texttoinclude) ? "Null" : "Len=" + paramIn.texttoinclude.Length) + " | " +
                        " [reasonrejection = " + (string.IsNullOrEmpty(paramIn.reasonrejection) ? "Null" : "Len=" + paramIn.reasonrejection.Length) + "]");
                    return Errors.IRET_API_CONFLIC * -1;

                }

                LOG.Debug("NotarizeUnitOfWork.UpdateNvUserProcedure - recupero tramite con trackid = " + paramIn.trackid);
                NvUsersProcedures nvup = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(paramIn.trackid, out msgerr);

                if (nvup == null)
                {
                    msgerr = "Error recuperando tramite con trackid = " + paramIn.trackid + ". Reintente mas tarde o consulte con soporte...";
                    LOG.Debug("NotarizeUnitOfWork.UpdateNvUserProcedure - Sale por NotFound [-404] el TrackId");
                    return Errors.IRET_ERR_PROCEDURE_NOTFOUND;
                }

                //Actualizo la BD con datos recibidos, y genero documento final a enviar al notario
                //1. Genero doc final
                string docToMerge;
                ret = DEHelper.GeneraDocFinalToNotarize(nvup, paramIn, out docToMerge, out msgerr);

                if (ret < 0 || string.IsNullOrEmpty(docToMerge))
                {
                    LOG.Error("NotarizeController.RevisionCompleted - Error actualizando procedure para notarizar - ret = " + 
                                ret + " [" + msgerr + "]");
                    return Errors.IRET_ERR_NVPROCEDURE_NO_SAVED;
                }

                //4) De acuerdo a lo seleccionado se envia a notarizar al notario seleccionado por el provider adecuado
                string _UrlBase = AdministradorNvConfig.GetUrlBase(nvup.IdProvider); //Hacer primero HashTable de providers para acceder rapido
                //List<Evidence> evidences = AdministradorNvConfig.GenerateListEvidences(paramIn.evidences);
                string[] listFiles = new string[2];
                listFiles[0] = nvup.DocumentB64Original;
                listFiles[1] = docToMerge; // ExtractCIPdf(evidences[0]);
                string _DocumentWithEvidences = DEHelper.MergeFiles(listFiles);
                NotarizeProviderResponsetModel response = NvProviderHelper.Notarize(_UrlBase, paramIn.trackid, nvup.IdProcedure,
                                                                        _DocumentWithEvidences, null); //Ver si se necesita mandar las evidencias por separado

                //5) Se procesa respuesta
                //   5.1) Actualiza NvUsersProcedures con status recibido
                if (response != null && response.code == 200)
                {
                    //Actualizo la info en BD
                    nvup.DocumentB64 = _DocumentWithEvidences;
                    nvup.Status = StatusNvUsersProcedures.STATUS_IN_PROCESS_NOTARIZING;
                    nvup.StatusRevisionNotary = paramIn.status;
                    nvup.TextToInclude = paramIn.texttoinclude;
                    nvup.ReasonReject = paramIn.reasonrejection;
                    try
                    {
                        nvup.UserRevisionNotary = (AdministradorNvNotaryUsers.BuscarNvNotaryUserByUserId(paramIn.userid)).Id;
                    }
                    catch (Exception ex)
                    {
                        nvup.UserRevisionNotary = 0;
                        LOG.Error("NotarizeUnitOfWork.UpdateNvUserProcedure - Error recuperando Id de UserId = " + 
                                  (string.IsNullOrEmpty(paramIn.userid)?"Null": paramIn.userid));
                    }

                    bool bret = AdministradorNvUsersProcedures.Update(nvup); //AdministradorNvUsersProcedures.SetNvUserProcedureNotarizing(paramIn.trackid);
                    if (!bret)
                    {
                        msgerr = "Error actualizando estado de tramite! [" + Errors.IRET_ERR_SAVE_DB.ToString() + "]";
                        LOG.Debug("NotarizeUnitOfWork.UpdateNvUserProcedure - Retorno error [" + msgerr + "]");
                        return Errors.IRET_ERR_SAVE_DB;
                    } else
                    {
                        LOG.Debug("NotarizeUnitOfWork.UpdateNvUserProcedure - Sale con tramite actualiado!");
                        return Errors.IRET_OK;
                    }
                }
                else
                {
                    ret = Errors.IRET_ERR_INFORMED_PROVIDER_SERVICE;
                    msgerr = "Error informado por el provider en proceso de notarizacion.";
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NotarizeUnitOfWork.UpdateNvUserProcedure - Error: " + ex.Message);
            }
            LOG.Debug("NotarizeUnitOfWork.UpdateNvUserProcedure OUT!");
            return ret;
        }
    }
}