﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.Core.Database.Notary.config;
using Bio.Digital.Receipt.Core.Helpers;
using Bio.Digital.Receipt.rest.Models;
using log4net;

namespace Bio.Digital.Receipt.rest.Controllers
{
    public class NotaryPortalUnitOfWork
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NotaryPortalUnitOfWork));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listProcedures"></param>
        /// <param name="flagliviano">0 - Elimina </param>
        /// <returns></returns>        
        internal static List<Procedure> ConvertList(List<NvUsersProcedures> listProcedures, int flagliviano = 0)
        {
            List<Procedure> ret = null;
            NvUsers user;
            string auxusername;
            NvNotaryUsers notaryuser;
            string auxusernotaryname;
            string auxusernotarymail;
            Procedure proc;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.ConvertList - IN...");
                if (listProcedures == null || listProcedures.Count == 0)
                {
                    LOG.Debug("NotaryPortalUnitOfWork.ConvertList - Param null => Sale con null!");
                    return null;
                }
                ret = new List<Procedure>();

                LOG.Debug("NotaryPortalUnitOfWork.ConvertList - Start convert => len = " + listProcedures.Count.ToString());
                foreach (NvUsersProcedures item in listProcedures)
                {
                    auxusernotaryname = null;
                    auxusernotarymail = null;
                    //Tomo datos de users para completar los datos adicionales
                    user = AdministradorNvUsers.BuscarNvUserById(item.IdUser);
                    auxusername = (string.IsNullOrEmpty(user.Name) ? "" : user.Name) + " " +
                                (string.IsNullOrEmpty(user.PatherLastName) ? "" : user.PatherLastName);
                    if (item.UserRevisionNotary > 0) //Si aun no está asignado a alguien, nadie lo reviso aun
                    {
                        notaryuser = AdministradorNvNotaryUsers.BuscarNvNotaryUserById(item.UserRevisionNotary);
                        auxusernotaryname = (string.IsNullOrEmpty(notaryuser.Name) ? "" : notaryuser.Name) + " " +
                                    (string.IsNullOrEmpty(notaryuser.LastName) ? "" : notaryuser.LastName);
                        auxusernotarymail = notaryuser.Mail;
                    }
                    //Pongo en null los objetos de cada signatory y evidence para que no de error de serializacion de JSON por ciclico
                    foreach (NvUsersProceduresSignatory itemAux in item.NvUsersProceduresSignatory)
                    {
                        itemAux.NvUsersProcedures = null;
                    }
                    foreach (NvUsersProceduresEvidences itemAux in item.NvUsersProceduresEvidences)
                    {
                        itemAux.NvUsersProcedures = null;
                    }
                    proc = new Procedure(item, auxusername, user.Mail, auxusernotaryname, auxusernotarymail);

                    //Si es liviano => elimino doc que ocupa mucho
                    if (flagliviano == 0)
                    {
                        proc.dbProcedure.DocumentB64 = null;
                        proc.dbProcedure.DocumentB64Notarized = null;
                        proc.dbProcedure.DocumentB64Original = null;
                    }

                    ret.Add(proc);
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error(" Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.ConvertList - OUT! => len = " + ((ret!=null)?ret.Count.ToString():"Null"));
            return ret;
        }

        /// <summary>
        /// Genera lista de retorno para la lista de tramites devueltas con filtro
        /// </summary>
        /// <param name="listProcedures"></param>
        /// <param name="flagliviano"></param>
        /// <returns></returns>
        internal static List<ListItemProcedure> ConvertListReturn(List<NvUsersProcedures> listProcedures)
        {
            List<ListItemProcedure> ret = null;
            ListItemProcedure proc;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.ConvertList - IN...");
                if (listProcedures == null || listProcedures.Count == 0)
                {
                    LOG.Debug("NotaryPortalUnitOfWork.ConvertList - Param null => Sale con null!");
                    return null;
                }
                ret = new List<ListItemProcedure>();

                LOG.Debug("NotaryPortalUnitOfWork.ConvertList - Start convert => len = " + listProcedures.Count.ToString());
                foreach (NvUsersProcedures item in listProcedures)
                {
                    string name = "Pendiente";
                    if (item.NvUsersProceduresSignatory != null && item.NvUsersProceduresSignatory.Count > 0) {
                        name = (string.IsNullOrEmpty(item.NvUsersProceduresSignatory[0].Name) ? "" : item.NvUsersProceduresSignatory[0].Name) +
                                 " " +
                                 (string.IsNullOrEmpty(item.NvUsersProceduresSignatory[0].Lastname) ? "" : item.NvUsersProceduresSignatory[0].Lastname);

                        if (name.Trim().Length == 0) name = "Pendiente";
                        proc = new ListItemProcedure(item.Id, item.TrackId,
                                                     item.NvUsersProceduresSignatory[0].Typeid + " " + item.NvUsersProceduresSignatory[0].Valueid,
                                                     item.Status, name, item.NameProcedure, item.DateInit.ToString("dd/MM/yyyy"),
                                                     item.NvUsersProceduresSignatory.Count);
                        ret.Add(proc);
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                LOG.Error(" Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.ConvertListReturn - OUT! => len = " + ((ret != null) ? ret.Count.ToString() : "Null"));
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int ModifyRevisionProcedure(ModifyProcedureNotaryModel param, out string msgerr)
        {
            int ret = Errors.IRET_OK;
            msgerr = null;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.ModifyProcedure - IN...");
                NvUsersProcedures proc = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(param.trackid, out msgerr);

                if (proc == null)
                {
                    msgerr = "Tramite inexistente con trackid = " + param.trackid;
                    ret = Errors.IRET_ERR_PROCEDURE_NOTFOUND;
                } else
                {
                    //Si no esta en estado de revision => Salgo informando
                    //if (proc.Status != StatusNvUsersProcedures.STATUS_READY_TO_REVISION_NOTARIZE)
                    //{
                    //    msgerr = "El estado actual del tramite impide su modificacion"
                    //}


                    //Si esta aprobado => save status, texttoinclude,  
                    if (param.status == 1)
                    {
                        LOG.Debug("NotaryPortalUnitOfWork.ModifyProcedure - Save tramite APROBADO con trackid = " + param.trackid);
                        proc.Status = StatusNvUsersProcedures.STATUS_READY_TO_NOTARIZE;
                        proc.StatusRevisionNotary = param.status;
                        proc.TextToInclude = param.texttoinclude;
                    } else if (param.status == 2)
                    {
                        LOG.Debug("NotaryPortalUnitOfWork.ModifyProcedure - Save tramite RECHAZADO con trackid = " + param.trackid);
                        proc.Status = StatusNvUsersProcedures.STATUS_REJECTED;
                        proc.StatusRevisionNotary = param.status;
                        proc.ReasonReject = param.reasonreject;
                    }
                    if (!AdministradorNvUsersProcedures.Update(proc))
                    {
                        ret = Errors.IRET_ERR_NVPROCEDURE_NO_SAVED * -1;
                        msgerr = "Error salvando tramite modificado con trackid = " + param.trackid;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                msgerr = "Error desconocido ModifyProcedure [" + ex.Message + "]";
                LOG.Error("NotaryPortalUnitOfWork.ModifyProcedure - Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.ModifyProcedure - OUT!");
            return ret;
        }

        /// <summary>
        /// Determina que sea un doc valido => != null y que tenga formato PDF en esta v1
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        internal static bool IsCorrectDocument(string document)
        {
            bool ret = false;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.IsCorrectDocument IN...");
                if (string.IsNullOrEmpty(document))
                {
                    LOG.Debug("NotaryPortalUnitOfWork.IsCorrectDocument document==null => true");
                    return false;
                }

                ret = DEHelper.IsDocumentPDF(document);
                LOG.Debug("NotaryPortalUnitOfWork.IsCorrectDocument Check DEHelper.IsDocumentPDF = " + ret.ToString());
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("NotaryPortalUnitOfWork.IsCorrectDocument Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.IsCorrectDocument OUT!");
            return ret;
        }

        /// <summary>
        /// Chequea que rut sea correcto => !=null y bien formado
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        internal static bool IsCorrectRut(string rut)
        {
            bool ret = false;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.IsCorrectRut IN...");
                if (string.IsNullOrEmpty(rut))
                {
                    LOG.Debug("NotaryPortalUnitOfWork.IsCorrectRut rut==null => true");
                    return false;
                }
                libs.RUN RUN = libs.RUN.Convertir(rut);
                LOG.Debug("NotaryPortalUnitOfWork.IsCorrectRut RUN Ok => " + (RUN != null).ToString());
                ret = (RUN != null);
            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("NotaryPortalUnitOfWork.IsCorrectRut Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.IsCorrectRut OUT!");
            return ret;
        }

        /// <summary>
        /// Chequea mail correcto => Mail!=null y bien formado
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        internal static bool IsCorrectMail(string mail)
        {
            bool ret = false;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.IsCorrectMail IN...");
                if (string.IsNullOrEmpty(mail))
                {
                    LOG.Debug("NotaryPortalUnitOfWork.IsCorrectMail mail==null => true");
                    return false;
                }

                ret = libs.Utils.ComprobarFormatoEmail(mail);
                LOG.Debug("NotaryPortalUnitOfWork.IsCorrectMail mail Ok => " + (ret).ToString());

            }
            catch (Exception ex)
            {
                ret = false;
                LOG.Error("NotaryPortalUnitOfWork.IsCorrectMail Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.IsCorrectMail OUT!");
            return ret;
        }

        /// <summary>
        /// Chequea:
        ///     1) Si llega documento 
        ///     2) Si viene al menos un firmante
        ///     3) Si los datos del formante estan completos (mail + RUT)
        /// </summary>
        /// <param name="paramIn"></param>
        /// <param name="msgRet"></param>
        /// <returns></returns>
        internal static bool IsParamCreateCorrect(CreateProcedureModel paramIn, out string msgRet)
        {
            bool ret = true;
            msgRet = "";
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.IsParamCreateCorrect IN...");
                
                //     1) Si llega documento 
                if (String.IsNullOrEmpty(paramIn.document) || !IsCorrectDocument(paramIn.document))
                {
                    msgRet = "Documento enviado nulo o formato errone (debe ser PDF)";
                    ret = false;
                }

                //     2) Si viene al menos un firmante
                if (!ret || paramIn.signatories == null || paramIn.signatories.Count == 0)
                {
                    msgRet = "Debe informarse al menos un firmante!";
                    ret = false;
                }

                //  3) Si los datos del formante estan completos(mail + RUT)
                if (ret)
                {
                    foreach (Signatory item in paramIn.signatories)
                    {
                        if (String.IsNullOrEmpty(item.typeid) || String.IsNullOrEmpty(item.typeid) || !IsCorrectRut(item.valueid)) {
                            msgRet = "typeid/valueid incorrectos o nulos";
                            ret = false;
                            break;
                        } else if (String.IsNullOrEmpty(item.mail) || !IsCorrectMail(item.mail))
                        {
                            msgRet = "Mail con formato incorrecto o nulo";
                            ret = false;
                            break;
                        }
                        //else if (String.IsNullOrEmpty(item.phone) || !IsCorrectPhone(item.phone))
                        //{
                        //    msgRet = "Telefono con formato incorrecto o nulo";
                        //    ret = false;
                        //    break;
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                ret = false;
                msgRet = "IsParamCreateCorrect Excp Error [" + ex.Message + "]";
                LOG.Error("NotaryPortalUnitOfWork.IsParamCreateCorrect Excp Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.IsParamCreateCorrect OUT! [ret = " + ret.ToString() + "]");
            return ret;
        }

        /// <summary>
        /// Chequea:
        ///     1) Existe el user + esta habilitado + tiene el perfil para crear
        ///     2) Compañia habilitada
        /// </summary>
        /// <param name="iduser"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int IsEnabledToCreate(int iduser, out string msgerr)
        {
            int ret = 0;
            msgerr = "";
            try
            {
                NvUsers _User = AdministradorNvUsers.BuscarNvUserById(iduser);
                if (_User == null)
                {
                    msgerr = "Imposible recuperar el usuario";
                    ret = Errors.IRET_API_ERR_USER_EXIST * -1;
                } else
                {
                    if (_User.IsBlocked == 1) //Esta bloqueado
                    {
                        msgerr = "Usuario bloqueado " + (_User.DateBlocked.HasValue ?
                                                         "desde " + _User.DateBlocked.Value.ToString("dd/MM/yyyy") : "");
                        ret = Errors.IRET_API_ERR_USER_BLOCKED * -1;
                    } else
                    {
                        if (_User.Rol >= 3)  //Si no es auditor que es solo lectura pasa
                        {
                            msgerr = "Usuario sin permisos para inicio de tramite!";
                            ret = Errors.IRET_API_UNAUTHORIZED * -1;
                        }
                        else
                        {
                            Company _Company = AdministradorCompany.BuscarCompanyById(_User.CompanyId);
                            if (_Company == null || _Company.EndDate.HasValue) //Esta bloqueado
                            {
                                msgerr = "Compania deshabilitada " + (_Company.EndDate.HasValue ?
                                                                 "desde " + _Company.EndDate.Value.ToString("dd/MM/yyyy") : "");
                                ret = Errors.IRET_ERR_COMPANY_DISABLED * -1;
                            }
                        }
                    }
                } 
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                msgerr = "Error desconocido chqueando habilitacion [" + ex.Message + "]";
                LOG.Error("NotaryPortalUnitOfWork.IsEnabledToCreate Excp Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.IsParamCreateCorrect OUT! [ret = " + ret.ToString() + "]");
            return ret;
        }

        /// <summary>
        /// Crea el procedure, haciendo:
        ///     1) Crea objeto y llena valores default desde paramIn
        ///     2) Si vienen Evidences las agrega en la lista de Evidences
        ///     3) Por cada signature crea una peticion a BPWeb
        ///     4) Graba en BD
        ///     5) Retorna
        /// </summary>
        /// <param name="paramIn"></param>
        /// <param name="trackid"></param>
        /// <param name="msgerr"></param>
        /// <returns></returns>
        internal static int CreateProcedure(CreateProcedureModel paramIn, out string trackid, out string msgerr)
        {
            int ret = 0;
            trackid = "";
            msgerr = "";
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.CreateProcedure IN...");

                //1) Crea objeto y llena valores default desde paramIn
                
                NvUsersProcedures _Procedure = new NvUsersProcedures();
                _Procedure.TrackId = AdministradorNvUsersProcedures.GetTrackIdUnique();
                LOG.Debug("NotaryPortalUnitOfWork.CreateProcedure - Creo objeto para iduser = " + paramIn.iduser +
                            " - TrackID = " + _Procedure.TrackId);
                _Procedure.Status = StatusNvUsersProcedures.STATUS_CI_PENDING; //Status inicial, deben completar las CI para continuar la revision manual y conferencia
                _Procedure.IdUser = paramIn.iduser;
                _Procedure.IdNotary = paramIn.notaryId;
                _Procedure.IdProcedure = paramIn.procedureId;
                _Procedure.DateInit = DateTime.Now;
                _Procedure.DocumentB64 = paramIn.document;
                _Procedure.DocumentB64Original = paramIn.document;
                _Procedure.Message = paramIn.message;
                _Procedure.NameProcedure = AdministradorNvConfig.GetNameProcedure(paramIn.notaryId, paramIn.procedureId); ; // ""; //Get Nombre de Procedure por si cambia
                //TODO - Ver si se puede cargar algo mas

                //     2) Si vienen Evidences las agrega en la lista de Evidences
                if (paramIn.evidences != null && paramIn.evidences.Count > 0)
                {
                    _Procedure.NvUsersProceduresEvidences = new List<NvUsersProceduresEvidences>();
                    NvUsersProceduresEvidences ev;
                    foreach (Models.Evidence item in paramIn.evidences)
                    {
                        ev = new NvUsersProceduresEvidences();
                        ev.Name = item.name;
                        ev.Type = item.type;
                        ev.Value = item.value;
                        ev.TrackIdExt = item.trackidext;
                        ev.NvUsersProcedures = _Procedure;
                        _Procedure.NvUsersProceduresEvidences.Add(ev);
                    }
                }

                //     3) Por cada signature crea una peticion a BPWeb
                if (paramIn.signatories != null && paramIn.signatories.Count > 0)
                {
                    _Procedure.NvUsersProceduresSignatory = new List<NvUsersProceduresSignatory>();
                    NvUsersProceduresSignatory signatory;
                    foreach (Models.Signatory item in paramIn.signatories)
                    {
                        signatory = new NvUsersProceduresSignatory();
                        signatory.Phone = item.phone;
                        signatory.Typeid = item.typeid;
                        signatory.Valueid = item.valueid;
                        signatory.Mail = item.mail;

                        //3) Get URL de CI WEB para retornar
                        string sURLCI;
                        string sTrackIdCI = GenerateCIWeb(paramIn, item, out sURLCI, out msgerr);
                        if (string.IsNullOrEmpty(sURLCI) || string.IsNullOrEmpty(sTrackIdCI))
                        {
                            LOG.Error("NotaryPortalUnitOfWork.CreateProcedure - Error creando TraciIdCI o sURLCI para CI");
                            //return Request.CreateResponse(HttpStatusCode.Conflict,
                            //    (new NotarizeResponseModel(Errors.IRET_API_ERR_INIT_CI, "Error creando TraciIdCI o sURLCI para CI", null)));
                            ret = Errors.IRET_API_ERR_INIT_CI * -1;
                            msgerr = "Error generando inciio de certificacion de identidad para ID = " + item.valueid;
                            return ret;
                        }
                        signatory.Trackidci = sTrackIdCI;
                        signatory.Urlbpweb = sURLCI;
                        signatory.NvUsersProcedures = _Procedure;
                        _Procedure.NvUsersProceduresSignatory.Add(signatory);
                    }

                    //  4) Graba en BD
                    bool bRet = AdministradorNvUsersProcedures.Update(_Procedure);

                    if (bRet) //informo por mail el inicio del proceso
                    {
                        ret = Errors.IRET_OK;
                        msgerr = "";
                        trackid = _Procedure.TrackId;
                        foreach (NvUsersProceduresSignatory item in _Procedure.NvUsersProceduresSignatory)
                        {
                            //Informo x mail
                            string[] strbody = new string[4];
                            strbody[0] = "Estimad@ <b>" + item.Typeid + " " + item.Valueid + "</b>" + 
                                         ", se ha iniciado el trámite <b>" + _Procedure.NameProcedure + "</b>, en la Notaria <b><i>" + 
                                         AdministradorNvConfig.GetNameNotary(1,_Procedure.IdNotary) + "</i></b> para que usted lo firme. Attachado en este mail se encuentra el documento "
                                            + "a firmar si lo desea revisar.";
                            strbody[1] = "El codigo unico de Certificación de Identidad para consultas es: <b>" + _Procedure.TrackId + "</b>.";
                            strbody[2] = "Siga los pasos propuestos de Certificación de Identidad, y cuando lo complete, cierre la ventana del navegador. " +
                                "Enviarémos una copia de la Certificación realizada a su mail y a quien lo requirió";
                            //strbody[3] = "<button type=\"button\" style=\"color: white; font - family:'Arial'; background - color: #6EA4CA;\" onclick=\"window.open('" +
                            //     BPResponse.verifyUrl + "', '_blank');\">  Empezar proceso de Certificación de Identidad...  </button>";
                            strbody[3] = "Inicie el proceso de Certificación de Identidad presionando  <a href=\"" +
                                 item.Urlbpweb + "\" target =\"_blank\"><font style=\"color:ligth-blue; font-family:'Arial'\">AQUI</font></a>.";
                            byte[] docpdf = Convert.FromBase64String(_Procedure.DocumentB64Original);
                            libs.Utils.NVNotify(item.Mail, "Inicio de Certificacion de Identidad", strbody, docpdf, "pdf",
                                            _Procedure.TrackId + ".pdf");
                        }
                    } else
                    {
                        ret = Errors.IRET_ERR_NVPROCEDURE_NO_SAVED * -1;
                        msgerr = "Error grabando tramite en base de datos!";
                    }
                } else //Si no hay firmante retorna error
                {
                    ret = Errors.IRET_ERR_PARAM_INCORRECTOS;
                    msgerr = "Debe existir al menos un firmante!";
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                msgerr = "Error desconocido en CreateProcedure [" + ex.Message + "]";
                LOG.Error("NotaryPortalUnitOfWork.CreateProcedure Excp Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.CreateProcedure OUT! [ret = " + ret.ToString() + "]");
            return ret;
        }

        /// <summary>
        /// Debe generar la transaccion de la CIWeb con la conexion a BPWeb, y creacion de transaccion en tabla rd_CITx
        /// </summary>
        /// <param name="trackId"></param>
        /// <param name="paramin"></param>
        /// <param name="urlciweb"></param>
        /// <param name="msgerr"></param>
        /// <returns>trackid de CI Web o nulo si falla</returns>
        internal static string GenerateCIWeb(CreateProcedureModel paramIn, Signatory signatory, out string urlciweb, out string msgerr)
        {
            urlciweb = null;
            msgerr = null;
            string trackId = null;
            string sTrackIdCItoReturn = null;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.GenerateCIWeb IN...");

                //3) Creo TrackId Nuevo
                trackId = AdministradorRdCITx.GetTrackIdUnique();
                if (string.IsNullOrEmpty(trackId))
                {
                    LOG.Error("NotaryPortalUnitOfWork.GenerateCIWeb - TrackId generado invalido [Null]");
                    return null;
                }


                //4) Creo TxCreate en BioPortal
                //string msgerr;
                string trackidbp;
                string listMails = null;
                BPTxCreateR BPResponse;
                int ret = BPHelper.TxCreateNotarize(trackId, paramIn, signatory, out msgerr, out listMails, out trackidbp, out BPResponse);

                if (ret < 0 || BPResponse == null)
                {
                    LOG.Error("NotaryPortalUnitOfWork.GenerateCIWeb -  BPHelper.TxCreate Error => " +
                        ret + " - " + (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr));
                    return null;
                    //Request.CreateResponse(HttpStatusCode.Conflict,
                    //(new ErrorModel(Errors.IRET_API_CONFLIC, "Problemas iniciar proceso de verificacion en servicio biometrico. " +
                    //"TxCreate Error => " + ret + " - " + (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr),
                    //null)));
                }
                else
                {


                    //5) Grabo en BD
                    ret = 0;
                    ret = AdministradorRdCITx.CreateNotarizeTxWeb(trackId, signatory, BPResponse, trackidbp, out msgerr);

                    //6) Retorno si hay error
                    if (ret < 0)
                    {
                        LOG.Error("NotaryPortalUnitOfWork.GenerateCIWeb - Error creando CI Tx Web [" + ret + "-" + msgerr + "]");
                        return null;
                    }
                    urlciweb = BPResponse.verifyUrl;
                    sTrackIdCItoReturn = trackId;
                    LOG.Debug("NotaryPortalUnitOfWork.GenerateCIWeb - Creada CI Web con trackid = " + trackId + " y url = " + urlciweb);
                }
            }
            catch (Exception ex)
            {
                urlciweb = null;
                LOG.Error("NotaryPortalUnitOfWork.GenerateCIWeb Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.GenerateCIWeb OUT!");
            return sTrackIdCItoReturn;
        }

        /// <summary>
        /// Recupera StatusCI para actualizar
        /// </summary>
        /// <param name="trackid"></param>
        /// <param name="rdout"></param>
        /// <returns></returns>
        internal static int GetStatusCI(string trackid, out string rdout)
        {
            rdout = null;
            int ret = -1;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.GetStatusCI IN...");
                string xmlparamout;
                Bio.Digital.Receipt.Api.XmlParamIn oParamin = new Bio.Digital.Receipt.Api.XmlParamIn();
                oParamin.Actionid = 6;
                oParamin.Origin = 1;
                oParamin.Companyid = Convert.ToInt32(Properties.Settings.Default.NVCompanyCI);
                oParamin.ApplicationId = Convert.ToInt32(Properties.Settings.Default.NVAppCI);
                oParamin.CIParam = new Bio.Digital.Receipt.Api.CIParam();
                oParamin.CIParam.TrackId = trackid;
                LOG.Debug("NotaryPortalUnitOfWork.GetStatusCI - Consultando ...");
                ret = Core.Services.CIServiceManager.Process(oParamin, out xmlparamout);
                LOG.Debug("NotaryPortalUnitOfWork.GetStatusCI - ret = " + ret);
                if (ret == 1)
                {
                    if (!string.IsNullOrEmpty(xmlparamout))
                    {
                        LOG.Debug("NotaryPortalUnitOfWork.GetStatusCI - Deserializando xmlparamout...");
                        Bio.Digital.Receipt.Api.XmlParamOut oParamout =
                            Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                        if (oParamout != null && !string.IsNullOrEmpty(oParamout.Receipt))
                        {
                            LOG.Debug("NotaryPortalUnitOfWork.GetStatusCI - Retornando RD...");
                            rdout = oParamout.Receipt;
                            ret = 1;
                        }
                        else
                        {
                            LOG.Error("NotaryPortalUnitOfWork.GetStatusCI - Error Deserializando => oParamout == null o Receipt Nulo");
                        }
                    }
                    else
                    {
                        LOG.Warn("NotaryPortalUnitOfWork.GetStatusCI - Retorno xmlparamout == null");
                    }
                }
                else
                {
                    LOG.Warn("NotaryPortalUnitOfWork.GetStatusCI - Status CI aun en 0!");
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("NotaryPortalUnitOfWork.GetStatusCI Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.GetStatusCI OUT!");
            return ret;
        }

        /// <summary>
        /// Parsea RD recibido desde CI en Webhook para updatear la BD
        /// </summary>
        /// <param name="strIn"></param>
        /// <param name="htRD"></param>
        /// <returns></returns>
        internal static bool ParseRDEnvelope(string strIn, out Hashtable htRD)
        {
            //RdRecibos oRDRet = null;
            bool bret = false;
            htRD = null;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.ParseRDEnvelope IN...");
                //1.- Parseo RDEnvelope
                XmlDocument doc = new XmlDocument();
                System.IO.StringReader sr = new System.IO.StringReader(strIn);
                doc.Load(sr);
                sr.Close();

                if (doc != null)
                {
                    //string sRD = doc.GetElementsByTagName("Rd")[0].OuterXml;

                    //oRDRet = Api.XmlUtils.DeserializeObject<RdRecibos>(sRD);

                    //if (string.IsNullOrEmpty(sRD)) //(oRDRet != null)
                    //{

                    LOG.Debug("NotaryPortalUnitOfWork.ParseRDEnvelope LoadXML RDEnvelope correcto...");
                    //Display all the book titles.
                    XmlNodeList elemList = doc.GetElementsByTagName("ExtensionItem");
                    htRD = new Hashtable();
                    foreach (XmlNode item in elemList)
                    {
                        htRD.Add(item.FirstChild.InnerText, item.LastChild.InnerText);
                    }
                    LOG.Debug("NotaryPortalUnitOfWork.ParseRDEnvelope - Len htRD = " + (htRD != null ? "NULL" : htRD.Count.ToString()));
                    bret = true;
                    //} else
                    //{
                    //    LOG.Warn("NotarizeController.ParseRDEnvelope Parseo RD incorrecto. Se aborta parseo!");
                    //}
                }
            }
            catch (Exception ex)
            {
                //oRDRet = null;
                bret = false;
                LOG.Error("NotaryPortalUnitOfWork.ParseRDEnvelope Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.ParseRDEnvelope OUT!");
            return bret; // oRDRet;
        }

        /// <summary>
        /// Recupera StatusCI para actualizar el procedure. Toma directo desde BD para hacerlo mas facil.
        /// Si la CI se hace en otro server se debe cambiar este procedure para hacerlo via Web Service o APIRest
        /// </summary>
        /// <param name="trackid"></param>
        /// <param name="citxOut"></param>
        /// <returns></returns>
        internal static int GetStatusCIDirectBD(string trackid, out RdCITx citxOut)
        {
            citxOut = null;
            int ret = Errors.IRET_OK;
            citxOut = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.GetStatusCIDirectBD IN...");

                citxOut = AdministradorRdCITx.GetCITxByTrackId(trackid, out msgerr);

                if (citxOut == null)
                {
                    ret = Errors.IRET_ERR_CI_NO_IN_DB;
                    msgerr = "La Certificacion de Identidad con trackdid = " + trackid + " no existe!";
                } 
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NotaryPortalUnitOfWork.GetStatusCIDirectBD Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.GetStatusCIDirectBD OUT!");
            return ret;
        }

        /// <summary>
        /// Dado un procedure y signatory con el status de la CI, se llenan los datos en la lista NvUsersProceduresSignatory
        /// en el Signatory correspondiente.
        /// </summary>
        /// <param name="procedure"></param>
        /// <param name="rd"></param>
        /// <param name="htRD"></param>
        /// <param name="trackidci"></param>
        /// <param name="nvup"></param>
        /// <returns></returns>
        internal static int UpdateInfoSignatoryInMemory(NvUsersProcedures procedure, NvUsersProceduresSignatory signatory,
                                                        RdCITx citx, out string trackidci, out NvUsersProcedures nvup)
        {
            int ret = Errors.IRET_OK;
            trackidci = null;
            nvup = null;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.UpdateInfoSignatoryInMemory IN! Actualizando Signatory => " +
                           signatory.Typeid + "/" + signatory.Valueid + " en Procedure.trackid = " + procedure.TrackId + "...");
                foreach (NvUsersProceduresSignatory item in procedure.NvUsersProceduresSignatory)
                {
                    if (item.Id == signatory.Id)
                    {
                        item.Birthdate = libs.Utils.Parse(citx.BirthDate); // libs.Utils.Parse((string)htRD["BirthDate"]); 
                        item.Expirationdate = libs.Utils.Parse(citx.ExprationDate); // (string)htRD["ExpirationDate"]);
                        item.Fingersample = citx.FingerSample; //Ver RD nuevo para completar
                        item.Fingersampleimage = citx.FingerSampleJpg;
                        item.Georef = citx.GeoRef;
                        item.Idcardimageback = citx.IdCardImageBack;
                        item.Idcardimagefront = citx.IdCardImageFront;
                        item.Idcardphotoimage = citx.IdCardPhotoImage;
                        item.Idcardsignatureimage = citx.IdCardSignatureImage;
                        item.Issuedate = libs.Utils.Parse(citx.IssueDate);
                        item.Lastmodify = DateTime.Now;
                        item.Lastname = citx.PhaterLastName + (string.IsNullOrEmpty(citx.MotherLastName)? "" : " " + citx.MotherLastName);
                        item.Manualsignatureimage = citx.ManualSignatureImage;
                        item.Map = citx.Map;
                        item.Nacionality = citx.Nacionality;
                        item.Name = citx.Name;
                        item.Score = citx.Score;
                        item.Threshold = citx.Threshold;
                        item.Verifyresult = citx.ResultCode.ToString(); //1 es CI generada | 0-No se genero 
                        item.Videourl = citx.VideoURL;
                        item.Workstationid = citx.WorkstationId;
                        item.Status = 1; //Completa Positiva o Negativa pero completa. Luego el revisor deberá ver la forma que verificar la 
                                         //identidad si fue verificación negativa
                        nvup = procedure;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NotaryPortalUnitOfWork.UpdateInfoSignatoryInMemory Excp Error: " + ex.Message);
            }
            return ret;
        }

        /// <summary>
        /// Dado un Procedure, recorre lo necesario para determinar el status y cambiardo/grabarlo si hace falta
        /// </summary>
        /// <param name="nvupIN">Objeto de entrada</param>
        /// <param name="nvupOUT">Objeto de salida con cambio realizado si se hizo</param>
        /// <returns></returns>
        internal static bool UpdateStatusProcedure(NvUsersProcedures nvupIN, out NvUsersProcedures nvupOUT)
        {
            bool bret = false;
            nvupOUT = nvupIN;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.UpdateStatusProcedure IN...");
                if (nvupIN != null)
                {
                    //Si esta CI Pending => recorro los signatory para saber si completo cada uno y si es asi, 
                    //con verify + o - => Paso a nuevo estado. 
                    //Por ahora paso a ReadyToNotarize porque obviamos el pago por ahora.
                    if (nvupIN.Status == StatusNvUsersProcedures.STATUS_MAIL_VERIFY_PENDING ||
                        nvupIN.Status == StatusNvUsersProcedures.STATUS_CI_PENDING)
                    {
                        bool _bAllCIComnplete = true;
                        LOG.Debug("NotaryPortalUnitOfWork.UpdateStatusProcedure - Recorro signatories de trackid=" + nvupIN.TrackId +
                                    " para revisar status...");
                        foreach (NvUsersProceduresSignatory item in nvupIN.NvUsersProceduresSignatory)
                        {
                            _bAllCIComnplete = _bAllCIComnplete && (item.Status == 1);
                        }
                        if (_bAllCIComnplete) {
                            LOG.Debug("NotaryPortalUnitOfWork.UpdateStatusProcedure - Actualizo status de trackid=" + nvupIN.TrackId +
                                        " STATUS_READY_TO_REVISION_NOTARIZE...");
                            nvupIN.Status = StatusNvUsersProcedures.STATUS_READY_TO_REVISION_NOTARIZE;
                            bret = AdministradorNvUsersProcedures.Update(nvupIN);
                            if (!bret)
                            {
                                LOG.Warn("NotaryPortalUnitOfWork.UpdateStatusProcedure - Error en AdministradorNvUsersProcedures.Update..:");
                            }
                        }  // TODO - Terminar de revisar la logica !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    }
                }
            }
            catch (Exception ex)
            {
                bret = false;
                LOG.Error("NotaryPortalUnitOfWork.UpdateStatusProcedure Excp Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.UpdateStatusProcedure OUT! ret = " + bret.ToString());
            return bret;
        }

        /// <summary>
        /// Dado un Procedure, da la baja logica
        /// </summary>
        /// <param name="nvupIN">Objeto de entrada</param>
        /// <param name="nvupOUT">Objeto de salida con cambio realizado si se hizo</param>
        /// <returns></returns>
        internal static bool DeleteLogicProcedure(NvUsersProcedures nvupIN, out NvUsersProcedures nvupOUT)
        {
            bool bret = false;
            nvupOUT = null;
            try
            {
                LOG.Debug("NotaryPortalUnitOfWork.DeleteLogicProcedure IN...");
                if (nvupIN != null)
                {
                    LOG.Debug("NotaryPortalUnitOfWork.DeleteLogicProcedure - Actualizo status de trackid=" + nvupIN.TrackId +
                                " CANCELLED...");
                    nvupIN.Status = StatusNvUsersProcedures.STATUS_CANCELLED;
                    bret = AdministradorNvUsersProcedures.Update(nvupIN);
                    if (!bret)
                    {
                        LOG.Warn("NotaryPortalUnitOfWork.DeleteLogicProcedure - Error en AdministradorNvUsersProcedures.DeleteLogicProcedure..:");
                    } else
                    {
                        nvupOUT = nvupIN;
                    }    
                }
            }
            catch (Exception ex)
            {
                bret = false;
                LOG.Error("NotaryPortalUnitOfWork.UpdateStatusProcedure Excp Error: " + ex.Message);
            }
            LOG.Debug("NotaryPortalUnitOfWork.UpdateStatusProcedure OUT! ret = " + bret.ToString());
            return bret;
        }
    }
}