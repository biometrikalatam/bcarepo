﻿using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.Core.Helpers;
using Bio.Digital.Receipt.Core.Services;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Bio.Digital.Receipt.rest.Controllers
{
    public static class SignerUnitOfWork //: ApiController
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(SignerUnitOfWork));

        internal static int UpdateRdSignerTx(RdCITx tx, out string msgerr, out RdCITx txupdated)
        {
            /*
                1.- Check Null y status que sea pendiente aun
                2.- Recupero desde BP status 
                3.- Si termino proceso en BP => Termino proceso pedido (en tramites tag hago poder)
                4.- Si debo informar via callback informo
                5.- Retorno
            */
            int ret = 0;
            msgerr = null;
            txupdated = null;
            try
            {
                LOG.Debug("SignerController.UpdateRdSignerTx IN...");
                //1.- Check Null y status que sea pendiente aun
                if (CIServiceManager.GetStatus(tx).Equals("DONE"))
                {
                    LOG.Debug("SignerController.UpdateRdSignerTx - Sale proque es DONE!");
                    ret = Errors.IRET_API_ERR_CI_COMPLETED;
                    txupdated = tx;
                    return ret;
                }

                if (CIServiceManager.GetStatus(tx).StartsWith("EXPIRED"))
                {
                    LOG.Debug("SignerController.UpdateRdSignerTx - Sale proque es EXPIRED!");
                    tx.ActionId = -10; //Expirada
                    AdministradorRdCITx.UpdateCITx(tx, out msgerr);
                    ret = Errors.IRET_API_ERR_CI_EXPIRED;
                    txupdated = tx;
                    return ret;
                }
                //2.- Recupero desde BP status
                BpTxDataResponse oBpTxDataResponse;
                LOG.Debug("SignerController.UpdateRdSignerTx - Pide status a BP con TrackId = " + tx.TrackIdBP);
                ret = BPHelper.TxStatus(tx.TrackIdBP, out msgerr, out oBpTxDataResponse);
                if (ret < 0)
                {
                    LOG.Debug("SignerController.UpdateRdSignerTx - Retorno de pedido a BP => " + ret + " => Sale!");
                    return ret;
                }

                //3.- Si termino proceso en BP => Genero CI 
                if (oBpTxDataResponse == null)
                {
                    LOG.Debug("SignerController.UpdateRdSignerTx - oBpTxDataResponse NULO...");
                    txupdated = tx; //Significa que no hubo error pero esta en pending aun en BPWeb => Inofrmo lo que está en tx
                }
                else  //Si llego aca es que esta DONE el proceso de verify en BP => Genero CI
                {
                    string xmlparamout = null;
                    XmlParamIn oXmlParamIn = new XmlParamIn();

                    tx = FillTxFromBPResponse(tx, oBpTxDataResponse);

                    int _resultVerify = oBpTxDataResponse.result;
                    double _score = oBpTxDataResponse.score;
                    LOG.Debug("SignerController.UpdateRdSignerTx - Procesa respuesta desde BP => score = " + _score.ToString() +
                        " - resultverify = " + _resultVerify);
                    if (_resultVerify == 1 || _resultVerify == 2)
                    {
                        LOG.Debug("NotarizeController.UpdateRdSignerTx - Result = 1 => Genera CI...");
                        if (oBpTxDataResponse.onboardingmandatory == 0) //Verify Cedula vs Selfie
                        {
                            oXmlParamIn.Actionid = 19;
                        }
                        else //Verify Selfie vs BD
                        {
                            oXmlParamIn.Actionid = 18;
                        }
                        oXmlParamIn.Companyid = tx.CompanyId;
                        oXmlParamIn.Origin = 1;
                        oXmlParamIn.ApplicationId =
                            AdministradorRdAplicaciones.BuscarRdAplicacionesByCode(tx.CompanyId, Properties.Settings.Default.SignerCode);
                        LOG.Debug("SignerController.UpdateRdSignerTx - ApplicationId recuperado con Code = " +
                                    Properties.Settings.Default.SignerCode + " => " + oXmlParamIn.ApplicationId);
                        oXmlParamIn.CIParam = new CIParam();
                        oXmlParamIn.CIParam.BirthDate = oBpTxDataResponse.personaldata.birthdate; // tx.BirthDate;
                        oXmlParamIn.CIParam.DestinaryMail = tx.DestinataryMail;
                        oXmlParamIn.CIParam.ExpirationDate = oBpTxDataResponse.personaldata.documentexpirationdate; // tx.ExprationDate;
                        oXmlParamIn.CIParam.GeoRef = tx.GeoRef; //Ver este valor si viene o sale de aca
                        oXmlParamIn.CIParam.IDCardImageBack = BPHelper.GetSample(oBpTxDataResponse, 1); //tx.IdCardImageBack;
                        oXmlParamIn.CIParam.IDCardImageFront = BPHelper.GetSample(oBpTxDataResponse, 0);  //tx.IdCardImageFront;
                        oXmlParamIn.CIParam.IDCardPhotoImage = BPHelper.GetSample(oBpTxDataResponse, 4); //tx.IdCardPhotoImage;
                        oXmlParamIn.CIParam.IDCardSignatureImage = BPHelper.GetSample(oBpTxDataResponse, 3);
                        ; // tx.IdCardSignatureImage;
                        oXmlParamIn.CIParam.IssueDate = null; // tx.IssueDate;
                        oXmlParamIn.CIParam.ManualSignatureImage = BPHelper.GetSample(oBpTxDataResponse, 5); //tx.ManualSignatureImage;
                        oXmlParamIn.CIParam.MotherLastName = oBpTxDataResponse.personaldata.motherlastname; // tx.MotherLastName;
                        oXmlParamIn.CIParam.Nacionality = oBpTxDataResponse.personaldata.nationality; // tx.Nacionality;
                        oXmlParamIn.CIParam.Name = oBpTxDataResponse.personaldata.name; // tx.Name;
                        oXmlParamIn.CIParam.PhaterLastName = oBpTxDataResponse.personaldata.patherlastname; // tx.PhaterLastName;
                        oXmlParamIn.CIParam.Selfie = BPHelper.GetSample(oBpTxDataResponse, 2); //tx.Selfie;
                        oXmlParamIn.CIParam.Score = oBpTxDataResponse.score.ToString(); // tx.Score;
                        oXmlParamIn.CIParam.Serial = oBpTxDataResponse.personaldata.documentseriesnumber; // tx.Serial;
                        oXmlParamIn.CIParam.Sex = oBpTxDataResponse.personaldata.sex; // tx.Sex;
                        oXmlParamIn.CIParam.Threshold = oBpTxDataResponse.threshold.ToString(); // tx.Threshold;
                        oXmlParamIn.CIParam.TrackId = tx.TrackId;
                        oXmlParamIn.CIParam.TypeId = oBpTxDataResponse.personaldata.typeid; // tx.TypeId;
                        oXmlParamIn.CIParam.ValidityType = (int)oBpTxDataResponse.typeverify; // tx.ValidityType;
                        oXmlParamIn.CIParam.ValueId = oBpTxDataResponse.personaldata.valueid; // tx.ValueId;
                        oXmlParamIn.CIParam.WorkStationID = oBpTxDataResponse.enduser;
                        oXmlParamIn.CIParam.GeoRef = oBpTxDataResponse.georef;
                        oXmlParamIn.CIParam.URLVideo = oBpTxDataResponse.videourl;

                        LOG.Debug("SignerController.UpdateRdSignerTx - CIServiceManager.GenerateCI_From_BPWeb ingresando...");
                        ret = CIServiceManager.GenerateSignerTx_From_BPWeb(tx, oXmlParamIn, _resultVerify, _score, false, tx.Form, 
                                                                           out xmlparamout);
                        LOG.Debug("SignerController.UpdateRdSignerTx - CIServiceManager.GenerateCI_From_BPWeb  ret 0 " + ret);
                        if (ret == 0) //Si actualizo ok => Mando mail a quien se pidio
                        {
                            LOG.Debug("SignerController.UpdateRdSignerTx - CIServiceManager.GenerateCI_From_BPWeb  sending mails a destinatarios...");
                            ret = SendMsgToDestinataries(tx.TrackId);
                            LOG.Debug("SignerController.UpdateRdSignerTx - CIServiceManager.GenerateCI_From_BPWeb ret mailing = " + ret);
                        }
                    }

                    //Comento porque no me importa ahora si es positivo o negativo, solo se informa en el poder pero no es bloqueante
                    //if (_resultVerify == 2)
                    //{
                    //    LOG.Debug("SignerController.UpdateRdSignerTx - Updating Verify Negative...");
                    //    tx.ActionId = -11; //Verificacion Negativa
                    //    tx.Score = _score.ToString();
                    //    AdministradorRdCITx.UpdateCITx(tx, out msgerr);
                    //    LOG.Debug("SignerController.UpdateRdSignerTx - Termino Update Negative. MsgErr => " +
                    //                (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr));
                    //    LOG.Debug("SignerController.UpdateRdSignerTx - CIServiceManager.GenerateCI_From_BPWeb sending mails a destinatarios avisando error...");
                    //    ret = SendCIToDestinatariesXError(tx.TrackId);
                    //    LOG.Debug("SignerController.UpdateRdSignerTx - CIServiceManager.GenerateCI_From_BPWeb ret mailing = " + ret);
                    //}

                    if (_resultVerify == 0)
                    {
                        LOG.Debug("SignerController.UpdateRdSignerTx - _resultVerify = 0 - operationcode = " + oBpTxDataResponse.operationcode);
                        if (oBpTxDataResponse.operationcode < 0)
                        {
                            tx.ActionId = oBpTxDataResponse.operationcode;
                            tx.Score = _score.ToString();
                            AdministradorRdCITx.UpdateCITx(tx, out msgerr);
                            LOG.Debug("SignerController.UpdateRdSignerTx - Termino Update Error [" + oBpTxDataResponse.operationcode
                                        + "]. MsgErr => " + (string.IsNullOrEmpty(msgerr) ? "Null" : msgerr));
                            LOG.Debug("SignerController.UpdateRdSignerTx - CIServiceManager.GenerateCI_From_BPWeb sending mails a destinatarios avisando error...");
                            ret = SendMsgToDestinatariesXError(tx.TrackId);
                            LOG.Debug("SignerController.UpdateRdSignerTx - CIServiceManager.GenerateCI_From_BPWeb ret mailing = " + ret);
                        }
                    }
                    LOG.Debug("SignerController.UpdateRdSignerTx - Recupero tx updated con id => " + (tx != null ? tx.Id.ToString() : "Null"));
                    txupdated = AdministradorRdCITx.GetCITxByRdId(tx.Id, out msgerr);
                    LOG.Debug("SignerController.UpdateRdSignerTx - txupdated != null => " + (txupdated != null).ToString());
                }
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("SignerController.UpdateRdSignerTx Error: " + ex.Message);
            }
            LOG.Debug("SignerController.UpdateRdSignerTx OUT!");
            return ret;
        }

        private static RdCITx FillTxFromBPResponse(RdCITx tx, BpTxDataResponse oBpTxDataResponse)
        {
            try
            {
                LOG.Debug("SignerController.FillTxFromBPResponse IN...");
                foreach (ImageSample item in oBpTxDataResponse.Samples)
                {
                    if (item.code.Equals("carregisterfront"))
                        tx.CarRegisterImageFront = item.data;

                    if (item.code.Equals("carregisterback"))
                        tx.CarRegisterImageBack = item.data;

                    if (item.code.Equals("writing"))
                        tx.WritingImage = item.data;

                    if (item.code.Equals("form"))
                        tx.Form = item.data;

                    if (item.code.Equals("manualsignature"))
                        tx.ManualSignatureImage = item.data;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("SignerController.FillTxFromBPResponse Error: " + ex.Message);
            }
            LOG.Debug("SignerController.FillTxFromBPResponse IN...");
            return tx;
        }

        internal static int SendMsgToDestinataries(string trackidCI)
        {
            int ret = 0;
            XmlParamOut oXmlOut;
            string msgerr;
            try
            {
                RdCITx tx = AdministradorRdCITx.GetCITxByTrackId(trackidCI, out msgerr);
                if (tx != null)
                {
                    //Added 10-05-2020 - Send mail HTMl con certificados attachados. MAndo PDF + XML
                    LOG.Debug("Informo por mail " + tx.DestinataryMail + " a destinatario enviando docs...");
                    string[] strbody = new string[2];
                    strbody[0] = "Usted completó el proceso de la generación del documento, con el firmante <b>" +
                                    tx.TypeId + " " + tx.ValueId + "</b>";
                    strbody[1] = "El codigo unico del tramite para consultas es: <b>" + tx.TrackId + "</b>";
                    byte[] docpdf = Convert.FromBase64String(tx.CertifyPDF);
                    //byte[] docxml = System.Text.Encoding.UTF8.GetBytes(tx.r);
                    //Send PDF

                    string datatocontat = GetPatenteFromJson(tx);
                    //| Patente = " + patente + " | eMail = " + mail
                    string[] amailto = new string[1];
                    amailto[0] = tx.DestinataryMail;
                    bool bret = libs.Utils.NVNotifySigner(amailto,
                                        "Documento Generado en TramitesTag.cl",
                                        strbody, docpdf, "pdf", tx.TrackId + ".pdf", 
                                        Properties.Settings.Default.PATH_Logos + tx.CompanyId.ToString() + "_logoheader.jpg",
                                        Properties.Settings.Default.PATH_Logos + tx.CompanyId.ToString() + "_logofooter.jpg");
                    LOG.Debug("SignerController.SendMsgToDestinataries - Enviado PDF a " + tx.DestinataryMail + " de trackid = " +
                                tx.TrackId + "/rut = " + tx.ValueId + " => " + (bret.ToString()));

                    //bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                    //                    "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
                    //                    oXmlOut.CIParam.TrackId + ".xml");
                    //LOG.Debug("Enviado XML => " + (bret.ToString()));

                    if (tx.ListMailsDistribution != null)
                    {
                        string[] _lDestinatary = tx.ListMailsDistribution.Replace(";", ",").Split(',');
                        //Armo cuerpo para enviar a TTag
                        strbody = new string[2];
                        strbody[0] = "Se completó el proceso de la generación del documento, con el firmante <b>" +
                                tx.TypeId + " " + tx.ValueId + "</b>";
                        strbody[1] = "El codigo unico del tramite para consultas es: <b>" + tx.TrackId + "</b>";
                        docpdf = Convert.FromBase64String(tx.CertifyPDF);

                        foreach (string item in _lDestinatary)
                        {
                            LOG.Debug("Informo por mail " + item + " a destinatario enviadno docs...");
                            strbody = new string[4];
                            strbody[0] = "Se completó el proceso de la generación del documento, con el firmante <b>" +
                                    tx.TypeId + " " + tx.ValueId + "</b>";
                            strbody[1] = "El codigo unico del tramite para consultas es: <b>" + tx.TrackId + "</b>";
                            strbody[2] = "Verificación " +
                                         (tx.ResultCode == 1 ?
                                         "<b><font style=\"color:green; font-family:'Arial'\">POSITIVA</font></b>" :
                                         "<b><font style=\"color:red; font-family:'Arial'\">NEGATIVA</font></b>") +
                                          " [Score=" + Convert.ToDouble(tx.Score).ToString("##.##") + "%]";
                            strbody[3] = "Visualice los detalles de la transacción presionando  <a href=\"" +
                                        Properties.Settings.Default.BASE_SIGNER_ViewInternal + "?trackid=" + tx.TrackId + 
                                        "\" target =\"_blank\"><font style=\"color:ligth-blue; font-family:'Arial'\">aqui</font></a>.";
                            //Envio mail a backend de TTag, con link a detalle si necesita.
                            bret = libs.Utils.NVNotify(item, "Documento Generado [RUT=" + tx.ValueId +
                                                      (string.IsNullOrEmpty(datatocontat) ? "" : "|" + datatocontat) + "]",
                                                      strbody, docpdf, "pdf", tx.TrackId + ".pdf");
                            LOG.Debug("SignerController.SendMsgToDestinataries - Enviado PDF a " + item + " de trackid = " +
                                 tx.TrackId + "/rut = " + tx.ValueId + " => " + (bret.ToString()));

                            //Added por TTag si hay Escritra enviamos.
                            //if (!string.IsNullOrEmpty(tx.WritingImage))
                            //{
                            //    docpdf = Convert.FromBase64String(tx.WritingImage);
                            //    bret = libs.Utils.NVNotify(tx.DestinataryMail,
                            //                    "Documento Adicional - Acreditacion de Poderes [RUT=" + tx.ValueId +
                            //                    (string.IsNullOrEmpty(datatocontat) ? "" : "|" + datatocontat) + "]",
                            //                    strbody, docpdf, "pdf", "Poder_Representacion_" + tx.TrackId + ".pdf");
                            //    LOG.Debug("SignerController.SendMsgToDestinataries - Enviado Poder de Representacion a " + tx.DestinataryMail + " de trackid = " +
                            //                tx.TrackId + "/rut = " + tx.ValueId + " => " + (bret.ToString()));
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("SignerController.SendMsgToDestinataries Error: " + ex.Message);
            }
            return ret;
        }

        private static string GetPatenteFromJson(RdCITx tx)
        {
            string sRet = null;
            string patente = null;
            string mailfirmante = null;
            mailfirmante = null;
            bool hayPatente = false;
            bool hayMailfirmante = false;

            try
            {
                LOG.Debug("SignerController.GetPatenteFromJson IN...");
                if (string.IsNullOrEmpty(tx.Form))
                {
                    LOG.Debug("SignerController.GetPatenteFromJson - Sale porque entrada => jsonform = null");
                    return null;
                }

                LOG.Debug("SignerController.GetPatenteFromJson - Deserialiso jsonform...");
                Dictionary<string, string> _FORM = JsonConvert.DeserializeObject<Dictionary<string, string>>(tx.Form);

                LOG.Debug("SignerController.GetPatenteFromJson - _FORM != Null => " + (_FORM != null).ToString());

                if (!string.IsNullOrEmpty(_FORM["patentevehiculo"]))
                {
                    patente = "Patente=" + _FORM["patentevehiculo"];
                    LOG.Debug("SignerController.GetPatenteFromJson - Set => " + patente);
                    hayPatente = true;
                }
                if (!string.IsNullOrEmpty(_FORM["mailfirmante"]))
                {
                    mailfirmante = "eMail=" + _FORM["mailfirmante"];
                    LOG.Debug("SignerController.GetPatenteFromJson - Set => " + mailfirmante);
                    hayMailfirmante = true;
                }

                if (hayPatente && hayMailfirmante)
                {
                    sRet = patente + "|" + mailfirmante;
                }
                else if (hayPatente && !hayMailfirmante)
                {
                    sRet = patente;
                }
                else if (!hayPatente && hayMailfirmante)
                {
                    sRet = mailfirmante;
                }

                sRet = sRet + "|" + (tx.ResultCode==1 ? "POSITIVO=" : "NEGATIVO=") +
                                     Convert.ToDouble(tx.Score).ToString("##.##") + "%";

                //foreach (KeyValuePair<string, string> item in _FORM) 
                //{
                //    if (item.Key.Equals(_key))
                //    {
                //        keytoreturn = item.Value;
                //        break;
                //    }
                //}
            }
            catch (Exception ex)
            {
                LOG.Error("SignerController.GetPatenteFromJson Error: " + ex.Message);
            }
            LOG.Debug("SignerController.GetPatenteFromJson OUT! - Retorno = " + (string.IsNullOrEmpty(sRet) ? "Null" : sRet));
            return sRet;
        }

        private static int SendMsgToDestinatariesXError(string trackId)
        {
            int ret = 0;
            XmlParamOut oXmlOut;
            string msgerr;
            try
            {
                RdCITx tx = AdministradorRdCITx.GetCITxByTrackId(trackId, out msgerr);
                if (tx != null)
                {
                    //Added 11-09-2020 - Send mail HTMl Avisando error
                    LOG.Debug("SignerController.SendMsgToDestinatariesXError - Informo por mail " + tx.DestinataryMail + " a destinatario enviando error...");
                    string[] strbody = new string[2];
                    strbody[0] = "<b><font color=\"red\">Error [" + CIServiceManager.GetStatus(tx)
                                    + "]</b> completando el tramite para el documento <b>" +
                                    tx.TypeId + " " + tx.ValueId + "</b>";
                    strbody[1] = "El codigo unico del tramite para consultas es: <b>" + tx.TrackId + "</b>";
                    //byte[] docpdf = Convert.FromBase64String(tx.CertifyPDF);
                    //byte[] docxml = System.Text.Encoding.UTF8.GetBytes(tx.r);
                    //Send PDF

                    bool bret = libs.Utils.NVNotify(tx.DestinataryMail,
                        "Certificación de Identidad con ERROR [" + tx.ActionId.ToString() + "]", strbody, null, null, null);
                    LOG.Debug("SignerController.SendMsgToDestinatariesXError - Enviado mail error a " + tx.DestinataryMail + " => " + (bret.ToString()));
                    //bret = libs.Utils.NVNotify(oXmlOut.CIParam.DestinaryMail,
                    //                    "Certificación de Identidad completada con exito [XML]", strbody, docxml, "xml",
                    //                    oXmlOut.CIParam.TrackId + ".xml");
                    //LOG.Debug("Enviado XML => " + (bret.ToString()));

                    if (tx.ListMailsDistribution != null)
                    {

                        string[] _lDestinatary = tx.ListMailsDistribution.Split(';');

                        foreach (string item in _lDestinatary)
                        {
                            LOG.Debug("NotarizeController.SendCIToDestinatariesXError - Informo por mail " + item + " a destinatario enviando error...");
                            strbody[0] = "<b><font color=\"red\">Error [" + CIServiceManager.GetStatus(tx)
                                    + "]</b> completando el tramite para el documento <b>" +
                                    tx.TypeId + " " + tx.ValueId + "</b>";
                            strbody[1] = "El codigo unico del tramite para consultas es: <b>" + tx.TrackId + "</b>";
                            //docpdf = Convert.FromBase64String(tx.CertifyPDF);
                            //byte[] docxml = System.Text.Encoding.UTF8.GetBytes(tx.r);
                            //Send PDF
                            bret = libs.Utils.NVNotify(item,
                                    "Certificación de Identidad con ERROR [" + tx.ActionId.ToString() + "]", strbody, null, null, null);
                            LOG.Debug("SignerController.SendMsgToDestinatariesXError - Enviado mail error a " + item + " => " + (bret.ToString()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                LOG.Error("SignerController.SendMsgToDestinatariesXError - Error: " + ex.Message);
            }
            return ret;
        }

        internal static int GeneraResponseVerify(RdCITx tx, int flag, out TxSignerVerifyModelR oTxSignerVerifyModelR, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            oTxSignerVerifyModelR = null;

            try
            {
                LOG.Debug("SignerController.GeneraResponseVerify IN...");
                oTxSignerVerifyModelR = new TxSignerVerifyModelR();
                //PRimero chequeo status => Si no esta DONE => retorno status
                if (!CIServiceManager.GetStatus(tx).Equals("DONE"))
                {
                    oTxSignerVerifyModelR.code = 0;
                    oTxSignerVerifyModelR.msgerr = null;
                    oTxSignerVerifyModelR.trackid = tx.TrackId;
                    oTxSignerVerifyModelR.status = CIServiceManager.GetStatus(tx);
                    oTxSignerVerifyModelR.statusdescription = (tx != null ? CIServiceManager.GetStatusDescription(tx.ActionId) : null);
                }
                else //Si es DONE => lleno la info de acuerdo al flag
                {
                    oTxSignerVerifyModelR.code = 0;
                    oTxSignerVerifyModelR.msgerr = null;
                    oTxSignerVerifyModelR.trackid = tx.TrackId;
                    oTxSignerVerifyModelR.status = CIServiceManager.GetStatus(tx);
                    oTxSignerVerifyModelR.statusdescription = (tx != null ? CIServiceManager.GetStatusDescription(tx.ActionId) : null);
                    oTxSignerVerifyModelR.securitycode = tx.SecurityCode;
                    oTxSignerVerifyModelR.date = tx.DateGen;
                    oTxSignerVerifyModelR.infoverify = new Dictionary<string, string>();
                    oTxSignerVerifyModelR.infoverify.Add("typeid",tx.TypeId);
                    oTxSignerVerifyModelR.infoverify.Add("valueid",tx.ValueId.Substring(0,5) + "***");

                    string lastname = string.IsNullOrEmpty(tx.PhaterLastName) ? "NULL" : tx.PhaterLastName;
                    if (lastname.Length < 4) {
                        for (int i = 0; i < 4-lastname.Length; i++)
                        {
                            lastname += "*";
                        }
                    }
                    oTxSignerVerifyModelR.infoverify.Add("lastname", lastname.Substring(0, 4) + "***");
                    oTxSignerVerifyModelR.infoverify.Add("mail", libs.Utils.EnmascaraMail(tx.DestinataryMail));
                    Dictionary<string, string> _FORM = JsonConvert.DeserializeObject<Dictionary<string, string>>(tx.Form);
                    string patente = _FORM.ContainsKey("patentevehiculo") ? _FORM["patentevehiculo"] : "NULL";
                    oTxSignerVerifyModelR.infoverify.Add("carnumber",patente.Substring(0,4) + "***");
                    oTxSignerVerifyModelR.infoverify.Add("taxidcompany", (string.IsNullOrEmpty(tx.TaxIdCompany)?
                                                                          "NULL":tx.TaxIdCompany.Substring(0, 4) + "***"));

                }
            }
            catch (Exception ex)
            {
                ret = -1;
                oTxSignerVerifyModelR.code = -1;
                oTxSignerVerifyModelR.msgerr = "SignerController.GeneraResponseVerify Error: " + ex.Message;
                msgerr = oTxSignerVerifyModelR.msgerr;
                oTxSignerVerifyModelR.trackid = tx.TrackId;
                oTxSignerVerifyModelR.status = CIServiceManager.GetStatus(tx);
                oTxSignerVerifyModelR.statusdescription = (tx != null ? CIServiceManager.GetStatusDescription(tx.ActionId) : null);
                LOG.Error("SignerController.GeneraResponseVerify Error: " + ex.Message);
            }
            LOG.Debug("SignerController.GeneraResponseVerify - OUT! ret = " + ret);
            return ret;
        }

        internal static int GeneraResponseGet(RdCITx tx, int flag, out TxSignerModelR oTxSignerModelR, out string msgerr)
        {
            int ret = 0;
            msgerr = null;
            oTxSignerModelR = null;
            try
            {
                oTxSignerModelR = new TxSignerModelR();
                LOG.Debug("SignerController.GeneraResponseGet - IN...");
                //PRimero chequeo status => Si no esta DONE => retorno status
                if (!CIServiceManager.GetStatus(tx).Equals("DONE"))
                {
                    LOG.Debug("SignerController.GeneraResponseGet - No es Status = DONE => Sale...");
                    oTxSignerModelR.code = 0;
                    oTxSignerModelR.msgerr = null;
                    oTxSignerModelR.trackid = tx.TrackId;
                    oTxSignerModelR.status = CIServiceManager.GetStatus(tx);
                    oTxSignerModelR.statusdescription = (tx != null ? CIServiceManager.GetStatusDescription(tx.ActionId) : null);
                    LOG.Debug("SignerController.GeneraResponseGet - Status = " + oTxSignerModelR.status);
                }
                else //Si es DONE => lleno la info de acuerdo al flag
                {
                    LOG.Debug("SignerController.GeneraResponseGet - Lleno salida....");
                    oTxSignerModelR.code = 0;
                    oTxSignerModelR.msgerr = null;
                    oTxSignerModelR.trackid = tx.TrackId;
                    oTxSignerModelR.status = CIServiceManager.GetStatus(tx);
                    oTxSignerModelR.statusdescription = (tx != null ? CIServiceManager.GetStatusDescription(tx.ActionId) : null);
                    oTxSignerModelR.receiptId = tx.ReceiptId;
                    oTxSignerModelR.type = tx.Type; //0-CI | 1-Document TTag u otros
                    oTxSignerModelR.actionId = tx.ActionId;
                    oTxSignerModelR.dateGen = tx.DateGen;
                    oTxSignerModelR.lastModify = tx.LastModify;
                    oTxSignerModelR.resultCode = tx.ResultCode;
                    oTxSignerModelR.typeId = tx.TypeId;
                    oTxSignerModelR.valueId = tx.ValueId;
                    oTxSignerModelR.taxidcompany = tx.TaxIdCompany; //GetTaxIdCompany(tx.Form);
                    oTxSignerModelR.workstationid = tx.WorkstationId;
                    oTxSignerModelR.georef = tx.GeoRef;
                    oTxSignerModelR.score = tx.Score;
                    oTxSignerModelR.threshold = tx.Threshold;

                    if (flag > 0) //Adjunta datos personales y del form
                    {
                        LOG.Debug("SignerController.GeneraResponseGet - Cargo datos personales..:");
                        oTxSignerModelR.formdata = JsonConvert.DeserializeObject<Dictionary<string, string>>(tx.Form);
                        oTxSignerModelR.formdata.Add("rut", tx.ValueId);
                        oTxSignerModelR.formdata.Add("nombre", tx.Name + (string.IsNullOrEmpty(tx.Name) ? "" : tx.Name) + " " +
                             (string.IsNullOrEmpty(tx.PhaterLastName) ? "" : tx.PhaterLastName) + " " +
                             (string.IsNullOrEmpty(tx.MotherLastName) ? "" : tx.MotherLastName));
                        oTxSignerModelR.formdata.Add("serialcedula", tx.Serial);
                        oTxSignerModelR.formdata.Add("birthdate", tx.BirthDate);
                        oTxSignerModelR.formdata.Add("sex", tx.Sex);
                        oTxSignerModelR.formdata.Add("mail", tx.DestinataryMail);
                    }

                    if (flag > 1) //Adjunta imagenes
                    {
                        LOG.Debug("SignerController.GeneraResponseGet - Cargo imagenes...");
                        LOG.Debug("SignerController.GeneraResponseGet - Cargo Poder...");
                        oTxSignerModelR.document = tx.CertifyPDF;
                        ImageSample oSample;
                        oTxSignerModelR.Samples = new List<ImageSample>();
                        LOG.Debug("SignerController.GeneraResponseGet - Cargo imagenes cedulas...");
                        oTxSignerModelR.Samples.Add(new ImageSample("idcardimagefront", TypeImage.cedulafront, TypeImageFormat.jpg, tx.IdCardImageFront));
                        oTxSignerModelR.Samples.Add(new ImageSample("idcardimageback", TypeImage.cedulaback, TypeImageFormat.jpg, tx.IdCardImageBack));
                        LOG.Debug("SignerController.GeneraResponseGet - Cargo selfie y manualsignature...");
                        oTxSignerModelR.Samples.Add(new ImageSample("selfie", TypeImage.selfie, TypeImageFormat.jpg, tx.Selfie));
                        oTxSignerModelR.Samples.Add(new ImageSample("manualsignatureimage", TypeImage.manualsignature, TypeImageFormat.jpg, tx.ManualSignatureImage));
                        LOG.Debug("SignerController.GeneraResponseGet - Cargo padron front y back...");
                        oTxSignerModelR.Samples.Add(new ImageSample("carregisterimagefront", TypeImage.image, TypeImageFormat.jpg, tx.CarRegisterImageFront));
                        oTxSignerModelR.Samples.Add(new ImageSample("carregisterimageback", TypeImage.image, TypeImageFormat.jpg, tx.CarRegisterImageBack));

                        if (!string.IsNullOrEmpty(tx.WritingImage))
                        {
                            LOG.Debug("SignerController.GeneraResponseGet - Cargo escritura. Len = " + tx.WritingImage.Length + "...");
                            LOG.Debug("SignerController.GeneraResponseGet - Escritura = " + tx.WritingImage.Substring(0,10) + "...");
                            //Chequeo si tiene formato de URLData "[\"data:application/pdf;base64,JVBERi0x...
                            string mimetype = null;
                            string datab64 = null;
                            if (tx.WritingImage.StartsWith("[\"data:"))
                            {
                                mimetype = libs.Utils.GetMimeTypeFromURLData(tx.WritingImage, out datab64);
                            } else
                            {
                                mimetype = libs.Utils.GetMimeType(Convert.FromBase64String(tx.WritingImage));
                                datab64 = tx.WritingImage;
                            }

                                
                            if (mimetype.Equals("PDF"))
                            {
                                LOG.Debug("SignerController.GeneraResponseGet - Es PDF...");
                                oTxSignerModelR.Samples.Add(new ImageSample("writingimage", TypeImage.pdf, TypeImageFormat.pdf, datab64));
                            }
                            else
                            {
                                LOG.Debug("SignerController.GeneraResponseGet - Es imagen JPG...");
                                TypeImageFormat tyf = TypeImageFormat.jpg;
                                if (mimetype.Equals("image/png"))
                                {
                                    LOG.Debug("SignerController.GeneraResponseGet - Es imagen PNG...");
                                    tyf = TypeImageFormat.png;
                                }
                                else if (mimetype.Equals("image/bmp"))
                                {
                                    LOG.Debug("SignerController.GeneraResponseGet - Es imagen BMP...");
                                    tyf = TypeImageFormat.bmp;
                                }
                                oTxSignerModelR.Samples.Add(new ImageSample("writingimage", TypeImage.image, tyf, datab64));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                oTxSignerModelR.code = -1;
                oTxSignerModelR.msgerr = "SignerController.GeneraResponseGet Error: " + ex.Message;
                msgerr = oTxSignerModelR.msgerr;
                oTxSignerModelR.trackid = tx.TrackId;
                oTxSignerModelR.status = CIServiceManager.GetStatus(tx);
                oTxSignerModelR.statusdescription = (tx != null ? CIServiceManager.GetStatusDescription(tx.ActionId) : null);
                LOG.Error("SignerController.GeneraResponseGet Error: " + ex.Message);
            }
            LOG.Debug("SignerController.GeneraResponseGet - OUT! ret = " + ret);
            return ret;
        }

        internal static string GetTaxIdCompany(string form)
        {
            string strReturn = null;
            try
            {
                LOG.Debug("SignerController.GetTaxIdCompany IN...");
                Dictionary<string, string> dicForm = JsonConvert.DeserializeObject<Dictionary<string, string>>(form);
                if (dicForm.ContainsKey("rutempresa"))
                {
                    strReturn = dicForm["rutempresa"];
                }
            }
            catch (Exception ex)
            {
                strReturn = null;
                LOG.Error("SignerController.GetTaxIdCompany Error: " + ex.Message);
            }
            LOG.Debug("SignerController.GetTaxIdCompany OUT! strReturn = " + (string.IsNullOrEmpty(strReturn) ? "Null" : strReturn));
            return strReturn;
        }
    }
}