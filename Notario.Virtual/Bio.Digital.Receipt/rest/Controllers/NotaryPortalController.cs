﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bio.Digital.Receipt.Core.Database;
using Bio.Digital.Receipt.rest.Models;
using log4net;
using Newtonsoft.Json;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Core.Database.Notary.config;
using Bio.Digital.Receipt.Core.Helpers;
using System.Xml;
using System.Net.Http.Headers;
using System.Collections;
using System.IO;
using Bio.Digital.Receipt.Core.Services;
using Swashbuckle.Swagger.Annotations;

namespace Bio.Digital.Receipt.rest.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class NotaryPortalController : ApiController
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NotaryPortalController));

        #region Login & Register


        /// <summary>
        /// Permite cheuqeo de login 
        /// </summary>
        /// <remarks>
        /// Dado un mail y password, chequea acceso, y retorna datos de usuario incluido perfil. 
        /// Los pasos que se realizan son:
        ///   * 1.- Chequeo de user, si existe y no está bloqueado
        ///   * 2.- Coincidencia con password
        ///   * 3.- Retorna usuario con sus datos, principalmente el perfil para controlar las acciones en el frontend
        /// </remarks>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/notary/v2/login")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "LoginPortalNotaryModelR", typeof(LoginPortalNotaryModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object LoginNotaryPortal([FromBody] LoginPortalNotaryModel param)
        {
            NvNotaryUsers user = null;
            string msgerr = null;
            try
            {
                LOG.Debug("NotaryPortalController.LoginNotaryPortal - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotaryPortalController.LoginNotaryPortal - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                LOG.Debug("NotaryPortalController.LoginNotaryPortal - Check parametros...");
                if (param == null || string.IsNullOrEmpty(param.mail) || string.IsNullOrEmpty(param.password))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ResponseModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos Complete y reintente...")));
                }
                LoginPortalNotaryModel paramIn = param; //JsonConvert.DeserializeObject<LoginRequestModel>(strIn);

                //2) Reviso que el usuario esté dado de alta por mail
                //LOG.Debug("NotaryPortalController.LoginNotaryPortal - Recuperando user con mail = " + paramIn.mail + "...");
                //user = AdministradorNvNotaryUsers.BuscarNvNotaryUserByMail(paramIn.mail);

                //if (user == null)
                //{
                //    LOG.Error("NotarizeController.Login - Usuarios NO existe! - Por Mail = " + paramIn.mail);
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                //        (new ErrorModel(Errors.IRET_ERR_USER_NOT_EXIST, "Acceso NO Autorizado [" +
                //                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                //}
                //if (user.IsBlocked == 1)
                //{
                //    LOG.Error("NotarizeController.Login - Usuario bloqueado => " + paramIn.mail);
                //    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                //        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Usuario bloqueado", "Usuario bloqueado " +
                //                            (user.DateBlocked.HasValue ? "desde el " + user.DateBlocked.Value.ToString("dd/MM/yyyy") : ""))));
                //}

                //3) Valido Password
                LOG.Debug("NotaryPortalController.LoginNotaryPortal - Check Login user con mail = " + paramIn.mail + "...");
                int ret = AdministradorNvNotaryUsers.Login(paramIn.mail, paramIn.password, out msgerr, out user);

                if (ret < 0)
                {
                    LOG.Warn("NotaryPortalController.LoginNotaryPortal - Error validando login usuario [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, 
                            (new ErrorModel(ret, "Error validando login", 
                            "Error validando login usuario [" + msgerr + "]. Reintente...")));
                } else
                {
                    user.Password = null;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotaryPortalController.LoginNotaryPortal Error Desconcido procesando validacion de login : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, 
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido", 
                                "Error Desconocido procesando validacion de login [" + ex.Message + "]")));
            }
            LOG.Debug("NotaryPortalController.LoginNotaryPortal OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, new LoginPortalNotaryModelR(200, user)); //(new ResponseModel(200, "Login verificado con exito!"));

        }

        #endregion Login & Register

        #region Procedures

        /// <summary>
        /// Retorna listado de tramites (NvUserProcedures) que cumplan con el filtro enviado.
        /// </summary>
        /// <remarks>
        /// Los posibles son:
        ///   * 1.- int notaryid: Obligatorio para filtar por la notaria en cuestión. 
        ///   * 2.- string trackid: [Opcional] por si se desea un único procedure (Idem que el uso de api/notary/v2/get) Si este valor no es nulo, se ignora rango de fechas.
        ///   * 3.- string mail: [Opcional] Del user que inicio el trámite
        ///   * 4.- string mailnotary: [Opcional] De un funcionario de la notaria que tomo o reviso el tramite (ver si lo usamos)
        ///   * 5.- DateTime from: [Opcional] Si llega nulo junto a till, se toman 3 dias desde la fecha actual para atras.
        ///   * 6.- DateTime till: [Opcional] Si diferencia entra till - from es mas de 15 dias, se ajusta a 15 dias.
        ///   * 7.- int status: [Opcional] Si se necesita por status, se envia sino 0 para todos.
        ///   * 8.- int withdetails: 0-Liviano | 1-withdetails. Esto incluye las evidencias y sus estados o las elimina. Para grillas generales es recoendable 0-Liviano para que sea mas rápido el proceso.
        ///  
        /// Los estados posibles para filtrar son:
        ///   * int STATUS_MAIL_VERIFY_PENDING = 0
        ///   * int STATUS_CI_PENDING = 1
        ///   * int STATUS_PAYMENT_PENDING = 2
        ///   * int STATUS_READY_TO_REVISION_NOTARIZE = 3
        ///   * int STATUS_READY_TO_NOTARIZE = 4
        ///   * int STATUS_IN_PROCESS_NOTARIZING = 5
        ///   * int STATUS_REJECTED = 6
        ///   * int STATUS_NOTARIZED = 7
        ///   
        ///  Los estados que pueden operar desde este portal son desde el 3 en adelante. Hasta el 3, es solo informativo para este portal. 
        /// </remarks>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/notary/v2/list")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "ListProceduresNotaryModelR", typeof(ListProceduresNotaryModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object ListProceduresPortalNotary([FromBody] ListProceduresNotaryModel param)
        {
            List<NvUsersProcedures> listProcedures = null;
            string msgerr = null;
            List<Procedure> listProceduresRet = null;
            try
            {
                LOG.Debug("NotaryPortalController.ListProceduresPortalNotary - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotaryPortalController.ListProceduresPortalNotary - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                LOG.Debug("NotaryPortalController.ListProceduresPortalNotary - Check parametros...");
                if (param == null || param.notaryid <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos o incorrectos. Complete y reintente...",
                                                "Parametros nulos o incorrectos. Complete y reintente..")));
                }

                //2) Chequeo parametros y seteo filtros
                if (param.till == null)  //Si viene nuelo ambos o uno diferencia de 3 dias solo
                {
                    param.till = DateTime.Now;
                    param.from = param.till.AddDays(-3);
                } else if (param.till != null && param.from == null)
                {
                    param.from = param.till.AddDays(-3);
                } else if (param.till.AddDays(-15) > param.from)  //No dejo mas de 15 dias de rango
                {
                    param.from = param.till.AddDays(-15);
                }
                ListProceduresNotaryModel paramIn = param; //JsonConvert.DeserializeObject<LoginRequestModel>(strIn);

                //3) Recupero procedures
                LOG.Debug("NotaryPortalController.ListProceduresPortalNotary - Recupero procedures para NotaryId =  " + 
                            param.notaryid.ToString() + " | Desde = " + param.from.ToString("dd/MM/yyyy HH:mm:ss") + 
                            " - Hasta = " + param.till.ToString("dd/MM/yyyy HH:mm:ss") + " | status = " + paramIn.status.ToString() +
                            " | trackid = " + (string.IsNullOrEmpty(paramIn.trackid)?"Null": paramIn.trackid) +
                            " | mail cliente = " + (string.IsNullOrEmpty(paramIn.trackid) ? "Null" : paramIn.trackid));
                int ret = AdministradorNvUsersProcedures.GetNvUsersProcedureByFilter(paramIn.notaryid, paramIn.mailnotary, 
                                                                paramIn.mail, paramIn.from, paramIn.till, paramIn.trackid,
                                                                paramIn.status, out msgerr, out listProcedures, 
                                                                (paramIn.withdetails==1));

                if (ret < 0)
                {
                    LOG.Error("NotaryPortalController.ListProceduresPortalNotary - Error recuperando tramites [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                            (new ErrorModel(ret, "Error recuperando tramites",
                            "Error recuperando tramites [" + msgerr + "]. Reintente...")));
                } else
                {
                    LOG.Error("NotaryPortalController.ListProceduresPortalNotary - Ingresando ConvertList..."); 
                    listProceduresRet = NotaryPortalUnitOfWork.ConvertList(listProcedures, paramIn.withdetails);
                    LOG.Error("NotaryPortalController.ListProceduresPortalNotary - ConvertList OUT!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotaryPortalController.ListProceduresPortalNotary - Error Desconcido procesando recuperacion de tramites : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
                                "Error Desconcido procesando recuperacion de tramites [" + ex.Message + "]")));
            }
            LOG.Debug("NotaryPortalController.ListProceduresPortalNotary OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, new ListProceduresNotaryModelR(listProceduresRet));
        }

        /// <summary>
        /// Recupera los datos de un procedure
        /// </summary>
        /// <remarks>
        /// Recupera los datos de un procedure con detalles (lista de evidencias y estados incluido). Es el mismo resultado 
        /// que si se usa api/notary/v2/list y se envia como filtro el trackid como filtro.
        /// </remarks>
        /// <param name="trackid"></param>
        /// <returns></returns>
        [Route("api/notary/v2/get")]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, "ListProceduresNotaryModelR", typeof(ListProceduresNotaryModelR))]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object GetProcedure(string trackid)
        {
            int ret = 0;
            List<NvUsersProcedures> listProcedures = null;
            string msgerr = null;
            List<Procedure> listProceduresRet = null;
            try
            {
                LOG.Debug("NotaryPortalController.GetProcedure - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotaryPortalController.GetProcedure - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros     
                LOG.Debug("NotaryPortalController.GetProcedure - Check parametros...");
                if (string.IsNullOrEmpty(trackid))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros nulos o incorrectos. Complete y reintente...",
                                                "Parametros nulos o incorrectos. Complete y reintente..")));
                }

                //3) Recupero procedures
                LOG.Debug("NotaryPortalController.GetProcedure - Recupero procedures con trackid = " + trackid);
                NvUsersProcedures proc = AdministradorNvUsersProcedures.GetNvUsersProcedureByTrackId(trackid, true);

                if (proc == null)
                {
                    ret = -1;
                    LOG.Error("NotaryPortalController.GetProcedure - Error recuperando tramites [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                            (new ErrorModel(ret, "Error recuperando tramites",
                            "Error recuperando tramites [" + msgerr + "]. Reintente...")));
                }
                else
                {
                    LOG.Error("NotaryPortalController.GetProcedure - Ingresando ConvertList...");
                    listProcedures = new List<NvUsersProcedures>();
                    listProcedures.Add(proc);
                    listProceduresRet = NotaryPortalUnitOfWork.ConvertList(listProcedures);
                    LOG.Error("NotaryPortalController.GetProcedure - ConvertList OUT!");
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NotaryPortalController.GetProcedure - Error Desconcido procesando recuperacion de tramites : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
                                "Error Desconcido procesando recuperacion de tramites [" + ex.Message + "]")));
            }
            LOG.Debug("NotaryPortalController.GetProcedure OUT!");
            return Request.CreateResponse(HttpStatusCode.OK, new ListProceduresNotaryModelR(listProceduresRet));
        }

        /// <summary>
        /// Modifica el procedure para Aprobacion o Rechazo por parte del funcionario.
        /// </summary>
        /// <remarks>
        /// Se envía el trackid del procedur, status de aprobado o rechazado y el texto adecuado segun status:
        /// * 1.- string trackid: Obligatorio
        /// * 2.- int status: 1-Aprobado | 2-Rechazado
        /// * 3.- string texttoinclude: Texto para incluir en documento generado si status = 1
        /// * 4.- string reasonreject: Motivo del rechazo si status = 2
        /// </remarks>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("api/notary/v2/modifyrevision")]
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK)]
        [SwaggerResponse(HttpStatusCode.NotFound, "ErrorModel", typeof(ErrorModel))]
        public object ModifyRevisionProcedure([FromBody] ModifyProcedureNotaryModel param)
        {
            int ret = Errors.IRET_ERR_DESCONOCIDO;
            List<NvUsersProcedures> listProcedures = null;
            string msgerr = null;
            List<Procedure> listProceduresRet = null;
            try
            {
                LOG.Debug("NotaryPortalController.ModifyRevisionProcedure - IN...");
                //0) Check Basic Authentication
                if (!NotarizeController.Authorized(Request, out msgerr))
                {
                    LOG.Warn("NotaryPortalController.ModifyRevisionProcedure - No Autorizado!");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized,
                        (new ErrorModel(Errors.IRET_API_ERR_USER_BLOCKED, "Acceso NO Autorizado [" +
                                        (string.IsNullOrEmpty(msgerr) ? "" : msgerr) + "]", null)));
                }

                //1) Deserializo parámetros y controlo consistencia  
                LOG.Debug("NotaryPortalController.ModifyRevisionProcedure - Check parametros...");
                if (param == null || (param.status != 1 && param.status != 2))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametro nulo o incorrectos. Complete y reintente...",
                                                "Parametro nulo o incorrectos. Complete y reintente..")));
                } else if ((param.status == 1 && string.IsNullOrEmpty(param.texttoinclude)) ||
                           (param.status == 2 && string.IsNullOrEmpty(param.reasonreject)))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                              (new ErrorModel(Errors.IRET_ERR_PARAM_INCORRECTOS, "Parametros incorrectos. Complete y reintente...",
                                                "Parametros incorrectos. Complete y reintente..")));
                }

                //3) Recupero procedures
                LOG.Debug("NotaryPortalController.ModifyRevisionProcedure - Modifico procedures con trackid = " + param.trackid);
                ret = NotaryPortalUnitOfWork.ModifyRevisionProcedure(param, out msgerr);

                if (ret < 0)
                {
                    LOG.Error("NotaryPortalController.ModifyRevisionProcedure - Error modificando tramite con trackid = "
                               + param.trackid + " [" + msgerr + "]");
                    return Request.CreateResponse(HttpStatusCode.Conflict,
                            (new ErrorModel(ret, "Error recuperando tramites",
                            "Error recuperando tramites [" + msgerr + "]. Reintente...")));
                }
                //else
                //{
                //    LOG.Error("NotaryPortalController.ListProceduresPortalNotary - Ingresando ConvertList...");
                //    listProceduresRet = NotaryPortalUnitOfWork.ConvertList(listProcedures);
                //    LOG.Error("NotaryPortalController.ListProceduresPortalNotary - ConvertList OUT!");
                //}
            }
            catch (Exception ex)
            {
                ret = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("NotaryPortalController.ModifyRevisionProcedure - Error Desconcido procesando modificacion de tramite desde notaria : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                            (new ErrorModel(Errors.IRET_ERR_DESCONOCIDO, "Error Desconocido",
                                "Error Desconcido procesando modificacion de tramite desde notaria [" + ex.Message + "]")));
            }
            LOG.Debug("NotaryPortalController.ModifyRevisionProcedure OUT!");
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        //[Route("api/notary/v2/download")]
        //[HttpGet]
        //[SwaggerResponse(HttpStatusCode.OK, "ProceduresPortalNotaryModelR", typeof(ProceduresPortalNotaryModelR))]
        //[SwaggerResponse(HttpStatusCode.NotFound, "Error", typeof(Error))]
        //public object Download([FromBody] ProceduresPortalNotaryModel param)
        //{

        //}



        #endregion Procedures

        #region basico
        // GET: api/NotaryPortal
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/NotaryPortal/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/NotaryPortal
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/NotaryPortal/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/NotaryPortal/5
        //public void Delete(int id)
        //{
        //}
        #endregion basico
    }
}
