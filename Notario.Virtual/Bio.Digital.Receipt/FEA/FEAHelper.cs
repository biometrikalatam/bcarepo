﻿using Bio.Digital.Receipt.Api;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.FEA
{
    public class FEAHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(FEAHelper));

        internal static int SignCloud(int provider, string name, int idapp, string xmltosign, out string xmlsigned)
        {
            int ret = 0;
            xmlsigned = null;

            try
            {
                string urlfea = ((Core.Helpers.RdAplicacionesMemory)(Global.HASHTABLE_RDAPPLICATION[idapp])).RdApp.URLFEA;
                string appfea = ((Core.Helpers.RdAplicacionesMemory)(Global.HASHTABLE_RDAPPLICATION[idapp])).RdApp.APPFEA;
                string userfea = ((Core.Helpers.RdAplicacionesMemory)(Global.HASHTABLE_RDAPPLICATION[idapp])).RdApp.USERFEA;
                string pswfea = ((Core.Helpers.RdAplicacionesMemory)(Global.HASHTABLE_RDAPPLICATION[idapp])).RdApp.SignaturePsw;

                string straux = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + xmltosign.Substring(xmltosign.IndexOf("<Rd"));
                byte[] byFile = System.Text.Encoding.UTF8.GetBytes(straux);
                string strFile = Convert.ToBase64String(byFile);
                using (cl.servisign.esigner6.WSIntercambiaDocSoapService wssigner = new cl.servisign.esigner6.WSIntercambiaDocSoapService())
                {
                    wssigner.Url = urlfea;
                    wssigner.Timeout = 60000;
                    cl.servisign.esigner6.intercambiaDocEncabezado enc = new cl.servisign.esigner6.intercambiaDocEncabezado();
                    enc.User = userfea;  //"biometrika"
                    enc.Password = pswfea; //"1234"
                    enc.NombreConfiguracion = appfea; // "BIOXML454";

                    cl.servisign.esigner6.intercambiaDocParametro param = new cl.servisign.esigner6.intercambiaDocParametro();
                    param.Documento = strFile;
                    param.NombreDocumento = name;
                    param.MetaData = null;

                    cl.servisign.esigner6.intercambiaDocResponseIntercambiaDoc resp = wssigner.intercambiaDoc(enc, param);

                    if (resp != null)
                    {
                        if (resp.IntercambiaDocResult.Estado.Equals("OK"))
                        {
                            xmlsigned = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(resp.IntercambiaDocResult.Documento));
                            //System.IO.FileStream fs1 = new System.IO.FileStream(resp.IntercambiaDocResult.NombreDocumento, System.IO.FileMode.CreateNew);
                            //byte[] byArrResp = Convert.FromBase64String(resp.IntercambiaDocResult.Documento);
                            //fs1.Write(byArrResp, 0, byArrResp.Length);
                            //fs1.Close();
                        }
                        else
                        {
                            LOG.Error("Error FEA = " + resp.IntercambiaDocResult.Comentarios);
                            ret = Errors.IRET_ERR_SIGNATURE;
                        }
                    }
                    else
                    {
                        LOG.Error("Error FEA = Rpta NULL");
                        ret = Errors.IRET_ERR_SIGNATURE;
                    }
                }
            }
            catch (Exception ex)
            {
                LOG.Error("FEAHelper.SignCloud Error = " + ex.Message);
                ret = Errors.IRET_ERR_DESCONOCIDO;
            }


            return ret;
        }
    }
}