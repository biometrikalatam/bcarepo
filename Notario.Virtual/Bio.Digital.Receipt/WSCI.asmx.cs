﻿using log4net;
using System;
using System.Web.Services;
using Bio.Digital.Receipt.Core.Services;
using Bio.Digital.Receipt.Api;

namespace Bio.Digital.Receipt
{
    /// <summary>
    /// Summary description for WSCI
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSCI : System.Web.Services.WebService
    {

        private static readonly ILog LOG = LogManager.GetLogger(typeof(WSCI));

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlparamin"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        [WebMethod]
        public int Process(string xmlparamin, out string xmlparamout)
        {
            xmlparamout = null;
            XmlParamOut oXmlOut = new XmlParamOut();
            int res = -1;
            string msgerr = null;
            string namedebug = "";

            try
            {
                 //1.- Deserializo objeto xmlparamin
                LOG.Debug("WSCI.Process Arrived xmlparamin = " + xmlparamin);

                XmlParamIn oXmlIn = Api.XmlUtils.DeserializeObject<XmlParamIn>(xmlparamin);
                if (oXmlIn == null)
                {
                    oXmlOut.Message = "Error deserializando parametros de entrada";
                    LOG.Error("WSCI.Process Error - " + oXmlOut.Message);
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }

                //2.- Chequeo que esté toda la información necesaria sino retorno con ERR
                if (!CIServiceManager.IsCorrectParamInForAction(oXmlIn, out msgerr))
                {
                    oXmlOut.Message = "Error de parametros para la accion solicidata [" +
                        msgerr + "]";
                    LOG.Error("WSCI.Process Error - " + oXmlOut.Message);
                    xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                    return Errors.IRET_ERR_PARAM_INCORRECTOS;
                }

                //3.- Llamo a ServicesManager.Process con parametros deserializados.
                res = CIServiceManager.Process(oXmlIn, out xmlparamout);
                LOG.Debug("WSCI.Process ret = " + res + " - xmlparamout = " + xmlparamout);
            }
            catch (Exception ex)
            {
                res = Errors.IRET_ERR_DESCONOCIDO;
                oXmlOut.Message = "Error Desconocido - ex = " + ex.Message;
                xmlparamout = Api.XmlUtils.SerializeObject<XmlParamOut>(oXmlOut);
                LOG.Error("WSCI.Process", ex);
            }
            //LOG.Debug("FEM.Process Returned res = " + res.ToString() + " - xmlparamout = " + oXmlOut);
            //if (Properties.Settings.Default.DebugVideoImagen > 0)
            //{
            //    FEMServiceManager.SaveDebugData("XmlParamOut_" + namedebug, xmlparamout, 3);
            //}
            return res;
        }



        /// <summary>
        /// Cehquea status de CI, para control desde servicios de terceros
        /// </summary>
        /// <param name="xmlparamin"></param>
        /// <param name="xmlparamout"></param>
        /// <returns></returns>
        [WebMethod]
        public int CheckStatusCI(string trackid)
        {
           
            int res = -1;
            string msgerr = null;
            string namedebug = "";

            try
            {
                //1.- Deserializo objeto xmlparamin
                LOG.Debug("WSCI.CheckStatusCI Arrived trackid = " + trackid);

                //3.- Llamo a ServicesManager.Process con parametros deserializados.
                res = CIServiceManager.CheckStatusCI(trackid);
                LOG.Debug("WSCI.Process ret = " + res);
            }
            catch (Exception ex)
            {
                res = Errors.IRET_ERR_DESCONOCIDO;
                LOG.Error("WSCI.CheckStatusCI", ex);
            }
            return res;
        }
    }
}
