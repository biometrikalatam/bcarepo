﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bio.Digital.Receipt.Api
{
    public class Errors
    {
        #region Constantes

        public static int IRET_OK = 0;

        public static int IRET_ERR_DESCONOCIDO = -1;
        public static string SRET_ERR_DESCONOCIDO = "Error desconocido";

        public static int IRET_ERR_PARAM_INCORRECTOS = -2;
        public static string SRET_ERR_PARAM_INCORRECTOS = "Los parametros ingresados son incorrectos";

        public static int IRET_ERR_SCHEMA_RD_INVALID = -3;
        public static int IRET_ERR_SAVE_DB = -4;
        public static string SRET_ERR_SAVE_DB = "Error grabando en base de datos";

        public static int IRET_ERR_RECEIPT_NO_IN_DB = -5;
        public static string SRET_ERR_RECEIPT_NO_IN_DB = "Receipt ingresado no existe en base de datos";

        public static int IRET_ERR_COMPANY_NOT_EXIST = -6;
        public static string SRET_ERR_COMPANY_NOT_EXIST = "Company no existe";

        public static int IRET_ERR_COMPANY_DISABLED = -7;
        public static string SRET_ERR_COMPANY_DISABLED = "Company deshabilitada";

        public static int IRET_ERR_APLICATION_NOT_EXIST = -8;
        public static string SRET_ERR_APLICATION_NOT_EXIST = "Aplicación inexistente";

        public static int IRET_ERR_APLICATION_DISABLED = -9;
        public static string SRET_ERR_APLICATION_DISABLED = "Aplicación deshabilitada";

        public static int IRET_ERR_SIGNATURE = -20;
        public static string SRET_ERR_SIGNATURE = "Error en firma";

        public static int IRET_ERR_SIGNATURE_INVALID = -21;
        public static string SRET_ERR_SIGNATURE_INVALID = "Firma Invalida";

        public static int IRET_ERR_PROCESSING_PASSWORD = -22;
        public static string SRET_ERR_PROCESSING_PASSWORD = "Error procesando clave";

        public static int IRET_ERR_LOGIN = -23;
        public static string SRET_ERR_LOGIN = "Error en el login. Password no coincide";

        public static int IRET_ERR_TSA = -40;
        public static string SRET_ERR_TSA = "Error TSA";

        public static int IRET_ERR_ADDINGWATERMARK = -101;
        public static string SRET_ERR_ADDINGWATERMARK = "Agregando WaterMark";

        #region 400 - CI Error

        public static int IRET_ERR_GENERATINGQR = -401;
        public static string SRET_ERR_GENERATINGQR = "Generando QR de conexion con CI";

        public static int IRET_ERR_CI_NO_IN_DB = -402;
        public static string SRET_ERR_CI_NO_IN_DB = "CI no se encuentra en BD";

        public static int IRET_ERR_CI_PDF_NO_IN_DB = -403;
        public static string SRET_ERR_CI_PDF_NO_IN_DB = "CI PDF no se encuentra en BD";

        public static int IRET_ERR_CI_TRACKID_NO_IN_DB = -404;
        public static string SRET_ERR_CI_TRACKID_NO_IN_DB = "Track Id de no se encuentra en BD";

        public static int IRET_ERR_CI_VERYFING = -405;
        public static string SRET_ERR_CI_VERYFING = "Error verificando identidad";

        public static int IRET_ERR_CI_NEGATIVE_VERIFICATION = -406;
        public static string SRET_ERR_CI_NEGATIVE_VERIFICATION = "Verificacion Negativa de identidad";

        public static int IRET_ERR_CI_PDF_GENERATING = -407;
        public static string SRET_ERR_CI_PDF_GENERATING = "Error generando certificado PDF";

        public static int IRET_ERR_CI_IDCARD_PARSING = -408;
        public static string SRET_ERR_CI_IDCARD_PARSING = "Error parsing OCR";

        public static int IRET_ERR_CI_ANSWER_BP_NULL = -409;
        public static string SRET_ERR_CI_ANSWER_BP_NULL = "Respuesta BioPortal Nula";

        public static int IRET_ERR_CI_NO_COMPLETE = -410;
        public static string SRET_ERR_CI_NO_COMPLETE = "CI aun no completa";

        public static int IRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED = -411;
        public static string SRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED = "Error reconociendo OCR por mala calidad de imagen de cedula";

        public static int IRET_ERR_CI_TRACKID_COMPLETED = -412;
        public static string SRET_ERR_CI_TRACKID_COMPLETED = "Track Id completada con anterioridad";

        public static int IRET_ERR_CI_NO_COMPLETE_DEFEATED = -413;
        public static string SRET_ERR_CI_NO_COMPLETE_DEFEATED = "CI aun no completa vencida";

        public static int IRET_ERR_CI_VALUEID_INCORRECT = -414;
        public static string SRET_ERR_CI_VALUEID_INCORRECT = "ValueId incorrecto o diferente a configurado";

        public static int IRET_ERR_CI_DECLARACIONJURADA_INCORRECT = -415;
        public static string SRET_ERR_CI_DECLARACIONJURADA_INCORRECT = "Declaracion Jurada Incorrecta";

        public static int IRET_ERR_CI_TX_WEB_CREATE = -416;
        public static string SRET_ERR_CI_TX_WEB_CREATE = "Creacion CI Web Tx";

        public static int IRET_ERR_CI_TX_SEND_NOTIFY = -417;
        public static string SRET_ERR_CI_TX_SEND_NOTIFY = "Envio Notificacion CI Web";

        public static int IRET_ERR_WORKFLOW_DONT_EXIST = -418;
        public static string SRET_ERR_WORKFLOW_DONT_EXIST = "Workflow no existe o error recuperando";

        public static int IRET_ERR_DONT_CHECK_ENROLLED = -419;
        public static string SRET_ERR_DONT_CHECK_ENROLLED = "Error chequeando enrolled (Response null)";

        public static int IRET_ERR_CI_IDCARD_INCONSISTENCE_DATA_OCR_RECOGNIZED = -421;
        public static string SRET_ERR_CI_IDCARD_INCONSISTENCE_DATA_OCR_RECOGNIZED = "Inconsistencia entre pedido y reconocido";

        #endregion 400 - CI Error

        #region 500 - NV Error
        /*
        Continue = 100,
        SwitchingProtocols = 101,
        OK = 200,
        Created = 201,
        Accepted = 202,
        NonAuthoritativeInformation = 203,
        NoContent = 204,
        ResetContent = 205,
        PartialContent = 206,
        MultipleChoices = 300,
        Ambiguous = 300,
        MovedPermanently = 301,
        Moved = 301,
        Found = 302,
        Redirect = 302,
        SeeOther = 303,
        RedirectMethod = 303,
        NotModified = 304,
        UseProxy = 305,
        Unused = 306,
        TemporaryRedirect = 307,
        RedirectKeepVerb = 307,
        BadRequest = 400,
        Unauthorized = 401,
        PaymentRequired = 402,
        Forbidden = 403,
        NotFound = 404,
        MethodNotAllowed = 405,
        NotAcceptable = 406,
        ProxyAuthenticationRequired = 407,
        RequestTimeout = 408,
        Conflict = 409,
        Gone = 410,
        LengthRequired = 411,
        PreconditionFailed = 412,
        RequestEntityTooLarge = 413,
        RequestUriTooLong = 414,
        UnsupportedMediaType = 415,
        RequestedRangeNotSatisfiable = 416,
        ExpectationFailed = 417,
        UpgradeRequired = 426,
        InternalServerError = 500,
        NotImplemented = 501,
        BadGateway = 502,
        ServiceUnavailable = 503,
        GatewayTimeout = 504,
        HttpVersionNotSupported = 505 
        */
        public static int IRET_API_OK = 200;
        public static string SRET_API_OK = "Retorno OK";

        public static int IRET_API_CREATED = 201;
        public static string SRET_API_CREATED = "Recurso Creado OK";

        public static int IRET_API_BAD_REQUEST = 400;
        public static string SRET_API_BAD_REQUEST = "Llamada mal formada";

        public static int IRET_API_UNAUTHORIZED = 401;
        public static string SRET_API_UNAUTHORIZED = "Usuario no autorizado";

        public static int IRET_API_ERR_USER_BLOCKED = 1402;
        public static string SRET_API_ERR_USER_BLOCKED = "Usuario bloqueado!";

        public static int IRET_API_ERR_USER_EXIST = 1403;
        public static string SRET_API_ERR_USER_EXIST = "Usuario ya existe!";

        public static int IRET_API_ERR_NOT_FOUND = 1404;
        public static string SRET_API_ERR_NOT_FOUND = "Recurso no disponible";

        public static int IRET_API_METHOD_NOT_ALLOWED = 1405;
        public static string SRET_API_METHOD_NOT_ALLOWED = "Metodo no permitido";

        public static int IRET_ERR_USER_NOT_EXIST = 1406;
        public static string SRET_ERR_USER_NOT_EXIST = "Usuario NO existe!";

        public static int IRET_ERR_MAIL_VALIDATED = 1407;
        public static string SRET_ERR_MAIL_VALIDATED = "Mail ya validado!";

        public static int IRET_ERR_PREREGISTER_NOT_EXIST = 1408;
        public static string SRET_ERR_PREREGISTER_NOT_EXIST = "Pre Registro NO existe!";

        public static int IRET_API_CONFLIC = 409;
        public static string SRET_API_CONFLICT = "Conflicto en la operacion";

        public static int IRET_API_ERR_INIT_CI = 1409;
        public static string SRET_API_ERR_INIT_CI = "Error iniciando transaccion de CI";

        public static int IRET_API_ERR_CODE = 1410;
        public static string SRET_API_ERR_CODE = "Codigo erroneo";

        public static int IRET_API_ERR_CI_COMPLETED = 1411;
        public static string SRET_API_ERR_CI_COMPLETED = "Transaccion CI ya completa. No se puede reiniciar";

        public static int IRET_API_ERR_CI_EXPIRED = 1412;
        public static string SRET_API_ERR_CI_EXPIRED = "Transaccion CI ya expirada. Inicie otro proceso";

        public static int IRET_API_INTERNAL_SERVER_ERROR = 500;
        public static string SRET_API_INTERNAL_SERVER_ERROR = "";

        public static int IRET_ERR_NVPROCEDURE_NO_SAVED = 1501;
        public static string SRET_ERR_NVPROCEDURE_NO_SAVED = "NvUserProcedure no grabada en BD";

        public static int IRET_ERR_NVCONFIG_NO_CONFIGURED = 1502;
        public static string SRET_ERR_NVCONFIG_NO_CONFIGURED = "NvConfig no configurado!";

        public static int IRET_ERR_CALLING_PROVIDER_SERVICE = 1503;
        public static string SRET_ERR_CALLING_PROVIDER_SERVICE = "Error llamando al provider en su servicio";

        public static int IRET_ERR_INFORMED_PROVIDER_SERVICE = 1504;
        public static string SRET_ERR_INFORMED_PROVIDER_SERVICE = "Error informado por el provider en notarizacion";

        public static int IRET_ERR_USER_PROCEDURE_NOT_FOUND = 1505;
        public static string SRET_ERR_USER_PROCEDURE_NOT_FOUND = "Tramite no encontrado en BD";

        public static int IRET_ERR_PROCEDURES_NO_CONFIGURED = 1506;
        public static string SRET_ERR_PROCEDURES_NO_CONFIGURED = "Porcedures no configurados!";

        public static int IRET_ERR_PROCEDURE_NO_READY_TO_NOTARIZE = 1507;
        public static string SRET_ERR_PROCEDURE_NO_READY_TO_NOTARIZE = "Porcedure no listo para notarizar!";

        public static int IRET_ERR_PROCEDURE_PENDING = 1508;
        public static string SRET_ERR_PROCEDURE_PENDING = "Porcedure pendiente de notarizar!";

        public static int IRET_ERR_CREATING_PAYMENT_ORDER = -416;
        public static string SRET_ERR_CREATING_PAYMENT_ORDER = "Error Creando Orden de Pago";

        public static int IRET_ERR_CONSULTING_STATUS_PAYMENT_ORDER = -417;
        public static string SRET_ERR_CONSULTING_STATUS_PAYMENT_ORDER = "Error consultando status Orden de Pago";

        public static int IRET_ERR_STATUS_PAYMENT_INCORRECT = -418;
        public static string SRET_ERR_STATUS_PAYMENT_INCORRECT = "Estado de pago incorrecto";

        public static int IRET_ERR_DESERIALIZE_RESPONSE = -419;
        public static string SRET_ERR_DESERIALIZE_RESPONSE = "Error deserializando respuesta";

        public static int IRET_ERR_SERIALIZE_RESPONSE = -420;
        public static string SRET_ERR_SERIALIZE_RESPONSE = "Error serializando respuesta";

        public static int IRET_ERR_PROCEDURE_NOTFOUND = -421;
        public static string SRET_ERR_PROCEDURE_NOTFOUND = "Error tramite no existente";

        public static int IRET_ERR_INVALID_PASSWORD = -422;
        public static string SRET_ERR_INVALID_PASSWORD = "Clave invalida";


        #endregion 500 - NV Error

        #region 600 - API Error

        static public int IERR_SERIALIZE_XMLPARAMIN = -601;
        static public string SERR_SERIALIZE_XMLPARAMIN = "Error al serializar XmlParamIn";

        static public int IERR_DESERIALIZE_XMLPARAMOUT = -602;
        static public string SERR_DESERIALIZE_XMLPARAMOUT = "Error al deserializar XmlParamOut";

        #endregion 600 - API Error 

        #endregion Constantes

        static public string GetDescription(int error)
        {
            switch (error)
            {
                case -1:
                    return SRET_ERR_DESCONOCIDO;
                case -2:
                    return SRET_ERR_PARAM_INCORRECTOS;
                case -4:
                    return SRET_ERR_SAVE_DB;
                case -5:
                    return SRET_ERR_RECEIPT_NO_IN_DB;
                case -6:
                    return SRET_ERR_COMPANY_NOT_EXIST;
                case -7:
                    return SRET_ERR_COMPANY_DISABLED;
                case -8:
                    return SRET_ERR_APLICATION_NOT_EXIST;
                case -9:
                    return SRET_ERR_APLICATION_DISABLED;

                case -20:
                    return SRET_ERR_SIGNATURE;
                case -21:
                    return SRET_ERR_SIGNATURE_INVALID;

                case -40:
                    return SRET_ERR_TSA;

                case -101:
                    return SRET_ERR_ADDINGWATERMARK;

                case -401:
                    return SRET_ERR_GENERATINGQR;
                case -402:
                    return SRET_ERR_CI_NO_IN_DB;
                case -403:
                    return SRET_ERR_CI_PDF_NO_IN_DB;
                case -404:
                    return SRET_ERR_CI_TRACKID_NO_IN_DB;
                case -405:
                    return SRET_ERR_CI_VERYFING;
                case -406:
                    return SRET_ERR_CI_NEGATIVE_VERIFICATION;
                case -407:
                    return SRET_ERR_CI_PDF_GENERATING;
                case -408:
                    return SRET_ERR_CI_IDCARD_PARSING;
                case -409:
                    return SRET_ERR_CI_ANSWER_BP_NULL;
                case -410:
                    return SRET_ERR_CI_NO_COMPLETE;
                case -411:
                    return SRET_ERR_CI_IDCARD_BAD_OCR_RECOGNIZED;
                case -412:
                    return SRET_ERR_CI_TRACKID_COMPLETED;
                case -413:
                    return SRET_ERR_CI_NO_COMPLETE_DEFEATED;
                case -414:
                    return SRET_ERR_CI_VALUEID_INCORRECT;
                 case -415:
                    return SRET_ERR_CI_DECLARACIONJURADA_INCORRECT;

                case -601:
                    return SERR_SERIALIZE_XMLPARAMIN;
                case -602:
                    return SERR_DESERIALIZE_XMLPARAMOUT;
                default:
                    return "Error No Documentado";
            }

        }
    }
}
