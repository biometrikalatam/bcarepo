﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bio.Digital.Receipt.Api
{
    public class CIParam : System.ICloneable
    {

        public CIParam() { }

        public string TrackId { get; set; }
        public int ValidityType { get; set; }
        public string IDCardImageFront { get; set; }
        public string IDCardImageBack { get; set; }
        public string IDCardPhotoImage { get; set; }
        public string IDCardSignatureImage { get; set; }
        public string ManualSignatureImage { get; set; }
        public string Selfie { get; set; }
        public string WorkStationID { get; set; }  //MAC en Windows y IMEI en mobile
        public string GeoRef { get; set; }
        public string TypeId { get; set; }
        public string ValueId { get; set; }
        public string Name { get; set; }
        public string PhaterLastName { get; set; }
        public string MotherLastName { get; set; }
        public string Sex { get; set; }
        public string BirthDate { get; set; }
        public string Nacionality { get; set; }
        public string IssueDate { get; set; }
        public string ExpirationDate { get; set; }
        public string Serial { get; set; }
        public string CertificatePDF { get; set; }
        public int QRTokenValidity { get; set; }  //Minutes
        public string DateGenToken { get; set; }  //dd/MM/yyyy
        public string DestinaryMail { get; set; }
        public string Score { get; set; }
        public string Threshold { get; set; }
        public string Map { get; set; }
        public string URLVideo { get; set; }
        //Added 15-11-2020 - Para pasar parametros variables, como por ejemplo habiliar o no el liveness en el QR para app celular
        //[DataMember]
        public List<Param> DynamicParam; 

        public object Clone()
        {
            return MemberwiseClone();
        }

        public bool ContainsKey(string key)
        {
            try
            {
                if (DynamicParam == null || DynamicParam.Count == 0)
                    return false;

                foreach (Param item in DynamicParam)
                {
                    if (item.key.Equals(key))
                        return true;
                }

            }
            catch (Exception ex)
            {
                //LOG.Error(" Error: " + ex.Message);
            }
            return false;
        }

        public static bool ContainsKey(string key, List<Param> oDynamicParam)
        {
            try
            {
                if (oDynamicParam == null || oDynamicParam.Count == 0)
                    return false;

                foreach (Param item in oDynamicParam)
                {
                    if (item.key.Equals(key))
                        return true;
                }

            }
            catch (Exception ex)
            {
                //LOG.Error(" Error: " + ex.Message);
            }
            return false;
        }

        public static string GetValue(string key, List<Param> oDynamicParam)
        {
            try
            {
                if (oDynamicParam == null || oDynamicParam.Count == 0)
                    return null;

                foreach (Param item in oDynamicParam)
                {
                    if (item.key.Equals(key))
                        return item.value;
                }

            }
            catch (Exception ex)
            {
                //LOG.Error(" Error: " + ex.Message);
            }
            return null;
        }
    }
}
