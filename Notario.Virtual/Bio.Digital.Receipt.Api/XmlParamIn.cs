﻿namespace Bio.Digital.Receipt.Api
{
    ///<summary>
    /// Clase destinada a la serializacion de parametros de entrada de Web Services
    ///</summary>
    public class XmlParamIn : System.ICloneable
    {
        #region Private

        /// <summary>
        /// Posibles Valores =
        ///      1-Generate
        ///      2-Verify
        ///      3-Get
        /// </summary>
        private int _actionid;

        /// <summary>
        ///Id de aplicación
        /// </summary>
        //private int _applicationid;

        /// <summary>
        ///Descripcion del recibo édido si aplica
        /// </summary>
        private string _description;

        //private RdExtensionItem[] _extensiones;

        /// <summary>
        /// Datos adicionales variables
        /// </summary>
        private string _additionaldata;

        /// <summary>
        /// Id de Compañia para filtrar si está deshabilitada.
        /// Si no viene informada o no existe, se rechaza transaccion
        /// </summary>
        private int _companyid;

        /// <summary>
        /// Usuario de la compañia companyid, para referencia.
        /// </summary>
        private int _userid;

        /// <summary>
        /// Id de Origen de la transacción. Si el origen no existe se rechaza la transacción.
        /// </summary>
        private int _origin;

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist
        /// </summary>
        private string _clientid;

        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        private string _ipenduser;

        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        private string _enduser;

        /// <summary>
        /// XML del recibo generado. Es un Envelope, donde está el XML firmado y el Timestamp
        /// </summary>
        private string _receipt;

        /// <summary>
        /// Receipt Id para Get
        /// </summary>
        private string _receiptid;

        #endregion Private

        #region Public

        /// <summary>
        /// RDEnvelope, conde el sobre inclye el XML del recibo firmado con XMLDSIG y el timestamp
        /// </summary>
        public string Receipt
        {
            get { return _receipt; }
            set { _receipt = value; }
        }

        /// <summary>
        /// Posibles Valores =
        ///      1-Verify
        ///      2-Enroll
        ///      3-Get
        /// </summary>
        public int Actionid
        {
            get { return _actionid; }
            set { _actionid = value; }
        }

        /// <summary>
        /// Es usado para enviar datos adicionales de la muestra, por ejemplo si es WSQ,
        /// se envia W=256|H=256, o si es template NEC, se envia COEF=90. Esto debe ser
        /// entendico por el Matcher correspondiente, o quien trate el parametro IN.
        /// </summary>
        public string Additionaldata
        {
            get { return _additionaldata; }
            set { _additionaldata = value; }
        }

        /// <summary>
        /// Id de Compañia para filtrar si está deshabilitada.
        /// Si no viene informada o no existe, se rechaza transaccion
        /// </summary>
        public int Companyid
        {
            get { return _companyid; }
            set { _companyid = value; }
        }

        /// <summary>
        /// Usuario de la compañia companyid, para referencia.
        /// </summary>
        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }
        }

        /// <summary>
        /// Id de Origen de la transacción. Si el origen no existe se rechaza la transacción.
        /// </summary>
        public int Origin
        {
            get { return _origin; }
            set { _origin = value; }
        }

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist
        /// </summary>
        public string Clientid
        {
            get { return _clientid; }
            set { _clientid = value; }
        }

        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        public string Ipenduser
        {
            get { return _ipenduser; }
            set { _ipenduser = value; }
        }

        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        public string Enduser
        {
            get { return _enduser; }
            set { _enduser = value; }
        }

        /// <summary>
        /// Id Aplicaicon (Tipo Recibo)
        /// </summary>
        //public int ApplicationId { get => _applicationid; set => _applicationid = value; }
        //public string Description { get => _description; set => _description = value; }
        //public RdExtensionItem[] Extensiones { get => _extensiones; set => _extensiones = value; }
        //public string ReceiptId { get => _receiptid; set => _receiptid = value; }

        public int ApplicationId { get; set; }
        public string Description { get; set; }
        public RdExtensionItem[] Extensiones { get; set; }
        public string ReceiptId { get; set; }

        public CIParam CIParam { get; set; }
        //public string TrackIdCI { get; set; }
        //public int ValidityType { get; set; }

        #endregion Public

        #region Implementation of ICloneable

        /// <summary>
        /// Crea un nuevo objeto copiado de la instancia actual.
        /// </summary>
        /// <returns>
        /// Nuevo objeto que es una copia de esta instancia.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public object Clone()
        {
            return MemberwiseClone();
        }

        #endregion Implementation of ICloneable
    }
}