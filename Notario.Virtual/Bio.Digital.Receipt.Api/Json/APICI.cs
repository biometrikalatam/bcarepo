﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bio.Digital.Receipt.Api.Json
{
    /// <summary>
    /// Clase que permite la comunicación con la API de Notario Virtual
    /// </summary>
    public class APICI
    {
        protected IRestClient Client;

        /// <summary>
        /// Constructor, en su instanciación configura el host de conección
        /// </summary>
        /// <param name="host">URI de la API NotarioVirtual</param>
        public APICI(string host)
        {
            Client = new RestClient(host);
        }

        /// <summary>
        /// Método que permite el consumo del método Proccess de NotarioVirtual
        /// </summary>
        /// <param name="xmlParamIn">Clase XmlParamIn con datos a procesar</param>
        /// <param name="customTimeOut">Tiempo de timeout de los WebService, es un parametro opcional y por defecto esta configurado a 30000 (Milisegundos)</param>
        /// <returns></returns>
        public XmlParamOut Proccess(XmlParamIn xmlParamIn, int customTimeOut = 0)
        {
            var request = new RestRequest("api/CI/Proccess/{customTimeOut?}", Method.POST);
            if (customTimeOut > 0)
            {
                request.Timeout = customTimeOut;
                request.AddUrlSegment("customTimeOut", customTimeOut.ToString());
            }

            request.AddJsonBody(xmlParamIn);
            try
            {
                IRestResponse response = Client.Execute(request);
                var content = response.Content;

                return JsonConvert.DeserializeObject<XmlParamOut>(content);
            }
            catch (Exception e)
            {
                return new XmlParamOut
                {
                    Message = Errors.GetDescription(Errors.IERR_DESERIALIZE_XMLPARAMOUT)
                };
            }
        }

        /// <summary>
        /// Método que permite el consumo del método Proccess de NotarioVirtual
        /// </summary>
        /// <param name="xmlParamIn">Clase XmlParamIn con datos a procesar</param>
        /// <param name="customTimeOut">Tiempo de timeout de los WebService, es un parametro opcional y por defecto esta configurado a 30000 (Milisegundos)</param>
        /// <returns></returns>
        public int CheckStatusCI(string trackid, int customTimeOut = 0)
        {
            var request = new RestRequest("api/CI/CheckStatusCI/{customTimeOut?}", Method.POST);
            if (customTimeOut > 0)
            {
                request.Timeout = customTimeOut;
                request.AddUrlSegment("customTimeOut", customTimeOut.ToString());
            }

            request.AddJsonBody(trackid);
            try
            {
                IRestResponse response = Client.Execute(request);
                var content = response.Content;

                return Convert.ToInt32(content); 
            }
            catch (Exception e)
            {
                return -1; 
            }
        }
    }
}
