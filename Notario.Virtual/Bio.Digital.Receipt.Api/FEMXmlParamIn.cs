﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bio.Digital.Receipt.Api
{
    public class FEMXmlParamIn : System.ICloneable
    {

        #region Private

        #endregion Private

        #region Public


        /// <summary>
        /// Posibles Valores =
        ///      1-Verify
        ///      2-Enroll
        ///      3-Get
        /// </summary>
        public int Actionid { get; set; }

        /// <summary>
        /// Es usado para enviar datos adicionales de la muestra, por ejemplo si es WSQ,
        /// se envia W=256|H=256, o si es template NEC, se envia COEF=90. Esto debe ser
        /// entendico por el Matcher correspondiente, o quien trate el parametro IN.
        /// </summary>
        public string Additionaldata { get; set; }

        /// <summary>
        /// Id de Compañia para filtrar si está deshabilitada.
        /// Si no viene informada o no existe, se rechaza transaccion
        /// </summary>
        public int Companyid { get; set; }

        /// <summary>
        /// Usuario de la compañia companyid, para referencia.
        /// </summary>
        public int Userid { get; set; }

        /// <summary>
        /// Id de Origen de la transacción. Si el origen no existe se rechaza la transacción.
        /// </summary>
        public int Origin { get; set; }

        /// <summary>
        /// Id del sensor utilizado en la captura de la muestra si es que existe. Sino se coloca
        /// un default NE = No Exist
        /// </summary>
        public string Clientid { get; set; }

        /// <summary>
        /// Ip del usaurio final. Si viene en el token se sobreescribe este campo.
        /// </summary>
        public string Ipenduser { get; set; }

        /// <summary>
        /// Usuario final de quien es el sample
        /// </summary>
        public string Enduser { get; set; }


        public string Description { get; set; }
        public RdExtensionItem[] Extensiones { get; set; }
        public string ReceiptId { get; set; }

        #endregion Public

        #region Implementation of ICloneable

        /// <summary>
        /// Crea un nuevo objeto copiado de la instancia actual.
        /// </summary>
        /// <returns>
        /// Nuevo objeto que es una copia de esta instancia.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public object Clone()
        {
            return MemberwiseClone();
        }

        #endregion Implementation of ICloneable
    }
}
