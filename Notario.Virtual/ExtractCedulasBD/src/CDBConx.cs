﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace ExtractCedulasBD.src
{
    /// <summary>
    /// Clase destinada a generar las conexiones a las bases de datos, y preparar
    /// los SqlCommand con sus respectivos parámetros, de tal forma optimizar dichas 
    /// operaciones y mejorar las performance del sistema.
    /// </summary>
    public class CDBConx
    {

        /// <summary>
        /// Variable estática para enviar información al log de ejecución. 
        /// Ver para detalles log4net.
        /// </summary>
        private static readonly ILog log =
                                LogManager.GetLogger(typeof(CDBConx));

        /// <summary>
        /// Permite saber si la conexión a una base de datos es correta.
        /// </summary>
        public bool isConxOK = false;

        /// <summary>
        /// Mantendrá el diferencial de los registros biométricos que se deben chequear.
        /// </summary>
        public SqlDataReader readerBTR;

        /// <summary>
        /// Para leer los Bir de Blacklist cuando está comparando con BL y encuentra duplicados.
        /// </summary>
        public SqlDataReader readerBHKBL;

        /// <summary>
        /// Comando para acceso a bases de datos para consultas
        /// </summary>
        public SqlCommand cmd;

        /// <summary>
        /// Comando para acceso a bases de datos para consultas en BD de Diagnostic. Para cuando se trabaja con Lista Negra
        /// </summary>
        public SqlCommand cmdCHK;

        /// <summary>
        /// Comando para insertar registros en base de datos de BioCheck, en caso
        /// de encontrar matchings positivos.
        /// </summary>
        public SqlCommand cmdIns;

        /// <summary>
        /// Comando para insertar registros en base de datos de BioCheck, en caso
        /// de encontrar matchings positivos.
        /// </summary>
        public SqlCommand cmdInsBL;

        /// <summary>
        /// Conexión a la base de datos del BTR
        /// </summary>
        public SqlConnection conxBTR;
        /// <summary>
        /// Conexión par ainserción, en caso de ser necesario, a la base de datos de
        /// BioCheck.
        /// </summary>
        public SqlConnection conxBCHKIns;

        /// <summary>
        /// Constructor que crea por default las conexiones a las diferentes bases de datos,
        /// asi como los comandos necesarios para accderlas, con sus respectivos parámetros.
        /// uego, en las diferentes ejecuciones del sistema, solo se modifican los valores 
        /// de dichos parámetros, optimizando asi los tiempos de ejecución.
        /// Para las conexiones toma el string de conexión configurado en el sitema (Ver 
        /// <b><i>Biokey.BioCheck.config</i></b> para mas detalles).
        /// </summary>
        public CDBConx()
        {

            try
            {
                //Abro conexiones
                conxBTR = new SqlConnection("");
                conxBTR.Open();
                conxBCHKIns = new SqlConnection("");
                conxBCHKIns.Open();

                //Creo Command para insertar datos en Duplicados
                cmdIns = new SqlCommand();
                cmdIns.CommandType = CommandType.Text;
                //Datos de una identidad
                SqlParameter paramIdIdent1Ins = new SqlParameter("@idident1Ins", SqlDbType.Int);
                SqlParameter paramTipoID1Ins = new SqlParameter("@tipoid1Ins", SqlDbType.VarChar);
                SqlParameter paramNroID1Ins = new SqlParameter("@nroid1Ins", SqlDbType.VarChar);
                SqlParameter paramNombre1Ins = new SqlParameter("@nombre1Ins", SqlDbType.VarChar);
                SqlParameter paramIdBir1Ins = new SqlParameter("@idbir1Ins", SqlDbType.Int);
                SqlParameter paramIdDedo1Ins = new SqlParameter("@iddedo1Ins", SqlDbType.Int);
                SqlParameter paramCreation1Ins = new SqlParameter("@creation1Ins", SqlDbType.DateTime);
                cmdIns.Parameters.Add(paramIdIdent1Ins);
                cmdIns.Parameters.Add(paramTipoID1Ins);
                cmdIns.Parameters.Add(paramNroID1Ins);
                cmdIns.Parameters.Add(paramNombre1Ins);
                cmdIns.Parameters.Add(paramIdBir1Ins);
                cmdIns.Parameters.Add(paramIdDedo1Ins);
                cmdIns.Parameters.Add(paramCreation1Ins);

                //Datos de la otra identidad
                SqlParameter paramIdIdent2Ins = new SqlParameter("@idident2Ins", SqlDbType.Int);
                SqlParameter paramTipoID2Ins = new SqlParameter("@tipoid2Ins", SqlDbType.VarChar);
                SqlParameter paramNroID2Ins = new SqlParameter("@nroid2Ins", SqlDbType.VarChar);
                SqlParameter paramNombre2Ins = new SqlParameter("@nombre2Ins", SqlDbType.VarChar);
                SqlParameter paramIdBir2Ins = new SqlParameter("@idbir2Ins", SqlDbType.Int);
                SqlParameter paramIdDedo2Ins = new SqlParameter("@iddedo2Ins", SqlDbType.Int);
                SqlParameter paramCreation2Ins = new SqlParameter("@creation2Ins", SqlDbType.DateTime);
                cmdIns.Parameters.Add(paramIdIdent2Ins);
                cmdIns.Parameters.Add(paramTipoID2Ins);
                cmdIns.Parameters.Add(paramNroID2Ins);
                cmdIns.Parameters.Add(paramNombre2Ins);
                cmdIns.Parameters.Add(paramIdBir2Ins);
                cmdIns.Parameters.Add(paramIdDedo2Ins);
                cmdIns.Parameters.Add(paramCreation2Ins);

                SqlParameter paramUmbralIns = new SqlParameter("@umbralIns", SqlDbType.Int);
                SqlParameter paramScoreIns = new SqlParameter("@scoreIns", SqlDbType.Int);
                cmdIns.Parameters.Add(paramUmbralIns);
                cmdIns.Parameters.Add(paramScoreIns);

                cmdIns.CommandText = "INSERT INTO bp_diagnostic_duplicados " +
                                     "(ident_id_1, typeid_1, valueid_1, name_1, bir_id_1, bir_bodytype_1, bir_creation_1," +
                                     "ident_id_2, typeid_2, valueid_2, name_2, bir_id_2, bir_bodytype_2, bir_creation_2," +
                                     "umbral, score) " +
                                     "VALUES " +
                                     "(@idident1Ins,@tipoid1Ins,@nroid1Ins,@nombre1Ins,@idbir1Ins,@iddedo1Ins,@creation1Ins," +
                                     "@idident2Ins,@tipoid2Ins,@nroid2Ins,@nombre2Ins,@idbir2Ins,@iddedo2Ins,@creation2Ins," +
                                     "@umbralIns,@scoreIns)";
                cmdIns.Connection = conxBCHKIns;

                //Creo Command para insertar datos en Duplicados En BlackList
                cmdInsBL = new SqlCommand();
                cmdInsBL.CommandType = CommandType.Text;
                //Datos de una identidad
                SqlParameter paramIdIdent1InsBL = new SqlParameter("@idident1Ins", SqlDbType.Int);
                SqlParameter paramTipoID1InsBL = new SqlParameter("@tipoid1Ins", SqlDbType.VarChar);
                SqlParameter paramNroID1InsBL = new SqlParameter("@nroid1Ins", SqlDbType.VarChar);
                SqlParameter paramNombre1InsBL = new SqlParameter("@nombre1Ins", SqlDbType.VarChar);
                SqlParameter paramIdBir1InsBL = new SqlParameter("@idbir1Ins", SqlDbType.Int);
                SqlParameter paramIdDedo1InsBL = new SqlParameter("@iddedo1Ins", SqlDbType.Int);
                SqlParameter paramCreation1InsBL = new SqlParameter("@creation1Ins", SqlDbType.DateTime);
                cmdInsBL.Parameters.Add(paramIdIdent1InsBL);
                cmdInsBL.Parameters.Add(paramTipoID1InsBL);
                cmdInsBL.Parameters.Add(paramNroID1InsBL);
                cmdInsBL.Parameters.Add(paramNombre1InsBL);
                cmdInsBL.Parameters.Add(paramIdBir1InsBL);
                cmdInsBL.Parameters.Add(paramIdDedo1InsBL);
                cmdInsBL.Parameters.Add(paramCreation1InsBL);

                //Datos de la otra identidad
                SqlParameter paramIdIdent2InsBL = new SqlParameter("@idident2Ins", SqlDbType.Int);
                SqlParameter paramTipoID2InsBL = new SqlParameter("@tipoid2Ins", SqlDbType.VarChar);
                SqlParameter paramNroID2InsBL = new SqlParameter("@nroid2Ins", SqlDbType.VarChar);
                SqlParameter paramNombre2InsBL = new SqlParameter("@nombre2Ins", SqlDbType.VarChar);
                SqlParameter paramIdBir2InsBL = new SqlParameter("@idbir2Ins", SqlDbType.Int);
                SqlParameter paramIdDedo2InsBL = new SqlParameter("@iddedo2Ins", SqlDbType.Int);
                SqlParameter paramCreation2InsBL = new SqlParameter("@creation2Ins", SqlDbType.DateTime);
                cmdInsBL.Parameters.Add(paramIdIdent2InsBL);
                cmdInsBL.Parameters.Add(paramTipoID2InsBL);
                cmdInsBL.Parameters.Add(paramNroID2InsBL);
                cmdInsBL.Parameters.Add(paramNombre2InsBL);
                cmdInsBL.Parameters.Add(paramIdBir2InsBL);
                cmdInsBL.Parameters.Add(paramIdDedo2InsBL);
                cmdInsBL.Parameters.Add(paramCreation2InsBL);

                SqlParameter paramUmbralInsBL = new SqlParameter("@umbralIns", SqlDbType.Int);
                SqlParameter paramScoreInsBL = new SqlParameter("@scoreIns", SqlDbType.Int);
                cmdInsBL.Parameters.Add(paramUmbralInsBL);
                cmdInsBL.Parameters.Add(paramScoreInsBL);

                cmdInsBL.CommandText = "INSERT INTO bp_diagnostic_duplicados_blacklist " +
                                     "(ident_id_1, typeid_1, valueid_1, name_1, bir_id_1, bir_bodytype_1, bir_creation_1," +
                                     "ident_id_2, typeid_2, valueid_2, name_2, bir_id_2, bir_bodytype_2, bir_creation_2," +
                                     "umbral, score) " +
                                     "VALUES " +
                                     "(@idident1Ins,@tipoid1Ins,@nroid1Ins,@nombre1Ins,@idbir1Ins,@iddedo1Ins,@creation1Ins," +
                                     "@idident2Ins,@tipoid2Ins,@nroid2Ins,@nombre2Ins,@idbir2Ins,@iddedo2Ins,@creation2Ins," +
                                     "@umbralIns,@scoreIns)";
                cmdInsBL.Connection = conxBCHKIns;

                //Creo Command para extraer datos desde BTR
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conxBTR;
                //Parametros para extraccion
                SqlParameter paramIdBir1 = new SqlParameter("@idbir_1", SqlDbType.Int);
                //				paramIdBir1.Value = bir_id_base;    
                cmd.Parameters.Add(paramIdBir1);
                SqlParameter paramIdBir2 = new SqlParameter("@idbir_2", SqlDbType.Int);
                //				paramIdBir2.Value = bir_id_current;    			
                cmd.Parameters.Add(paramIdBir2);
                SqlParameter paramIdIDent1 = new SqlParameter("@idident_1", SqlDbType.Int);
                //				paramIdIDent1.Value = ident_id_base;  
                cmd.Parameters.Add(paramIdIDent1);
                SqlParameter paramIdIDent2 = new SqlParameter("@idident_2", SqlDbType.Int);
                //				paramIdIDent2.Value = ident_id_current;    		
                cmd.Parameters.Add(paramIdIDent2);

                isConxOK = true;
            }
            catch (Exception ex)
            {
                log.Error("Error preparando conexiones a BD para insertar duplicados!", ex);
                isConxOK = false;
            }
            log.Info("CDBConx.CDBConx - Conexiones OK!");
        }

        /// <summary>
        /// Constructor que crea por default las conexiones a las diferentes bases de datos,
        /// asi como los comandos necesarios para accderlas, con sus respectivos parámetros.
        /// uego, en las diferentes ejecuciones del sistema, solo se modifican los valores 
        /// de dichos parámetros, optimizando asi los tiempos de ejecución.
        /// Para las conexiones toma el string de conexión configurado en el sitema (Ver 
        /// <b><i>Biokey.BioCheck.config</i></b> para mas detalles).
        /// </summary>
        public CDBConx(string _strConxBPS, string _strConxBD)
        {

            try
            {
                //Abro conexiones
                conxBTR = new SqlConnection(_strConxBPS);
                conxBTR.Open();
                conxBCHKIns = new SqlConnection(_strConxBD);
                conxBCHKIns.Open();

                //Creo Command para insertar datos en Duplicados
                cmdIns = new SqlCommand();
                cmdIns.CommandType = CommandType.Text;
                //Datos de una identidad
                SqlParameter paramIdIdent1Ins = new SqlParameter("@idident1Ins", SqlDbType.Int);
                SqlParameter paramTipoID1Ins = new SqlParameter("@tipoid1Ins", SqlDbType.VarChar);
                SqlParameter paramNroID1Ins = new SqlParameter("@nroid1Ins", SqlDbType.VarChar);
                SqlParameter paramNombre1Ins = new SqlParameter("@nombre1Ins", SqlDbType.VarChar);
                SqlParameter paramIdBir1Ins = new SqlParameter("@idbir1Ins", SqlDbType.Int);
                SqlParameter paramIdDedo1Ins = new SqlParameter("@iddedo1Ins", SqlDbType.Int);
                SqlParameter paramCreation1Ins = new SqlParameter("@creation1Ins", SqlDbType.DateTime);
                cmdIns.Parameters.Add(paramIdIdent1Ins);
                cmdIns.Parameters.Add(paramTipoID1Ins);
                cmdIns.Parameters.Add(paramNroID1Ins);
                cmdIns.Parameters.Add(paramNombre1Ins);
                cmdIns.Parameters.Add(paramIdBir1Ins);
                cmdIns.Parameters.Add(paramIdDedo1Ins);
                cmdIns.Parameters.Add(paramCreation1Ins);

                //Datos de la otra identidad
                SqlParameter paramIdIdent2Ins = new SqlParameter("@idident2Ins", SqlDbType.Int);
                SqlParameter paramTipoID2Ins = new SqlParameter("@tipoid2Ins", SqlDbType.VarChar);
                SqlParameter paramNroID2Ins = new SqlParameter("@nroid2Ins", SqlDbType.VarChar);
                SqlParameter paramNombre2Ins = new SqlParameter("@nombre2Ins", SqlDbType.VarChar);
                SqlParameter paramIdBir2Ins = new SqlParameter("@idbir2Ins", SqlDbType.Int);
                SqlParameter paramIdDedo2Ins = new SqlParameter("@iddedo2Ins", SqlDbType.Int);
                SqlParameter paramCreation2Ins = new SqlParameter("@creation2Ins", SqlDbType.DateTime);
                cmdIns.Parameters.Add(paramIdIdent2Ins);
                cmdIns.Parameters.Add(paramTipoID2Ins);
                cmdIns.Parameters.Add(paramNroID2Ins);
                cmdIns.Parameters.Add(paramNombre2Ins);
                cmdIns.Parameters.Add(paramIdBir2Ins);
                cmdIns.Parameters.Add(paramIdDedo2Ins);
                cmdIns.Parameters.Add(paramCreation2Ins);

                SqlParameter paramUmbralIns = new SqlParameter("@umbralIns", SqlDbType.Int);
                SqlParameter paramScoreIns = new SqlParameter("@scoreIns", SqlDbType.Int);
                cmdIns.Parameters.Add(paramUmbralIns);
                cmdIns.Parameters.Add(paramScoreIns);

                cmdIns.CommandText = "INSERT INTO bp_diagnostic_duplicados " +
                                     "(ident_id_1, typeid_1, valueid_1, name_1, bir_id_1, bir_bodytype_1, bir_creation_1," +
                                     "ident_id_2, typeid_2, valueid_2, name_2, bir_id_2, bir_bodytype_2, bir_creation_2," +
                                     "umbral, score) " +
                                     "VALUES " +
                                     "(@idident1Ins,@tipoid1Ins,@nroid1Ins,@nombre1Ins,@idbir1Ins,@iddedo1Ins,@creation1Ins," +
                                     "@idident2Ins,@tipoid2Ins,@nroid2Ins,@nombre2Ins,@idbir2Ins,@iddedo2Ins,@creation2Ins," +
                                     "@umbralIns,@scoreIns)";
                cmdIns.Connection = conxBCHKIns;

                //Creo Command para insertar datos en Duplicados En BlackList
                cmdInsBL = new SqlCommand();
                cmdInsBL.CommandType = CommandType.Text;
                //Datos de una identidad
                SqlParameter paramIdIdent1InsBL = new SqlParameter("@idident1Ins", SqlDbType.Int);
                SqlParameter paramTipoID1InsBL = new SqlParameter("@tipoid1Ins", SqlDbType.VarChar);
                SqlParameter paramNroID1InsBL = new SqlParameter("@nroid1Ins", SqlDbType.VarChar);
                SqlParameter paramNombre1InsBL = new SqlParameter("@nombre1Ins", SqlDbType.VarChar);
                SqlParameter paramIdBir1InsBL = new SqlParameter("@idbir1Ins", SqlDbType.Int);
                SqlParameter paramIdDedo1InsBL = new SqlParameter("@iddedo1Ins", SqlDbType.Int);
                SqlParameter paramCreation1InsBL = new SqlParameter("@creation1Ins", SqlDbType.DateTime);
                cmdInsBL.Parameters.Add(paramIdIdent1InsBL);
                cmdInsBL.Parameters.Add(paramTipoID1InsBL);
                cmdInsBL.Parameters.Add(paramNroID1InsBL);
                cmdInsBL.Parameters.Add(paramNombre1InsBL);
                cmdInsBL.Parameters.Add(paramIdBir1InsBL);
                cmdInsBL.Parameters.Add(paramIdDedo1InsBL);
                cmdInsBL.Parameters.Add(paramCreation1InsBL);

                //Datos de la otra identidad
                SqlParameter paramIdIdent2InsBL = new SqlParameter("@idident2Ins", SqlDbType.Int);
                SqlParameter paramTipoID2InsBL = new SqlParameter("@tipoid2Ins", SqlDbType.VarChar);
                SqlParameter paramNroID2InsBL = new SqlParameter("@nroid2Ins", SqlDbType.VarChar);
                SqlParameter paramNombre2InsBL = new SqlParameter("@nombre2Ins", SqlDbType.VarChar);
                SqlParameter paramIdBir2InsBL = new SqlParameter("@idbir2Ins", SqlDbType.Int);
                SqlParameter paramIdDedo2InsBL = new SqlParameter("@iddedo2Ins", SqlDbType.Int);
                SqlParameter paramCreation2InsBL = new SqlParameter("@creation2Ins", SqlDbType.DateTime);
                cmdInsBL.Parameters.Add(paramIdIdent2InsBL);
                cmdInsBL.Parameters.Add(paramTipoID2InsBL);
                cmdInsBL.Parameters.Add(paramNroID2InsBL);
                cmdInsBL.Parameters.Add(paramNombre2InsBL);
                cmdInsBL.Parameters.Add(paramIdBir2InsBL);
                cmdInsBL.Parameters.Add(paramIdDedo2InsBL);
                cmdInsBL.Parameters.Add(paramCreation2InsBL);

                SqlParameter paramUmbralInsBL = new SqlParameter("@umbralIns", SqlDbType.Int);
                SqlParameter paramScoreInsBL = new SqlParameter("@scoreIns", SqlDbType.Int);
                cmdInsBL.Parameters.Add(paramUmbralInsBL);
                cmdInsBL.Parameters.Add(paramScoreInsBL);

                cmdInsBL.CommandText = "INSERT INTO bp_diagnostic_duplicados_bl " +
                                     "(ident_id_1, typeid_1, valueid_1, name_1, bir_id_1, bir_bodytype_1, bir_creation_1," +
                                     "ident_id_2, typeid_2, valueid_2, name_2, bir_id_2, bir_bodytype_2, bir_creation_2," +
                                     "umbral, score) " +
                                     "VALUES " +
                                     "(@idident1Ins,@tipoid1Ins,@nroid1Ins,@nombre1Ins,@idbir1Ins,@iddedo1Ins,@creation1Ins," +
                                     "@idident2Ins,@tipoid2Ins,@nroid2Ins,@nombre2Ins,@idbir2Ins,@iddedo2Ins,@creation2Ins," +
                                     "@umbralIns,@scoreIns)";
                cmdInsBL.Connection = conxBCHKIns;

                //Creo Command para extraer datos desde BTR
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conxBTR;
                //Parametros para extraccion
                SqlParameter paramIdBir1 = new SqlParameter("@idbir_1", SqlDbType.Int);
                //				paramIdBir1.Value = bir_id_base;    
                cmd.Parameters.Add(paramIdBir1);
                SqlParameter paramIdBir2 = new SqlParameter("@idbir_2", SqlDbType.Int);
                //				paramIdBir2.Value = bir_id_current;    			
                cmd.Parameters.Add(paramIdBir2);
                SqlParameter paramIdIDent1 = new SqlParameter("@idident_1", SqlDbType.Int);
                //				paramIdIDent1.Value = ident_id_base;  
                cmd.Parameters.Add(paramIdIDent1);
                SqlParameter paramIdIDent2 = new SqlParameter("@idident_2", SqlDbType.Int);
                //				paramIdIDent2.Value = ident_id_current;    		
                cmd.Parameters.Add(paramIdIDent2);

                //Added31-01
                //Creo Command para extraer datos desde BTR
                cmdCHK = new SqlCommand();
                cmdCHK.CommandType = CommandType.Text;
                cmdCHK.Connection = conxBCHKIns;
                //Parametros para extraccion
                SqlParameter paramIdBirCHK1 = new SqlParameter("@idbir_1", SqlDbType.Int);
                cmdCHK.Parameters.Add(paramIdBirCHK1);
                SqlParameter paramIdBirCHK2 = new SqlParameter("@idbir_2", SqlDbType.Int);
                cmdCHK.Parameters.Add(paramIdBirCHK2);
                SqlParameter paramIdIDentCHK1 = new SqlParameter("@idident_1", SqlDbType.Int);
                cmdCHK.Parameters.Add(paramIdIDentCHK1);
                SqlParameter paramIdIDentCHK2 = new SqlParameter("@idident_2", SqlDbType.Int);
                cmdCHK.Parameters.Add(paramIdIDentCHK2);


                isConxOK = true;
            }
            catch (Exception ex)
            {
                log.Error("Error preparando conexiones a BD para insertar duplicados!", ex);
                isConxOK = false;
            }
            log.Info("CDBConx.CDBConx - Conexiones OK!");
        }

        /// <summary>
        /// Dado un string de conexión, genera una conexión a la base de datos, y la 
        /// devuelve, o bien devuelve null si le es imposib conectarse, dejando constancia
        /// en log de operaciones global del sistema si existieron errores.
        /// </summary>
        /// <param name="strconx">String de conexión del tipo: 
        /// <i>"Data Source=Server;Initial Catalog=BASE1;User ID=sa;password=pswsa;"</i></param>
        /// <returns>Retorna un objeto SqlConnection con una conexión válida, o sino null si 
        /// no pudo establecerla.</returns>
        internal static SqlConnection GetConnection(string strconx)
        {
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(strconx);
            }
            catch (Exception ex)
            {
                connection = null;
                log.Error("CDBConx.GetConnection", ex);
            }

            return connection;
        }


        /// <summary>
        /// Dada una conexión a iuna base de datos, la cierra si está abierta.
        /// </summary>
        /// <param name="conx">Objeto del tipo Sql Connection, para cerrar si está abierta.</param>
        /// <returns>Retorna true si terminó la operación correctamente.</returns>
        internal static bool CloseConnection(SqlConnection conx)
        {
            bool ret = false;
            try
            {
                conx.Close();
                ret = true;
            }
            catch (Exception ex)
            {
                log.Error("CDBConx.CloseConnection", ex);
            }

            return ret;
        }

    }
}
