﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ExtractCedulasBD.src;
using log4net;
using log4net.Config;

namespace ExtractCedulasBD
{
    public class Program
    {
        [DllImport("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        private static extern int SetProcessWorkingSetSize(IntPtr process, int minimumWorkingSetSize, int maximumWorkingSetSize);

        private static readonly ILog LOG = LogManager.GetLogger(typeof(Program));
        /// <summary>
        /// Clase de conexiones globales a bases de datos, y de comandos 
        /// SQL con parámetros generales. 
        /// </summary>
        static internal CDBConx g_DBConx;

        public static void Main(string[] args)
        {
            try
            {
                XmlConfigurator.Configure(new FileInfo("logger.cfg"));
                LOG.Info("Biometrika Extract Images from DB Starting...");

                //Abro conexión a BD
                //g_DBConx = new CDBConx(Properties.Settings.Default.ECStringConexion, Properties.Settings.Default.ECStringConexion);

                //Mantendrá el diferencial de los que s deben chequear.
                SqlDataReader readerDifBTR;
                //Para generar comandos necesarios
                SqlCommand cmd;
                SqlCommand cmdIns;

                //Conexiones necesarias para datareaders
                SqlConnection conxDifBTR;
                SqlConnection conxDifBCHK;
                SqlConnection conxDifBCHKIns;

                string sAux;
                string sTemplate;
                //BOKVFTemplate oTemplate;
                //ITemplate inTemplate, outTemplate;            
                string inWSQorTemplate, outTemplate;

                int _IdBirLastUsed = 0;

                int ret = 0;

                try
                {
                    LOG.Info("Main.Program IN...");

                    //Abro conexiones
                    conxDifBTR = new SqlConnection(Properties.Settings.Default.ECStringConexion);
                    conxDifBTR.Open();
                    LOG.Debug("Main.Program Conexion abierta!");

                    //Elimino los idbir necesarios para evitar inconsistencias
                    SqlParameter paramIdBir = new SqlParameter("@idbir", SqlDbType.Int);
                    paramIdBir.Value = Properties.Settings.Default.ECIdInit;

                    cmd = new SqlCommand();
                    cmd.Connection = conxDifBTR;
                    cmd.Parameters.Add(paramIdBir);

                    if (Properties.Settings.Default.ECIdEnd == 0)
                    {
                        cmd.CommandText = "SELECT " +
                                            Properties.Settings.Default.ECColumnaIdentify + "," +
                                            Properties.Settings.Default.ECColumnaFront + "," +
                                            Properties.Settings.Default.ECColumnaBack + "," +
                                            Properties.Settings.Default.ECColumnFoto + 
                                          " FROM " + Properties.Settings.Default.ECTabla  + 
                                          " WHERE " + Properties.Settings.Default.ECNameId + " > @idbir";
                    }
                    else
                    {
                        SqlParameter paramIdBir1 = new SqlParameter("@idbir1", SqlDbType.Int);
                        paramIdBir1.Value = Properties.Settings.Default.ECIdEnd;
                        cmd.CommandText = cmd.CommandText = "SELECT " +
                                            Properties.Settings.Default.ECColumnaIdentify + "," +
                                            Properties.Settings.Default.ECColumnaFront + "," +
                                            Properties.Settings.Default.ECColumnaBack + "," +
                                            Properties.Settings.Default.ECColumnFoto +
                                          " FROM " + Properties.Settings.Default.ECTabla +
                                          " WHERE " + Properties.Settings.Default.ECNameId + " > @idbir AND " +
                                          Properties.Settings.Default.ECNameId + " < @idbir1";
                        cmd.Parameters.Add(paramIdBir1);
                    }

                    LOG.Debug("Main.Program Leo datos => " + cmd.CommandText);

                   

                    readerDifBTR = cmd.ExecuteReader();
                    LOG.Debug("Main.Program - Recuperados.");

                    if (readerDifBTR != null && readerDifBTR.HasRows)
                    {
                        LOG.Debug("Main.Program - Hay datos...");
                    } else
                    {
                        LOG.Debug("Main.Program - NO Hay datos");
                    }

                    string _valueid;
                    string _front;
                    string _back;
                    string _foto;
                    Stopwatch watcher = new Stopwatch();
                    watcher.Start();
                    int _CantidadProcesados = 0; 
                    while (readerDifBTR.Read())
                    {

                        if (_CantidadProcesados % 25 == 0)
                        {
                            LOG.Info("Main.Program -    => Procesados -> " + _CantidadProcesados + "...");
                            alzheimer();
                        }
                        _CantidadProcesados++;

                        _valueid = (readerDifBTR.IsDBNull(1) ? null : readerDifBTR.GetString(0));
                        if (string.IsNullOrEmpty(_valueid))
                        {
                            _valueid = _CantidadProcesados.ToString();
                        } else
                        {
                            _valueid = _CantidadProcesados.ToString() + "_" + _valueid;
                        }
                        _front = (readerDifBTR.IsDBNull(1) ? null : readerDifBTR.GetString(1));
                        _back = (readerDifBTR.IsDBNull(2) ? null : readerDifBTR.GetString(2));
                        _foto = (readerDifBTR.IsDBNull(3) ? null : readerDifBTR.GetString(3));

                        if (!String.IsNullOrEmpty(_front)) //Extraccion o set ok => Sigo
                        {
                            try
                            {
                                SaveImage(_valueid, "Front", _front);
                            }
                            catch (Exception exIns)
                            {
                                LOG.Error("Main.Program - Insert Minucia - Error grabando front de [" + _valueid.ToString() + "]");
                            }
                        }

                        if (!String.IsNullOrEmpty(_back)) //Extraccion o set ok => Sigo
                        {
                            try
                            {
                                SaveImage(_valueid, "Back", _back);
                            }
                            catch (Exception exIns)
                            {
                                LOG.Error("Main.Program - Insert Minucia - Error grabando back) de [" + _valueid.ToString() + "]");
                            }
                        }

                        if (!String.IsNullOrEmpty(_foto)) //Extraccion o set ok => Sigo
                        {
                            try
                            {
                                SaveImage(_valueid, "Photo", _foto);
                            }
                            catch (Exception exIns)
                            {
                                LOG.Error("Main.Program - Insert Minucia - Error grabando foto de [" + _valueid.ToString() + "]");
                            }
                        }
                        _CantidadProcesados++;
                    }
                    watcher.Stop();
                    LOG.Info("Main.Program -  Fin Proceso Generacion - Tiempo total utilizado = " + watcher.Elapsed.ToString());
                    LOG.Debug("Main.Program - => Procesados -> " + _CantidadProcesados + "...");
                    LOG.Debug("Main.Program - Fin Proceso recuperados!");
                    readerDifBTR.Close();
                   
                    LOG.Info("Main.Program - OUT!");
                }
                catch (Exception ex)
                {
                    LOG.Error("Main.Program - Error ", ex);
                }
                LOG.Info("CBioCheck.GeneraMinucias - OUT!");
            }
            catch (Exception ex)
            {

                LOG.Error("Main.Program - Error: " + ex.Message);
            }
        }

        private static void SaveImage(string semilla, string nombre, string imgb64)
        {
            try
            {
                byte[] byImage = Convert.FromBase64String(imgb64);

                System.IO.File.WriteAllBytes("data\\" + semilla + "_" + nombre + ".jpg", byImage);
            }
            catch (Exception ex)
            {

                LOG.Error("Main.SaveImage Error: " + ex.Message);
            }
        }

        public static void alzheimer()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1);
        }
    }
}
