﻿using Bio.Digital.Receipt.Api;
using log4net;
//using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bio.Digital.Receipt.FEMTest.Helpers
{
    internal class NVHelper
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(NVHelper));

        internal static int Notarize(string video, string image, string typeid, string valueid, string documentid,
            string deviceid, string georef, string email, System.Windows.Forms.Label labFeedback, out string xmlRDenvelope, out string msgerrNV)
        {
            msgerrNV = "";
            int ret = 0;
            xmlRDenvelope = null;
            //msg = null;
            Bio.Digital.Receipt.Api.RdExtensionItem item;
            string xmlparamin;
            string xmlparamout;
            Bio.Digital.Receipt.Api.XmlParamOut oParamout;

            try
            {
                LOG.Debug("NVHelper.Notarize IN...");
                if (labFeedback != null) labFeedback.Visible = true;
                ShowMsg(labFeedback, "Iniciando notarización");
                XmlParamIn oParamin = new XmlParamIn();
                oParamin.Actionid = 4; //FEMGenerate mas Add Water Mark
                oParamin.ApplicationId = 6; // Properties.Settings.Default.NVIdApp;
                oParamin.Companyid = 9; // Properties.Settings.Default.NVCompanyId;
                oParamin.Description = "FEM Signature";
                oParamin.Origin = 1;
                oParamin.Extensiones = new Bio.Digital.Receipt.Api.RdExtensionItem[7];
                item = new RdExtensionItem();
                item.key = "Video";
                item.value = video;
                LOG.Debug("NVHelper.Notarize key=Video => value=" + item.value);
                oParamin.Extensiones[0] = item;
                item = new RdExtensionItem();
                item.key = "Photo";
                item.value = image;
                LOG.Debug("NVHelper.Notarize key=Photo => value=" + item.value);
                oParamin.Extensiones[1] = item;
                item = new RdExtensionItem();
                item.key = "FirmanteId";
                item.value = typeid + "_" + valueid;
                LOG.Debug("NVHelper.Notarize key=FirmanteId => value=" + item.value);
                oParamin.Extensiones[2] = item;
                item = new RdExtensionItem();
                item.key = "DocumentId";
                item.value = documentid;
                LOG.Debug("NVHelper.Notarize key=DocumentId => value=" + item.value);
                oParamin.Extensiones[3] = item;
                item = new RdExtensionItem();
                item.key = "DeviceId";
                item.value = deviceid;
                LOG.Debug("NVHelper.Notarize key=DeviceId => value=" + item.value);
                oParamin.Extensiones[4] = item;
                item = new RdExtensionItem();
                item.key = "GeoRef";
                item.value = georef;
                LOG.Debug("NVHelper.Notarize key=" + item.key + "=> value=" + item.value);
                oParamin.Extensiones[5] = item;
                item = new RdExtensionItem();
                item.key = "eMail";
                item.value = email;
                LOG.Debug("NVHelper.Notarize key=eMail => value=" + item.value);
                oParamin.Extensiones[6] = item;
                xmlparamin = Bio.Digital.Receipt.Api.XmlUtils.SerializeAnObject(oParamin);

                using (FEMWS.FEM NV = new FEMWS.FEM())
                {
                    ShowMsg(labFeedback, "Notarizando en línea. Espere...");
                    NV.Url = Properties.Settings.Default.Bio_Digital_Receipt_FEMTest_FEMWS_FEM;
                    NV.Timeout = 2000000; // Properties.Settings.Default.NVTimeout;
                    LOG.Debug("NVHelper.Notarize Conectando a => " + NV.Url + " - Timeout=" + NV.Timeout);
                    //4 - Advanced Signature para BCI
                    int rdRet = NV.Process(xmlparamin, out xmlparamout);
                    ShowMsg(labFeedback, "Recibida respuesta...");
                    if (rdRet == 0)
                    {
                        ShowMsg(labFeedback, "Recibida certificación OK...");
                        oParamout = Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                        xmlRDenvelope = oParamout.Receipt;
                    }
                    else
                    {
                        ShowMsg(labFeedback, "Error certificación [" + rdRet.ToString() + "]");
                        if (!String.IsNullOrEmpty(xmlparamout))
                        {
                            oParamout = Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                            LOG.Error("NVHelper.Notarize Error de NV [" + rdRet + " - " + oParamout.Message + "]");
                        }
                        else
                        {
                            LOG.Error("NVHelper.Notarize Error de NV [" + rdRet + "]");
                        }

                    }
                    ret = rdRet;
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NVHelper.ShowMsg Error Desconocido Notarizando = " + ex.Message);
                ret = -1;
            }
            if (labFeedback != null) labFeedback.Visible = false;
            return ret;
        }

        private static void ShowMsg(Label labFeedback, string msg)
        {
            try
            {
                if (labFeedback != null)
                {
                    labFeedback.Text = msg;
                    labFeedback.Refresh();
                }
            }
            catch (Exception ex)
            {
                LOG.Error("NVHelper.ShowMsg Error Desconocido Notarizando = " + ex.Message);
            }
        }
    }
}

