﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Bio.Digital.Receipt.FEMTest
{
    public partial class Form1 : Form
    {
        private static readonly ILog LOG = LogManager.GetLogger(typeof(Form1));
        string RDEnvelope;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnFEM_Click(object sender, EventArgs e)
        {

            openFileDialog1.Title = "Seleccione un video a certificar...";
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {

                try
                {
                    string video = GetVideoInBase64(openFileDialog1.FileName);
                    if (String.IsNullOrEmpty(video))
                    {
                        richTextBox1.Text = "Error leyendo video...";
                        return;
                    }
                    //_notarizing = true;
                    string image = GetImageInBase64();
                    string typeid = "RUT";
                    string valueid = "21284415-5";
                    string documentid = "999999N";
                    string deviceid = "MACGGS";
                    string georef = "-33.450001,-70.667000";
                    string mail = "gsuhit@biometrika.cl";
                    string xmlRDEnvelope;
                    string msgerrNV;
                    int ret = Helpers.NVHelper.Notarize(video, image, typeid, valueid, documentid, deviceid, georef, mail,
                                                        null, out xmlRDEnvelope, out msgerrNV);
                    if (ret == 0)
                    {
                        RDEnvelope = xmlRDEnvelope;
                    }
                    else
                    {
                        richTextBox1.Text = "Error notarizando [ret=" + ret.ToString() + "-" + msgerrNV + "]";
                    }
                }
                catch (Exception ex)
                {
                    richTextBox1.Text = "Error notarizando [" + ex.Message + "]";
                    LOG.Error("FEUI.labStep3_Click Error Notarize [" + ex.Message + "]");
                }
            }

            ShowResult(RDEnvelope);
        }

        //private int ParseFEMtoBS(string fem, out BS bs)
        //{
        //    int ret = 0;
        //    //bs = new BS();
        //    try
        //    {
        //        XmlDocument xmlDoc = new XmlDocument();
        //        xmlDoc.PreserveWhitespace = false;
        //        xmlDoc.LoadXml(fem);
        //        XmlNode node = xmlDoc.GetElementsByTagName("ExtensionItem")[1];
        //        bs.BiometricSignature = node.ChildNodes[1].InnerText;
        //        bs.BiometricSignatureW = 200;
        //        bs.BiometricSignatureH = 200;
        //        bs.BiometricProvider = "Notario Virtual";
        //        bs.BiometricSignatureType = 41;
        //        node = xmlDoc.GetElementsByTagName("ExtensionItem")[2];
        //        bs.IdentityId = node.ChildNodes[1].InnerText;
        //        bs.IdnetityName = node.ChildNodes[1].InnerText;
        //        node = xmlDoc.GetElementsByTagName("ExtensionItem")[7];
        //        bs.QRVerify = node.ChildNodes[1].InnerText;
        //        bs.BSId = xmlDoc.FirstChild.FirstChild.Attributes["reciboID"].Value;
        //        bs.NVreceiptId = xmlDoc.FirstChild.FirstChild.Attributes["reciboID"].Value;
        //        bs.TimestampSignature = xmlDoc.FirstChild.LastChild.ChildNodes[1].InnerText;
        //        bs.NVReceipttimestamp = xmlDoc.FirstChild.LastChild.ChildNodes[1].InnerText;
        //        bs.TSASerialNumber = xmlDoc.FirstChild.LastChild.ChildNodes[2].InnerText;
        //        bs.TSA = xmlDoc.FirstChild.LastChild.ChildNodes[3].InnerText;
        //        bs.TSADateTimeSign = xmlDoc.FirstChild.LastChild.ChildNodes[1].InnerText;
        //        bs.TSAPolicy = xmlDoc.FirstChild.LastChild.ChildNodes[0].InnerText;
        //        bs.TSAURLVerify = xmlDoc.FirstChild.LastChild.ChildNodes[5].InnerText;
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return 0;
        //}


        private void ShowResult(string fEMResponse)
        {
            try
            {
                if (fEMResponse != null)
                {
                    string FEM = fEMResponse;
                    //if (ParseFEMtoBS(FEM, out _BS) == 0)
                    //{
                    //    btnGeneraDE.Enabled = true;
                    //}
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.PreserveWhitespace = false;
                    xmlDoc.LoadXml(FEM);
                    string foto;
                    string qr;
                    if (FEM.StartsWith("<RD"))
                    {
                        XmlNode node = xmlDoc.GetElementsByTagName("ExtensionItem")[1];
                        foto = node.ChildNodes[1].InnerText;
                        XmlNode node1 = xmlDoc.GetElementsByTagName("ExtensionItem")[7];
                        qr = node1.ChildNodes[1].InnerText;
                    }
                    else
                    {
                        foto = null;
                        qr = null;
                    }

                    if (qr != null)
                    {
                        byte[] byImgQR = Convert.FromBase64String(qr);
                        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgQR);
                        Bitmap bmp = new Bitmap(Image.FromStream(m));
                        m.Close();
                        picFEMQR.Image = bmp;
                    }

                    if (foto != null)
                    {
                        byte[] byImgF = Convert.FromBase64String(foto);
                        System.IO.MemoryStream m = new System.IO.MemoryStream(byImgF);
                        Bitmap bmp = new Bitmap(Image.FromStream(m));
                        m.Close();
                        picFEMPhoto.Image = bmp;
                    }
                    foreach (XmlNode item in xmlDoc.GetElementsByTagName("ExtensionItem"))
                    {
                        if (!item.ChildNodes[0].InnerText.Equals("Video") && !item.ChildNodes[0].InnerText.Equals("Photo")
                            && !item.ChildNodes[0].InnerText.Equals("qrlinkfem"))
                            richTextBox1.Text = richTextBox1.Text + item.ChildNodes[0].InnerText + "=" + item.ChildNodes[1].InnerText + Environment.NewLine;
                    }

                }
                //    XmlDocument xmlDocABS = new XmlDocument();
                //    xmlDocABS.PreserveWhitespace = false;
                //    xmlDocABS.LoadXml(abs);

                //    // Check arguments.
                //    if (xmlDoc != null)
                //    {
                //        //                        string sample = xmlDoc.GetElementsByTagName("Sample")[0].InnerText;
                //        string sample = xmlDocABS.GetElementsByTagName("Sample")[0].InnerText;
                //    }
                //} else
                //{

            }
            catch (Exception ex)
            {


            }
        }

        private string GetImageInBase64()
        {
            string strimage = null;
            openFileDialog1.Title = "Seleccione un imagen a certificar...";
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                byte[] byimg = System.IO.File.ReadAllBytes(openFileDialog1.FileName); // + ".compressed");
                strimage = Convert.ToBase64String(byimg);
            }
            return strimage;
        }


        private string GetVideoInBase64(string path)
        {
            string ret = null;
            try
            {
                byte[] byVideo = System.IO.File.ReadAllBytes(path); // + ".compressed");
                ret = Convert.ToBase64String(byVideo);
            }
            catch (Exception ex)
            {
                LOG.Error("FEUI.GetImageInBase64 Error [" + ex.Message + "]");
            }
            return ret;
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {

            byte[] imgin = System.IO.File.ReadAllBytes(@"C:\tmp\aa\WaterMark.bmp");
            MemoryStream msin = new MemoryStream(imgin);
            MemoryStream msou = new MemoryStream();
            //byte[] imgout;
            AddWaterMark(msin, "Testito", msou);

            pictureBox4.Image = new Bitmap(msou);

            Image imgbase = Image.FromFile(@"C:\tmp\aa\base.jpg");

            Image imgfinal = OverlayImages(imgbase, pictureBox4.Image, 10, 10);
            imgfinal.Save(@"C:\tmp\aa\imgfinal.jpg");
            pictureBox5.Image = imgfinal;



        }

        public void AddWaterMark(MemoryStream ms, string watermarkText, MemoryStream outputStream)
        {
            System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
            Graphics gr = Graphics.FromImage(img);
            Font font = new Font("Tahoma", (float)40);
            Color color = Color.Black; // Color.FromArgb(50, 241, 235, 105);
            double tangent = (double)img.Height / (double)img.Width;
            double angle = Math.Atan(tangent) * (180 / Math.PI);
            double halfHypotenuse = Math.Sqrt((img.Height * img.Height) + (img.Width * img.Width)) / 2;
            double sin, cos, opp1, adj1, opp2, adj2;

            //for (int i = 100; i > 0; i--)
            //{
            //    font = new Font("Tahoma", i, FontStyle.Bold);
            //    SizeF sizef = gr.MeasureString(watermarkText, font, int.MaxValue);

            //    sin = Math.Sin(angle * (Math.PI / 180));
            //    cos = Math.Cos(angle * (Math.PI / 180));
            //    opp1 = sin * sizef.Width;
            //    adj1 = cos * sizef.Height;
            //    opp2 = sin * sizef.Height;
            //    adj2 = cos * sizef.Width;

            //    if (opp1 + adj1 < img.Height && opp2 + adj2 < img.Width)
            //        break;
            //    //
            //}

            StringFormat stringFormat = new StringFormat();
            //stringFormat.Alignment = StringAlignment.Center;
            //stringFormat.LineAlignment = StringAlignment.Center;
            stringFormat.Alignment = StringAlignment.Near;
            stringFormat.LineAlignment = StringAlignment.Near;

            //gr.SmoothingMode = SmoothingMode.AntiAlias;
            //gr.RotateTransform((float)angle);
            //gr.DrawString(watermarkText, font, new SolidBrush(color), new Point((int)halfHypotenuse, 0), stringFormat);

            font = new Font("Arial", (float)Convert.ToInt32(txtCompany.Text), FontStyle.Regular);
            int ypos = Convert.ToInt32(txtValueId.Text);
            gr.DrawString("Mundo Pacífico", font, new SolidBrush(color), new Point(Convert.ToInt32(txtTypeId.Text), ypos), stringFormat);
            gr.DrawString("RUT 21284415-2", font, new SolidBrush(color), new Point(Convert.ToInt32(txtTypeId.Text), ypos + Convert.ToInt32(txtDocumentId.Text)), stringFormat);
            gr.DrawString("123456N", font, new SolidBrush(color), new Point(Convert.ToInt32(txtTypeId.Text), ypos + (2*Convert.ToInt32(txtDocumentId.Text))), stringFormat);

            img.Save(outputStream, ImageFormat.Jpeg);

            img.Save(@"C:\tmp\aa\WaterMark_ConWM.bmp", ImageFormat.Jpeg);
        }

        /// <remarks>
        /// *****************************************************************
        /// Snippet Title: Overlay Images
        /// Code's author: Elektro
        /// Date Modified: 30-April-2015
        /// *****************************************************************
        /// </remarks>
        /// <summary>
        /// Overlay an image over a background image.
        /// </summary>
        /// <param name="backImage">The background image.</param>
        /// <param name="topImage">The topmost image.</param>
        /// <param name="topPosX">An optional adjustment of the top image's "X" position.</param>
        /// <param name="topPosY">An optional adjustment of the top image's "Y" position.</param>
        /// <returns>The overlayed image.</returns>
        /// <exception cref="ArgumentNullException">backImage or topImage</exception>
        /// <exception cref="ArgumentException">Image bounds are greater than background image.;topImage</exception>
        public Image OverlayImages(Image backImage, Image topImage, int topPosX = 0, int topPosY = 0)
        {

            if (backImage == null)
            {
                throw new ArgumentNullException(paramName: "backImage");

            }
            else if (topImage == null)
            {
                throw new ArgumentNullException(paramName: "topImage");

            }
            else if ((topImage.Width > backImage.Width) || (topImage.Height > backImage.Height))
            {
                throw new ArgumentException("Image bounds are greater than background image.", "topImage");

            }
            else
            {
                topPosX += Convert.ToInt32(txtMail.Text); //Convert.ToInt32((backImage.Width / 2) - (topImage.Width / 2));
                topPosY += Convert.ToInt32(txtMsgToRead.Text); //Convert.ToInt32((backImage.Height / 2) - (topImage.Height / 2));

                Bitmap bmp = new Bitmap(backImage.Width, backImage.Height);

                using (Graphics canvas = Graphics.FromImage(bmp))
                {
                    canvas.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    canvas.DrawImage(image: backImage, destRect: new Rectangle(0, 0, bmp.Width, bmp.Height), srcRect: new Rectangle(0, 0, bmp.Width, bmp.Height), srcUnit: GraphicsUnit.Pixel);
                    canvas.DrawImage(image: topImage, destRect: new Rectangle(topPosX, topPosY, topImage.Width, topImage.Height), srcRect: new Rectangle(0, 0, topImage.Width, topImage.Height), srcUnit: GraphicsUnit.Pixel);

                    canvas.Save();
                }
                return bmp;
            }
        }

        private void btnGeneraDE_Click(object sender, EventArgs e)
        {

        }

        //=======================================================
        //Service provided by Telerik (www.telerik.com)
        //=======================================================
    }
}
