﻿using BCR.System.Configuration;
using BCR.System.Enum;
using BCR.System.Extension;
using BCR.System.Log;
using Bio.Digital.Receipt.Api;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Bio.Digital.Receipt.Web.API
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {

            //test(); 

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            string pathLogMask = WebConfigurationManager.AppSettings["Log4netPath"];
            string logLevel = WebConfigurationManager.AppSettings["LogLevel"];
            LogType logLevelEnum = logLevel.ToEnum<LogType>(LogType.Debug);

            string nHibernateLogLevel = WebConfigurationManager.AppSettings["NHibernateLogLevel"];
            LogType nHibernateLogLevelEnum = nHibernateLogLevel.ToEnum<LogType>(LogType.Debug);

            LogConfiguration logConfiguration = new LogConfiguration(LogProvider.Log4Net, new FileInfo(pathLogMask), logLevelEnum, nHibernateLogLevelEnum);
            SystemConfiguration systemConfiguration = new SystemConfiguration(logConfiguration, false);

            Log4Bio.Info("Iniciando systema");
        }

        private void test()
        {
            try
            {
                XmlParamIn pin = new XmlParamIn();
                pin.Actionid = 1;
                pin.CIParam = new CIParam();
                pin.CIParam.DynamicParam = new List<Param>();
                Param p = new Param();
                p.key = "k1";
                p.value = "v1";
                pin.CIParam.DynamicParam.Add(p);
                p = new Param();
                p.key = "k2";
                p.value = "v2";
                pin.CIParam.DynamicParam.Add(p);
                string s = JsonConvert.SerializeObject(pin);

                int i = 0;
            }
            catch (Exception ex)
            {

                Log4Bio.Error(" Error: " + ex.Message);
            }
        }
    }
}
