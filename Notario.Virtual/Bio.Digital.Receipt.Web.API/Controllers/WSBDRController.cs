﻿using BCR.System.Log;
using Bio.Digital.Receipt.Api;
using System.Web.Http;

namespace Bio.Digital.Receipt.Web.API.Controllers
{
    public class WSBDRController : BaseApiController
    {
        [Route("api/WSBDR/Proccess/{customTimeOut?}")]
        [HttpPost]
        public XmlParamOut Proccess([FromBody] XmlParamIn xmlParamIn, 
            int customTimeOut = 0)
        {
            Log4Bio.Info("WSBDRController.Proccess");

            string strXmlParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);

            if (string.IsNullOrEmpty(strXmlParamIn))
                return new XmlParamOut
                {
                    ExecutionResult = Errors.IERR_SERIALIZE_XMLPARAMIN,
                    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                };

            string strXmlParamOut;
            int ret = WsBdrHelper.Proccess(strXmlParamIn, out strXmlParamOut, customTimeOut);

            XmlParamOut response = XmlHelper.DeserializeXmlParamOut(strXmlParamOut);
            if (ret != 0)
            {
                Log4Bio.Debug("Error generado en SOAP, traspasamos la respuesta a XMLParamOut, Result:" + ret);
                response.ExecutionResult = ret;
                response.Message += ", " + Errors.GetDescription(ret);
            }

            return response;
        }
    }
}
