﻿using Bio.Digital.Receipt.Web.API.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Bio.Digital.Receipt.Web.API.Controllers
{
    public class BaseApiController : ApiController
    {
        protected WsBdrHelper WsBdrHelper => new WsBdrHelper();
        protected FEMHelper FemHelper => new FEMHelper();
        protected XmlHelper XmlHelper => new XmlHelper();
        protected CIHelper CIHelper => new CIHelper();

    }
}
