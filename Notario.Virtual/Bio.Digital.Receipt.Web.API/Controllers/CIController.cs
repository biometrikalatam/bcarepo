﻿using BCR.System.Log;
using Bio.Digital.Receipt.Api;
using System.Web.Http;

namespace Bio.Digital.Receipt.Web.API.Controllers
{
    public class CIController : BaseApiController
    {
        [Route("api/CI/Proccess/{customTimeOut?}")]
        [HttpPost]
        public XmlParamOut Proccess([FromBody] XmlParamIn xmlParamIn, 
            int customTimeOut = 0)
        {
            Log4Bio.Info("CIController.Proccess");

            //string strXmlParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);
            string strXmlParamIn = XmlHelper.SerializeXmlParamIn(xmlParamIn);

            if (string.IsNullOrEmpty(strXmlParamIn))
                return new XmlParamOut
                {
                    ExecutionResult = Errors.IERR_SERIALIZE_XMLPARAMIN,
                    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                };

            string strXmlParamOut;
            int ret = CIHelper.Proccess(strXmlParamIn, out strXmlParamOut, customTimeOut);

            XmlParamOut response = null;
            if (!string.IsNullOrEmpty(strXmlParamOut))
            {
                response = XmlHelper.DeserializeXmlParamOut(strXmlParamOut);
            }
            else
            {
                response = new XmlParamOut();
            }
            if (ret != 0)
            {
                Log4Bio.Debug("Error generado en SOAP, traspasamos la respuesta a XMLParamOut, Result:" + ret);
                response.ExecutionResult = ret;
                response.Message += ", " + Errors.GetDescription(ret);
            }

            return response;
        }

        [Route("api/CI/CheckStatusCI/{customTimeOut?}")]
        [HttpPost]
        public int CheckStatusCI([FromUri] string trackid,
                            int customTimeOut = 0)
        {
            Log4Bio.Info("CIController.Proccess");

            if (string.IsNullOrEmpty(trackid))
                return Errors.IERR_SERIALIZE_XMLPARAMIN; //new XmlParamOut
                //{
                //    ExecutionResult = Errors.IERR_SERIALIZE_XMLPARAMIN,
                //    Message = Errors.GetDescription(Errors.IERR_SERIALIZE_XMLPARAMIN)
                //};

            //string strXmlParamOut;
            int ret = CIHelper.CheckStatusCI(trackid, customTimeOut);

            //XmlParamOut response = null;
            //if (!string.IsNullOrEmpty(strXmlParamOut))
            //{
            //    response = XmlHelper.DeserializeXmlParamOut(strXmlParamOut);
            //}
            //else
            //{
            //    response = new XmlParamOut();
            //}
            //if (ret != 0)
            //{
            //    Log4Bio.Debug("Error generado en SOAP, traspasamos la respuesta a XMLParamOut, Result:" + ret);
            //    response.ExecutionResult = ret;
            //    response.Message += ", " + Errors.GetDescription(ret);
            //}

            return ret;
        }
    }
}
