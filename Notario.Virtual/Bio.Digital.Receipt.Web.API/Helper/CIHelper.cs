﻿using BCR.System.Log;
using Bio.Digital.Receipt.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Bio.Digital.Receipt.Web.API.Helper
{
    public class CIHelper
    {
        public int TimeOut { get; internal set; }

        /// <summary>
        /// Constructor, recibe como parametro de entrada opcional el timeout global para los llamados a los web service
        /// </summary>
        public CIHelper()
        {
            int timeOut;
            if (Int32.TryParse(WebConfigurationManager.AppSettings["Call_NV_TimeOut"], out timeOut))
                TimeOut = timeOut;
            else
                TimeOut = 30000;
        }

        internal int Proccess(string xmlParamIn, out string xmlParamOut, int customTimeOut)
        {
            int retActual = Errors.IRET_ERR_DESCONOCIDO;
            xmlParamOut = null;

            try
            {
                Log4Bio.Debug("Llamando a Bio.Digital.Receipt.Process, timeOut: "
                    + (customTimeOut > 0 ? customTimeOut : TimeOut));

                using (WSCI.WSCI soapWS = new WSCI.WSCI())
                {
                    soapWS.Timeout = customTimeOut > 0 ? customTimeOut : TimeOut;
                    retActual = soapWS.Process(xmlParamIn, out xmlParamOut);
                    Log4Bio.Info("Respuesta Bio.Digital.Receipt.Process: " + retActual);
                }

                return retActual;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error FEMHelper.Proccess", e);
                return Errors.IRET_ERR_DESCONOCIDO;
            }
        }

        internal int CheckStatusCI(string trackid, int customTimeOut)
        {
            int retActual = Errors.IRET_ERR_DESCONOCIDO;

            try
            {
                Log4Bio.Debug("Llamando a Bio.Digital.Receipt.CheckStatusCI, timeOut: "
                    + (customTimeOut > 0 ? customTimeOut : TimeOut));

                using (WSCI.WSCI soapWS = new WSCI.WSCI())
                {
                    soapWS.Timeout = customTimeOut > 0 ? customTimeOut : TimeOut;
                    retActual = soapWS.CheckStatusCI(trackid);
                    Log4Bio.Info("Respuesta Bio.Digital.Receipt.CheckStatusCI: " + retActual);
                }

                return retActual;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error CIHelper.CheckStatusCI", e);
                return Errors.IRET_ERR_DESCONOCIDO;
            }
        }
    }
}