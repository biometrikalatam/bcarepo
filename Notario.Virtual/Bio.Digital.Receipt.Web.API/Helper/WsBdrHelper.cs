﻿using BCR.System.Log;
using Bio.Digital.Receipt.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Bio.Digital.Receipt.Web.API.Helper
{
    public class WsBdrHelper
    {
        public int TimeOut { get; internal set; }

        /// <summary>
        /// Constructor, recibe como parametro de entrada opcional el timeout global para los llamados a los web service
        /// </summary>
        public WsBdrHelper()
        {
            int timeOut;
            if (Int32.TryParse(WebConfigurationManager.AppSettings["Call_NV_TimeOut"], out timeOut))
                TimeOut = timeOut;
            else
                TimeOut = 30000;
        }

        internal int Proccess(string xmlParamIn, out string xmlParamOut, int customTimeOut)
        {
            int retActual = Errors.IRET_ERR_DESCONOCIDO;
            xmlParamOut = null;

            try
            {
                Log4Bio.Debug("Llamando a BioPortalServerWS.Enroll, timeOut: "
                    + (customTimeOut > 0 ? customTimeOut : TimeOut));

                using (WSDigitalReceipt.WSBDR soapWS = new WSDigitalReceipt.WSBDR())
                {
                    soapWS.Timeout = customTimeOut > 0 ? customTimeOut : TimeOut;
                    retActual = soapWS.Process(xmlParamIn, out xmlParamOut);
                    Log4Bio.Info("Respuesta BioPortalServerWS.Enroll: " + retActual);
                }

                return retActual;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error WsBdrHelper.Proccess", e);
                return Errors.IRET_ERR_DESCONOCIDO;
            }
        }
    }
}