﻿using BCR.System.Log;
using Bio.Digital.Receipt.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bio.Digital.Receipt.Web.API.Helper
{
    public class XmlHelper
    {
        internal string SerializeXmlParamIn(XmlParamIn xmlParamIn)
        {
            Log4Bio.Debug("Seralizando xmlParamIn...");

            try
            {
                return XmlUtils.SerializeObject(xmlParamIn);
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error serializando XmlParamIn", e);
                return null;
            }
        }

        internal XmlParamOut DeserializeXmlParamOut(string xmlParamOut)
        {
            Log4Bio.Debug("Deseralizando xmlParamOut...");

            try
            {
                return XmlUtils.DeserializeObject<XmlParamOut>(xmlParamOut);
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error deserializando XmlParamOut", e);
                return new XmlParamOut
                {
                    ExecutionResult = Errors.IERR_DESERIALIZE_XMLPARAMOUT,
                    Message = Errors.GetDescription(Errors.IERR_DESERIALIZE_XMLPARAMOUT)
                };
            }
        }
    }
}