﻿using BCR.System.Log;
using Bio.Digital.Receipt.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Bio.Digital.Receipt.Web.API.Helper
{
    public class FEMHelper
    {
        public int TimeOut { get; internal set; }

        /// <summary>
        /// Constructor, recibe como parametro de entrada opcional el timeout global para los llamados a los web service
        /// </summary>
        public FEMHelper()
        {
            int timeOut;
            if (Int32.TryParse(WebConfigurationManager.AppSettings["Call_NV_TimeOut"], out timeOut))
                TimeOut = timeOut;
            else
                TimeOut = 30000;
        }

        internal int Proccess(string xmlParamIn, out string xmlParamOut, int customTimeOut)
        {
            int retActual = Errors.IRET_ERR_DESCONOCIDO;
            xmlParamOut = null;

            try
            {
                Log4Bio.Debug("Llamando a Bio.Digital.Receipt.Process, timeOut: "
                    + (customTimeOut > 0 ? customTimeOut : TimeOut));

                using (FEMWS.FEM soapWS = new FEMWS.FEM())
                {
                    soapWS.Timeout = customTimeOut > 0 ? customTimeOut : TimeOut;
                    retActual = soapWS.Process(xmlParamIn, out xmlParamOut);
                    Log4Bio.Info("Respuesta Bio.Digital.Receipt.Process: " + retActual);
                }

                return retActual;
            }
            catch (Exception e)
            {
                Log4Bio.Error("Error FEMHelper.Proccess", e);
                return Errors.IRET_ERR_DESCONOCIDO;
            }
        }
    }
}