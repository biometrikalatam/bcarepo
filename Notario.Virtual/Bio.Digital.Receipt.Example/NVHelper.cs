﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Example.NVWS;

namespace Bio.Digital.Receipt.Example
{
    /// <summary>
    /// 
    /// </summary>
    public class NVHelper
    {
  
        internal static int Notarize(string url, int idapp, string origen, string desc, string[] extensiones, out string xmlRDenvelope, out string msg)
        {
            int ret = 0;
            xmlRDenvelope = null;
            msg = null;
            Api.RdExtensionItem item;
            string xmlparamin;
            string xmlparamout;
            Api.XmlParamOut oParamout;

            try
            {
                Api.XmlParamIn oParamin = new Api.XmlParamIn();
                oParamin.Actionid = 1;
                oParamin.ApplicationId = idapp;
                oParamin.Description = desc;
                oParamin.Origin = Convert.ToInt32(origen);
                oParamin.Extensiones = new Api.RdExtensionItem[extensiones.Length];
                for (int i = 0; i < extensiones.Length; i++)
                {
                    string[] sep = extensiones[i].Split('|');
                    item = new Api.RdExtensionItem();
                    item.key = sep[0];
                    item.value = sep[1];
                    oParamin.Extensiones[i] = item;
                }
                xmlparamin = Api.XmlUtils.SerializeAnObject(oParamin);
                using (NVWS.WSBDR NV = new NVWS.WSBDR())
                {
                    NV.Url = url;
                    NV.Timeout = 600000;
                    int rdRet = NV.Process(xmlparamin, out xmlparamout);
                    if (rdRet == 0)
                    {
                        oParamout = Api.XmlUtils.DeserializeObject<Api.XmlParamOut>(xmlparamout);
                        xmlRDenvelope = oParamout.Receipt;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(xmlparamout)) {
                            oParamout = Api.XmlUtils.DeserializeObject<Api.XmlParamOut>(xmlparamout);
                            msg = "Error de NV [" + rdRet + " - " + oParamout.Message + "]";
                        } else
                        {
                            msg = "Error de NV [" + rdRet + "]";
                        }
                        
                    }
                    ret = rdRet;
                }
            }
            catch (Exception ex)
            {
                msg = "Error Desconocido Notarizando = " + ex.Message;   
                ret = -1;
            }

            return ret;
        }


       
        internal static int NVVerify(string url, string rd, int type, out string retorno, out string msg)
        {
            int ret = 0;
            retorno = null;
            msg = null;
            try
            {
                Api.XmlParamIn oParamin = new Api.XmlParamIn();
                oParamin.Actionid = 2;
                oParamin.Receipt = rd;
                //oParamin.ApplicationId = idapp;
                //oParamin.Description = desc;
                //oParamin.Origin = Convert.ToInt32(origen);
                //oParamin.Extensiones = new Api.RdExtensionItem[extensiones.Length];
                //for (int i = 0; i < extensiones.Length; i++)
                //{
                //    string[] sep = extensiones[i].Split('|');
                //    item = new Api.RdExtensionItem();
                //    item.key = sep[0];
                //    item.value = sep[1];
                //    oParamin.Extensiones[i] = item;
                //}
                string xmlparamin = Api.XmlUtils.SerializeAnObject(oParamin);
                using (NVWS.WSBDR NV = new NVWS.WSBDR())
                {
                    NV.Url = url;
                    NV.Timeout = 30000;
                    ret = NV.Process(xmlparamin, out retorno);
                    Api.XmlParamOut oParamout;
                    if (ret == 0)
                    {
                        oParamout = Api.XmlUtils.DeserializeObject<Api.XmlParamOut>(retorno);
                        if (oParamout != null)
                        {
                            msg = "Resultado Verificación = " +  oParamout.ResultVerify;
                        }
                    } else
                    {
                        msg = "Error [" + ret.ToString() + "]";
                    }

                    //RDRetorno rdRet = NV.VerifyReceipt(rd, type);

                    //if (rdRet.codigo == 0)
                    //{
                    //    retorno = rdRet.xmlret;
                    //}
                    //else
                    //{
                    //    msg = rdRet.codigo + " - " + rdRet.descripcion;
                    //}
                    //ret = rdRet.codigo;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "Error => " + ex.Message;
            }

            return ret;
        }

        internal static string ExtractRecipt(string retorno)
        {
            string ret = null; 
            try
            {
                Api.XmlParamOut pout = Api.XmlUtils.DeserializeObject<Api.XmlParamOut>(retorno);
                if (pout != null)
                {
                    ret = pout.Receipt;
                }
            }
            catch (Exception ex)
            {
                
            }
            return ret;
        }

        internal static int NVGet(string url, string idapp, string receiptid, out string retorno, out string msg)
        {
            int ret = 0;
            retorno = null;
            msg = null;
            try
            {
                Api.XmlParamIn oParamin = new Api.XmlParamIn();
                oParamin.Actionid = 3;
                oParamin.ReceiptId = receiptid;
                oParamin.ApplicationId = Convert.ToInt32(idapp);
 
                string xmlparamin = Api.XmlUtils.SerializeAnObject(oParamin);
                using (NVWS.WSBDR NV = new NVWS.WSBDR())
                {
                    NV.Url = url;
                    NV.Timeout = 30000;
                    ret = NV.Process(xmlparamin, out retorno);
                    Api.XmlParamOut oParamout;
                    if (ret == 0)
                    {
                        oParamout = Api.XmlUtils.DeserializeObject<Api.XmlParamOut>(retorno);
                        if (oParamout != null)
                        {
                            msg = "Resultado Get OK = " + !String.IsNullOrEmpty(oParamout.Receipt);
                        }
                    }
                    else
                    {
                        msg = "Error [" + ret.ToString() + "]";
                    }

                }
            }
            catch (Exception ex)
            {
                ret = -1;
                msg = "Error => " + ex.Message;
            }

            return ret;
        }


        internal static int Notarize(string url, int actionid, string video, string image, string typeid, string valueid, string documentid,
            string deviceid, string georef, string email, out string xmlRDenvelope, out string msgerrNV)
        {
            msgerrNV = "";
            int ret = 0;
            xmlRDenvelope = null;
            //msg = null;
            Bio.Digital.Receipt.Api.RdExtensionItem item;
            string xmlparamin;
            string xmlparamout;
            Bio.Digital.Receipt.Api.XmlParamOut oParamout;

            try
            {          
                Api.XmlParamIn oParamin = new Api.XmlParamIn();
                oParamin.Actionid = actionid; //FEMGenerate mas Add Water Mark
                oParamin.ApplicationId = 6; // Properties.Settings.Default.NVIdApp;
                oParamin.Companyid = 9; // Properties.Settings.Default.NVCompanyId;
                oParamin.Description = "FEM Signature";
                oParamin.Origin = 1;
                oParamin.Extensiones = new Bio.Digital.Receipt.Api.RdExtensionItem[7];
                item = new Api.RdExtensionItem();
                item.key = "Video";
                item.value = video;
                oParamin.Extensiones[0] = item;
                item = new Api.RdExtensionItem();
                item.key = "Photo";
                item.value = image;
                oParamin.Extensiones[1] = item;
                item = new Api.RdExtensionItem();
                item.key = "FirmanteId";
                item.value = typeid + "_" + valueid;
                oParamin.Extensiones[2] = item;
                item = new Api.RdExtensionItem();
                item.key = "DocumentId";
                item.value = documentid;
                oParamin.Extensiones[3] = item;
                item = new Api.RdExtensionItem();
                item.key = "DeviceId";
                item.value = deviceid;
                oParamin.Extensiones[4] = item;
                item = new Api.RdExtensionItem();
                item.key = "GeoRef";
                item.value = georef;
                oParamin.Extensiones[5] = item;
                item = new Api.RdExtensionItem();
                item.key = "eMail";
                item.value = email;
                oParamin.Extensiones[6] = item;

                

                xmlparamin = Bio.Digital.Receipt.Api.XmlUtils.SerializeAnObject(oParamin);

                using (FEMWS.FEM NV = new FEMWS.FEM())
                {
                    NV.Url = url; //Properties.Settings.Default.Bio_Digital_Receipt_Example_FEMWS_FEM;
                    NV.Timeout = 6000000; // Properties.Settings.Default.NVTimeout;
                    //4 - Advanced Signature para BCI
                    int rdRet = NV.Process(xmlparamin, out xmlparamout);
                    if (rdRet == 0)
                    {
                        oParamout = Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                        xmlRDenvelope = oParamout.Receipt;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(xmlparamout))
                        {
                            oParamout = Bio.Digital.Receipt.Api.XmlUtils.DeserializeObject<Bio.Digital.Receipt.Api.XmlParamOut>(xmlparamout);
                        }
                        else
                        {
                        }

                    }
                    ret = rdRet;
                }
            }
            catch (Exception ex)
            {
                ret = -1;
            }
            return ret;
        }
    }
}