﻿using Bio.Digital.Receipt.Api;
using Bio.Digital.Receipt.Api.Json;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bio.Digital.Receipt.Example
{
    public partial class Form1 : Form
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Form1));

        bool ES_ESTRESS = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listExt.Items.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listExt.Items.Add(txtKey.Text.Trim() + "|" + txtValue.Text.Trim());
        }

        private bool ValidaDatos(out string msg)
        {
            bool ret = false;
            msg = null;
            try
            {
                if (listExt.Items.Count == 0)
                {
                    msg = "Error => Debe cargar extensiones...";
                    return false;
                }

                if (String.IsNullOrEmpty(txtIdApp.Text))
                {
                    msg = "Error => Debe ingresar un Id de aplicación...";
                    return false;
                }

                ret = true;
            }
            catch (Exception ex)
            {
                msg = "Excepcion ValidaDatos = " + ex.Message;
                ret = false;
            }
            return ret;
        }

        private void rtbView_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(rtbView.Text))
            {
                button4.Enabled = false;
            }
            else
            {
                button4.Enabled = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string retorno;
            string msg;
            int retget = NVHelper.NVVerify(txtURL.Text + "WSBDR.asmx", rtbView.Text, 1, out retorno, out msg);
            if (retget == 0)
            {
                rtbView.Text = retorno;
                btnFEMWebApi.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + retorno;
            }
            else
            {
                rtbView.Text = "Error = " + retget.ToString() + " - " + msg;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            rtbView.Text = "";
            btnFEMWebApi.DocumentText = "";
            txtIdApp.Text = "";
            txtOrigen.Text = "";
            txtDescripcion.Text = "";
            txtKey.Text = "";
            txtValue.Text = "";
            listExt.Items.Clear();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string retorno;
            string msg;
            int retget = NVHelper.NVGet(txtURL.Text + "WSBDR.asmx", txtIdApp.Text, txtReceiptid.Text, out retorno, out msg);
            if (retget == 0)
            {
                rtbView.Text = retorno;
                btnFEMWebApi.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + NVHelper.ExtractRecipt(retorno);
            }
            else
            {
                rtbView.Text = "Error = " + retget.ToString() + " - " + msg;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string xmlNV;
            string msg;

            if (ValidaDatos(out msg))
            {

                string[] arr = new string[this.listExt.Items.Count];

                int i = 0;
                foreach (var item in listExt.Items)
                {
                    arr[i++] = item.ToString();
                }

                rtbView.Text = "Notarizando en " + txtURL.Text + "...";
                int ret = NVHelper.Notarize(txtURL.Text + "WSBDR.asmx", Convert.ToInt32(txtIdApp.Text), txtOrigen.Text, txtDescripcion.Text, arr, out xmlNV, out msg);
                if (ret == 0)
                {
                    rtbView.Text = xmlNV;
                    btnFEMWebApi.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + xmlNV;
                }
                else
                {
                    rtbView.Text = "ERROR => " + msg;
                }
            }
            else
            {
                rtbView.Text = "ERROR => " + msg;
            }

        }

        private void btnNotarizarAPI_Click(object sender, EventArgs e)
        {
            string xmlNV = null;
            string msg = null;
            if (ValidaDatos(out msg))
            {

                string[] arr = new string[this.listExt.Items.Count];

                int i = 0;
                foreach (var item in listExt.Items)
                {
                    arr[i++] = item.ToString();
                }

                // API
                // Primero instanciamos la clase de comunicación con la API
                APIWsBdr APIWsBdr = new APIWsBdr(txtUrlAPI.Text.Trim());

                // Construimos el objeto parametro de entrada con los datos obtenidos
                XmlParamIn xmlParamIn = new XmlParamIn
                {
                    Actionid = 1,
                    ApplicationId = Convert.ToInt32(txtIdApp.Text),
                    Description = txtDescripcion.Text,
                    Origin = Convert.ToInt32(txtOrigen.Text),
                    Extensiones = new RdExtensionItem[arr.Length]
                };

                // Agregamos las Extensiones utilizadas a nuestro objeto
                for (int x = 0; x < arr.Length; x++)
                {
                    string[] sep = arr[x].Split('|');
                    RdExtensionItem item = new RdExtensionItem();
                    item.key = sep[0];
                    item.value = sep[1];
                    xmlParamIn.Extensiones[x] = item;
                }

                rtbView.Text = "Notarizando en " + txtUrlAPI.Text + "...";

                // Para la ejecución solo llamamos APIWsBdr.Proccess y le pasamos como parametro de entrada nuestro objeto con los datos obtenidos
                XmlParamOut xmlParamOut = APIWsBdr.Proccess(xmlParamIn);
                if (xmlParamOut != null)
                    xmlNV = XmlUtils.SerializeObject<XmlParamOut>(xmlParamOut);
                else
                    rtbView.Text = "ERROR => Error al deserializar XmlParamOut";

                if (xmlParamOut.ExecutionResult == 0)
                {
                    rtbView.Text = xmlNV;
                    btnFEMWebApi.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + xmlNV;
                }
                else
                {
                    rtbView.Text = "ERROR => " + xmlParamOut.Message;
                }
                
            }
            else
            {
                rtbView.Text = "ERROR => " + msg;
            }
        }

        private void btnRecuperarAPI_Click(object sender, EventArgs e)
        {
            string xmlNV = null;
            // API
            // Primero instanciamos la clase de comunicación con la API
            APIWsBdr APIWsBdr = new APIWsBdr(txtUrlAPI.Text.Trim());

            XmlParamIn xmlParamIn = new XmlParamIn
            {
                Actionid = 3,
                ReceiptId = txtReceiptid.Text.Trim(),
                ApplicationId = Convert.ToInt32(txtIdApp.Text.Trim())
            };

            // Para la ejecución solo llamamos APIWsBdr.Proccess y le pasamos como parametro de entrada nuestro objeto con los datos obtenidos
            XmlParamOut xmlParamOut = APIWsBdr.Proccess(xmlParamIn);
            if (xmlParamOut != null)
                xmlNV = XmlUtils.SerializeObject<XmlParamOut>(xmlParamOut);
            else
                rtbView.Text = "ERROR => Error al deserializar XmlParamOut";

            if (xmlParamOut.ExecutionResult == 0)
            {
                rtbView.Text = xmlNV;
                btnFEMWebApi.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + NVHelper.ExtractRecipt(xmlNV);
            }
            else
            {
                rtbView.Text = "ERROR => " + xmlParamOut.Message;
            }
        }

        private void btnVerifAPI_Click(object sender, EventArgs e)
        {
            string xmlNV = null;
            // API
            // Primero instanciamos la clase de comunicación con la API
            APIWsBdr APIWsBdr = new APIWsBdr(txtUrlAPI.Text.Trim());

            XmlParamIn xmlParamIn = new XmlParamIn
            {
                Actionid = 2,
                Receipt = rtbView.Text.Trim()
            };

            // Para la ejecución solo llamamos APIWsBdr.Proccess y le pasamos como parametro de entrada nuestro objeto con los datos obtenidos
            XmlParamOut xmlParamOut = APIWsBdr.Proccess(xmlParamIn);
            if (xmlParamOut != null)
                xmlNV = XmlUtils.SerializeObject<XmlParamOut>(xmlParamOut);
            else
                rtbView.Text = "ERROR => Error al deserializar XmlParamOut";

            if (xmlParamOut.ExecutionResult == 0)
            {
                rtbView.Text = xmlNV;
                btnFEMWebApi.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + xmlNV;
            }
            else
            {
                rtbView.Text = "ERROR => " + xmlParamOut.Message;
            }
        }

 
        private string GetImageInBase64()
        {
            string strimage = null;
            //openFileDialog1.Title = "Seleccione un imagen a certificar...";
            //if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            if (!string.IsNullOrEmpty(txtPathFoto.Text))
            {
                byte[] byimg = System.IO.File.ReadAllBytes(txtPathFoto.Text); // + ".compressed");
                strimage = Convert.ToBase64String(byimg);
            }
            return strimage;
        }


        private string GetVideoInBase64(string path)
        {
            string ret = null;
            try
            {
                byte[] byVideo = System.IO.File.ReadAllBytes(path); // + ".compressed");
                ret = Convert.ToBase64String(byVideo);
            }
            catch (Exception ex)
            {
                
            }
            return ret;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            string xmlNV = null;
            string msg = null;
            if (ValidaDatosFEM(out msg))
            {
                openFileDialog1.Title = "Seleccione un video a certificar...";
                if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
                {

                    try
                    {
                        string video = GetVideoInBase64(openFileDialog1.FileName);
                        if (String.IsNullOrEmpty(video))
                        {
                            rtbView.Text = "Error leyendo video...";
                            return;
                        }

                        string image = GetImageInBase64();

                        //Armo XmlParamIn para enviar parámetros
                        XmlParamIn oParamin = new Api.XmlParamIn();
                        oParamin.Actionid = 4; //FEMGenerate mas Add Water Mark
                        oParamin.ApplicationId = 6; // Properties.Settings.Default.NVIdApp;
                        oParamin.Companyid = 9; // Properties.Settings.Default.NVCompanyId;
                        oParamin.Description = "FEM Signature";
                        oParamin.Origin = Convert.ToInt32(txtOrigen.Text);

                        oParamin.Extensiones = new Bio.Digital.Receipt.Api.RdExtensionItem[7];
                        RdExtensionItem item = new Api.RdExtensionItem();
                        item.key = "Video";
                        item.value = video;
                        oParamin.Extensiones[0] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "Photo";
                        item.value = image;
                        oParamin.Extensiones[1] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "FirmanteId";
                        item.value = txtTypeId.Text + " " + txtValue.Text;
                        oParamin.Extensiones[2] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "DocumentId";
                        item.value = txtDocId.Text;
                        oParamin.Extensiones[3] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "DeviceId";
                        item.value = "MASExample";
                        oParamin.Extensiones[4] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "GeoRef";
                        item.value = "-33.450001,-70.667000";
                        oParamin.Extensiones[5] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "eMail";
                        item.value = "gsuhti@biometrika.cl";
                        oParamin.Extensiones[6] = item;
                        //string xmlparamin = Bio.Digital.Receipt.Api.XmlUtils.SerializeAnObject(oParamin);

                        // API
                        // Primero instanciamos la clase de comunicación con la API
                        APIFem APIFem = new APIFem(txtUrlAPI.Text.Trim());

                         rtbView.Text = "Notarizando en " + txtUrlAPI.Text + "...";

                        // Para la ejecución solo llamamos APIWsBdr.Proccess y le pasamos como parametro de entrada nuestro objeto con los datos obtenidos
                        XmlParamOut xmlParamOut = APIFem.Proccess(oParamin);
                        if (xmlParamOut != null)
                            xmlNV = XmlUtils.SerializeObject<XmlParamOut>(xmlParamOut);
                        else
                            rtbView.Text = "ERROR => Error al deserializar XmlParamOut";

                        if (xmlParamOut.ExecutionResult == 0)
                        {
                            rtbView.Text = xmlNV;
                            btnFEMWebApi.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + xmlNV;
                        }
                        else
                        {
                            rtbView.Text = "ERROR => " + xmlParamOut.Message;
                        }
                    }
                    catch (Exception ex)
                    {
                        rtbView.Text = "ERROR => " + ex.Message;
                    }
                }
            }
            else
            {
                rtbView.Text = "ERROR => " + msg;
            }
        }

        private bool ValidaDatosFEM(out string msg)
        {
            bool ret = false;
            msg = null;
            try
            {
                if (string.IsNullOrEmpty(txtDocId.Text) ||
                    string.IsNullOrEmpty(txtValueId.Text) ||
                    string.IsNullOrEmpty(txtTypeId.Text))
                {
                    msg = "Error => Debe cargar valores para la FEM";
                    return false;
                }

                ret = true;
            }
            catch (Exception ex)
            {
                msg = "Excepcion ValidaDatosFEM = " + ex.Message;
                ret = false;
            }
            return ret;
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            string xmlNV = null;
            string msg = null;
            if (ValidaDatosFEM(out msg))
            {
                //openFileDialog1.Title = "Seleccione un video a certificar...";
                //if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
                if (!string.IsNullOrEmpty(txtPathVideo.Text) && !string.IsNullOrEmpty(txtPathFoto.Text))
                {

                    try
                    {
                        string video = GetVideoInBase64(txtPathVideo.Text);
                        if (String.IsNullOrEmpty(video))
                        {
                            rtbView.Text = "Error leyendo video...";
                            return;
                        }

                        string image = GetImageInBase64();

                        //Armo XmlParamIn para enviar parámetros
                        XmlParamIn oParamin = new Api.XmlParamIn();
                        oParamin.Actionid = chkActionId.Checked ? 4 : 1; ; //FEMGenerate mas Add Water Mark
                        oParamin.ApplicationId = 6; // Properties.Settings.Default.NVIdApp;
                        oParamin.Companyid = 9; // Properties.Settings.Default.NVCompanyId;
                        oParamin.Description = "FEM Signature";
                        oParamin.Origin = Convert.ToInt32(txtOrigen.Text);

                        oParamin.Extensiones = new Bio.Digital.Receipt.Api.RdExtensionItem[7];
                        RdExtensionItem item = new Api.RdExtensionItem();
                        item.key = "Video";
                        item.value = video;
                        oParamin.Extensiones[0] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "Photo";
                        item.value = image;
                        oParamin.Extensiones[1] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "FirmanteId";
                        item.value = txtTypeId.Text + " " + txtValueId.Text;
                        oParamin.Extensiones[2] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "DocumentId";
                        item.value = txtDocId.Text;
                        oParamin.Extensiones[3] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "DeviceId";
                        item.value = "MASExample";
                        oParamin.Extensiones[4] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "GeoRef";
                        item.value = "-33.450001,-70.667000";
                        oParamin.Extensiones[5] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "eMail";
                        item.value = "gsuhti@biometrika.cl";
                        oParamin.Extensiones[6] = item;
                        //string xmlparamin = Bio.Digital.Receipt.Api.XmlUtils.SerializeAnObject(oParamin);

                        // API
                        // Primero instanciamos la clase de comunicación con la API
                        APIFem APIFem = new APIFem(txtUrlAPI.Text.Trim());

                        rtbView.Text = "Notarizando en " + txtUrlAPI.Text + "...";

                        // Para la ejecución solo llamamos APIWsBdr.Proccess y le pasamos como parametro de entrada nuestro objeto con los datos obtenidos
                        //string xmlparamin = XmlUtils.SerializeObject<XmlParamIn>(oParamin);
                        //System.IO.StreamWriter sw = new System.IO.StreamWriter(@"C:\tmp\aa\xmlparamin.xml");
                        //sw.Write(xmlparamin);
                        //sw.Close();

                        XmlParamOut xmlParamOut = APIFem.Proccess(oParamin, 120000);
                        if (xmlParamOut != null)
                        {
                            //xmlNV = XmlUtils.SerializeObject<XmlParamOut>(xmlParamOut);
                            //System.IO.StreamWriter sw1 = new System.IO.StreamWriter(@"C:\tmp\aa\xmlparamout.xml");
                            //sw1.Write(xmlNV);
                            //sw1.Close();

                            rtbView.Text = xmlParamOut.Receipt;
                            btnFEMWebApi.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + xmlParamOut.Receipt;
                        }
                        else
                        {
                            rtbView.Text = "ERROR => " + xmlParamOut.Message;
                        }
                    }
                    catch (Exception ex)
                    {
                        rtbView.Text = "ERROR => " + ex.Message;
                    }
                }
            }
            else
            {
                rtbView.Text = "ERROR => " + msg;
            }
        }

        private void button7_Click_1(object sender, EventArgs e)
        {
            ExecuteFEM("button7");
        }

        private void ExecuteFEM(string IdThread)
        {
            string RDEnvelope;

            //openFileDialog1.Title = "Seleccione un video a certificar...";
            //if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            if (!string.IsNullOrEmpty(txtPathVideo.Text) && !string.IsNullOrEmpty(txtPathFoto.Text))
            {

                try
                {
                    string video = GetVideoInBase64(txtPathVideo.Text);
                    if (String.IsNullOrEmpty(video))
                    {
                        rtbView.Text = "Error leyendo video...";
                        return;
                    }
                    //_notarizing = true;
                    string image = GetImageInBase64();
                    string typeid = txtTypeId.Text;
                    string valueid = txtValueId.Text;
                    string documentid = txtDocId.Text;
                    string deviceid = "MACGGS";
                    string georef = "-33.450001,-70.667000";
                    string mail = "gsuhit@biometrika.cl";
                    string xmlRDEnvelope;
                    string msgerrNV;
                    int actionid = chkActionId.Checked ? 4 : 1;
                    string url = txtURL.Text + "FEM.asmx";
                    int ret = NVHelper.Notarize(url, actionid, video, image, typeid, valueid, documentid, deviceid, georef, mail,
                                                        out xmlRDEnvelope, out msgerrNV);
                    if (ret == 0)
                    {
                        if (ES_ESTRESS)
                        {
                            //SetMsg("  Notarizado OK!" + Environment.NewLine, true);
                            SetMsg("  " + IdThread + " - Notarizado OK!", true);
                            //rtbView.Refresh();
                        }
                        else
                        {
                            RDEnvelope = xmlRDEnvelope;
                            btnFEMWebApi.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + RDEnvelope;
                        }

                    }
                    else
                    {
                        if (ES_ESTRESS)
                        {
                            SetMsg("  " + IdThread + " - Notarizado NOOK!!! [" + ret.ToString() + "]" , true);
                        }
                        else
                        {
                            rtbView.Text = "Error notarizando [ret=" + ret.ToString() + "-" + msgerrNV + "]";
                        }
                    }
                }
                catch (Exception ex)
                {
                    SetMsg("  " + IdThread + " - Error notarizando [" + ex.Message + "]", true);
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            frmTestEstres frm = new frmTestEstres();
            frm.Show(this);
        }

        private void btnPathVideo_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Seleccione un video a certificar...";
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {

                try
                {
                    txtPathVideo.Text = openFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    rtbView.Text = "Error seleccionando video [" + ex.Message + "]";
                }
            }
        }

        private void btnPathFoto_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Seleccione un video a certificar...";
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {

                try
                {
                    txtPathFoto.Text = openFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    rtbView.Text = "Error seleccionando foto [" + ex.Message + "]";
                }
            }
        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }


        internal static Thread tTest = null;
        private void button9_Click_1(object sender, EventArgs e)
        {
            string msg = "Test Estres iniciado => " + Environment.NewLine +
                           "  Cantidad Threads = " + txtThreads.Text + " x Cantidad Repeticiones = " + txtRepeticiones.Text + " => " +
                           (Convert.ToInt32(txtThreads.Text) * Convert.ToInt32(txtRepeticiones.Text)).ToString() + " archivos en Storage!" + Environment.NewLine +
                           Environment.NewLine +
                           " (Add WM = " + (chkActionId.Checked).ToString() + ")" +
                           Environment.NewLine +
                           "Verifique el log con los resultados y teimpos asociados!";

            log.Info(msg);
            rtbView.Text = msg;

            for (int i = 0; i < Convert.ToInt32(txtThreads.Text); i++)
            {
                //tTest = new Thread(new ThreadStart(ExecuteTestStress));
                tTest = new Thread(() => ExecuteTestStress(i.ToString()));
                tTest.Start();
            }
            rtbView.Text = rtbView.Text + Environment.NewLine + Environment.NewLine + "Threads Lanzados!";
        }

        private void ExecuteTestStress(string threadNumber)
        {
            ES_ESTRESS = true;
            //SetMsg("Start Estress: Repeticiones => " + txtRepeticiones.Text + Environment.NewLine, true);
            SetMsg("TH " + threadNumber + ": - Start Estress: Repeticiones => " + txtRepeticiones.Text, true);
            DateTime dtStart = DateTime.Now;
            if (chkFEM.Checked)
            {
                if (chkWS.Checked)
                {
                    for (int i = 0; i < Convert.ToInt32(txtRepeticiones.Text); i++)
                    {
                        //SetMsg("  ----> Repeticion = " + (i + 1).ToString() + " - Notarizando..." + Environment.NewLine, true);
                        SetMsg("TH " + threadNumber + ": ----> Repeticion = " + (i + 1).ToString() + " - Notarizando...", true);
                        //rtbView.Refresh();
                        //button7_Click_1(null, null);
                        ExecuteFEM("TH " + threadNumber + ": ----> Repeticion = " + (i + 1).ToString());
                    }
                }
                else
                {
                    for (int i = 0; i < Convert.ToInt32(txtRepeticiones.Text); i++)
                    {
                        button8_Click_1(null, null);
                    }
                }
            }
            else
            {
                if (chkWS.Checked)
                {
                    for (int i = 0; i < Convert.ToInt32(txtRepeticiones.Text); i++)
                    {

                    }
                }
                else
                {
                    for (int i = 0; i < Convert.ToInt32(txtRepeticiones.Text); i++)
                    {

                    }
                }
            }
            DateTime dtEnd = DateTime.Now;

            SetMsg(Environment.NewLine +
                           "TH " + threadNumber + ": Fin Test Estress =>" + Environment.NewLine +
                           "   Start = " + dtStart.ToString("dd/MM/yyyy HH:mm:ss") + Environment.NewLine +
                           "   End = " + dtEnd.ToString("dd/MM/yyyy HH:mm:ss") + Environment.NewLine +
                           "   Tiempo Total = " + (dtEnd - dtStart).TotalSeconds.ToString() + " segundos [" +
                           (dtEnd - dtStart).TotalMinutes.ToString() + " minutos]", true); 
            ES_ESTRESS = false;
        }

        // This delegate enables asynchronous calls for setting
        // the text property on a TextBox control.
        delegate void SetMsgCallback(string msg, bool add);
        object lockOBJ1 = new object();
        private void SetMsg(string msg, bool add)
        {
            log.Info(msg);
            //// InvokeRequired required compares the thread ID of the
            //// calling thread to the thread ID of the creating thread.
            //// If these threads are different, it returns true.
            
            //if (this.rtbView.InvokeRequired)
            //{
            //    SetMsgCallback d = new SetMsgCallback(SetMsg);
            //    try
            //    {
            //        this.Invoke(d, new object[] { msg, add });
            //    }
            //    catch (Exception ex) { }
            //}
            //else
            //{
            //    lock (lockOBJ1)
            //    {
            //        try
            //        {
            //            if (add)
            //            {
            //                this.rtbView.Text = rtbView.Text + msg;
            //            } else
            //            {
            //                this.rtbView.Text = msg;
            //            }
            //            rtbView.Refresh();
            //        }
            //        catch (Exception ex) { }
            //    }

            //}
            
        }

        private void btnCIAPI_Click(object sender, EventArgs e)
        {
            string xmlNV = null;
            string msg = null;
            if (ValidaDatosFEM(out msg))
            {
                //openFileDialog1.Title = "Seleccione un video a certificar...";
                //if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
                if (!string.IsNullOrEmpty(txtPathVideo.Text) && !string.IsNullOrEmpty(txtPathFoto.Text))
                {

                    try
                    {
                        string video = GetVideoInBase64(txtPathVideo.Text);
                        if (String.IsNullOrEmpty(video))
                        {
                            rtbView.Text = "Error leyendo video...";
                            return;
                        }

                        string image = GetImageInBase64();

                        //Armo XmlParamIn para enviar parámetros
                        XmlParamIn oParamin = new Api.XmlParamIn();
                        oParamin.Actionid = chkActionId.Checked ? 4 : 1; ; //FEMGenerate mas Add Water Mark
                        oParamin.ApplicationId = 6; // Properties.Settings.Default.NVIdApp;
                        oParamin.Companyid = 9; // Properties.Settings.Default.NVCompanyId;
                        oParamin.Description = "FEM Signature";
                        oParamin.Origin = Convert.ToInt32(txtOrigen.Text);

                        oParamin.Extensiones = new Bio.Digital.Receipt.Api.RdExtensionItem[7];
                        RdExtensionItem item = new Api.RdExtensionItem();
                        item.key = "Video";
                        item.value = video;
                        oParamin.Extensiones[0] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "Photo";
                        item.value = image;
                        oParamin.Extensiones[1] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "FirmanteId";
                        item.value = txtTypeId.Text + " " + txtValueId.Text;
                        oParamin.Extensiones[2] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "DocumentId";
                        item.value = txtDocId.Text;
                        oParamin.Extensiones[3] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "DeviceId";
                        item.value = "MASExample";
                        oParamin.Extensiones[4] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "GeoRef";
                        item.value = "-33.450001,-70.667000";
                        oParamin.Extensiones[5] = item;
                        item = new Api.RdExtensionItem();
                        item.key = "eMail";
                        item.value = "gsuhti@biometrika.cl";
                        oParamin.Extensiones[6] = item;
                        //string xmlparamin = Bio.Digital.Receipt.Api.XmlUtils.SerializeAnObject(oParamin);

                        // API
                        // Primero instanciamos la clase de comunicación con la API
                        APIFem APIFem = new APIFem(txtUrlAPI.Text.Trim());

                        rtbView.Text = "Notarizando en " + txtUrlAPI.Text + "...";

                        // Para la ejecución solo llamamos APIWsBdr.Proccess y le pasamos como parametro de entrada nuestro objeto con los datos obtenidos
                        //string xmlparamin = XmlUtils.SerializeObject<XmlParamIn>(oParamin);
                        //System.IO.StreamWriter sw = new System.IO.StreamWriter(@"C:\tmp\aa\xmlparamin.xml");
                        //sw.Write(xmlparamin);
                        //sw.Close();

                        XmlParamOut xmlParamOut = APIFem.Proccess(oParamin, 120000);
                        if (xmlParamOut != null)
                        {
                            //xmlNV = XmlUtils.SerializeObject<XmlParamOut>(xmlParamOut);
                            //System.IO.StreamWriter sw1 = new System.IO.StreamWriter(@"C:\tmp\aa\xmlparamout.xml");
                            //sw1.Write(xmlNV);
                            //sw1.Close();

                            rtbView.Text = xmlParamOut.Receipt;
                            btnFEMWebApi.DocumentText = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + xmlParamOut.Receipt;
                        }
                        else
                        {
                            rtbView.Text = "ERROR => " + xmlParamOut.Message;
                        }
                    }
                    catch (Exception ex)
                    {
                        rtbView.Text = "ERROR => " + ex.Message;
                    }
                }
            }
            else
            {
                rtbView.Text = "ERROR => " + msg;
            }
        }

        private void btnCI_Click(object sender, EventArgs e)
        {

        }
    }
}