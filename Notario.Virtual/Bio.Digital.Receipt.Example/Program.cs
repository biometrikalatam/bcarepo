﻿using log4net;
using log4net.Config;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Bio.Digital.Receipt.Example
{
    static class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            //TestSerilizeDict();

            //TestSend();
            //XmlDocument doc = new XmlDocument();
            //doc.Load(@"D:\de\CI_20191127180400.xml");

            ////Display all the book titles.
            //XmlNodeList elemList = doc.GetElementsByTagName("ExtensionItem");
            //foreach (XmlNode item in elemList)
            //{
            //    if (item.InnerText.IndexOf("PDFCICertify") >= 0)
            //    {
            //        string val = item.LastChild.InnerText;
            //    }
            //}

            //DateTime s = null;
            //string s1 = s.ToString("dd/MM/yyyy");
            //bool b = CedulaVencida("26-08-2019");
            //b = CedulaVencida("10/02/10");
            //b = CedulaVencida("19/07/2017");
            //b = CedulaVencida("09-26-2019");
            //b = CedulaVencida("08-26-18");



            //string xml1 = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?><xml><data>45</data></xml>";
            //byte[] by1 = Encoding.Default.GetBytes(xml1);
            //string b64 = Convert.ToBase64String(by1);

            //byte[] by2 = Convert.FromBase64String(b64);
            //string xml2 = Encoding.Default.GetString(by2);

            //int i = 0;

            XmlConfigurator.Configure(new FileInfo("Logger.cfg"));
            log.Info("Bio.Digital.Receipt.Example Start...");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            log.Info("Bio.Digital.Receipt.Example End!");
        }

        

        private static void TestSend()
        {
            try
            {
                RestClient Client = new RestClient("http://localhost:9999");
                var request = new RestRequest("/api/WebhookReceiveCI", Method.POST);
                request.Timeout = 60000;
                request.AddJsonBody("<RDEnvelope><Rd Id=\"IDRDREF.v1\" reciboID=\"ed467bc247bf4e0e9e1709989130fadf\" version=\"1\"><ip>200.83.165.91</ip></Rd></RDEnvelope>");

                IRestResponse response = Client.Execute(request);
                var content = response.Content;
           
           
            }
            catch (Exception ex)
            {

                string s = " Error: " + ex.Message;
            }
        }

        internal static bool CedulaVencida(string sDate)
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("es-ES");
            bool ret = false;
            DateTime myDate = new DateTime(1900, 1, 1);
            int mes = 0;
            bool hayerr = false;
            DateTime dt = new DateTime();
            try
            {
                //LOG.Debug("CIResultMobile.aspx.CedulaVencida IN... " + sDate);
                try
                {
                    dt = DateTime.Parse(sDate, ci);
                    ret = dt < DateTime.Now;
                }
                catch (Exception ex)
                {
                    //LOG.Error("CIResultMobile.aspx.CedulaVencida Parse Error = " + ex.Message);
                    hayerr = true;
                }

                if (hayerr)
                {
                    hayerr = false;
                    try
                    {
                        ci = new System.Globalization.CultureInfo("en-US");
                        dt = DateTime.Parse(sDate, ci);
                        ret = dt < DateTime.Now;
                    }
                    catch (Exception ex)
                    {
                        //LOG.Error("CIResultMobile.aspx.CedulaVencida Parse Error = " + ex.Message);
                        hayerr = true;
                    }
                }

                if (hayerr)
                {
                    hayerr = false;
                    string d = sDate.Substring(0, 2);
                    string m = sDate.Substring(3, 2);
                    string y = sDate.Substring(6);

                    try
                    {
                        dt = new DateTime(Convert.ToInt32(d), Convert.ToInt32(m), Convert.ToInt32(y));
                    }
                    catch (Exception ex)
                    {
                        //LOG.Error("CIResultMobile.aspx.CedulaVencida Parse Error = " + ex.Message);
                        hayerr = false;
                    }

                    try
                    {
                        dt = new DateTime(Convert.ToInt32(m), Convert.ToInt32(d), Convert.ToInt32(y));
                    }
                    catch (Exception ex)
                    {
                        //LOG.Error("CIResultMobile.aspx.CedulaVencida Parse Error = " + ex.Message);
                        hayerr = false;
                    }



                    ret = dt < DateTime.Now;
                }

            }
            catch (Exception ex)
            {
                //LOG.Error("CIResultMobile.aspx.CedulaVencida Error = " + ex.Message);
                ret = false;
            }
            //LOG.Debug("CIResultMobile.aspx.CedulaVencida OUT! Vencida = " + ret.ToString());
            return ret;
        }
    }
}
