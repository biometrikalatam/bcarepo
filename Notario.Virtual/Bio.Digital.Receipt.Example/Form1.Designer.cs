﻿namespace Bio.Digital.Receipt.Example
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFEMWebApi = new System.Windows.Forms.WebBrowser();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnVerifAPI = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.rtbView = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIdApp = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.listExt = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtOrigen = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.txtReceiptid = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.btnNotarizarAPI = new System.Windows.Forms.Button();
            this.txtUrlAPI = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnRecuperarAPI = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnPathFoto = new System.Windows.Forms.Button();
            this.btnPathVideo = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPathFoto = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPathVideo = new System.Windows.Forms.TextBox();
            this.chkActionId = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDocId = new System.Windows.Forms.TextBox();
            this.txtValueId = new System.Windows.Forms.TextBox();
            this.txtTypeId = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chkWS = new System.Windows.Forms.CheckBox();
            this.chkFEM = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtThreads = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtRepeticiones = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnCIAPI = new System.Windows.Forms.Button();
            this.btnCI = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnFEMWebApi);
            this.groupBox1.Location = new System.Drawing.Point(666, 291);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(681, 304);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "XML Viewer...";
            // 
            // btnFEMWebApi
            // 
            this.btnFEMWebApi.Location = new System.Drawing.Point(13, 12);
            this.btnFEMWebApi.MinimumSize = new System.Drawing.Size(20, 20);
            this.btnFEMWebApi.Name = "btnFEMWebApi";
            this.btnFEMWebApi.Size = new System.Drawing.Size(660, 278);
            this.btnFEMWebApi.TabIndex = 14;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnVerifAPI);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.rtbView);
            this.groupBox2.Location = new System.Drawing.Point(666, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(681, 269);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Plain Viewer...";
            // 
            // btnVerifAPI
            // 
            this.btnVerifAPI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnVerifAPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVerifAPI.Location = new System.Drawing.Point(592, 58);
            this.btnVerifAPI.Name = "btnVerifAPI";
            this.btnVerifAPI.Size = new System.Drawing.Size(83, 53);
            this.btnVerifAPI.TabIndex = 33;
            this.btnVerifAPI.Text = "Verificar (API)";
            this.btnVerifAPI.UseVisualStyleBackColor = false;
            this.btnVerifAPI.Click += new System.EventHandler(this.btnVerifAPI_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(592, 19);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(83, 33);
            this.button4.TabIndex = 32;
            this.button4.Text = "&Verificar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // rtbView
            // 
            this.rtbView.Location = new System.Drawing.Point(15, 19);
            this.rtbView.Name = "rtbView";
            this.rtbView.Size = new System.Drawing.Size(571, 234);
            this.rtbView.TabIndex = 13;
            this.rtbView.Text = resources.GetString("rtbView.Text");
            this.rtbView.TextChanged += new System.EventHandler(this.rtbView_TextChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(314, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(328, 79);
            this.label1.TabIndex = 17;
            this.label1.Text = "Ejemplo integración con Notario Virtual...";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "URL";
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(24, 148);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(601, 20);
            this.txtURL.TabIndex = 19;
            this.txtURL.Text = "http://nv2.southcentralus.cloudapp.azure.com/NVService/";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(34, 216);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Id Aplicacion";
            // 
            // txtIdApp
            // 
            this.txtIdApp.Location = new System.Drawing.Point(121, 213);
            this.txtIdApp.Name = "txtIdApp";
            this.txtIdApp.Size = new System.Drawing.Size(90, 20);
            this.txtIdApp.TabIndex = 21;
            this.txtIdApp.Text = "1";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.listExt);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.txtValue);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtKey);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(37, 291);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(588, 267);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Extensiones...";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(495, 267);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 30;
            this.button2.Text = "&Borrar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // listExt
            // 
            this.listExt.FormattingEnabled = true;
            this.listExt.Location = new System.Drawing.Point(21, 101);
            this.listExt.Name = "listExt";
            this.listExt.Size = new System.Drawing.Size(549, 160);
            this.listExt.TabIndex = 29;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(52, 72);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 28;
            this.button1.Text = "&Agregar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(52, 46);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(518, 20);
            this.txtValue.TabIndex = 27;
            this.txtValue.Text = "Value1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Value";
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(52, 20);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(279, 20);
            this.txtKey.TabIndex = 25;
            this.txtKey.Text = "Key1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Key";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(121, 239);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(504, 20);
            this.txtDescripcion.TabIndex = 26;
            this.txtDescripcion.Text = "Ejemplo de Integracion NV";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(41, 242);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Descripción";
            // 
            // txtOrigen
            // 
            this.txtOrigen.Location = new System.Drawing.Point(289, 213);
            this.txtOrigen.Name = "txtOrigen";
            this.txtOrigen.Size = new System.Drawing.Size(336, 20);
            this.txtOrigen.TabIndex = 28;
            this.txtOrigen.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(239, 216);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Origen";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(339, 565);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(140, 39);
            this.button3.TabIndex = 31;
            this.button3.Text = "&Notarizar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1214, 601);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(127, 23);
            this.button5.TabIndex = 32;
            this.button5.Text = "&Borrar Todo";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtReceiptid
            // 
            this.txtReceiptid.Location = new System.Drawing.Point(121, 268);
            this.txtReceiptid.Name = "txtReceiptid";
            this.txtReceiptid.Size = new System.Drawing.Size(247, 20);
            this.txtReceiptid.TabIndex = 34;
            this.txtReceiptid.Text = "59f69900f1864c9c8b3597ecec09dcec";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(41, 268);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "Receipt Id";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(374, 263);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(116, 27);
            this.button6.TabIndex = 35;
            this.button6.Text = "&Recuperar";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnNotarizarAPI
            // 
            this.btnNotarizarAPI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnNotarizarAPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNotarizarAPI.Location = new System.Drawing.Point(485, 565);
            this.btnNotarizarAPI.Name = "btnNotarizarAPI";
            this.btnNotarizarAPI.Size = new System.Drawing.Size(140, 39);
            this.btnNotarizarAPI.TabIndex = 36;
            this.btnNotarizarAPI.Text = "Notarizar (API)";
            this.btnNotarizarAPI.UseVisualStyleBackColor = false;
            this.btnNotarizarAPI.Click += new System.EventHandler(this.btnNotarizarAPI_Click);
            // 
            // txtUrlAPI
            // 
            this.txtUrlAPI.Location = new System.Drawing.Point(24, 187);
            this.txtUrlAPI.Name = "txtUrlAPI";
            this.txtUrlAPI.Size = new System.Drawing.Size(601, 20);
            this.txtUrlAPI.TabIndex = 38;
            this.txtUrlAPI.Text = " http://qawebapiservice.enotario.cl/";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(34, 171);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 37;
            this.label9.Text = "API";
            // 
            // btnRecuperarAPI
            // 
            this.btnRecuperarAPI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnRecuperarAPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecuperarAPI.Location = new System.Drawing.Point(496, 263);
            this.btnRecuperarAPI.Name = "btnRecuperarAPI";
            this.btnRecuperarAPI.Size = new System.Drawing.Size(129, 27);
            this.btnRecuperarAPI.TabIndex = 39;
            this.btnRecuperarAPI.Text = "Recuperar (API)";
            this.btnRecuperarAPI.UseVisualStyleBackColor = false;
            this.btnRecuperarAPI.Click += new System.EventHandler(this.btnRecuperarAPI_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(176, 620);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 43;
            this.label10.Text = "TypeId";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(176, 646);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 13);
            this.label11.TabIndex = 45;
            this.label11.Text = "ValueId";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(151, 676);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 13);
            this.label12.TabIndex = 47;
            this.label12.Text = "DocumentId";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnPathFoto);
            this.groupBox4.Controls.Add(this.btnPathVideo);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.txtPathFoto);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.txtPathVideo);
            this.groupBox4.Controls.Add(this.chkActionId);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.txtDocId);
            this.groupBox4.Controls.Add(this.txtValueId);
            this.groupBox4.Controls.Add(this.txtTypeId);
            this.groupBox4.Controls.Add(this.button8);
            this.groupBox4.Controls.Add(this.button7);
            this.groupBox4.Location = new System.Drawing.Point(37, 610);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(621, 153);
            this.groupBox4.TabIndex = 48;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // btnPathFoto
            // 
            this.btnPathFoto.Location = new System.Drawing.Point(577, 73);
            this.btnPathFoto.Name = "btnPathFoto";
            this.btnPathFoto.Size = new System.Drawing.Size(28, 20);
            this.btnPathFoto.TabIndex = 61;
            this.btnPathFoto.Text = "...";
            this.btnPathFoto.UseVisualStyleBackColor = true;
            this.btnPathFoto.Click += new System.EventHandler(this.btnPathFoto_Click);
            // 
            // btnPathVideo
            // 
            this.btnPathVideo.Location = new System.Drawing.Point(577, 29);
            this.btnPathVideo.Name = "btnPathVideo";
            this.btnPathVideo.Size = new System.Drawing.Size(28, 20);
            this.btnPathVideo.TabIndex = 60;
            this.btnPathVideo.Text = "...";
            this.btnPathVideo.UseVisualStyleBackColor = true;
            this.btnPathVideo.Click += new System.EventHandler(this.btnPathVideo_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(221, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 13);
            this.label17.TabIndex = 59;
            this.label17.Text = "Path Foto";
            // 
            // txtPathFoto
            // 
            this.txtPathFoto.Location = new System.Drawing.Point(216, 72);
            this.txtPathFoto.Name = "txtPathFoto";
            this.txtPathFoto.Size = new System.Drawing.Size(354, 20);
            this.txtPathFoto.TabIndex = 58;
            this.txtPathFoto.Text = "C:\\tmp\\aa\\pic.jpg";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(221, 13);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 13);
            this.label16.TabIndex = 57;
            this.label16.Text = "Path Video";
            // 
            // txtPathVideo
            // 
            this.txtPathVideo.Location = new System.Drawing.Point(216, 29);
            this.txtPathVideo.Name = "txtPathVideo";
            this.txtPathVideo.Size = new System.Drawing.Size(354, 20);
            this.txtPathVideo.TabIndex = 56;
            this.txtPathVideo.Text = "C:\\tmp\\aa\\1547731743510.mp4";
            // 
            // chkActionId
            // 
            this.chkActionId.AutoSize = true;
            this.chkActionId.Location = new System.Drawing.Point(10, 119);
            this.chkActionId.Name = "chkActionId";
            this.chkActionId.Size = new System.Drawing.Size(191, 17);
            this.chkActionId.TabIndex = 55;
            this.chkActionId.Text = "Agrega Marca de Agua en Server?";
            this.chkActionId.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(7, 75);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 13);
            this.label15.TabIndex = 54;
            this.label15.Text = "Document Id";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(32, 49);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 53;
            this.label14.Text = "Value Id";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(36, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 13);
            this.label13.TabIndex = 52;
            this.label13.Text = "Type Id";
            // 
            // txtDocId
            // 
            this.txtDocId.Location = new System.Drawing.Point(92, 74);
            this.txtDocId.Name = "txtDocId";
            this.txtDocId.Size = new System.Drawing.Size(93, 20);
            this.txtDocId.TabIndex = 51;
            this.txtDocId.Text = "123456NN";
            // 
            // txtValueId
            // 
            this.txtValueId.Location = new System.Drawing.Point(92, 46);
            this.txtValueId.Name = "txtValueId";
            this.txtValueId.Size = new System.Drawing.Size(93, 20);
            this.txtValueId.TabIndex = 50;
            this.txtValueId.Text = "21284415-2";
            // 
            // txtTypeId
            // 
            this.txtTypeId.Location = new System.Drawing.Point(92, 20);
            this.txtTypeId.Name = "txtTypeId";
            this.txtTypeId.Size = new System.Drawing.Size(55, 20);
            this.txtTypeId.TabIndex = 49;
            this.txtTypeId.Text = "RUT";
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(430, 105);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(140, 39);
            this.button8.TabIndex = 48;
            this.button8.Text = "&FEM (API)";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(283, 105);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(140, 39);
            this.button7.TabIndex = 47;
            this.button7.Text = "&FEM";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chkWS);
            this.groupBox5.Controls.Add(this.chkFEM);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.txtThreads);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.txtRepeticiones);
            this.groupBox5.Controls.Add(this.button9);
            this.groupBox5.Location = new System.Drawing.Point(679, 620);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(316, 143);
            this.groupBox5.TabIndex = 57;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Test Estres...";
            // 
            // chkWS
            // 
            this.chkWS.AutoSize = true;
            this.chkWS.Checked = true;
            this.chkWS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkWS.Location = new System.Drawing.Point(107, 32);
            this.chkWS.Name = "chkWS";
            this.chkWS.Size = new System.Drawing.Size(44, 17);
            this.chkWS.TabIndex = 63;
            this.chkWS.Text = "WS";
            this.chkWS.UseVisualStyleBackColor = true;
            // 
            // chkFEM
            // 
            this.chkFEM.AutoSize = true;
            this.chkFEM.Checked = true;
            this.chkFEM.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFEM.Location = new System.Drawing.Point(35, 32);
            this.chkFEM.Name = "chkFEM";
            this.chkFEM.Size = new System.Drawing.Size(48, 17);
            this.chkFEM.TabIndex = 62;
            this.chkFEM.Text = "FEM";
            this.chkFEM.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(37, 62);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 61;
            this.label19.Text = "Threads";
            // 
            // txtThreads
            // 
            this.txtThreads.Location = new System.Drawing.Point(96, 59);
            this.txtThreads.Name = "txtThreads";
            this.txtThreads.Size = new System.Drawing.Size(55, 20);
            this.txtThreads.TabIndex = 60;
            this.txtThreads.Text = "1";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(9, 88);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(81, 13);
            this.label18.TabIndex = 59;
            this.label18.Text = "Repeticiones";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // txtRepeticiones
            // 
            this.txtRepeticiones.Location = new System.Drawing.Point(96, 85);
            this.txtRepeticiones.Name = "txtRepeticiones";
            this.txtRepeticiones.Size = new System.Drawing.Size(55, 20);
            this.txtRepeticiones.TabIndex = 58;
            this.txtRepeticiones.Text = "2";
            this.txtRepeticiones.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Red;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(157, 35);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(150, 73);
            this.button9.TabIndex = 57;
            this.button9.Text = "&Test Estres";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::Bio.Digital.Receipt.Example.Properties.Resources.FE3D_BIG_50_50_80_50;
            this.pictureBox2.Location = new System.Drawing.Point(44, 584);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(121, 40);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 49;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::Bio.Digital.Receipt.Example.Properties.Resources.NV2_RGB_SF_T;
            this.pictureBox1.Location = new System.Drawing.Point(23, 31);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(285, 79);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnCIAPI);
            this.groupBox6.Controls.Add(this.btnCI);
            this.groupBox6.Location = new System.Drawing.Point(1017, 623);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(324, 140);
            this.groupBox6.TabIndex = 58;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Certification Identity";
            // 
            // btnCIAPI
            // 
            this.btnCIAPI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnCIAPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCIAPI.Location = new System.Drawing.Point(165, 85);
            this.btnCIAPI.Name = "btnCIAPI";
            this.btnCIAPI.Size = new System.Drawing.Size(140, 39);
            this.btnCIAPI.TabIndex = 50;
            this.btnCIAPI.Text = "&CI (API)";
            this.btnCIAPI.UseVisualStyleBackColor = false;
            this.btnCIAPI.Click += new System.EventHandler(this.btnCIAPI_Click);
            // 
            // btnCI
            // 
            this.btnCI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnCI.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCI.Location = new System.Drawing.Point(165, 29);
            this.btnCI.Name = "btnCI";
            this.btnCI.Size = new System.Drawing.Size(140, 39);
            this.btnCI.TabIndex = 49;
            this.btnCI.Text = "&CI";
            this.btnCI.UseVisualStyleBackColor = false;
            this.btnCI.Click += new System.EventHandler(this.btnCI_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1359, 775);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnRecuperarAPI);
            this.Controls.Add(this.txtUrlAPI);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnNotarizarAPI);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.txtReceiptid);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtOrigen);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.txtIdApp);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtURL);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Notario Virtual - Example integración...";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.WebBrowser btnFEMWebApi;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox rtbView;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIdApp;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.ListBox listExt;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtOrigen;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txtReceiptid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button btnNotarizarAPI;
        private System.Windows.Forms.TextBox txtUrlAPI;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnVerifAPI;
        private System.Windows.Forms.Button btnRecuperarAPI;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDocId;
        private System.Windows.Forms.TextBox txtValueId;
        private System.Windows.Forms.TextBox txtTypeId;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.CheckBox chkActionId;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnPathFoto;
        private System.Windows.Forms.Button btnPathVideo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtPathFoto;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPathVideo;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtThreads;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtRepeticiones;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.CheckBox chkWS;
        private System.Windows.Forms.CheckBox chkFEM;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnCIAPI;
        private System.Windows.Forms.Button btnCI;
    }
}

