﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Metamap.Api.Model
{
    public class Samples
    {
        public List<Sample> listSamples;

        public Samples()
        {
            listSamples = new List<Sample>();
        }

        
    }
    public class Sample
    {
        public string inputType { get; set; }
        public int group { get; set; }
        public Data data { get; set; }

        public Sample(string _inputType, int _group, Data _data)
        {
            this.inputType = _inputType;
            this.group = _group;
            this.data = _data;
        }
        
    }
    public class Data
    {
        public string type { get; set; }
        public string page { get; set; }
        public string filename { get; set; }

        public Data(string _type, string _page, string _filename)
        {
            this.type = _type;
            this.page = _page;
            this.filename = _filename;
        }
    }
    
}
