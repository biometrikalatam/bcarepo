﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Metamap.Api.Model
{

    public class MediaData
    {
        public string data;
    }
    public class MediaResponse
    {
        public int code { get; set; }
        public MediaData data { get; set; }
        public string message { get; set; }
        public string name { get; set; }
        public string type { get; set; }

    }
}
