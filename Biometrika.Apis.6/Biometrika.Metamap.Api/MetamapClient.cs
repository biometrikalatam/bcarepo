﻿using Biometrika.Core._6.Errors;
using Biometrika.Metamap.Api.Model;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Text;

namespace Biometrika.Metamap.Api
{
    public class MetamapClient
    {
        private string _ACCESS_TOKEN = null;
        private DateTime _EXPIRATION_TOKEN = DateTime.MinValue;
        public string _URL_BASE { get; set; } = "https://api.getmati.com/";
        public int _URL_TIMEOUT { get; set; } = 30000;
        public string _CLIENT_ID { get; set; }
        public string _CLIENT_SECRET { get; set; }
        public bool _IS_INITIALIZED { get; set; } = false;

        public MetamapClient()
        {

        }
        public MetamapClient(string _Url_Base, int _Url_timeout, string _Client_Id, string _Client_Secret)
        {
            _URL_BASE = _Url_Base;
            _URL_TIMEOUT = _Url_timeout;    
            _CLIENT_ID = _Client_Id;
            _CLIENT_SECRET = _Client_Secret;
        }

        public int Initialize(string _Url_Base, int _Url_timeout, string _Client_Id, string _Client_Secret, out string msgerr)
        {
            int ret = Errors.IERR_UNKNOWN;
            msgerr = null;
            try
            {
                if (string.IsNullOrEmpty(_Url_Base) ||
                    string.IsNullOrEmpty(_Client_Id) ||
                    string.IsNullOrEmpty(_Client_Secret))
                {
                    ret = Errors.IERR_BAD_PARAMETER;
                    msgerr = "Initialize - Los parámetros no pueden ser nulos!";
                    return ret;
                }
                _URL_BASE = _Url_Base;
                if (!_URL_BASE.EndsWith("/")) _URL_BASE += "/";
                _URL_TIMEOUT = _Url_timeout;
                _CLIENT_ID = _Client_Id;
                _CLIENT_SECRET = _Client_Secret;
                ret = RefreshToken(out msgerr);
            }
            catch (Exception ex)
            {
                ret = Errors.IERR_UNKNOWN;
                msgerr = "Initialize - Excp [" + ex.Message + "]";
            }
            return ret;
        }

        public int RefreshToken(out string msgerr)
        {
            int ret = Errors.IERR_UNKNOWN;
            msgerr = null;
            try
            {
                if (string.IsNullOrEmpty(_URL_BASE) ||
                    string.IsNullOrEmpty(_CLIENT_ID) ||
                    string.IsNullOrEmpty(_CLIENT_SECRET))
                {
                    ret = Errors.IERR_BAD_PARAMETER;
                    msgerr = "GetToken - Los valores base no pueden ser nulos!";
                    return ret;
                }

                var client = new RestClient(new RestClientOptions { Timeout = _URL_TIMEOUT }); // _URL_BASE + "oauth");
                //client.Timeout = _URL_TIMEOUT;
                var request = new RestRequest(_URL_BASE + "oauth", Method.Post);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");

                request.AddHeader("Authorization", "Basic " + 
                                  Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(_CLIENT_ID + ":" + _CLIENT_SECRET)));
                request.AddParameter("grant_type", "client_credentials");
                var response = client.Execute(request);

                if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK
                        && !string.IsNullOrEmpty(response.Content))
                {
                    TokenResponse tr = JsonConvert.DeserializeObject<TokenResponse>(response.Content);
                    if (tr != null)
                    {
                        _ACCESS_TOKEN = tr.access_token;
                        _EXPIRATION_TOKEN = DateTime.Now.AddSeconds(tr.expiresIn);
                        _IS_INITIALIZED = true;
                    } else
                    {
                        ret = Errors.IERR_DESERIALIZING_DATA;
                        msgerr = "GetToken - Error deserializando respuesta de Token!";
                    }
                    //LOG.Debug("BPHelper.TxCreateSigner - BPResponseTxCreate != null => " + (bpr != null).ToString());
                }
                else
                {
                    ret = Errors.IERR_3RO_RESPONSE_NULL;
                    msgerr = "GetToken - Respuesta NUll de Token!";
                    //LOG.Warn("BPHelper.TxCreate - response != null => " + (response != null).ToString() + " - " +
                    //         ((response != null) ? response.StatusCode.ToString() : "null"));
                }
            }
            catch (Exception ex)
            {
                _IS_INITIALIZED = false;
                ret = Errors.IERR_UNKNOWN;
                msgerr = "Initialize - Excp [" + ex.Message + "]";
            }
            return ret;
        }

        public string GetToken(out string msgerr)
        {
            int ret = Errors.IERR_UNKNOWN;
            string retToken = null;
            msgerr = null;
            try
            {
                if (string.IsNullOrEmpty(_URL_BASE) ||
                    string.IsNullOrEmpty(_CLIENT_ID) ||
                    string.IsNullOrEmpty(_CLIENT_SECRET))
                {
                    ret = Errors.IERR_BAD_PARAMETER;
                    msgerr = "Initialize - Los valores base no pueden ser nulos!";
                    return null;
                }
               
                if (string.IsNullOrEmpty(_ACCESS_TOKEN) ||
                    _EXPIRATION_TOKEN < DateTime.Now)
                {
                    ret = RefreshToken(out msgerr);
                } else
                {
                    ret = Errors.IERR_OK;
                    msgerr = null;
                }
                retToken = _ACCESS_TOKEN;
            }
            catch (Exception ex)
            {
                retToken = null;
                ret = Errors.IERR_UNKNOWN;
                msgerr = "Initialize - Excp [" + ex.Message + "]";
            }
            return retToken;
        }

        public string GetMedia(string url, string type, out string msgerr)
        {
            string ret = null;
            msgerr = null;
            try
            {
                //Ver si se necesita el type
                //TODO
                //   1) Recuperar desde metamap el dato pedido (url)
                //   2) Devolver en Base64

                var request = new RestRequest(url, RestSharp.Method.Get);
                request.AddHeader("Authorization", "Bearer " + GetToken(out msgerr));
                var client = new RestClient(new RestClientOptions { Timeout = _URL_TIMEOUT });
                var response = client.Execute(request);

                if (response != null) 
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        byte[] video = response.RawBytes;
                        if (video != null)
                        {
                            ret = Convert.ToBase64String(video);
                        }
                        else
                        {
                            msgerr = "Error recuperando el recurso [null]";
                        }
                    } else
                    {
                        try
                        {
                            MediaResponse mresp = JsonConvert.DeserializeObject<MediaResponse>(response.Content);
                            msgerr = "Error recuperando media [" + response.StatusCode + " - " +
                                        (mresp!=null && mresp.message!=null? mresp.message : "No Message") + "]";
                        }
                        catch (Exception ex)
                        {
                            msgerr = "Error recuperando media [" + response.StatusCode + "] [ex = " + ex.Message + "]";
                        }
                        ret = null;
                        
                    }
                } else
                {
                    msgerr = "Error recuperando media [response=null]";
                }
            }
            catch (Exception ex)
            {
                ret = null;
                msgerr = "MetamapClient.GetMedia Excp [" + ex.Message + "]";
            }
            return ret;
        }

        public void CreateTx()
        {
            //var client = new RestClient("https://api.getmati.com/v2/verifications");
            //client.Timeout = 30000;
            //var request = new RestRequest(Method.POST);
            //request.AddHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOnsiX2lkIjoiNjJjMzQ4NDllOThmZjMwMDFjOWRhYzM0IiwibWVyY2hhbnQiOnsiX2lkIjoiNjJjMzQ4NDhkOGQ4ZmEwMDFjOTBmOGU1Iiwib3duZXIiOiI2MmMzNDg0ODA2ZWJlYTAwMWNhYzQ1YmUiLCJzdWJzY3JpcHRpb25TdGF0dXMiOnsidmFsdWUiOiJhY3RpdmUiLCJ1cGRhdGVkQXQiOiIyMDIyLTA3LTIwVDAwOjAwOjAwLjAxOFoifX19LCJ1c2VyIjp7Il9pZCI6IjYyYzM0ODQ4MDZlYmVhMDAxY2FjNDViZSJ9LCJzY29wZSI6InZlcmlmaWNhdGlvbl9mbG93IGlkZW50aXR5OnJlYWQgdmVyaWZpY2F0aW9uOnJlYWQiLCJpYXQiOjE2NTk3MTk5NDUsImV4cCI6MTY1OTcyMzU0NSwiaXNzIjoib2F1dGgyLXNlcnZlciJ9.9DvhdVyK1Z04CrSVqf_b7VixH8JjfMej7wgNo_PWr1Y");
            //request.AddHeader("Content-Type", "application/json");
            //var body = @"{" + "\n" +
            //@"	""flowId"": ""62ec37fb97f544001c256b37""," + "\n" +
            //@"    ""metadata"": {" + "\n" +
            //@"        ""trackid"": ""41075e0b80a74d57954f133458a3f820""," + "\n" +
            //@"        ""date"": ""04/08/2022""," + "\n" +
            //@"        ""email"": ""gsuhit@biometrika.cl""" + "\n" +
            //@"    }" + "\n" +
            //@"} ";
            //request.AddParameter("application/json", body, ParameterType.RequestBody);
            //IRestResponse response = client.Execute(request);
            //Console.WriteLine(response.Content);
        }

        public void SendSamples()
        {
            //var client = new RestClient("https://api.getmati.com/v2/identities/62ed5647420ea4001db5f3a8/send-input");
            //client.Timeout = 30000;
            //var request = new RestRequest(Method.POST);
            //request.AddHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOnsiX2lkIjoiNjJjMzQ4NDllOThmZjMwMDFjOWRhYzM0IiwibWVyY2hhbnQiOnsiX2lkIjoiNjJjMzQ4NDhkOGQ4ZmEwMDFjOTBmOGU1Iiwib3duZXIiOiI2MmMzNDg0ODA2ZWJlYTAwMWNhYzQ1YmUiLCJzdWJzY3JpcHRpb25TdGF0dXMiOnsidmFsdWUiOiJhY3RpdmUiLCJ1cGRhdGVkQXQiOiIyMDIyLTA3LTIwVDAwOjAwOjAwLjAxOFoifX19LCJ1c2VyIjp7Il9pZCI6IjYyYzM0ODQ4MDZlYmVhMDAxY2FjNDViZSJ9LCJzY29wZSI6InZlcmlmaWNhdGlvbl9mbG93IGlkZW50aXR5OnJlYWQgdmVyaWZpY2F0aW9uOnJlYWQiLCJpYXQiOjE2NTk3MTk5NDUsImV4cCI6MTY1OTcyMzU0NSwiaXNzIjoib2F1dGgyLXNlcnZlciJ9.9DvhdVyK1Z04CrSVqf_b7VixH8JjfMej7wgNo_PWr1Y");
            //request.AddParameter("inputs", "[{\"inputType\":\"document-photo\",\"group\":0,\"data\":{\"type\":\"national-id\",\"country\":\"cl\",\"region\":\"\",\"page\":\"front\",\"filename\":\"Frente_600x600.jpg\"}},{\"inputType\":\"document-photo\",\"group\":0,\"data\":{\"type\":\"national-id\",\"country\":\"cl\",\"region\":\"\",\"page\":\"back\",\"filename\":\"reverso_600x600.jpg\"}}]");
            //request.AddFile("document", "/D:/tmp/Cedulas/Frente_600x600.jpg");
            //request.AddFile("document", "/D:/tmp/Cedulas/reverso_600x600.jpg");
            //IRestResponse response = client.Execute(request);
            //Console.WriteLine(response.Content);
        }

        public string GetResultTx(string url, out string msgerr)
        {
            //var client = new RestClient("https://api.getmati.com/v2/verifications/62e41855f41bdb001cb2de0f");
            //client.Timeout = 30000;
            //var request = new RestRequest(Method.GET);
            //request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            //request.AddHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOnsiX2lkIjoiNjJjMzQ4NDllOThmZjMwMDFjOWRhYzM0IiwibWVyY2hhbnQiOnsiX2lkIjoiNjJjMzQ4NDhkOGQ4ZmEwMDFjOTBmOGU1Iiwib3duZXIiOiI2MmMzNDg0ODA2ZWJlYTAwMWNhYzQ1YmUiLCJzdWJzY3JpcHRpb25TdGF0dXMiOnsidmFsdWUiOiJhY3RpdmUiLCJ1cGRhdGVkQXQiOiIyMDIyLTA3LTIwVDAwOjAwOjAwLjAxOFoifX19LCJ1c2VyIjp7Il9pZCI6IjYyYzM0ODQ4MDZlYmVhMDAxY2FjNDViZSJ9LCJzY29wZSI6InZlcmlmaWNhdGlvbl9mbG93IGlkZW50aXR5OnJlYWQgdmVyaWZpY2F0aW9uOnJlYWQiLCJpYXQiOjE2NTk3MTk5NDUsImV4cCI6MTY1OTcyMzU0NSwiaXNzIjoib2F1dGgyLXNlcnZlciJ9.9DvhdVyK1Z04CrSVqf_b7VixH8JjfMej7wgNo_PWr1Y");
            //IRestResponse response = client.Execute(request);
            //Console.WriteLine(response.Content);
            string ret = null;
            msgerr = null;
            try
            {
                //Ver si se necesita el type
                //TODO
                //   1) Recuperar desde metamap el dato pedido (url)
                //   2) Devolver en Base64

                var request = new RestRequest(url, RestSharp.Method.Get);
                request.AddHeader("Authorization", "Bearer " + GetToken(out msgerr));
                var client = new RestClient(new RestClientOptions { Timeout = _URL_TIMEOUT });
                var response = client.Execute(request);

                if (response != null)
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK && !string.IsNullOrEmpty(response.Content))
                    {
                        ret = response.Content.ToString();
                    }
                    else
                    {
                        msgerr = "Error recuperando ResultTx Json [response.content=null]";
                    }
                }
                else
                {
                    msgerr = "Error recuperando ResultTx [response=null]";
                }
            }
            catch (Exception ex)
            {
                ret = null;
                msgerr = "MetamapClient.GetResultTx Excp [" + ex.Message + "]";
            }
            return ret;
        }


    }
}