﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Core._6.Utils
{
    public static class Utils
    {
        //public class Utils
        //{
        //    private static readonly ILog log = LogManager.GetLogger(typeof(Utils));

        //    public static DateTime GetDateTime(string strdt, bool isMin)
        //    {
        //        DateTime dtret = DateTime.Parse("01/01/1950 00:00:00");
        //        try
        //        {
        //            if (isMin) dtret = DateTime.Parse("01/01/1950 00:00:00");
        //            else dtret = DateTime.Parse("01/01/2999 23:59:59");

        //            if (!String.IsNullOrEmpty(strdt))
        //            {
        //                if (strdt.Trim().Length <= 10)
        //                {
        //                    if (isMin) strdt = strdt + " 00:00:00";
        //                    else strdt = strdt + " 23:59:59";
        //                }
        //                dtret = DateTime.Parse(strdt);
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Error Utils.GetDateTime", ex);
        //        }

        //        return dtret;
        //    }

        //    internal static string GetMapFromGoogleMap(string geoRef)
        //    {
        //        string retmap = Global.MAP_DEFAULT;
        //        try
        //        {
        //            //- 33.3513061452878,-70.5044428392026
        //            //string sURL = "https://maps.googleapis.com/maps/api/staticmap?center=" + geoRef +
        //            //              "&zoom=" + Settings.Default.GoogleMapZoom +
        //            //              "&size=" + Settings.Default.GoogleMapSize +
        //            //              "&markers=anchor:24,24%7Cicon:" + Settings.Default.GoogleMapICON + "%7C" + geoRef +
        //            //              "&key=" + Settings.Default.GoogleMapKEY;
        //            string sURL = Settings.Default.GoogleMApURLApi + "?zoom=" + Settings.Default.GoogleMapZoom +
        //                          "&size=" + Settings.Default.GoogleMapSize +
        //                          "&markers=anchor:24,24%7Cicon:" + Settings.Default.GoogleMapICON + "%7C" + geoRef +
        //                          "&key=" + Settings.Default.GoogleMapKEY;
        //            log.Debug("Utils.GetMapFromGoogleMap sURL=" + sURL);
        //            HttpWebRequest wrGETURL = (HttpWebRequest)WebRequest.Create(sURL);
        //            HttpWebResponse webresponse = (HttpWebResponse)wrGETURL.GetResponse();
        //            string ct = webresponse.ContentType;
        //            Stream objStream;
        //            objStream = wrGETURL.GetResponse().GetResponseStream();
        //            byte[] buffer = ReadFully(objStream);
        //            retmap = Convert.ToBase64String(buffer);
        //        }
        //        catch (Exception ex)
        //        {
        //            retmap = Global.MAP_DEFAULT;
        //            log.Error("Utils.GetMapFromGoogleMap Error", ex);
        //        }
        //        return retmap;
        //    }

        //    private static byte[] ReadFully(Stream input)
        //    {
        //        try
        //        {
        //            int bytesBuffer = 1024;
        //            byte[] buffer = new byte[bytesBuffer];
        //            using (MemoryStream ms = new MemoryStream())
        //            {
        //                int readBytes;
        //                while ((readBytes = input.Read(buffer, 0, buffer.Length)) > 0)
        //                {
        //                    ms.Write(buffer, 0, readBytes);
        //                }
        //                return ms.ToArray();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Utils.ReadFully Error ", ex);
        //            return null;
        //        }
        //    }

        //    public static bool SendMail(string[] mailto, string subject, string body,
        //                                byte[] attach, string mimetypeattach,
        //                                string nombreattach)
        //    {
        //        bool ret = false;
        //        MemoryStream ms = null;
        //        try
        //        {
        //            log.Debug("Utils.SendMail IN...");
        //            //save the data to a memory stream


        //            //Envio de Correo
        //            log.Debug("Utils.SendMail UserSMTP = " + Settings.Default.UserSMTP + " / " + Settings.Default.ClaveSMTP);
        //            MailAddress SendFrom = new MailAddress(Settings.Default.UserSMTP, Settings.Default.SMTPDisplayName);
        //            log.Debug("Utils.SendMail SendTo = " + mailto[0]);
        //            MailAddress SendTo = new MailAddress(mailto[0]);
        //            MailMessage MyMessage = null;
        //            try
        //            {
        //                SmtpClient emailClient = new SmtpClient(Settings.Default.ServerSMTP);
        //                emailClient.Port = 587;
        //                emailClient.EnableSsl = true;
        //                NetworkCredential _Credential =
        //                            new NetworkCredential(Settings.Default.UserSMTP, Settings.Default.ClaveSMTP, null);
        //                MyMessage = new MailMessage(SendFrom, SendTo);
        //                if (mailto.Length > 1)
        //                {
        //                    for (int i = 0; i < mailto.Length; i++)
        //                    {
        //                        MyMessage.To.Add(new MailAddress(mailto[i]));
        //                        log.Debug("Utils.SendMail - Correo => Enviar a:" + mailto[i] + " desde : " + SendFrom);
        //                    }
        //                    //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.cl"));
        //                    //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.pe"));
        //                    //MyMessage.To.Add(new MailAddress("gsuhit@yahoo.com"));
        //                }

        //                if (attach != null && attach.Length > 0)
        //                {
        //                    ms = new MemoryStream(attach);
        //                    ms.Position = 0;
        //                    string mt = MediaTypeNames.Application.Pdf;
        //                    if ((mimetypeattach == "docx") || (mimetypeattach == "doc"))
        //                        mt = MediaTypeNames.Application.Rtf;
        //                    else if ((mimetypeattach == "xls") || (mimetypeattach == "xlsx"))
        //                        mt = MediaTypeNames.Application.Octet;
        //                    else if ((mimetypeattach == "pdf"))
        //                        mt = MediaTypeNames.Application.Pdf;
        //                    else
        //                        mt = MediaTypeNames.Application.Octet;
        //                    MyMessage.Attachments.Add(new Attachment(ms, nombreattach, mt));
        //                }

        //                log.Debug("Utils.SendMail Set Body HTML...");
        //                MyMessage.IsBodyHtml = true;
        //                log.Debug("Utils.SendMail Subject = " + subject);
        //                MyMessage.Subject = subject;
        //                log.Debug("Utils.SendMail - Body = " + body);
        //                MyMessage.Body = body;
        //                emailClient.Credentials = _Credential;
        //                try
        //                {
        //                    log.Debug("Utils.SendMail Enviando...");
        //                    emailClient.Send(MyMessage);
        //                    log.Debug("Utils.SendMail Enviado sin error!");
        //                    ret = true;
        //                }
        //                catch (Exception ex)
        //                {
        //                    log.Error("emailClient.Send(MyMessage)- Error", ex);
        //                    ret = false;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                log.Error("emailClient.Send [" + ex.Message + "]");
        //                ret = false;
        //            }

        //            if (ms != null)
        //            {
        //                ms.Close();
        //                ms.Dispose();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Utils.SendMail Error SendMail", ex);
        //            ret = false;
        //        }
        //        log.Debug("Utils.SendMail OUT!");
        //        return ret;

        //    }

        //    public static bool SendMailHtml(string[] mailto, string subject, string body,
        //                                byte[] attach, string mimetypeattach,
        //                                string nombreattach)
        //    {
        //        bool ret = false;
        //        MemoryStream ms = null;
        //        try
        //        {
        //            log.Debug("Utils.SendMail IN...");
        //            //save the data to a memory stream


        //            //Envio de Correo
        //            log.Debug("Utils.SendMail UserSMTP = " + Settings.Default.UserSMTP + " / " + Settings.Default.ClaveSMTP);
        //            MailAddress SendFrom = new MailAddress(Settings.Default.UserSMTP, Settings.Default.SMTPDisplayName);
        //            log.Debug("Utils.SendMail SendTo = " + mailto[0]);
        //            MailAddress SendTo = new MailAddress(mailto[0]);
        //            MailMessage MyMessage = null;
        //            try
        //            {
        //                SmtpClient emailClient = new SmtpClient(Settings.Default.ServerSMTP);
        //                emailClient.Port = 587;
        //                emailClient.EnableSsl = true;
        //                NetworkCredential _Credential =
        //                            new NetworkCredential(Settings.Default.UserSMTP, Settings.Default.ClaveSMTP, null);
        //                MyMessage = new MailMessage(SendFrom, SendTo);
        //                if (mailto.Length > 1)
        //                {
        //                    for (int i = 1; i < mailto.Length; i++)
        //                    {
        //                        MyMessage.To.Add(new MailAddress(mailto[i]));
        //                        log.Debug("Utils.SendMail - Correo => Enviar a:" + mailto[i] + " desde : " + SendFrom);
        //                    }
        //                    //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.cl"));
        //                    //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.pe"));
        //                    //MyMessage.To.Add(new MailAddress("gsuhit@yahoo.com"));
        //                }

        //                if (attach != null && attach.Length > 0)
        //                {
        //                    ms = new MemoryStream(attach);
        //                    ms.Position = 0;
        //                    string mt = MediaTypeNames.Application.Pdf;
        //                    if ((mimetypeattach == "docx") || (mimetypeattach == "doc"))
        //                        mt = MediaTypeNames.Application.Rtf;
        //                    else if ((mimetypeattach == "xls") || (mimetypeattach == "xlsx"))
        //                        mt = MediaTypeNames.Application.Octet;
        //                    else if ((mimetypeattach == "pdf"))
        //                        mt = MediaTypeNames.Application.Pdf;
        //                    else
        //                        mt = MediaTypeNames.Application.Octet;
        //                    MyMessage.Attachments.Add(new Attachment(ms, nombreattach, mt));
        //                }

        //                log.Debug("Utils.SendMail Set Body HTML...");
        //                //MyMessage.IsBodyHtml = true;

        //                //Create two views, one text, one HTML.
        //                //string s = ".";
        //                //System.Net.Mail.AlternateView plainTextView =
        //                //    System.Net.Mail.AlternateView.CreateAlternateViewFromString(s, Encoding.UTF8, "text/html");
        //                System.Net.Mail.AlternateView htmlView =
        //                    System.Net.Mail.AlternateView.CreateAlternateViewFromString(body, Encoding.UTF8, "text/html");

        //                //Add image to HTML version

        //                System.Net.Mail.LinkedResource imageResourceH =
        //                    new System.Net.Mail.LinkedResource(Properties.Settings.Default.LogoMailH, MediaTypeNames.Image.Jpeg);
        //                imageResourceH.ContentId = "HDIImageH";
        //                htmlView.LinkedResources.Add(imageResourceH);

        //                System.Net.Mail.LinkedResource imageResourceF =
        //                    new System.Net.Mail.LinkedResource(Properties.Settings.Default.LogoMailF, MediaTypeNames.Image.Jpeg);
        //                imageResourceF.ContentId = "HDIImageF";
        //                htmlView.LinkedResources.Add(imageResourceF);

        //                //Add two views to message.

        //                //MyMessage.AlternateViews.Add(plainTextView);
        //                MyMessage.AlternateViews.Add(htmlView);

        //                log.Debug("Utils.SendMail Subject = " + subject);
        //                MyMessage.Subject = subject;
        //                log.Debug("Utils.SendMail - Body = " + body);
        //                //MyMessage.Body = body;
        //                emailClient.Credentials = _Credential;
        //                try
        //                {
        //                    log.Debug("Utils.SendMail Enviando...");
        //                    emailClient.Send(MyMessage);
        //                    log.Debug("Utils.SendMail Enviado sin error!");
        //                    ret = true;
        //                }
        //                catch (Exception ex)
        //                {
        //                    log.Error("emailClient.Send(MyMessage)- Error", ex);
        //                    ret = false;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                log.Error("emailClient.Send [" + ex.Message + "]");
        //                ret = false;
        //            }

        //            if (ms != null)
        //            {
        //                ms.Close();
        //                ms.Dispose();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Utils.SendMail Error SendMail", ex);
        //            ret = false;
        //        }
        //        log.Debug("Utils.SendMail OUT!");
        //        return ret;

        //    }

        //    internal static string GetUserName(string additionaldata)
        //    {
        //        string uname = null;
        //        try
        //        {
        //            log.Debug("Utils.GetUserName - IN => additionaldata = " + (string.IsNullOrEmpty(additionaldata) ? "Null" : additionaldata));

        //            if (string.IsNullOrEmpty(additionaldata))
        //                uname = null;
        //            else
        //            {
        //                Dictionary<string, string> dicIN = JsonConvert.DeserializeObject<Dictionary<string, string>>(additionaldata);

        //                if (dicIN.ContainsKey("UserName"))
        //                {
        //                    uname = dicIN["UserName"];
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            uname = null;
        //            log.Error("Utils.GetUserName - Exc Error: " + ex.Message);
        //        }
        //        return uname;
        //    }

        //    public static bool SendMailHtmlSigner(string[] mailto, string subject, string body,
        //                                          byte[] attach, string mimetypeattach, string nombreattach,
        //                                          string logoheader, string logofooter)
        //    {
        //        bool ret = false;
        //        MemoryStream ms = null;
        //        try
        //        {
        //            log.Debug("Utils.SendMail IN...");
        //            //save the data to a memory stream


        //            //Envio de Correo
        //            log.Debug("Utils.SendMail UserSMTP = " + Settings.Default.UserSMTP + " / " + Settings.Default.ClaveSMTP);
        //            MailAddress SendFrom = new MailAddress(Settings.Default.UserSMTP, "Notify"); // Settings.Default.SMTPDisplayName);
        //            log.Debug("Utils.SendMail SendTo = " + mailto[0]);
        //            MailAddress SendTo = new MailAddress(mailto[0]);
        //            MailMessage MyMessage = null;
        //            try
        //            {
        //                SmtpClient emailClient = new SmtpClient(Settings.Default.ServerSMTP);
        //                emailClient.Port = 587;
        //                emailClient.EnableSsl = true;
        //                NetworkCredential _Credential =
        //                            new NetworkCredential(Settings.Default.UserSMTP, Settings.Default.ClaveSMTP, null);
        //                MyMessage = new MailMessage(SendFrom, SendTo);
        //                if (mailto.Length > 1)
        //                {
        //                    for (int i = 1; i < mailto.Length; i++)
        //                    {
        //                        MyMessage.To.Add(new MailAddress(mailto[i]));
        //                        log.Debug("Utils.SendMail - Correo => Enviar a:" + mailto[i] + " desde : " + SendFrom);
        //                    }
        //                    //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.cl"));
        //                    //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.pe"));
        //                    //MyMessage.To.Add(new MailAddress("gsuhit@yahoo.com"));
        //                }

        //                if (attach != null && attach.Length > 0)
        //                {
        //                    ms = new MemoryStream(attach);
        //                    ms.Position = 0;
        //                    string mt = MediaTypeNames.Application.Pdf;
        //                    if ((mimetypeattach == "docx") || (mimetypeattach == "doc"))
        //                        mt = MediaTypeNames.Application.Rtf;
        //                    else if ((mimetypeattach == "xls") || (mimetypeattach == "xlsx"))
        //                        mt = MediaTypeNames.Application.Octet;
        //                    else if ((mimetypeattach == "pdf"))
        //                        mt = MediaTypeNames.Application.Pdf;
        //                    else
        //                        mt = MediaTypeNames.Application.Octet;
        //                    MyMessage.Attachments.Add(new Attachment(ms, nombreattach, mt));
        //                }

        //                log.Debug("Utils.SendMail Set Body HTML...");
        //                //MyMessage.IsBodyHtml = true;

        //                //Create two views, one text, one HTML.
        //                //string s = ".";
        //                //System.Net.Mail.AlternateView plainTextView =
        //                //    System.Net.Mail.AlternateView.CreateAlternateViewFromString(s, Encoding.UTF8, "text/html");
        //                System.Net.Mail.AlternateView htmlView =
        //                    System.Net.Mail.AlternateView.CreateAlternateViewFromString(body, Encoding.UTF8, "text/html");

        //                //Add image to HTML version

        //                System.Net.Mail.LinkedResource imageResource =
        //                    new System.Net.Mail.LinkedResource(logoheader, MediaTypeNames.Image.Jpeg);
        //                imageResource.ContentId = "HDIImageHeader";
        //                htmlView.LinkedResources.Add(imageResource);

        //                System.Net.Mail.LinkedResource imageResource1 =
        //                    new System.Net.Mail.LinkedResource(logofooter, MediaTypeNames.Image.Jpeg);
        //                imageResource1.ContentId = "HDIImageFooter";
        //                htmlView.LinkedResources.Add(imageResource1);

        //                //Add two views to message.

        //                //MyMessage.AlternateViews.Add(plainTextView);
        //                MyMessage.AlternateViews.Add(htmlView);

        //                log.Debug("Utils.SendMail Subject = " + subject);
        //                MyMessage.Subject = subject;
        //                log.Debug("Utils.SendMail - Body = " + body);
        //                //MyMessage.Body = body;
        //                emailClient.Credentials = _Credential;
        //                try
        //                {
        //                    log.Debug("Utils.SendMail Enviando...");
        //                    emailClient.Send(MyMessage);
        //                    log.Debug("Utils.SendMail Enviado sin error!");
        //                    ret = true;
        //                }
        //                catch (Exception ex)
        //                {
        //                    log.Error("emailClient.Send(MyMessage)- Error", ex);
        //                    ret = false;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                log.Error("emailClient.Send [" + ex.Message + "]");
        //                ret = false;
        //            }

        //            if (ms != null)
        //            {
        //                ms.Close();
        //                ms.Dispose();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Utils.SendMail Error SendMail", ex);
        //            ret = false;
        //        }
        //        log.Debug("Utils.SendMail OUT!");
        //        return ret;

        //    }

        //    internal static bool IsCedulaAntigua(string serial)
        //    {
        //        bool bRet = false;
        //        try
        //        {
        //            log.Debug("Utils.IsCedulaAntigua - IN = serial = " + (string.IsNullOrEmpty(serial) ? "Null" : serial));
        //            if (string.IsNullOrEmpty(serial))
        //                return false;
        //            else if (serial.Substring(0, 1).Equals("A"))
        //                bRet = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            bRet = false;
        //            log.Error("Utils.IsCedulaAntigua - Error: " + ex.Message);
        //        }
        //        log.Debug("Utils.IsCedulaAntigua - OUT => IsCedulaAntigua = " + bRet.ToString());
        //        return bRet;
        //    }

        //    public static bool NVNotify(string mailto, string subject, string[] body,
        //                                byte[] attach, string mimetypeattach,
        //                                string nombreattach)
        //    {
        //        string[] amailto = new string[1];
        //        amailto[0] = mailto;
        //        return NVNotify(amailto, subject, body, attach, mimetypeattach, nombreattach);
        //    }

        //    public static bool NVNotify(string[] mailto, string subject, string[] body,
        //                                byte[] attach, string mimetypeattach,
        //                                string nombreattach)
        //    {
        //        bool bret = false;
        //        string body2 = "";
        //        try
        //        {
        //            if (Global._FRONTEND_IMAGE.Equals("BK"))
        //            {  //"<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" + bgcolor=\"#E7F6FE\">
        //               // "<tr rowspan=\"" + (body.Length-1).ToString() + "\" bgcolor=\"#E7F6FE\"><td align=\"center\"></td></tr>" +
        //                string strbody = "<table border=\"0\" width=\"95%\" align=\"center\" bgcolor=\"#FFFFFF\"" + ">" +
        //                                 "<tr><td align=\"center\">" +
        //                                 "</td></tr>";
        //                for (int i = 0; i < body.Length; i++)
        //                {
        //                    strbody += "<tr><td><p style=\"font-family:'Arial'\">" + body[i] + "</p></td></tr>";
        //                }
        //                strbody += "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
        //                                "<a href=\"http://www.biometrikalatam.com\" target=\"_blank\">Biometrika S.A.<a></p></td></tr>" +
        //                                "<tr height=\"5\" bgcolor=\"#34ade2\"><td></td></tr>" +
        //                             "</table>";  //5882ba

        //                body2 = @"<table border=""0"" width=""95%"" align=""center"">" +
        //                               @"<tr><td align=""center""><a href=""http://www.biometrikalatam.com"" target=""_blank"">" +
        //                               @"<img src=""cid:HDIImageH"" width=""100%"" /></a></td></tr></table>" + strbody +
        //                               @"<table border=""0"" width=""95%"" align=""center"">" +
        //                               @"<tr><td align=""center""><a href=""http://www.biometrikalatam.com"" target=""_blank"">" +
        //                               @"<img src=""cid:HDIImageF""  width=""100%"" /></a></td></tr></table>";
        //                body2 = @"<table border=""0"" width=""100%"" align=""center"" bgcolor=""#edf5f9""><tr><td align=""center"">" +
        //                        body2 +
        //                        "<tr><td></table>";
        //                bret = libs.Utils.SendMailHtml(mailto, "BiometrikaLatam Notify - " + subject, body2, attach, mimetypeattach, nombreattach);
        //            }
        //            else
        //            { //Es NV por default
        //                string strbody = "<table border=\"0\" width=\"60%\" align=\"center\">" +
        //                               "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
        //                               "<tr><td align=\"center\">" +
        //                                   "</td></tr>";
        //                for (int i = 0; i < body.Length; i++)
        //                {
        //                    strbody += "<tr><td><p style=\"font-family:'Arial'\">" + body[i] + "</p></td></tr>";
        //                }
        //                strbody += "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
        //                                "<a href=\"http://www.notariovirtual.cl\" target=\"_blank\">Notario Virtual<a>.</p></td></tr>" +
        //                                "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
        //                             "</table>";
        //                body2 = @"<table border=""0"" width=""60%"" align=""center"">" +
        //                               @"<tr><td align=""center""><a href=""http://www.notariovirtual.cl"" target=""_blank"">" +
        //                               @"<img src=""cid:HDIImageH"" /></a></td></tr></table>" + strbody;
        //                bret = libs.Utils.SendMailHtml(mailto, "Notario Virtual Notify - " + subject, body2, attach, mimetypeattach, nombreattach);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            bret = false;
        //            log.Error("Utils.NVNotify Error: " + ex.Message);
        //        }
        //        return bret;
        //    }

        //    public static bool NVNotifySigner(string[] mailto, string subject, string[] body,
        //                                byte[] attach, string mimetypeattach,
        //                                string nombreattach, string logoheader, string logofooter)
        //    {
        //        bool bret = false;
        //        try
        //        {
        //            string strbody = "<table border=\"0\" width=\"60%\" align=\"center\">" +
        //                           "<tr height=\"5\" bgcolor=\"#EA9407\"><td></td></tr>" +
        //                           "<tr><td align=\"center\">" +
        //                               "</td></tr>";
        //            for (int i = 0; i < body.Length; i++)
        //            {
        //                strbody += "<tr><td><p style=\"font-family:'Arial'\">" + body[i] + "</p></td></tr>";
        //            }
        //            strbody += "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
        //                            "<a href=\"http://www.tramitestag.cl\" target=\"_blank\">TramitesTag<a>.</p></td></tr>" +
        //                            "<tr height=\"5\" bgcolor=\"#EA9407\"><td></td></tr>" +
        //                            "<tr><td align=\"right\"><a href=\"http://www.biometrikalatam.com\" target=\"_blank\">" +
        //                            "<img src=\"cid:HDIImageFooter\" /></a></td></tr></table>";
        //            string body2 = @"<table border=""0"" width=""60%"" align=""center"">" +
        //                           @"<tr><td align=""center""><a href=""http://www.tramitestag.cl"" target=""_blank"">" +
        //                           @"<img src=""cid:HDIImageHeader"" /></a></td></tr></table>" + strbody;

        //            bret = libs.Utils.SendMailHtmlSigner(mailto, "Notify - " + subject, body2, attach, mimetypeattach, nombreattach,
        //                                                 logoheader, logofooter);
        //        }
        //        catch (Exception ex)
        //        {
        //            bret = false;
        //            log.Error("Utils.NVNotify Error: " + ex.Message);
        //        }
        //        return bret;
        //    }

        //    public static bool ComprobarFormatoEmail(string sEmailAComprobar)
        //    {
        //        String sFormato;
        //        sFormato = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
        //        if (Regex.IsMatch(sEmailAComprobar, sFormato))
        //        {
        //            if (Regex.Replace(sEmailAComprobar, sFormato, String.Empty).Length == 0)
        //            {
        //                return true;
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }

        //    /// <summary>
        //    /// Parsea una fecha con diferentes formatos. Sino devuelve MinValue
        //    /// Formatos de fecha procesados:
        //    ///     yyyy/MM/dd
        //    ///     yyyy/M/dd
        //    ///     dd/MM/yyyy
        //    ///     dd/M/yyyy
        //    ///     M/dd/yyyy
        //    ///     MM/dd/yyyy
        //    /// </summary>
        //    /// <param name="strDate"></param>
        //    /// <returns></returns>
        //    public static DateTime Parse(string strDate)
        //    {
        //        DateTime dRet = new DateTime(1900, 1, 1);
        //        bool mustReturn = false;
        //        try
        //        {
        //            log.Debug("Utils.Parse IN - Fechav = " + (string.IsNullOrEmpty(strDate) ? "Null" : strDate));
        //            if (string.IsNullOrEmpty(strDate))
        //                return dRet;

        //            //Reemplazo - por /
        //            strDate = strDate.Replace("-", "/");
        //            log.Debug("Utils.Parse IN - Fechav = " + strDate);
        //            try
        //            {
        //                log.Debug("Utils.Parse try yyyy/MM/dd...");
        //                dRet = DateTime.ParseExact(strDate, "yyyy/MM/dd", null);
        //                log.Debug("Utils.Parse try yyyy/MM/dd OK!");
        //                mustReturn = true;
        //            }
        //            catch (Exception ex1)
        //            {
        //                mustReturn = false;
        //                log.Debug("Utils.Parse Ex: " + ex1.Message);
        //            }

        //            if (mustReturn)
        //                return dRet;

        //            try
        //            {
        //                log.Debug("Utils.Parse try yyyy/M/dd...");
        //                dRet = DateTime.ParseExact(strDate, "yyyy/M/dd", null);
        //                log.Debug("Utils.Parse try yyyy/M/dd OK!");
        //                mustReturn = true;
        //            }
        //            catch (Exception ex1)
        //            {
        //                mustReturn = false;
        //                log.Debug("Utils.Parse Ex: " + ex1.Message);
        //            }

        //            if (mustReturn)
        //                return dRet;
        //            //------------------
        //            try
        //            {
        //                log.Debug("Utils.Parse try dd/MM/yyyy...");
        //                dRet = DateTime.ParseExact(strDate, "dd/MM/yyyy", null);
        //                log.Debug("Utils.Parse try dd/MM/yyyy OK!");
        //                mustReturn = true;
        //            }
        //            catch (Exception ex1)
        //            {
        //                mustReturn = false;
        //                log.Debug("Utils.Parse Ex: " + ex1.Message);
        //            }

        //            if (mustReturn)
        //                return dRet;

        //            try
        //            {
        //                log.Debug("Utils.Parse try dd/M/yyyy...");
        //                dRet = DateTime.ParseExact(strDate, "dd/M/yyyy", null);
        //                log.Debug("Utils.Parse try dd/M/yyyy OK!");
        //                mustReturn = true;
        //            }
        //            catch (Exception ex1)
        //            {
        //                mustReturn = false;
        //                log.Debug("Utils.Parse Ex: " + ex1.Message);
        //            }

        //            if (mustReturn)
        //                return dRet;

        //            try
        //            {
        //                log.Debug("Utils.Parse try M/dd/yyyy...");
        //                dRet = DateTime.ParseExact(strDate, "M/dd/yyyy", null);
        //                log.Debug("Utils.Parse try M/dd/yyyy OK!");
        //                mustReturn = true;
        //            }
        //            catch (Exception ex1)
        //            {
        //                mustReturn = false;
        //                log.Debug("Utils.Parse Ex: " + ex1.Message);
        //            }

        //            if (mustReturn)
        //                return dRet;

        //            try
        //            {
        //                log.Debug("Utils.Parse try MM/dd/yyyy...");
        //                dRet = DateTime.ParseExact(strDate, "MM/dd/yyyy", null);
        //                log.Debug("Utils.Parse try MM/dd/yyyy OK!");
        //                mustReturn = true;
        //            }
        //            catch (Exception ex1)
        //            {
        //                mustReturn = false;
        //                log.Debug("Utils.Parse Ex: " + ex1.Message);
        //            }

        //            if (mustReturn)
        //                return dRet;

        //            try
        //            {
        //                log.Debug("Utils.Parse try M/d/yyyy...");
        //                dRet = DateTime.ParseExact(strDate, "M/d/yyyy", null);
        //                log.Debug("Utils.Parse try M/d/yyyy OK!");
        //                mustReturn = true;
        //            }
        //            catch (Exception ex1)
        //            {
        //                mustReturn = false;
        //                log.Debug("Utils.Parse Ex: " + ex1.Message);
        //            }
        //        }
        //        catch (Exception ex)
        //        {

        //            log.Error("Utils.Pars Error: " + ex.Message);
        //        }
        //        return dRet;
        //    }

        //    internal static string EnmascaraMail(string destinataryMail)
        //    {
        //        string sRet = "NULL";
        //        try
        //        {
        //            log.Debug("Utils.EnmascaraMail IN...");
        //            if (string.IsNullOrEmpty(destinataryMail))
        //            {
        //                log.Debug("Utils.EnmascaraMail - Mail Recibido nulo. Sale...");
        //                sRet = "NULL";
        //            }
        //            else
        //            {
        //                log.Debug("Utils.EnmascaraMail - Mail Recibido => " + destinataryMail);
        //                string[] partes = destinataryMail.Split('@');
        //                if (partes.Length != 2)
        //                    return sRet;

        //                partes[0] = partes[0].Substring(0, 1) + "***";
        //                sRet = partes[0] + "@" + partes[1];
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            sRet = "NULL";
        //            log.Error("Utils.EnmascaraMail Error: " + ex.Message);
        //        }
        //        log.Debug("Utils.EnmascaraMail - OUT! => sale con valor => " + sRet);
        //        return sRet;
        //    }

        //    public static int CalcularEdad(DateTime fechaNacimiento)
        //    {
        //        // Obtiene la fecha actual:
        //        DateTime fechaActual = DateTime.Today;

        //        // Comprueba que la se haya introducido una fecha válida; si 
        //        // la fecha de nacimiento es mayor a la fecha actual se muestra mensaje 
        //        // de advertencia:
        //        if (fechaNacimiento > fechaActual)
        //        {
        //            Console.WriteLine("La fecha de nacimiento es mayor que la actual.");
        //            return -1;
        //        }
        //        else
        //        {
        //            int edad = fechaActual.Year - fechaNacimiento.Year;

        //            // Comprueba que el mes de la fecha de nacimiento es mayor 
        //            // que el mes de la fecha actual:
        //            if (fechaNacimiento.Month >= fechaActual.Month)
        //            {
        //                if (fechaNacimiento.Day > fechaActual.Day)
        //                {
        //                    --edad;
        //                }
        //            }

        //            return edad;
        //        }
        //    }

        //    public static string GetMimeType(byte[] data)
        //    {
        //        string WhatType = "Unknown";
        //        try
        //        {
        //            log.Debug("Utils.GetMimeType IN...");
        //            WhatType = GetImageMimeType(data);
        //            log.Debug("Utils.GetMimeType - Sale GetImageMimeType => WhatType = " + WhatType);

        //            if (WhatType.Equals("image/unknown"))
        //            {
        //                log.Debug("Utils.GetMimeType - Entra a GetFileMimeType...");
        //                WhatType = GetFileMimeType(data);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            WhatType = "Unknown";
        //            //LOG.Error(" Error: " + ex.Message);
        //        }
        //        log.Debug("Utils.GetMimeType OUT! Return = " + WhatType);
        //        return WhatType;
        //    }

        //    public static string GetImageMimeType(byte[] imageData)
        //    {
        //        string mimeType = "image/unknown";

        //        try
        //        {
        //            Guid id;
        //            log.Debug("Utils.GetImageMimeType IN...");
        //            using (MemoryStream ms = new MemoryStream(imageData))
        //            {
        //                using (System.Drawing.Image img = System.Drawing.Image.FromStream(ms))
        //                {
        //                    id = img.RawFormat.Guid;
        //                }
        //            }

        //            if (id == ImageFormat.Png.Guid)
        //            {
        //                mimeType = "image/png";
        //            }
        //            else if (id == ImageFormat.Bmp.Guid)
        //            {
        //                mimeType = "image/bmp";
        //            }
        //            else if (id == ImageFormat.Emf.Guid)
        //            {
        //                mimeType = "image/x-emf";
        //            }
        //            else if (id == ImageFormat.Exif.Guid)
        //            {
        //                mimeType = "image/jpeg";
        //            }
        //            else if (id == ImageFormat.Gif.Guid)
        //            {
        //                mimeType = "image/gif";
        //            }
        //            else if (id == ImageFormat.Icon.Guid)
        //            {
        //                mimeType = "image/ico";
        //            }
        //            else if (id == ImageFormat.Jpeg.Guid)
        //            {
        //                mimeType = "image/jpeg";
        //            }
        //            else if (id == ImageFormat.MemoryBmp.Guid)
        //            {
        //                mimeType = "image/bmp";
        //            }
        //            else if (id == ImageFormat.Tiff.Guid)
        //            {
        //                mimeType = "image/tiff";
        //            }
        //            else if (id == ImageFormat.Wmf.Guid)
        //            {
        //                mimeType = "image/wmf";
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            mimeType = "image/unknown";
        //            log.Error("Utils.GetImageMimeType [" + ex.Message + "]");
        //        }
        //        log.Debug("Utils.GetImageMimeType OUT! - Return mimeType = " + mimeType);
        //        return mimeType;
        //    }

        //    internal static string GetMimeTypeFromURLData(string _sSample, out string datab64)
        //    {
        //        string sRet = null;
        //        datab64 = null;
        //        try
        //        {
        //            log.Debug("Utils.GetMimeTypeFromURLData IN...");
        //            List<string> arrSamples = JsonConvert.DeserializeObject<List<string>>(_sSample);

        //            _sSample = arrSamples[0];
        //            if (string.IsNullOrEmpty(_sSample))
        //                return null;

        //            string[] arrAux1 = _sSample.Split(',');
        //            if (arrAux1.Length != 2)
        //                return null;

        //            log.Debug("Utils.GetMimeTypeFromURLData - Set data b64...");
        //            datab64 = arrAux1[1];

        //            string[] arrAux2 = _sSample.Split(';')[0].Split(':');

        //            if (arrAux2[1].Contains("pdf"))
        //            {
        //                log.Debug("Utils.GetMimeTypeFromURLData - Set mimetype = pdf...");
        //                sRet = "PDF";
        //            }
        //            else if (arrAux2[1].Contains("jpg") || arrAux2[1].Contains("jpeg"))
        //            {
        //                log.Debug("Utils.GetMimeTypeFromURLData - Set mimetype = jpg...");
        //                sRet = "JPG";
        //            }
        //            else if (arrAux2[1].Contains("png"))
        //            {
        //                log.Debug("Utils.GetMimeTypeFromURLData - Set mimetype = png...");
        //                sRet = "PNG";
        //            }
        //            else if (arrAux2[1].Contains("bmp"))
        //            {
        //                log.Debug("Utils.GetMimeTypeFromURLData - Set mimetype = bmp...");
        //                sRet = "BMP";
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            sRet = null;
        //            log.Error("Utils.GetMimeTypeFromURLData Error: " + ex.Message);
        //        }
        //        log.Debug("Utils.GetMimeTypeFromURLData OUT! - MimeType = " + sRet);
        //        return sRet;
        //    }

        //    public static string GetFileMimeType(byte[] fileData)
        //    {
        //        String WhatType;
        //        try
        //        {
        //            log.Debug("Utils.GetFileMimeType IN...");
        //            StreamReader sr = new StreamReader(new MemoryStream(fileData));
        //            char[] buf = new char[5];
        //            sr.Read(buf, 0, 4);
        //            sr.Close();
        //            String Hdr = buf[0].ToString()
        //                   + buf[1].ToString()
        //                   + buf[2].ToString()
        //                   + buf[3].ToString()
        //                   + buf[4].ToString();


        //            if (Hdr.StartsWith("%PDF"))
        //                WhatType = "PDF";
        //            else if (Hdr.StartsWith("MZ"))
        //                WhatType = "EXE or DLL";
        //            else if (Hdr.StartsWith("BM"))
        //                WhatType = "BMP";
        //            else if (Hdr.StartsWith("?_"))
        //                WhatType = "HLP (help file)";
        //            else if (Hdr.StartsWith("\0\0\\1"))
        //                WhatType = "Icon (.ico)";
        //            else if (Hdr.StartsWith("\0\0\\2"))
        //                WhatType = "Cursor (.cur)";
        //            else
        //                WhatType = "Unknown";
        //        }
        //        catch (Exception ex)
        //        {
        //            WhatType = "Unknown";
        //            log.Error("Utils.GetFileMimeType [" + ex.Message + "]");
        //        }
        //        log.Debug("Utils.GetFileMimeType OUT! - Return = " + WhatType);
        //        return WhatType;
        //    }

        //    internal static bool NVNotifyBackend(string[] mailto, string subject, string[] body,
        //                                         byte[] attach, string mimetypeattach,
        //                                         string nombreattach)
        //    {
        //        bool bret = false;
        //        try
        //        {
        //            string strbody = "<table border=\"0\" width=\"60%\" align=\"center\">" +
        //                           "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
        //                           "<tr><td align=\"center\">" +
        //                               "</td></tr>";
        //            for (int i = 0; i < body.Length; i++)
        //            {
        //                strbody += "<tr><td><p style=\"font-family:'Arial'\">" + body[i] + "</p></td></tr>";
        //            }
        //            strbody += "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
        //                            "<a href=\"http://www.notariovirtual.cl\" target=\"_blank\">Notario Virtual<a>.</p></td></tr>" +
        //                            "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
        //                         "</table>";
        //            string body2 = @"<table border=""0"" width=""60%"" align=""center"">" +
        //                           @"<tr><td align=""center""><a href=""http://www.notariovirtual.cl"" target=""_blank"">" +
        //                           @"<img src=""cid:HDIImage"" /></a></td></tr></table>" + strbody;

        //            bret = libs.Utils.SendMailHtml(mailto, "Notario Virtual Notify - " + subject, body2, attach, mimetypeattach, nombreattach);
        //        }
        //        catch (Exception ex)
        //        {
        //            bret = false;
        //            log.Error("Utils.NVNotify Error: " + ex.Message);
        //        }
        //        return bret;
        //    }

        //    internal static double GetPorcentajeNEC(double score, double threshold)
        //    {
        //        double ret = 100;
        //        try
        //        {
        //            log.Debug("DEHelper.GetPorcentajeNEC - IN => score = " + score.ToString());
        //            if (score >= 15000 - threshold)
        //            {
        //                ret = 100;
        //            }
        //            else
        //            {
        //                double resto = 15000 - threshold;  //Resto de la diferencia entre 15000 y el umbral
        //                double porcentajeResto = score * 100 / resto; //Calculo el porcentaje de lo que significa ese score para ese resto
        //                double toSume = 20 * porcentajeResto / 100; //Saco el porcentaje sobre el 20% variable que uso
        //                ret = 80 + toSume; //A un 80% fijo por ser positivo le sumo el variable obtenido
        //                if (ret > 100) //Por si acaso se pasa (no deberia pero para estar seguro)
        //                    ret = 100;
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            ret = 100;
        //            log.Error("DEHelper.GetPorcentajeNEC - Error: " + ex.Message);
        //        }
        //        log.Debug("DEHelper.GetPorcentajeNEC - OUT! - Porcentaje = " + ret.ToString());
        //        return ret;
        //    }
        //}
    }
}
