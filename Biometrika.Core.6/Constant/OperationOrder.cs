﻿namespace Biometrika.Core._6.Contant
{
    public class OperationOrder
    {
        static public int OPERATIONORDER_ANY = -2;
        static public string SOPERATIONORDER_ANY = "Cualquiera";
        static public int OPERATIONORDER_All = -1;
        static public string SOPERATIONORDER_All = "Todos";
        static public int OPERATIONORDER_DEFAULT = 0;
        static public string SOPERATIONORDER_DEFAULT = "Default";

        static public int OPERATIONORDER_LOCALONLY = 1;
        static public string SOPERATIONORDER_LOCALONLY = "Local Solo";
        static public int OPERATIONORDER_LOCALFIRST = 2;
        static public string SOPERATIONORDER_LOCALFIRST = "Local Primero";
        static public int OPERATIONORDER_REMOTEONLY = 3;
        static public string SOPERATIONORDER_REMOTEONLY = "Remoto Solo";

        static public string GetName(int af)
        {
            switch (af)
            {
                case -2:
                    return SOPERATIONORDER_ANY;
                case -1:
                    return SOPERATIONORDER_All;
                case 0:
                    return SOPERATIONORDER_DEFAULT;
                case 1:
                    return SOPERATIONORDER_LOCALONLY;
                case 2:
                    return SOPERATIONORDER_LOCALFIRST;
                case 3:
                    return SOPERATIONORDER_REMOTEONLY;
                default:
                    return "Operation no documentada";
            }
        }
    }
}
