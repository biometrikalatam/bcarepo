﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Biometrika.Core._6.Notify
{
    public static class NotifyService
    {
        //private static readonly ILog log = LogManager.GetLogger(typeof(Utils));

      

        //public static bool SendMail(string[] mailto, string subject, string body,
        //                            byte[] attach, string mimetypeattach,
        //                            string nombreattach)
        //{
        //    bool ret = false;
        //    MemoryStream ms = null;
        //    try
        //    {
        //        //log.Debug("Utils.SendMail IN...");
        //        //save the data to a memory stream


        //        //Envio de Correo
        //        log.Debug("Utils.SendMail UserSMTP = " + Settings.Default.UserSMTP + " / " + Settings.Default.ClaveSMTP);
        //        MailAddress SendFrom = new MailAddress(Settings.Default.UserSMTP, Settings.Default.SMTPDisplayName);
        //        log.Debug("Utils.SendMail SendTo = " + mailto[0]);
        //        MailAddress SendTo = new MailAddress(mailto[0]);
        //        MailMessage MyMessage = null;
        //        try
        //        {
        //            SmtpClient emailClient = new SmtpClient(Settings.Default.ServerSMTP);
        //            emailClient.Port = 587;
        //            emailClient.EnableSsl = true;
        //            NetworkCredential _Credential =
        //                        new NetworkCredential(Settings.Default.UserSMTP, Settings.Default.ClaveSMTP, null);
        //            MyMessage = new MailMessage(SendFrom, SendTo);
        //            if (mailto.Length > 1)
        //            {
        //                for (int i = 0; i < mailto.Length; i++)
        //                {
        //                    MyMessage.To.Add(new MailAddress(mailto[i]));
        //                    log.Debug("Utils.SendMail - Correo => Enviar a:" + mailto[i] + " desde : " + SendFrom);
        //                }
        //                //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.cl"));
        //                //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.pe"));
        //                //MyMessage.To.Add(new MailAddress("gsuhit@yahoo.com"));
        //            }

        //            if (attach != null && attach.Length > 0)
        //            {
        //                ms = new MemoryStream(attach);
        //                ms.Position = 0;
        //                string mt = MediaTypeNames.Application.Pdf;
        //                if ((mimetypeattach == "docx") || (mimetypeattach == "doc"))
        //                    mt = MediaTypeNames.Application.Rtf;
        //                else if ((mimetypeattach == "xls") || (mimetypeattach == "xlsx"))
        //                    mt = MediaTypeNames.Application.Octet;
        //                else if ((mimetypeattach == "pdf"))
        //                    mt = MediaTypeNames.Application.Pdf;
        //                else
        //                    mt = MediaTypeNames.Application.Octet;
        //                MyMessage.Attachments.Add(new Attachment(ms, nombreattach, mt));
        //            }

        //            log.Debug("Utils.SendMail Set Body HTML...");
        //            MyMessage.IsBodyHtml = true;
        //            log.Debug("Utils.SendMail Subject = " + subject);
        //            MyMessage.Subject = subject;
        //            log.Debug("Utils.SendMail - Body = " + body);
        //            MyMessage.Body = body;
        //            emailClient.Credentials = _Credential;
        //            try
        //            {
        //                log.Debug("Utils.SendMail Enviando...");
        //                emailClient.Send(MyMessage);
        //                log.Debug("Utils.SendMail Enviado sin error!");
        //                ret = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                log.Error("emailClient.Send(MyMessage)- Error", ex);
        //                ret = false;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("emailClient.Send [" + ex.Message + "]");
        //            ret = false;
        //        }

        //        if (ms != null)
        //        {
        //            ms.Close();
        //            ms.Dispose();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Utils.SendMail Error SendMail", ex);
        //        ret = false;
        //    }
        //    log.Debug("Utils.SendMail OUT!");
        //    return ret;

        //}

        public static bool SendMailHtml(string serversmtp, string usersmtp, string clavesmtp, string displayname,
                                    string logoHeader, string logoFooter,
                                    string[] mailto, string subject, string body,
                                    byte[] attach, string mimetypeattach,
                                    string nombreattach)
        {
            bool ret = false;
            MemoryStream ms = null;
            try
            {
                //log.Debug("Utils.SendMail IN...");
                //save the data to a memory stream


                //Envio de Correo
                //log.Debug("Utils.SendMail UserSMTP = " + usersmtp + " / " + Settings.Default.ClaveSMTP);
                MailAddress SendFrom = new MailAddress(usersmtp, displayname);
                //log.Debug("Utils.SendMail SendTo = " + mailto[0]);
                MailAddress SendTo = new MailAddress(mailto[0]);
                MailMessage MyMessage = null;
                try
                {
                    SmtpClient emailClient = new SmtpClient(serversmtp);
                    emailClient.Port = 587;
                    emailClient.EnableSsl = true;
                    NetworkCredential _Credential =
                                new NetworkCredential(usersmtp, clavesmtp, null);
                    MyMessage = new MailMessage(SendFrom, SendTo);
                    if (mailto.Length > 1)
                    {
                        for (int i = 1; i < mailto.Length; i++)
                        {
                            MyMessage.To.Add(new MailAddress(mailto[i]));
                            //log.Debug("Utils.SendMail - Correo => Enviar a:" + mailto[i] + " desde : " + SendFrom);
                        }
                        //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.cl"));
                        //MyMessage.To.Add(new MailAddress("gsuhit@biometrika.pe"));
                        //MyMessage.To.Add(new MailAddress("gsuhit@yahoo.com"));
                    }

                    if (attach != null && attach.Length > 0)
                    {
                        ms = new MemoryStream(attach);
                        ms.Position = 0;
                        string mt = MediaTypeNames.Application.Pdf;
                        if ((mimetypeattach == "docx") || (mimetypeattach == "doc"))
                            mt = MediaTypeNames.Application.Rtf;
                        else if ((mimetypeattach == "xls") || (mimetypeattach == "xlsx"))
                            mt = MediaTypeNames.Application.Octet;
                        else if ((mimetypeattach == "pdf"))
                            mt = MediaTypeNames.Application.Pdf;
                        else
                            mt = MediaTypeNames.Application.Octet;
                        MyMessage.Attachments.Add(new Attachment(ms, nombreattach, mt));
                    }

                    //log.Debug("Utils.SendMail Set Body HTML...");
                    
                    System.Net.Mail.AlternateView htmlView =
                        System.Net.Mail.AlternateView.CreateAlternateViewFromString(body, Encoding.UTF8, "text/html");

                    //Add image to HTML version
                    if (!string.IsNullOrEmpty(logoHeader))
                    {
                        System.Net.Mail.LinkedResource imageResourceH =
                            new System.Net.Mail.LinkedResource(
                                new System.IO.MemoryStream(Convert.FromBase64String(logoHeader))
                                , MediaTypeNames.Image.Jpeg);
                        imageResourceH.ContentId = "HDIImageH";
                        htmlView.LinkedResources.Add(imageResourceH);
                    }

                    if (!string.IsNullOrEmpty(logoFooter))
                    {
                        System.Net.Mail.LinkedResource imageResourceF =
                        new System.Net.Mail.LinkedResource(
                             new System.IO.MemoryStream(Convert.FromBase64String(logoFooter))
                                , MediaTypeNames.Image.Jpeg);
                        imageResourceF.ContentId = "HDIImageF";
                        htmlView.LinkedResources.Add(imageResourceF);
                    }

                    //Add two views to message.

                    //MyMessage.AlternateViews.Add(plainTextView);
                    MyMessage.AlternateViews.Add(htmlView);

                    //log.Debug("Utils.SendMail Subject = " + subject);
                    MyMessage.Subject = subject;
                    //log.Debug("Utils.SendMail - Body = " + body);
                    //MyMessage.Body = body;
                    emailClient.Credentials = _Credential;
                    try
                    {
                        //log.Debug("Utils.SendMail Enviando...");
                        emailClient.Send(MyMessage);
                        //log.Debug("Utils.SendMail Enviado sin error!");
                        ret = true;
                    }
                    catch (Exception ex)
                    {
                        //log.Error("emailClient.Send(MyMessage)- Error", ex);
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    //log.Error("emailClient.Send [" + ex.Message + "]");
                    ret = false;
                }

                if (ms != null)
                {
                    ms.Close();
                    ms.Dispose();
                }
            }
            catch (Exception ex)
            {
                //log.Error("Utils.SendMail Error SendMail", ex);
                ret = false;
            }
            //log.Debug("Utils.SendMail OUT!");
            return ret;

        }

        

        //public static bool NVNotify(string mailto, string subject, string[] body,
        //                            byte[] attach, string mimetypeattach,
        //                            string nombreattach)
        //{
        //    string[] amailto = new string[1];
        //    amailto[0] = mailto;
        //    return Notify(amailto, subject, body, attach, mimetypeattach, nombreattach);
        //}

        public static bool Notify(string serversmtp, string usersmtp, string clavesmtp, string displayname,
                                    string logoHeader, string logoFooter, 
                                    string[] mailto, string subject, string[] body,
                                    byte[] attach, string mimetypeattach,
                                    string nombreattach)
        {
            bool bret = false;
            string body2 = "";
            try
            {
                //if (Global._FRONTEND_IMAGE.Equals("BK"))
                //{  //"<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" + bgcolor=\"#E7F6FE\">
                    // "<tr rowspan=\"" + (body.Length-1).ToString() + "\" bgcolor=\"#E7F6FE\"><td align=\"center\"></td></tr>" +
                    string strbody = "<table border=\"0\" width=\"95%\" align=\"center\" bgcolor=\"#FFFFFF\"" + ">" +
                                     "<tr><td align=\"center\">" +
                                     "</td></tr>";
                    for (int i = 0; i < body.Length; i++)
                    {
                        strbody += "<tr><td><p style=\"font-family:'Arial'\">" + body[i] + "</p></td></tr>";
                    }
                    strbody += "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
                                    "<a href=\"http://www.biometrikalatam.com\" target=\"_blank\">Biometrika S.A.<a></p></td></tr>" +
                                    "<tr height=\"5\" bgcolor=\"#34ade2\"><td></td></tr>" +
                                 "</table>";  //5882ba

                    body2 = @"<table border=""0"" width=""95%"" align=""center"">" +
                                   @"<tr><td align=""center""><a href=""http://www.biometrikalatam.com"" target=""_blank"">" +
                                   @"<img src=""cid:HDIImageH"" width=""100%"" /></a></td></tr></table>" + strbody +
                                   @"<table border=""0"" width=""95%"" align=""center"">" +
                                   @"<tr><td align=""center""><a href=""http://www.biometrikalatam.com"" target=""_blank"">" +
                                   @"<img src=""cid:HDIImageF""  width=""100%"" /></a></td></tr></table>";
                    body2 = @"<table border=""0"" width=""100%"" align=""center"" bgcolor=""#edf5f9""><tr><td align=""center"">" +
                            body2 +
                            "<tr><td></table>";
                    bret = SendMailHtml(serversmtp, usersmtp, clavesmtp, displayname, logoHeader, logoFooter, 
                        mailto, "BiometrikaLatam Notify - " + subject, body2, attach, mimetypeattach, nombreattach);
                //}
                //else
                //{ //Es NV por default
                //    string strbody = "<table border=\"0\" width=\"60%\" align=\"center\">" +
                //                   "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                //                   "<tr><td align=\"center\">" +
                //                       "</td></tr>";
                //    for (int i = 0; i < body.Length; i++)
                //    {
                //        strbody += "<tr><td><p style=\"font-family:'Arial'\">" + body[i] + "</p></td></tr>";
                //    }
                //    strbody += "<tr><td><p style=\"font-family:'Arial'\">Gracias por utilizar los servicios de " +
                //                    "<a href=\"http://www.notariovirtual.cl\" target=\"_blank\">Notario Virtual<a>.</p></td></tr>" +
                //                    "<tr height=\"5\" bgcolor=\"#5882ba\"><td></td></tr>" +
                //                 "</table>";
                //    body2 = @"<table border=""0"" width=""60%"" align=""center"">" +
                //                   @"<tr><td align=""center""><a href=""http://www.notariovirtual.cl"" target=""_blank"">" +
                //                   @"<img src=""cid:HDIImageH"" /></a></td></tr></table>" + strbody;
                //    bret = libs.Utils.SendMailHtml(mailto, "Notario Virtual Notify - " + subject, body2, attach, mimetypeattach, nombreattach);
                //}
            }
            catch (Exception ex)
            {
                bret = false;
                //log.Error("Utils.NVNotify Error: " + ex.Message);
            }
            return bret;
        }

        

        //public static string GetMimeType(byte[] data)
        //{
        //    string WhatType = "Unknown";
        //    try
        //    {
        //        log.Debug("Utils.GetMimeType IN...");
        //        WhatType = GetImageMimeType(data);
        //        log.Debug("Utils.GetMimeType - Sale GetImageMimeType => WhatType = " + WhatType);

        //        if (WhatType.Equals("image/unknown"))
        //        {
        //            log.Debug("Utils.GetMimeType - Entra a GetFileMimeType...");
        //            WhatType = GetFileMimeType(data);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        WhatType = "Unknown";
        //        //LOG.Error(" Error: " + ex.Message);
        //    }
        //    log.Debug("Utils.GetMimeType OUT! Return = " + WhatType);
        //    return WhatType;
        //}

        //public static string GetImageMimeType(byte[] imageData)
        //{
        //    string mimeType = "image/unknown";

        //    try
        //    {
        //        Guid id;
        //        log.Debug("Utils.GetImageMimeType IN...");
        //        using (MemoryStream ms = new MemoryStream(imageData))
        //        {
        //            using (System.Drawing.Image img = System.Drawing.Image.FromStream(ms))
        //            {
        //                id = img.RawFormat.Guid;
        //            }
        //        }

        //        if (id == ImageFormat.Png.Guid)
        //        {
        //            mimeType = "image/png";
        //        }
        //        else if (id == ImageFormat.Bmp.Guid)
        //        {
        //            mimeType = "image/bmp";
        //        }
        //        else if (id == ImageFormat.Emf.Guid)
        //        {
        //            mimeType = "image/x-emf";
        //        }
        //        else if (id == ImageFormat.Exif.Guid)
        //        {
        //            mimeType = "image/jpeg";
        //        }
        //        else if (id == ImageFormat.Gif.Guid)
        //        {
        //            mimeType = "image/gif";
        //        }
        //        else if (id == ImageFormat.Icon.Guid)
        //        {
        //            mimeType = "image/ico";
        //        }
        //        else if (id == ImageFormat.Jpeg.Guid)
        //        {
        //            mimeType = "image/jpeg";
        //        }
        //        else if (id == ImageFormat.MemoryBmp.Guid)
        //        {
        //            mimeType = "image/bmp";
        //        }
        //        else if (id == ImageFormat.Tiff.Guid)
        //        {
        //            mimeType = "image/tiff";
        //        }
        //        else if (id == ImageFormat.Wmf.Guid)
        //        {
        //            mimeType = "image/wmf";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mimeType = "image/unknown";
        //        log.Error("Utils.GetImageMimeType [" + ex.Message + "]");
        //    }
        //    log.Debug("Utils.GetImageMimeType OUT! - Return mimeType = " + mimeType);
        //    return mimeType;
        //}

        //internal static string GetMimeTypeFromURLData(string _sSample, out string datab64)
        //{
        //    string sRet = null;
        //    datab64 = null;
        //    try
        //    {
        //        log.Debug("Utils.GetMimeTypeFromURLData IN...");
        //        List<string> arrSamples = JsonConvert.DeserializeObject<List<string>>(_sSample);

        //        _sSample = arrSamples[0];
        //        if (string.IsNullOrEmpty(_sSample))
        //            return null;

        //        string[] arrAux1 = _sSample.Split(',');
        //        if (arrAux1.Length != 2)
        //            return null;

        //        log.Debug("Utils.GetMimeTypeFromURLData - Set data b64...");
        //        datab64 = arrAux1[1];

        //        string[] arrAux2 = _sSample.Split(';')[0].Split(':');

        //        if (arrAux2[1].Contains("pdf"))
        //        {
        //            log.Debug("Utils.GetMimeTypeFromURLData - Set mimetype = pdf...");
        //            sRet = "PDF";
        //        }
        //        else if (arrAux2[1].Contains("jpg") || arrAux2[1].Contains("jpeg"))
        //        {
        //            log.Debug("Utils.GetMimeTypeFromURLData - Set mimetype = jpg...");
        //            sRet = "JPG";
        //        }
        //        else if (arrAux2[1].Contains("png"))
        //        {
        //            log.Debug("Utils.GetMimeTypeFromURLData - Set mimetype = png...");
        //            sRet = "PNG";
        //        }
        //        else if (arrAux2[1].Contains("bmp"))
        //        {
        //            log.Debug("Utils.GetMimeTypeFromURLData - Set mimetype = bmp...");
        //            sRet = "BMP";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sRet = null;
        //        log.Error("Utils.GetMimeTypeFromURLData Error: " + ex.Message);
        //    }
        //    log.Debug("Utils.GetMimeTypeFromURLData OUT! - MimeType = " + sRet);
        //    return sRet;
        //}

        //public static string GetFileMimeType(byte[] fileData)
        //{
        //    String WhatType;
        //    try
        //    {
        //        log.Debug("Utils.GetFileMimeType IN...");
        //        StreamReader sr = new StreamReader(new MemoryStream(fileData));
        //        char[] buf = new char[5];
        //        sr.Read(buf, 0, 4);
        //        sr.Close();
        //        String Hdr = buf[0].ToString()
        //               + buf[1].ToString()
        //               + buf[2].ToString()
        //               + buf[3].ToString()
        //               + buf[4].ToString();


        //        if (Hdr.StartsWith("%PDF"))
        //            WhatType = "PDF";
        //        else if (Hdr.StartsWith("MZ"))
        //            WhatType = "EXE or DLL";
        //        else if (Hdr.StartsWith("BM"))
        //            WhatType = "BMP";
        //        else if (Hdr.StartsWith("?_"))
        //            WhatType = "HLP (help file)";
        //        else if (Hdr.StartsWith("\0\0\\1"))
        //            WhatType = "Icon (.ico)";
        //        else if (Hdr.StartsWith("\0\0\\2"))
        //            WhatType = "Cursor (.cur)";
        //        else
        //            WhatType = "Unknown";
        //    }
        //    catch (Exception ex)
        //    {
        //        WhatType = "Unknown";
        //        log.Error("Utils.GetFileMimeType [" + ex.Message + "]");
        //    }
        //    log.Debug("Utils.GetFileMimeType OUT! - Return = " + WhatType);
        //    return WhatType;
        //}

        
    }
}
